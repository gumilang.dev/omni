all: install clean test

test:
	./vendor/bin/phpunit --testdox

install:
	composer install && composer dump-autoload

clean:
	php artisan optimize:clear
