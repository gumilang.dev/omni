<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('test-facebook', function () {
//     return view('test-facebook');
// });

// Auth::routes();

Route::get('/home', 'Home\Controllers\HomeController@index')->name('home');

Route::middleware(['default.cors'])->get('/{any}', 'Home\Controllers\HomeController@index')->where('any', '.*');
