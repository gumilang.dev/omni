<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;

// SET THROTTLE 120 REQUEST / 1 MINUTE
Route::group(['middleware' => ['throttle:120,1', 'bindings']], function () {

  Route::get("clear-cache",function() {
    Cache::flush();
  });

  // EXPORT EXCEL REPORTING
  // Route::get('{appId}/export_excel/user/{start}/{end}', 'User\Controllers\UserController@exportExceluser');
  Route::get('{appId}/export_excel/user/{start}/{end}/{search}', 'Reporting\Controllers\ReportingController@exportUsers');
  Route::get('{appId}/export_excel/message/{start}/{end}', 'Reporting\Controllers\ReportingController@exportMessages');
  Route::get('{appId}/export_excel/room/{start}/{end}', 'Reporting\Controllers\ReportingController@exportRoom');
  Route::get('{appId}/export_excel/hsm', 'Reporting\Controllers\ReportingController@exportHsm');
  Route::get('{appId}/export_excel/tag/{start}/{end}', 'Reporting\Controllers\ReportingController@exportTag');
  Route::get('{appId}/export_excel/note/{start}/{end}', 'Reporting\Controllers\ReportingController@exportNote');
  Route::get('{appId}/export_excel/time-served/{start}/{end}', 'Reporting\Controllers\ReportingController@exportTimeServed');

  Route::get('approve-template/{token}', 'Whatsapp\Controllers\HsmTemplateController@approveTemplate');

  // GENERAL
  Route::get('menus', 'Menu\Controllers\MenuController@getMenus');
  Route::post('submenus', 'Menu\Controllers\SubmenuController@getParent');
  Route::get('submenus', 'Menu\Controllers\MenuController@getSubmenus');
  Route::get('template', 'BroadcastMessage\Controllers\MobileBroadcastController@test');

  // CHANNEL
  Route::resource('channel', 'Channel\Controllers\ChannelController');
  Route::post("upload-file-webchat",'UploadFile\Controllers\UploadFileController@uploadFile');
  Route::post("delete-file-webchat",'UploadFile\Controllers\UploadFileController@deleteFile');

  // LOG
  Route::post('log/save', 'Log\Controllers\LogController@saveApiLog');

  // MOBILE OMNI
  Route::post('mobile-omni/get-data', 'MobileOmni\Controllers\MobileOmniController@getData');

  // SURVEY SENSUM
  Route::group(['prefix' => 'external', 'middleware' => ['subsciption.user']], function () {
    Route::post('get-messages', 'ExternalApi\Controllers\ExternalApiController@getData');
    Route::post('login', 'ExternalApi\Controllers\ExternalApiController@getData');
  });

  Route::group(["prefix" => "user"], function() {
    Route::post('location/update','User\Controllers\UserController@updateLocation');
  });

  // REMOTE AUTH MIDDLEWARE
  Route::group(['prefix' => "{appId}", 'middleware' => ['remote.auth']], function () {
    Route::get('room/test', 'Room\Controllers\RoomController@getRoomListTest');

    // RATING AGENT
    Route::post("rating","AgentRating\Controllers\AgentRatingController@store");

    // ACCESSIBILITY
    Route::get('accessibility/get-role', 'Accessibility\Controllers\AccessibilityController@getRole');
    Route::get('accessibility', 'Accessibility\Controllers\AccessibilityController@index');

    // BROADCAST
    Route::post('broadcast/send-hsm', 'BroadcastMessage\Controllers\HsmBroadcastController@sendBroadcast');

    // HSM TEMPLATE
    Route::resource('hsm-template', 'Whatsapp\Controllers\HsmTemplateController');
    Route::get('template-name', 'Whatsapp\Controllers\HsmTemplateController@getTemplateName');
    Route::get('mail-template', 'Whatsapp\Controllers\HsmTemplateController@sendMailTemplate');

    // INTEGRATION WHATSAPP
    Route::get('whatsapp-account', 'Integration\Controllers\IntegrationController@getWhatsappAccount');

    // PLAN
    Route::group(['prefix' => 'plan'], function () {
      Route::get('menu', 'Plan\Controllers\PlanController@getMenus');
    });

    // Route::get('get-template', 'Whatsapp\Controllers\HsmTemplateController@getTemplate');
    Route::group(['middleware' => ['role']], function () {
      // CUSTOM FILTER
      Route::post("/custom-filter","CustomFilter\Controllers\CustomFilterController@create");
      Route::get("/custom-filter/{userId}","CustomFilter\Controllers\CustomFilterController@get");
      Route::post("/custom-filter/update","CustomFilter\Controllers\CustomFilterController@update");
      Route::post("/custom-filter/delete","CustomFilter\Controllers\CustomFilterController@destroy");

      // BLOCK USER
      Route::post('block', 'BlockUser\Controllers\BlockUserController@block');
      Route::post('unblock', 'BlockUser\Controllers\BlockUserController@unblock');
      Route::get('user-blocked', 'BlockUser\Controllers\BlockUserController@getBlockedUser');
      Route::get('user-blocked', 'BlockUser\Controllers\BlockUserController@getBlockedUser');
      Route::post('is-blocked', 'BlockUser\Controllers\BlockUserController@isUserBlocked');

      // DASHBOARD
      Route::get('dashboard/get-overall-all-apps', 'Dashboard\Controllers\DashboardController@getOverallAllApps');
      Route::get('dashboard/get-overall', 'Dashboard\Controllers\DashboardController@getOverall');
      Route::get('dashboard/get-overall-req', 'Dashboard\COntrollers\DashboardController@getOverallRequest');
      Route::get('dashboard/get-data', 'Dashboard\Controllers\DashboardController@getData');
      Route::get('dashboard/get-data-chat', 'Dashboard\Controllers\DashboardController@getDataChat');
      Route::get('dashboard/get-test', 'Dashboard\Controllers\DashboardController@getTest');
      Route::get('dashboard/first-response', 'Dashboard\Controllers\DashboardController@_firstResponse');

      // DASHBOARD OVERALL
      Route::get('dashboard/overall-messages', 'Dashboard\Controllers\DashboardController@getOverallNumberConversation');
      Route::get('dashboard/overall-users', 'Dashboard\Controllers\DashboardController@getOverallUser');
      Route::get('dashboard/overall-transactions', 'Dashboard\Controllers\DashboardController@_getTotalTransaction');
      Route::get('dashboard/mau', 'Dashboard\Controllers\DashboardController@getMau'); //monthly active user per month

      // DASHBOARD CONVERSATION AND USER
      Route::get('dashboard/conversation-user', 'Dashboard\Controllers\DashboardController@getConversationAndUser');

      // DASHBOARD TRAFFIC BY CHANNEL
      Route::get('dashboard/traffic', 'Dashboard\Controllers\DashboardController@getTrafficChannel');

      // DASHBOARD GENERAL
      Route::get('dashboard/general-total-message', 'Dashboard\Controllers\DashboardController@generalTotalMessages');
      Route::get('dashboard/general-new-users', 'Dashboard\Controllers\DashboardController@generalNewUsers');
      Route::get('dashboard/general-transactions', 'Dashboard\Controllers\DashboardController@generalTransactions');
      Route::get('dashboard/general-amount', 'Dashboard\Controllers\DashboardController@generalAmount');
      Route::get('dashboard/general-total-live-conversations', 'Dashboard\Controllers\DashboardController@totalLiveConversations');
      Route::get('dashboard/general-total-ongoing-conversations', 'Dashboard\Controllers\DashbaordController@totalOngoingConversations');
      Route::get('dashboard/general-total-resolved-conversations', 'Dashboard\Controllers\DashbaordController@totalResolvedConversations');
      Route::get('dashboard/general-avg-agent-first-response-time', 'Dashboard\Controllers\DashboardController@avgAgentFirstResponseTime');

      // DASHBOARD GENERAL CHART
      Route::get('dashboard/general-chart-consumer-base', 'Dashboard\Controllers\DashboardController@generalChartConsumerBase');
      Route::get('dashboard/general-chart-new-user',' Dashboard\Controllers\DashboardController@generalChartNewUsers');
      Route::get('dashboard/general-recent-user', 'Dashboard\Controllers\DashboardController@generalRecentUser');
      Route::get('dashboard/general-agent-availability', 'Dashboard\Controllers\DashboardController@generalAgentAvailability');
      Route::get('dashboard/general-top-stories', 'Dashboard\Controllers\DashboardController@generalTopStories');
      Route::get('dashboard/general-tags', 'Dashboard\Controllers\DashboardController@generalTags');
      Route::get('dashboard/overall-live-conversations', 'Dashboard\Controllers\DashboardController@getOverallLiveConversations'); //un used
      Route::get('dashboard/top-stories', 'Dashboard\Controllers\DashboardController@generalTopStories'); //un used
      Route::get('dashboard/top-stories2', 'Dashboard\Controllers\DashboardController@getTopStory2');

      // DASHBOARD AGENT
      Route::get('dashboard/total-message-agent', 'Dashboard\Controllers\DashboardController@totalMessageAgent');
      Route::get('dashboard/new-conversation-per-agent', 'Dashboard\Controllers\DashboardController@newConversationPerAgent');
      Route::get('dashboard/total-ongoing-conversation-per-agent', 'Dashboard\Controllers\DashboardController@onGoingConversationPerAgent');
      Route::get('dashboard/total-resolved-conversation-per-agent', 'Dashboard\Controllers\DashboardController@resolvedConversationPerAgent');
      Route::get('dashboard/daily-messages-per-agent', 'Dashboard\Controllers\DashboardController@dailyMessagesPerAgent');
      Route::get('dashboard/daily-new-conversation-per-agent', 'Dashboard\Controllers\DashboardController@dailyNewConversationPerAgent');
      Route::get('dashboard/average-conversation-duration-per-agent', 'Dashboard\Controllers\DashboardController@averageConversationDurationPerAgent');
      Route::get('dashboard/average-first-response-time-per-agent', 'Dashboard\Controllers\DashboardController@averageFirstResponseTimePerAgent');
      Route::get('dashboard/getAgentList', 'Dashboard\Controllers\DashboardController@getAgentList');
      Route::get('dashboard/served-room-conversation-per-group-agent', 'Dashboard\Controllers\DashboardController@servedRoomConvPerGroupAgent');

      // DASHBOARD CHAT
      Route::get('dashboard/average-conversation-duration', 'Dashboard\Controllers\DashboardController@getAverageConversationDuration');
      Route::get('dashboard/average-message-per-conversation', 'Dashboard\Controllers\DashboardController@getAverageMessagesPerConversation');
      Route::get('dashboard/total-hsm', 'Dashboard\Controllers\DashboardController@getTotalHsm');
      Route::get('dashboard/total-hsm-pertemplate', 'Dashboard\Controllers\DashboardController@getTotalHsmPertemplate');
      // Route::get('dashboard/testing', 'Dashboard\Controllers\DashboardController@testing');
      Route::get('dashboard/daily-ongoing-room-conversation', 'Dashboard\Controllers\DashboardController@getDailyOnGoingConversation');
      Route::get('dashboard/daily-resolved-room-conversation', 'Dashboard\Controllers\DashboardController@getDailyResolveConversation');
      Route::get('dashboard/daily-hsm', 'Dashboard\Controllers\DashboardController@getDailyHsm');
      Route::get('dashboard/daily-message', 'Dashboard\Controllers\DashboardController@getDailyMessage');

      // SESSION
      Route::post("/session","Session\Controllers\SessionController@create");

      // EXPORT EXCEL REPORTING
      Route::get("/excel/user","Reporting\Controllers\ReportingController@exportUsers");
      Route::get('/excel/message', 'Reporting\Controllers\ReportingController@exportMessages');
      Route::get('/excel/room', 'Reporting\Controllers\ReportingController@exportRoom');
      Route::get('/excel/hsm', 'Reporting\Controllers\ReportingController@exportHsm');
      Route::get('/excel/tag', 'Reporting\Controllers\ReportingController@exportTag');
      Route::get('/excel/note', 'Reporting\Controllers\ReportingController@exportNote');
      Route::get('/excel/report-time-served', 'Reporting\Controllers\ReportingController@exportTimeServed');
      Route::get("/excel/handle-room","Reporting\Controllers\ReportingController@exportHandleRoom");
      Route::get("/excel/ticket","Reporting\Controllers\ReportingController@exportTicket");
      Route::get("/excel/broadcast-mobile","Reporting\Controllers\ReportingController@exportBroadcastMessageMobile");
      Route::get("/excel/broadcast-whatsapp","Reporting\Controllers\ReportingController@exportBroadcastMessageWhatsapp");
      Route::get("/excel/role-history", "Reporting\Controllers\ReportingController@exportRoleHistoryActivity");

      // EXPORT PDF
      Route::get("/pdf/hsm", 'Reporting\Controllers\ReportingController@exportPdfHsm');
      Route::get("/excel/agent-activities","Reporting\Controllers\ReportingController@exportAgentActivities");

      // LOG HANDLE ROOM
      Route::get('/log-handle-room/report', 'LogHandleRoom\Controllers\LogHandleRoomController@report');

      // TICKET
      Route::post("/ticket","Ticket\Controllers\TicketController@create");
      Route::post("/ticket/destroy","Ticket\Controllers\TicketController@destroy");
      Route::put("/ticket/{id}","Ticket\Controllers\TicketController@update");
      Route::get("/ticket","Ticket\Controllers\TicketController@all");
      Route::get("/ticket/{room_id}","Ticket\Controllers\TicketController@show");
      Route::get("/ticket/{id}/get-information","Ticket\Controllers\TicketController@getInformation");
      Route::get("/ticket/export/pdf","Ticket\Controllers\TicketController@exportTicket");

      // ASSIGN AGENT
      Route::post("/agent/assign","Agent\Controller\AgentController@assignTo");
      Route::post("/agent/approval-assign","Agent\Controller\AgentController@approvelAssign");
      Route::post("/agent/reject-assign","Agent\Controller\AgentController@rejectAssign");
      Route::post("/agent/change-status","Agent\Controller\AgentController@changeStatus");

      // ONLINE HISTORY
      Route::post("/online-history","OnlineHistory\Controller\OnlineHistoryController@create");

      // ROLE
      Route::get('role', 'Role\Controllers\RoleController@index');
      Route::post('role/delete', 'Role\Controllers\RoleController@destroy');
      Route::get('role-paginate', 'Role\Controllers\RoleController@paginate');
      Route::get('role/{roleId}', 'Role\Controllers\RoleController@getDetailRole');
      Route::get('role/{roleId}/show', 'Role\Controllers\RoleController@show');
      Route::post('role/{roleId}/update', 'Role\Controllers\RoleController@update');
      Route::post('role', 'Role\Controllers\RoleController@store');

      // ROOM TAG
      Route::post('room-tag','RoomTag\Controller\RoomTagController@store');
      Route::get('room-tag/{id}/delete','RoomTag\Controller\RoomTagController@destroy');

      // ADDITIONAL INFORMATION
      Route::post('additional-information','AdditionalInformation\Controller\AdditionalInformationController@create');
      Route::get('additional-information/{id}/delete','AdditionalInformation\Controller\AdditionalInformationController@destroy');
      Route::put('additional-information/{id}/update','AdditionalInformation\Controller\AdditionalInformationController@update');

      // TAG MANAGEMENT
      Route::get('tag/report', 'Tag\Controller\TagController@report');
      Route::get('tag', 'Tag\Controller\TagController@index');
      Route::post('tag/with-room','Tag\Controller\TagController@tagWithRoom');
      Route::post('tag', 'Tag\Controller\TagController@create');
      Route::get('tag/{id}/delete', 'Tag\Controller\TagController@delete');

      // CHAT TEMPLATE
      Route::get('chat-template','ChatTemplate\Controllers\ChatTemplateController@index');
      Route::post('chat-template','ChatTemplate\Controllers\ChatTemplateController@create');
      Route::put('chat-template/{id}/update','ChatTemplate\Controllers\ChatTemplateController@update');
      Route::get('chat-template/{id}/delete','ChatTemplate\Controllers\ChatTemplateController@destroy');

      // NOTES
      Route::post('notes','Notes\Controller\NotesController@create');
      Route::get('notes/report','Notes\Controller\NotesController@report');
      Route::post('notes/{id}/update','Notes\Controller\NotesController@update');

      // SEGMENT
      Route::get('segment','Segment\Controller\SegmentController@index');
      Route::get('segment/{id}','Segment\Controller\SegmentController@show');
      Route::get('segment-all/{id}', 'Segment\Controller\SegmentController@showAll');
      Route::post('segment','Segment\Controller\SegmentController@store');
      Route::post('segment/update','Segment\Controller\SegmentController@update');
      Route::post('segment/delete','Segment\Controller\SegmentController@destroy');

      // USER
      Route::get('user/get-user-list', 'User\Controllers\UserController@getUserList');
      Route::get('user/get-wa-user-list', 'User\Controllers\UserController@getWaUserList');
      Route::get('user/get-wa-user-list-24', 'User\Controllers\UserController@getWaUserList24Hour');
      Route::get('user/get-all-wa-user-list-24', 'User\Controllers\UserController@getAllWaUserListLast24Hour');
      Route::post('user/assign-story-group', 'User\Controllers\UserController@assignStoryGroup');
      Route::resource('user', 'User\Controllers\UserController');
      Route::get('user', 'User\Controllers\UserController@index');
      Route::put('user/{userId}/update','User\Controllers\UserController@updateUserInformation');

      // REPORTING
      Route::get('reporting/customers', 'Reporting\Controllers\ReportingController@getDataCustomer');
      Route::get('reporting/messages', 'Reporting\Controllers\ReportingController@getDataMessages');
      Route::get('reporting/room-msg', 'Reporting\Controllers\ReportingController@getDataRoomMsg');
      Route::get('reporting/agent-performance', 'Reporting\Controllers\ReportingController@agentPerformanceOptimize');
      Route::get('reporting/report-hsm', 'Reporting\Controllers\ReportingController@getDataReportHsm');
      Route::get('reporting/get-total-hsm-status', 'Reporting\Controllers\ReportingController@getTotalHsmStatus');
      Route::get('reporting/get-role-history-activity', 'Reporting\Controllers\ReportingController@getRoleHistoryActivity');
      Route::get('reporting/get-name-user-platform-list', 'Reporting\Controllers\ReportingController@getNameUserPlatformList');

      // USER PLATFORM
      Route::get('user-platform/get-all', 'User\Controllers\UserPlatformController@getAll');
      Route::get('user-platform/get-email', 'User\Controllers\UserPlatformController@getUserPlatformByEmail');
      Route::get('user-platform/user-data', 'User\Controllers\UserPlatformController@getUserData');
      Route::post('user-platform/update-user-data', 'User\Controllers\UserPlatformController@updateUserData');
      Route::post('user-platform/update-avatar', 'User\Controllers\UserPlatformController@uploadImage');
      // Route::get('user-platform', 'User\Controllers\UserPlatformController@index');

      // TRX
      Route::resource('transaction', 'Transaction\Controllers\TransactionController');
      Route::post('transaction/get-detail', 'Transaction\Controllers\TransactionController@getTransactionDetail');

      // PPOB PRODUCT
      Route::resource('ppob-product', 'Ppob\Controllers\PpobProductController');

      // INTEGRATION
      Route::resource('integrate', 'Integration\Controllers\IntegrationController');
      Route::post('integration/delete-all-integration', 'Integration\Controllers\IntegrationController@deleteAllIntegration');

      // INTEGRATION WHATSAPP
      Route::post('add-whatsapp-account', 'Integration\Controllers\IntegrationController@addWhatsappAccount');
      Route::post('update-whatsapp-account', 'Integration\Controllers\IntegrationController@updateWhatsappAccount');
      Route::post('set-wa-webhook', 'Integration\Controllers\IntegrationController@setWaWebhook');

      // SET / DELETE TWITTER WEBHOOK
      Route::post('setWebhookTwitter', 'Integration\Controllers\IntegrationController@setWebhookTwitter');
      Route::post('deleteWebhookTwitter', 'Integration\Controllers\IntegrationController@deleteWebhookTwitter');

      // ROOM
      Route::get('room/get-room-list', 'Room\Controllers\RoomController@getRoomList');
      Route::get('room/get-room-list-paginate', 'Room\Controllers\RoomController@getRoomListPaginate');
      Route::get('room/get-room-list-new', 'Room\Controllers\RoomController@getRoomListNew');
      Route::get('room/get-room-list-new-v2', 'Room\Controllers\RoomController@getRoomListNewOptimize');
      Route::post('room/get-more-room', 'Room\Controllers\RoomController@getMoreRoom');
      Route::get('room/total-unhandle-room', 'Room\Controllers\RoomController@totalUnhandleRoom');
      Route::get('room/total-unserved-room', 'Room\Controllers\RoomController@totalUnservedRoom');
      Route::post("room/get-count-room-by-status",'Room\Controllers\RoomController@getCountRoomByStatus');

      // CHAT
      Route::post('chat/send-message', 'Chat\Controllers\ChatController@sendMessage');
      Route::post('chat/upload-file', 'Chat\Controllers\ChatController@uploadFile');
      Route::post('chat/delete-file', 'Chat\Controllers\ChatController@deleteFile');
      Route::post("chat/search-message",'Chat\Controllers\ChatController@searchMessage');
      Route::post("chat/wa-24-hour-check", 'Chat\Controllers\ChatController@wa24HourCheck');

      // MESSAGE
      Route::get('message/get-unread-message', 'Message\Controllers\MessageController@getUnreadMessage');
      Route::put('message/update-unread-message', 'Message\Controllers\MessageController@updateUnreadMessage');
      Route::post('message/get-more-message', 'Message\Controllers\MessageController@getMoreMessage');
      Route::post('message/get-more-message-search', 'Message\Controllers\MessageController@getMoreMessageInSearch');

      // APP
      Route::get('app', 'App\Controllers\AppController@edit');
      Route::post('app', 'App\Controllers\AppController@update');
      Route::get('app/get-app-list', 'Home\Controllers\HomeController@getAppList');
      Route::get('app/get-notif-list', 'Home\Controllers\HomeController@getNotificationList');

      // BROADCAST
      Route::post('broadcast/send/mobile', 'BroadcastMessage\Controllers\MobileBroadcastController@sendBroadcast');
      Route::post('broadcast/send-channel', 'BroadcastMessage\Controllers\ChannelBroadcastController@sendBroadcast');
      Route::post('broadcast/send-whatsapp', 'BroadcastMessage\Controllers\ChannelBroadcastController@sendBroadcastNonHsmWhatsapp');
      Route::post('broadcast/upload-image', 'BroadcastMessage\Controllers\MobileBroadcastController@uploadImage');
      Route::post('broadcast/upload-bc-image', 'BroadcastMessage\Controllers\ChannelBroadcastController@uploadImage');
      Route::post('broadcast/delete-image', 'BroadcastMessage\Controllers\ChannelBroadcastController@deleteImage');
      Route::post('broadcast-only/mobile', 'BroadcastMessage\Controllers\MobileBroadcastController@sendBroadcastOnly');

      // REPORTING BROADCAST
      Route::get("broadcast/report/mobile","BroadcastMessage\Controllers\ReportingBroadcastController@reportingMobile");
      Route::get("broadcast/report/whatsapp","BroadcastMessage\Controllers\ReportingBroadcastController@reportingWhatsapp");

      // BROADCAST WHATSAPP
      Route::get('broadcast/qr-code-whatsapp', 'BroadcastMessage\Controllers\BroadcastMessageController@getQrCode');
      Route::post('broadcast/whatsapp-send', 'BroadcastMessage\Controllers\BroadcastMessageController@whatsappSend');
      Route::post('broadcast/whatsapp-image-send', 'BroadcastMessage\Controllers\BroadcastMessageController@whatsappImageSend');
      Route::get('broadcast/whatsapp-send-async', 'BroadcastMessage\Controllers\BroadcastMessageController@whatsappSendAsync');

      // APP CONTENT
      Route::resource('app-content', 'AppContent\Controllers\AppContentController');
      Route::post('app-content/upload-image', 'AppContent\Controllers\AppContentController@uploadImage');

      // AUTO RESPONSE & OFFICE HOUR
      Route::get('auto-response-data', 'AutoResponseOfficeHour\Controllers\AutoResponseController@getAutoResponseData');
      Route::post('update-auto-response', 'AutoResponseOfficeHour\Controllers\AutoResponseController@updateAutoResponseData');
      Route::post('update-office-hour', 'AutoResponseOfficeHour\Controllers\AutoResponseController@updateOfficeHour');
      Route::get('get-office-hour','AutoResponseOfficeHour\Controllers\AutoResponseController@getOfficeHour');
      Route::post('update-hours','AutoResponseOfficeHour\Controllers\AutoResponseController@updateOperationalHours');
      Route::post('auto-response-upload-file', 'AutoResponseOfficeHour\Controllers\AutoResponseController@uploadFile');

      // STATIC TOKEN
      Route::get('token', 'StaticToken\Controllers\StaticTokenController@getToken');
      Route::get('generate-token', 'StaticToken\Controllers\StaticTokenController@generateToken');

      // GENERAL VALUE
      Route::get('general-value', 'GeneralValue\Controllers\GeneralValueController@index');
      Route::post('general-value', 'GeneralValue\Controllers\GeneralValueController@store');
      Route::put('general-value/{generalValueId}', 'GeneralValue\Controllers\GeneralValueController@update');
      Route::delete('general-value/{generalValueId}', 'GeneralValue\Controllers\GeneralValueController@destroy');

      // ACCESSIBILITY
      Route::post('accessibility', 'Accessibility\Controllers\AccessibilityController@store');
      Route::get('accessibility/check-access', 'Accessibility\Controllers\AccessibilityController@checkAccess');
      Route::post('accessibility/delete-access', 'Accessibility\Controllers\AccessibilityController@deleteAccess');

      // STORY
      Route::get('bot/story', 'Bot\Controllers\BotController@getAllStory');
      Route::get('bot/story-group', 'Bot\Controllers\BotController@getAllStoryGroup');

      // WEBCHAT SETTINGS
      Route::get('webchat-style', 'Webchat\Controllers\WebchatStyleController@index');
      Route::post('webchat-style', 'Webchat\Controllers\WebchatStyleController@store');
      Route::post('webchat-style/remove-file', 'Webchat\Controllers\WebchatStyleController@deleteFile');
      Route::post('header-logo', 'Webchat\Controllers\WebchatStyleController@updateHeaderLogo');
      Route::post('avatar', 'Webchat\Controllers\WebchatStyleController@updateAvatar');
      Route::post('header-background', 'Webchat\Controllers\WebchatStyleController@updateHeaderBackground');
      Route::post("credit-image",'Webchat\Controllers\WebchatStyleController@updateCreditImage');
      Route::post("webchat/launcher-close",'Webchat\Controllers\WebchatStyleController@updateLauncherClose');
      Route::post("webchat/launcher-open",'Webchat\Controllers\WebchatStyleController@updateLauncherOpen');
      Route::post('webchat/restore', 'Webchat\Controllers\WebchatStyleController@restoreStyle');

      // TEAM MEMBER
      Route::get('member', 'TeamMember\Controllers\TeamMemberController@getTeamMember');
      Route::post('member/delete', 'TeamMember\Controllers\TeamMemberController@deleteAccess');
      Route::post('member/edit', 'TeamMember\Controllers\TeamMemberController@editAccess');
      Route::post('member', 'TeamMember\Controllers\TeamMemberController@store');
      Route::get('member/registered-member', 'TeamMember\Controllers\TeamMemberController@getRegisteredMember');
      Route::post('member/reset-default-password', 'TeamMember\Controllers\TeamMemberController@resetDefaultPassword');

      // GROUP MANAGEMENT
      Route::get('group', 'Group\Controllers\GroupController@index');
      Route::get('group/get-agent', 'Group\Controllers\GroupController@getAgent');
      Route::post('group/create-group', 'Group\Controllers\GroupController@createGroup');
      Route::post('group/update-group', 'Group\Controllers\GroupController@updateGroup');
      Route::post('group/update-group-agent', 'Group\Controllers\GroupController@updateGroupAgent');
      Route::post('group/delete-group', 'Group\Controllers\GroupController@deleteGroup');

      // MOBILE OMNI
      Route::resource('mobile-omni', 'MobileOmni\Controllers\MobileOmniController');

      // TRIPA
      Route::get('claim/personal-accidents', 'ClaimTripa\Controllers\ClaimTripaController@getClaimPersonalAccidents');
      Route::post('claim/update', 'ClaimTripa\Controllers\ClaimTripaController@updateStatusClaim');
      Route::get('claim/travels', 'ClaimTripa\Controllers\ClaimTripaController@getClaimTravels');
      Route::get('claim/vehicles', 'ClaimTripa\Controllers\ClaimTripaController@getClaimVehicles');
      Route::get('claim/wildfires', 'ClaimTripa\Controllers\ClaimTripaController@getClaimWildfires');
      Route::get('list-agent', 'ClaimTripa\Controllers\ClaimTripaController@listApprovalAgent');
      Route::post('update/agent', 'ClaimTripa\Controllers\ClaimTripaController@updateStatusAgent');

      //TRIPA HISTORY NOTIFICATION
      Route::get('history-notification', 'HistoryNotification\Controllers\HistoryNotificationController@getHistoryNotification');

      //TRIPA HISTORY PENGAJUAN POLIS
      Route::get('history/personal-accidents', 'TripaTransaksiPengajuanPolis\Controllers\TransaksiPengajuanPolisController@historyTransactionPersonalAccidents');
      Route::get('history/travels', 'TripaTransaksiPengajuanPolis\Controllers\TransaksiPengajuanPolisController@historyTransactionTravels');
      Route::get('history/vehicles', 'TripaTransaksiPengajuanPolis\Controllers\TransaksiPengajuanPolisController@historyTransactionVehicles');
      Route::get('history/wildfires', 'TripaTransaksiPengajuanPolis\Controllers\TransaksiPengajuanPolisController@historyTransactionWildfires');

      // MODULE
      Route::group(["prefix" => "module"], function() {
        Route::get('get-all', 'Module\Controllers\ModuleController@index');
        Route::get('get-all-with-map', 'Module\Controllers\ModuleController@getAllWithMap');
      });
    });
  });

  // DECODER MIDDLEWARE
  Route::group(['prefix' => '{appId}', 'middleware' => ['decoder']], function () {
      // WHATSAPP GROUP
      Route::post('create-group', 'Whatsapp\Controllers\WhatsappGroupController@createGroup');
      Route::get('list-group', 'Whatsapp\Controllers\WhatsappGroupController@listGroup');
      Route::post('update-group-subject', 'Whatsapp\Controllers\WhatsappGroupController@updateGroupSubject');
      Route::post('get-invite-group', 'Whatsapp\Controllers\WhatsappGroupController@getInviteGroup');
      Route::post('set-group-admin', 'Whatsapp\Controllers\WhatsappGroupController@setGroupAdmin');
      Route::post('remove-group-admin', 'Whatsapp\Controllers\WhatsappGroupController@removeGroupAdmin');
      Route::post('remove-group-participants', 'Whatsapp\Controllers\WhatsappGroupController@removeGroupParticipants');
      Route::post('set-group-icon', 'Whatsapp\Controllers\WhatsappGroupController@setGroupIcon');

      // MOBILE
      Route::post('register/mobile', 'Webhook\MobileWebhookController@registerAndLogin');

      // LIVECHAT
      Route::post('livechat/request', 'Livechat\Controllers\LivechatControllerNew@request');
      Route::post('livechat/resolve', 'Livechat\Controllers\LivechatControllerNew@resolve');
      Route::post('livechat/handle', 'Livechat\Controllers\LivechatControllerNew@handle');
      Route::post('livechat/assign-agent', 'Livechat\Controllers\LivechatControllerNew@assignAgent');
      Route::post('livechat/assign-group', 'Livechat\Controllers\LivechatControllerNew@assignGroup');
      Route::post('livechat/unserved-room', 'Livechat\Controllers\LivechatControllerNew@unservedRoom');
      Route::get('livechat/reporting-assignment','Livechat\Controllers\LivechatControllerNew@reportingAssignment');
      // Route::post('livechat/request', 'Livechat\Controllers\LivechatController@requestLivechat');
      // Route::post('livechat/handle', 'Livechat\Controllers\LivechatController@handleLivechat');
      // Route::post('livechat/resolve', 'Livechat\Controllers\LivechatController@stopLivechat');
      Route::post('livechat/cancel','Livechat\Controllers\LivechatControllerNew@cancel');
      Route::post('livechat/check-status','Livechat\Controllers\LivechatController@checkStatusLivechat');

      // TIME REPORT SERVED
      Route::get('report-served/get', 'TimeReportServed\Controller\TimeReportServedController@index');
      Route::get('report-served/export', 'TimeReportServed\Controller\TimeReportServedController@export');

      // FUNCTION LIST
      Route::prefix('function-list')->group(function () {
        Route::post('masking-input-text', 'FunctionList\Controllers\FunctionListController@maskingText');
      });
  });

  // TEST ENDPOINT, BACDOOR MIDDLEWARE
  Route::group(['middleware' => ['backdoor']], function () {
    Route::post('test/test', 'Test\Controllers\TestController@test');
    Route::get('test/other-test', 'Test\Controllers\TestController@otherTest');
    Route::post('test/encode', 'Test\Controllers\TestController@encode');
    Route::post('test/decode', 'Test\Controllers\TestController@decodeBackend');
    Route::post('test/hit-counter', 'Test\Controllers\TestController@hitCounter');
    Route::get('testImage', 'Test\Controllers\TestController@testImage');
    Route::post('test/backend', 'Test\Controllers\TestController@backendLogin');
    Route::post('test/sync-role', 'Test\Controllers\TestController@syncRoleId');
    Route::post('test/encrypt', 'Test\Controllers\TestController@encrypt');
    Route::post('test/decrypt', 'Test\Controllers\TestController@decrypt');
    Route::post('test/top-stories', 'Test\Controllers\TestController@getTopStory');
    Route::post('test/submenu/update', 'Test\Controllers\TestController@updateSubMenu');
    Route::get('test/interval', 'Test\Controllers\TestController@testInterval');
    Route::post('test/clear-room-double', 'Test\Controllers\TestController@clearDoubleRoom');
    Route::post('test/clear-livechat-double', 'Test\Controllers\TestController@clearDoubleLivechat');
    Route::post('test/create-room-fresh', 'Test\Controllers\TestController@createroom');
    //twitter
    Route::get('webhook/twitter', 'Test\Controllers\TestController@webhookTwitter');
    Route::post('webhook/twitter', 'Test\Controllers\TestController@webhookTwitter');
    Route::post('twitter/getMedia', 'Test\Controllers\TestController@getMedia');
    // Warning!! Copy Webchat Data from Launcher Open field to Launcher Close field
    Route::post('copy/open-to-close', 'Test\Controllers\TestController@copyLauncher');
    // Warning!! Migrate story group icon color from old to new merging platform icon
    Route::post('migrate/story-groups/icon', 'Test\Controllers\TestController@migrateStoryGroupIcon');
  });

  // DECODER MIDDLEWARE, BACKDOOR MIDDLEWARE, MOBILE ENCRYPT MIDDLEWARE
  Route::group(['prefix' => "{appId}", 'middleware' => ['decoder','backdoor','mobile.encrypt']], function () {
    Route::post('mobile/decrypt', 'Test\Controllers\TestController@mobileDecrypt');
  });
});

// SET THROTTLE 500 REQUEST / 1 MINUTE
Route::group(['middleware' => ['throttle:500,1', 'bindings']], function () {
  // ROUTE GROUP FOR ALL WEBHOOK
  Route::group(["prefix" => "{appId}", "middleware" => ["decoder","is.blocked"]], function() {

    // GET MORE MESSAGE FOR MOBILE
    Route::post('message/get-more-message-mobile', 'Message\Controllers\MessageController@getMoreMessageMobile');

    // SEND MESSAGE AUTO FOLLOW UP
    Route::post('chat/send-message-auto-followup', 'Chat\Controllers\ChatController@sendMessage');

    // RATING AGENT
    Route::post("rating","AgentRating\Controllers\AgentRatingController@store");

    // SEND HSM EXTERNAL
    Route::post('send-hsm', 'BroadcastMessage\Controllers\HsmBroadcastController@sendBroadcastHsmExternal')->middleware('throttle:500,1');

    // LINE
    Route::post('webhook/line', 'Line\Controllers\LineWebhookController@webhook')->middleware('custom.cors');
    Route::post('webhooktest/line', 'Line\Controllers\LineWebhookController@webhookTest');

    // WHATSAPP
    Route::post('webhook/whatsappinfobip', 'Whatsapp\Controllers\WhatsappInfobipWebhookController@webhook');
    Route::post('webhook/whatsappapiwha', 'Whatsapp\Controllers\WhatsappApiWhaWebhookController@webhook');
    Route::post('webhook/whatsapptesting', 'Whatsapp\Controllers\WhatsappDamcorpWebhookController@webhookTest');
    Route::post('webhook/whatsapp-damcorp/{id}', 'Whatsapp\Controllers\WhatsappDamcorpWebhookController@webhook')->middleware('custom.cors');
    Route::post('webhook/whatsapp-wappin/{id}', 'Whatsapp\Controllers\WhatsappWappinWebhookController@webhook');
    Route::post('webhook/whatsapp-official/{id}', 'Whatsapp\Controllers\WhatsappOfficialWebhookController@webhook')->middleware('custom.cors');

    // TELEGRAM
    Route::post('webhook/telegram', 'Telegram\Controllers\TelegramWebhookController@webhook')->middleware('custom.cors');

    // MOBILE
    Route::post('webhook/mobile', 'Mobile\Controllers\MobileWebhookController@webhook')->middleware('mobile.encrypt');

    // WEBCHAT
    Route::post('webhook/webchat', 'Webchat\Controllers\WebchatWebhookController@webhook')->middleware('custom.cors');
    Route::post('register/webchat', 'Webchat\Controllers\WebchatWebhookController@registerUser');
    Route::get('style/webchat', 'Webchat\Controllers\WebchatWebhookController@getStyle');
    Route::post('message/get-messages', 'Webchat\Controllers\WebchatWebhookController@getMessages');
    Route::post('register/check', 'Webchat\Controllers\WebchatWebhookController@checkRegisteredUser');
    Route::get('queue/check', 'Webchat\Controllers\WebchatWebhookController@checkQueue');
    Route::get('webchat/check-location-config', 'Webchat\Controllers\WebchatWebhookController@checkLocationConfig');
    Route::post("livechat/check","Webchat\Controllers\WebchatWebhookController@checkLivechat");

    // Twitter
    Route::get('webhook/twitter', 'Twitter\Controllers\TwitterWebhookController@webhookCRC');
    Route::post('webhook/twitter', 'Twitter\Controllers\TwitterWebhookController@webhook')->middleware('custom.cors');

    // GENERAL VALUE
    Route::post('general-value/get-by-key', 'GeneralValue\Controllers\GeneralValueController@getByValue');

    // FACEBOOK
    Route::post('webhook/facebook', 'Facebook\Controllers\FacebookWebhookController@webhook');
    Route::get('webhook/facebook', 'Facebook\Controllers\FacebookWebhookController@webhookVerify');
    Route::get('integration/facebook/get-longlived-token', 'Integration\Controllers\IntegrationController@getLongLivedUserToken');

    // BOT
    Route::get('bot/get-user-bot/{userId}', 'Bot\Controllers\BotController@getUserBot');
  });
});
