<?php

return [
  'middleware' => env('ROLE_MIDDLEWARE_ENABLED', false),
];
