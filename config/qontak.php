<?php

return [
  "code" => env('QONTAK_CODE', ""),
  "application_id" => env("QONTAK_APPLICATION_ID",""),
  "secret" => env("QONTAK_SECRET",""),
  "callback_url" => env("QONTAK_CALLBACK_URL","")
];
