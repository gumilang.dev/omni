<?php

return [
  "host" => env("RABBITMQ_HOST"),
  "user" => env("RABBITMQ_USER"),
  "password" => env("RABBITMQ_PASSWORD"),
  "vhost" => env("RABBITMQ_VHOST"),
  "port" => env("RABBITMQ_PORT")
];
