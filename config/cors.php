<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laravel CORS Options
    |--------------------------------------------------------------------------
    |
    | The allowed_methods and allowed_headers options are case-insensitive.
    |
    | You don't need to provide both allowed_origins and allowed_origins_patterns.
    | If one of the strings passed matches, it is considered a valid origin.
    |
    | If array('*') is provided to allowed_methods, allowed_origins or allowed_headers
    | all methods / origins / headers are allowed.
    |
    */

    /*
     * You can enable CORS for 1 or multiple paths.
     * Example: ['api/*']
     */
    'paths' => ['api/*'],

    /*
    * Matches the request method. `[*]` allows all methods.
    */
    'allowed_methods' => ['POST','GET','PUT','DELETE','OPTIONS'],

    /*
     * Matches the request origin. `[*]` allows all origins. Wildcards can be used, eg `*.mydomain.com`
     */
    'allowed_origins' => [
      /* internal */
      'https://lenna.ai',
      'https://app.lenna.ai',
      'https://dev.lenna.ai',
      'https://staging.lenna.ai',
      'https://covid19.lenna.ai',
      'http://35.247.181.235',
      'http://34.101.156.14',
      '*.lenna.ai',
      /* external */
      'https://api.line.me',
      'https://graph.facebook.com',
      'https://api.telegram.org',
      'http://panel.apiwha.com',
      'https://fcm.googleapis.com',
      '*.api.infobip.com',
      'https://waba.damcorp.id',
      '*.jshell.net',
      'https://jsfiddle.net',
      'https://fiddle.jshell.net',
      'http://jsfiddle.net',
      'https://codepen.io',
      'https://cdpn.io',
			/* local */
      '*.lenna.test',
      'localhost',
      'http://localhost',
      'http://localhost:8081',
      'http://localhost:8082',
      /* customer */
      'https://logojeans.com',
      'https://9to9.co.id',
      '*.gbk.id',
      '*.qontak.net',
      'https://api-klikbelanjalogo.qontak.net',
      'https://api-dandankustore.qontak.net',
      'https://api-lovegbk.qontak.net',
      'https://api-ikeasentulcity.qontak.net',
      'https://api-ikeaalamsutera.qontak.net',
      'https://api-campinaicecream.qontak.net',
      'https://api-campinacs.qontak.net',
      'https://18.139.236.223:9103',
      'http://everbest-uat.ascentisecommerce.com',
      'http://tracceevb.ascentisecommerce.com',
      'https://id.everbestshoes.com',
      'https://www.evbshoes.com',
      'https://www.tracceshoes.com',
      'http://dev-reservation.gbk.id',
      'https://covid19.go.id',
      'http://10.10.22.39',
      'http://10.10.22.40',
      'https://www.balifiber.id',
      'http://192.168.189.102:19001',
      'http://147.139.135.183',
      'https://cobacoba.divtbs.id',
      'https://bnitbs.id',
      'http://103.84.192.163',
      'https://beta.crowdo.co.id',
      'https://crowdo.co.id',
      'https://beta.crowdo.co.id'.
      'https://p2p-beta.crowdo.co.id',
      'https://investment-beta.crowdo.co.id',
      'https://crowdo.co.id',
      'https://p2p.crowdo.co.id',
      'https://investment.crowdo.co.id',
      'https://beta.crowdo.com',
      'https://p2p-beta.crowdo.com',
      'https://investment-beta.crowdo.com',
      'https://crowdo.com',
      'https://p2p.crowdo.com',
      'https://investment.crowdo.com',
      'https://p2p-beta.crowdo.co.id',
      'https://ar.digi46.id',
      'https://bnicorpu.co.id',
      'https://www.bnicorpu.co.id',
      'https://www.bnicorpu.co.id/smarter',
      'https://www.bnicorpu.co.id/beta_smarter',
      'https://kopegtel-metropolitan.co.id'
    ],

    /*
     * Patterns that can be used with `preg_match` to match the origin.
     */
    'allowed_origins_patterns' => ['/localhost:\d/'],

    /*
     * Sets the Access-Control-Allow-Headers response header. `[*]` allows all headers.
     */
    'allowed_headers' => [
      'Accept',
      'Access-Control-Allow-Origin',
			'Authorization',
			'Content-Type',
      'Origin',
			'X-CSRF-TOKEN',
      'X-Requested-With',
      'X-Socket-Id',
      'X-XSRF-TOKEN',
      'X-Backdoor-Token',
      'X-Mobile-Omni',
      'cache-control',
			'token',
			'userId',
      'X-LENNA-WEBCHAT'
    ],

    /*
     * Sets the Access-Control-Expose-Headers response header with these headers.
     */
    'exposed_headers' => [],

    /*
     * Sets the Access-Control-Max-Age response header when > 0.
     */
    'max_age' => 60,

    /*
     * Sets the Access-Control-Allow-Credentials header.
     */
    'supports_credentials' => true,
];
