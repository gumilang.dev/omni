<?php

namespace Tests\Unit\BroadcastMessage;

use Tests\TestCase;
use Carbon\Carbon;
use App\Exceptions\BadRequestParameterException;
use App\Exceptions\RecordNotExpectedException;
use App\Modules\BroadcastMessage\Services\BroadcastMessageServiceImpl;
use App\Modules\BroadcastMessage\Repositories\BroadcastMessageRepository;

use Mockery;

class BroadcastMessageServiceImplTest extends TestCase
{

  private $broadcastMessageService;
  private $mockBroadcastMessageRepository;
  private $appId = 6;

  public function setUp(): void
  {
    parent::setUp();
    $this->mockBroadcastMessageRepository = Mockery::mock(BroadcastMessageRepository::class);
    $this->broadcastMessageService = new BroadcastMessageServiceImpl($this->mockBroadcastMessageRepository);
  }


  public function testShouldGetReportingBroadcastMessageAndReturnBadRequestParameterException() {
    $response = $this->broadcastMessageService->reporting(null,1,2,3,3,3);

    $this->assertInstanceOf(BadRequestParameterException::class, $response);
  }

  public function testShoulddValidateIfTheGivenStatusDataIsNotAvailable() {
    $response = $this->broadcastMessageService->recordLog("test", ["receiver_id" => 109]);

    $this->assertInstanceOf(RecordNotExpectedException::class, $response);
  }

  public function testShouldValidateIfTheGivenDataIsNull() {
    $response = $this->broadcastMessageService->recordLog(null, null);

    $this->assertInstanceOf(BadRequestParameterException::class, $response);
  }

  public function testShoulSuccessCreateLogBroadcast() {
    $given_data = ["receiver_id" => 112, "channel_id" => 7, "app_id" => 6];
    $given_status = "success";

    $this->mockBroadcastMessageRepository
          ->shouldReceive("insert")
          ->with(array_merge(["status" => $given_status], $given_data))
          ->andReturn(true);

    $response = $this->broadcastMessageService->recordLog("success",$given_data);

    $this->assertTrue($response);
  }

  public function testCreateBroadcastMessageWhatsappFormat() {
      $response = $this->broadcastMessageService->createWhatsappBroadcastFormat(1,[] ,"failed", $this->appId, 1,1,1);
      $this->assertArrayHasKey("app_id",$response);
      $this->assertArrayHasKey("integration_id",$response);
      $this->assertArrayHasKey("data",$response);
      $this->assertArrayHasKey("notification",$response);
      $this->assertArrayHasKey("receiver_id",$response);
      $this->assertArrayHasKey("status",$response);
  }

  public function testShouldValidateWhenRecordLogWhatsappIfGivenDataIsNotValida() {
    $response = $this->broadcastMessageService->recordLogWhatsapp(null, null ,null,null,null,null,null);

    $this->assertInstanceOf(BadRequestParameterException::class,$response);
  }

  public function testShouldSuccessCreateRecordLogWhatsappAndReturnRecordNotExpectedExceptionCause() {
    $user = new \StdClass;
    $user->status = true;
    $user->id = 1;

    $response = $this->broadcastMessageService->recordLogWhatsapp("single", $user,[],[],$this->appId, 19,20);
    $this->assertInstanceOf(RecordNotExpectedException::class,$response);
  }

  public function testShouldSuccessCrateRecordLogWhatsapp() {
    $user = array();
    $user["id"] = 1;

    $this->mockBroadcastMessageRepository
          ->shouldReceive("insert")
          ->once();

    $response = $this->broadcastMessageService->recordLogWhatsapp("single", $user,[(object)["status" => "sent"]],[],$this->appId, 19,200);

    $this->assertTrue($response);
  }


}
