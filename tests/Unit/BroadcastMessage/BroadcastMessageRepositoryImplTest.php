<?php

namespace Tests\Unit\BroadcastMessage;

use Tests\TestCase;
use Carbon\Carbon;
use App\Modules\BroadcastMessage\Repositories\Impl\BroadcastMessageRepositoryImpl;

class BroadcastMessageRepositoryImplTest extends TestCase
{

  private $broadcastMessageRepository;
  private $appId = 6;

  public function setUp(): void
  {
    parent::setUp();
    $this->broadcastMessageRepository = new BroadcastMessageRepositoryImpl();
  }

  public function testShouldGetBroadcastMessageDataWithCorrectField()
  {
    $this->broadcastMessageRepository->insert(["channel_id" => 7, "data" => '{"title":"title","message":null}', "app_id" => $this->appId,"receiver_id" => 12482,"status" => "testasdasd"]);

    $response = $this->broadcastMessageRepository->getBy($this->appId);
    if(count($response->items()) > 0) {
      $first_response = $response->items()[0];
      $this->assertObjectHasAttribute("id",$first_response);
      $this->assertObjectHasAttribute("name",$first_response);
      $this->assertObjectHasAttribute("status",$first_response);
      $this->assertObjectHasAttribute("notification",$first_response);
      $this->assertObjectHasAttribute("type",$first_response);
      $this->assertObjectHasAttribute("data",$first_response);
      $this->assertObjectHasAttribute("executed_by",$first_response);
    }else {
      $this->assertEquals(count($response->items()), 0);
    }
  }

  public function testShouldGetAllBroadcastMessageWithStatusSend() {
    $this->broadcastMessageRepository->insert(["channel_id" => 7, "data" => '{"title":"title","message":null}', "app_id" => $this->appId,"receiver_id" => 12482,"status" => "test"]);

    $response = $this->broadcastMessageRepository->getBy($this->appId, function ($query) {
      $query->where("ob.status", "test");
    });


    if(count($response->items()) > 0) {
      $first_response = $response->items()[0];

      $this->assertObjectHasAttribute("id",$first_response);
      $this->assertObjectHasAttribute("name",$first_response);
      $this->assertObjectHasAttribute("status",$first_response);
      $this->assertObjectHasAttribute("notification",$first_response);
      $this->assertObjectHasAttribute("type",$first_response);
      $this->assertObjectHasAttribute("data",$first_response);
      $this->assertObjectHasAttribute("executed_by",$first_response);
      $this->assertEquals($first_response->status, "test");
    }else {
      $this->assertCount(0,$response->items());
    }


  }

  public function testShouldGetAllBroadcastMessageIfFilteredByDate() {
    $this->broadcastMessageRepository->insert(["channel_id" => 7, "data" => '{"title":"title","message":null}', "app_id" => $this->appId,"receiver_id" => 12482,"status" => "sent"]);

    $response = $this->broadcastMessageRepository->getBy($this->appId, function ($query) {
      $query->whereDate("ob.created_at",">=",Carbon::now()->toDateTimeString());
    });

    if(count($response->items()) > 0) {
      $first_response = $response->items()[0];
      $this->assertObjectHasAttribute("id",$first_response);
      $this->assertObjectHasAttribute("name",$first_response);
      $this->assertObjectHasAttribute("status",$first_response);
      $this->assertObjectHasAttribute("notification",$first_response);
      $this->assertObjectHasAttribute("type",$first_response);
      $this->assertObjectHasAttribute("data",$first_response);
      $this->assertObjectHasAttribute("executed_by",$first_response);

    }else{
      $this->assertEquals(count($response->items()), 0);
    }
  }

  public function testShouldGetAllBroadcastMessageIfFilterByUsername() {
    $response = $this->broadcastMessageRepository->getBy($this->appId, function ($query) {
      $query->where("au.name", "ILIKE", "%ham%");
    });

    if(count($response->items()) > 0) {
      $first_response = $response->items()[0];
      $this->assertObjectHasAttribute("id",$first_response);
      $this->assertObjectHasAttribute("name",$first_response);
      $this->assertObjectHasAttribute("status",$first_response);
      $this->assertObjectHasAttribute("notification",$first_response);
      $this->assertObjectHasAttribute("type",$first_response);
      $this->assertObjectHasAttribute("data",$first_response);
      $this->assertObjectHasAttribute("executed_by",$first_response);

      $this->assertStringContainsString("Zul",$first_response->name);
    }else {
      $this->assertCount(0,$response->items());
    }
  }

}
