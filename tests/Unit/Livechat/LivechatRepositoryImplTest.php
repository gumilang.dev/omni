<?php

namespace Tests\Unit\Livechat;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Modules\Livechat\Request\CreateLivechatRequest;
use App\Modules\Livechat\Repositories\Impl\LivechatRepositoryImpl;

class LivechatRepositoryImplTest extends TestCase
{
    private $livechatRepository;
    private $roomId  = 3000;
    private $livechatId;
    private $handleBy  = 1000;
    private $status = "request";
    private $assignee_id = 10;
    private $appId = 6;

    public function setUp(): void {
      parent::setUp();
      $this->livechatRepository = new LivechatRepositoryImpl();
    }

    public function testCreateAndShouldExistInDB()
    {

      $createLivechatRequest = new CreateLivechatRequest(
        $this->roomId,
        $this->handleBy,
        $this->status
      );
      $this->livechatRepository->insert($createLivechatRequest->toArray());

      $response = $this->livechatRepository->getByRoomId($this->roomId);

      $this->assertEquals($this->handleBy, $response->handle_by);
      $this->assertEquals($this->roomId, $response->room_id);
      $this->assertNull($response->start_at);
      $this->assertNull($response->end_at);
      $this->assertNull($response->request_at);
      $this->assertEquals("request",$response->status);
    }
    // TODO: make dynamic livechat id parameter
    public function testShouldReturnLivechatById() {
      $response = $this->livechatRepository->getByLivechatId(19);
      $this->assertIsObject($response);
    }

    // TODO: make dynamic user id parameter
    public function testShouldReturnLivechatByUserId() {
      $response = $this->livechatRepository->getByAgentId($this->handleBy);
      $this->assertNotNull($response);
      $this->assertIsObject($response);
    }

    public function testShouldUpdateLivechat() {
      $response = $this->livechatRepository->update(
        [
          "handle_by" => $this->handleBy
        ],
        [
          "status" => "resolved"
        ]
      );
      $this->assertEquals(1,$response); // true
    }

    public function testShouldDeleteLivechat() {
      $response = $this->livechatRepository->delete(["room_id" => $this->roomId]);
      $checkInDb = $this->livechatRepository->getByAgentId($this->handleBy);
      $this->assertCount(0,$checkInDb);
      $this->assertEquals(1,$response); // true
    }

    public function testShouldCreateHistoryAssignment() {
      $response = $this->livechatRepository->createHistoryAssignment("ahmad haidar","ahmadhdr.22@gmail.com","haidar","admin","haidar albaqir",6);
      $this->assertTrue($response);
    }

  public function testShouldSuccessGetHistoryAssignment() {
      $response = $this->livechatRepository->getHistoryAssignment($this->appId,true);

      if(count($response) < 1) {
        $this->assertNotNull($response);
      }else {
        $first_response = $response[0];
        $this->assertObjectHasAttribute("customer_name",$first_response);
        $this->assertObjectHasAttribute("customer_email",$first_response);
        $this->assertObjectHasAttribute("assignor",$first_response);
        $this->assertObjectHasAttribute("assignee",$first_response);
        $this->assertObjectHasAttribute("created_at",$first_response);
        $this->assertObjectHasAttribute("assignor_role",$first_response);
      }

    }

}
