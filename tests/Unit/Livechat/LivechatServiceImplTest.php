<?php namespace Tests\Unit\Livechat;

use Mockery;
use Tests\TestCase;
use Carbon\Carbon;
use App\Modules\Livechat\Services\LivechatServiceImpl;
use App\Exceptions\RecordNotFoundException;
use App\Exceptions\RecordNotExpectedException;
use App\Exceptions\RecordDuplicateException;
use App\Exceptions\BadRequestParameterException;
use App\Modules\Livechat\Repositories\LivechatRepository;
use App\Modules\Room\Repositories\RoomRepository;
use App\Modules\Livechat\Response\LivechatResponse;
use App\Modules\Room\Response\RoomResponse;
use App\Modules\Livechat\Request\CreateLivechatRequest;
use Illuminate\Support\Facades\Event;
use App\Events\LivechatRequest;
use App\Events\LivechatHandle;
use App\Events\LivechatResolved;
use App\Events\LivechatAssigned;


class LivechatServiceImplTest extends TestCase{

  private $userId = 100000;
  private $livechatService;
  private $mockLivechatRepository;
  private $mockRoomRepository;
  private $livechatResponse;
  private $roomResponse;
  private $roomId = 100;
  private $appId = 6;
  private $agentId = 10;
  private $groupId = 1;
  private $assigneeId = 10;
  private $assignorId = 100;

  public function setUp(): void {
    parent::setUp();
    $this->mockLivechatRepository = Mockery::mock(LivechatRepository::class);
    $this->mockRoomRepository = Mockery::mock(RoomRepository::class);
    $this->livechatService = new LivechatServiceImpl($this->mockLivechatRepository,$this->mockRoomRepository);

    $this->livechatResponse = new LivechatResponse(1,$this->roomId,null,null,"request",null,null,null,null,null);
    $this->roomResponse = new RoomResponse();
    $this->roomResponse->id = $this->roomId;
    $this->roomResponse->app_id = $this->appId;
  }

  public function testShouldRequestLivechatAndThrowRecordNotFoundException() {
    $this->mockRoomRepository
          ->shouldReceive("getByUserId")
          ->with($this->userId)
          ->andReturn(null)
          ->once();
    $response = $this->livechatService->request($this->userId);

    $this->assertInstanceOf(RecordNotFoundException::class,$response);
  }

  public function testRequestLivechatAndThrowRecordDuplicateException() {
    $this->mockRoomRepository
          ->shouldReceive("getByUserId")
          ->with($this->userId)
          ->andReturn($this->roomResponse)
          ->once();


    $this->mockLivechatRepository
         ->shouldReceive("getByRoomId")
         ->with(100)
         ->andReturn($this->livechatResponse)
         ->once();

    $response = $this->livechatService->request($this->userId);

    $this->assertInstanceOf(RecordDuplicateException::class,$response);
  }

  public function testRequestLivechatAndsSuccessExistInDB() {
    Event::fake();

    $this->mockRoomRepository
          ->shouldReceive("getByUserId")
          ->with($this->userId)
          ->andReturn($this->roomResponse)
          ->once();


    $this->mockLivechatRepository
         ->shouldReceive("getByRoomId")
         ->andReturn(null)
         ->with($this->roomId)
         ->once();

    $request = new CreateLivechatRequest($this->roomId, null, "request",  Carbon::now()->toDateTimeString());
    $createLivechatRequest = $request->toArray();

    $this->mockLivechatRepository
         ->shouldReceive("insert")
         ->with($createLivechatRequest)
         ->andReturn(true)
         ->once();

    $this->mockRoomRepository
         ->shouldReceive("getAllUnservedRoom")
         ->with($this->appId)
         ->once();

    $this->roomResponse->livechat  = $this->livechatResponse;


    $this->mockRoomRepository
          ->shouldReceive("getByUserId")
          ->andReturn($this->roomResponse)
          ->with($this->userId)
          ->once();


    $response = $this->livechatService->request($this->userId);

    Event::assertDispatched(LivechatRequest::class);

    $this->assertNotNull($response);
    $this->assertEquals($this->roomResponse->id,$response->id);
    $this->assertEquals($this->roomResponse->livechat->room_id,$response->livechat->room_id);
    $this->assertEquals($this->roomResponse->livechat->handle_by,$response->livechat->handle_by);
    $this->assertEquals($this->roomResponse->livechat->status,$response->livechat->status);
    $this->assertEquals($this->roomResponse->livechat->description,$response->livechat->description);
    $this->assertEquals($this->roomResponse->livechat->start_at,$response->livechat->start_at);
    $this->assertEquals($this->roomResponse->livechat->request_at,$response->livechat->request_at);
    $this->assertEquals($this->roomResponse->livechat->end_at,$response->livechat->end_at);
    $this->assertEquals($this->roomResponse->livechat->updated_by,$response->livechat->updated_by);
  }

  public function testResolveLivechatAndReturnRoomRecordNotfoundException() {
    $this->mockRoomRepository->shouldReceive("getById")->with($this->roomId)->andReturn(null);
    $response = $this->livechatService->resolve($this->roomId,$this->agentId);

    $this->assertInstanceOf(RecordNotFoundException::class,$response);
  }


  public function testResolveLivechatAndReturnLivechatRecordNotfoundException() {
    $this->mockRoomRepository->shouldReceive("getById")->with($this->roomId)->andReturn($this->roomResponse);
    $response = $this->livechatService->resolve($this->roomId,$this->agentId);

    $this->assertInstanceOf(RecordNotFoundException::class,$response);
  }

  public function testResolveLivechatAndReturnRecordNotExpectedExceptionCauseCurrentLivechatStatusIsRequestExpectedLive() {
    $this->roomResponse->livechat = new LivechatResponse(1,$this->roomId,12,null,"request",null,null,null,null,null);

    $this->mockRoomRepository->shouldReceive("getById")->with($this->roomId)->andReturn($this->roomResponse);
    $response = $this->livechatService->resolve($this->roomId,$this->agentId);

    $this->assertInstanceOf(RecordNotExpectedException::class,$response);
  }

  public function testResolveLivechatAndReturnRecordNotExpectedExceptionCauseCurrentLivechatStatusIsResolvedExpectedLive() {
    $this->roomResponse->livechat = new LivechatResponse(1,$this->roomId,12,null,"resolved",null,null,null,null,null);

    $this->mockRoomRepository->shouldReceive("getById")->with($this->roomId)->andReturn($this->roomResponse);
    $response = $this->livechatService->resolve($this->roomId,$this->agentId);

    $this->assertInstanceOf(RecordNotExpectedException::class,$response);
  }

  public function testResolveLivechatAndSucessCResolveLivechat() {
    Event::fake();
    $this->roomResponse->livechat = new LivechatResponse(1,$this->roomId,12,null,"live",null,null,null,null,null);

    $this->mockRoomRepository->shouldReceive("getById")->with($this->roomId)->andReturn($this->roomResponse)->twice();

    $this->mockLivechatRepository->shouldReceive("update")->with([
      "room_id" => $this->roomId
      ],
      [
      "updated_by" => $this->agentId,
      "status" => "resolved",
      "end_at" =>  Carbon::now()->toDateTimeString()
      ]);

    $response = $this->livechatService->resolve($this->roomId,$this->agentId);
    Event::assertDispatched(LivechatResolved::class);
    $this->assertTrue($response);
  }

  public function testShouldHandleLivechatAndThrowBadRequestParameterExceptionCauseAppIdOrUserIdIsNull(){
    $response = $this->livechatService->handle($this->agentId,null);

    $this->assertInstanceOf(BadRequestParameterException::class, $response);
  }

  public function testShouldHandleLivechatAndThrowRecordNotFoundExceptionCauseRoomIsNotAvailible() {
    $this->mockRoomRepository
        ->shouldReceive("getByUnservedRoom")
        ->with($this->appId)
        ->andReturn(null)
        ->once();

    $response = $this->livechatService->handle($this->agentId,$this->appId);

    $this->assertInstanceOf(RecordNotFoundException::class, $response);
  }

  public function testShouldHandleLivechatAndSuccess() {
    Event::fake();
    $this->mockRoomRepository
        ->shouldReceive("getByUnservedRoom")
        ->with($this->appId)
        ->andReturn($this->roomResponse)
        ->once();

    $this->mockLivechatRepository
         ->shouldReceive("update")
         ->with([
              "room_id" => $this->roomId
            ],
            [
              "updated_by" => $this->agentId,
              "handle_by" => $this->agentId,
              "status" => "live",
              "start_at" => Carbon::now()->toDateTimeString()
          ])
         ->andReturn();

    $this->mockRoomRepository
        ->shouldReceive("getById")
        ->with($this->roomId)
        ->andReturn($this->roomResponse)
        ->once();

    $this->mockRoomRepository
       ->shouldReceive("getAllUnservedRoom")
       ->atMost()
       ->times(1);

    $response = $this->livechatService->handle($this->agentId,$this->appId);

    Event::assertDispatched(LivechatHandle::class);

    $this->assertTrue($response);
  }

  public function testShouldAssignToAgentLivechatAndReturnRoomNotFoundException() {
    $this->mockRoomRepository
        ->shouldReceive("getById")
        ->with($this->roomId)
        ->andReturn(null);
    $response = $this->livechatService->assignAgent($this->roomId,$this->agentId,$this->groupId,"haidar",null,null);

    $this->assertInstanceOf(RecordNotFoundException::class,$response);

  }

  public function testShouldSuccessAssignLivechatWithCurrentLivechatStatusIsBot() {
    Event::fake();
    $this->mockRoomRepository
      ->shouldReceive("getById")
      ->with($this->roomId)
      ->andReturn($this->roomResponse)
      ->twice();

    $this->mockLivechatRepository
      ->shouldReceive("insert")
      ->with([
        "room_id" => $this->roomId,
        "handle_by" => $this->agentId,
        "updated_by" => $this->agentId,
        "request_at" => Carbon::now()->toDateTimeString(),
        "start_at" => Carbon::now()->toDateTimeString(),
        "group_id" => $this->groupId,
        "status" => "live"
      ])
      ->once()
      ->andReturn(true);

    $this->mockRoomRepository
      ->shouldReceive("getAllUnservedRoom")
      ->atMost()
      ->times(1);

    $response = $this->livechatService->assignAgent($this->roomId,$this->agentId,$this->groupId,"haidar",null,null);
    Event::assertDispatched(LivechatAssigned::class);
    $this->assertTrue($response);
  }

  public function testShouldSuccessAssignLivechatWithCurrentLivechatStatusIsLiveOrRequest() {
    Event::fake();

    $this->roomResponse->livechat = new LivechatResponse(1,12,12,"live",null,null,null,null,null,null);
    $this->mockRoomRepository
      ->shouldReceive("getById")
      ->with($this->roomId)
      ->andReturn($this->roomResponse);

    $this->mockLivechatRepository
        ->shouldNotReceive("insert")
        ->never();

    $this->mockLivechatRepository
      ->shouldReceive("update")
      ->with([
          "room_id" => $this->roomId,
        ],
        [
          "handle_by" => $this->agentId,
          "updated_by" => $this->agentId,
          "status" => "live",
          "start_at" => Carbon::now()->toDateTimeString(),
      ])
      ->once()
      ->andReturn(true);

    $this->mockRoomRepository
      ->shouldReceive("getAllUnservedRoom")
      ->atMost()
      ->times(1);

    $this->roomResponse->livechat = new LivechatResponse(1,$this->agentId,$this->agentId,"live",null,null,null,null,null,null);

    $this->mockRoomRepository
      ->shouldReceive("getById")
      ->with($this->roomId)
      ->andReturn($this->roomResponse);

    $response = $this->livechatService->assignAgent($this->roomId,$this->agentId,$this->groupId,"haidar",null,null);

    Event::assertDispatched(LivechatAssigned::class);
    $this->assertTrue($response);
  }

  public function testShouldAssignToAgentLivechatAndReturnBadParameterRequestExceptionCauseUserIdOrAgentIddIsNull() {
    $response = $this->livechatService->assignAgent(null,null,null,null,null,null);

    $this->assertInstanceOf(BadRequestParameterException::class,$response);

  }

  public function testShouldAssignLivechatToGroupAgentAndReturnBadParameterRequestExceptionCauseGroupIdOrRoomIdIsNull() {
    $response = $this->livechatService->assignGroup(null,null);
    $this->assertInstanceOf(BadRequestParameterException::class,$response);
  }

  public function testShouldAssignLivechatToGroupAgentAndReturnRecordNotFoundException() {
    $this->mockRoomRepository
      ->shouldReceive("getById")
      ->with($this->roomId)
      ->andReturn(null);

    $response = $this->livechatService->assignGroup($this->groupId,$this->roomId);
    $this->assertInstanceOf(RecordNotFoundException::class,$response);
  }

  public function testShouldAssignLivechatToGroupAgentAndSuccessWithStatusBot() {
    Event::fake();
    $this->mockRoomRepository
      ->shouldReceive("getById")
      ->with($this->roomId)
      ->andReturn($this->roomResponse);

    $this->mockLivechatRepository
        ->shouldReceive("insert")
        ->with([
          "room_id" => $this->roomId,
          "status" => "request",
          "request_at" => Carbon::now()->toDateTimeString(),
          "group_id" => $this->groupId
        ])
        ->andReturn(true);

    $this->mockLivechatRepository
        ->shouldNotReceive("update")
        ->never();

   $this->mockRoomRepository
        ->shouldReceive("getAllUnservedRoom")
        ->atMost()
        ->times(1);

    $this->mockRoomRepository
        ->shouldReceive("getById")
        ->with($this->roomId)
        ->andReturn($this->roomResponse);

    $response = $this->livechatService->assignGroup($this->groupId,$this->roomId,$this->assigneeId);

    Event::assertDispatched(LivechatAssigned::class);
    $this->assertTrue($response);
  }

  public function testShouldAssignLivechatToGroupAgentAndSuccessWithStatusLiveOrRequest() {
    Event::fake();
    $this->roomResponse->livechat = new LivechatResponse(1,null,null,"request",null,null,null,null,Carbon::now()->toDateTimeString(),$this->groupId);

    $this->mockRoomRepository
      ->shouldReceive("getById")
      ->with($this->roomId)
      ->andReturn($this->roomResponse);

      $this->mockLivechatRepository
      ->shouldNotReceive("insert")
      ->never();

    $this->mockLivechatRepository
        ->shouldReceive("update")
        ->with(
        [
          "room_id" => $this->roomId
        ],
        [
        "status" => "request",
        "handle_by" => null,
        "updated_by" => null,
        "start_at" => null,
        "group_id" => $this->groupId
      ])
      ->andReturn(true);

    $this->roomResponse->livechat = new LivechatResponse(1,null,null,"request",null,null,null,null,Carbon::now()->toDateTimeString(),$this->groupId);

    $this->mockRoomRepository
        ->shouldReceive("getById")
        ->with($this->roomId)
        ->andReturn($this->roomResponse);

    $this->mockRoomRepository
        ->shouldReceive("getAllUnservedRoom")
        ->atMost()
        ->times(1);

    $response = $this->livechatService->assignGroup($this->groupId,$this->roomId);

    Event::assertDispatched(LivechatAssigned::class);
    $this->assertTrue($response);
  }

  public function testShouldGetAllUnservedRoom() {
    $this->mockRoomRepository
      ->shouldReceive("getAllUnservedRoom")
      ->andReturn([])
      ->atMost()
      ->times(1);

    $response = $this->livechatService->unservedRoom($this->appId);
    $this->assertIsArray($response);
  }

  public function testShouldValidateIfGivenAssigneeIdAndRoomIsNullWhenRecordAssignmemnt() {
    $response = $this->livechatService->recordAssignment(null, null,null, null);

    $this->assertInstanceOf(BadRequestParameterException::class, $response);
  }

  public function testShouldSuccessCreateHistoryAssignment() {
    $this->roomResponse->livechat = new LivechatResponse(1,null,null,"live",null,null,null,null,Carbon::now()->toDateTimeString(),$this->groupId);

    $created_by = new \StdClass;
    $created_by->name = "ahmad haidar";
    $created_by->email = "ahmadhdr.22@gmail.com";

    $this->roomResponse->created_by = $created_by;

    $this->mockLivechatRepository
         ->shouldReceive("createHistoryAssignment")
         ->andReturn(true)
         ->once();

    $response = $this->livechatService->recordAssignment($this->roomResponse, "ahmad haidar","ival@lenna.ai","admin");

    $this->assertTrue($response);
  }

  public function testShouldValidateIfGivenAppIdIsNull() {
    $response = $this->livechatService->reportingAssignment(null, false, null,null, "");
    $this->assertInstanceOf(BadRequestParameterException::class, $response);
  }

  public function testShouldSuccessGetReportingAssignment() {
    $this->mockLivechatRepository
      ->shouldReceive("getHistoryAssignment")
      ->atMost()
      ->andReturn([])
      ->times(1);

    $response = $this->livechatService->reportingAssignment($this->appId, false ,null,null, "");

    return $this->assertNotNull($response);
  }

}

