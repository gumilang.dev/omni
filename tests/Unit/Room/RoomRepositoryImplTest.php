<?php

namespace Tests\Unit\Room;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Modules\Room\Repositories\Impl\RoomRepositoryImpl;
use App\Modules\Room\Request\GetAllRoomRequest;

class RoomRepositoryImplTest extends TestCase
{
    private $roomRepositoryImpl;
    private $appId = 6;

    public function setUp(): void {
      parent::setUp();
      $this->roomRepositoryImpl = new RoomRepositoryImpl();
    }

    public function testShouldGetAllRoomWithAppID()
    {
      $request = new GetAllRoomRequest($this->appId, 1, "", "", [],false, 7);
      $response = $this->roomRepositoryImpl->getByAppID($request);

      $first_response = $response[0];
      $this->assertObjectHasAttribute("id",$first_response);
      $this->assertObjectHasAttribute("unread_count",$first_response);
      $this->assertObjectHasAttribute("created_at",$first_response);
      $this->assertObjectHasAttribute("updated_at",$first_response);
      $this->assertObjectHasAttribute("created_by",$first_response);
      $this->assertObjectHasAttribute("channel",$first_response);
      $this->assertObjectHasAttribute("notes",$first_response);
      $this->assertObjectHasAttribute("additional_informations",$first_response);
      $this->assertObjectHasAttribute("livechat",$first_response);
      $this->assertObjectHasAttribute("messages",$first_response);
      $this->assertObjectHasAttribute("contact_id",$first_response->created_by);
      $this->assertObjectHasAttribute("integration",$first_response);
      $this->assertObjectHasAttribute("is_blocked",$first_response);

      $this->assertIsInt($first_response->unread_count);
      $this->assertIsInt($first_response->id);
      $this->assertIsString($first_response->created_at);
      $this->assertIsString($first_response->updated_at);
      $this->assertIsObject($first_response->created_by);
      $this->assertIsObject($first_response->channel);
      $this->assertIsArray($first_response->messages);
      $this->assertIsObject($first_response->integration);
      $this->assertIsBool($first_response->is_blocked);

      $this->assertIsObject($response);
      $this->assertNotNull($response);
    }

    public function testShouldReturnEmptyRoomIfGivenUnknowAppId() {
      $request = new GetAllRoomRequest(1000, 1, "", "", [],false, 7);
      $response = $this->roomRepositoryImpl->getByAppID($request);

      $this->assertCount(0, $response);
    }

    public function testShouldReturnSingleRoom() {
      $response = $this->roomRepositoryImpl->getByUserId(29561);

      $this->assertObjectHasAttribute("id",$response);
      $this->assertObjectHasAttribute("unread_count",$response);
      $this->assertObjectHasAttribute("created_at",$response);
      $this->assertObjectHasAttribute("updated_at",$response);
      $this->assertObjectHasAttribute("created_by",$response);
      $this->assertObjectHasAttribute("channel",$response);
      $this->assertObjectHasAttribute("notes",$response);
      $this->assertObjectHasAttribute("additional_informations",$response);
      $this->assertObjectHasAttribute("livechat",$response);
      $this->assertObjectHasAttribute("messages",$response);
      $this->assertObjectHasAttribute("contact_id",$response->created_by);
      $this->assertObjectHasAttribute("integration",$response);
      $this->assertObjectHasAttribute("is_blocked",$response);

      $this->assertIsInt($response->unread_count);
      $this->assertIsInt($response->id);
      $this->assertIsString($response->created_at);
      $this->assertIsString($response->updated_at);
      $this->assertIsObject($response->created_by);
      $this->assertIsObject($response->channel);
      $this->assertIsArray($response->messages);
      $this->assertIsObject($response->integration);
      $this->assertIsBool($response->is_blocked);
      // $this->assertIsObject($response);
      $this->assertNotNull($response);
    }

    public function testShouldGetSingleRoomByUnservedStatus() {
      $response = $this->roomRepositoryImpl->getByUnservedRoom($this->appId);

      $this->assertObjectHasAttribute("id",$response);
      $this->assertObjectHasAttribute("unread_count",$response);
      $this->assertObjectHasAttribute("created_at",$response);
      $this->assertObjectHasAttribute("updated_at",$response);
      $this->assertObjectHasAttribute("created_by",$response);
      $this->assertObjectHasAttribute("channel",$response);
      $this->assertObjectHasAttribute("notes",$response);
      $this->assertObjectHasAttribute("additional_informations",$response);
      $this->assertObjectHasAttribute("livechat",$response);
      $this->assertObjectHasAttribute("contact_id",$response->created_by);
      $this->assertObjectHasAttribute("integration",$response);
      $this->assertObjectHasAttribute("is_blocked",$response);

      $this->assertNull($response->livechat->start_at);
      $this->assertNull($response->livechat->end_at);
      $this->assertNotNull($response->livechat->request_at);
      $this->assertObjectHasAttribute("messages",$response);
      $this->assertIsInt($response->unread_count);
      $this->assertIsInt($response->id);
      $this->assertIsString($response->created_at);
      $this->assertIsString($response->updated_at);
      $this->assertIsObject($response->created_by);
      $this->assertIsObject($response->channel);
      $this->assertIsArray($response->messages);
      $this->assertIsBool($response->is_blocked);
      $this->assertIsObject($response->integration);

      $this->assertIsObject($response);
      $this->assertNotNull($response);
    }

    public function testShouldGetAllUnservedRoom() {
      $response = $this->roomRepositoryImpl->getAllUnservedRoom($this->appId);
      $first_response = $response[0];
      $this->assertObjectHasAttribute("id",$first_response);
      $this->assertObjectHasAttribute("unread_count",$first_response);
      $this->assertObjectHasAttribute("created_at",$first_response);
      $this->assertObjectHasAttribute("updated_at",$first_response);
      $this->assertObjectHasAttribute("created_by",$first_response);
      $this->assertObjectHasAttribute("channel",$first_response);
      $this->assertObjectHasAttribute("notes",$first_response);
      $this->assertObjectHasAttribute("additional_informations",$first_response);
      $this->assertObjectHasAttribute("livechat",$first_response);
      $this->assertObjectHasAttribute("contact_id",$first_response->created_by);
      $this->assertObjectHasAttribute("integration",$first_response);
      $this->assertObjectHasAttribute("is_blocked",$first_response);

      $this->assertIsInt($first_response->id);
      $this->assertIsString($first_response->created_at);
      $this->assertIsString($first_response->updated_at);
      $this->assertIsObject($first_response->livechat);
      $this->assertNull($first_response->livechat->start_at);
      $this->assertNull($first_response->livechat->end_at);
      $this->assertNotNull($first_response->livechat->request_at);
      $this->assertEquals("request",$first_response->livechat->status);
      $this->assertIsBool($first_response->is_blocked);
      $this->assertIsObject($first_response->integration);

      $this->assertNotNull($response);
    }

    public function testShouldGetRoomById() {
      $response = $this->roomRepositoryImpl->getById(7);

      $this->assertObjectHasAttribute("id",$response);
      $this->assertObjectHasAttribute("unread_count",$response);
      $this->assertObjectHasAttribute("created_at",$response);
      $this->assertObjectHasAttribute("updated_at",$response);
      $this->assertObjectHasAttribute("created_by",$response);
      $this->assertObjectHasAttribute("channel",$response);
      $this->assertObjectHasAttribute("channel",$response);
      $this->assertObjectHasAttribute("notes",$response);
      $this->assertObjectHasAttribute("additional_informations",$response);
      $this->assertObjectHasAttribute("livechat",$response);
      $this->assertObjectHasAttribute("messages",$response);
      $this->assertObjectHasAttribute("contact_id",$response->created_by);
      $this->assertObjectHasAttribute("integration",$response);
      $this->assertObjectHasAttribute("is_blocked",$response);

      $this->assertIsInt($response->unread_count);
      $this->assertIsInt($response->id);
      $this->assertIsString($response->created_at);
      $this->assertIsString($response->updated_at);
      $this->assertIsObject($response->created_by);
      $this->assertIsObject($response->channel);
      $this->assertIsArray($response->messages);
      $this->assertIsObject($response->integration);
      $this->assertIsBool($response->is_blocked);
      // $this->assertIsObject($response);
      $this->assertNotNull($response);
    }


}
