<?php
namespace Tests\Integration;

use Tests\TestCase;

class TimeReportServedTest extends TestCase {

  public function setUp() {
    parent::setUp();
    $this->service = $this->app->make("App\Services\TimeReportServedImpl");
  }

  public function test_create_report_served() {
    // setup
    $mock = [
      "request_at" => "02-02-2001",
      "served_at" => "02-02-2002",
      "agent_id" => 1,
      "room_id" => 1,
      "app_id" => 6,
      "agent_email" => "ahmadhdr.22@gmail.com",
      "agent_name" => "ahmad haidar",
      "diff" => 1
    ];


    // exercise
    $response = $this->service->createReport($mock);
    // verify
    $this->assertIsObject($response);
  }

  public function test_it_should_be_throw_error() {
     // setup
     $mock = [
      "request_at" => "02-02-2001",
      "served_at" => "02-02-2002",
      "room_id" => 1
    ];


    // exercise
    $response = $this->service->createReport($mock);
    $error = $response["error"];
    // verify
    $this->assertTrue($error);
    $this->assertArrayHasKey("message",$response);
  }


  public function tearDown()
  {
      parent::tearDown();
  }

}
