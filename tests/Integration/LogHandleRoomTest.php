<?php
namespace Tests\Integration;

use Tests\TestCase;
use Log;

class LogHandleRoomTest extends TestCase {

  public function setUp() {
    parent::setUp();
    $this->service = $this->app->make("App\Services\LogHandleRoomServiceImpl");
  }

  public function setUpData($room_id,$user_id) {
    return [
      "room_id" => $room_id,
      "user_id" => $user_id,
      "app_id" => 1
    ];
  }

  public function test_create_log() {

    $response = $this->service->createLog($this->setUpData(1,1));
    // verify
    $this->assertIsObject($response);
  }

  public function test_should_throw_error_when_room_id_is_not_integer() {
    $response = $this->service->createLog($this->setUpData("1",1));
    $this->assertTrue($response["error"]);
    $this->assertArrayHasKey("message",$response);
  }

  public function test_should_throw_error_when_user_id_is_not_integer() {
    $response = $this->service->createLog($this->setUpData(1,"1"));
    $this->assertTrue($response["error"]);
    $this->assertArrayHasKey("message",$response);
  }

  public function test_should_throw_error_when_user_id_is_null() {
    $response = $this->service->createLog($this->setUpData("","1"));
    $this->assertTrue($response["error"]);
    $this->assertArrayHasKey("message",$response);
  }

  public function test_should_throw_error_when_room_id_is_null() {
    $response = $this->service->createLog($this->setUpData("","1"));
    $this->assertTrue($response["error"]);
    $this->assertArrayHasKey("message",$response);
  }

  public function tearDown()
  {
      parent::tearDown();
  }

}
