<?php

namespace App\Modules\BroadcastMessage\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Shared\Common\WebResponse;
use App\Modules\BroadcastMessage\Services\BroadcastMessageServiceImpl;

class ReportingBroadcastController extends ApiController
{

  private $broadcastMessageService;

  public function __construct(BroadcastMessageServiceImpl $broadcastMessageService) {
    $this->broadcastMessageService = $broadcastMessageService;
  }

  public function reportingMobile(Request $request, $appId) {
    $call_service = $this->broadcastMessageService->reporting($appId,$request->filterText, $request->start, $request->end,$request->paginated,"mobile");
    return $this->catchError($call_service, function($response) {
      return WebResponse::clean($response);
    });
  }

  public function reportingWhatsapp(Request $request, $appId) {
    $call_service = $this->broadcastMessageService->reporting($appId,$request->filterText, $request->start, $request->end,$request->paginated,"whatsapp");
    return $this->catchError($call_service, function($response) {
      return WebResponse::clean($response);
    });
  }

}
