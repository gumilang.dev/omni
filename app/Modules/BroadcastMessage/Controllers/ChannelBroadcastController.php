<?php

namespace App\Modules\BroadcastMessage\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Modules\Chat\Controllers\ChatController;
use App\Http\Controllers\ApiController;
use PeterPetrus\Auth\PassportToken;
use App\Modules\User\Models\UserPlatform;
use App\Modules\BroadcastMessage\Models\BroadcastMessage;
use App\Modules\User\Models\User;
use App\Modules\Line\Services\LineApiController;
use App\Modules\Facebook\Services\FacebookApiController;
use App\Modules\Telegram\Services\TelegramApiController;
use App\Modules\Whatsapp\Services\InfobipApiController;
use App\Modules\Whatsapp\Services\DamcorpApiController;
use App\Modules\Whatsapp\Services\WaOfficialApiController;
use App\Modules\Whatsapp\Services\ApiWhaApiController;
use App\Modules\Whatsapp\Services\WappinApiController;
use App\Modules\BroadcastMessage\Helpers\BroadcastMessageHelper;
use App\Modules\Log\Helpers\WebhookHelper;
use App\Modules\Chat\Helpers\ChatHelper;
use App\Modules\Mobile\Services\MobileApiController;
use App\Modules\Integration\Models\Integration;
use App\Modules\Mobile\Helpers\LennaMessageType;
use App\Modules\Log\Models\ApiLog;
use App\Events\SendBroadcastWhatsapp;
use App\Modules\BroadcastMessage\Services\BroadcastMessageServiceImpl;


class ChannelBroadcastController extends ApiController
{
    private $broadcastMessageService;

    public function __construct(BroadcastMessageServiceImpl $broadcastMessageService) {
      $this->broadcastMessageService = $broadcastMessageService;
    }

    public function sendBroadcast(Request $request, $appId)
    {
        // receiver
        if (isset($request->phone) && $request->channel == 'whatsapp') {
            $user = User::where([['phone', "=", $request->phone], ['app_id', '=', $appId]])->first();
        } else {
            $user = User::where('id', $request->userId)->first();
        }

        if (!$user)
            return $this->errorResponse(null, 'user not found');

        $chatRoom = $user->rooms->first();
        if (!$chatRoom)
            return $this->errorResponse(null, 'user should chat the bot first');


        if (!$integration = $this->_getIntegrationData($user->channel_id, $user->app_id))
            return $this->errorResponse(null, "Plase connect your {$chatRoom->channel->name} channel first");

        // sender
        $decodedToken = PassportToken::dirtyDecode($request->header('token'));
        if (!isset($decodedToken['user_id']))
            return $this->errorResponse(null, 'token not valid');

        $participable = UserPlatform::where('id', $decodedToken['user_id'])->firstOrFail();

        // participate sender to room
        ChatHelper::participateUser($participable, $chatRoom->id);
        // set content
        foreach ($request->messageBody as $key => $message) {
            $content[] = $this->_setMessageType($message);
        }
        // create message
        $newMessage = ChatHelper::createMessage(['content' => $content, 'user' => $participable, 'room' => $chatRoom]);

        switch ($chatRoom->channel->name) {
            case 'line':

                $data = [
                    'chatRoom' => $chatRoom,
                    'integration' => $integration
                ];
                $lineController = new LineApiController($data);
                $apiType = 'push';
                $response = $lineController->pushMessage($newMessage, $request->quickbutton);
                break;
            case 'facebook':
                $data = [
                    'chatRoom' => $chatRoom,
                    'integration' => $integration
                ];

                $facebookController = new FacebookApiController($data);
                $response = $facebookController->echoMessage($newMessage);
                $apiType = 'echo';

                break;
            case 'telegram':
                $data = [
                    'chatRoom' => $chatRoom,
                    'botToken' => $integration->integration_data['botToken']
                ];

                $telegramApiController = new TelegramApiController($data);
                $response = $telegramApiController->sendMessage($newMessage);
                $apiType = 'send';

                break;
            case 'whatsapp':
                $data = [
                    'chatRoom' => $chatRoom,
                    'integration' => $integration
                ];

                switch ($integration->integration_data['apiService']) {
                    case 'infobip':
                        $infobipApiController = new InfobipApiController($data);
                        $response = $infobipApiController->sendAdvanceMessage($newMessage);

                        break;
                    case 'damcorp':
                        $damcorpApi = new DamcorpApiController($data);
                        $response = $damcorpApi->sendMessage($newMessage);

                        break;
                    case 'apiwha':
                        $ApiWhaApiController = new ApiWhaApiController($data);
                        $response = $ApiWhaApiController->sendMessage($newMessage);
                        if (!$response->success) {
                            return $this->errorResponse(null, $response->description);
                        }
                        break;

                }


                $apiType = 'send';
                break;
            case 'mobile':
                // jika sendertype dari bot, jangan lakukan fcm broadcast
                if ($request->senderType != 'bot') {

                    $data = [
                        'integration' => $integration
                    ];
                    switch ($newMessage->type) {
                        case 'text':
                            $body = $newMessage->content->text;
                            break;

                        default:
                            $body = "Send $newMessage->type message";

                            break;
                    }


                    $newBroadcastMessage = BroadcastMessageHelper::createMessage([
                        'data' => [
                            'type' => $request->type,
                            'category' => $request->category,
                            'sub_category' => $request->subCategory,
                            'title' => $request->title,
                            'message' => $newMessage->content,
                        ],
                        'channel' => $request->channel,
                        'receiver_id' => $chatRoom->created_by->id,
                        'broadcastType' => $request->type,
                        'broadcastCategory' => $request->category,
                        'broadcastSubCategory' => $request->subCategory,
                    ]);

                    $mobileApiController = new MobileApiController($data);

                    $response = $mobileApiController->sendBroadcast($newBroadcastMessage, ['sendTo' => $chatRoom->created_by->fcm_token]);

                }
                $apiType = 'send';
                break;
        }

        $broadcastMessage = BroadcastMessageHelper::createMessage([
            'data' => [
                'title' => $request->messageTitle,
                'message' => $request->messageBody,
            ],
            'channel' => $request->channel,
            'receiver_id' => $user->id,
            'broadcastType' => $request->type,
            'broadcastCategory' => $request->category,
        ]);
        ApiLog::create([
            'app_id' => $appId,
            'user_id' => $request->decodedUserId,
            'request' => json_encode($request->all()),
            'response' => json_encode([
                'success' => true,
                'data' => $broadcastMessage
            ])
        ]);
        return $this->successResponse($broadcastMessage, 'success');
    }

    public function sendBroadcastWhatsapp(Request $request, $appId){
        // return $request;

        // receiver
        if (isset($request->user->number) && $request->channel == 'whatsapp') {
            $user = User::where([['phone', "=", $request->user->number], ['app_id', '=', $appId]])->first();
        } else {
            $user = User::where('id', $request->userId)->first();
        }

        if (!$user)
            return $this->errorResponse(null, 'user not found');

        $chatRoom = $user->rooms->first();
        if (!$chatRoom)
            return $this->errorResponse(null, 'user should chat the bot first');
        if (!$integration = $this->_getIntegrationData($user->channel_id, $appId)) {
            return $this->errorResponse(null, "Plase connect your {$chatRoom->channel->name} channel first");
        }

       // sender
        $decodedToken = PassportToken::dirtyDecode($request->header('token'));
        if (!isset($decodedToken['user_id']))
            return $this->errorResponse(null, 'token not valid');

        $participable = UserPlatform::where('id', $decodedToken['user_id'])->firstOrFail();
        // return $participable;
        // participate sender to room
        ChatHelper::participateUser($participable, $chatRoom->id);
        // return $request;
        //  set content
        // $contents = ['type' => 'text', 'text' => $request->messageBody, 'speech' => $request->messageBody];
        foreach ($request->messageBody as $key => $message) {
            $content[] = $this->_setMessageType($message);
        }
        // return $content;
        // $this->_setMessageType($content);


        // create message
        $newMessage = ChatHelper::createMessage(['content' => $content, 'user' => $participable, 'room' => $chatRoom]);
            // return $newMessage;
        switch ($chatRoom->channel->name) {
            case 'whatsapp':
                $data = [
                    'chatRoom' => $chatRoom,
                    'integration' => $integration
                ];

                switch ($integration->integration_data['apiService']) {
                    case 'infobip':
                        $infobipApiController = new InfobipApiController($data);
                        $response = $infobipApiController->sendAdvanceMessage($newMessage);
                        break;
                    case 'damcorp':
                        $damcorpApi = new DamcorpApiController($data);
                        // return 'damcorp';
                        $response = $damcorpApi->sendMessage($newMessage);
                        break;
                    case 'official':
                        $WaOfficialApiController = new WaOfficialApiController($data);
                        $response = $WaOfficialApiController->sendMessage($newMessage);
                        break;
                    case 'apiwha':
                        $ApiWhaApiController = new ApiWhaApiController($data);
                        $response = $ApiWhaApiController->sendMessage($newMessage);
                        if (!$response->success) {
                            return $this->errorResponse(null, $response->description);
                        }
                        break;

                }
                break;

            default:
                return $this->errorResponse(null, "Someting went wrong");
                break;
        }
        $broadcastMessage = BroadcastMessageHelper::createMessage([
            'data' => [
                'title' => $request->messageTitle,
                'message' => $request->messageBody,
            ],
            'channel' => $request->channel,
            'receiver_id' => $user->id,
            'broadcastType' => $request->type,
            'broadcastCategory' => $request->category,
        ]);
        ApiLog::create([
            'app_id' => $appId,
            'user_id' => $request->decodedUserId,
            'request' => json_encode($request->all()),
            'response' => json_encode([
                'success' => true,
                'data' => $broadcastMessage
            ])
        ]);

        return $this->successResponse($broadcastMessage, 'Success');
    }

    public function sendBroadcastNonHsmWhatsapp(Request $request, $appId)
    {
        if (!$integration = WebhookHelper::getIntegrationDataById('whatsapp', $appId, $request->integrationId)) {
            return $this->errorResponse(null, "Plase connect your {$request->channel} channel first");
        }
        $decodedToken = PassportToken::dirtyDecode($request->header('token'));
        if (!isset($decodedToken['user_id']))
            return $this->errorResponse(null, 'token not valid');

        foreach ($request->messageBody as $key => $message) {
            $content[] = $this->_setMessageType($message);
        }

        $newMessage = ChatHelper::createMessageOnly(['content' => $content]);

        if ($request->broadcastType == 'all') {
            $user = $request->allUser;
            foreach ($request->allUser as $key => $singlePhone) {
                $phone[] = $singlePhone['number'];
            }
        } else if ($request->broadcastType == 'single'){
            $user = $request->user;
            $phone[] = ($request->user['number']);
        } else if ($request->broadcastType == 'segment'){
            $user = $request->segmentUser;
            foreach ($request->segmentUser as $key => $singlePhone) {
                $phone[] = $singlePhone['phone'];
            }
            // $phone[] = ($request->segmentUser);
        } else {
            $user = $request->user;
            foreach ($request->user as $key => $singlePhone) {
                $phone[] = $singlePhone['number'];
            }
        }

        switch ($request->channel) {
            case 'whatsapp':
                $data = [
                    'integration' => $integration,
                    'user_id' => $request->decodedUserId
                ];

                switch ($integration->integration_data['apiService']) {
                    case 'infobip':
                        $infobipApiController = new InfobipApiController($data);
                        $response = $infobipApiController->sendAdvanceMessage($newMessage, $phone);
                        break;
                    case 'damcorp':
                        $damcorpApi = new DamcorpApiController($data);
                        $response = $damcorpApi->callSendBroadcastNonHsm($newMessage, $phone);
                        break;
                    case 'official':
                        $WaOfficialApiController = new WaOfficialApiController($data);
                        $response = $WaOfficialApiController->callSendBroadcastNonHsm($newMessage, $phone);
                        break;
                    case 'apiwha':
                        $ApiWhaApiController = new ApiWhaApiController($data);
                        $response = $ApiWhaApiController->sendMessage($newMessage, $phone);
                        if (!$response->success) {
                            return $this->errorResponse(null, $response->description);
                        }
                        break;
                    case 'wappin':
                        $apiWappin = new WappinApiController($data);
                        $response = $apiWappin->callSendBroadcastNonHsm($newMessage, $phone);
                        break;
                }
                break;

            default:
                return $this->errorResponse(null, "Someting went wrong");
                break;
        }
        $this->broadcastMessageService->recordLogWhatsapp($request->broadcastType, $user, $response,["content" => $content], $appId, $request->integrationId,$request->decodedUserId);
        return $this->successResponse($response, 'Success');

    }

    public function uploadImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'file|max:5120|required'
          ]);

          if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first('file'), "Upload image failed");
          }

          $name = $request->file('file')->getClientOriginalName();
          $file = $request->file('file');
          $extension = $request->file('file')->getClientOriginalExtension();

          if ($request->hasFile('file')){
            $allowedExension = ["jpeg","jpg","gif","png","PNG","JPG","JPEG"];
            if(in_array($extension,$allowedExension)) {
              Storage::disk('public_whatsapp')->put($name, file_get_contents($file));
              return $this->successResponse(env('APP_URL').'/upload/whatsapp/'.$name, 'file '.$name.' uploaded with extention '.$extension.'');
            }else{
              return $this->errorResponse(null,"extension not allowed");
            }
          }else{
            return $this->errorResponse(null,"file cannot be null");
          }

          if ($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg') {
            $optimizerChain = OptimizerChainFactory::create();
            $optimizerChain->optimize(public_path('/upload/whatsapp/').$name, public_path('/upload/whatsapp/').$name);
          }

    }

    public function deleteImage(Request $request)
    {
        $fileToDelete = [];
        Storage::disk('public_whatsapp')->delete([$request->fileToDelete]);
        return $this->successResponse(null, 'file deleted');
    }

    public function _getIntegrationData($channelId, $appId)
    {
        $integration = Integration::where([['app_id', "=", $appId], ['status', "=", true], ['channel_id', "=", $channelId]])->first();
        return $integration;
    }
    public function _getIntegrationDataV2($channelName, $appId, $integrationId)
    {
        $integration = Integration::where([['app_id', "=", $appId], ['status', "=", true], ['id', '=', $integrationId]])
            ->whereHas('channel', function ($q) use ($channelName) {
                $q->where('name', $channelName);
            })->first();
        return $integration;
    }
    public function _setMessageType($data)
    {
        // dd($data);
        $lennaMessageType = new LennaMessageType;
        switch ($data['type']) {
            case 'text':
                $output = $lennaMessageType->text([
                    'text' => $data['text']
                ]);

                break;
            case 'file':
                $output = $lennaMessageType->file([
                    'fileUrl' => $data['fileUrl'],
                    'fileName' => $data['fileName'],

                ]);
                break;
            case 'image':
                $output = $lennaMessageType->image([
                    'previewImageUrl' => $data['previewImageUrl'],
                    'originalContentUrl' => $data['originalContentUrl'],
                    'caption' => $data['caption']
                ]);
                break;
            case 'audio':
                $output = $lennaMessageType->audio([
                    'originalContentUrl' => $data['originalContentUrl']
                ]);
                break;
            case 'video':
                $output = $lennaMessageType->video([
                    'originalContentUrl' => $data['originalContentUrl'],
                    'previewImageUrl' => $data['previewImageUrl']
                ]);
                break;
            case 'confirm':
                $output = $lennaMessageType->confirm([
                    'text' => $data['text'],
                    'actions' => $data['actions'],
                ]);
                break;
            case 'html':
                $output = $lennaMessageType->html([
                    'content' => $data['html']
                ]);
                break;
            case 'grid':
                $output = $lennaMessageType->grid([
                    'title' => $data['title'],
                    'subTitle' => $data['subTitle'],
                    'imageUrl' => $data['imageUrl'],
                    'columns' => $data['columns']
                ]);
                break;
            case 'transaction':
                $output = $lennaMessageType->summary([
                    'title' => $data['title'] ?? '',
                    'subType' => $data['subType'] ?? '',
                    'imageUrl' => $data['imageUrl'] ?? '',
                    'columns' => $data['columns'] ?? $data['elements']
                ]);
                break;
            case 'summary':
                $output = $lennaMessageType->summary([
                    'title' => $data['title'] ?? '',
                    'subType' => $data['subType'] ?? '',
                    'imageUrl' => $data['imageUrl'] ?? '',
                    'columns' => $data['columns'] ?? $data['elements']
                ]);
                break;
            case 'carousel':
                $output = $lennaMessageType->carousel([
                    'imageAspectRatio' => $data['imageAspectRatio'],
                    'imageSize' => $data['imageSize'],
                    'columns' => $data['columns'],
                ]);
                break;
            case 'button':
                $output = $lennaMessageType->button([
                    'text' => $data['text'],
                    'actions' => $data['actions'],
                ]);
                break;
            case 'weather':
                $output = $lennaMessageType->weather([
                    'area' => $data['area'],
                    'country' => $data['country'],
                    'countryCode' => $data['countryCode'],
                    'columns' => $data['columns'],
                ]);
                break;
            default:
                $output = $data;

                break;
        }
        return $output;
    }

}
