<?php
namespace App\Modules\BroadcastMessage\Controllers;
ini_set('memory_limit', '-1');

use Illuminate\Http\Request;
use App\Modules\Chat\Controllers\ChatController;
use App\Http\Controllers\ApiController;
use PeterPetrus\Auth\PassportToken;
use App\Modules\User\Models\UserPlatform;
use App\Modules\Chat\Helpers\ChatHelper;
use App\Modules\Log\Helpers\WebhookHelper;
use App\Modules\User\Models\User;
use App\Modules\StaticToken\Models\StaticToken;
use App\Modules\Mobile\Services\BackendService;
use App\Modules\Line\Services\LineApiController;
use App\Modules\Facebook\Services\FacebookApiController;
use App\Modules\Telegram\Services\TelegramApiController;
use App\Modules\Whatsapp\Services\InfobipApiController;
use App\Modules\Whatsapp\Services\DamcorpApiController;
use App\Modules\Whatsapp\Services\WaOfficialApiController;
use App\Modules\Whatsapp\Services\ApiWhaApiController;
use App\Modules\Whatsapp\Services\WappinApiController;
use App\Modules\BroadcastMessage\Helpers\BroadcastMessageHelper;
use App\Modules\Mobile\Services\MobileApiController;
use App\Modules\Integration\Models\Integration;
use App\Modules\Mobile\Helpers\LennaMessageType;
use App\Modules\Bot\Services\BotService;
use App\Modules\Channel\Models\Channel;
use App\Modules\Log\Models\ApiLog;
use App\Modules\Log\Models\WebhookLog;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;





class HsmBroadcastController extends ApiController
{

    public function __construct()
    {
        $this->channel = Channel::where('name', 'whatsapp')->first();
    }

    public function test(){
        return $this->successResponse('testing');
    }

    public function _startConversation($storyId, $botId, $userId, $appId)
    {
        $botService = new BotService(['appId' => $appId]);
        $inputPhrase = $botService->getInputPhrase($storyId)->input_phrase;

        return $botService->nlpResponse([
            'botId' => $botId,
            'userId' => $userId,
            'phrase' => $inputPhrase,
            'lat' => 0,
            'lon' => 0,
            'channel' => 'whatsapp',
            'headers' => [],
        ]);


    }

    public function __saveLog($data = [])
    {
        ApiLog::create([
            'app_id' => $data['app_id'],
            'user_id' => $data['user_id'],
            'request' => $data['request'],
            'response' => $data['response']
        ]);
    }

    public function sendBroadcast(Request $request, $appId)
    {
        // return $request;
        $validator = Validator::make($request->all(), [
            'channel' => 'bail|required', // bail = stop running validation rules on an attribute after the first validation failure
            'type' => 'bail|required',
            'category' => 'bail|required',
            'templateName' => 'bail|required',
            'messageTitle' => 'bail|required',
            'phone' => 'bail|required|array',
            'templateParams' => 'bail|array',
            'integrationId' => 'bail|required'
            // 'status' => 'required'
        ]);

        if ($validator->fails()) {
            if ($validator->errors()) {
                return $this->errorResponse(null, $validator->errors()->first());
            }
        }

        $apiLog = ApiLog::where('user_id',$request->decodedUserId)->get();
        // return count($apiLog);
        $user = UserPlatform::find($request->decodedUserId);
        $remarks = json_decode($user->remarks,TRUE);
        $ipAddress = $remarks['ip_address'];
        $limit = $remarks['limit'];
        if($remarks){
            if($ipAddress!=0){
                if($request->ip()!=$ipAddress){
                    $response = $this->errorResponse(405, "ip adress not allowed");
                    $data = [
                        'app_id' => $appId,
                        'user_id' => $request->decodedUserId,
                        'request' => json_encode(['ipAdress'=>$request->ip(),'request'=>request()->all()]),
                        'response' => json_encode($response->original)
                    ];
                    $this->__saveLog($data);
                    return $response;
                }
            }
            if($user->email_verified_at==null){
                $response = $this->errorResponse(401, "access denied, please contact admin");
                $data = [
                    'app_id' => $appId,
                    'user_id' => $request->decodedUserId,
                    'request' => json_encode($request->all()),
                    'response' => json_encode($response->original)
                ];
                $this->__saveLog($data);
                return $response;
            }
            if($limit!="unlimited"){
                if(count($apiLog)>$limit){
                    $response = $this->errorResponse(null, "the limit has exceeded the maximum");
                    $data = [
                        'app_id' => $appId,
                        'user_id' => $request->decodedUserId,
                        'request' => json_encode($request->all()),
                        'response' => json_encode($response->original)
                    ];
                    $this->__saveLog($data);
                    return $response;
                }
            }
        }

        // if (!$integration = $this->_getIntegrationData($request->channel, $appId))
        //     return $this->errorResponse(null, "Plase connect your {$request->channel} channel first");

        if (!$integration = WebhookHelper::getIntegrationDataById('whatsapp', $appId, $request->integrationId)) {
            return $this->errorResponse(null, "Plase connect your {$request->channel} channel first");
        }


        // if (!$botIntegration = $this->_getIntegrationData('bot', $appId))
        //     return $this->errorResponse(null, "Plase connect your {$request->channel} channel first");

        $phoneToSend = [];
        foreach ($request->phone as $key => $singlePhone) {
            $phoneToSend[] = str_replace("+", "", $singlePhone);
        }
        foreach ($phoneToSend as $key => $phone) {

            // receiver and register user

            // $user = User::where([['channel_user_id', '=', $phone], ['app_id', '=', $appId]])->first();
            // if (!$user) {
            //     $user = $this->_registerUser($phone, $appId);

            // } else {
                  // $chatRoom = $user->rooms->first();
                // // sender
                // $decodedToken = PassportToken::dirtyDecode($request->header('token'));
                // if (!isset($decodedToken['user_id']))
                //     return $this->errorResponse(null, 'token not valid');

                // $participable = UserPlatform::where('id', $decodedToken['user_id'])->firstOrFail();

                // // participate sender to room
                // ChatHelper::participateUser($participable, $chatRoom->id);

                // // set content
                // $message = [
                //     'text' => "send $request->templateName template",
                //     'type' => "text"

                // ];
                // $content[] = $this->_setMessageType($message);

                // // create message
                // $newMessage = ChatHelper::createMessage(['content' => $content, 'user' => $participable, 'room' => $chatRoom]);
            // }


            // if request has story id, start converstiation
            if (isset($request->storyId)) {
                $botId = $botIntegration->integration_data['botId'];
                $nlpResponse = $this->_startConversation($request->storyId, $botId, $user->id, $appId);
                $firstResponse = $nlpResponse->response[0]->text;
            }
        }
        $data = [
            'integration' => $integration,
            'user_id' => $request->decodedUserId,
        ];
        switch ($integration->integration_data['apiService']) {
            // case 'infobip':
            //     $infobipApiController = new InfobipApiController($data);
            //     $response = $infobipApiController->sendHsm($newMessage);

            //     break;
            case 'damcorp':
                $damcorpApi = new DamcorpApiController($data);
                // if request has story id, send with botstory
                if (isset($request->storyId)) {
                    $response = $damcorpApi->sendHsm($request->templateName, $phoneToSend, ['Your virtual assistant', $firstResponse]);
                } else {
                    $response = $damcorpApi->sendHsm($request->templateName, $phoneToSend, $request->templateParams);
                }
                break;

            case 'official':
                $WaOfficialApiController = new WaOfficialApiController($data);
                // if request has story id, send with botstory
                if (isset($request->storyId)) {
                    $response = $WaOfficialApiController->sendHsm($request->templateName, $phoneToSend, ['Your virtual assistant', $firstResponse]);
                // } else if(isset($request->mediaUrl)) {
                //     $response = $WaOfficialApiController->sendHsm($request->templateName, $phoneToSend, $request->templateParams, $request->header, $request->footer, $request->button);
                } else {
                    $optional = [
                        "header" => $request->header,
                        "footer" => $request->footer,
                        "button" => $request->button,
                        "buttonParamCTA" => $request->buttonParamsCTA,
                        "buttonParamQR" => $request->buttonParamsQR
                    ];
                    $response = $WaOfficialApiController->sendHsm($request->templateName, $phoneToSend, $request->templateParams, $optional);
                }
                break;
            case 'wappin':
                $wappinApi = new WappinApiController($data);
                $response = $wappinApi->sendHsm($request->templateName, $phoneToSend, $request->templateParams);
                if ($response->success == false) {
                  return $this->errorResponse($response->error, $response->message);
                }
                break;

        }




        $broadcastMessage = BroadcastMessageHelper::createMessage([
            'data' => [
                'title' => $request->messageTitle,
                'message' => $request->messageBody,
            ],
            'channel' => $request->channel,
            'receiver_id' => $request->userId ?? null,
            'broadcastType' => $request->type,
            'broadcastCategory' => $request->category,
            'response_from_provider' => $response //cek hsm response
        ]);

        $response = $this->successResponse($broadcastMessage, 'success');
        $data = [
            'app_id' => $appId,
            'user_id' => $request->decodedUserId,
            'request' => json_encode($request->all()),
            'response' => json_encode($response->original)
        ];
        $this->__saveLog($data);
        return $response;
    }

    public function sendBroadcastHsmExternal(Request $request, $appId)
    {
        DB::beginTransaction();
        try {
            $token = BackendService::decryptText(env("ENCRYPT_KEY"), $request->token);
        } catch (\Throwable $th) {
            //throw $th;
            return $this->errorResponse(null, "Invalid Token");
        }

        $appToken = StaticToken::where('app_id', $appId)->first();
        if (!$appToken) {
            return $this->errorResponse(null, 'App not found');
        }
        if ($token == $appToken->token) {

            $validator = Validator::make($request->all(), [
                'channel' => 'required',
                'type' => 'required',
                'category' => 'required',
                'templateName' => 'required',
                // 'messageTitle' => 'required',
                'phone' => 'required|array',
                'templateParams' => 'required|array'
                // 'status' => 'required'
            ]);

            if ($validator->fails()) {
                if ($validator->errors()->first('channel')) {
                    return $this->errorResponse(null, $validator->errors()->first('channel'));
                } else if ($validator->errors()->first('type')) {
                    return $this->errorResponse(null, $validator->errors()->first('type'));
                } else if ($validator->errors()->first('category')) {
                    return $this->errorResponse(null, $validator->errors()->first('category'));
                } else if ($validator->errors()->first('templateName')) {
                    return $this->errorResponse(null, $validator->errors()->first('templateName'));
                // } else if ($validator->errors()->first('messageTitle')) {
                //     return $this->errorResponse(true, $validator->errors()->first('messageTitle'));
                } else if ($validator->errors()->first('phone')) {
                    return $this->errorResponse(true, $validator->errors()->first('phone'));
                } else if ($validator->errors()->first('templateParams')) {
                    return $this->errorResponse(true, $validator->errors()->first('templateParams'));
                }
            }


            // if (!$integration = $this->_getIntegrationData($request->channel, $appId))
            //     return $this->errorResponse(null, "Plase connect your {$request->channel} channel first");
            if (!$integration = WebhookHelper::getIntegrationDataById('whatsapp', $appId, $request->integrationId)) {
                return $this->errorResponse(null, "Plase connect your {$request->channel} channel first");
            }

            // dd($integration);

            $phoneToSend = [];
            foreach ($request->phone as $key => $singlePhone) {
                $phoneToSend[] = str_replace("+", "", $singlePhone);
            }
            foreach ($phoneToSend as $key => $phone) {

                // if request has story id, start converstiation
                if (isset($request->storyId)) {
                    $botId = $botIntegration->integration_data['botId'];
                    $nlpResponse = $this->_startConversation($request->storyId, $botId, $user->id, $appId);
                    $firstResponse = $nlpResponse->response[0]->text;
                }
            }
            $data = [
                'integration' => $integration,
                'user_id' => $request->decodedUserId,
            ];
            switch ($integration->integration_data['apiService']) {
                // case 'infobip':
                //     $infobipApiController = new InfobipApiController($data);
                //     $response = $infobipApiController->sendHsm($newMessage);

                //     break;
                case 'damcorp':
                    $damcorpApi = new DamcorpApiController($data);
                    // if request has story id, send with botstory
                    if (isset($request->storyId)) {
                        $response = $damcorpApi->sendHsm($request->templateName, $phoneToSend, ['Your virtual assistant', $firstResponse]);
                    } else {
                        $response = $damcorpApi->sendHsm($request->templateName, $phoneToSend, $request->templateParams);
                    }
                    break;

                case 'official':
                    $WaOfficialApiController = new WaOfficialApiController($data);
                    // if request has story id, send with botstory
                    if (isset($request->storyId)) {
                        $response = $WaOfficialApiController->sendHsm($request->templateName, $phoneToSend, ['Your virtual assistant', $firstResponse]);
                    } else {
                        $response = $WaOfficialApiController->sendHsm($request->templateName, $phoneToSend, $request->templateParams);
                    }
                    break;

            }


            // $broadcastMessage = BroadcastMessageHelper::createMessage([
            //     'data' => [
            //         'title' => $request->messageTitle,
            //         'message' => $request->messageBody,
            //     ],
            //     'channel' => $request->channel,
            //     'receiver_id' => $request->userId ?? null,
            //     'broadcastType' => $request->type,
            //     'broadcastCategory' => $request->category,
            //     'response_from_provider' => $response //cek hsm response
            // ]);
            // $response = $this->successResponse($broadcastMessage, 'success');
            // $data = [
            //     'app_id' => $appId,
            //     'user_id' => $request->decodedUserId,
            //     'request' => json_encode($request->all()),
            //     'response' => json_encode($response->original)
            // ];
            // $this->__saveLog($data);
            DB::commit();
            return $this->successResponse($response, 'success');
            // return $this->successResponse(null, 'success');
        } else {
            DB::rollback();
            return $this->errorResponse(null, 'Invalid token');
        }


    }

    public function _registerUser($phone, $appId)
    {

        $pictureUrl = env("APP_URL") . "/images/pictures/no_avatar.jpg";
        $email = $phone . "@whatsapp.com";

        $fields = [
            'app_id' => $appId,
            'channel_user_id' => $phone,
            'name' => $phone,
            'nickname' => $phone,
            'password' => bcrypt("$appId:$email"),
            'picture' => $pictureUrl,
            'email' => $email,
            'phone' => $phone,
        ];
        $user = $this->channel->users()->firstOrCreate(
            ['channel_user_id' => $phone, 'app_id' => $appId],
            $fields
        );
        return $user;
    }


    public function _getIntegrationData($channelName, $appId)
    {
        $integration = Integration::where([['app_id', "=", $appId], ['status', "=", true]])
            ->whereHas('channel', function ($q) use ($channelName) {
                $q->where('name', $channelName);
            })
            ->first();
        return $integration;
    }

    public function _getIntegrationDataById($channelName, $appId, $id)
    {
      $integration = Integration::where([['id', '=', $id], ['app_id', "=", $appId], ['status', "=", true]])
        ->whereHas('channel', function ($q) use ($channelName) {
            $q->where('name', $channelName);
        })
        ->orderBy('id', 'ASC')
        ->first();
      return $integration;
    }

    public function _setMessageType($data)
    {
        $lennaMessageType = new LennaMessageType;
        switch ($data['type']) {
            case 'text':
                $output = $lennaMessageType->text([
                    'text' => $data['text']
                ]);

                break;
            case 'file':
                $output = $lennaMessageType->file([
                    'fileUrl' => $data['fileUrl'],
                    'fileName' => $data['fileName'],

                ]);
                break;
            case 'image':
                $output = $lennaMessageType->image([
                    'previewImageUrl' => $data['previewImageUrl']
                ]);
                break;
            case 'audio':
                $output = $lennaMessageType->audio([
                    'originalContentUrl' => $data['originalContentUrl']
                ]);
                break;
            case 'video':
                $output = $lennaMessageType->video([
                    'originalContentUrl' => $data['originalContentUrl'],
                    'previewImageUrl' => $data['previewImageUrl']
                ]);
                break;
            case 'confirm':
                $output = $lennaMessageType->confirm([
                    'text' => $data['text'],
                    'actions' => $data['actions'],
                ]);
                break;
            case 'html':
                $output = $lennaMessageType->html([
                    'content' => $data['html']
                ]);
                break;
            case 'grid':
                $output = $lennaMessageType->grid([
                    'title' => $data['title'],
                    'subTitle' => $data['subTitle'],
                    'imageUrl' => $data['imageUrl'],
                    'columns' => $data['columns']
                ]);
                break;
            case 'transaction':
                $output = $lennaMessageType->summary([
                    'title' => $data['title'] ?? '',
                    'subType' => $data['subType'] ?? '',
                    'imageUrl' => $data['imageUrl'] ?? '',
                    'columns' => $data['columns'] ?? $data['elements']
                ]);
                break;
            case 'summary':
                $output = $lennaMessageType->summary([
                    'title' => $data['title'] ?? '',
                    'subType' => $data['subType'] ?? '',
                    'imageUrl' => $data['imageUrl'] ?? '',
                    'columns' => $data['columns'] ?? $data['elements']
                ]);
                break;
            case 'carousel':
                $output = $lennaMessageType->carousel([
                    'imageAspectRatio' => $data['imageAspectRatio'],
                    'imageSize' => $data['imageSize'],
                    'columns' => $data['columns'],
                ]);
                break;
            case 'button':
                $output = $lennaMessageType->button([
                    'text' => $data['text'],
                    'actions' => $data['actions'],
                ]);
                break;
            case 'weather':
                $output = $lennaMessageType->weather([
                    'area' => $data['area'],
                    'country' => $data['country'],
                    'countryCode' => $data['countryCode'],
                    'columns' => $data['columns'],
                ]);
                break;
            default:
                $output = $data;

                break;
        }
        return $output;
    }

}

