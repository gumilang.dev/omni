<?php

namespace App\Modules\BroadcastMessage\Controllers;

use Illuminate\Http\Request;
use App\Modules\User\Models\User;
use Carbon\Carbon;
use App\Modules\BroadcastMessage\Models\BroadcastMessage;
use App\Modules\Integration\Models\Integration;
use App\Modules\BroadcastMessage\Helpers\BroadcastMessageHelper;
use App\Modules\Mobile\Services\MobileApiController;
use App\Http\Controllers\ApiController;
use App\Modules\Whatsapp\Services\InfobipApiController;
use App\Modules\Whatsapp\Services\ApiWhaApiController;
use App\Modules\Mobile\Helpers\LennaMessageType;
use App\Modules\Room\Models\Room;
use App\Modules\Log\Helpers\WebhookHelper;
use App\Modules\Chat\Helpers\ChatHelper;
use App\Modules\Bot\Services\BotService;
use Illuminate\Support\Facades\Validator;
use App\Modules\ExternalApi\Models\ExternalApi;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\RequestGuzzle;


class BroadcastMessageController extends ApiController
{

    private $chatRoom;

    public function createBroadcastMessage($request)
    {
        return BroadcastMessageHelper::createMessage([
            'data' => [
                'type' => $request->type,
                'topics' => $request->topics,
                'category' => $request->category,
                'sub_category' => $request->subCategory,
                'title' => $request->messageTitle,
                'message' => $request->messageBody,
                'iconType' => "info",
            ],
            'notification' => [
                'title' => $request->notificationTitle,
                'body' => $request->notificationBody,
            ],
            'channel' => $request->channel,
            'receiver_id' => $request->userId ?? null,
            'broadcastType' => $request->type,
            'broadcastCategory' => $request->category,
            'broadcastSubCategory' => $request->subCategory,
            'topics' => $request->topics ?? null,
            'client' => $request->client,
        ]);
    }

    public function checkIntegration($request, $appId)
    {
        if (!$integration = WebhookHelper::getIntegrationData($request->channel, $appId))
            return $this->errorResponse(null, 'integration not active');

        return $integration;

    }
    public function callWhatsappApi($data, $message, $userId)
    {
        $newMessage = $this->_createNewMessage($message, $userId, $data['chatRoom']);
        switch ($data['integration']->integration_data['apiService']) {
            case 'infobip':
                $infobipApiController = new InfobipApiController($data);
                if ($data['messageType'] == 'template') {
                    $response = $infobipApiController->sendTemplateMessage($newMessage, $data['template']);

                } else {
                    $response = $infobipApiController->sendAdvanceMessage($newMessage);

                }

                break;
            case 'apiwha':
                $ApiWhaApiController = new ApiWhaApiController($data);
                $response = $ApiWhaApiController->sendMessage($newMessage);
                if (!$response->success) {
                    return $this->errorResponse(null, $response->description);
                }
                break;
        }
        return $response;
    }
    public function sendBroadcastWhatsapp(Request $request, $appId)
    {
        $integration = $this->checkIntegration($request, $appId);
        $newBroadcastMessage = $this->createBroadcastMessage($request);

        if ($request->broadcastType == 'multiple') {
            $multipleUser = explode(PHP_EOL, $request->multipleUser);

            foreach ($multipleUser as $key => $phone) {
                $user = User::where([['channel_user_id', "=", $phone], ['app_id', "=", $appId]])->first();

                // register user if not registered
                if (!$user) {
                    $user = $this->registerUser($whatsappData, $appId);
                }
                $botService = new BotService(['appId' => $appId]);
                $botResponse = $botService->replyMessage([
                    'userId' => $user->id,
                    'text' => "kamu siapa",
                    'room' => $user->rooms->first(),
                    'lat' => 0,
                    'lon' => 0,
                    'channel' => 'whatsapp',
                    'is_broadcast' => true,

                    // "headers" => ["Authorization" => "$backendResponse->token_type $backendResponse->access_token"]
                ]);

                if ($user) {
                    $data = [
                        'chatRoom' => $user->rooms->first(),
                        'integration' => $integration,
                        'messageType' => $request->whatsapp['messageType'],
                        'template' => [
                            'templateName' => $request->whatsapp['templateName'],
                            'templateData' => [],
                        ]
                    ];
                    $this->callWhatsappApi($data, $request->messageBody, $user->id);
                }

            }
        } elseif ($request->broadcastType == 'single') {

            $chatRoom = Room::where('created_by', $request->userId)->first();
            $data = [
                'chatRoom' => $chatRoom,
                'integration' => $integration,
                'messageType' => $request->whatsapp['messageType'],
                'template' => [
                    'templateName' => $request->whatsapp['templateName'],
                    'templateData' => [],
                ]

            ];
            $this->callWhatsappApi($data, $request->messageBody, $request->userId);

        }

        return $this->successResponse([
            'broadcastType' => $request->type,
            'broadcastCategory' => $request->category,
            'broadcastMessage' => $newBroadcastMessage,
        ], 'broadcast sent');

    }

    public function registerUser($phone, $appId)
    {

        $pictureUrl = env("APP_URL") . "/images/pictures/no_avatar.jpg";
        $email = $phone . "@whatsapp.com";

        $fields = [
            'app_id' => $appId,
            'channel_user_id' => $phone,
            'name' => $phone,
            'nickname' => $phone,
            'password' => bcrypt("$appId:$email"),
            'picture' => $pictureUrl,
            'email' => $email,
            'phone' => $phone,
        ];
        $user = $this->channel->users()->firstOrCreate(
            ['channel_user_id' => $phone, 'app_id' => $appId],
            $fields
        );
        return $user;
    }

    public function sendBroadcastMobile(Request $request, $appId)
    {
        $integration = $this->checkIntegration($request, $appId);

        $data = [
            'integration' => $integration
        ];
        $newBroadcastMessage = $this->createBroadcastMessage($request);

        $mobileApiController = new MobileApiController($data);
        $response = $mobileApiController->sendBroadcast(
            $newBroadcastMessage
        );

        return $this->successResponse([
            'broadcastType' => $request->type,
            'broadcastCategory' => $request->category,
            'broadcastMessage' => $newBroadcastMessage,
        ], 'broadcast sent');

    }


    public function _createNewMessage($text, $userId, $chatRoom)
    {

        $participable = User::find($userId);
        // participate sender to room
        ChatHelper::participateUser($participable, $chatRoom->id);

    // set content
        $lennaMessageType = new LennaMessageType;
        $content[] = $lennaMessageType->text(['text' => $text]);

    // create message
        $newMessage = ChatHelper::createMessage(['content' => $content, 'user' => $participable, 'room' => $chatRoom]);

        return $newMessage;
    }


		public function getQrCode(){
			$client = new Client();

			$apiUrl = ExternalApi::where('provider','wanotif-auth')->first();
			$apiUrlAuthWanotif = $apiUrl->base_url;
			$apiKey = $apiUrl->key;



			try {
					$response = $client->request('GET', $apiUrlAuthWanotif,
					[
							'headers' => [
									'Apikey' => $apiKey
									]
									]);
			}catch (\Exception $e) {
					$resp = json_decode($e->getResponse()->getBody());
					if($resp->wanotif->status == 'API Key Not Valid.'){
							return response()->json([
									'status' => 'Failed',
									'code' => 400,
									'data' => [
											'message' => 'Api Key Not Valid'
									]
							]);
					}
			}


			$responseWa =  json_decode($response->getBody());
					if($responseWa->wanotif->status == 'connected'){
							return response()->json([
									'status' => 'Success',
									'code' => 200,
									'data' => [
											'message' => 'Connected'
											]
							]);
					} else if ($responseWa->wanotif->status == 'loading') {
							return response()->json([
									'status' => 'Success',
									'code' => 200,
									'data' => [
											'message' => 'Loading'
											]
							]);
					} else {
							return response()->json([
									'status' => 'Success',
									'code' => 200,
									'data' => [
															'qrcode' => $responseWa->wanotif->qrcode
													]
							]);
					}

	}

	public function whatsappSend(Request $request){
			$client = New Client();

			$validator = Validator::make($request->all(), [
					'number' => 'required|min:5',
					'message' => 'required'
			]);


			//check validation
			if($validator->fails()){
					if($validator->errors()->first('number')){
							return response()->json([
									'status' => 'Failed',
									'code' => 400,
									'data' => [
											'message' => $validator->errors()->first('number')
									]
							]);
					}else if ($validator->errors()->first('message')){
							return response()->json([
									'status' => 'Failed',
									'code' => 400,
									'data' => [
											'message' => $validator->errors()->first('message')
									]
							]);
					}
			}



			$broadcastNumber = $request->number;
			$broadcastMessage = $request->message;
			$explodeBroadcastNumber = explode(',', $broadcastNumber);
			$substrExplode = substr_replace($explodeBroadcastNumber, '62', 0,1);

			$apiUrl = ExternalApi::where('provider','wanotif-send')->first();
			$apiUrlSend = $apiUrl->base_url;
			$apiKey = $apiUrl->key;



			foreach($substrExplode as $phone){

					$response[] = $client->requestAsync('POST', $apiUrlSend, [
							'form_params' => [
									'Apikey' => $apiKey,
									'Phone' => $phone,
									'Message' => $broadcastMessage
							]
					]);

			}



			try {
					$result = Promise\unwrap($response);
			} catch (RequestException $e) {
					$resp = json_decode($e->getResponse()->getBody());
					if($resp->wanotif->status == 'API Key Not Valid.') {
							return response()->json([
									'status' => 'Failed',
									'code' => 400,
									'data' => [
											'message' => 'Api Key Not Valid.'
									]
							]);
					} else if($resp->wanotif->status == 'failed') {
							return response()->json([
									'status' => 'Failed',
									'code' => 400,
									'data' => [
											'message' => 'Failed'
									]
							]);
					}
			}

			$responseWa = json_decode($result[0]->getBody());

			 if($responseWa->wanotif->status == 'sent'){
					 return response()->json([
							'status' => 'Success',
							'code' => 200,
							'data' => [
									'message' => 'Message sent'
							]
					 ]);

			//  }else if ($responseWa->wanotif->status == 'API Key Not Valid.'){
			//     return response()->json([
			//         'status' => 'Api key not valid'
			//     ]);
			 } else if ($responseWa->wanotif->status == 'failed') {
					 return response()->json([
							 'status' => 'Failed',
							 'code' => 400,
							 'data' => [
									 'message' => 'Failed'
							 ]
							 ]);
					} else {
							return response()->json([
									'status' => 'Failed',
									'code' => 400,
									'data' => [
											'message' => 'Failed'
									]
									]);
					}


	}

	public function whatsappImageSend(Request $request){
			$client = New Client();
			$apikey = 'wntSVqQ9MAotFG1pDLDNzA6gZN6dcmFY';

			$validator = Validator::make($request->all(), [
					'number' => 'required|min:5',
					'message' => 'required',
					'imagesLink' => 'required',
					'filename' => 'required'
			]);

			if($validator->fails()){
					if($validator->errors()->first('number')){
							return response()->json([
									'status' => 'Failed',
									'code' => 400,
									'data' => [
											'message' => $validator->errors()->first('number')
									]
							]);
					} else if ($validator->errors()->first('imagesLink')){
							return response()->json([
									'status' => 'Failed',
									'code' => 400,
									'data' => [
											'message' => $validator->errors()->first('imagesLink')
									]
							]);
					} else if ($validator->errors()->first('message')){
							return response()->json([
									'status' => 'Failed',
									'code' => 400,
									'data' => [
											'message' => $validator->errors()->first('message')
									]
							]);
					} else if ($validator->errors()->first('filename')){
							return response()->json([
									'status' => 'Failed',
									'code' => 400,
									'data' => [
											'message' => $validator->errors()->first('filename')
									]
							]);
					}
			}


			$broadcastNumber = $request->number;
			$broadcastImageLink = $request->imagesLink;
			$broadcastMessage = $request->message;
			$broadcastImageFileName = $request->filename;

			$explodeBroadcastNumber = explode(',', $broadcastNumber);
			$substrExplode = substr_replace($explodeBroadcastNumber, '62', 0,1);

			$apiUrl = ExternalApi::where('provider','wanotif-send-images')->first();
			$apiUrlSend = $apiUrl->base_url;

			foreach($substrExplode as $phone){

					$response[] = $client->requestAsync('POST', $apiUrlSend, [
							'form_params' => [
									'Apikey' => $apikey,
									'Phone' => $phone,
									'Body' => $broadcastImageLink,
									'Caption' => $broadcastMessage,
									'Filename' => $broadcastImageFileName
							]
					]);

			}

			// use try and catch to get 400 response

			try {
					$result = Promise\unwrap($response);
			} catch (RequestException $e) {
					$resp = json_decode($e->getResponse()->getBody());
					if($resp->wanotif->status == 'API Key Not Valid.') {
							return response()->json([
									'status' => 'Failed',
									'code' => 400,
									'data' => [
											'message' => 'Api Key Not Valid.'
									]
							]);
					} else if ($resp->wanotif->status == 'Server Not Found.') {
							return response()->json([
									'status' => 'Failed',
									'code' => 400,
									'data' => [
											'message' => 'Server images not found.'
									]
							]);
					} else if ($resp->wanotif->status == 'failed') {
							return response()->json([
									'status' => 'Failed',
									'code' => 400,
									'data' => [
											'message' => 'Failed'
									]
							]);
					}
			}

			$responseWa = json_decode($result[0]->getBody());

			 if($responseWa->wanotif->status == 'sent'){
					 return response()->json([
							'status' => 'Success',
							'code' => 200,
							'data' => [
									'message' => 'Message sent'
							]
					 ]);
			} else if ($responseWa->wanotif->status == 'failed') {
					return response()->json([
							'status' => 'Failed',
							'code' => 400,
							'data' => [
									'message' => 'Failed'
							]
					]);
			} else {
					return response()->json([
							'status' => 'Failed',
							'code' => 400,
							'data' => [
									'message' => 'Failed'
							]
							]);
			}

	}





}
