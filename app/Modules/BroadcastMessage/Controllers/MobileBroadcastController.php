<?php

namespace App\Modules\BroadcastMessage\Controllers;

use Illuminate\Http\Request;
use App\Modules\User\Models\User;
use Carbon\Carbon;
use App\Modules\BroadcastMessage\Models\BroadcastMessage;
use App\Modules\Integration\Models\Integration;
use App\Modules\BroadcastMessage\Helpers\BroadcastMessageHelper;
use App\Modules\Mobile\Services\MobileApiController;
use App\Http\Controllers\ApiController;
use App\Modules\Whatsapp\Services\InfobipApiController;
use App\Modules\Whatsapp\Services\ApiWhaApiController;
use App\Modules\Mobile\Helpers\LennaMessageType;
use App\Modules\Room\Models\Room;
use App\Modules\Log\Helpers\WebhookHelper;
use App\Modules\Chat\Helpers\ChatHelper;
use App\Modules\Bot\Services\BotService;
use App\Events\SendBroadcastMobile;


class MobileBroadcastController extends ApiController
{

    private $chatRoom;

    public function createBroadcastMessage($request)
    {
        if ($request->user_email) {
            $userId = null;
        } else {
            $userId = $request->userId;
        }

        return BroadcastMessageHelper::createMessage([
            'data' => [
                'type' => $request->type,
                'topics' => $request->topics ?? null,
                'category' => $request->category,
                'sub_category' => $request->subCategory,
                'title' => $request->messageTitle,
                'message' => $request->messageBody,
                'iconType' => $request->iconType ?? "info",
            ],
            'notification' => [
                'title' => $request->notificationTitle,
                'body' => $request->notificationBody,
            ],
            'channel' => $request->channel,
            'receiver_id' => $userId ?? null,
            'user_email' => $request->user_email ?? null,
            'broadcastType' => $request->type,
            'broadcastCategory' => $request->category,
            'broadcastSubCategory' => $request->subCategory,
            'topics' => $request->topics ?? null,
            'client' => $request->client,
        ]);
    }

    public function createBroadcastMessageSegment($request,$data)
    {
        $userId = $data['userId'];

        return BroadcastMessageHelper::createMessage([
            'data' => [
                'type' => $request->type,
                'topics' => $request->topics ?? null,
                'category' => $request->category,
                'sub_category' => $request->subCategory,
                'title' => $request->messageTitle,
                'message' => $request->messageBody,
                'iconType' => $request->iconType ?? "info",
            ],
            'notification' => [
                'title' => $request->notificationTitle,
                'body' => $request->notificationBody,
            ],
            'channel' => $request->channel,
            'receiver_id' => $userId ?? null,
            'user_email' => null,
            'broadcastType' => $request->type,
            'broadcastCategory' => $request->category,
            'broadcastSubCategory' => $request->subCategory,
            'topics' => $request->topics ?? null,
            'client' => $data['client'],
        ]);
    }

    public function createBroadcastMessageMultiple($request,$userEmail,$appId)
    {
        $user = User::where(['app_id'=>$appId,'email'=>$userEmail])->first();
        return BroadcastMessageHelper::createMessage([
            'data' => [
                'type' => $request->type,
                'topics' => $request->topics ?? null,
                'category' => $request->category,
                'sub_category' => $request->subCategory,
                'title' => $request->messageTitle,
                'message' => $request->messageBody,
                'iconType' => $request->iconType ?? "info",
            ],
            'notification' => [
                'title' => $request->notificationTitle,
                'body' => $request->notificationBody,
            ],
            'channel' => $request->channel,
            'receiver_id' => $user->id ?? null,
            'user_email' => $user->email ?? null,
            'broadcastType' => $request->type,
            'broadcastCategory' => $request->category,
            'broadcastSubCategory' => $request->subCategory,
            'topics' => $request->topics ?? null,
            'client' => $user->client ?? null,
            'fcm_token' => $user->fcm_token ?? null
        ]);
    }

    public function checkIntegration($request, $appId)
    {
        if (!$integration = WebhookHelper::getIntegrationData($request->channel, $appId))
            return $this->errorResponse(null, 'integration not active');

        return $integration;

    }

    public function sendBroadcast(Request $request, $appId)
    {
        // return $request;
        $integration = $this->checkIntegration($request, $appId);

        $data = [
            'integration' => $integration
        ];

        if ($request->type == 'segment') {
           return $this->sendBroadcastSegment($request,$data,$appId);
        }
        if ($request->type == 'multiple') {
            return $this->sendBroadcastMultiple($request,$data,$appId);
        }

        $newBroadcastMessage = $this->createBroadcastMessage($request);
        // dd($newBroadcastMessage);

        $mobileApiController = new MobileApiController($data);
        $response = $mobileApiController->sendBroadcast(
            $newBroadcastMessage,$appId
        );
        // dd($response);

        $newBroadcastMessage["integration_id"] = $data["integration"]->id;
        $newBroadcastMessage["app_id"] = $appId;

        if ($response == "Email not Found") {
            event(new SendBroadcastMobile("failed",$newBroadcastMessage,$request->decodedUserId));
            return $this->errorResponse($response,$response);
        } else if ($response == "400 Bad Request") {
            event(new SendBroadcastMobile("failed",$newBroadcastMessage,$request->decodedUserId));
            return $this->errorResponse($response,'Expired Token');
        } else if ($response->failure == 1) {
            // return $this->errorResponse($response, $response->results[0]->error);
            return $this->successResponse([
                'broadcastType' => $request->type,
                'broadcastCategory' => $request->category,
                'broadcastMessage' => $newBroadcastMessage,
                // 'response' => $response
            ], $response->results[0]->error);
            event(new SendBroadcastMobile("failed",$newBroadcastMessage,$request->decodedUserId));
        } elseif ($response->success == 1) {
            event(new SendBroadcastMobile("success",$newBroadcastMessage,$request->decodedUserId));
            return $this->successResponse([
                'broadcastType' => $request->type,
                'broadcastCategory' => $request->category,
                'broadcastMessage' => $newBroadcastMessage,
                // 'response' => $response
            ], 'broadcast sent');
        }

    }

    public function sendBroadcastSegment($request,$data,$appId)
    {
        foreach ($request->segmentUser as $key => $value) {
            $mobileApiController = new MobileApiController($data);
            $newBroadcastMessage[$key] = $this->createBroadcastMessageSegment($request,$value);

            $newBroadcastMessage[$key]["app_id"] = $appId;
            $newBroadcastMessage[$key]["integration_id"] = $data["integration"]->id;

            $responses[$key] = $mobileApiController->sendBroadcast($newBroadcastMessage[$key], $appId);
            if (!isset($responses[$key])){
                $userEmail[$key] = $value['user_email'];
                $msg[$key] = 'Something went wrong';
                $status[$key] = 'failed';
                event(new SendBroadcastMobile("failed",$newBroadcastMessage[$key],$request->decodedUserId));
            } else if (isset($responses[$key]->success) &&  $responses[$key]->success == 1) {
                $userEmail[$key] = $value['user_email'];
                $msg[$key] = 'sent';
                $status[$key] = 'success';
                event(new SendBroadcastMobile("failed",$newBroadcastMessage[$key],$request->decodedUserId));
            } else if  (isset($responses[$key]->failure) && $responses[$key]->failure == 1){
                $userEmail[$key] = $value['user_email'];
                $msg[$key] = $responses[$key]->results[0]->error;
                $status[$key] = 'failed';
                event(new SendBroadcastMobile("failed",$newBroadcastMessage[$key],$request->decodedUserId));
            } else if ($responses[$key] == 'Email not Found') {
                $userEmail[$key] = $value['user_email'];
                $msg[$key] = 'Email not Found';
                $status[$key] = 'failed';
                event(new SendBroadcastMobile("failed",$newBroadcastMessage[$key],$request->decodedUserId));
            } else if ($responses[$key] == '400 Bad Request') {
                $userEmail[$key] = $value['user_email'];
                $msg[$key] = 'Expired Token';
                $status[$key] = 'failed';
                event(new SendBroadcastMobile("failed",$newBroadcastMessage[$key],$request->decodedUserId));
            } else {
                $userEmail[$key] = $value['user_email'];
                $msg[$key] = 'Error';
                $status[$key] = 'failed';
                event(new SendBroadcastMobile("failed",$newBroadcastMessage[$key],$request->decodedUserId));
            }
            $resp[$key] = [
                'user_email' => $value['user_email'],
                'status' => $status[$key],
                'msg' => $msg[$key]
            ];
        }
        return $this->successResponse($resp);
    }

    public function sendBroadcastMultiple($request,$data,$appId)
    {
        foreach ($request->multipleUser as $key => $value) {
            $mobileApiController = new MobileApiController($data);
            $newBroadcastMessage[$key] = $this->createBroadcastMessageMultiple($request,$value,$appId);

            $newBroadcastMessage[$key]["app_id"] = $appId;
            $newBroadcastMessage[$key]["integration_id"] = $data["integration"]->id;

            $responses[$key] = $mobileApiController->sendBroadcast($newBroadcastMessage[$key], $appId);
            // dd($responses[$key]->success);
            if (!isset($responses[$key])){
                $userEmail[$key] = $value;
                $msg[$key] = 'Something went wrong';
                $status[$key] = 'failed';
                event(new SendBroadcastMobile("failed",$newBroadcastMessage[$key],$request->decodedUserId));
            } else if (isset($responses[$key]->success) &&  $responses[$key]->success == 1) {
                $userEmail[$key] = $value;
                $msg[$key] = 'sent';
                $status[$key] = 'success';
                event(new SendBroadcastMobile("success",$newBroadcastMessage[$key],$request->decodedUserId));
            } else if  (isset($responses[$key]->failure) && $responses[$key]->failure == 1){
                $userEmail[$key] = $value;
                $msg[$key] = $responses[$key]->results[0]->error;
                $status[$key] = 'failed';
                event(new SendBroadcastMobile("failed",$newBroadcastMessage[$key],$request->decodedUserId));
            } else if ($responses[$key] == 'Email not Found') {
                $userEmail[$key] = $value;
                $msg[$key] = 'Email not Found';
                $status[$key] = 'failed';
                event(new SendBroadcastMobile("failed",$newBroadcastMessage[$key],$request->decodedUserId));
            } else if ($responses[$key] == '400 Bad Request') {
                $userEmail[$key] = $value;
                $msg[$key] = 'Expired Token';
                $status[$key] = 'failed';
                event(new SendBroadcastMobile("failed",$newBroadcastMessage[$key],$request->decodedUserId));
            } else {
                $userEmail[$key] = $value;
                $msg[$key] = 'Error';
                $status[$key] = 'failed';
                event(new SendBroadcastMobile("failed",$newBroadcastMessage[$key],$request->decodedUserId));
            }
            $resp[$key] = [
                'user_email' => $value,
                'status' => $status[$key],
                'msg' => $msg[$key]
            ];
        }
        // dd($responses);
        // dd($newBroadcastMessage);
        return $this->successResponse($resp);
    }

    public function uploadImage(Request $request)
    {
        if($request->hasFile("file")) {
          $name = time()  . $request->file('file')->getClientOriginalName();
          $extension = $request->file('file')->getClientOriginalExtension();
          $allowedExension = ["jpeg","jpg","gif","png","webm","mpg","ogg","mp4","avi","mov","doc","dot","wbk","docx","docm","dotx","docb","xls","xlt","xlm","xlsx","ppt","pot","pps","pdf","PNG","JPG","JPEG"];
          if(in_array($extension,$allowedExension)) {
            $request->file->move(public_path('/upload/mobile'), $name);
            return response()->json(['image_url' => env("APP_URL") . "/upload/mobile/" . $name], 200);
          }else{
            return response()->json(["success" => false, "error" => true,"message" => "extension not allowed"],400);
          }
        }else{
          return response()->json(["success" => false, "error" => true,"message" => "image cannot be null"],400);
        }
    }

    public function sendBroadcastOnly(Request $request, $appId)
    {
      $broadcastMessage = BroadcastMessageHelper::createMessage([
        'data' => [
            'type' => "single",
            'topics' => null,
            'category' => "chat",
            'sub_category' => "message",
            'title' => "New Message",
            'message' => $request->content,
            'iconType' => "info",
        ],
        'notification' => [
            'title' => "New Message",
            'body' => $request->content,
        ],
        'channel' => "mobile",
        'receiver_id' => $request->created_by['id'],
        'broadcastType' => "single",
        'broadcastCategory' => "chat",
        'broadcastSubCategory' => "message",
        'topics' => null,
        'client' => 'android',
      ]);

      // return $request;
      $integration = $this->checkIntegration($request, $appId);

      $data = [
          'integration' => $integration
      ];

      $mobileApiController = new MobileApiController($data);

      $response = $mobileApiController->sendBroadcast($broadcastMessage, ['sendTo' => $request->created_by['fcm_token']]);

      return $this->successResponse([
        'broadcastType' => 'single',
        'broadcastCategory' => 'chat',
        'broadcastMessage' => $broadcastMessage,
      ], 'broadcast sent');
    }
}
