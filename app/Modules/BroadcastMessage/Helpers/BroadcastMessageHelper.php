<?php

namespace App\Modules\BroadcastMessage\Helpers;

use Carbon\Carbon;
use App\Modules\Room\Models\Room;
use App\Modules\Message\Models\Message;
use App\Modules\BroadcastMessage\Models\BroadcastMessage;
use App\Modules\Channel\Models\Channel;

class BroadcastMessageHelper
{

    public static function createMessage($data = [])
    {
        $channel = Channel::where(['name' => $data['channel']])->first();


        $fields = [
            'channel_data' => $data['channelData'] ?? null,
            'notification' => $data['notification'] ?? [],
            'data' => $data['data'] ?? [],
            'type' => $data['broadcastType'],
            'category' => $data['broadcastCategory'] ?? null,
            'sub_category' => $data['broadcastSubCategory'] ?? null,
            'topics' => $data['topics'] ?? null,
            'receiver_id' => $data['receiver_id'],
            'client' => $data['client'] ?? null,
            'fcm_token' => $data['fcm_token'] ?? null
        ];
        $message = new BroadcastMessage($fields);
        // $channel->broadcastMessages()->save($message);

        $response = [
            'channel_data' => $data['channelData'] ?? null,
            'notification' => $data['notification'] ?? [],
            'data' => $data['data'] ?? [],
            'type' => $data['broadcastType'],
            'category' => $data['broadcastCategory'] ?? null,
            'sub_category' => $data['broadcastSubCategory'] ?? null,
            'topics' => $data['topics'] ?? null,
            'receiver_id' => $data['receiver_id'],
            'user_email' =>$data['user_email'] ?? null,
            'client' => $data['client'] ?? null,
            'fcm_token' => $data['fcm_token'] ?? null,
            'response_from_provider' => $data['response_from_provider'] ?? [] //test show response
        ];
        return $response;
    }


}