<?php namespace App\Modules\BroadcastMessage\Services;

use App\Exceptions\BadRequestParameterException;
use App\Exceptions\RecordNotExpectedException;
use App\Shared\Common\BaseService;
use App\Modules\BroadcastMessage\Repositories\BroadcastMessageRepository;
use PDF;
use Carbon\Carbon;

class BroadcastMessageServiceImpl extends BaseService {

  private $broadcastMessageRepository;

  public function __construct(BroadcastMessageRepository $broadcastMessageRepository)
  {
    $this->broadcastMessageRepository = $broadcastMessageRepository;
  }

  public function reporting($app_id,$status,$start, $end,$paginated,$channel) {
    if(is_null($app_id)) {
      return new BadRequestParameterException("app id cannot be null");
    }
    if($paginated == "true") {
      return $this->broadcastMessageRepository->getBy($app_id,function ($query) use($status, $start, $end, $channel) {
        $query->when($status, function ($fn) use($status) {
          $fn->where("ob.status","ilike","%".$status."%")->orWhere("au.name","ilike","%".$status."%");
        });

        $query->when($channel == "mobile", function($fn) {
          $fn->whereIn("ob.client",["android","ios","mobile"]);
        });

        $query->when($channel == "whatsapp", function($fn) {
          $fn->whereIn("ob.client",["whatsapp"]);
        });

        $query->when(!is_null($start) && $start !== "" && $end !== "", function ($fn) use($start, $end) {
          $fn->whereDate("ob.created_at",">=",$start);
          $fn->whereDate("ob.created_at", "<=", $end);
        });
      });
    }else {
      return $this->broadcastMessageRepository->getByNotPaginated($app_id,function ($query) use($status, $start, $end,$channel) {
        $query->when($status , function ($fn) use($status) {
          $fn->where("ob.status","ilike","%".$status."%")->orWhere("au.name","ilike","%".$status."%");
        });

        $query->when($channel == "mobile", function($fn) {
          $fn->whereIn("ob.client",["android","ios","mobile"]);
        });

        $query->when($channel == "whatsapp", function($fn) {
          $fn->whereIn("ob.client",["whatsapp"]);
        });

        $query->when(!is_null($start) && $start !== "" && $end !== "", function ($fn) use($start, $end) {
          $fn->whereDate("ob.created_at",">=",$start);
          $fn->whereDate("ob.created_at", "<=", $end);
        });
      });
    }

  }

  public function recordLog($status, $broadcasted_data)
  {

    $statuses = ["success","failed"];

    if(is_null($broadcasted_data) || is_null($status)) {
      return new BadRequestParameterException("field status and broadcasted data cannot be null");
    }

    if(!in_array($status, $statuses)) {
      return new RecordNotExpectedException("failed create log, cause given status is not available");
    }

    $this->broadcastMessageRepository->insert(array_merge($broadcasted_data, ["status" => $status]));

    return true;
  }

  public function createWhatsappBroadcastFormat($type,$user_id, $data, $status,$app_id, $integration_id,$send_by)
  {
    return array(
      "channel_id" => 7,
      "type" => $type,
      "client" => "whatsapp",
      "receiver_id" => $user_id,
      "notification" => json_encode($data),
      "data" => json_encode($data),
      "status" => $status,
      "app_id" => $app_id,
      "integration_id" => $integration_id,
      "created_at" => Carbon::now()->toDateTimeString(),
      "updated_at" => Carbon::now()->toDateTimeString(),
      "send_by" => $send_by
    );
  }

  public function getStatus($response_from_provider, $keyword) {
    foreach($response_from_provider as $key => $each_response) {
      if($each_response->to == $keyword) {
        return (object)["status" => $each_response->status == "sent" ? "success" : "false"];
        break 1;
      }
    }
  }

  public function recordLogWhatsapp($type, $user, $response_from_provider,$data, $app_id, $integration_id,$send_by) {

    $inserted_data = [];

    if(is_null($type) || is_null($user) || is_null($response_from_provider) || is_null($app_id) || is_null($integration_id)) {
       return new BadRequestParameterException("field type, user and response_from_provider cannot be null");
    }

    if($type == "single") {

      if(count($response_from_provider) < 1) {
        return new RecordNotExpectedException("respone from provider field is required");
      }

      $status = $response_from_provider[0]->status == "sent" ? "success" : "false";
      $inserted_data[] = $this->createWhatsappBroadcastFormat($type,$user["id"],$data,$status,$app_id,$integration_id ,$send_by);
    }

    if($type == "all") {
      foreach ($user as $key => $each_user) {
        $inserted_data[] = $this->createWhatsappBroadcastFormat($type,$each_user['id'],$data, $this->getStatus($response_from_provider, $each_user['number'])->status ,$app_id,$integration_id,$send_by);
      }
    }

    if($type == "multiple") {
      foreach ($user as $key => $each_user) {
        $inserted_data[] = $this->createWhatsappBroadcastFormat($type,$each_user['id'],$data,$this->getStatus($response_from_provider, $each_user['number'])->status,$app_id,$integration_id,$send_by);
      }
    }

    if($type == "segment") {
      foreach ($user as $key => $each_user) {
        $inserted_data[] = $this->createWhatsappBroadcastFormat($type,$each_user['id'],$data,$this->getStatus($response_from_provider, $each_user['phone'])->status,$app_id,$integration_id,$send_by);
      }
    }

    $this->broadcastMessageRepository->insert($inserted_data);

    return true;



  }

}
