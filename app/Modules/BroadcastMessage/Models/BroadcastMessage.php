<?php

namespace App\Modules\BroadcastMessage\Models;

use Illuminate\Database\Eloquent\Model;

class BroadcastMessage extends Model
{
    protected $connection = 'pgsql';

    protected $guarded = [];

    protected $casts = [
        'content' => 'object',
        'notification' => 'object',
        'data' => 'object',
        'receiver' => 'array',
    ];
    public function channel()
    {
        return $this->belongsTo("App\Modules\Channel\Models\Channel");
    }
    public function user()
    {
        return $this->belongsTo("App\Modules\User\Models\User", 'receiver_id');
    }
}
