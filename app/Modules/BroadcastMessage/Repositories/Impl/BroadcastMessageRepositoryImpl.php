<?php namespace App\Modules\BroadcastMessage\Repositories\Impl;

use App\Shared\Repositories\AbstractRepositoryImpl;
use App\Modules\BroadcastMessage\Repositories\BroadcastMessageRepository;
use App\Modules\BroadcastMessage\Models\BroadcastMessage;
use DB;

class BroadcastMessageRepositoryImpl extends AbstractRepositoryImpl implements BroadcastMessageRepository {

  public function __construct()
  {
      $this->model(new BroadcastMessage());
  }

  public function getBy($app_id, $fn = null)
  {
    return DB::table("omnichannel.broadcast_messages as ob")
             ->leftJoin("app.users as au", function($query) {
               $query->on("au.id","=","ob.receiver_id");
             })
             ->join("auth.users as auu", function($query) {
               $query->on("auu.id","=","ob.send_by");
             })
             ->select([
               "ob.id",
               "ob.status",
               "ob.notification",
               "ob.created_at",
               "ob.type",
               "auu.email as executed_by",
               "ob.data",
                DB::raw("(
                    CASE
                      when ob.type = 'all'
                      then au.name
                      else au.name
                    END
                  ) as name")
              ])
             ->where("ob.app_id", $app_id)
             ->when(is_callable($fn), function($query) use($fn) {
               $fn($query);
             })
             ->paginate(15);
  }

  public function insert($data) {
    \Log::debug($data);
    return DB::table("omnichannel.broadcast_messages")
              ->insert($data);
  }

  public function getByNotPaginated($app_id, $fn = null) {
    return DB::table("omnichannel.broadcast_messages as ob")
             ->leftJoin("app.users as au", function($query) {
               $query->on("au.id","=","ob.receiver_id");
             })
             ->join("auth.users as auu", function($query) {
              $query->on("auu.id","=","ob.send_by");
            })
             ->select([
              "ob.id",
              "ob.status",
              "ob.notification",
              "ob.created_at",
              "ob.type",
              "auu.email as executed_by",
              "ob.data",
               DB::raw("(
                   CASE
                     when ob.type = 'all'
                     then au.name
                     else au.name
                   END
                 ) as name")
             ])
             ->where("ob.app_id", $app_id)
             ->when(is_callable($fn), function($query) use($fn) {
               $fn($query);
             })
             ->get();
  }

}
