<?php namespace App\Modules\BroadcastMessage\Repositories;

use App\Shared\Repositories\AbstractRepository;

interface BroadcastMessageRepository extends AbstractRepository{
  public function getBy($app_id,$filter);
}
