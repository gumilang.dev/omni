<?php

namespace App\Modules\Passport\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\Client;

class PassportClient extends Client
{
    protected $connection = 'auth';

}
