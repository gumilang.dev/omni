<?php

namespace App\Modules\Passport\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\PersonalAccessClient;

class PassportPersonalAccessClient extends PersonalAccessClient
{
    protected $connection = 'auth';

}
