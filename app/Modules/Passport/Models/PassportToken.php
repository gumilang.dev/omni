<?php

namespace App\Modules\Passport\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\Token;

class PassportToken extends Token
{
    protected $connection = 'auth';

}
