<?php

namespace App\Modules\Passport\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\AuthCode;

class PassportAuthCode extends AuthCode
{
    protected $connection = 'auth';
}
