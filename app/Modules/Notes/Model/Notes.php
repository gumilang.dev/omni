<?php

namespace App\Modules\Notes\Model;

use Illuminate\Database\Eloquent\Model;

class Notes extends Model
{
    protected $fillable = ["room_id","content"];
}
