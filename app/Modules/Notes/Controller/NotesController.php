<?php

namespace App\Modules\Notes\Controller;

use Illuminate\Http\Request;
use App\Services\NotesServiceImpl;
use App\Http\Controllers\ApiController;
use App\Modules\Room\Models\Room;
use App\Services\PaginatorServiceImpl;

class NotesController extends ApiController
{

    protected $service;

    public function __construct(NotesServiceImpl $service,PaginatorServiceImpl $paginator) {
        $this->service = $service;
        $this->paginator = $paginator;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$appId)
    {
        $data = [
            "room_id" => $request->input("room_id"),
            "content" => $request->input("content")
        ];
        $response = $this->service->store($data);

        return $this->successResponse($response,'Notes created');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notes  $notes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $appId, $id)
    {
        $data = [
            "room_id" => $request->input("room_id"),
            "content" => $request->input("content")
        ];
        $response = $this->service->update(['id' => $id],$data);

        return $this->successResponse($response,'Notes updated');
    }

    public function report(Request $request, $appId) {
        $sort = $request->get('sort');
        $filterText = $request->get('filterText');
        $per_page = $request->get('per_page');
        $startFilterDate = $request->get('start');
        $endFilterDate = $request->get('end');

        $room = Room::where('app_id', $appId)->with(['channel','notes'])->whereHas('users', function ($query) use ($request,$startFilterDate,$endFilterDate,$filterText) {
            $query->where('deleted_at', '=', null);
        })->where(function($q) use ($startFilterDate, $endFilterDate) {
          $q->whereHas('users', function($u) use ($startFilterDate, $endFilterDate) {
            if (!empty($startFilterDate) && !empty($endFilterDate)) {
              $u->whereDate('users.created_at', '>=', $startFilterDate)
                ->whereDate('users.created_at', '<=', $endFilterDate);
            }
          })->orWhereHas('notes', function($n) use ($startFilterDate, $endFilterDate) {
            if (!empty($startFilterDate) && !empty($endFilterDate)) {
              $n->whereDate('notes.created_at', '>=', $startFilterDate)
                ->whereDate('notes.created_at', '<=', $endFilterDate);
            }
          });
        })->orderBy("created_at","DESC")->get();


        if($filterText) {
          $room = $room->filter(function ($item) use ($filterText) {
              return false !== stristr($item->created_by["name"], $filterText) || false !== stristr($item->notes["content"], $filterText) || false !== stristr($item->channel["name"],$filterText);
          })->values();
        }

        if (!$per_page) {
            $per_page = 15;
        }

        $data =  $this->paginator->paginate($room,$per_page);
        return response()->json($data);
    }

}
