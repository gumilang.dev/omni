<?php

namespace App\Modules\Reporting\Controllers;

use Illuminate\Http\Request;
use App\Modules\Message\Models\Message;
use App\Modules\Room\Models\Room;
use App\Modules\Livechat\Models\Livechat;
use App\Http\Controllers\ApiController;
use App\Modules\User\Models\UserApp;
use App\Modules\User\Models\User;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
use App\Modules\User\Models\UserRole;
use App\Modules\User\Models\UserPlatform;
use App\Modules\Log\Models\ApiLog;
use App\Modules\TeamMember\Models\HistoryActivityUserRole;
use Hashids\Hashids;

use App\Exports\UserExport;
use App\Exports\MessageExport;
use App\Exports\RoomMsgExport;
use App\Exports\HsmExport;
use App\Exports\TagExport;
use App\Exports\NoteExport;
use App\Exports\TicketExport;
use App\Exports\TimeServedExport;
use App\Exports\LogHandleRoomExport;
use App\Exports\RoleHistoryActivityExport;
use App\Exports\AgentActivitiesExport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use App\Modules\Role\Helpers\RoleHelper;
use App\Exports\RecordBroadcastMobileExport;
use App\Exports\RecordBroadcastWhatsappExport;

class ReportingController extends ApiController
{
    public function getDataCustomer(Request $request, $appId)
    {
        $sort = $request->get('sort');
        $filterText = $request->get('filterText');
        $per_page = $request->get('per_page');
        $channel = $request->get('channel');
        $startFilterDate = $request->get('start');
        $endFilterDate = $request->get('end');

        if($channel == 'line'){
            $channel = 1;
        } else if ($channel == 'facebook'){
            $channel = 2;
        } else if ($channel == 'telegram'){
            $channel = 3;
        } else if ($channel == 'whatsapp'){
            $channel = 4;
        } else if ($channel == 'webchat'){
            $channel = 5;
        } else if ($channel == 'mobile'){
            $channel = 7;
        }

        $user = User::with('channel')->where('app_id', $appId)->orderBy('created_at', 'DESC');

        if ($sort) {
            $sort = explode('|', $sort);
            $user = $user->orderBy($sort[0], $sort[1]);
        }


        //fix search / filter channel
        if ($filterText) {
            $user = $user->where(function ($query) use ($filterText) {
                $query->where('name', 'ILIKE', '%' . $filterText . '%')->orWhere('email', 'ILIKE', '%' . $filterText . '%')->orWhere('phone', 'ILIKE', '%' . $filterText . '%');
            });
        }

        if ($channel) {
            $user = $user->where(function ($q) use ($channel) {
                $q->where('channel_id', $channel);
            });
        }

        if ($startFilterDate && $endFilterDate) {
            $user = $user->where(function ($q) use ($startFilterDate,$endFilterDate) {
                //  $q->whereBetween('created_at', [$startFilterDate, $endFilterDate]);
                $q->whereDate('created_at', '>=', $startFilterDate);
                $q->whereDate('created_at', '<=', $endFilterDate);
            });
        }

        if (!$per_page) {
            $per_page = 15;
        }

        // $user = $user->with(['channel'])->where('app_id', $appId);

        return response()->json($user->paginate($per_page));


    }

    public function getDataMessages(Request $request, $appId)
    {
        $sort = $request->get('sort');
        $filterText = $request->get('filterText');
        $per_page = $request->get('per_page');
        $channel = $request->get('channel');
        $startFilterDate = $request->get('start');
        $endFilterDate = $request->get('end');

        // $data = Message::whereHas('room', function ($q) use ($appId) {
        //             $q->where('app_id', $appId);
        //         })
        //         ->where('messageable_type','!=', 'bot')
        //         ->orderBy('created_at', 'DESC');
                // ->take(10)
                // ->get();

        // $data = Message::with(['room' => function ($q) use ($appId) {
        //     $q->where('app_id', $appId);
        // }])
        $data = Message::whereHas('room', function ($q) use ($appId) {
            $q->where('app_id', $appId);
        })
        ->where('messageable_type','!=', 'bot')
        ->orderBy('created_at', 'DESC');
        // ->take(10)
        // ->get();

        $data2 = [];
        foreach ($data as $key => $value) {
            $data2[] = $data[$key]->content[0]->text;
            // $tmp_data[] = $value;
        }
        $data3 = [];
        foreach ($data as $key => $value) {
            $value->messages = $data2[$key];
            $data3[] = $value;
        }

        if ($sort) {
            $sort = explode('|', $sort);
            $user = $data->orderBy($sort[0], $sort[1]);
        }

        if ($filterText){
            $data = $data->where(function ($q) use ($filterText){
              if (preg_match('~[0-9]+~', $filterText)) {
                $q->where('room_id',$filterText);
              }else{
                $q->where('content','ILIKE', '%'.trim($filterText).'%');
              }

            });
        }

        if ($startFilterDate && $endFilterDate) {
            $data = $data->where(function ($q) use ($startFilterDate,$endFilterDate) {
                //  $q->whereBetween('created_at', [$startFilterDate, $endFilterDate]);
                $q->whereDate('created_at', '>=', $startFilterDate);
                $q->whereDate('created_at', '<=', $endFilterDate);
            });
        }

        if (!$per_page) {
            $per_page = 15;
        }

        return response()->json($data->paginate($per_page));
        // return $data;
    }

    public function getDataRoomMsg(Request $request, $appId)
    {
        $sort = $request->get('sort');
        $filterText = $request->get('filterText');
        $per_page = $request->get('per_page');
        $channel = $request->get('channel');
        $startFilterDate = $request->get('start');
        $endFilterDate = $request->get('end');

        if ($startFilterDate && $endFilterDate) {
            $data = Room::where('app_id', $appId)->orderBy('created_at', 'DESC')
                        ->where(function ($q) use ($startFilterDate,$endFilterDate) {
                                $q->whereDate('created_at', '>=', $startFilterDate);
                                $q->whereDate('created_at', '<=', $endFilterDate);
                             })->orderBy('created_at', 'DESC')->get();
        } else {
            $data = Room::where('app_id', $appId)->orderBy('created_at', 'DESC')->get();
        }

        if ($sort) {
            $sort = explode('|', $sort);
            $user = $data->orderBy($sort[0], $sort[1]);
        }

        if ($filterText){
            $data = $data->filter(function ($item) use ($filterText) {
                return false !== stristr($item->created_by->name, $filterText) || false !== stristr($item->created_by->channel->name, $filterText);
            })->values();
        }

        if (!$per_page) {
            $per_page = 15;
        }

        $datass = $this->paginate($data, $per_page);
        $datass->appends($request->only(['filterText','start','end','sort']));
        return response()->json($datass);
        // return response()->json($data->paginate($per_page));
    }

    public function agentPerformanceOptimize(Request $request,$appId) {
      $per_page = $request->get("per_page") ?? 100;
      $page = $request->get("page") ?? 1;
      $search   = $request->get("filterText");

      $agents = DB::table("omnichannel.app_user_platform as aup")
                ->join("auth.users as au",function($query) {
                  $query->on("au.id","=","aup.user_id");
                })
                ->join("auth.roles as ar",function($query) {
                  $query->on("ar.id","=","aup.role_id");
                })
                ->where("aup.app_id",$appId)
                ->where("ar.slug","staff")
                ->where(function($query) use($search) {
                  if($search) {
                    $query->where("au.name","ilike","%".$search."%")->orWhere("au.email","ilike","%".$search."%");
                  }
                })
                ->addSelect(DB::raw("
                  (
                    SELECT json_agg(
                      json_build_object (
                        'start_at', ol.start_at,
                        'end_at', ol.end_at,
                        'first_response_message_time', (
                          SELECT om.created_at FROM omnichannel.messages as om
                            WHERE om.messageable_type = 'user_platform'
                            AND om.room_id = ol.room_id
                            AND om.created_at BETWEEN ol.start_at AND ol.end_at
                            ORDER BY
                            om.id ASC
                            LIMIT 1
                        )
                      )
                    )
                    FROM omnichannel.livechats as ol
                    INNER JOIN omnichannel.rooms as orr
                    ON orr.id  = ol.room_id
                    WHERE ol.handle_by = au.id
                    AND ol.status = 'resolved'
                    AND orr.app_id = aup.app_id
                    AND ol.end_at IS NOT NULL
                    AND ol.start_at IS NOT NULL
                  ) as livechat
                "))
                ->addSelect(DB::raw("
                    (
                      SELECT COUNT(om.id)
                      FROM omnichannel.messages as om
                      INNER JOIN omnichannel.rooms as orr
                      ON orr.id  = om.room_id
                      WHERE orr.app_id = aup.app_id
                      AND om.messageable_type = 'user_platform'
                      AND om.messageable_id = au.id
                    ) as total_messages
                "))
                ->addSelect("au.email","au.name","au.created_at","au.id")
                ->paginate($per_page)
                ->map(function($each_agent) {
                  $each_agent->livechat = json_decode($each_agent->livechat);
                  $each_agent->total_resolved_livechat = is_null($each_agent->livechat) ? 0 : count($each_agent->livechat);
                  $each_agent->resolution = is_null($each_agent->livechat) ? 0 : (function() use($each_agent) {
                    $resolution_time = array_map(function($each_livechat) {
                        return Carbon::parse($each_livechat->start_at)->diffInMinutes($each_livechat->end_at);
                    },$each_agent->livechat);
                    $calculate_resolution_time = round(array_sum($resolution_time) / count($each_agent->livechat));
                    return $calculate_resolution_time;
                  })();
                  $each_agent->first_response  = is_null($each_agent->livechat) ? 0 : (function() use($each_agent) {
                    $first_response_time = array_map(function($each_livechat) {
                       return Carbon::parse($each_livechat->start_at)->diffInMinutes(
                         is_null($each_livechat->first_response_message_time) ?
                          $each_livechat->start_at :
                        Carbon::parse($each_livechat->first_response_message_time)
                       );
                    },$each_agent->livechat);
                    $calculate_first_response_time_average = number_format((float)(array_sum($first_response_time) / count($first_response_time)), 1, '.', '');
                    return $calculate_first_response_time_average;
                  })();
                  return $each_agent;
                });

      if (!$per_page) {
          $per_page = 15;
      }

      $datas = $this->paginate($agents, $per_page);
      $datas->appends($request->only(['filterText']));
      return response()->json($datas);
    }

    public function agentPerformance(Request $request, $appId)
    {
        $sort = $request->get('sort');
        $filterText = $request->get('filterText');
        $per_page = $request->get('per_page');
        $channel = $request->get('channel');
        $startFilterDate = $request->get('start');
        $endFilterDate = $request->get('end');

        // $agents = UserRole::select('*')->with(['userPlatform' => function ($q) {
        //     $q->select('*');
        // }])
        // $agents = UserRole::with(['userPlatform'])
        // ->whereHas('role', function ($q) {
        //     $q->where('name', 'Staff');
        // })
        // ->where('platform_id', '=', env('OMNI_PLATFORM_ID'))
        // ->where('product_id', '=', $appId)
        // ->get();
        $agents = RoleHelper::agents($appId);

        \Log::debug(count($agents));


        foreach ($agents as $key => $agent) {
                $resolved[$key] = Livechat::whereHas('room', function ($q) use ($appId){
                                        $q->where('app_id', $appId);
                                    })
                                  ->where('handle_by', $agent->id)->where('status', 'resolved')->count();

                $totalMessages[$key] = Message::whereHas('room', function ($q) use ($appId){
                                                        $q->where('app_id', $appId);
                                                    })
                                                ->where('messageable_type','=','user_platform')->where('messageable_id','=',$agent->id)->count();

                $resolutionTime[$key] = Livechat::whereHas('room', function ($q) use ($appId) {
                                                $q->where('app_id', $appId);
                                            })
                                        ->where('handle_by', $agent->id)->where('status', 'resolved')
                                        ->get();
                if(($resolutionTime[$key]->isEmpty())){
                     $resolutionTimeAvg[$key] = 0;
                }
                else {
                    foreach($resolutionTime[$key] as $key2 => $dataAvg){
                        $diff[$key][$key2] = Carbon::parse($dataAvg->start_at)->diffInMinutes(Carbon::parse($dataAvg->end_at));
                        \Log::debug($diff[$key][$key2]);
                    }
                    $resolutionTimeAvg[$key] = round(array_sum($diff[$key]) / count($diff[$key]));
                }



                $firstResponseTime[$key] = Livechat::whereHas('room', function ($q) use ($appId) {
                                                $q->where('app_id', $appId);
                                            })
                                        ->where('handle_by', $agent->id)->where('status', 'resolved')
                                        ->get();

                if($firstResponseTime[$key]->isEmpty()){
                    // $firstResponseTime[$key] = ['start_at' => ]
                    $firstResponseTimeAvg[$key] = 0;
                }
                else {
                    foreach ($firstResponseTime[$key] as $key2 => $d) {
                        // $join[$key2] = $d;
                        $msg[$key][$key2] = Message::whereHas('room', function ($q) use ($appId) {
                                                            $q->where('app_id', $appId);
                                                        })
                                                ->whereBetween('created_at', [$d->start_at, $d->end_at])
                                                ->where('messageable_type', 'user_platform')
                                                ->orderBy('id', 'ASC')
                                                ->first();
                        if($msg[$key][$key2] === null){
                            $msg[$key][$key2]['created_at'] = $d->start_at;
                        }
                        $diffInMinutes[$key][$key2] = Carbon::parse($d->start_at)->diffInMinutes(Carbon::parse($msg[$key][$key2]['created_at']));
                    }
                    $firstResponseTimeAvg[$key] = number_format((float)(array_sum($diffInMinutes[$key]) / count($diffInMinutes[$key])), 1, '.', '');
                    // return number_format((float)$avg, 2, '.', '');
                    // $asdf[$key] = $msg[$key];
            }


            }

        foreach ($agents as $key => $agent) {
                $agent->total_resolved_livechat = $resolved[$key];
                $agent->total_messages = $totalMessages[$key];
                $agent->resolution = $resolutionTimeAvg[$key] . ' m';
                $agent->first_response = $firstResponseTimeAvg[$key] . ' m';
                unset($agent->apps);
                $datas[] = $agent;
            }

        // foreach ($agents as $key => $agent) {
        //     $data[$key] = Livechat::where('status', 'resolved')
        //                     ->whereHas('room', function ($q) use ($appId) {
        //                         $q->where('app_id', $appId);
        //                     })
        //                     ->where('handle_by', $agent->user_id)
        //                     ->count();
        // }



        // if ($sort) {
        //     $sort = explode('|', $sort);
        //     $user = $data->orderBy($sort[0], $sort[1]);
        // }

        if ($filterText){
           $agents = $agents->filter(function ($item) use ($filterText) {
               return false !== stristr($item->name, $filterText) || false !== stristr($item->email, $filterText) || false !== stristr($item->id, $filterText);
           })->values();
        }

        // if ($filterText){
        //     // $data = $data->where(function ($q) use ($filterText){
        //     //     $q->whereIn('user.name', $filterText)
        //     //     ->orWhere('room_id', $filterText);
        //     // });
        //     $data = $data->filter(function ($item) use ($filterText) {
        //         return false !== stristr($item->created_by->name, $filterText) || false !== stristr($item->created_by->channel->name, $filterText);
        //     })->values();
        // }


        // if ($startFilterDate && $endFilterDate) {
        //     $data = $data->where(function ($q) use ($startFilterDate,$endFilterDate) {
        //          $q->whereBetween('created_at', [$startFilterDate, $endFilterDate]);
        //     });
        // }


        if (!$per_page) {
            $per_page = 15;
        }

        $datas = $this->paginate($agents, $per_page);
        $datas->appends($request->only(['filterText']));
        return response()->json($datas);


        // return response()->json(['data' => $datas]);
        // return $agents;

        // return response()->json($agents->paginate($per_page));


    }

    public function getDataReportHsm(Request $request, $appId)
    {
        $sort = $request->get('sort');
        $filterText = $request->get('filterText');
        $per_page = $request->get('per_page');
        $channel = $request->get('channel');
        $integrationId = $request->get('integrationId');
        $startFilterDate = $request->get('start');
        $endFilterDate = $request->get('end');
        $templateName = $request->get('templateName');
        $status = $request->get('status');

        $data = DB::table("broadcast_messages as bm")->where('app_id', $appId)
                    ->where("bm.category", 'hsm')
                    ->where("bm.channel_id", 4)
                    ->leftJoin("auth.users as u", "u.id", "=", "bm.send_by") //still get null data when send_by is null
                    ->select("bm.*", "u.name")
                    ->orderBy("bm.created_at", "DESC");

        if ($integrationId) {
            $data = $data->where(function ($q) use ($integrationId) {
                $q->where('bm.integration_id', $integrationId);
            });
        }

        if ($templateName) {
            $data = $data->where(function ($q) use ($templateName) {
                $q->where('bm.topics', $templateName);
            });
        }

        if ($status) {
            $data = $data->where(function ($q) use ($status) {
                $q->where('bm.status', $status);
            });
        }

        if ($sort) {
            $sort = explode('|', $sort);
            $user = $data->orderBy($sort[0], $sort[1]);
        }

        if ($filterText){
            $data = $data->where(function ($q) use ($filterText){
                $q->where('bm.number', "ILIKE", '%'.$filterText.'%');
            });
        }

        if ($startFilterDate && $endFilterDate) {
            $data = $data->where(function ($q) use ($startFilterDate,$endFilterDate) {
                //  $q->whereBetween('created_at', [$startFilterDate, $endFilterDate]);
                $q->whereDate('bm.created_at', '>=', $startFilterDate);
                $q->whereDate('bm.created_at', '<=', $endFilterDate);
            });
        }


        if (!$per_page) {
            $per_page = 15;
        }

        return response()->json($data->paginate($per_page));

    }

    public function getTotalHsmStatus(Request $request, $appId)
    {
        $start = $request->get('start');
        $end = $request->get('end');

        if ($start && $end) {
            $sent = DB::table("broadcast_messages")->where("app_id", $appId)
                    ->where("category", 'hsm')
                    ->where("channel_id", 4)
                    ->where("status", 'sent')
                    ->whereDate("created_at", ">=", $start)
                    ->whereDate("created_at", "<=", $end)
                    ->count();
            $failed = DB::table("broadcast_messages")->where("app_id", $appId)
                        ->where("category", 'hsm')
                        ->where("channel_id", 4)
                        ->where('status', 'failed')
                        ->whereDate("created_at", ">=", $start)
                        ->whereDate("created_at", "<=", $end)
                        ->count();
            $invalid = DB::table("broadcast_messages")->where("app_id", $appId)
                        ->where("category", 'hsm')
                        ->where('channel_id', 4)
                        ->where('status', 'invalid')
                        ->whereDate("created_at", ">=", $start)
                        ->whereDate("created_at", "<=", $end)
                        ->count();
            $all = DB::table("broadcast_messages")->where("app_id", $appId)
                            ->where("category", "hsm")
                            ->where("channel_id", 4)
                            ->whereDate("created_at", ">=", $start)
                            ->whereDate("created_at", "<=", $end)
                            ->count();

            return $this->successResponse([
                'sent' => $sent,
                'failed' => $failed,
                'invalid' => $invalid,
                'all' => $all
            ]);
        }

        $sent = DB::table("broadcast_messages")->where("app_id", $appId)
                    ->where("category", 'hsm')
                    ->where("channel_id", 4)
                    ->where("status", 'sent')
                    ->count();
        $failed = DB::table("broadcast_messages")->where("app_id", $appId)
                      ->where("category", 'hsm')
                      ->where("channel_id", 4)
                      ->where('status', 'failed')
                      ->count();
        $invalid = DB::table("broadcast_messages")->where("app_id", $appId)
                      ->where("category", 'hsm')
                      ->where('channel_id', 4)
                      ->where('status', 'invalid')
                      ->count();
        $all = DB::table("broadcast_messages")->where("app_id", $appId)
                        ->where("category", "hsm")
                        ->where("channel_id", 4)
                        ->count();

        return $this->successResponse([
            'sent' => $sent,
            'failed' => $failed,
            'invalid' => $invalid,
            'all' => $all
        ]);
    }
    public function getRoleHistoryActivity(Request $request, $appId)
    {
        $sorts = $request->get('sort');
        $filterText = $request->get('filterText');
        $per_page = $request->get('per_page');
        $startFilterDate = $request->get('start');
        $endFilterDate = $request->get('end');
        $userPlatform = $request->get('user_platform_name');
        $activity = $request->get('activity');
        $exportType = $request->get('exportType');

        $data = DB::table("history_activity_user_roles as h")->where('app_id', $appId);
        if ($sorts) {
            $sorts = explode('|', $sorts);
            // return $sorts;
            $data = $data->orderBy($sorts[0], $sorts[1]);
        } else {
            $data->orderBy('created_at', 'desc');
        }
        if ($userPlatform) {
            $data = $data->where(function ($q) use ($userPlatform) {
                            $value = "%{$userPlatform}%";
                            $q->where('user_platform_name', "ILIKE", $value);
                        });
        }
        if ($activity) {
            $data = $data->where('activity', $activity);
        }
        if ($filterText) {
            $data = $data->where(function ($q) use ($filterText) {
                $value = "%{$filterText}%";
                $q->where('user_platform_name', "ILIKE", $value);
                // $q->orWhere()
            });
        }
        if ($startFilterDate && $endFilterDate) {
            $data = $data->where(function ($q) use ($startFilterDate, $endFilterDate) {
                $q->whereDate('created_at', ">=", $startFilterDate);
                $q->whereDate('created_at', "<=", $endFilterDate);
            });
        }
        if (!$per_page) {
            $per_page = 15;
        }

        if ($exportType == 'pdf') {
            return $this->successResponse($data->get(), "success");
        }

        return response()->json($data->paginate($per_page));
        // return $data;
    }

    public function getNameUserPlatformList(Request $request, $appId)
    {
        $users = DB::table('app_user_platform as up')
                    ->join('auth.users as u', 'up.user_id', '=', 'u.id')
                    ->where('up.app_id', $appId)
                    ->select('u.name')
                    ->get();
        return $users;
    }

    public function exportUsers(Request $request,$appId)
    {
        return (new UserExport)->AppId($appId, $request->get("start"), $request->get("end"),$request->get("search"))->download("Report User.xlsx");
    }
    public function exportMessages(Request $request, $appId)
    {
        $start = $request->get("start");
        $end = $request->get("end");
        $search = $request->get("search");
        return (new MessageExport)->AppId($appId, $start, $end,$search)->download("Report Messages $start - $end.xlsx");
    }
    public function exportRoom(Request $request, $appId)
    {
        $start = $request->get("start");
        $end = $request->get("end");
        $search = $request->get("search");
        return (new RoomMsgExport)->AppId($appId,$start,$end,$search)->download("Report RoomMsg $start - $end.xlsx");
    }
    public function exportHsm(Request $request, $appId)
    {
        $start = json_decode($request->get("filterDate"))->start;
        $end = json_decode($request->get("filterDate"))->end;
        $search = $request->get("filterText") ?? "";
        $integrationId = $request->get("integrationId") ?? "";
        $templateName = $request->get("templateName") ?? "";
        $status = $request->get("status") ?? "";
        return (new HsmExport)->AppId($appId,$start,$end,$search,$integrationId,$templateName,$status)->download("Report Hsm $start - $end.xlsx");
    }

    public function exportPdfHsm(Request $request, $appId)
    {
        $start = json_decode($request->get("filterDate"))->start;
        $end = json_decode($request->get("filterDate"))->end;
        $search = $request->get("filterText");
        $integrationId = $request->get("integrationId");
        $templateName = $request->get("templateName");
        $status = $request->get("status");

        $datas = DB::table("broadcast_messages as bm")->where('bm.app_id', $appId)
                   ->where("bm.category", "hsm")
                   ->where("bm.channel_id", 4)
                   ->leftJoin("auth.users as u", "u.id", "=", "bm.send_by") //still get null data when send_by is null
                   ->select("bm.*", "u.name")
                   ->orderBy("bm.id", "DESC");

        if ($search) {
            $datas = $datas->where(function ($q) use ($search) {
                $q->where("bm.number", "ILIKE", "%".$search."%");
            });
        }
        if ($integrationId) {
            $datas = $datas->where(function ($q) use ($integrationId) {
                $q->where('bm.integration_id', $integrationId);
            });
        }
        if ($templateName) {
            $datas = $datas->where(function ($q) use ($templateName) {
                $q->where('bm.topics', $templateName);
            });
        }
        if ($status) {
            $datas = $datas->where(function ($q) use ($status) {
                $q->where('bm.status', $status);
            });
        }
        if ($start && $end) {
            $datas = $datas->where(function ($q) use ($start,$end) {
                $q->whereDate('bm.created_at', '>=', $start);
                $q->whereDate('bm.created_at', '<=', $end);
            });
        }
        $datas->get();
        return $this->successResponse($datas->get(),"success");

    }


    public function exportTag(Request $request, $appId)
    {
      $start = $request->get("start");
      $end = $request->get("end");
      $search = $request->get("search");
      return (new TagExport)->AppId($appId, $start, $end,$search)->download("Report Tag $start - $end.xlsx");
    }

    public function exportHandleRoom(Request $request,$appId) {
      $start = $request->get("start");
      $end = $request->get("end");
      $search = $request->get("search");
      return (new LogHandleRoomExport)->AppId($request,$appId, $start, $end,$search)->download("Report Log Handle Room $start - $end.xlsx");
    }

    public function exportTimeServed(Request $request, $appId)
    {
      $start = $request->get("start");
      $end = $request->get("end");
      $search = $request->get("search");
      return (new TimeServedExport)->AppId($appId, $start, $end,$search)->download("Report Time Served $start - $end.xlsx");
    }
    public function exportNote(Request $request,$appId)
    {
        $start = $request->get("start");
        $end   = $request->get("end");
        $search = $request->get("search");
        return (new NoteExport)->AppId($appId, $start, $end,$search)->download("Report Note $start - $end.xlsx");
    }


    public function exportTicket(Request $request,$appId)
    {
        $condition = [
            "status" => $request->status,
            "priority" => $request->priority,
            "type" => $request->type,
            "agent_id" => $request->agent_id,
            "solved_at" => json_decode($request->solved_at),
            "created_at" => json_decode($request->created_at)
       ];
       return (new TicketExport)->AppId($appId, $condition)->download("Reporting Ticket.xlsx");
    }

    public function exportRoleHistoryActivity(Request $request, $appId)
    {
        $start = json_decode($request->get("filterDate"))->start;
        $end = json_decode($request->get("filterDate"))->end;
        $search = $request->get("filterText") ?? "";
        $userPlatformName = $request->get("user_platform_name") ?? "";
        $activity = $request->get("activity") ?? "";

        return (new RoleHistoryActivityExport)->AppId($appId,$start,$end,$search,$userPlatformName,$activity)->download("Report Role History Activity $start - $end.xlsx");
    }

    public function paginate($items, $perPage, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage)->values(), $items->count(), $perPage, $page, [
          'path' => LengthAwarePaginator::resolveCurrentPath()
        ]);
    }

    public function exportAgentActivities(Request $request,$appId) {
      return (new AgentActivitiesExport($appId, $request->start, $request->end, $request->filterText))
        ->download("report.xlsx");
    }


    public function exportBroadcastMessageMobile(Request $request, $appId) {
      return (new RecordBroadcastMobileExport($appId, $request->start, $request->end, $request->filterText))
                ->download("report.xlsx");
    }

    public function exportBroadcastMessageWhatsapp(Request $request, $appId) {
      return (new RecordBroadcastWhatsappExport($appId, $request->start, $request->end, $request->filterText))
        ->download("report.xlsx");
    }


} //end controller
