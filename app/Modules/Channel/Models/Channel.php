<?php

namespace App\Modules\Channel\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Channel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $connection = 'pgsql';

    public function users()
    {
        return $this->hasMany('App\Modules\User\Models\User');
    }
    public function rooms()
    {
        return $this->hasMany('App\Modules\Room\Models\Room');
    }
    public function broadcastMessages()
    {
        return $this->hasMany('App\Modules\BroadcastMessage\Models\BroadcastMessage');
    }
    public function integration()
    {
        return $this->hasOne('App\Modules\Integration\Models\Integration');
    }
    public function userChannels()
    {
        return $this->hasMany('App\Modules\User\Models\UserRoleChannel', 'channel_id');
    }
}
