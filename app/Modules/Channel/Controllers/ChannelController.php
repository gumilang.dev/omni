<?php

namespace App\Modules\Channel\Controllers;

use App\Modules\Channel\Models\Channel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChannelController extends Controller
{

    public function index()
    {
        $channel = Channel::select('id', 'name')->where('name', '!=', 'bot')->get();
        return $channel;
    }

}
