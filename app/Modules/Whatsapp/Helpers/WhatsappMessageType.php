<?php

namespace App\Modules\Whatsapp\Helpers;

class WhatsappMessageType
{


    public function __construct()
    {

    }

    public function text($input = [])
    {
        $output = $input->text ?? '';
        $output = [
            'text' => $output
        ];
        return $output;

    }

    public function file($input = [])
    {

        $output = [
            'fileUrl' => $input->fileUrl ?? '',
            'fileName' => $input->fileName ?? 'File',
        ];
        return $output;

    }
    public function image($input = [])
    {
        // $output = $input->originalContentUrl ?? $input->previewImageUrl ?? 'No image found';
        $output = [
            'imageUrl' => $input->originalContentUrl ?? $input->previewImageUrl,
            'caption' => $input->caption
        ];
        return $output;
    }

    public function video($input = [])
    {
        // $output = $input->originalContentUrl ?? $input->previewImageUrl ?? 'No video found';
        $output = [
            'videoUrl' => $input->originalContentUrl ?? $input->previewVideoUrl,
            'caption' => $input->caption
        ];
        return $output;
    }

    public function template($input = [])
    {
        $output = $input->template ?? '';
        //bold
        $output = str_replace('<p>', '', $output);
        $output = str_replace('</p>', "\n", $output);

        $output = str_replace('<b>', '*', $output);
        $output = str_replace('</b>', '*', $output);
        $output = str_replace('<strong>', '*', $output);
        $output = str_replace('</strong>', '*', $output);

        //italic
        $output = str_replace('<i>', '_', $output);
        $output = str_replace('</i>', '_', $output);
        $output = str_replace('<em>', '_', $output);
        $output = str_replace('</em>', '_', $output);

        $output = str_replace('<del>', '~', $output);
        $output = str_replace('</del>', '~', $output);

        //strike
        $output = str_replace('<s>', '~', $output);
        $output = str_replace('</s>', '~', $output);

        //code
        $output = str_replace('<pre>', '```', $output);
        $output = str_replace('</pre>', '```', $output);
        $output = str_replace('<br>', "\n", $output);
        $output = strip_tags($output);

        $output = [
            'text' => $output
        ];
        // dd($output);
        return $output;
    }

    public function audio($input = [])
    {
        $output = $input->originalContentUrl ?? 'No audio found';

        return $output;
    }

    public function confirm($input = [])
    {
        $output = $input->text . "\n" ?? "Apa kamu mau melanjutkan?\n";
        $output = "Ketik\n";

        $output .= "*{$input->actions[0]->label}* untuk {$input->actions[0]->text}\n";
        $output .= "*{$input->actions[1]->label}* untuk {$input->actions[1]->text}\n";
        $output = [
            'text' => $output
        ];
        return $output;

    }

    public function carousel($input = [])
    {
        $text = "";
        $split = false;
        foreach ($input->columns as $data) {
            $text .= "*$data->title* - $data->text\n";

            foreach ($data->actions as $key => $act) {
                $data = $act->data ? $act->data : $act->label;
                $text .= "Ketik *$data* untuk $act->label\n";

            }
            $text .= "\n";
            if (strlen($text) >= 1500 && !$split) {
                $text .= "### \n";
                $split = true;
            }

        }
        if (strlen($text) >= 1500) {
            $text = explode('###', $text);

            $output = [
                'multiple' => $text,
            ];
            return $output;
        }

        $output = [
            'text' => $text,
        ];
        return $output;


    }


    public function flightScheduleCarousel($input = [])
    {
        $text = "";
        $split = false;

        foreach ($input->columns as $col) {
            $text .= "*$col->airlineName ($col->flightNo)* - $col->fullVia $col->price\n";
            $text .= "Ketik *{$col->actions->data}* untuk pilih";

            $text .= "\n \n";
            if (strlen($text) >= 1500 && !$split) {
                $text .= "### \n";
                $split = true;
            }

        }
        if (strlen($text) >= 1500) {
            $text = explode('###', $text);

            $output = [
                'multiple' => $text,
            ];
            return $output;
        }

        $output = [
            'text' => $text,
        ];

        return $output;


    }

    public function summary($input = [])
    {
        $output = $input->title . " \n" ?? "Ringkasan Transaksi \n";

        foreach ($input->columns as $data) {
            $output .= "*$data->field*: $data->value\n";

        }
        $output = [
            'text' => $output
        ];
        return $output;


    }
    public function transaction($input = [])
    {
        $output = $input->title . " \n" ?? "Ringkasan Transaksi \n";

        foreach ($input->columns as $data) {
            $output .= "*$data->field*: $data->value\n";

        }
        $output = [
            'text' => $output
        ];
        return $output;
    }

    public function html($input = [])
    {
        $output = $input->content ?? '';
        //bold
        $output = str_replace("<p>", "", $output);
        $output = str_replace("</p>", "\n", $output);

        //bold
        $output = str_replace("<b>", "*", $output);
        $output = str_replace("</b>", "*", $output);
        $output = str_replace("<strong>", "*", $output);
        $output = str_replace("</strong>", "*", $output);

        //italic
        $output = str_replace("<i>", "_", $output);
        $output = str_replace("</i>", "_", $output);
        $output = str_replace("<em>", "_", $output);
        $output = str_replace("</em>", "_", $output);

        $output = str_replace("<del>", "~", $output);
        $output = str_replace("</del>", "~", $output);

        //strike
        $output = str_replace("<s>", "~", $output);
        $output = str_replace("</s>", "~", $output);

        //code
        $output = str_replace("<br>", "\n", $output);
        // $output = str_replace("<pre>", "```", $output);
        // $output = str_replace("</pre>", "```", $output);

        //ol ul li
        $output = str_replace("<ol>", "", $output);
        $output = str_replace("</ol>", "", $output);
        $output = str_replace("<ul>", "", $output);
        $output = str_replace("</ul>", "", $output);
        $output = str_replace("<li>", "•", $output);
        $output = str_replace("</li>", "\n", $output);

        $output = strip_tags($output);
        $output = [
            "text" => $output
        ];
        return $output;
    }
}
