<?php

namespace App\Modules\Whatsapp\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
// use Illuminate\Notifications\Notifiable;
use Illuminate\Notifications\HasDatabaseNotifications;
use Illuminate\Notifications\RoutesNotifications;


class HsmTemplate extends Model
{
    use SoftDeletes, HasDatabaseNotifications, RoutesNotifications;
    protected $connection = 'pgsql';
    protected $hidden = ['app_id', 'provider'];
    protected $guarded = [];

    public function getEmailAttribute($value)
    {
        return ['fachriza@lenna.ai', 'ival@lenna.ai'];
        // return ['fachriza@lenna.ai'];
    }

}
