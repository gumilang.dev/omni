<?php

namespace App\Modules\Whatsapp\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\RequestGuzzle;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Carbon\Carbon;
use Exception;
use Log;

use App\Http\Controllers\Controller;

use App\Modules\Whatsapp\Helpers\WhatsappMessageType;
use App\Modules\Common\Services\EncryptService;

use App\Modules\ExternalApi\Models\ExternalApi;
use App\Modules\Integration\Models\Integration;
use App\Modules\Whatsapp\Models\HsmTemplate;
use App\Modules\BroadcastMessage\Models\BroadcastMessage;
use App\Modules\Log\Models\ApiLog;

class WappinApiController extends Controller
{
    public function __construct($data)
    {
        $this->data = $data;
        $this->wappinApi = ExternalApi::where(['category' => 'channel', 'provider' => 'wappin'])->first();
        if (env('GUZZLE_PROXY_PORT') && env('GUZZLE_PROXY')) {
          $proxy = env('GUZZLE_PROXY').':'.env('GUZZLE_PROXY_PORT');
        }
        $guzzleSWC = [
            'base_uri' => str_replace("{subdomain}", 'swc', $this->wappinApi->base_url),
            'verify' => false,
            'proxy' => $proxy ?? null
        ];
        $guzzleAPI = [
            'base_uri' => str_replace("{subdomain}", 'api', $this->wappinApi->base_url),
            'verify' => false,
            'proxy' => $proxy ?? null
        ];
        $this->clientSWC = new Client($guzzleSWC);
        $this->clientAPI = new Client($guzzleAPI);
    }

    public function sendMessage($newMessage)
    {
      foreach ($newMessage->content as $key => $content) {
        $converted = $this->convertToWhatsappMessageType($content);

        if (isset($converted['multiple'])) {
          foreach ($converted['multiple'] as $key => $text) {
            $response = $this->callSendAPI(['message' => $text], $converted['type']);
          }
        } else {
          $response = $this->callSendAPI($converted, $converted['type']);
        }
      }
      return $response;
    }

    public function callSendAPI($message, $type)
    {
        $templateName = $type;
        $phone = (array) $this->data['chatRoom']->created_by->phone;
        $params = $message;

        $responseCheckToken = $this->checkTokenSWC();
        if (!$responseCheckToken->success) {
          return $responseCheckToken;
        }

        $responseWaId = $this->checkWaId($phone, $params);
        if (!$responseWaId->success) {
          return $responseWaId;
        }

        switch ($templateName) {
          case 'text':
              $response = $this->sendMessageText($responseWaId->data[0], $params);
            break;
          case 'image':
              $response = $this->sendMessageImage($responseWaId->data[0], $params);
            break;
          case 'document':
              $response = $this->sendMessageDocument($responseWaId->data[0], $params);
            break;
        }
        return $response;
    }

    public function saveTokenSWC($response)
    {
      $this->integrationData->integration_data->tokenSWC = $response->data->users->token;
      $this->integrationData->integration_data->tokenSWCExpired = $response->data->users->expires_after;
      $this->saveIntegration($response);
    }

    public function saveIntegration($response)
    {
      $appId = $this->integrationData->app_id;
      $integrationId = $this->integrationData->id;
      $integration = Integration::where(['app_id' => $appId, 'id' => $integrationId, 'channel_id' => 4])->first();
      $integration->integration_data = $this->integrationData->integration_data;
      $integration->save();
      $this->integrationData = $integration->fresh();
    }

    public function checkTokenSWC()
    {
      $this->integrationData = json_decode($this->data['integration']);

      if (!isset($this->integrationData->integration_data->tokenSWCExpired) || !isset($this->integrationData->integration_data->tokenSWC)) {
        $response = $this->loginSWC();
        if (!$response->success) {
          return (object)[
            'success' => false,
            'error'   => $response->error,
            'message' => $response->message,
          ];
        }
        return $response;
      } else {
        $now = Carbon::now('Asia/Jakarta');
        $expired = Carbon::parse($this->integrationData->integration_data->tokenSWCExpired);
        $diffHours = $now->diffInHours($expired, false);
        // if expired
        if ($diffHours <= 0) {
          $response = $this->loginSWC();
          if (!$response->success) {
            return (object)[
              'success' => false,
              'error'   => $response->error,
              'message' => $response->message,
            ];
          }
          return $response;
        } else {
          return (object)[
            'success' => true,
            'data'    => null,
            'message' => null,
          ];
        }
      }
    }

    public function loginSWC()
    {
      $apiProp = $this->wappinApi->endpoints()->where('name', 'v1-users-login')->first();
      try {
        $response = $this->clientSWC->request($apiProp->method, $apiProp->endpoint, [
            'headers' => [
              'Content-Type' => 'application/json'
            ],
            'auth' => [
              $this->integrationData->integration_data->username,
              $this->integrationData->integration_data->password
            ]
        ]);
        $response = (object)[
          'success' => true,
          'data' => json_decode($response->getBody()->getContents())
        ];
        $this->saveTokenSWC($response);
      } catch (RequestException $e) {
        $response = (object)[
          'success' => false,
          'error'   => $e->getResponse()->getBody(true) ?? null,
          'message' => $e->getMessage() ?? null
        ];
      }
      return $response;
    }

    public function checkWaId($phone, $params = [])
    {
      $apiProp = $this->wappinApi->endpoints()->where('name', 'v1-contacts')->first();
      $phonePlus = [];
      foreach ($phone as $key => $singlePhone) {
        $singlePhone = preg_replace("/[^0-9]/", "", $singlePhone);
        if(!strstr($singlePhone, '+')){
          $phonePlus[] = "+$singlePhone";
        }
      }
      try {
        $response = $this->clientSWC->request(
          $apiProp->method,
          $apiProp->endpoint,
          [
            'headers' => [
              'Content-Type' => 'application/json',
              'Authorization' => 'Bearer '.$this->integrationData->integration_data->tokenSWC
            ],
            'json' => [
              'blocking' => 'wait',
              'contacts' => $phonePlus
            ],
          ]
        );
        $response = (object)[
          'success' => true,
          'data' => json_decode($response->getBody())
        ];
      } catch (RequestException $e) {
        $resp = json_decode($e->getResponse()->getBody());
        if ($resp->errors->code == 406 || $resp->errors->code == 401) {
          $this->loginSWC();
        } else if ($resp->errors->code == 901) {
          $response = (object)[
            'success' => false,
            'error'   => $e->getResponse()->getBody(true) ?? null,
            'message' => $resp->errors->title ?? null
          ];
        } else {
          $response = (object)[
            'success' => false,
            'error'   => $e->getResponse()->getBody(true) ?? null,
            'message' => $e->getMessage() ?? null
          ];
        }
      }

      if ($response->success) {
        $waId = [];
        foreach ($response->data->contacts as $key => $value) {
          if ($value->status == 'valid'){
            $waId[$key] = $value->wa_id;
          } else if ($value->status == 'invalid'){
            $waId[$key] = $value->input;
          }
        }
        return (object)[
          'success' => true,
          'data' => $waId
        ];
      } else {
        return $response;
      }
    }

    public function sendMessageText($responseWaId, $params)
    {
      $apiProp = $this->wappinApi->endpoints()->where('name', 'v1-messages')->first();
      $payload = [
        'recipient_type' => 'individual',
        'to' => $responseWaId,
        'preview_url' => true,
        'type' => 'text',
        'text' => [
          'body' => $params['message']
        ]
      ];
      try {
        $response = $this->clientSWC->request($apiProp->method, $apiProp->endpoint, [
          'headers' => [
            'Content-Type'  => 'application/json',
            'Authorization' => 'Bearer '.$this->integrationData->integration_data->tokenSWC
          ],
          'json' => $payload
        ]);
        $resp = json_decode($response->getBody());
        $response = (object)[
          'success' => true,
          'data'    => $resp,
        ];
        return $response;
      } catch (RequestException $e) {
        $resp = json_decode($e->getResponse()->getBody());
        if ($resp->errors->code == 406 || $resp->errors->code == 401) {
          $this->loginSWC();
        } else if ($resp->errors->code == 901) {
          $response = (object)[
            'success' => false,
            'error'   => $resp->errors->details ?? null,
            'message' => $resp->errors->title ?? null
          ];
          return $response;
        } else {
          $response = (object)[
            'success' => false,
            'error'   => $e->getResponse()->getBody(true) ?? null,
            'message' => $e->getMessage() ?? null
          ];
          return $response;
        }
      }
    }

    public function sendMessageImage($responseWaId, $params)
    {
      $apiProp = $this->wappinApi->endpoints()->where('name', 'v1-messages')->first();
      $payload = [
        'recipient_type' => 'individual',
        'to' => $responseWaId,
        'type' => 'image',
        'image' => (object)[
          'link' => $params['image'],
          'caption' => $params['caption'] ?? ''
        ]
      ];
      try {
        $response = $this->clientSWC->request($apiProp->method, $apiProp->endpoint, [
          'headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.$this->integrationData->integration_data->tokenSWC
          ],
          'json' => $payload
        ]);
        $resp = json_decode($response->getBody());
        $response = (object)[
          'success' => true,
          'data'    => $resp,
        ];
        return $response;
      } catch (RequestException $e) {
        // $response = $e->getResponse();
        // $responseBodyAsString = $response->getBody()->getContents();
        $resp = json_decode($e->getResponse()->getBody());
        if ($resp->errors->code == 406 || $resp->errors->code == 401) {
          $this->loginSWC();
          // return $this->sendMessageImage($responseWaId, $params);
        } else if ($resp->errors->code == 901) {
          $response = (object)[
            'success' => false,
            'error'   => $resp->errors->details ?? null,
            'message' => $resp->errors->title ?? null
          ];
          return $response;
        } else {
          $response = (object)[
            'success' => false,
            'error'   => $e->getResponse()->getBody(true) ?? null,
            'message' => $e->getMessage() ?? null
          ];
          return $response;
        }
      }
    }

    public function sendMessageDocument($responseWaId, $params)
    {
      $apiProp = $this->wappinApi->endpoints()->where('name', 'v1-messages')->first();
      $payload = [
        'recipient_type' => 'individual',
        'to' => $responseWaId,
        'type' => 'document',
        'document' => [
          'link' => $params['document'],
          'caption' => $params['caption'] ?? ''
        ]
      ];
      try {
        $response = $this->clientSWC->request($apiProp->method, $apiProp->endpoint, [
          'verify' => false,
          'headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.$this->integrationData->integration_data->tokenSWC
          ],
          'json' => $payload
        ]);
        $resp = json_decode($response->getBody());
        $response = (object)[
          'success' => true,
          'data'    => $resp,
        ];
        return $response;
      } catch (RequestException $e) {
        $resp = json_decode($e->getResponse()->getBody());
        if ($resp->errors->code == 406 || $resp->errors->code == 401) {
          $this->loginSWC();
          // return $this->sendMessageImage($responseWaId, $params);
        } else if ($resp->errors->code == 901) {
          $response = (object)[
            'success' => false,
            'error'   => $resp->errors->details ?? null,
            'message' => $resp->errors->title ?? null
          ];
          return $response;
        } else {
          $response = (object)[
            'success' => false,
            'error'   => $e->getResponse()->getBody(true) ?? null,
            'message' => $e->getMessage() ?? null
          ];
          return $response;
        }
      }
    }

    public function sendHsm($templateName, $phone, $params = [])
    {
        $responseCheckToken = $this->checkTokenAPI();
        if (!$responseCheckToken->success) {
          return $responseCheckToken;
        }

        $responseCheckTokenSWC = $this->checkTokenSWC();
        if (!$responseCheckTokenSWC->success) {
          return $responseCheckTokenSWC;
        }

        $responseWaId = $this->checkWaId($phone, $params);
        if (!$responseWaId->success) {
          return $responseWaId;
        }

        switch ($templateName) {
          case 'bctext':
              $response = $this->broadcastNonHSM($responseWaId, $params);
            break;
          case 'bcimage':
              $response = $this->broadcastNonHSM($responseWaId, $params);
            break;
          default:
              $templateData = $this->getTemplateData($templateName, $this->integrationData->app_id);
              if (!$templateData->success) {
                return $templateData;
              }
              $response = $this->sendBroadcastHSM($templateData->data, $responseWaId, $params);
            break;
        }
        // $response = $this->checkToken($templateName, $phone, $params);
        // $response = $this->checkWaId($templateName, $phone, $params);
        // $response = $this->callSendHsmApi($templateName, $phone, $params);
        // print_r($params); die();
        return $response;
    }

    public function callSendBroadcastNonHsm($newMessage, $phone)
    {
      $params = $newMessage;

      $responseCheckTokenSWC = $this->checkTokenSWC();
      if (!$responseCheckTokenSWC->success) {
        return $responseCheckTokenSWC;
      }

      $responseWaId = $this->checkWaId($phone, $params);
      if (!$responseWaId->success) {
        return $responseWaId;
      }

      $response = $this->broadcastNonHSM($newMessage->content[0], $phone);
      return $response;
    }

    public function broadcastNonHSM($body, $phone)
    {
      $apiProp = $this->wappinApi->endpoints()->where('name', 'v1-messages')->first();
      $convertBroadcastMessage = $this->convertBroadcastMessage($body);
      foreach ($phone as $key => $value) {
        $number[] = $value;
        $payload = [
          'to' => $value,
          'type' => $body->type,
          'recipient_type' => 'individual'
        ];
        $payload = array_merge($payload, $convertBroadcastMessage);
        $response[$key] = $this->clientSWC->requestAsync($apiProp->method, $apiProp->endpoint, [
          'http_errors' => false,
          'verify' => false,
          'headers' => [
            'Content-Type' => 'application/json',
            // 'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
            'Authorization' => 'Bearer '.$this->integrationData->integration_data->tokenSWC
          ],
          'json' => $payload,
        ]);
      }

      $result = Promise\unwrap($response);
      $data = $status = [];
      foreach ($result as $key => $res) {
        if($res->getStatusCode() == 201) {
          $status = 'sent';
        } else {
          Log::error('Failed Broadcast Non HSM Code : (' . json_encode($res->getStatusCode()). ')');
          Log::error('Failed Broadcast Non HSM. Message : (' . json_encode($res->getBody()). ')');
          $status = 'failed';
        }

        $res = json_decode($res->getBody());
        $result = [
          'to' => $number[$key],
          'messageId' => $res->messages[0]->id,
          'status' => $status
        ];

        $payload = [
          'app_id' => $this->integrationData->app_id
        ];

        $data[$key] = $this->formatResponse($result, $payload);
      }
      return $data;
    }

    public function checkTokenAPI()
    {
      $this->integrationData = json_decode($this->data['integration']);

      if (!isset($this->integrationData->integration_data->tokenAPIExpired) || !isset($this->integrationData->integration_data->tokenAPI)) {
        $response = $this->getTokenAPI();
        if (!$response->success) {
          return (object)[
            'success' => false,
            'error'   => $response->error,
            'message' => $response->message,
          ];
        }
        return $response;
      } else {
        $now = Carbon::now('Asia/Jakarta');
        $expired = Carbon::parse($this->integrationData->integration_data->tokenAPIExpired);
        $diffHours = $now->diffInHours($expired, false);
        // if expired
        if ($diffHours <= 0) {
          $response = $this->getTokenAPI();
          if (!$response->success) {
            return (object)[
              'success' => false,
              'error'   => $response->error,
              'message' => $response->message,
            ];
          }
          return $response;
        } else {
          return (object)[
            'success' => true,
            'data'    => null,
            'message' => null,
          ];
        }
      }
    }

    public function getTokenAPI()
    {
      $apiProp = $this->wappinApi->endpoints()->where('name', 'v1-token-get')->first();
      try {
        $response = $this->clientAPI->request($apiProp->method, $apiProp->endpoint, [
            'headers' => [
              'Content-Type' => 'application/json'
            ],
            'auth' => [
              $this->integrationData->integration_data->clientId,
              $this->integrationData->integration_data->secretKey
            ]
        ]);
        $response = (object)[
          'success' => true,
          'data' => json_decode($response->getBody()->getContents())
        ];
        $this->saveTokenAPI($response);
      } catch (RequestException $e) {
        $response = (object)[
          'success' => false,
          'error'   => $e->getResponse()->getBody(true) ?? null,
          'message' => $e->getMessage() ?? null
        ];
      }
      return $response;
    }

    public function saveTokenAPI($response)
    {
      $this->integrationData->integration_data->tokenAPI = $response->data->data->access_token;
      $this->integrationData->integration_data->tokenAPIExpired = $response->data->data->expired_datetime;
      $this->saveIntegration($response);
    }

    public function sendBroadcastHSM($templateData, $responseWaId, $params = [])
    {
      $apiProp = $this->wappinApi->endpoints()->where('name', 'v1-message-do-send-hsm')->first();
      // $phone = [];
      $defaultParams = [];
      foreach ($params as $key => $value) {
        $defaultParams[$key+1] = $value;
      }
      foreach ($responseWaId->data as $key => $phone) {
        $numbers[] = $phone;
        $payload = [
          'client_id' => $this->integrationData->integration_data->clientId,
          'project_id' => $this->integrationData->integration_data->projectId,
          'type' => $templateData->template_name,
          'recipient_number' => $phone,
          'language_code' => $templateData->languange,
          'params' => (object)$defaultParams
        ];
        // Log::info($payload);
        if ($payload['params'] == []) {
          unset($payload['params']);
        };
        $response[$key] = $this->clientAPI->requestAsync($apiProp->method, $apiProp->endpoint, [
          'http_errors' => false,
          'verify' => false,
          'headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.$this->integrationData->integration_data->tokenAPI
          ],
          'json' => $payload
        ]);
      }

      $result = Promise\unwrap($response);
      $msgId = $to = $status = $message = $data = [];
      foreach ($result as $key => $val){
        $val = json_decode($val->getBody());
        $number = preg_replace("/[^0-9]/", "", $numbers[$key]);

        if($val->status !== "200") {
          $status = 'failed';
        } else {
          $status = 'sent';
        }

        $result = [
          'to' => $number,
          'messageId' => $val->message_id,
          'status' => $status,
          'message' => $val->message
        ];

        $payload = [
          'topics' => $templateData->template_name,
          'app_id' => $this->integrationData->app_id,
          'integration_id' => $this->data['integration']->id,
          'send_by' => $this->data['user_id'] ?? $this->data['integration']->integration_data['whatsappName']
        ];

        $data[] = $this->formatResponse($result, $payload);
      }
      $response = (object)[
        'success' => true,
        'data' => $data
      ];
      return $response;
    }

    public function getTemplateData($templateName, $appId)
    {
      $hsm = HsmTemplate::where('app_id', $appId)->where('provider', 'wappin')->where('status', 'active')->where('template_name', $templateName)->first();
      if ($hsm == null) {
        $response = (object)[
          'success' => false,
          'error'   => null,
          'message' => 'HSM Template not found'
        ];
      } else {
        $response = (object)[
          'success' => true,
          'data' => $hsm
        ];
      }
      return $response;
    }

    public function formatResponse($result, $payload)
    {
      $data = (object)[
        'to' => $result['to'],
        'msgId' => $result['messageId'],
        'status' => $result['status'],
        'message' => $result['message'] ?? null
      ];
      BroadcastMessage::create([
        'channel_id' => 4,
        'type' => 'broadcast',
        'category' => 'hsm',
        'client' => 'whatsapp',
        'topics' => $payload['topics'] ?? null,
        'app_id' => $payload['app_id'],
        'status' => $result['status'],
        'number' => $result['to'],
        'data' => $data,
        'integration_id' => $payload['integration_id'],
        'send_by' => $payload['send_by'] ?? null,
        'created_at' => Carbon::now('Asia/Jakarta'),
        'updated_at' => Carbon::now('Asia/Jakarta')
      ]);
      return $data;
    }

    // public function download($filename)
    // {
    //   $name = $filename;
    //   $file = $file = '/home/USERNAME/storage/report/'.$name;

    //   $header = array(
    //       'Content-Type' => 'application/csv',
    //       'Content-Disposition' => 'attachment',
    //       'Content-lenght' => filesize($file),
    //       'filename' => $name,
    //   );

    //   // auth code
    //   return Response::download($file, $name, $header);
    // }

    /*
    public function createGroup($subject)
    {
        $apiProp = $this->wappinApi->endpoints()->where('name', 'v1-groups')->first();
        $requestData = [
            'subject' => $subject
        ];

        $response = $this->client->request(
            $apiProp->method,
            $apiProp->endpoint,
            [
                'http_errors' => false,
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
                "json" => $requestData,
            ]
        );
        $responses = json_decode($response->getBody());

        return $responses;

    }

    public function listGroup()
    {
        $checkToken = $this->checkTokenQontak();

        $apiProp = $this->wappinApi->endpoints()->where('name', 'v1-groups')->first();

        try{
        $response = $this->client->request(
            'get',
            $apiProp->endpoint,
            [
                // 'http_errors' => false,
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
            ]
        );

        }catch (\Exception $e) {
            $resp = json_decode($e->getResponse()->getBody());
            // return $resp->errors[0];
            if($resp->errors[0]->title == 'Access denied') {
                return $response = $this->updateTokenQontak();
            }
        }

        $responses = json_decode($response->getBody());

        // if($responses->errors[0]->details == 'Invalid credentials.') {
        //     // return 'invalid assdda';
        //     return $checkToken = $this->updateTokenQontak();
        // }

        $datas = $subject = [];
        foreach ($responses->groups as $key => $value) {

            $responseGroupInfo = $this->client->request(
                'get',
                $apiProp->endpoint.'/'.$value->id,
                [
                    'http_errors' => false,
                        'verify' => false,
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                        ],
                ]
            );
            $responsesGroupInfo = json_decode($responseGroupInfo->getBody());

            $getInviteLink = $this->client->request(
                'get',
                $apiProp->endpoint.'/'.$value->id.'/invite',
                [
                    'http_errors' => false,
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
                ]
            );
            $responseGetInviteLink = json_decode($getInviteLink->getBody());

            $value->id = $value->id;
            $value->subject = $responsesGroupInfo->groups[0]->subject;
            $value->admins = $responsesGroupInfo->groups[0]->admins;
            $value->participants = $responsesGroupInfo->groups[0]->participants;
            $value->invite = $responseGetInviteLink->groups[0]->link;
            $datas[] = $value;

        }
        return $datas;

    }

    public function setGroupAdmin($groupId, $participants)
    {
        $apiProp = $this->wappinApi->endpoints()->where('name', 'v1-groups')->first();
        $requestData = [
            'wa_ids' => $participants
        ];

        $response = $this->client->request(
            'patch',
            $apiProp->endpoint.'/'.$groupId.'/admins',
            [
                'http_errors' => false,
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
                "json" => $requestData,
            ]
        );
        $responses = json_decode($response->getBody());

        return $responses;
    }

    public function removeGroupAdmin($groupId, $participants)
    {
        $apiProp = $this->wappinApi->endpoints()->where('name', 'v1-groups')->first();
        $requestData = [
            'wa_ids' => $participants
        ];

        $response = $this->client->request(
            'delete',
            $apiProp->endpoint.'/'.$groupId.'/admins',
            [
                'http_errors' => false,
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
                "json" => $requestData,
            ]
        );
        $responses = json_decode($response->getBody());

        return $responses;
    }

    public function removeGroupParticipants($groupId, $participants)
    {
        $apiProp = $this->wappinApi->endpoints()->where('name', 'v1-groups')->first();
        $requestData = [
            'wa_ids' => $participants
        ];

        $response = $this->client->request(
            'delete',
            $apiProp->endpoint.'/'.$groupId.'/participants',
            [
                'http_errors' => false,
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
                "json" => $requestData,
            ]
        );
        $responses = json_decode($response->getBody());

        return $responses;
    }
    */

    public function convertToWhatsappMessageType($message)
    {
      $whatsappMessageType = new WhatsappMessageType;
      $waMessageType = $whatsappMessageType->{$message->type}($message);
      // convert to qontak api need
      switch ($message->type) {
        case 'text':
          $converted = [
            'message' => $waMessageType['text'],
            'type' => 'text'
          ];
          break;
        case 'image':
          $converted = [
            'image' => $waMessageType['imageUrl'],
            'type' => 'image'
          ];
          break;
        case 'file':
          $converted = [
            'document' => $waMessageType['fileUrl'],
            'caption' => $waMessageType['fileName'],
            'type' => 'document'
          ];
          break;
        case 'template':
          $converted = [
            'message' => $waMessageType['text'],
            'type' => 'text'
          ];
          break;
        case 'html':
          $converted = [
            'message' => 'Message type '.$message->type.' not supported',
            'type' => 'text'
          ];
          break;
        default:
          if (isset($waMessageType['multiple'])) {
            $converted = [
              'multiple' => $waMessageType['multiple'],
              'type' => 'text'
            ];
          } else {
            $converted = [
              'message' => $waMessageType['text'],
              'type' => 'text'
            ];
          }
          break;
      }
      return $converted;
    }

    public function convertBroadcastMessage($body)
    {
      switch ($body->type) {
        case 'text':
          $converted = [
            'preview_url' => true,
            'text' => [
              'body' => $body->text
            ]
          ];
          break;
        case 'image':
          $converted = [
            'image' => (object)[
              'link' => $body->originalContentUrl,
              'caption' => $body->caption ?? ''
            ]
          ];
          break;
        default:
          $converted = [
            'text' => [
              'body' => $body->text
            ]
          ];
          break;
      }
      return $converted;
    }

    public function getImageLink($message)
    {
      $mediaId = $message['image']['id'] ?? null;
      if ($mediaId == null) {
        return (object)[
          'success' => false,
          'data'    => env('APP_URL') . '/images/pictures/image_not_found.jpg'
        ];
      }
      $media = $this->getMedia($mediaId, '_wa.png');
      if (!$media->success) {
        return (object)[
          'success' => false,
          'data'    => env('APP_URL') . '/images/pictures/image_not_found.jpg'
        ];
      }
      return $media;
    }

    public function getVideoLink($message)
    {
      $mediaId = $message['video']['id'] ?? null;
      if ($mediaId == null) {
        return (object)[
          'success' => false,
          'data'    => env('APP_URL') . '/videos/default.mp4'
        ];
      }
      $media = $this->getMedia($mediaId, '_wa.mp4');
      if (!$media->success) {
        return (object)[
          'success' => false,
          'data'    => env('APP_URL') . '/videos/default.mp4'
        ];
      }
      return $media;
    }

    public function getVoiceLink($message)
    {
      $mediaId = $message['voice']['id'] ?? null;
      if ($mediaId == null) {
        return (object)[
          'success' => false,
          'data'    => env('APP_URL') . '/audios/default.mp3'
        ];
      }
      $media = $this->getMedia($mediaId, '_wa.mp3');
      if (!$media->success) {
        return (object)[
          'success' => false,
          'data'    => env('APP_URL') . '/audios/default.mp3'
        ];
      }
      return $media;
    }

    public function getAudioLink($message)
    {
      $mediaId = $message['audio']['id'] ?? null;
      if ($mediaId == null) {
        return (object)[
          'success' => false,
          'data'    => env('APP_URL') . '/audios/default.mp3'
        ];
      }
      $media = $this->getMedia($mediaId, '_wa.mp3');
      if (!$media->success) {
        return (object)[
          'success' => false,
          'data'    => env('APP_URL') . '/audios/default.mp3'
        ];
      }
      return $media;
    }

    public function getFileLink($message)
    {
      $mediaId = $message['document']['id'] ?? null;
      $mediaName = $message['document']['filename'];
      if ($mediaId == null) {
        return (object)[
          'success' => false,
          'data'    => env('APP_URL') . '/pictures/image_not_found.jpg'
        ];
      }
      $extention = substr($mediaName, strrpos($mediaName, '.', -1), strlen($mediaName));
      $media = $this->getMedia($mediaId, $extention);
      if (!$media->success) {
        return (object)[
          'success' => false,
          'data'    => env('APP_URL') . '/pictures/image_not_found.jpg'
        ];
      }
      return $media;
    }

    public function getMedia($mediaId, $extention)
    {
      $this->integrationData = json_decode($this->data['integration']);

      // check token
      $responseCheckTokenSWC = $this->checkTokenSWC();
      if (!$responseCheckTokenSWC->success) {
        return (object)['success' => false];
      }

      $apiProp = $this->wappinApi->endpoints()->where('name', 'v1-media')->first();
      try {
        $response = $this->clientSWC->request($apiProp->method, $apiProp->endpoint.'/'.$mediaId, [
          // 'stream' => true,
          // 'http_errors' => false,
          'headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.$this->integrationData->integration_data->tokenSWC
          ],
        ]);
        $data = $response->getBody()->getContents();
        $name = Carbon::now('Asia/Jakarta')->timestamp;
        $name = $name.$extention;
        Storage::disk('public_chat')->put($name, $data);
        $media = env('APP_URL') . '/upload/chat/' . $name;
        $response = (object)[
          'success' => true,
          'data'    => $media
        ];
      } catch (RequestException $e) {
        $resp = json_decode($e->getResponse()->getBody());
        if ($resp->errors->code == 406 || $resp->errors->code == 401) {
          $this->loginSWC();
          $response = (object)[
            'success' => false,
            'error'   => $e->getResponse()->getBody(true) ?? null,
            'message' => $resp->errors->title ?? null,
          ];
        } else if ($resp->errors->code == 901) {
          $response = (object)[
            'success' => false,
            'error'   => $e->getResponse()->getBody(true) ?? null,
            'message' => $resp->errors->title ?? null,
          ];
        } else {
          $response = (object)[
            'success' => false,
            'error'   => $e->getResponse()->getBody(true) ?? null,
            'message' => $e->getMessage() ?? null,
          ];
        }
        Log::error(json_encode($response));
      }
      return $response;
    }

    public function getContact($message)
    {
      $contact = $message['contacts'][0];
      $contact = collect($contact);
      foreach ($contact['phones'] as $key => $value) {
        $columns[] = [
          'field' => $key++,
          'value' => $value['phone']
        ];
      }
      $contactName = $contact['name']['formatted_name'];
      $data = (object)[
        'title' => 'Contact '.$contactName,
        'phones' => $columns
      ];
      return $data;
    }
}
