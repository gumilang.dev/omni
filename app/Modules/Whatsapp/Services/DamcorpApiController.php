<?php

namespace App\Modules\Whatsapp\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\ExternalApi\Models\ExternalApi;
use App\Modules\Log\Models\ApiLog;
use App\Modules\BroadcastMessage\Models\BroadcastMessage;
use GuzzleHttp\Client;
use App\Modules\Whatsapp\Helpers\WhatsappMessageType;
use Illuminate\Support\Facades\Storage;
use App\Modules\Common\Services\EncryptService;
use Carbon\Carbon;


class DamcorpApiController extends Controller
{
    protected $client;
    protected $damcorpApi;

    public function __construct($data)
    {
        $this->data = $data;

        $this->damcorpApi = ExternalApi::where(['category' => 'channel', 'provider' => 'damcorp'])->first();
        $this->damcorpApiGroup = ExternalApi::where(['category' => 'group', 'provider' => 'damcorp'])->first();
        $guzzleProp = [
            'base_uri' => $this->damcorpApi->base_url
        ];
        $this->client = new Client($guzzleProp);
    }

    public function callSendApi($message, $type)
    {
        $apiProp = $this->damcorpApi->endpoints()->where('name', "send-$type")->first();

        $requestData = [
            'token' => $this->data['integration']->integration_data['apiKey'],
            'to' => "+" . $this->data['chatRoom']->created_by->phone,
        ];

        $requestData = array_merge($requestData, $message);
        unset($requestData['type']);

        $response = $this->client->request(
            $apiProp->method,
            $apiProp->endpoint,
            [
                "json" => $requestData,
            ]
        );
        $response = json_decode($response->getBody()->getContents());
        return $response;
    }

    public function callSendHsmApi($templateName, $phone, $params = [])
    {

        $apiProp = $this->damcorpApi->endpoints()->where('name', "send-hsm")->first();
        $phonePlus = [];
        foreach ($phone as $key => $singlePhone) {
            $singlePhone = preg_replace("/[^0-9]/", "", $singlePhone);
            if (!strstr($singlePhone, "+")) {
                $phonePlus[] = "+$singlePhone";
            }
        }
        $requestData = [
            'token' => $this->data['integration']->integration_data['apiKey'],
            // 'to' => (is_array($phone)) ? $phonePlus : "+" . $phone,
            'to' => $phonePlus,
            'param' => $params
        ];

        $response = $this->client->request(
            $apiProp->method,
            $apiProp->endpoint . "/$templateName",
            [
                "json" => $requestData,
            ]
        );
        $responses = json_decode($response->getBody());

        $result = json_decode($response->getBody())->data;
        $to = $msgId = $status = $rsltData = [];
        foreach ($result as $key => $r) {
            //  $data[$key] = $r;
             $to[$key] = $r->to;
             $msgId[$key] = $r->msgId;
             $status[$key] = $r->status;
             $rsltData[$key] = $r;
        }
        $datas = [];
        foreach ($result as $key => $r) {
            $r->to = $to[$key];
            $r->msgId = $msgId[$key];
            $r->status = $status[$key];
            // $list->msgId = $id[$key];
            // $list->status = $status[$key];
            $datas[$key] = $r;

            if ($to[$key] == '+') {
                $to[$key] = 'null';
            }

            BroadcastMessage::create([
                'channel_id' => 4,
                'type' => 'broadcast',
                'category' => 'hsm',
                'client' => 'whatsapp',
                'topics' => $templateName,
                'app_id' => $this->data['integration']->app_id,
                'status' => $status[$key],
                'number' => $to[$key],
                'data' => $rsltData[$key],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            // ApiLog::create([
            //     'app_id' => $this->data['integration']->app_id,
            //     'user_id' => $this->data['user_id'],
            //     'request' => 'hsm:'.$templateName,
            //     'response' => $status[$key],
            //     'number' => $to[$key]
            // ]);

        }

        return $responses->data;

    }

    public function callSendBroadcastNonHsm($newMessage, $phone)
    {
        foreach ($newMessage->content as $key => $value) {
            $converted = $this->_convertToWhatsappMessageType($value);
            // dd($converted);
            if (isset($converted['multiple'])) {
              foreach ($converted['multiple'] as $key => $text) {
                $response = $this->sendBroadcastNonHsm(['message' => $text], $converted['type'], $phone);
              }
            } else {
              $response = $this->sendBroadcastNonHsm($converted, $converted['type'], $phone);

              }
          }
        //   dd($converted);
          return $response;
    }

    public function sendBroadcastNonHsm($message, $type, $phone){
        // dd($message);
        $apiProp = $this->damcorpApi->endpoints()->where('name', "send-$type")->first();
        $phonePlus = [];

        foreach ($phone as $key => $singlePhone) {
            if (!strstr($singlePhone, "+")) {
                $phonePlus[] = "+".$singlePhone;
            }
        }

        $requestData = [
            'token' => $this->data['integration']->integration_data['apiKey'],
            // 'to' => (is_array($phone)) ? $phonePlus : "+" . $phone,
            'to' => $phonePlus,
        ];
        $requestData = array_merge($requestData, $message);
        unset($requestData['type']);

        $response = $this->client->request(
            $apiProp->method,
            $apiProp->endpoint,
            [
                "json" => $requestData,
            ]
        );
        $responses = json_decode($response->getBody(), TRUE);
        $result = json_decode($response->getBody())->data;

        $finalArray = array();
        foreach ($result as $key => $value) {
            array_push($finalArray, array(
                'channel_id' => 4,
                'type' => 'broadcast',
                'category' => 'non-hsm',
                'client' => 'whatsapp',
                'status' => $value->status,
                'number' => $value->to,
                'data' => json_encode($message),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ));
        }
        // mass insert
        BroadcastMessage::insert($finalArray);
        return $result;
    }

    public function sendMessage($newMessage)
    {
      // $decryptedContent = EncryptService::decrypt($newMessage->content[0]->data, env('ENCRYPT_MESSAGE_KEY'), 20);
      // $decryptedContent = json_decode($decryptedContent);
        foreach ($newMessage->content as $key => $content) {
          $converted = $this->_convertToWhatsappMessageType($content);

          if (isset($converted['multiple'])) {

            foreach ($converted['multiple'] as $key => $text) {
              $response = $this->callSendApi(['message' => $text], $converted['type']);
            }
          } else {
            // dd($converted);
            $response = $this->callSendApi($converted, $converted['type']);

            }
        }
        return $response;
    }

    public function sendHsm($templateName, $phone, $params = [])
    {


        $response = $this->callSendHsmApi($templateName, $phone, $params);

        return $response;
    }

    public function _convertToWhatsappMessageType($message)
    {
        $whatsappMessageType = new WhatsappMessageType;
        $waMessageType = $whatsappMessageType->{$message->type}($message);
        // convert to damcorp api need
        switch ($message->type) {
            case 'image':
                $converted = [
                    'image' => $waMessageType['imageUrl'],
                    'caption' => $waMessageType['caption'],
                    'type' => 'image'
                ];
                break;
            case 'file':
                $converted = [
                    'document' => $waMessageType['fileUrl'],
                    'caption' => $waMessageType['fileName'],
                    'type' => 'document'
                ];
                break;
            case 'template':
                $converted = [
                    'message' => $waMessageType['text'],
                    'type' => 'text'
                ];
                break;
            default:
                if (isset($waMessageType['multiple'])) {
                    $converted = [
                        'multiple' => $waMessageType['multiple'],
                        'type' => 'text'
                    ];
                } else {
                    $converted = [
                        'message' => $waMessageType['text'],
                        'type' => 'text'
                    ];
                }
                break;
        }

        return $converted;
    }

    public function createGroup($subject)
    {
        $apiProp = $this->damcorpApi->endpoints()->where('name', 'create-group')->first();
        $requestData = [
            'token' => $this->data['integration']->integration_data['apiKey'],
            'subject' => $subject
        ];


        $response = $this->client->request(
            $apiProp->method,
            $apiProp->endpoint,
            [
                "json" => $requestData,
            ]
        );
        $responses = json_decode($response->getBody());

        return $responses;
    }

    public function listGroup()
    {
        $apiProp = $this->damcorpApi->endpoints()->where('name', 'list-group')->first();

        $requestData = [
            'token' => $this->data['integration']->integration_data['apiKey']
        ];

        $response = $this->client->request(
            $apiProp->method,
            $apiProp->endpoint,
            [
                "query" => $requestData,
            ]
        );
        $responses = json_decode($response->getBody());

        return $responses;
    }

    public function getInviteGroup($groupId)
    {
        $apiProp = $this->damcorpApi->endpoints()->where('name', 'list-group')->first();

        $requestData = [
            'token' => $this->data['integration']->integration_data['apiKey']
        ];

        $response = $this->client->request(
            $apiProp->method,
            $apiProp->endpoint.'/'.$groupId.'/invite',
            [
                "query" => $requestData,
            ]
        );
        $responses = json_decode($response->getBody());

        return $responses;
    }

    public function setGroupAdmin($groupId, $participants)
    {
        $number = [];
        foreach ($participants as $key => $p) {
            if (strstr($p, "62")) {
                $number[] = substr_replace($p, '0', 0,2);
            }
        }

        $apiProp = $this->damcorpApi->endpoints()->where('name', 'list-group')->first();
        $requestData = [
            'token' => $this->data['integration']->integration_data['apiKey'],
            'participants' => $number
        ];

        $response = $this->client->request(
            'PUT',
            $apiProp->endpoint.'/'.$groupId.'/admin',
            [
                "json" => $requestData,
            ]
        );
        $responses = json_decode($response->getBody());

        return $responses;
    }

    public function removeGroupAdmin($groupId, $participants)
    {
        $number = [];
        foreach ($participants as $key => $p) {
            if (strstr($p, "62")) {
                $number[] = substr_replace($p, '0', 0,2);
            }
        }

        $apiProp = $this->damcorpApi->endpoints()->where('name', 'list-group')->first();
        $requestData = [
            'token' => $this->data['integration']->integration_data['apiKey'],
            'participants' => $number
        ];

        $response = $this->client->request(
            'delete',
            $apiProp->endpoint.'/'.$groupId.'/admin',
            [
                "json" => $requestData,
            ]
        );
        $responses = json_decode($response->getBody());

        return $responses;
    }

    public function removeGroupParticipants($groupId, $participants)
    {
        $number = [];
        foreach ($participants as $key => $p) {
            if (strstr($p, "62")) {
                $number[] = substr_replace($p, '0', 0,2);
            }
        }

        $apiProp = $this->damcorpApi->endpoints()->where('name', 'list-group')->first();
        $requestData = [
            'token' => $this->data['integration']->integration_data['apiKey'],
            'participants' => $number
        ];

        $response = $this->client->request(
            'delete',
            $apiProp->endpoint.'/'.$groupId.'/participants',
            [
                "json" => $requestData,
            ]
        );
        $responses = json_decode($response->getBody());

        return $responses;
    }

    public function updateGroupSubject($groupId, $subject)
    {
        $apiProp = $this->damcorpApi->endpoints()->where('name', 'list-group')->first();
        $requestData = [
            'token' => $this->data['integration']->integration_data['apiKey'],
            'subject' => $subject
        ];

        $response = $this->client->request(
            'put',
            $apiProp->endpoint.'/'.$groupId,
            [
                "json" => $requestData,
            ]
        );
        $responses = json_decode($response->getBody());

        return $responses;
    }

    public function setGroupIcon($groupId, $base64img)
    {
        return $base64img;
        $apiProp = $this->damcorpApi->endpoints()->where('name', 'list-group')->first();
        $requestData = [
            'token' => $this->data['integration']->integration_data['apiKey'],
            'icon' => $base64img
        ];

        $response = $this->client->request(
            'post',
            $apiProp->endpoint.'/'.$groupId.'/icon',
            [
                'http_errors' => false,
                "json" => $requestData,
            ]
        );
        $responses = json_decode($response->getBody());

        return $responses;
    }

    public function getImageLink($message)
    {
        $requestData = [
            'token' => $this->data['integration']->integration_data['apiKey'],
            'id' => $message['image']['id']
        ];

        $response = $this->client->request(
            'get',
            'whatsapp/downloadMedia',
            [
                // 'http_errors' => false,
                "query" => $requestData,
            ]
        );

        $responses = json_decode($response->getBody(), TRUE);
        $data = $responses['data'];
        $dataDecode = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data));

        $name = rand().'_wa.jpeg';
        Storage::disk('public_chat')->put($name, $dataDecode);
        // echo '<img src="'.$data.'">';

        return env("APP_URL") . "/upload/chat/" . $name;
    }

    public function getVideoLink($message)
    {
        $requestData = [
            'token' => $this->data['integration']->integration_data['apiKey'],
            'id' => $message['video']['id']
        ];

        $response = $this->client->request(
            'get',
            'whatsapp/downloadMedia',
            [
                // 'http_errors' => false,
                "query" => $requestData,
            ]
        );

        $responses = json_decode($response->getBody(), TRUE);
        $data = $responses['data'];
        $dataDecode = base64_decode(preg_replace('#^data:video/\w+;base64,#i', '', $data));

        $name = rand().'_wa.mp4';
        Storage::disk('public_chat')->put($name, $dataDecode);

        $videoUrl = env("APP_URL") . "/upload/chat/" . $name;
        return [
            'videoUrl' => $videoUrl
        ];
    }

    public function getFileLink($message)
    {
        $requestData = [
            'token' => $this->data['integration']->integration_data['apiKey'],
            'id' => $message['document']['id']
        ];

        $mediaId = $message['document']['id'];
        $mediaName = $message['document']['filename'];

        $response = $this->client->request(
            'get',
            'whatsapp/downloadMedia',
            [
                // 'http_errors' => false,
                "query" => $requestData,
            ]
        );

        $responses = json_decode($response->getBody(), TRUE);
        // dd($responses);
        $data = $responses['data'];
        $dataDecode = base64_decode(preg_replace('#^data:application/\w+;base64,#i', '', $data));
        // $datas = $response->getBody()->getContents();
        // $getExtension = explode('/', mime_content_type($data));

        // if ($getExtension[1] == 'vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
        //     $fileExtension = '.xlsx';
        // } else if ($getExtension[1] == 'vnd.openxmlformats-officedocument.wordprocessingml.document') {
        //     $fileExtension = '.docx';
        // }

        // dd($extension);

        // $name = rand().'_wa.mp4';
        Storage::disk('public_chat')->put($mediaName.'.xlsx', $dataDecode);

        $fileUrl = env("APP_URL") . "/upload/chat/" . $mediaName.'.xlsx';
        return [
            'fileUrl' => $fileUrl
        ];
    }


}
