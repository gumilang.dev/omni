<?php

namespace App\Modules\Whatsapp\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\ExternalApi\Models\ExternalApi;
use GuzzleHttp\Client;
use App\Modules\Whatsapp\Helpers\WhatsappMessageType;

class InfobipApiController extends Controller
{
    protected $client;
    protected $infobipApi;

    public function __construct($data)
    {
        $this->data = $data;

        $this->infobipApi = ExternalApi::where(['category' => 'channel', 'provider' => 'infobip'])->first();
        $guzzleProp = [
            'base_uri' => $this->data['integration']->integration_data['whatsappBaseUrl']
        ];
        $this->client = new Client($guzzleProp);
    }

    public function callSendApi($message)
    {
        $apiProp = $this->infobipApi->endpoints()->where('name', 'send-advance-message')->first();

        $requestData = [
            'scenarioKey' => $this->data['integration']->integration_data['scenarioKey'],
            'destinations' => [
                (object)[
                    "to" => (object)[
                        "phoneNumber" => $this->data['chatRoom']->created_by->channel_user_id
                    ]
                ]
            ],
            'whatsApp' => (object)$message
        ];
        $response = $this->client->request(
            $apiProp->method,
            $apiProp->endpoint,
            [
                "json" => $requestData,
                'headers' => ['Authorization' => 'App ' . $this->data['integration']->integration_data['apiKey']]
            ]
        );
        $response = json_decode($response->getBody()->getContents());
        return $response;
    }

    public function sendAdvanceMessage($newMessage)
    {

        foreach ($newMessage->content as $key => $content) {
            $converted = $this->convertToWhatsappMessageType($content);

            if (isset($converted['multiple'])) {

                foreach ($converted['multiple'] as $key => $text) {

                    $response = $this->callSendApi(['text' => $text]);
                }
            } else {
                $response = $this->callSendApi($converted);

            }
        }
        return $response;

    }

    public function sendTemplateMessage($message, $template)
    {
        $templateMessage = [
            'templateName' => $template['templateName'],
            'templateNamespace' => $template['templateNamespace'] ?? '',
            'templateData' => $template['templateData'],
            'language' => 'en',
        ];
        $response = $this->callSendApi($templateMessage);

        return $response;

    }

    public function convertToWhatsappMessageType($message)
    {

        $whatsappMessageType = new WhatsappMessageType;
        $message = $whatsappMessageType->{$message->type}($message);
        return $message;
    }
}
