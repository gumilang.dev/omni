<?php

namespace App\Modules\Whatsapp\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\ExternalApi\Models\ExternalApi;
use GuzzleHttp\Client;
use App\Modules\Whatsapp\Helpers\WhatsappMessageType;

class ApiWhaApiController extends Controller
{
    protected $client;
    protected $whatsappApi;

    public function __construct($data)
    {
        $this->data = $data;

        $this->whatsappApi = ExternalApi::where(['category' => 'channel', 'provider' => 'whatsapp apiwha'])->first();
        $guzzleProp = [
            'base_uri' => $this->whatsappApi->base_url
        ];
        $this->client = new Client($guzzleProp);
    }


    public function sendMessage($newMessage)
    {
        $apiProp = $this->whatsappApi->endpoints()->where('name', 'send-message')->first();

        if (count($newMessage->content) > 1) {
            $message = '';
            foreach ($newMessage->content as $key => $content) {
                $message .= $this->convertToWhatsappMessageType($content) . "\n";

            }
        } else {
            $message = $this->convertToWhatsappMessageType($newMessage->content[0]);

        }

        $requestData = [
            'apikey' => $this->data['integration']->integration_data['apiKey'],
            'number' => $this->data['chatRoom']->created_by->channel_user_id,
            'text' => $message
        ];
        // dd($requestData);
        $response = $this->client->request(
            $apiProp->method,
            $apiProp->endpoint,
            [
                "form_params" => $requestData
            ]
        );
        $response = json_decode($response->getBody()->getContents());
        return $response;
    }

    public function convertToWhatsappMessageType($message)
    {
        $whatsappMessageType = new WhatsappMessageType;
        switch ($message->type) {
            case 'text':
                $message = $whatsappMessageType->text(['text' => $message->text])['text'];
                break;
            case 'image':
                $message = $whatsappMessageType->image(['previewImageUrl' => $message->previewImageUrl])['imageUrl'];
                break;
            case 'audio':
                $message = $whatsappMessageType->audio(['originalContentUrl' => $message->originalContentUrl]);
                break;
            case 'carousel':
                $message = $whatsappMessageType->carousel([
                    'columns' => $message->columns,
                    'imageAspectRatio' => $message->imageAspectRatio,
                    'imageSize' => $message->imageSize,
                ])['text'];
                break;
            case 'confirm':
                $message = $whatsappMessageType->confirm([
                    'actions' => $message->actions,
                    'text' => $message->text,
                ])['text'];
                break;
      // case 'buttons':
      //   $message =$whatsappMessageType->carousel([
      //     'columns' => $message->columns,
      //     'imageAspectRatio'=>$message->imageAspectRatio,
      //     'imageSize'=>$message->imageSize,
      //   ]);
      //   break;
            default:
        # code...
                break;
        }
        return $message;
    }
}
