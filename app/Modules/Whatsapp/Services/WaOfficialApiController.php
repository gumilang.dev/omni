<?php

namespace App\Modules\Whatsapp\Services;
ini_set("memory_limit", "-1");
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\ExternalApi\Models\ExternalApi;
use App\Modules\Integration\Models\Integration;
use App\Modules\BroadcastMessage\Models\BroadcastMessage;
use GuzzleHttp\Client;
use App\Modules\Whatsapp\Helpers\WhatsappMessageType;

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\RequestGuzzle;
use Carbon\Carbon;
use App\Modules\Log\Models\ApiLog;
use Psr\Http\Message\StreamInterface;
use Illuminate\Support\Facades\Storage;
use App\Modules\Common\Services\EncryptService;

class WaOfficialApiController extends Controller
{
    protected $client;
    protected $officialApi;

    public function __construct($data)
    {
        $this->data = $data;
        $this->header = [];
        $this->footer = [];
        $this->button = [];
        $this->buttonParamCTA = [];
        $this->buttonParamQR = [];
        $this->officialApi = ExternalApi::where(['category' => 'channel', 'provider' => 'qontak'])->first();
        $guzzleProp = [
            'base_uri' => $this->data['integration']->integration_data['whatsappBaseUrl']
        ];
        $this->client = new Client($guzzleProp);
    }

    public function sendMessage($newMessage)
    {
        // $decryptedContent = EncryptService::decrypt(json_encode($newMessage->content), env('ENCRYPT_MESSAGE_KEY'), 20);
        // $newMessage->content = json_encode($newMessage->content);
        foreach ($newMessage->content as $key => $content) {
            $converted = $this->_convertToWhatsappMessageType($content);

            if (isset($converted['multiple'])) {

                foreach ($converted['multiple'] as $key => $text) {

                    $response = $this->callSendApi(['message' => $text], $converted['type']);
                }
            } else {
                $response = $this->callSendApi($converted, $converted['type']);
            }
        }
        return $response;
    }

    public function callSendApi($message, $type)
    {
        $phone = (array) $this->data['chatRoom']->created_by->phone;
        $params = $message;

        $templateName = $type;
        $response = $this->checkToken($templateName, $phone, $params);
        return $response;
    }

    public function sendHsm($templateName, $phone, $params = [], $optional = [])
    {
        $this->header = $optional['header'] ?? null;
        $this->footer = $optional['footer'] ?? null;
        $this->button = $optional['button'] ?? null;
        $this->buttonParamCTA = $optional['buttonParamCTA'] ?? null;
        $this->buttonParamQR = $optional['buttonParamQR'] ?? null;
        // dd($optional);
        $response = $this->checkToken($templateName, $phone, $params);
        return $response;
    }

    public function checkToken($templateName, $phone, $params = [])
    {
        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-users-login')->first();
        $integrationData = json_decode($this->data['integration']);

        //decode integration data for check token is null or not
        if ($integrationData->integration_data->token == null) {
            try {
                $response = $this->client->request(
                    $apiProp->method,
                    $apiProp->endpoint,
                    [
                        'verify' => false,
                        'auth' => [
                            $this->data['integration']->integration_data['username'],
                            $this->data['integration']->integration_data['password']
                        ],
                        'headers' => [
                            'Content-Type' => 'application/json',
                            // 'Authorization' => 'Basic '.$this->officialApi->remark //bisa dapat token dari auth atau dari headers menggunakan encode base64 format username:password
                        ]
                        // 'json' => $requestData,
                    ]
                );

            }catch (\Exception $e) {
                $resp = json_decode($e->getResponse()->getBody());
                return $resp->errors[0];
            }
            $response = json_decode($response->getBody());
            $integrationData->integration_data->token = $response->users[0]->token;

            $appId = $this->data['integration']->app_id;
            $integrationId = $this->data['integration']->id;
            $integration = Integration::where(['id' => $integrationId, 'status' => true, 'channel_id' => 4])->first();

            $integration->integration_data = $integrationData->integration_data;
            $this->data['integration'] = $integration; //update construct data integration
            $integration->save();

            $response = $this->checkWaId($templateName, $phone, $params);
            return $response;
        }

        return $response = $this->checkWaId($templateName, $phone, $params);
    }

    public function updateToken($templateName, $phone, $params = [])
    {
        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-users-login')->first();
        $integrationData = json_decode($this->data['integration']);

        try {
            $response = $this->client->request(
                $apiProp->method,
                $apiProp->endpoint,
                [
                    'verify' => false,
                    'auth' => [
                        $this->data['integration']->integration_data['username'],
                        $this->data['integration']->integration_data['password']
                    ],
                    'headers' => [
                        'Content-Type' => 'application/json',
                        // 'Authorization' => 'Basic '.$this->officialApi->remark //bisa dapat token dari auth atau dari headers menggunakan encode base64 format username:password
                    ]
                    // 'json' => $requestData,
                ]
            );

        }catch (\Exception $e) {
            $resp = json_decode($e->getResponse()->getBody());
            return $resp->errors[0];
        }
        $response = json_decode($response->getBody());
        $integrationData->integration_data->token = $response->users[0]->token;
        $appId = $this->data['integration']->app_id;

        $integrationId = $this->data['integration']->id;
        $integration = Integration::where(['id' => $integrationId, 'status' => true, 'channel_id' => 4])->first();

        $integration->integration_data = $integrationData->integration_data;
        $this->data['integration'] = $integration; //update construct data integration
        $integration->save();
        $response = $this->checkWaId($templateName, $phone, $params);
        return $response;

    }

    public function checkWaId($templateName, $phone, $params = [])
    {
        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-contacts')->first();
        $phonePlus = [];
        // $list->to = preg_replace("/[^0-9]/", "", $numbers[$key]);
        foreach ($phone as $key => $singlePhone) {
            $singlePhone = preg_replace("/[^0-9]/", "", $singlePhone);
            if(!strstr($singlePhone, "+")){
                $phonePlus[] = "+$singlePhone";
            }
        }
        // print_r($phonePlus); die()
        $requestData = [
            "blocking" => "wait",
            "contacts" => $phonePlus
        ];

        try {
        $response = $this->client->request(
            $apiProp->method,
            $apiProp->endpoint,
            [
                'verify' => false,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                ],
                'json' => $requestData,
            ]
        );

        }catch (\Exception $e) {
            $resp = json_decode($e->getResponse()->getBody());
            // return $resp->errors[0];
            if ($resp->errors[0]->code == 1005) {
                return $response = $this->updateToken($templateName, $phone, $params);
            }

        }


        $response = json_decode($response->getBody());

        $wa_id = [];
        foreach ($response->contacts as $key => $value) {
            if ($value->status == "valid"){
                $wa_id[$key] = $value->wa_id;
            } else if ($value->status == "invalid"){
                $wa_id[$key] = preg_replace("/[^0-9]/", "",$value->input);

            }

        }

        if ($templateName == 'text') {
            return $response = $this->sendMessageOfficialApi($wa_id, $params);
        } else if ($templateName == 'image') {
            return $response = $this->sendImageOfficialApi($wa_id, $params);
        } else if ($templateName == 'document') {
            return $response = $this->sendDocOfficialApi($wa_id, $params);
        } else if ($templateName == 'bctext' || $templateName == 'bcimage') {
            return $response = $this->sendBcMsgNonHsm($templateName, $wa_id, $params);
        } else {
            return $response = $this->sendHsmOfficialApi($templateName, $wa_id, $params);
        }


    }

    public function sendBcMsgNonHsm($templateName, $wa_id, $params)
    {
        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-messages')->first();
        $phone = [];

        $convertOfficialType = $this->_convertOfficialMsgType($params);
        // dd($convertOfficialType);
        foreach ($wa_id as $key => $phone) {
            $numbers[] = $phone;

            $requestData = [
                "to" => $phone,
                "type" => $params['type'],
                "recipient_type" => "individual"
            ];
            $requestData = array_merge($requestData, $convertOfficialType);

            $response[$key] = $this->client->requestAsync(
                $apiProp->method,
                $apiProp->endpoint,
                [
                    'http_errors' => false,
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
                    'json' => $requestData,
                ]
            );
        }

        $result = Promise\unwrap($response);
        $id = $status = [];
        foreach ($result as $key => $list) {
            $dataResult = json_decode($list->getBody());
            // $msgId = $dataResult->messages[0]->id;
            if (isset($dataResult->messages[0]->id)) {
                $list->msgId = $dataResult->messages[0]->id;
                $list->status = 'sent';
                $list->to = $numbers[$key];
            } else {
                $list->msgId = null;
                $list->status = 'failed';
                $list->to = $numbers[$key];
            }
            $data[$key] = $list;
        }

        return $data;


    }

    public function sendDocOfficialApi($wa_id, $params)
    {
        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-messages')->first();

        try {
            // $result = Promise\unwrap($response);
            $response = $this->client->request(
                $apiProp->method,
                $apiProp->endpoint,
                [
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
                    'json' => [
                        "to" => $wa_id[0],
                        "type" => "document",
                        "recipient_type" => "individual",
                        "document" => [
                                "link" => $params['document'],
                                "filename" => $params['caption']
                            ]
                        ],
                        ]
                    );
                }catch (\Exception $e) {
                    $resp = json_decode($e->getResponse()->getBody());
                    return $resp->errors[0];
                    // return $resp->errors[0];
                    // return ['errors' => $resp->errors[0]];

                }
                return $response =  json_decode($response->getBody());
    }

    public function sendImageOfficialApi($wa_id, $params)
    {
        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-messages')->first();

        try {
            // $result = Promise\unwrap($response);
            $response = $this->client->request(
                $apiProp->method,
                $apiProp->endpoint,
                [
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
                    'json' => [
                        "to" => $wa_id[0],
                        "type" => "image",
                        "recipient_type" => "individual",
                        "image" => [
                                "link" => $params['image']
                            ]
                        ],
                        ]
                    );
                }catch (\Exception $e) {
                    $resp = json_decode($e->getResponse()->getBody());
                    return $resp->errors[0];
                    // return $resp->errors[0];
                    // return ['errors' => $resp->errors[0]];

                }
                return $response =  json_decode($response->getBody());
    }

    public function sendMessageOfficialApi($wa_id, $params)
    {
        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-messages')->first();

        try {
            // $result = Promise\unwrap($response);
            $response = $this->client->request(
                $apiProp->method,
                $apiProp->endpoint,
                [
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
                    'json' => [
                        "to" => $wa_id[0],
                        "type" => "text",
                        "recipient_type" => "individual",
                        "text" => [
                            "body" => $params['message']
                            ]
                        ],
                        ]
                    );
                }catch (\Exception $e) {
                    $resp = json_decode($e->getResponse()->getBody());
                    return $resp->errors[0];
                    // return $resp->errors[0];
                    // return ['errors' => $resp->errors[0]];

                }
                return $response =  json_decode($response->getBody());


    }

    public function setWebhook($webhookUrl)
    {
        $this->forceUpdateToken();
        try {
            $response = $this->client->request("PATCH", "v1/settings/application",
        [
            'verify' => false,
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
            ],
            'json' => [
                "webhooks" => [
                    "url" => $webhookUrl
                ]
            ]
        ]);
        } catch (\Exception $e) {
            $respError = json_decode($e->getResponse()->getBody());
            return $respError->errors[0];
        }
        return $response;
    }

    public function sendHsmOfficialApi($templateName, $wa_id, $params = [])
    {
        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-messages')->first();

        $phone = [];
        $defaultParams = [];
        foreach ($params as $key => $value) {
            $data = ["default"=>$value];
            $defaultParams[] = $data;
            $bodyParam = ["type"=>"text", "text"=>$value];
            $bodyParams[] = $bodyParam;
        }

        foreach ($wa_id as $key => $phone) {
            $numbers[] = $phone;
            $components = $headerPayload = $footerPayload = $buttonPayload = $buttonPayloadCTA = $bodyPayload = [];
            if ($this->header && $this->header !== "[]") {
                if ($this->header['headerType'] == "media") {
                    $headerPayload = [
                        "type" => "header",
                        "parameters" => [(object)[ //maybe next header can be multiple image
                            "type" => "image",
                            "image" => [
                                // "link" => "https://dev.lenna.ai/botstudio/public/storage/uploaded/4wsmwbr9IQBkuym8mNYKbWzXt9IAgjOKWLNLEEx3.jpeg"
                                "link" => $this->header["mediaUrl"]
                            ]
                        ]]
                    ];
                }
                if ($this->header['headerType'] == "text") {
                    $headerPayload = [
                        "type" => "header",
                        "text" => "text-header"
                    ];
                }
            }

            if ($bodyParams) {
                $bodyPayload = [
                    "type" => "body",
                    "parameters" => $bodyParams
                ];
            }
            if ($this->footer && $this->footer !== "[]") {
                $footerPayload = [
                    "type" => "footer",
                    "text" => "footer-text"
                ];
            }

            if ($this->button && $this->button !== "[]") {
                if ($this->button['buttonType'] == "call-to-action") {
                    foreach ($this->button['callToAction'] as $key2 => $value) {
                        if ($value["callToActionType"] == "visit website") {
                            if ($value["urlType"] == "static") {
                                //null / do nothing
                            } else {
                                $buttonPayloadVW = [
                                    "type" => "button",
                                        "sub_type" => "url",
                                        "index" => "$key2",
                                        "parameters" => [(object)[
                                            "type" => "text",
                                            "text" => $this->buttonParamCTA[$key2]
                                        ]]
                                ];
                                array_push($components, $buttonPayloadVW);
                            }
                        } else if ($value["callToActionType"] == "call phone number"){
                            //apinya belum ada
                            // $buttonCallToActionCPN = [
                            //     "type" => "button",
                            //         "sub_type" => "url",
                            //         "index" => "0",
                            //         "parameters" => [(object)[
                            //             "type" => "text",
                            //             "text" => $this->buttonParamCTA[0]
                            //         ]]
                            // ];
                            // array_push($components, $buttonCallToActionCPN);
                        }
                    }


                } else if ($this->button['buttonType'] == "quick-reply") {
                    foreach ($this->button['quickReply'] as $key => $value) {
                        $buttonPayloadQR = [
                            "type" => "button",
                            "sub_type" => "quick_reply",
                            "index" => "1",
                            "parameters" => (object)[
                                "type" => "payload",
                                "payload" => $value['text']
                                // "payload" => $this->button["quickReply"][0]["text"]
                            ]
                        ];
                        array_push($components, $buttonPayloadQR);
                    }
                }
            }

            array_push($components, $headerPayload, $bodyPayload, $footerPayload, $buttonPayload);
            $components = array_values(array_filter($components));
            $components = array_filter($components);
            // return $components; die();
            // dd($components);

            $payload = [
                "to" => $phone,
                "type" => "template",
                "template" => [
                    "namespace" => $this->data['integration']->integration_data['namespaceHsm'],
                    "name" => $templateName,
                    "language" => [
                        "policy" => "deterministic",
                        "code" => "id"
                    ],
                    "components" => (object)$components
                ]
            ];

            // dd($payload);

            $response[$key] = $this->client->requestAsync(
                $apiProp->method,
                $apiProp->endpoint,
                [
                    'http_errors' => false,
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
                    'json' => $payload
                ]
            );

            if(($key !== 0) && ($key%20) == 0){ // % = sisa bagi
                sleep(1); // this should halt for 1 seconds for every loop
            }
        }

        // $result = $response;
        $result = Promise\unwrap($response);


        $id = $status = $datas = [];
        foreach ($result as $key => $list){
            $dataResult = json_decode($list->getBody());
            $dataResult2[] = json_decode($list->getBody());
            if(isset($dataResult->errors[0]->href)) {
                return [
                    'title' => $dataResult->errors[0]->title,
                    'details' => $dataResult->errors[0]->details
                ];
            }
            if(isset($dataResult->messages[0]->id)) {
                $id[$key] = $dataResult->messages[0]->id;
                $status[$key] = 'sent';
            } else {
                $id[$key] = null;
                $status[$key] = 'failed';
                $errorMsg[$key] = $dataResult->errors[0]->details ?? null;
            }

            $list->to = preg_replace("/[^0-9]/", "", $numbers[$key]);
            $list->msgId = $id[$key];
            $list->status = $status[$key];
            $list->message = $errorMsg[$key] ?? null;
            $datas[$key] = $list;

            BroadcastMessage::create([
                'channel_id' => 4,
                'type' => 'broadcast',
                'category' => 'hsm',
                'client' => 'whatsapp',
                'topics' => $templateName,
                'app_id' => $this->data['integration']->app_id,
                'status' => $status[$key],
                'number' => preg_replace("/[^0-9]/", "",$list->to),
                'data' => $datas[$key],
                'integration_id' => $this->data['integration']->id,
                'send_by' => $this->data['user_id'] ?? null,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

        }

        return $datas;

    }

    public function callSendBroadcastNonHsm($newMessage, $phone)
    {
        foreach ($newMessage->content as $key => $content) {
            $converted = $this->_convertToWhatsappMessageType($content);
            if (isset($converted['multiple'])) {

                foreach ($converted['multiple'] as $key => $text) {

                    $response = $this->callSendApiNonHsm(['message' => $text], $converted['type'], $phone);
                }
            } else {
                $response = $this->callSendApiNonHsm($converted, $converted['type'], $phone);
            }
        }
        return $response;
    }

    public function callSendApiNonHsm($message, $type, $phone)
    {
        $templateName = "bc".$type;
        $params = $message;
        $response = $this->checkToken($templateName, $phone, $params);
        return $response;
    }

    public function sendBroadcastNonHsm($content, $phones)
    {
        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-messages')->first();
        $msg = $content[0]['text'];
        // $number = [];

        foreach ($phones as $key => $singlePhone) {
            $numbers[] = $singlePhone;

            $response[$key] = $this->client->requestAsync(
                $apiProp->method,
                $apiProp->endpoint,
                [
                    'http_errors' => false,
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
                    'json' => [
                        "to" => $singlePhone,
                        "type" => "text",
                        "recipient_type" => "individual",
                        "text" => [
                            "body" => $msg
                            ]
                        ],
                ]

            );
        }

        $result = Promise\unwrap($response);
        $id = $status = $datas = $resp = [];
        $finalArray = array();

        foreach ($result as $key => $r) {
            $dataResult = json_decode($r->getBody());

            if(isset($dataResult->messages[0]->id)) {
                $resp[$key]['to'] = $numbers[$key];
                $resp[$key]['msgId'] = $dataResult->messages[0]->id;
                $resp[$key]['status'] = 'sent';
                array_push($finalArray, array(
                    'channel_id' => 4,
                    'type' => 'broadcast',
                    'category' => 'non-hsm',
                    'client' => 'whatsapp',
                    'status' => 'sent',
                    'number' => $numbers[$key],
                    'data' => json_encode($content),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ));
            } else {
                $resp[$key]['to'] = $numbers[$key];
                $resp[$key]['msgId'] = null;
                $resp[$key]['status'] = 'failed';
                array_push($finalArray, array(
                    'channel_id' => 4,
                    'type' => 'broadcast',
                    'category' => 'non-hsm',
                    'client' => 'whatsapp',
                    'status' => 'invalid',
                    'number' => $numbers[$key],
                    'data' => json_encode($content),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ));
            }
        }
        BroadcastMessage::insert($finalArray);

        return $resp;



    }

    public function createGroup($subject)
    {
        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-groups')->first();
        $requestData = [
            'subject' => $subject
        ];

        $response = $this->client->request(
            $apiProp->method,
            $apiProp->endpoint,
            [
                'http_errors' => false,
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
                "json" => $requestData,
            ]
        );
        $responses = json_decode($response->getBody());

        return $responses;

    }

    public function listGroup()
    {
        $checkToken = $this->checkTokenOfficial();

        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-groups')->first();

        try{
        $response = $this->client->request(
            'get',
            $apiProp->endpoint,
            [
                // 'http_errors' => false,
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
            ]
        );

        }catch (\Exception $e) {
            $resp = json_decode($e->getResponse()->getBody());
            // return $resp->errors[0];
            if($resp->errors[0]->title == 'Access denied') {
                return $response = $this->updateTokenOfficial();
            }
        }

        $responses = json_decode($response->getBody());

        // if($responses->errors[0]->details == 'Invalid credentials.') {
        //     // return 'invalid assdda';
        //     return $checkToken = $this->updateTokenOfficial();
        // }

        $datas = $subject = [];
        foreach ($responses->groups as $key => $value) {

            $responseGroupInfo = $this->client->request(
                'get',
                $apiProp->endpoint.'/'.$value->id,
                [
                    'http_errors' => false,
                        'verify' => false,
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                        ],
                ]
            );
            $responsesGroupInfo = json_decode($responseGroupInfo->getBody());

            $getInviteLink = $this->client->request(
                'get',
                $apiProp->endpoint.'/'.$value->id.'/invite',
                [
                    'http_errors' => false,
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
                ]
            );
            $responseGetInviteLink = json_decode($getInviteLink->getBody());

            $value->id = $value->id;
            $value->subject = $responsesGroupInfo->groups[0]->subject;
            $value->admins = $responsesGroupInfo->groups[0]->admins;
            $value->participants = $responsesGroupInfo->groups[0]->participants;
            $value->invite = $responseGetInviteLink->groups[0]->link;
            $datas[] = $value;

        }
        return $datas;

    }

    public function setGroupAdmin($groupId, $participants)
    {
        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-groups')->first();
        $requestData = [
            'wa_ids' => $participants
        ];

        $response = $this->client->request(
            'patch',
            $apiProp->endpoint.'/'.$groupId.'/admins',
            [
                'http_errors' => false,
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
                "json" => $requestData,
            ]
        );
        $responses = json_decode($response->getBody());

        return $responses;
    }

    public function removeGroupAdmin($groupId, $participants)
    {
        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-groups')->first();
        $requestData = [
            'wa_ids' => $participants
        ];

        $response = $this->client->request(
            'delete',
            $apiProp->endpoint.'/'.$groupId.'/admins',
            [
                'http_errors' => false,
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
                "json" => $requestData,
            ]
        );
        $responses = json_decode($response->getBody());

        return $responses;
    }

    public function removeGroupParticipants($groupId, $participants)
    {
        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-groups')->first();
        $requestData = [
            'wa_ids' => $participants
        ];

        $response = $this->client->request(
            'delete',
            $apiProp->endpoint.'/'.$groupId.'/participants',
            [
                'http_errors' => false,
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
                "json" => $requestData,
            ]
        );
        $responses = json_decode($response->getBody());

        return $responses;
    }

    public function _convertToWhatsappMessageType($message)
    {
        $whatsappMessageType = new WhatsappMessageType;
        // dd($message);
        $waMessageType = $whatsappMessageType->{$message->type}($message);
        // convert to Official api need
        switch ($message->type) {
            case 'image':
                $converted = [
                    'image' => $waMessageType['imageUrl'],
                    'caption' => $waMessageType['caption'],
                    'type' => 'image'
                ];
                break;
            case 'file':
                $converted = [
                    'document' => $waMessageType['fileUrl'],
                    'caption' => $waMessageType['fileName'],
                    'type' => 'document'
                ];
                break;
            case 'template':
                $converted = [
                    'message' => $waMessageType['text'],
                    'type' => 'text'
                ];
                break;
            case 'html':
                $converted = [
                    'message' => $waMessageType['text'],
                    'type' => 'text'
                ];
                break;
            default:
                if (isset($waMessageType['multiple'])) {
                    $converted = [
                        'multiple' => $waMessageType['multiple'],
                        'type' => 'text'
                    ];
                } else {
                    $converted = [
                        'message' => $waMessageType['text'],
                        'type' => 'text'
                    ];
                }
                break;
        }


        return $converted;
    }

    public function _convertOfficialMsgType($data)
    {
        // dd($data);
        switch ($data['type']) {
            case 'text':
                $converted = [
                    'text' => [
                        'body' => $data['message']
                    ]
                ];
                break;
            case 'image':
                $converted = [
                    'image' => [
                        'link' => $data['image'],
                        'caption' => $data["caption"]
                    ]
                ];
                break;
            default:
                $converted = [
                    'text' => [
                        'body' => $data['type']
                    ]
                ];
                break;
        }
        return $converted;
    }


    public function checkTokenOfficial()
    {
        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-users-login')->first();

        $integrationData = json_decode($this->data['integration']);
        // $this->data['integration']->integration_data['whatsappBaseUrl'];

        if ($integrationData->integration_data->token == null) {
            try {
                $response = $this->client->request(
                    $apiProp->method,
                    $apiProp->endpoint,
                    [
                        'verify' => false,
                        'auth' => [
                            $this->data['integration']->integration_data['username'],
                            $this->data['integration']->integration_data['password']
                        ],
                        'headers' => [
                            'Content-Type' => 'application/json',
                            // 'Authorization' => 'Basic '.$this->officialApi->remark //bisa dapat token dari auth atau dari headers menggunakan encode base64 format username:password
                        ]
                        // 'json' => $requestData,
                    ]
                );

            }catch (\Exception $e) {
                $resp = json_decode($e->getResponse()->getBody());
                return $resp->errors[0];
            }
            $response = json_decode($response->getBody());
            $integrationData->integration_data->token = $response->users[0]->token;

            $appId = $this->data['integration']->app_id;
            $integration = Integration::where(['app_id' => $appId])->whereHas('channel', function ($query) {
                $query->where('name', '=', 'whatsapp');
            })->first();

            $integration->integration_data = $integrationData->integration_data;
            $integration->save();
        }
    }

    public function updateTokenOfficial()
    {
        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-users-login')->first();

        $integrationData = json_decode($this->data['integration']);
        // $this->data['integration']->integration_data['whatsappBaseUrl'];
            try {
                $response = $this->client->request(
                    $apiProp->method,
                    $apiProp->endpoint,
                    [
                        'verify' => false,
                        'auth' => [
                            $this->data['integration']->integration_data['username'],
                            $this->data['integration']->integration_data['password']
                        ],
                        'headers' => [
                            'Content-Type' => 'application/json',
                            // 'Authorization' => 'Basic '.$this->officialApi->remark //bisa dapat token dari auth atau dari headers menggunakan encode base64 format username:password
                        ]
                        // 'json' => $requestData,
                    ]
                );

            }catch (\Exception $e) {
                $resp = json_decode($e->getResponse()->getBody());
                return $resp->errors[0];
            }
            $response = json_decode($response->getBody());
            $integrationData->integration_data->token = $response->users[0]->token;

            $appId = $this->data['integration']->app_id;
            $integration = Integration::where(['app_id' => $appId])->whereHas('channel', function ($query) {
                $query->where('name', '=', 'whatsapp');
            })->first();

            $integration->integration_data = $integrationData->integration_data;
            $integration->save();
            return $this->listGroup();
    }

    public function getImageLink($message)
    {
        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-media')->first();
        $integrationData = json_decode($this->data['integration']);
        // dd($mediaId = $message['image']['id']);
        $mediaId = $message['image']['id'] ?? null;
        if ($mediaId == null) {
            return env("APP_URL") . "/images/pictures/image_not_found.jpg";
        }
        // $path = public_path('/upload/whatsapp', 'test.jpg');
        // $file_path = fopen($path,'w');
        // $resource = fopen('/upload/whatsapp', 'w');
        // $stream = GuzzleHttp\Psr7\stream_for($resource);

        try {
            $response = $this->client->request(
                $apiProp->method,
                $apiProp->endpoint.'/'.$mediaId,
                [
                    'stream' => true,
                    'http_errors' => false,
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
                ]
            );
        } catch (\Exception $e) {
            $resp = json_decode($e->getResponse()->getBody());
            return $resp->errors[0];
        }
        $data = $response->getBody()->getContents();
        $getName = $response->getHeader('Content-Disposition')[0];
        $imgName = json_decode(str_replace("attachment; filename=", "", $getName));
        $name = $imgName."_wa.png";

        Storage::disk('public_chat')->put($name, $data);
        return env("APP_URL") . "/upload/chat/" . $name;

    }

    public function getVideoLink($message)
    {
        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-media')->first();
        $integrationData = json_decode($this->data['integration']);
        $mediaId = $message['video']['id'] ?? null;
        if ($mediaId == null) {
          return [
              'videoUrl' => env("APP_URL") . "/videos/default.mp4",
          ];
        }

        try {
            $response = $this->client->request(
                $apiProp->method,
                $apiProp->endpoint.'/'.$mediaId,
                [
                    'stream' => true,
                    'http_errors' => false,
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                    ],
                ]
            );
        } catch (\Exception $e) {
            $resp = json_decode($e->getResponse()->getBody());
            return $resp->errors[0];
        }
        $data = $response->getBody()->getContents();
        $getName = $response->getHeader('Content-Disposition')[0];
        $imgName = json_decode(str_replace("attachment; filename=", "", $getName));
        $name = $imgName."_wa.mp4";

        Storage::disk('public_chat')->put($name, $data);
        $fileUrl = env("APP_URL") . "/upload/chat/" . $name;

        return [
            'videoUrl' => $fileUrl,
        ];

    }

    public function getFileLink($message)
    {
        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-media')->first();
        $integrationData = json_decode($this->data['integration']);
        $mediaId = $message['document']['id'];
        $mediaName = $message['document']['filename'];

                try {
                    $response = $this->client->request(
                            $apiProp->method,
                            $apiProp->endpoint.'/'.$mediaId,
                            [
                                    'stream' => true,
                                    'http_errors' => false,
                                    'verify' => false,
                                    'headers' => [
                                            'Content-Type' => 'application/json',
                                            'Authorization' => 'Bearer '.$this->data['integration']->integration_data['token']
                                    ],
                            ]
                    );
            } catch (\Exception $e) {
                    $resp = json_decode($e->getResponse()->getBody());
                    return $resp->errors[0];
            }
            $data = $response->getBody()->getContents();


            Storage::disk('public_chat')->put($mediaName, $data);
            $fileUrl = env("APP_URL") . "/upload/chat/" . $mediaName;

            return [
                    'fileUrl' => $fileUrl,
            ];

    }

    public function forceUpdateToken()
    {
        $apiProp = $this->officialApi->endpoints()->where('name', 'v1-users-login')->first();
        $integrationData = json_decode($this->data['integration']);

        try {
            $response = $this->client->request(
                $apiProp->method,
                $apiProp->endpoint,
                [
                    'verify' => false,
                    'auth' => [
                        $this->data['integration']->integration_data['username'],
                        $this->data['integration']->integration_data['password']
                    ],
                    'headers' => [
                        'Content-Type' => 'application/json',
                        // 'Authorization' => 'Basic '.$this->officialApi->remark //bisa dapat token dari auth atau dari headers menggunakan encode base64 format username:password
                    ]
                    // 'json' => $requestData,
                ]
            );

        }catch (\Exception $e) {
            $resp = json_decode($e->getResponse()->getBody());
            return $resp->errors[0];
        }
        $response = json_decode($response->getBody());
        $integrationData->integration_data->token = $response->users[0]->token;
        $appId = $this->data['integration']->app_id;

        $integrationId = $this->data['integration']->id;
        $integration = Integration::where(['id' => $integrationId, 'status' => true, 'channel_id' => 4])->first();

        $integration->integration_data = $integrationData->integration_data;
        $this->data['integration'] = $integration; //update construct data integration
        $integration->save();
        return $response;
    }

}

