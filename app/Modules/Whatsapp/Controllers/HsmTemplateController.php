<?php

namespace App\Modules\Whatsapp\Controllers;

use Illuminate\Http\Request;

use GuzzleHttp\Client;
use App\Modules\Whatsapp\Models\HsmTemplate;
use App\Modules\App\Models\App;
use App\Http\Controllers\ApiController;
use App\Modules\Log\Helpers\WebhookHelper;
use Illuminate\Support\Facades\Validator;
use App\Mail\OmniEmail;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Notifications\RequestHsmTemplateNotification;
use App\Notifications\DeleteHsmTemplateNotification;
use Illuminate\Notifications\Notifiable;


class HsmTemplateController extends ApiController
{
    protected $integration;

    public function index(Request $request, $appId)
    {
        $sort = $request->get('sort');
        $filterText = $request->get('filterText');
        $per_page = $request->get('per_page');

        $data = DB::table('hsm_templates as h')
                    ->join('integrations as i', 'h.integration_id' , '=', 'i.id')
                    ->where('h.app_id', $appId)
                    ->where('h.deleted_at', null)
                    ->addSelect('h.*')
                    ->addSelect(DB::raw("
                        (
                            json_build_object(
                                'id', i.id,
                                'whatsappNumber', i.integration_data->'whatsappNumber',
                                'whatsappName', i.integration_data->'whatsappName'
                            )
                        ) as integration
                    "))
                    ->orderBy('h.id', 'ASC');

        if ($sort) {
            $sort = explode('|', $sort);
            $user = $data->orderBy($sort[0], $sort[1]);
        }

        if ($filterText){
            $data = $data->where(function ($q) use ($filterText){
                $q->where('template_name', 'ILIKE', '%' . $filterText . '%')
                ->orWhere('category', 'ILIKE', '%' . $filterText . '%')
                ->orWhere('preview', 'ILIKE', '%' . $filterText . '%');
            });
        }

        if (!$per_page) {
            $per_page = 15;
        }

        return response()->json($data->paginate($per_page));
    }


    public function show($appId)
    {
        $hsmTemplate = HsmTemplate::where('app_id', $appId)->first();
        return $this->successResponse(['hsmTemplate' => $hsmTemplate]);
    }


    public function store(Request $request, $appId)
    {
        $validator = Validator::make($request->all(), [
            'template_name' => 'required|max:45',
            'category' => 'required|max:35',
            'languange' => 'required|max:35',
            'body' => 'required',
            // 'status' => 'required'
        ]);

        // return $request;

        if ($validator->fails()) {
            if ($validator->errors()->first('template_name')) {
                return $this->errorResponse(null, $validator->errors()->first('template_name'));
            } else if ($validator->errors()->first('category')) {
                return $this->errorResponse(null, $validator->errors()->first('category'));
            } else if ($validator->errors()->first('languange')) {
                return $this->errorResponse(null, $validator->errors()->first('languange'));
            } else if ($validator->errors()->first('body')) {
                return $this->errorResponse(null, $validator->errors()->first('body'));
            // } else if ($validator->errors()->first('status')) {
            //     return $this->errorResponse(true, $validator->errors()->first('status'));
            }
        }

        $checkTemplateName = HsmTemplate::where('app_id', $appId)
                                        ->where('template_name', $request->template_name)
                                        ->first();

        $param = substr_count($request->body, '{{');
        $integration = WebhookHelper::getIntegrationDataById('whatsapp', $appId, $request->integration_id);

        if ($checkTemplateName) {
            return $this->errorResponse(null, 'Template name already exist');
        }
        if (!$integration) {
            return $this->errorResponse(null, 'Please connect your whatsapp first');
        }
        $header = $footer = $button = [];
        if ($request->headerType) {
            $header = [
                'headerType' => $request->headerType,
                'textHeader' => $request->textHeader ?? '',
                'mediaUrl' => ""
            ];
        }
        if ($request->textFooter) {
            $footer = [
                'textFooter' => $request->textFooter
            ];
        }
        if ($request->buttonType == "quick-reply") {
            $button = [
                "buttonType" => $request->buttonType,
                "quickReply" => $request->buttonText
            ];
        }
        if ($request->buttonType == "call-to-action") {
            $button = [
                "buttonType" => $request->buttonType,
                "callToAction" => $request->callToAction,
            ];
        }
        // return json_encode($header, true);
        // $hsmTemplate = HsmTemplate::create([
        //     'app_id' => $appId,
        //     'template_name' => $request->template_name,
        //     'languange' => $request->languange['code'],
        //     'category' => $request->category,
        //     'preview' => $request->body,
        //     'provider' => $integration->integration_data['apiService'],
        //     'params' => $param,
        //     'status' => 'request',
        //     'integration_id' => $integration->id,
        //     'header' => $header,
        //     'footer' => $footer,
        //     'button' => $button
        // ]);
        $hsmTemplate = new HsmTemplate;
        $hsmTemplate->app_id = $appId;
        $hsmTemplate->template_name = $request->template_name;
        $hsmTemplate->languange = $request->languange['code'];
        $hsmTemplate->category = $request->category;
        $hsmTemplate->preview = $request->body;
        $hsmTemplate->provider = $integration->integration_data["apiService"];
        $hsmTemplate->params = $param;
        $hsmTemplate->status = "request";
        $hsmTemplate->integration_id = $integration->id;
        $hsmTemplate->header = json_encode($header);
        $hsmTemplate->footer = json_encode($footer);
        $hsmTemplate->button = json_encode($button);
        $hsmTemplate->save();

        $app = App::where('id', $appId)->first();

        $data = [
            'app_name' => $app->name,
            'template_name' => $request->template_name,
            'category' => $request->category,
            'preview' => $request->body,
            'params' => $param,
            'languange' => $request->languange['code'],
            'header' => $header,
            'footer' => $footer,
            'button' => $button,
            'token' => base64_encode($request->template_name)
        ];

        // $to = ['fachriza19@gmail.com'];
        // $hsmTemplateMail->getEmailAttribute($to);

        $hsmTemplate->notify(new RequestHsmTemplateNotification($data));

        return $this->successResponse($hsmTemplate, 'Template Requested');

    }





    public function edit($id)
    {
        return $id;
    }


    public function update(Request $request, $appId, $hsmTemplateId)
    {
        $validatedData = $request->validate([
            'key' => 'required',
            'value' => 'required',
        ]);

        $data = [
            'key' => $request->key,
            'value' => $request->value,
            'value_type' => $request->value_type,
            'description' => $request->description,
            'is_active' => $request->is_active,
            'updated_by' => $request->decodedUserId,

        ];

        $hsmTemplate = HsmTemplate::where('id', $hsmTemplateId)->update($data);


        return $this->successResponse(['hsmTemplate' => $hsmTemplate], 'general value updated');

    }


    public function destroy($appId, $hsmTemplateId)
    {
        $app = App::where('id', $appId)->first();
        // return $hsmTemplateId;
        $hsmTemplate = HsmTemplate::findOrFail($hsmTemplateId);
        $hsmTemplate->delete();

        $data = [
            'app_name' => $app->name,
            'template_name' => $hsmTemplate->template_name,

        ];

        $hsmTemplate->notify(new DeleteHsmTemplateNotification($data));

        return $this->successResponse(null, 'Template deleted');

    }

    public function getTemplateName(Request $request, $appId){
        if ($request->integrationId) {
            $hsmTemplate = DB::table("hsm_templates")
                             ->where('app_id', $appId)
                             ->where('status', 'active')
                             ->where('integration_id', $request->integrationId)
                             ->where('deleted_at', null)
                             ->get();
            return $this->successResponse($hsmTemplate);
        } else {
            $hsmTemplate = DB::table("hsm_templates")
                             ->where('app_id', $appId)
                             ->where('status', 'active')
                             ->where('deleted_at', null)
                             ->get();
            return $this->successResponse($hsmTemplate);
        }
    }

    public function getTemplate(Request $request, $appId)
    {
        $sort = $request->get('sort');
        $filterText = $request->get('filterText');
        $per_page = $request->get('per_page');

        $data = HsmTemplate::where('app_id', $appId)
                                //   ->where('status', 'active')
                                  ->orderBy('id', 'ASC');

        if ($sort) {
            $sort = explode('|', $sort);
            $user = $data->orderBy($sort[0], $sort[1]);
        }

        if ($filterText){
            $data = $data->where(function ($q) use ($filterText){
                $q->where('template_name', 'ILIKE', '%' . $filterText . '%')
                ->orWhere('category', 'ILIKE', '%' . $filterText . '%')
                ->orWhere('preview', 'ILIKE', '%' . $filterText . '%');
            });
        }

        if (!$per_page) {
            $per_page = 15;
        }

        return response()->json($data->paginate($per_page));

    }

    public function requestTemplates(Request $request, $appId)
    {

    }

    public function sendMailTemplate()
    {
        // Mail::to("fachrizaa18@gmail.com")->send(new OmniEmail());
        // return "Email telah dikirim";

        // $hsmTemplate = HsmTemplate::first();
        // $data = [
        //     'template_name' => $hsmTemplate->template_name,
        //     'category' => $hsmTemplate->category,
        //     'preview' => $hsmTemplate->preview,
        //     'params' => $hsmTemplate->params,
        //     'languange' => $hsmTemplate->languange
        // ];

        // $to = ['fachriza19@gmail.com'];
        // $hsmTemplate->getEmailAttribute($to);
        // $hsmTemplate->notify(new RequestHsmTemplateNotification($data));
        // return 'Mail sent';
    }

    public function approveTemplate($token)
    {
        $templateName = base64_decode($token);
        $hsmTemplate = HsmTemplate::where('template_name', $templateName)->first();
        if ($hsmTemplate == null){
            return $this->successResponse(null, 'wrong token');
        } else {
            $hsmTemplate->status = 'active';
            $hsmTemplate->save();
            return $this->successResponse($hsmTemplate, 'Template Approved');
        }
        // return Redirect()->to(env('AUTH_URL'). '/approve');

        return 'Template Approved';
    }
}
