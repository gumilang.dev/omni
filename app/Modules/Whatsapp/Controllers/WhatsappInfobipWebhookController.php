<?php

namespace App\Modules\Whatsapp\Controllers;

use Hashids\Hashids;
use Illuminate\Http\Request;
use App\Modules\Log\Models\WebhookLog;
use App\Modules\Integration\Models\Integration;
use App\Modules\App\Models\App;
use App\Events\NewUser;
use App\Events\NewMessage;
use App\Modules\User\Models\User;
use App\Modules\Room\Models\Room;
use App\Modules\Message\Models\Message;
use App\Modules\Whatsapp\Services\InfobipApiController;
use App\Modules\Mobile\Helpers\LennaMessageType;

use Illuminate\Support\Str;
use App\Modules\Chat\Helpers\ChatHelper;
use App\Modules\Channel\Models\Channel;
use App\Modules\Bot\Services\BotService;
use App\Modules\Log\Helpers\WebhookHelper;
use App\Modules\Mobile\Services\BackendService;
use App\Http\Controllers\ApiController;

class WhatsappInfobipWebhookController extends ApiController
{
    protected $app;
    protected $integration;
    protected $whatsappController;
    protected $channel;

    public function __construct()
    {
        $this->channel = Channel::where('name', 'whatsapp')->first();

    }
    public function webhookTest(Request $request)
    {
        $webhookLog = WebhookLog::create([
            "source" => "WHATSAPP",
            "app_id" => "444",
            "events" => "whatsapp webhook test",
            "payload" => json_encode($request->all(), JSON_UNESCAPED_SLASHES),
            "response" => json_encode([
                'success' => true,
                'data' => "testing webhook"
            ])
        ]);

        return $this->successResponse(['events' => 'test webhook']);
    }
    public function webhook(Request $request, $appId)
    {

        if (!$this->integration = WebhookHelper::getIntegrationData('whatsapp', $appId))
            return $this->errorResponse(null, 'integration not active');

        if ($this->integration->integration_data['apiService'] != 'infobip') {
            return $this->errorResponse(null, 'activated with another Whatsapp API service, please activate using INFOBIP service');
        }
        $this->app = App::find($appId);
    // $this->whatsappController = new InfobipApiController(['apiKey' => $this->integration->integration_data['apiKey']]);

        $requestResult = $request->results[0];
        $response = $this->incomingMessage($requestResult, $this->app->id);
        $responseLog = $response;
        broadcast(new NewMessage($response['message'], $response['room'], $this->app));
        if (!WebhookHelper::isLive(['roomId' => $response['room']->id])) {

            $backendService = new BackendService(['appId' => $appId]);
            $backendResponse = $backendService->getUserToken([
                'email' => $response['user']->email,
                "password" => BackendService::encryptText(env("ENCRYPT_KEY"), "$appId:" . $response['user']->email),
                "fcm_token" => null,
                "client" => "whatsapp",
                "userId" => $response['user']->id
            ]);

            // $backendService->updateLocationAndIp([
            //     "user_id" => $response["user"]->id,
            //     "ip_address" => \Request::ip()
            // ]);

            $botService = new BotService(['appId' => $appId]);
            $botResponse = $botService->replyMessage([
                'userId' => $response['user']->id,
                'content' => $response['message']->content[0],
                'room' => $response['room'],
                'lat' => $request->lat ?? 0,
                'lon' => $request->lon ?? 0,
                'channel' => 'whatsapp',
                "headers" => ["Authorization" => "$backendResponse->token_type $backendResponse->access_token"],
                'lang' => $response['user']->lang
            ]);
        }
        $responseLog = (object)[
            'output' => $botResponse['nlp']->response ?? null,
            'quickbutton' => $botResponse['nlp']->quickbutton ?? [],
        ];


        $webhookLog = WebhookLog::create([
            "source" => "whatsapp",
            "app_id" => $this->app->id,
            "events" => 'message',
            "payload" => json_encode($request->all()),
            "response" => json_encode([
                'success' => true,
                'data' => $responseLog
            ])
        ]);

        return $this->successResponse([
            'events' => 'message',
            'result' => (object)[
                'output' => $botResponse['nlp']->response ?? null,
                'quickbutton' => $botResponse['nlp']->quickbutton ?? [],
            ]

        ]);
    }

    public function registerUser($message, $appId)
    {

        $pictureUrl = env("APP_URL") . "/images/pictures/no_avatar.jpg";
        $phone = $message['from'];
        $email = $phone . "@whatsapp.com";

        $fields = [
            'app_id' => $appId,
            'channel_user_id' => $phone,
            'name' => $phone,
            'nickname' => $phone,
            'password' => bcrypt("$appId:$email"),
            'picture' => $pictureUrl,
            'email' => $email,
            'phone' => $phone,
        ];
        $user = $this->channel->users()->firstOrCreate(
            ['channel_user_id' => $phone, 'app_id' => $appId],
            $fields
        );
        return $user;
    }

    public function incomingMessage($whatsappData, $appId)
    {

        $user = User::where([['channel_user_id', "=", $whatsappData['from']], ['app_id', "=", $appId]])->first();
    // register user if not registered
        if (!$user) {
            $user = $this->registerUser($whatsappData, $appId);
        }

        $chatRoom = ChatHelper::createRoomAndParticipate(['channel' => $this->channel, 'user' => $user, 'appId' => $appId]);

        $channelData = [];
        $content[] = $this->setMessageType($whatsappData['message']);
        $message = ChatHelper::createMessage(['channelData' => $channelData, 'content' => $content, 'user' => $user, 'room' => $chatRoom]);

        return [
            'message' => $message,
            'user' => $user,
            'room' => $chatRoom
        ];


    }

    public function setMessageType($message)
    {
        $lennaMessageType = new LennaMessageType;


        switch ($message['type']) {
            case 'TEXT':
                $content = $lennaMessageType->text(['text' => $message['text']]);
                break;
            case 'IMAGE':
                $content = $lennaMessageType->image(['previewImageUrl' => $message]);

                break;
            case 'location':
                $latLon = $this->parseLatLon($message);

                $content = $lennaMessageType->location(
                    [
                        " title " => " Location ",
                        'latitude' => $latLon[0],
                        'longitude' => $latLon[1]
                    ]
                );
                break;
            case 'audio':

                $content = $lennaMessageType->audio(['originalContentUrl' => $message]);

                break;
            case 'notsupported':

                $content = $lennaMessageType->text(['text' => $message]);

                break;
        }
        return $content;
    }

    public function parseLatLon($messageText)
    {
        $messageText = urldecode($messageText);
        if (Str::contains($messageText, ['/search'])) {

            $latLon = substr($messageText, strpos($messageText, " @") + 1);
            $latLon = substr($latLon, 0, strpos($latLon, ", 17 z "));
            $latLon = explode(", ", $latLon);
            return $latLon;

        }
        $latLon = substr($messageText, strpos($messageText, " q = ") + 2);
        $latLon = substr($latLon, 0, strpos($latLon, " & z "));
        $latLon = explode(", ", $latLon);
        return $latLon;

    }

    public function checkMessageType($messageText)
    {
        if (Str::contains($messageText, ['.png', '.jpg', '.gif', '.jpeg'])) {
            return 'image';
        } elseif (Str::contains($messageText, ['.ogg', '.mp3', '.aac', '.m4a'])) {
            return 'audio';
        } elseif (Str::contains($messageText, ['maps.google.com'])) {
            return 'location';
        } elseif (Str::contains($messageText, ['not supported'])) {
            return 'notsupported';
        } else {
            return 'text';
        }

    }

}
