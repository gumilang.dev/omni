<?php

namespace App\Modules\Whatsapp\Controllers;

use Hashids\Hashids;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Modules\Log\Models\WebhookLog;
use App\Modules\Integration\Models\Integration;
use App\Modules\App\Models\App;
use App\Modules\User\Models\User;
use App\Modules\Room\Models\Room;
use App\Modules\Message\Models\Message;
use App\Modules\Channel\Models\Channel;
use App\Modules\Livechat\Models\Livechat;

use App\Events\NewUser;
use App\Events\NewMessage;
use App\Events\RequestLive;

use App\Modules\Whatsapp\Services\InfobipApiController;
use App\Modules\Whatsapp\Services\DamcorpApiController;
use App\Modules\Bot\Services\BotService;
use App\Modules\Mobile\Services\BackendService;

use App\Modules\Mobile\Helpers\LennaMessageType;
use App\Modules\Chat\Helpers\ChatHelper;
use App\Modules\Log\Helpers\WebhookHelper;
use App\Modules\BlockUser\Services\BlockUserService;
use App\Http\Controllers\ApiController;

class WhatsappDamcorpWebhookController extends ApiController
{
    protected $app;
    protected $integration;
    protected $whatsappController;
    protected $channel;
    protected $blockUserService;
    public function __construct(
      BlockUserService $blockUserService
    )
    {
      $this->channel = Channel::where('name', 'whatsapp')->first();
      $this->blockUserService = $blockUserService;
    }
    public function webhookTest(Request $request)
    {
        $webhookLog = WebhookLog::create([
            "source" => "whatsapp",
            "app_id" => "444",
            "events" => "whatsapp webhook test",
            "payload" => json_encode($request->all(), JSON_UNESCAPED_SLASHES),
            "response" => json_encode([
                'success' => true,
                'data' => "testing webhook"
            ])
        ]);

        return $this->successResponse(['events' => 'test webhook']);
    }
    public function webhook(Request $request, $appId, $id = null)
    {
        $id = (int)$id;

        if (!$id) {
            return $this->errorResponse('wrong webhook url', 'wrong webhook url');
        }

        if (!$this->integration = WebhookHelper::getIntegrationDataById('whatsapp', $appId, $id)) {
            return $this->errorResponse(null, 'wrong app / integration not active');
        }

        if ($this->integration->integration_data['apiService'] != 'damcorp') {
            return $this->errorResponse(null, 'activated with another Whatsapp API service, please activate using DAMCORP service');
        }

        $this->app = App::find($appId);

        if (!empty($request->statuses[0])) {
                $webhookLog = WebhookLog::create([
                    "source" => "whatsapp",
                    "app_id" => $this->app->id,
                    "events" => 'message_status',
                    "payload" => json_encode($request->all()),
                    "response" => json_encode([
                        'success' => true,
                        'data' => []
                    ])
                ]);

            return $this->successResponse([
            ]);
        }

        if (!empty($request->messages[0]['group_id'])) {
            $webhookLog = WebhookLog::create([
                "source" => "whatsapp",
                "app_id" => $this->app->id,
                "events" => 'group_status',
                "payload" => json_encode($request->all()),
                "response" => json_encode([
                    'success' => true,
                    'data' => []
                ])
            ]);

            return $this->successResponse([
            ]);
        }

        $response = $this->_incomingMessage($request, $appId);
        if(is_bool($response) && $response == true) {
          return $this->successResponse(["data" => $response],"user blocked");
        }
        // if status resolved
        if (WebhookHelper::isResolved(['roomId' => $response['room']->id])) {
          // change status to bot by deleting livechat key
          $this->deleteLiveChat($response['room']->id);
        }
        broadcast(new NewMessage($response['message'], $response['room'], $this->app, $response['room']->integration));
        $botService = new BotService(['appId' => $appId]);
        if (!WebhookHelper::isLive(['roomId' => $response['room']->id])) {
          if (!WebhookHelper::isRequest(['roomId' => $response['room']->id])) {
            $backendService = new BackendService(['appId' => $appId]);
            $backendResponse = $backendService->getUserToken([
                'email' => $response['user']->email,
                "password" => BackendService::encryptText(env("ENCRYPT_KEY"), "$appId:" . $response['user']->email),
                "fcm_token" => null,
                "client" => "whatsapp",
                "userId" => $response['user']->id
            ]);
            // $backendService->updateLocationAndIp([
            //   'ip' => $request->ipinfo->ip,
            //   'city' => $request->ipinfo->city,
            //   'location' => $request->ipinfo->loc,
            //   'user_id' => $response['user']->id,
            //   'last_update' => $response['user']->updated_at
            // ]);

            $botResponse = $botService->replyMessage([
                'userId' => $response['user']->id,
                'content' => $response['message']->content[0],
                'room' => $response['room'],
                'lat' => $request->lat ?? 0,
                'lon' => $request->lon ?? 0,
                'channel' => 'whatsapp',
                "headers" => ["Authorization" => "$backendResponse->token_type $backendResponse->access_token"],
                'lang' => $response['user']->lang
            ]);
          }
        }

        // if bot not integrated, change status to request
        $botIntegration = $botService->getIntegration();
        if (!$botIntegration) {
          if (!WebhookHelper::isLive(['roomId' => $response['room']->id])) {
            if (!WebhookHelper::isRequest(['roomId' => $response['room']->id])) {
              WebhookHelper::requestLivechat($response['user']->id, $appId);
            }
          }
        }

        $responseLog = (object)[
            'output' => $botResponse['nlp']->response ?? null,
            'quickbutton' => $botResponse['nlp']->quickbutton ?? [],
            'type' => $botResponse['nlp']->type ?? null
        ];


        $webhookLog = WebhookLog::create([
            "source" => "whatsapp",
            "app_id" => $this->app->id,
            "events" => 'message',
            "payload" => json_encode($request->all()),
            "response" => json_encode([
                'success' => true,
                'data' => $responseLog
            ]),
            "user_id" => $response['user']->id,
            "bot_id" => $botIntegration ? $botIntegration->integration_data['botId'] : NULL
        ]);

        return $this->successResponse([
            'events' => 'message',
            'result' => (object)[
                'output' => $botResponse['nlp']->response ?? null,
                'quickbutton' => $botResponse['nlp']->quickbutton ?? [],
            ]

        ]);
    }

    public function _registerUser($contacts, $appId)
    {

        $pictureUrl = env("APP_URL") . "/images/pictures/no_avatar.jpg";
        // $phone = BackendService::encryptText(env("ENCRYPT_PROFILE_KEY"), $contacts['wa_id']);
        // $email = BackendService::encryptText(env("ENCRYPT_PROFILE_KEY"), $contacts['wa_id'] . "@whatsapp.com");
        $phone = $contacts['wa_id'];
        $email = $phone . $this->integration->id . "@whatsapp.com";

        $fields = [
            'app_id' => $appId,
            'channel_user_id' => $phone,
            'name' => $contacts['profile']['name'],
            'nickname' => $contacts['profile']['name'],
            'password' => bcrypt("$appId:$email"),
            'picture' => $pictureUrl,
            'email' => $email,
            'phone' => $phone,
            'integration_id' => $this->integration->id
        ];
        $user = $this->channel->users()->firstOrCreate(
            ['channel_user_id' => $phone, 'app_id' => $appId, 'integration_id' => $this->integration->id],
            $fields
        );
        return $user;
    }

    public function _incomingMessage($request, $appId)
    {
        $user = User::where([['channel_user_id', "=", $request->contacts[0]['wa_id']], ['app_id', "=", $this->app->id]])->first();
        // register user if not registered
        if (!$user) {
            $user = $this->_registerUser($request->contacts[0], $this->app->id);
        }

        if ($user->integration_id == null) { //update integration_id user
            $user->integration_id = $this->integration->id;
            $user->save();
        } else if ($user->integration_id !== $this->integration->id) { // if user same but different webhook / appId
            $user = $this->_registerUser($request->contacts[0], $this->app->id); //register new user with different integration_id
        }

        if($this->blockUserService->isBlocked($user->email, $appId)) {
          return true;
        }
        $integrationEvent = [
            'id' => $this->integration->id,
            'whatsappName' => $this->integration->integration_data['whatsappName'],
            'whatsappNumber' => $this->integration->integration_data['whatsappNumber']
        ];
        $chatRoom = ChatHelper::createRoomAndParticipate([
            'channel' => $this->channel,
            'user' => $user,
            'appId' => $this->app->id
        ]);
        $chatRoom['integration'] = $integrationEvent;
        $channelData = [['id' => $request['messages'][0]['id']]];
        $content[] = $this->_setMessageType($request->messages[0], $appId, $this->integration->id);
        $message = ChatHelper::createMessage(['channelData' => $channelData, 'content' => $content, 'user' => $user, 'room' => $chatRoom]);

        return [
            'message' => $message,
            'user' => $user,
            'room' => $chatRoom
        ];


    }

    public function _setMessageType($message, $appId, $integrationId)
    {
        $lennaMessageType = new LennaMessageType;
        // $integration = Integration::where([['app_id', "=", $appId], ['status', "=", true], ['channel_id', "=", 4]])->first(); //id 4 = whatsapp
        $integration = Integration::where(['app_id' => $appId, 'status' => true, 'channel_id' => 4, 'id' => $integrationId])->first(); //id 4 = whatsapp

        $data = [
            'integration' => $integration
        ];


        switch ($message['type']) {
            case 'text':
                $content = $lennaMessageType->text(['text' => $message['text']['body']]);
                break;
            case 'image':
                $damcorpApiController = new DamcorpApiController($data);
                $getMedia = $damcorpApiController->getImageLink($message);
                $content = $lennaMessageType->image([
                    'previewImageUrl' => $getMedia,
                    'caption' => $message["image"]["caption"] ?? ""
                ]);
                break;
            case 'video':
                $damcorpApiController = new DamcorpApiController($data);
                $getMedia = $damcorpApiController->getVideoLink($message);
                $content = $lennaMessageType->video([
                        'originalContentUrl' => $getMedia['videoUrl'],
                        'previewImageUrl' => $getMedia['videoUrl']
                    ]);
                break;
            case 'location':
                // $latLon = $this->parseLatLon($message['location']);

                $content = $lennaMessageType->location(
                    [
                        // "title" => $message['location']['name'],
                        'latitude' => $message['location']['latitude'],
                        'longitude' => $message['location']['longitude'],
                        // 'address' => $message['location']['address']
                    ]
                );
                break;
            case 'voice':
                $content = $lennaMessageType->audio(['originalContentUrl' => $message['voice']['link']]);
                break;
            case 'audio':
                $content = $lennaMessageType->audio(['originalContentUrl' => $message]);
                break;
            case 'unknown':
                $content = $lennaMessageType->text(['text' => $message['errors'][0]['details']]);
                break;
            case 'contacts':
                $content = $lennaMessageType->text(['text' => $message['contacts'][0]['phones'][0]['phone']]);
                break;
            case 'document':
                $damcorpApiController = new DamcorpApiController($data);
                $getFile = $damcorpApiController->getFileLink($message);

                $content = $lennaMessageType->file(
                    [
                        'fileName' => $message['document']['filename'],
                        'fileUrl' => $getFile['fileUrl'],
                    ]
                    );
                break;
            case 'system':
                $content = $lennaMessageType->text(['text' => $message['system']['body']]);
                break;

            case 'sticker':
                $content = $lennaMessageType->text(['text' => 'Sticker']);
                break;
            case 'notsupported':
                $content = $lennaMessageType->text(['text' => $message]);
                break;
            default :
                $content = $lennaMessageType->text(['text' => 'This message type not available']);
                break;
        }
        return $content;
    }

    public function parseLatLon($messageText)
    {
        $messageText = urldecode($messageText);
        if (Str::contains($messageText, ['/search'])) {

            $latLon = substr($messageText, strpos($messageText, " @") + 1);
            $latLon = substr($latLon, 0, strpos($latLon, ", 17 z "));
            $latLon = explode(", ", $latLon);
            return $latLon;

        }
        $latLon = substr($messageText, strpos($messageText, " q = ") + 2);
        $latLon = substr($latLon, 0, strpos($latLon, " & z "));
        $latLon = explode(", ", $latLon);
        return $latLon;

    }

    public function requestLivechat($senderId, $appId)
    {
        $room = User::find($senderId)->rooms->first();
        $liveChat = $room->liveChats()->firstOrCreate(
            ['room_id' => $room->id, 'status' => 'request'],
            [
                "request_at" => Carbon::now()->toDateTimeString(),
                "status" => 'request',
            ]
        );
        broadcast(new RequestLive($room->id, $appId, $senderId, $liveChat->id));
    }

    public function deleteLiveChat($roomId)
    {
        Livechat::where('room_id', $roomId)->delete();
    }


}
