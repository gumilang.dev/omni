<?php

namespace App\Modules\Whatsapp\Controllers;

// MODEL
use App\Modules\App\Models\App;
use App\Modules\Log\Models\WebhookLog;
use App\Modules\Room\Models\Room;
use App\Modules\User\Models\User;
use App\Modules\Integration\Models\Integration;
use App\Modules\Message\Models\Message;
use App\Modules\Livechat\Models\Livechat;
use App\Modules\Channel\Models\Channel;

// OTHER
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use Exception;
use Log;
use InvalidArgumentException;

// VENDOR
use Hashids\Hashids;
use Carbon\Carbon;

// SERVICE
use App\Modules\Whatsapp\Services\WappinApiController;
use App\Services\BlockUserService;
use App\Modules\Bot\Services\BotService;
use App\Modules\Mobile\Services\BackendService;

// HELPERS
use App\Modules\Mobile\Helpers\LennaMessageType;
use App\Modules\Chat\Helpers\ChatHelper;
use App\Modules\Log\Helpers\WebhookHelper;

// EVENT
use App\Events\NewUser;
use App\Events\NewMessage;
use App\Events\RequestLive;

use App\Http\Controllers\ApiController;

class WhatsappWappinWebhookController extends ApiController
{
    protected $app;
    protected $integration;
    protected $whatsappController;
    protected $channel;
    protected $blockUserService;
    public function __construct(BlockUserService $blockUserService)
    {
        $this->channel = Channel::where('name', 'whatsapp')->first();
        $this->blockUserService = $blockUserService;
    }

    public function webhookTest(Request $request)
    {
        $webhookLog = WebhookLog::create([
            'source' => 'WHATSAPP',
            'app_id' => '444',
            'events' => 'whatsapp webhook test',
            'payload' => json_encode($request->all(), JSON_UNESCAPED_SLASHES),
            'response' => json_encode([
                'success' => true,
                'data' => 'testing webhook'
            ])
        ]);

        return $this->successResponse(['events' => 'test webhook']);
    }

    public function webhook(Request $request, $appId, $id = null)
    {
      $id = (int)$id;
      if (!$id) {
        return $this->errorResponse('wrong webhook url', 'wrong webhook url');
      }

      if (!$this->integration = WebhookHelper::getIntegrationDataById('whatsapp', $appId, $id)) {
        return $this->errorResponse(null, 'wrong app / integration not active');
      }

      if ($this->integration->integration_data['apiService'] !== 'wappin') {
        return $this->errorResponse(null, 'activated with another Whatsapp API service, please activate using wa Official service');
      }

      $this->app = App::find($appId);

      if (!empty($request->statuses[0])) {
        $webhookLog = [
          'request' => json_encode($request->all()),
          'event' => 'message_status',
          'appId' => $this->app->id
        ];
        $this->insertWebhookLog($webhookLog);
        return $this->successResponse([]);
      }

      if (!empty($request->messages[0]['group_id'])) {
        $webhookLog = [
          'request' => json_encode($request->all()),
          'event' => 'group_status',
          'appId' => $this->app->id
        ];
        $this->insertWebhookLog($webhookLog);
        return $this->successResponse([]);
      }

      $response = $this->incomingMessage($request, $appId);

      // check user is blocked
      if(is_bool($response) && $response = true){
        return $this->successResponse(['data' => $response], 'user blocked');
      }
      // if status resolved
      if (WebhookHelper::isResolved(['roomId' => $response['room']->id])) {
        // change status to bot by deleting livechat key
        $this->deleteLiveChat($response['room']->id);
      }

      broadcast(new NewMessage($response['message'], $response['room'], $this->app, $response['room']->integration));

      $botService = new BotService(['appId' => $appId]);

      if (!WebhookHelper::isLive(['roomId' => $response['room']->id])) {
        if (!WebhookHelper::isRequest(['roomId' => $response['room']->id])) {
          $backendService = new BackendService(['appId' => $appId]);
          $backendResponse = $backendService->getUserToken([
            'email' => $response['user']->email,
            'password' => BackendService::encryptText(env('ENCRYPT_KEY'), $appId . ':' . $response['user']->email),
            'fcm_token' => null,
            'client' => 'whatsapp',
            'userId' => $response['user']->id
          ]);

          // $botService = new BotService(['appId' => $appId]);
          $botResponse = $botService->replyMessage([
            'userId' => $response['user']->id,
            'content' => $response['message']->content[0],
            'room' => $response['room'],
            'lat' => $request->lat ?? 0,
            'lon' => $request->lon ?? 0,
            'channel' => 'whatsapp',
            'headers' => ['Authorization' => "$backendResponse->token_type $backendResponse->access_token"],
            'lang' => 'id'
          ]);
          // $response['user']->lang
        }
      }

      // if bot not integrated, change status to request
      $botIntegration = $botService->getIntegration();
      if (!$botIntegration) {
        if (!WebhookHelper::isLive(['roomId' => $response['room']->id])) {
          if (!WebhookHelper::isRequest(['roomId' => $response['room']->id])) {
            WebhookHelper::requestLivechat($response['user']->id, $appId);
          }
        }
      }

      $responseLog = (object)[
        'output' => $botResponse['nlp']->response ?? null,
        'quickbutton' => $botResponse['nlp']->quickbutton ?? [],
        'type' => $botResponse['nlp']->type ?? null
      ];

      $webhookLog = [
        'request' => json_encode($request->all()),
        'event' => 'message',
        'responseData' => $responseLog,
        'userId' => $response['user']->id,
        'botId' => $botIntegration ? $botIntegration->integration_data['botId'] : null,
        'appId' => $this->app->id
      ];

      $this->insertWebhookLog($webhookLog);

      return $this->successResponse([
        'events' => 'message',
        'result' => (object)[
            'output' => $botResponse['nlp']->response ?? null,
            'quickbutton' => $botResponse['nlp']->quickbutton ?? [],
        ],
        // 'bot_response' => $botResponse, //testing to see bot response & response from qontak
        // 'backend_response' => $backendResponse //testing to see response
      ]);
    }

    public function insertWebhookLog($data = [])
    {
      WebhookLog::create([
        'source' => 'whatsapp',
        'app_id' => $data['appId'],
        'events' => $data['event'],
        'payload' => $data['request'],
        'response' => json_encode([
            'success' => true,
            'data' => $data['responseData'] ?? []
        ]),
        'user_id' => $data['userId'] ?? null,
        'bot_id' => $data['botId'] ?? null
      ]);
    }

    public function registerUser($contact, $appId)
    {
      $pictureUrl = env("APP_URL") . "/images/pictures/no_avatar.jpg";
      $phone = $contact['wa_id'];
      $email = $phone . $this->integration->id . "@whatsapp.com";

      $fields = [
        'app_id' => $appId,
        'channel_user_id' => $phone,
        'name' => $contact['profile']['name'],
        'nickname' => $contact['profile']['name'],
        'password' => bcrypt("$appId:$email"),
        'picture' => $pictureUrl,
        'email' => $email,
        'phone' => $phone,
        'integration_id' => $this->integration->id
      ];
      DB::beginTransaction();
      try {
        $user = $this->channel->users()->firstOrCreate(
          ['channel_user_id' => $phone, 'app_id' => $appId, 'integration_id' => $this->integration->id],
          $fields
        );
      } catch (Exception $e) {
        DB::rollback();
        Log::info($e->getMessage());
        throw new InvalidArgumentException('Failed to register user');
      }
      DB::commit();
      return $user;
    }

    public function incomingMessage($request, $appId)
    {
      $user = User::where([['channel_user_id', "=", $request->contacts[0]['wa_id']], ['app_id', "=", $this->app->id]])->first();

      // register user if not registered
      if (!$user) {
        $user = $this->registerUser($request->contacts[0], $this->app->id);
      }

      if ($user->integration_id == null) { //update integration_id user
        $user->integration_id = $this->integration->id;
        $user->save();
      } else if ($user->integration_id !== $this->integration->id) { // if user same but different webhook / appId
        $user = $this->registerUser($request->contacts[0], $this->app->id); //register new user with different integration_id
      }

      // check if user blocked
      if($this->blockUserService->isBlocked($user->email, $appId)) {
        return true;
      }

      $integrationEvent = [
        'id' => $this->integration->id,
        'whatsappName' => $this->integration->integration_data['whatsappName'],
        'whatsappNumber' => $this->integration->integration_data['whatsappNumber']
      ];

      $chatRoom = ChatHelper::createRoomAndParticipate([
        'channel' => $this->channel,
        'user' => $user,
        'appId' => $this->app->id
      ]);

      $chatRoom['integration'] = $integrationEvent;
      $channelData = [['id' => $request['messages'][0]['id']]];

      $content[] = $this->setMessageType($request->messages[0], $appId);

      $message = ChatHelper::createMessage(['channelData' => $channelData, 'content' => $content, 'user' => $user, 'room' => $chatRoom]);

      return [
          'message' => $message,
          'user' => $user,
          'room' => $chatRoom
      ];
    }

    public function setMessageType($message, $appId)
    {
      $lennaMessageType = new LennaMessageType;
      $data = [
        'integration' => $this->integration
      ];
      switch ($message['type']) {
        case 'text':
            $content = $lennaMessageType->text(['text' => $message['text']['body']]);
          break;
        case 'image':
            $WappinApiController = new WappinApiController($data);
            $media = $WappinApiController->getImageLink($message);
            $content = $lennaMessageType->image([
              'previewImageUrl' => $media->data,
              'caption' => $message['image']['caption'] ?? ''
            ]);
          break;
        case 'video':
            $WappinApiController = new WappinApiController($data);
            $media = $WappinApiController->getVideoLink($message);
            $content = $lennaMessageType->video([
              'originalContentUrl' => $media->data,
              'previewVideoUrl' => $media->data,
              'caption' => $message['video']['caption'] ?? ''
              // 'duration'=> $getMedia['duration']
            ]);
          break;
        case 'location':
            $content = $lennaMessageType->location([
              'title' => $message['location']['name'] ?? 'Location',
              'address' => $message['location']['address'] ?? 'Unknown Address',
              'latitude' => $message['location']['latitude'],
              'longitude' => $message['location']['longitude'],
              'locationUrl' => $message['location']['url'] ?? 'https://maps.google.com'
            ]);
          break;
        case 'voice':
            $WappinApiController = new WappinApiController($data);
            $media = $WappinApiController->getVoiceLink($message);
            $content = $lennaMessageType->audio(['originalContentUrl' => $media->data]);
          break;
        case 'audio':
            $WappinApiController = new WappinApiController($data);
            $media = $WappinApiController->getAudioLink($message);
            $content = $lennaMessageType->audio(['originalContentUrl' => $media->data]);
          break;
        case 'document':
            $WappinApiController = new WappinApiController($data);
            $media = $WappinApiController->getFileLink($message);
            $content = $lennaMessageType->file([
              'fileName' => $message['document']['filename'],
              'fileUrl' => $media->data,
            ]);
          break;
        case 'contacts':
            $WappinApiController = new WappinApiController($data);
            $contact = $WappinApiController->getContact($message);
            $content = $lennaMessageType->summary([
              'title' => $contact->title,
              'columns' => $contact->phones
            ]);
          break;
        case 'unknown':
            $content = $lennaMessageType->text(['text' => 'Message type is not currently supported']);
          break;
        case 'system':
            // $content = $lennaMessageType->text(['text' => $message['system']['body']]);
            $content = $lennaMessageType->text(['text' => 'system message type']);
          break;
        case 'sticker':
            $content = $lennaMessageType->text(['text' => 'Sticker']);
          break;
        default :
            $content = $lennaMessageType->text(['text' => 'This message type not available']);
          break;
      }
      return $content;
    }

    // public function parseLatLon($messageText)
    // {
    //   // $messageText = urldecode($messageText);
    //   if (Str::contains($messageText, ['/search'])) {
    //     $latLon = substr($messageText, strpos($messageText, " @") + 1);
    //     $latLon = substr($latLon, 0, strpos($latLon, ", 17 z "));
    //     $latLon = explode(", ", $latLon);
    //     return $latLon;
    //   }
    //   $latLon = substr($messageText, strpos($messageText, " q = ") + 2);
    //   $latLon = substr($latLon, 0, strpos($latLon, " & z "));
    //   $latLon = explode(", ", $latLon);
    //   return $latLon;
    // }

    public function requestLivechat($senderId, $appId)
    {
        $room = User::find($senderId)->rooms->first();
        $liveChat = $room->liveChats()->firstOrCreate(
            ['room_id' => $room->id, 'status' => 'request'],
            [
                "request_at" => Carbon::now()->toDateTimeString(),
                "status" => 'request',
            ]
        );
        broadcast(new RequestLive($room->id, $appId, $senderId, $liveChat->id));
    }

    public function deleteLiveChat($roomId)
    {
        Livechat::where('room_id', $roomId)->delete();
    }
}
