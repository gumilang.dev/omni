<?php

namespace App\Modules\Whatsapp\Controllers;

use Hashids\Hashids;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Modules\App\Models\App;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Validator;

use App\Modules\Whatsapp\Services\DamcorpApiController;
use App\Modules\Whatsapp\Services\WaOfficialApiController;

use App\Modules\Integration\Models\Integration;
use App\Modules\Channel\Models\Channel;
use App\Modules\Log\Models\ApiLog;
use App\Modules\Log\Models\WebhookLog;

use App\Modules\Log\Helpers\WebhookHelper;

use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;



class WhatsappGroupController extends ApiController
{
    protected $app;
    protected $integration;
    protected $whatsappController;
    protected $channel;

    public function __construct()
    {
        $this->channel = Channel::where('name', 'whatsapp')->first();
    }

    public function createGroup(Request $request, $appId)
    {
        $integrationId = $request->integrationId;
        if (!$integration = WebhookHelper::getIntegrationDataById('whatsapp', $appId, $integrationId))
            return $this->errorResponse(null, "Plase connect your channel first");

        $data = [
            'integration' => $integration,
            'user_id' => $request->decodedUserId,
        ];


        switch ($integration->integration_data['apiService']){
            case 'damcorp':
                $damcorpApi = new DamcorpApiController($data);
                $response = $damcorpApi->createGroup($request->subject);
                break;

            case 'official':
                $WaOfficialApiController = new WaOfficialApiController($data);
                $response = $WaOfficialApiController->createGroup($request->subject);
                break;
        }

        return $this->successResponse($response, 'Group Created');

    }

    public function listGroup(Request $request, $appId)
    {
        $filterText = $request->get('filterText');
        $sort       = $request->get('sort');
        $per_page   = $request->get('per_page');
        $integrationId = $request->get('id');

        if (!$integration = WebhookHelper::getIntegrationDataById('whatsapp', $appId, $integrationId))
            return $this->errorResponse(null, "Plase connect your channel first");

        $data = [
            'integration' => $integration,
            'user_id' => $request->decodedUserId,
        ];

        switch ($integration->integration_data['apiService']){
            case 'damcorp':
                $damcorpApi = new DamcorpApiController($data);
                $response = $damcorpApi->listGroup();
                break;

            case 'official':
                $WaOfficialApiController = new WaOfficialApiController($data);
                $response = $WaOfficialApiController->listGroup();
                break;
        }

        if ($integration->integration_data['apiService'] == 'damcorp') {
            $dataGroupDamcorp = collect($response->data);
            $dataAdmin = $dataParticipants = [];
            // foreach ($dataGroupDamcorp as $key => $value) {
            //     $dataAdmin[$key] = $value->admins;
            //     // foreach ($value->admins as $key => $a) {
            //     //     $dataAdmin[$key] = $a;
            //     // }
            // }
            // foreach ($dataGroupDamcorp as $key => $value) {
            //     $dataParticipants[$key] = $value->participants;
            // }
            // // return $dataParticipants;
            // return $dataAdmin;
            if ($filterText) {
                $dataGroupDamcorp = $dataGroupDamcorp->filter(function ($item) use ($filterText) {
                    return false !== stristr($item->subject, $filterText) || false !== stristr($item->id, $filterText);
                })->values();
              }
            if (!$per_page) {
                $per_page = 15;
              }

            $data = $this->paginate($dataGroupDamcorp, $per_page);
            $data->appends($request->only(['filterText']));
            return response()->json($data);


        } else if($integration->integration_data['apiService'] == 'official') {
            $dataGroupOfficial = collect($response);
            if ($filterText) {
                $dataGroupOfficial = $dataGroupOfficial->filter(function ($item) use ($filterText) {
                    return false !== stristr($item->subject, $filterText) || false !== stristr($item->id, $filterText);
                })->values();
            }
            if (!$per_page) {
                $per_page = 15;
            }

            $data = $this->paginate($dataGroupOfficial, $per_page);
            $data->appends($request->only(['filterText']));
            return response()->json($data);
            // return response()->json([
            //     'data' => $response
            // ]);
        }

        // return $this->successResponse($response);
    }

    public function getInviteGroup(Request $request, $appId)
    {
        if (!$integration = $this->_getIntegrationData('whatsapp', $appId))
            return $this->errorResponse(null, "Plase connect your channel first");

        $data = [
            'integration' => $integration,
            'user_id' => $request->decodedUserId,
        ];

        switch ($integration->integration_data['apiService']){
            case 'damcorp':
                $damcorpApi = new DamcorpApiController($data);
                $response = $damcorpApi->getInviteGroup($request->group_id);
                break;

            case 'official':
                $WaOfficialApiController = new WaOfficialApiController($data);
                $response = $WaOfficialApiController->getInviteGroup();
                break;
        }

        return $this->successResponse($response);
    }

    public function setGroupAdmin(Request $request, $appId)
    {
        if (!$integration = $this->_getIntegrationData('whatsapp', $appId))
            return $this->errorResponse(null, "Please connect your channel first");

        $data = [
            'integration' => $integration,
            'user_id' => $request->decodedUserId,
        ];
        // return $this->successResponse(null, 'Added');
        switch ($integration->integration_data['apiService']){
            case 'damcorp':
                $damcorpApi = new DamcorpApiController($data);
                $response = $damcorpApi->setGroupAdmin($request->group_id, $request->participants);
                break;

            case 'official':
                $WaOfficialApiController = new WaOfficialApiController($data);
                $response = $WaOfficialApiController->setGroupAdmin($request->group_id, $request->participants);
                break;
        }

        return $this->successResponse($response, 'Added as admin');


    }

    public function removeGroupAdmin(Request $request, $appId)
    {
        if (!$integration = $this->_getIntegrationData('whatsapp', $appId)) {
            return $this->errorResponse(null, "Please connect your channel first");
        }

        $data = [
            'integration' => $integration,
            'user_id' => $request->decodedUserId
        ];
        switch ($integration->integration_data['apiService']) {
            case 'damcorp':
                $damcorpApi = new DamcorpApiController($data);
                $response = $damcorpApi->removeGroupAdmin($request->group_id, $request->participants);
                break;

            case 'official':
                $WaOfficialApiController = new WaOfficialApiController($data);
                $response = $WaOfficialApiController->removeGroupAdmin($request->group_id, $request->participants);
                break;
        }
        return $this->successResponse($response);
    }

    public function removeGroupParticipants(Request $request, $appId)
    {
        if (!$integration = $this->_getIntegrationData('whatsapp', $appId)) {
            return $this->errorResponse(null, "Please connect your channel first");
        }

        $data = [
            'integration' => $integration,
            'user_id' => $request->decodedUserId
        ];

        switch ($integration->integration_data['apiService']) {
            case 'damcorp':
                $damcorpApi = new DamcorpApiController($data);
                $response = $damcorpApi->removeGroupParticipants($request->group_id, $request->participants);
                break;

            case 'official':
                $WaOfficialApiController = new WaOfficialApiController($data);
                $response = $WaOfficialApiController->removeGroupParticipants($request->group_id, $request->participants);
                break;
        }
        return $this->successResponse($response, 'Removed');
    }

    public function updateGroupSubject(Request $request, $appId)
    {
        if (!$integration = $this->_getIntegrationData('whatsapp', $appId)) {
            return $this->errorResponse(null, "Please connect your channel first");
        }

        $data = [
            'integration' => $integration,
            'user_id' => $request->decodedUserId
        ];

        switch ($integration->integration_data['apiService']) {
            case 'damcorp':
                $damcorpApi = new DamcorpApiController($data);
                $response = $damcorpApi->updateGroupSubject($request->group_id, $request->subject);
                break;

            case 'official':
                $WaOfficialApiController = new WaOfficialApiController($data);
                $response = $WaOfficialApiController->updateGroupSubject();
                break;
        }
        return $this->successResponse($response);
    }

    public function setGroupIcon(Request $request, $appId)
    {
        if (!$integration = $this->_getIntegrationData('whatsapp', $appId)) {
            return $this->errorResponse(null, "Please connect your channel first");
        }

        $data = [
            'integration' => $integration,
            'user_id' => $request->decodedUserId
        ];

        $name = time() . $request->file('image')->getClientOriginalName();
        $extension = $request->file('image')->getClientOriginalExtension().';';
        $base64img = 'data:image/'.$extension.'base64,'. base64_encode(file_get_contents($request->file('image')));
        $base64img2 = base64_encode(file_get_contents($request->file('image')));

        switch ($integration->integration_data['apiService']) {
            case 'damcorp':
                $damcorpApi = new DamcorpApiController($data);
                $response = $damcorpApi->setGroupIcon($request->group_id, $base64img);
                break;

            case 'official':
                $WaOfficialApiController = new WaOfficialApiController($data);
                $response = $WaOfficialApiController->setGroupIcon();
                break;
        }
        return $this->successResponse($response, $base64img2);
    }

    public function _getIntegrationData($channelName, $appId)
    {
        $integration = Integration::where([['app_id', "=", $appId], ['status', "=", true]])
            ->whereHas('channel', function ($q) use ($channelName) {
                $q->where('name', $channelName);
            })->first();
        return $integration;
    }


    public function paginate($items, $perPage, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage)->values(), $items->count(), $perPage, $page, [
          'path' => LengthAwarePaginator::resolveCurrentPath()
        ]);
    }





}//end controller

