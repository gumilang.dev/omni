<?php

namespace App\Modules\Whatsapp\Controllers;

use Hashids\Hashids;
use Illuminate\Http\Request;
use App\Modules\Log\Models\WebhookLog;
use App\Modules\Integration\Models\Integration;
use App\Modules\App\Models\App;
use App\Events\NewUser;
use App\Events\NewMessage;
use App\Modules\User\Models\User;
use App\Modules\Room\Models\Room;
use App\Modules\Message\Models\Message;
use App\Modules\Whatsapp\Services\ApiWhaApiController;
use App\Modules\Mobile\Helpers\LennaMessageType;

use Illuminate\Support\Str;
use App\Modules\Chat\Helpers\ChatHelper;
use App\Modules\Channel\Models\Channel;
use App\Modules\Bot\Services\BotService;
use App\Modules\Log\Helpers\WebhookHelper;
use App\Http\Controllers\ApiController;

class WhatsappApiWhaWebhookController extends ApiController
{
    protected $app;
    protected $integration;
    protected $whatsappController;
    protected $channel;

    public function __construct()
    {
        $this->channel = Channel::where('name', 'whatsapp')->first();

    }
    public function webhookTest(Request $request)
    {
        $webhookLog = WebhookLog::create([
            "source" => "WHATSAPP",
            "app_id" => "444",
            "events" => "whatsapp webhook test",
            "payload" => json_encode($request->all(), JSON_UNESCAPED_SLASHES),
            "response" => json_encode([
                'success' => true,
                'data' => "testing webhook"
            ])
        ]);

        return $this->successResponse(['events' => 'testing webhook']);
    }
    public function webhook(Request $request, $appId)
    {
        $request->data = json_decode($request->data, true);

        if (!$this->integration = WebhookHelper::getIntegrationData('whatsapp', $appId))
            return $this->errorResponse(null, 'integration not active');

        $this->app = App::find($appId);
    // $this->whatsappController = new ApiWhaApiController(['apiKey' => $this->integration->integration_data['apiKey']]);

        switch (strtolower($request->data['event'])) {
            case 'inbox':
                $response = $this->incomingMessage($request->data, $this->app->id);
                $responseLog = $response;
                broadcast(new NewMessage($response['message'], $response['room'], $this->app));
                if (!WebhookHelper::isLive(['roomId' => $response['room']->id])) {
                    $botService = new BotService(['appId' => $appId]);
                    $botResponse = $botService->replyMessage([
                        'userId' => $response['user']->id,
                        'content' => $response['message']->content[0],
                        'room' => $response['room'],
                        'lang' => $response['user']->lang
                    ]);
                }
                $responseLog = (object)[
                    'output' => $botResponse['nlp']->response ?? null,
                    'quickbutton' => $botResponse['nlp']->quickbutton ?? [],
                ];
                break;
        }

        $webhookLog = WebhookLog::create([
            "source" => "whatsapp",
            "app_id" => $this->app->id,
            "events" => $request->data['event'],
            "payload" => json_encode($request->all()),
            "response" => json_encode([
                'success' => true,
                'data' => $responseLog
            ])
        ]);

        return $this->successResponse(['events' => $request->data['event']]);

    }

    public function registerUser($message, $appId)
    {

        $pictureUrl = env("APP_URL") . "/images/pictures/no_avatar.jpg";
        $phone = $message['from'];
        $email = $phone . "@whatsapp.com";

        $fields = [
            'app_id' => $appId,
            'channel_user_id' => $phone,
            'name' => $phone,
            'nickname' => $phone,
            'password' => bcrypt("$appId:$email"),
            'picture' => $pictureUrl,
            'email' => $email,
            'phone' => $phone,
        ];
        $user = $this->channel->users()->firstOrCreate(
            ['channel_user_id' => $phone, 'app_id' => $appId],
            $fields
        );
        return $user;
    }

    public function incomingMessage($whatsappData, $appId)
    {
        $user = User::where([['channel_user_id', "=", $whatsappData['from']], ['app_id', "=", $appId]])->first();
    // register user if not registered
        if (!$user) {
            $user = $this->registerUser($whatsappData, $appId);
        }

        $chatRoom = ChatHelper::createRoomAndParticipate(['channel' => $this->channel, 'user' => $user, 'appId' => $appId]);

        $channelData = [];
        $content[] = $this->setMessageType($whatsappData['text']);
        $message = ChatHelper::createMessage(['channelData' => $channelData, 'content' => $content, 'user' => $user, 'room' => $chatRoom]);

        return [
            'message' => $message,
            'user' => $user,
            'room' => $chatRoom
        ];


    }

    public function setMessageType($messageText)
    {
        $lennaMessageType = new LennaMessageType;
        $messageType = $this->checkMessageType($messageText);

        switch ($messageType) {
            case 'text':
                $content = $lennaMessageType->text(['text' => $messageText]);
                break;
            case 'image':
        // dd($messageText);
                $content = $lennaMessageType->image(['previewImageUrl' => $messageText]);

                break;
            case 'location':
                $latLon = $this->parseLatLon($messageText);

                $content = $lennaMessageType->location(
                    [
                        "title" => "Location",
                        'latitude' => $latLon[0],
                        'longitude' => $latLon[1]
                    ]
                );
                break;
            case 'audio':

                $content = $lennaMessageType->audio(['originalContentUrl' => $messageText]);

                break;
            case 'notsupported':

                $content = $lennaMessageType->text(['text' => $messageText]);

                break;
        }
        return $content;
    }

    public function parseLatLon($messageText)
    {
        $messageText = urldecode($messageText);
        if (Str::contains($messageText, ['/search'])) {

            $latLon = substr($messageText, strpos($messageText, "@") + 1);
            $latLon = substr($latLon, 0, strpos($latLon, ",17z"));
            $latLon = explode(",", $latLon);
            return $latLon;

        }
        $latLon = substr($messageText, strpos($messageText, "q=") + 2);
        $latLon = substr($latLon, 0, strpos($latLon, "&z"));
        $latLon = explode(",", $latLon);
        return $latLon;

    }

    public function checkMessageType($messageText)
    {
        if (Str::contains($messageText, ['.png', '.jpg', '.gif', '.jpeg'])) {
            return 'image';
        } elseif (Str::contains($messageText, ['.ogg', '.mp3', '.aac', '.m4a'])) {
            return 'audio';
        } elseif (Str::contains($messageText, ['maps.google.com'])) {
            return 'location';
        } elseif (Str::contains($messageText, ['not supported'])) {
            return 'notsupported';
        } else {
            return 'text';
        }

    }

}
