<?php namespace App\Modules\ChatTemplate\Models;

use Illuminate\Database\Eloquent\Model;

class ChatTemplate extends Model
{
    //
    protected $fillable = ['key','content','app_id',"files"];
}
