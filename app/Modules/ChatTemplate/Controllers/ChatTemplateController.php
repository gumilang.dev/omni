<?php namespace App\Modules\ChatTemplate\Controllers;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Services\ChatTemplateServiceImpl;
use Illuminate\Support\Facades\Storage;

class ChatTemplateController extends ApiController
{
    private $chatTemplateService;
    public function __construct(ChatTemplateServiceImpl $chatTemplateService) {
        $this->chatTemplateService = $chatTemplateService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$appId)
    {
      $chatTemplate = $this->chatTemplateService->paginate(
          $request,
          ['app_id' => $appId],
          ['key','content']
    );
      return response()->json($chatTemplate);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$appId)
    {
        $data = [
            'key' => $request->input('key'),
            'content' => $request->input('content'),
            'app_id' => $appId,
            'files' => $request->input("files")
        ];
        $response = $this->chatTemplateService->store($data);
        if($response["error"]) {
            return $this->errorResponse($response["message"],"Message Template Failed",400);
        }else{
            return $this->successResponse($response,"Message Template Created");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ChatTemplate  $chatTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$appId,$id)
    {

        $data = [
            'key' => $request->input('key'),
            'content' => $request->input('content'),
            'files' => $request->input("files")
        ];
        // delete file
        //TODO: MAKE SERVICE FOR UPLOAD FILE
        $fileToDelete = json_decode($request->deletedFiles);
        foreach($fileToDelete as $each_file) {
          Storage::disk('public_chat')->delete([ $each_file->fileName ]);
        }

        $chatTemplate = $this->chatTemplateService->update(['id' => $id],$data);
        return $this->successResponse($chatTemplate,'Chat template edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChatTemplate  $chatTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$appId,$id)
    {
        $chatTemplate = $this->chatTemplateService->destroy(['id' => $id]);
        return $this->successResponse(['deleted_id' => $id],'Chat template deleted');
    }
}
