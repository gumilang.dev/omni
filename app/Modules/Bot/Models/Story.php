<?php

namespace App\Modules\Bot\Models;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    protected $connection = 'bot';

    public function storyGroup()
    {
        return $this->belongsTo('App\Modules\Bot\Models\StoryGroup');
    }

}
