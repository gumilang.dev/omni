<?php

namespace App\Modules\Bot\Models;

use Illuminate\Database\Eloquent\Model;

class StoryGroup extends Model
{
    protected $connection = 'bot';
    protected $table = 'story_groups';

    public function stories()
    {
        return $this->hasMany('App\Modules\Bot\Models\Story');
    }
    public function users()
    {
        return $this->belongsToMany('App\Modules\User\Models\User', 'app.story_group_user');
    }
}
