<?php

namespace App\Modules\Bot\Models;

use Illuminate\Database\Eloquent\Model;

class Bot extends Model
{
    protected $connection = 'bot';

    public function participatedIn()
    {
        return $this->morphToMany('App\Modules\Room\Models\Room', 'participable');
    }
    public function messages()
    {
        return $this->morphMany('App\Modules\Message\Models\Message', 'messageable');
    }
    public function users()
    {
      return $this->belongsToMany('App\Modules\User\Models\UserPlatform','user_bots','bot_id','user_id');
    }
    public function userBot(){
      return $this->belongsTo('App\Modules\User\Models\UserBot');
    }
}
