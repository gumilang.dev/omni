<?php

namespace App\Modules\Bot\Services;

use Illuminate\Http\Request;
use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Modules\Integration\Models\Integration;
use App\Modules\ExternalApi\Models\ExternalApi;
use App\Modules\Log\Models\ApiLog;
use App\Modules\Chat\Controllers\ChatController;
use App\Modules\Common\Helpers\GeneralHelper;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class BotService
{

    protected $data;
    protected $lennaApi;
    protected $client;
    public function __construct($data = [])
    {
        $this->data = $data;
        $this->lennaApi = ExternalApi::where(['category' => 'internal', 'provider' => 'lenna-tech'])->first();
        $guzzleProp = ['base_uri' => $this->lennaApi->base_url, 'verify' => false];
        // $guzzleProp = ['base_uri' => 'https://tech.app.lenna.ai/', 'verify' => false];
        $this->client = new Client($guzzleProp);
    }
    public function getIntegration()
    {
        $integration = Integration::where([['app_id', $this->data['appId']], ['status', true]])
            ->whereHas('channel', function ($q) {
                $q->where('name', 'bot');
            })->first();
        return $integration;
    }

    public function nlpResponse($data = [])
    {
        $apiProp = $this->lennaApi->endpoints()->where('name', 'nlp-chat')->first();
        $formData = [
            /* old NLP */
            // 'phrase' => $data['phrase'],
            // 'bot_id' => $data['botId'],
            // 'app_id' => $this->data['appId'],
            // 'user_id' => $data['userId'],
            // 'lat' => $data['lat'],
            // 'lon' => $data['lon'],
            // 'channel' => $data['channel'],
            /* new NLP */
            'bot_id' => $data['botId'],
            'user_id' => $data['userId'],
            'query' => $data['phrase'],
            'location' => $data['lat'].','.$data['lon'],
            'language' => $data['lang']
        ];
        try {
            $response = $this->client->request(
                $apiProp->method,
                $apiProp->endpoint,
                [
                    'headers' => $data['headers'],
                    /* old NLP */
                    // 'form_params' => $formData
                    /* new NLP */
                    'auth'    => ['lenna','w4rt36KHARISMA'],
                    'json' => $formData
                ]
            );
            $response = json_decode($response->getBody()->getContents());
        } catch (RequestException $e) {
            ApiLog::create([
                'url'           => $this->lennaApi->base_url.$apiProp->endpoint,
                'app_id'        => $this->data['appId'],
                'user_id'       => $data['userId'],
                'request'       => json_encode($formData),
                'response'      => $e->getMessage(),
                // 'response'      => $e->getResponse()->getBody(true),
                'ip_address'    => \Request::ip(),
                'user_agent'    => \Request::header('User-Agent'),
                'userable_type' => 'user',
                'userable_id'   => $data['userId'],
            ]);
            $response = false;
        } catch (ServerException $e) {
          ApiLog::create([
              'url'         => $this->lennaApi->base_url.$apiProp->endpoint,
              'app_id'      => $this->data['appId'],
              'user_id'     => $data['userId'],
              'request'     => json_encode($formData),
              // 'response'    => $e->getMessage(),
              'response'    =>  $e->getResponse()->getBody(true),
              'ip_address'  => \Request::ip(),
              'user_agent'  => \Request::header('User-Agent'),
              'userable_type' => 'user',
              'userable_id'   => $data['userId'],
          ]);
          $response = false;
        } catch (ClientException $e) {
            ApiLog::create([
                'url'         => $this->lennaApi->base_url.$apiProp->endpoint,
                'app_id'      => $this->data['appId'],
                'user_id'     => $data['userId'],
                'request'     => json_encode($formData),
                // 'response'    => $e->getMessage(),
                'response'    => $e->getResponse()->getBody(true),
                'ip_address'  => \Request::ip(),
                'user_agent'  => \Request::header('User-Agent'),
                'userable_type' => 'user',
                'userable_id'   => $data['userId'],
            ]);
            $response = false;
        }
        return $response;
    }

    public function send($data = [])
    {
        $chatController = new ChatController;
        $request = new \Illuminate\Http\Request();
        $request->replace([
            'roomId' => $data['roomId']->id,
            'senderId' => $data['senderId'],
            'senderType' => 'bot',
            'message' => $data['message'],
            'quickbutton' => $data['quickbutton'],
            'broadcast' => true,
        ]);
        $response = $chatController->sendMessage($request, $this->data['appId']);
        return $response;

    }

    public function isText($content)
    {
        if ($content->type == 'text') {
            return true;
        }
        return false;
    }

    public function isImage($content)
    {
        if ($content->type == 'image') {
            return true;
        }
        return false;
    }

    public function replyMessage($data = [])
    {
      /* NEW NLP */
      $headers = $data['headers']['Authorization'];
      $headers = explode(' ', $headers);
      $headers = ['token' => $headers[1]];
      /* NEW NLP */
        if (($integration = $this->getIntegration()) && (isset($data['text']) || $this->isText($data['content']) || $this->isImage($data['content']))) {
            $botId = $integration->integration_data['botId'];
            $nlp = $this->nlpResponse([
                'botId' => $botId,
                'userId' => $data['userId'],
                'phrase' => $data['text'] ?? $data['content']->text,
                'lat' => $data['lat'] ?? 0,
                'lon' => $data['lon'] ?? 0,
                'channel' => $data['channel'] ?? 'channel',
                // 'headers' => $data['headers'] ?? [],
                'headers' => $headers ?? [],
                'lang' => $data['lang']
            ]);
            if ($nlp == false) {
                $nlpConfig = array_filter(config('nlp'), function($q) use ($botId) {
                  return $q['botId'] == $botId;
                });
                if ($nlpConfig) {
                  $message = [
                    [
                        'type' => 'text',
                        'text' => $nlpConfig[0]['message'],
                        'speech' => $nlpConfig[0]['message'],
                    ]
                  ];
                } else {
                  $message = [
                    [
                        'type' => 'text',
                        'text' => 'Hai, apakah ada yang bisa saya bantu? Silahkan ketik "Hai" untuk dapat menampilkan menu utama, Terima Kasih',
                        'speech' => 'Hai, apakah ada yang bisa saya bantu? Silahkan ketik "Hai" untuk dapat menampilkan menu utama, Terima Kasih',
                    ]
                  ];
                }
            } else {
                $message = GeneralHelper::objectToArray($nlp->response);
            }
            // if (isset($data['is_broadcast'])) {
            //     return ['nlp' => $nlp];
            // }
            $sendMessage = $this->send([
                'senderId' => $botId,
                'roomId' => $data['room'],
                'message' => $message,
                'quickbutton' => $nlp->quickbutton ?? []
            ]);
            return ['message' => $sendMessage, 'nlp' => $nlp];
        }
        return 'bot not active';
    }


    public function getStoryList()
    {

    }
    public function getInputPhrase($storyId)
    {
        $apiProp = $this->lennaApi->endpoints()->where('name', 'bot-input-phrase')->first();

        $response = $this->client->request(
            $apiProp->method,
            str_replace('{story-id}', $storyId, $apiProp->endpoint)
        );
        $response = json_decode($response->getBody()->getContents());
        return $response;
    }
}
