<?php

namespace App\Modules\Bot\Controllers;

use Illuminate\Http\Request;
use App\Modules\Bot\Models\Story;
use App\Modules\Bot\Models\StoryGroup;
use App\Http\Controllers\ApiController;
use App\Modules\ExternalApi\Models\ExternalApi;
use GuzzleHttp\Client;
use App\Modules\Integration\Models\Integration;
use Hashids\Hashids;
use App\Modules\User\Models\UserBot;

class BotController extends ApiController
{

    public function __construct()
    {
        $this->hashids = new Hashids('', 6);
    }

    public function getAllStory(Request $request, $appId)
    {
        $integration = $this->_getIntegration($appId);
        $botId = $integration->integration_data['botId'];
        $botId = $this->hashids->encode($botId);
        $lennaApi = ExternalApi::where(['category' => 'internal', 'provider' => 'lenna'])->first();
        $guzzleProp = ['base_uri' => $lennaApi->base_url];
        $client = new Client($guzzleProp);

        $apiProp = $lennaApi->endpoints()->where('name', 'bot-story-list')->first();
        $response = $client->request(
            $apiProp->method,
            str_replace("{bot-id}", $botId, $apiProp->endpoint)
        );

        $response = json_decode($response->getBody()->getContents());
        return response()->json($response);
    }

    public function getAllStoryGroup(Request $request, $appId)
    {
        $integration = $this->_getIntegration($appId);
        if (!$integration) {
            return $this->errorResponse(null, 'bot not integrated');
        }
        $botId = $integration->integration_data['botId'];
        // $botId = $this->hashids->decode($integration->integration_data['botId']);
        $storyGroups = StoryGroup::where('bot_id', $botId)->get();
        return $this->successResponse(['storyGroups' => $storyGroups], 'content updated');

    }


    public function _getIntegration($appId)
    {
        $integration = Integration::where([['app_id', $appId], ['status', true]])
            ->whereHas('channel', function ($q) {
                $q->where('name', 'bot');
            })->first();
        return $integration;
    }

    public function getUserBot($userId)
    {
      $decodedUserId = $this->hashids->decode($userId);
      $user_bot = UserBot::where('user_id',$decodedUserId)->with('bot')->has('bot')->get();
      $user_bot = $user_bot->map(function ($value, $key){
        $data = (object)[
          'id' => $value->bot->id,
          'name' => $value->bot->name
        ];
        return $data;
      });
      return $user_bot;
    }


}
