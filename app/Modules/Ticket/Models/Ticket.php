<?php namespace App\Modules\Ticket\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\User\Models\UserPlatform;
use App\Modules\User\Models\User;
use App\Modules\Message\Models\Message;

class Ticket extends Model
{
    protected $table = "submit_ticket";
    protected $fillable = ["subject","status","room_id","app_id","type","agent_id","priority","user_id","media","start_at","user_info","ticket_id","files","description","solved_at","resolution_time"];
    protected $appends = ["agent","created_by"];
    protected $casts = [
      "files" => "array",
      "user_info" => "object"
    ];
    public function getAgentAttribute() {
        return UserPlatform::where("id",$this->attributes['agent_id'])->first();
    }

    public function getCreatedByAttribute()
    {
        if($this->attributes["media"] == "chat") return User::where('id', $this->attributes['user_id'])->first();
        return [];
    }


    public function chat($data) {
        return Message::where('room_id', $data['room_id'])
                    ->where("created_at",">=",$data["start_at"])
                    ->orderBy("id","asc")->get();
    }

}
