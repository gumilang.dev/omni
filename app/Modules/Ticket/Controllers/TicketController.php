<?php namespace App\Modules\Ticket\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Services\TicketServiceImpl;

class TicketController extends ApiController
{
    protected $service;
    public function __construct(TicketServiceImpl $ticketService) {
        $this->service = $ticketService;
    }

    public function all(Request $request, $appId) {
        $agent_id = $request->get("agent_id");
        if($request->is_filter) {
            return $this->service
                        ->filterTicket(
                          $appId,
                          [
                            "status" => $request->status,
                            "priority" => $request->priority,
                            "type" => $request->type,
                            "agent_id" => $request->agent_id,
                            "solved_at" => json_decode($request->solved_at),
                            "created_at" => json_decode($request->created_at)
			                    ],
// 			    "solved_at" => $request->solved_at,
// 			    "created_at" => $request->created_at
// 			  ],
                          $request->get("per_page")
                        );
        }else{
            if($agent_id) {
              $response = $this->service->getTicket(
                $request,
                [
                    'app_id' => $appId,
                    "agent_id" => $agent_id
                ],
                ['subject',"status"]
              );
            }else{
              $response = $this->service->getTicket(
                $request,
                [
                    'app_id' => $appId,
                ],
                ['subject',"status"]
              );
            }

        }

        return response()->json($response);
    }

    public function create(Request $request, $appId) {
        $response = $this->service->createTicket($request,$appId);
        if($response["error"]) {
            return $this->errorResponse($response["message"],"Submit ticket failed",400);
        }else{
            return $this->successResponse($response,"Submit Ticket Created");
        }
    }

    public function destroy(Request $request,$appId) {
        $response = $this->service->deleteTicket(["id" => $request->post("id")]);
        return $this->successResponse($response, "Ticket Deleted");
    }

    public function update(Request $request, $appId,$id) {
        $response = $this->service->updateTicket(
            ["id" => $id],
            $request
        );
        if($response["error"]) {
            return $this->errorResponse($response["message"],"Update ticket failed",400);
        }else{
            return $this->successResponse($response,"Update Ticket success");
        }
    }

    public function show(Request $request, $appId, $room_id) {
        $response = $this->service->getBy(["room_id" => $room_id]);
        return $this->successResponse($response,"fetched");
    }

    public function getInformation(Request $request,$appId,$id) {
        $response = $this->service->getInformationByMedia($id);
        return $response;
    }

    public function exportTicket(Request $request, $appId) {
        $condition = [
            "status" => $request->status,
            "priority" => $request->priority,
            "type" => $request->type,
            "agent_id" => $request->agent_id,
            "solved_at" => json_decode($request->solved_at),
            "created_at" => json_decode($request->created_at)
        ];
        $response_from_service = $this->service->exportTicket($appId,$condition);
        return $this->successResponse($response_from_service,"success");
    }
}
