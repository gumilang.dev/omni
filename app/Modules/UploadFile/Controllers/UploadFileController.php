<?php

namespace App\Modules\UploadFile\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Validator;


class UploadFileController extends ApiController {

  public function uploadFile(Request $request) {

    $validator = Validator::make($request->all(), [
      'file' => 'file|max:5120'
    ]);

    if ($validator->fails()) {
      return $this->errorResponse($validator->errors()->first('file'), "Fail upload image");
    }

    if($request->hasFile("file")) {
      $name = $request->file('file')->getClientOriginalName();
      $file = $request->file('file');
      $extension =  $request->file('file')->extension();
      $allowedExension = ["jpeg","jpg","gif","png","webm","mpg","ogg","mp4","avi","mov","doc","dot","wbk","docx","docm","dotx","docb","xls","xlt","xlm","xlsx","ppt","pot","pps","pdf","PNG","JPG","JPEG"];

      if(in_array($extension,$allowedExension)) {
        Storage::disk('public_chat')->put($name, file_get_contents($file));
        return $this->successResponse(null, 'file uploaded');
      }else{
        return $this->errorResponse(null,"extension not allowed");
      }
    }else{
      return $this->errorResponse(null,"file cannot be null");
    }
  }

  public function deleteFile(Request $request) {

    $fileToDelete = [];
    Storage::disk('public_chat')->delete([$request->fileToDelete]);
    return $this->successResponse(null, 'file deleted');

  }

}
