<?php

namespace App\Modules\Tag\Model;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['name','app_id'];

    public function rooms() {
        return $this->belongsToMany('App\Modules\Room\Models\Room')->withPivot('id');
    }
}
