<?php namespace App\Modules\Tag\Controller;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Modules\Tag\Model\Tag;
use App\Modules\Room\Models\Room;
use App\Modules\RoomTag\Model\RoomTag;
use App\Services\TagServiceImpl;
use App\Services\PaginatorServiceImpl;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class TagController extends ApiController
{
    private $tag;
    private $roomTag;
    private $tagService;
    private $paginator;

    public function __construct(Tag $tag,RoomTag $roomTag,TagServiceImpl $tagService,PaginatorServiceImpl $paginator) {
        $this->tag = $tag;
        $this->roomTag = $roomTag;
        $this->tagService = $tagService;
        $this->paginator = $paginator;
    }
    /**
     * get all tag
     * @return array tag
     */
    public function index(Request $request,$appId) {
        $tag = $this->tagService->paginate(
            $request,
            ['app_id' => $appId],
            ['name']
        );
        return response()->json($tag);
    }

    /**
     * create tag
     * @param  object datas
     * @return object data
     */
    public function create(Request $request,$appId) {
        $validator = Validator::make($request->all(), [
          'name' => [
            'required',
            'string',
            'alpha_dash',
            'max:30',
            Rule::unique('tags', 'name')->where(function ($q) use ($appId){
              return $q->where('app_id', $appId);
            })
          ]
        ]);
        if ($validator->fails()) {
          return $this->errorResponse(null, $validator->errors()->first());
        }
        $data = [
            'name' => htmlspecialchars($request->input('name')),
            'app_id' => $appId
        ];
        $tag = $this->tagService->store($data);
        return $this->successResponse($tag,'Tag creatad');
    }

    /**
     * report tag
     * @param string appId
     * @return array data
     */
    public function report(Request $request, $appId) {
        $sort = $request->get('sort');
        $filterText = $request->get('filterText');
        $per_page = $request->get('per_page');
        $startFilterDate = $request->get('start');
        $endFilterDate = $request->get('end');

        $room = Room::where('app_id', $appId)->with(['channel','tags'])->whereHas('users', function ($query) use ($request,$startFilterDate,$endFilterDate,$filterText) {
            $query->where('deleted_at', '=', null);
        })->where(function($q) use ($startFilterDate, $endFilterDate) {
          $q->whereHas('users', function($u) use ($startFilterDate, $endFilterDate) {
            if (!empty($startFilterDate) && !empty($endFilterDate)) {
              $u->whereDate('users.created_at', '>=', $startFilterDate)
                ->whereDate('users.created_at', '<=', $endFilterDate);
            }
          })->orWhereHas('tags', function($t) use ($startFilterDate, $endFilterDate) {
            if (!empty($startFilterDate) && !empty($endFilterDate)) {
              $t->whereDate('tags.created_at', '>=', $startFilterDate)
                ->whereDate('tags.created_at', '<=', $endFilterDate);
            }
          });
        })->orderBy("created_at","DESC")->get();


        if($filterText) {
          $room = $room->filter(function ($item) use ($filterText) {
              return false !== stristr($item->created_by["name"], $filterText) || false !== stristr($item->channel["name"],$filterText) || false !== array_search($filterText, array_column(json_decode($item->tags),"name"));
          })->values();
        }

        if (!$per_page) {
            $per_page = 15;
        }

        $data =  $this->paginator->paginate($room,$per_page);
        return response()->json($data);
    }

    /**
     * delete tag
     * @param any id
     * @return array tag
     */
    public function delete($appId,$id) {
      $data = $this->tagService->destroy(['id' => $id]);
      return $this->successResponse(['deleted_id' => $data],'Tag deleted');
    }

    /**
     * add tag with room
     * @param any data
     * @return array data
     */
    public function tagWithRoom(Request $request,$appId) {
        $validator = Validator::make($request->all(), [
          'name' => [
            'required',
            'string',
            'alpha_dash',
            'max:30',
            Rule::unique('tags', 'name')->where(function ($q) use ($appId){
              return $q->where('app_id', $appId);
            })
          ]
        ]);
        if ($validator->fails()) {
          return $this->errorResponse(null, $validator->errors()->first());
        }
        $tag = $this->tag::create([
            'name' => htmlspecialchars($request->input('name')),
            'app_id' => $appId
        ]);

        if($tag) {
            $data = [
                'room_id' => $request->input('room_id'),
                'tag_id' => $tag->id
            ];

            $room_tags = $this->roomTag::create($data);
            $tag->pivot = $room_tags;
            return $this->successResponse($tag,'RoomTag creatad with chat room');
        } else {
            return $this->errorResponse('Error','Something Went Wrong');
        }
    }
}
