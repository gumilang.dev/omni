<?php

namespace App\Modules\Livechat\Controllers;
use App\Modules\App\Models\App;
use App\Modules\CustomIntegration\Services\CustomIntegrationService;
use App\Modules\Livechat\Models\Livechat;
use App\Modules\Role\Models\Role;
use App\Modules\QontakTicketing\Services\QontakTicketingService;
use Illuminate\Http\Request;
use App\Modules\Room\Models\Room;
use App\Modules\User\Models\UserApp;
use Carbon\Carbon;
use App\Modules\User\Models\User;
use App\Events\RequestLive;
use App\Events\CancelRequestLiveAgent;
use App\Events\RequestHandled;
use App\Events\ChatResolved;
use App\Http\Controllers\ApiController;
use App\Modules\Room\Controllers\RoomController;
use App\Services\TimeReportServedImpl as TimeReportServedService;
use App\Modules\Livechat\Models\LogHandleChatByAdmin;
use App\Services\LogHandleRoomServiceImpl;
use App\Modules\Role\Helpers\RoleHelper;
use Log;
use DB;
use App\Modules\Log\Helpers\WebhookHelper;
use App\Services\SessionServiceImpl;

class LivechatController extends ApiController
{
    public $roomController;
    public $timeReportServedService;
    public $sessionService;
    public $logHandleRoom;
    public $customIntegrationService;
    private $qontakTicketingService;

  public function __construct(RoomController $roomController,TimeReportServedService $timeReportServedService, LogHandleRoomServiceImpl $logHandleRoom,SessionServiceImpl $sessionService, CustomIntegrationService $customIntegrationService, QontakTicketingService $qontakTicketingService) {
        $this->roomController = $roomController;
        $this->timeReportServedService = $timeReportServedService;
        $this->logHandleRoom = $logHandleRoom;
        $this->sessionService = $sessionService;
        $this->customIntegrationService = $customIntegrationService;
        $this->qontakTicketingService = $qontakTicketingService;
    }

    public function cancelLivechat(Request $request,$appId) {
      $room_id = $request->room_id;
      $livechat = Livechat::where("room_id",$room_id)->first();
      if($livechat["status"] == "request") {
        $livechat->delete();
      }
      $total = $this->roomController->getRoomByStatus($appId,"request");
      broadcast(new CancelRequestLiveAgent($appId,$total));
      return $this->successResponse([],"Live Agent Cancelled");
    }

    public function autoAgentAllocation($appId) {

      $rooms  = Room::where('app_id', $appId)->with(['channel','tags','additional_informations','notes'])->whereHas('liveChats', function ($q) {
        $q->where('status', 'request')->where('end_at', NULL)->where('request_at','!=',NULL);
      })->get()->sortBy('livechat.request_at')->values();

      // \Log::info($rooms);

      foreach($rooms as $value) {
        $agents = RoleHelper::agents($appId);

        $data = [];
        if(count($agents) > 0) {

          foreach($agents as $each_agent) {
            array_push($data,[
              "user_id" => $each_agent->id,
              "chat_live_count" => $this->roomController->getRoomByStatus($appId,"live",["agent_id" => $each_agent->id])
            ]);
          }


          $minLivechatCount = array_reduce($data,function($a,$b) {
            return $a["chat_live_count"] < $b["chat_live_count"] ? $a : $b;
          },array_shift($data));

        }




        if(count($agents) > 0) {
          if($value->livechat == null) {
            $liveChat = Livechat::create([
              'status' => 'live',
              'room_id' => $value->id,
              'request_at' => Carbon::now()->toDateTimeString(),
              'start_at' => Carbon::now()->toDateTimeString(),
              'handle_by' => $minLivechatCount["user_id"],
              'updated_by' => $minLivechatCount["user_id"]
            ]);
          } else {
            $liveChat = Livechat::where('id', $value->livechat->id)->update(
              [
                'status' => 'live',
                'start_at' => Carbon::now()->toDateTimeString(),
                'handle_by' => $minLivechatCount["user_id"],
                'updated_by' => $minLivechatCount["user_id"]
              ]
            );
          }

          $currentLivechat = Livechat::find($value->livechat->id);
          $user = [
            'id'    => $value->created_by->id,
            'type'  => 'user'
          ];

          $value->unread_count = Room::getUnreadCount($value->id, $user);

          $value->updated_at = Carbon::now()->toDateTimeString();

          // $total = $this->roomController->getRoomByStatus($appId,"request");
          $queue = WebhookHelper::getQueue($appId);
          broadcast(new RequestHandled($appId, $value, $currentLivechat, count($queue), $queue));
        }
      }
    }

    public function checkStatusLivechat(Request $request,$appId) {
      $room_id = $request->input("room_id");
      $isExist  = Livechat::where(["room_id" => $room_id,"status" => "request"])->get();
      if(count($isExist) > 0) {
        return $this->successResponse(["current_status_livechat" => true],"success check status livechat");
      }else{
        return $this->successResponse(["current_status_livechat" => false],"failed check status livechat");
      }
    }

    public function requestLivechat2(Request $request,$appId) {
       // check configuration
       $app = App::find($appId);
       $hasRequestHaveRoleProperty  = $request->role;
       $hasRequestHaveAdminProperty  = $request->adminId;

       $room = User::find($request->userId)->rooms->first();

       if($hasRequestHaveRoleProperty) {
         $this->logHandleRoom->createLog([
           "app_id" => $appId,
           "room_id" => $room->id,
           "user_id" => $hasRequestHaveAdminProperty
         ]);
       }

       if($room->livechat !== null && $room->livechat["status"] == "request") {
           $response = [
               "livechat" => $room->livechat
           ];
           return $this->successResponse($response,"you already request livechat, now you are in the queue");
       }else {
           $liveChat = $room->liveChats()->firstOrCreate(
               ['room_id' => $room->id, 'status' => 'request'],
               [
                   "request_at" => Carbon::now()->toDateTimeString(),
               ]
           );
           $queue = WebhookHelper::getQueue($appId);
           broadcast(new RequestLive($room->id, $appId, $request->userId, $liveChat->id, $queue));
           if($app->agent_allocation_automation) {
             $this->autoAgentAllocation($appId);
           }
           return $this->successResponse(['liveChat' => $liveChat], 'livechat requested');
       }
    }

    public function requestLivechat(Request $request, $appId)
    {
      // check configuration
      $app = App::find($appId);
      $hasRequestHaveRoleProperty  = $request->role;
      $hasRequestHaveAdminProperty  = $request->adminId;

      $room = User::find($request->userId)->rooms->first();

      if($hasRequestHaveRoleProperty) {
        $this->logHandleRoom->createLog([
          "app_id" => $appId,
          "room_id" => $room->id,
          "user_id" => $hasRequestHaveAdminProperty
        ]);
      }

      if($room->livechat == null) {
        $liveChat = $room->liveChats()->create([
          "room_id" => $room->id,
          "status" => "request",
          "request_at" => Carbon::now()->toDateTimeString()
        ]);
        $queue = WebhookHelper::getQueue($appId);
        broadcast(new RequestLive($room->id, $appId, $request->userId, $liveChat->id, $queue));
        if($app->agent_allocation_automation) {
          $this->autoAgentAllocation($appId);
        }
        return $this->successResponse(['liveChat' => $liveChat], 'livechat requested');
      }else {
        return $this->successResponse([],"cannot request livechat");
      }
    }

    public function handleLivechat2(Request $request, $appId) {
      DB::beginTransaction();
      $room = Room::where('app_id', $appId)->with(['channel','tags','additional_informations','notes'])->whereHas('liveChats', function ($q) {
        $q->where('status', 'request')->where('end_at', NULL)->where('request_at','!=',NULL);
      })->get()->sortBy('livechat.request_at')->take(1)->values()->first();

      // create livechat if null
      if($room->livechat == null) {
        $liveChat = Livechat::create([
          'status' => 'live',
          'room_id' => $room->id,
          'request_at' => Carbon::now()->toDateTimeString(),
          'start_at' => Carbon::now()->toDateTimeString(),
          'handle_by' => $request->agentId,
          'updated_by' => $request->agentId
        ]);
      } else {
        // Livechat::where(["room_id" => $room->id, "status" => "live"]) // fix double livechat
        //         ->delete();
        $liveChat = Livechat::where('id', $room->livechat->id)->update(
          [
            'status' => 'live',
            'start_at' => Carbon::now()->toDateTimeString(),
            'handle_by' => $request->agentId,
            'updated_by' => $request->agentId
          ]
        );
      }

      $currentLivechat = Livechat::find($room->livechat->id);
      $startTime = Carbon::parse($currentLivechat->request_at);
      $finishTime = Carbon::parse($currentLivechat->start_at);
      $agentInfo = $this->roomController->getAgentinfo($currentLivechat->handle_by);
      // create report time served
      $this->timeReportServedService->createReport([
        "request_at" => $currentLivechat->request_at,
        "served_at" => $currentLivechat->start_at,
        "room_id" => $currentLivechat->room_id,
        "agent_id" => $currentLivechat->handle_by,
        "diff" => $finishTime->diffInMinutes($startTime),
        "app_id" => $appId,
        "agent_name" => $agentInfo->name,
        "agent_email" => $agentInfo->email
      ]);

      // $user = [
      //   'id' => $request->agentId,
      //   'type' => 'user_platform'
      // ];

      $user = [
        'id'    => $room->created_by->id,
        'type'  => 'user'
      ];

      $room->unread_count = Room::getUnreadCount($room->id, $user);

      $room->updated_at = Carbon::now()->toDateTimeString();

      $queue = WebhookHelper::getQueue($appId);

      broadcast(new RequestHandled($appId, $room, $currentLivechat, count($queue), $queue));
      DB::commit();
      return $this->successResponse([
        'liveChat' => $currentLivechat,
        'room' => $room,
        'count' => count($queue),
        'queue' => $queue
      ], 'Take over chat success, you are now connected');
    }

    public function handleLivechat(Request $request,$appId) {
      $room = Room::where('app_id', $appId)->with(['channel','tags','additional_informations','notes'])->whereHas('liveChats', function ($q) {
        $q->where('status', 'request')->where('end_at', NULL)->where('request_at','!=',NULL);
      })->get()->sortBy("livechat.id")->take(1)->values()->first();
      // create livechat if null

      if(is_null($room)) {
        broadcast(new RequestHandled($appId, [], [], 0, []));
        return $this->successResponse([
        ], 'Room doesnt exist');
      }

      if($room->livechat == null) {
        $liveChat = Livechat::create([
          'status' => 'live',
          'room_id' => $room->id,
          'request_at' => Carbon::now()->toDateTimeString(),
          'start_at' => Carbon::now()->toDateTimeString(),
          'handle_by' => $request->agentId,
          'updated_by' => $request->agentId
        ]);
      } else {
        // Livechat::where(["room_id" => $room->id, "status" => "live"]) // fix double livechat
        //         ->delete();
        // check if double livechat, and delete livechat that have status live and resolved
        $allLivehat = Livechat::where(["room_id" => $room->id])->get()->values();
        foreach($allLivehat as $each_livechat) {
          if($each_livechat->status == "resolved" || $each_livechat->status == "live") {
            $each_livechat->delete();
          }else{
            $each_livechat->update([
              'status' => 'live',
              'start_at' => Carbon::now()->toDateTimeString(),
              'handle_by' => $request->agentId,
              'updated_by' => $request->agentId
            ]);
          }
        }
      }

      $currentLivechat = Livechat::where(["room_id" => $room->id, "status" => "live"])->first();
      $startTime = Carbon::parse($currentLivechat->request_at);
      $finishTime = Carbon::parse($currentLivechat->start_at);
      $agentInfo = $this->roomController->getAgentinfo($currentLivechat->handle_by);

      $this->timeReportServedService->createReport([
        "request_at" => $currentLivechat->request_at,
        "served_at" => $currentLivechat->start_at,
        "room_id" => $currentLivechat->room_id,
        "agent_id" => $currentLivechat->handle_by,
        "diff" => $finishTime->diffInMinutes($startTime),
        "app_id" => $appId,
        "agent_name" => $agentInfo->name,
        "agent_email" => $agentInfo->email
      ]);

      $user = [
        'id' => $request->agentId,
        'type' => 'user_platform'
      ];

      $user = [
        'id'    => $room->created_by->id,
        'type'  => 'user'
      ];

      $room->unread_count = Room::getUnreadCount($room->id, $user);

      $room->updated_at = Carbon::now()->toDateTimeString();

      $queue = WebhookHelper::getQueue($appId);

      broadcast(new RequestHandled($appId, $room, $currentLivechat, count($queue), $queue));
      DB::commit();
      return $this->successResponse([
        'liveChat' => $currentLivechat,
        'room' => $room,
        'count' => count($queue),
        'queue' => $queue
      ], 'Take over chat success, you are now connected');
    }

    public function stopLivechat(Request $request, $appId)
    {
        $room = User::find($request->userId)->rooms->first();

        if ($request->filled('agentId')) {
          $agent_info = $this->roomController->getAgentInfo($request->agentId);
        } else {
          $agent_info = $this->roomController->getAgentInfo($room->livechat->handle_by);
        }

        if ($request->filled('liveChatId')) {
          Livechat::where('id', $request->liveChatId)->update(
            [
              'status' => 'resolved',
              'end_at' => Carbon::now()->toDateTimeString(),
              'updated_by' => $request->agentId
            ]
          );
          $liveChat = Livechat::find($request->liveChatId);
        } else {
          Livechat::where('id', $room->livechat->id)->update(
            [
              'status' => 'resolved',
              'end_at' => Carbon::now()->toDateTimeString(),
              'updated_by' => $agent_info->id
            ]
          );
          $liveChat = Livechat::find($room->livechat->id);
        }
        $this->sessionService->createSession(["room_id" => $liveChat->room_id, "start_at" => $liveChat->start_at, "end_at" => $liveChat->end_at, "handle_by" => $liveChat->handle_by,'user_id' => $request->userId]);


        // send broadcast to user
        broadcast(new ChatResolved($appId, $liveChat, $agent_info, $request->userId, $request->description));

        if($this->customIntegrationService->isIntegrated($appId,"qontak-crm")) {
          $this->qontakTicketingService->updateTicket([
            "email" => $room->created_by->email,
            "room_id" => $room->id,
            "name" => $room->created_by->name,
            "chat_history" => $this->qontakTicketingService->getHistoryChat($room->id,$liveChat->start_at,$liveChat->end_at),
            "phone" => $room->created_by->phone,
            "nickname" => $room->created_by->nickname,
            "contact_id" => $room->created_by->contact_id,
            "type" => "update",
            "session" => [
              "start_at" => $liveChat->start_at,
              "end_at" => Carbon::now()->toDateTimeString()
            ]
          ],
            $room->id
          );
        }

        return $this->successResponse(['liveChat' => $liveChat], 'Livechat resolved');
    }
}
