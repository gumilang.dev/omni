<?php
namespace App\Modules\Livechat\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Livechat\Services\LivechatServiceImpl;
use App\Shared\Common\WebResponse;

class LivechatControllerNew extends Controller
{

    private $livechatService;

    public function __construct(LivechatServiceImpl $livechatService) {
      $this->livechatService = $livechatService;
    }

    public function request(Request $request) {
      $call_service = $this->livechatService->request($request->user_id,$request->administrator_id);
      return $this->catchError($call_service, function($response) {
        return WebResponse::create(200, "OK", $response, "success request livechat");
      });
    }

    public function handle(Request $request,$appId) {
      $call_service = $this->livechatService->handle($request->agent_id,$appId);
      return $this->catchError($call_service, function($response) {
        return WebResponse::create(200, "OK", $response, "take over success, you are now connected");
      });
    }

    public function resolve(Request $request) {
      $call_service = $this->livechatService->resolve($request->room_id,$request->updated_by);
      return $this->catchError($call_service, function($response) {
        return WebResponse::create(200, "OK", $response, "success resolve livechat");
      });
    }

    public function assignAgent(Request $request) {
      $call_service = $this->livechatService->assignAgent($request->room_id, $request->agent_id, $request->group_id,$request->assignee,$request->assignor, $request->assignor_role);
      return $this->catchError($call_service, function($response) {
        return WebResponse::create(200, "OK", $response, "success assign to agent");
      });
    }

    public function assignGroup(Request $request) {
      $call_service = $this->livechatService->assignGroup($request->group_id, $request->room_id, $request->assignor, $request->assignor_role);

      return $this->catchError($call_service, function($response) {

        return WebResponse::create(200, "OK", $response, "success assign to group");
      });
    }

    public function unservedRoom(Request $request, $appId) {
      $call_service = $this->livechatService->unservedRoom($appId);
      return $this->catchError($call_service, function($response) {
        return WebResponse::create(200, "OK", $response, "success get unserved room");
      });
    }

    public function cancel(Request $request, $appId) {
      $call_service = $this->livechatService->cancel($request->user_id);
      return $this->catchError($call_service, function($response){
        return WebResponse::create(200, "OK", $response, "success cancel live agent");
      });
    }

    public function reportingAssignment(Request $request, $appId) {
      $is_paginated = $request->paginated == "true" ? true : false;

      $call_service = $this->livechatService->reportingAssignment($appId,$is_paginated,$request->start ,$request->end,$request->filterText);
      return $this->catchError($call_service, function($response) {
        return WebResponse::clean($response);
      });
    }
}
