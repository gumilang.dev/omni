<?php namespace app\Modules\Livechat\Response;

class LivechatResponse {
  public $id;

  public $room_id;

  public $handle_by;

  public $updated_by;

  public $status;

  public $description;

  public $start_at;

  public $end_at;

  public $request_at;

  public $group_id;

  public function __construct(
      $id,
      $room_id,
      $handle_by,
      $updated_by,
      $status,
      $description,
      $start_at,
      $end_at,
      $request_at,
      $group_id
  ) {
    $this->id = $id;
    $this->room_id = $room_id;
    $this->handle_by = $handle_by;
    $this->updated_by = $updated_by;
    $this->status = $status;
    $this->description = $description;
    $this->start_at = $start_at;
    $this->end_at = $end_at;
    $this->request_at = $request_at;
    $this->group_id = $group_id;

  }

}
