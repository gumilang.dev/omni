<?php

namespace App\Modules\Livechat\Models;

use Illuminate\Database\Eloquent\Model;

class Livechat extends Model
{
    protected $connection = 'pgsql';

    protected $guarded = [];
    public $timestamps = false;
    public function room()
    {
        return $this->belongsTo('App\Modules\Room\Models\Room');
    }
    public function userPlatforms()
    {
        return $this->belongsTo('App\Modules\User\Models\UserPlatform');
    }
}
