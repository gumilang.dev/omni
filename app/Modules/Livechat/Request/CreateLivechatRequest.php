<?php namespace App\Modules\Livechat\Request;

class CreateLivechatRequest {

  private $room_id;
  private $handle_by;
  private $start_at;
  private $end_at;
  private $status;
  private $request_at;
  private $group_id;

  public function __construct($room_id, $handle_by,$status,$request_at = null,$start_at = null,$end_at = null,$group_id = null) {
    $this->room_id = $room_id;
    $this->handle_by = $handle_by;
    $this->start_at = $start_at;
    $this->end_at = $end_at;
    $this->status = $status;
    $this->request_at = $request_at;
    $this->group_id = $group_id;
  }

  public function toArray() {
    return [
      "room_id" => $this->room_id,
      "handle_by" => $this->handle_by,
      "start_at" => $this->start_at,
      "end_at" => $this->end_at,
      "status" => $this->status,
      "request_at" => $this->request_at,
      "group_id" => $this->group_id
    ];
  }
}
