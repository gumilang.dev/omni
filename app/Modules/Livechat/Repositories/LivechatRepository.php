<?php namespace App\Modules\Livechat\Repositories;

use App\Shared\Repositories\AbstractRepository;

interface LivechatRepository extends AbstractRepository{
  public function getByLivechatId($livechat_id);
  public function getByRoomId($room_id);
  public function getByAgentId($agent_id);
  public function getHistoryAssignment($app_id);
  public function createHistoryAssignment($customer_name,$customer_email, $assignor,$assignor_role,$assignee,$app_id);
  public function createHistoryAssignmentGroup($customer_name, $customer_email, $assignor, $assignor_role, $app_id, $group_id);
  public function getServedByAppId($app_id);
}
