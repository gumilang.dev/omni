<?php namespace App\Modules\Livechat\Repositories\Impl;

use App\Shared\Repositories\AbstractRepositoryImpl;
use App\Modules\Livechat\Repositories\LivechatRepository;
use App\Modules\Livechat\Models\Livechat;
use DB;

class LivechatRepositoryImpl  extends AbstractRepositoryImpl implements LivechatRepository {

  public function __construct() {
    $this->model(new Livechat());
  }

  public function getByLivechatId($livechat_id) {
    return $this->model::find($livechat_id);
  }

  public function getByRoomId($room_id) {
    return $this->model::where("room_id", $room_id)->first();
  }

  public function getByAgentId($agent_id) {
    return $this->model::where("handle_by",$agent_id)->get();
  }

  public function createHistoryAssignment($customer_name,$customer_email, $assignor,$assignor_role,$assignee,$app_id) {
    $is_success = DB::table("history_assignments")
              ->insert([
                "customer_name" => $customer_name,
                "customer_email" => $customer_email,
                "assignor" => $assignor,
                "assignor_role" => $assignor_role,
                "assignee" => $assignee,
                "app_id" => $app_id,
                "created_at" => $this->getCurrentTimeInTimestamp(),
                "updated_at" => $this->getCurrentTimeInTimestamp()
              ]);
    if($is_success) return true;
    return false;
  }

  public function createHistoryAssignmentGroup($customer_name, $customer_email, $assignor, $assignor_role, $app_id, $group_id) {
    $is_success = DB::table("history_group_assignments")
                    ->insert([
                      "customer_name" => $customer_name,
                      "customer_email" => $customer_email,
                      "assignor" => $assignor,
                      "assignor_role" => $assignor_role,
                      "group_id" => $group_id,
                      "app_id" => $app_id,
                      "created_at" => $this->getCurrentTimeInTimestamp(),
                      "updated_at" => $this->getCurrentTimeInTimestamp()
                    ]);
    if ($is_success) return true;
    return false;
  }

  public function getHistoryAssignment($app_id,$paginated = true,$fn = null) {
    if($paginated) {
      return DB::table("omnichannel.history_assignments as ha")
              ->where("ha.app_id",$app_id)
              ->when(is_callable($fn),function($query) use($fn) {
                $fn($query);
              })
              ->paginate(15);
    }else {
      return DB::table("omnichannel.history_assignments as ha")
              ->where("ha.app_id",$app_id)
              ->when(is_callable($fn),function($query) use($fn) {
                $fn($query);
              })
              ->get();
    }

  }

  public function getServedByAppId($appId)
  {
    return $this->model::whereHas('room', function($q) use ($appId) {
      $q->where('app_id', $appId);
    })->where('status', 'live')->get();
  }

}
