<?php namespace App\Modules\Livechat\Services;

use App\Modules\Livechat\Repositories\LivechatRepository;
use App\Modules\Room\Repositories\RoomRepository;
use App\Modules\Group\Repositories\GroupRepository;
use App\Events\LivechatRequest;
use App\Events\LivechatHandle;
use App\Events\LivechatResolved;
use App\Events\LivechatAssigned;
use App\Events\LivechatCancel;
use App\Exceptions\RecordNotFoundException;
use App\Exceptions\RecordNotExpectedException;
use App\Exceptions\BadRequestParameterException;
use App\Exceptions\RecordDuplicateException;
use App\Modules\Livechat\Request\CreateLivechatRequest;
use App\Shared\Common\BaseService;
use App\Modules\Log\Helpers\WebhookHelper;
use DB;

class LivechatServiceImpl extends BaseService {

  private $livechatRepository;
  private $roomRepository;
  private $groupRepository;

  public function __construct(LivechatRepository $livechatRepository, RoomRepository $roomRepository, GroupRepository $groupRepository) {
    $this->livechatRepository = $livechatRepository;
    $this->roomRepository = $roomRepository;
    $this->groupRepository = $groupRepository;
  }

  public function request($user_id, $administrator_id = null) {
    $room = $this->roomRepository->getByUserId($user_id);

    if(is_null($room)) {
      return new RecordNotFoundException("user with id " . $user_id . "is not found in our system");
    }

    $chech_livechat_is_exist = $this->livechatRepository->getByRoomId($room->id);

    // livechat already exist
    if(!is_null($chech_livechat_is_exist)) {
      return new RecordDuplicateException("user with id " . $user_id . "already request livechat");
    }

    $create_livechat_request = new CreateLivechatRequest($room->id, null, "request", $this->getCurrentTimeInTimestamp());

    $this->livechatRepository->insert($create_livechat_request->toArray());

    $unserved_room = $this->roomRepository->getAllUnservedRoom($room->app_id);

    $updated_room = $this->roomRepository->getByUserId($user_id);

    $this->dispatch(new LivechatRequest($updated_room,$unserved_room,$administrator_id));

    return $updated_room;
  }

  public function resolve($room_id,$updated_by) {
    $room = $this->roomRepository->getById($room_id);

    // room doesnot exist
    if(is_null($room)) {
      return new RecordNotFoundException("room with id " . $room_id . "is not found in our system");
    }

    // livechat doesnot exist
    if(is_null($room->livechat)) {
      return new RecordNotFoundException("livechat with room id ".$room_id." is not found in our system");
    }

    // livechat status = "request"
    if($room->livechat->status == "request" || $room->livechat->status == "resolved") {
      return new RecordNotExpectedException("livechat status is not expected, expected status is live");
    }

    $this->livechatRepository->update([
      "room_id" => $room->id
      ],
      [
      "updated_by" => $updated_by,
      "status" => "resolved",
      "end_at" => $this->getCurrentTimeInTimestamp()
      ]
    );

    $updated_room = $this->roomRepository->getById($room_id);

    $this->dispatch(new LivechatResolved($updated_room));

    return true;
  }

  public function handle($agent_id,$app_id) {

    if(is_null($app_id) || is_null($agent_id)) {
      return new BadRequestParameterException("user_id or app_id cannot be null");
    }

    /** GET READY TO HANDLE ROOM DATA */

    // GET UNSERVED ROOM BY FIFO METHOD AND USE DATA IN VARIABLE ROOM
    $room = $this->roomRepository->getByUnservedRoom($app_id);

    // GET ALL GROUPS OF CURRENT AGENT
    $groups = $this->groupRepository->getGroupByAgentId($app_id, $agent_id);

    // IF HAVE GROUP
    if (count($groups) > 0) {
      // GET UNSERVED ROOM BY FIFO METHOD FOR GROUPS OF CURRENT AGENT
      $groupsId = collect($groups)->pluck('id')->toArray();
      $unservedRoomGroups = $this->roomRepository->getUnservedRoomByGroupsId($app_id, $groupsId);

      // IF HAVE UNSERVED ROOM FOR GROUPS OF CURRENT AGENT
      if ($unservedRoomGroups !== null) {
        // USE DATA IN VARIABLE ROOM
        $room = $unservedRoomGroups;
      }
    }

    /** END OF GET READY TO HANDLE ROOM DATA */

    // ROOM DOESNT EXIST
    if (is_null($room)) {
      return new RecordNotFoundException("room is not available");
    }

    // IF LIVECHAT GROUP
    if ($room->livechat->group_id !== null) {
      // CHECK AGENT HAVE GROUPS OR NOT
      if (count($groups) > 0) {
        // IF READY TO HANDLE ROOM DATA GROUP HAVE EXISTING GROUP ON LIST GROUPS FOR CURRENT AGENT
        $groupsId = collect($groups)->pluck('id')->toArray();
        if (in_array($room->livechat->group_id, $groupsId)) {
          $payload = [
            'roomId' => $room->id,
            'groupId' => $room->livechat->group_id,
            'agentId' => $agent_id
          ];
          $this->handleByGroup($payload);
        } else {
          // GET UNSERVED ROOM BY CURRENT GROUP OF AGENTS
          $room = $this->roomRepository->getUnservedRoomByGroupsId($app_id, $groupsId);

          if ($room !== null) {
            if (in_array($room->livechat->group_id, $groupsId)) {
              $payload = [
                'roomId' => $room->id,
                'groupId' => $room->livechat->group_id,
                'agentId' => $agent_id
              ];
              $this->handleByGroup($payload);
            } else {
              // IF READY TO HANDLE ROOM DATA GROUP DOESN'T HAVE EXISTING GROUP ON LIST GROUPS FOR CURRENT AGENT, RETURN FORBIDDEN STATUS
              $group = $this->groupRepository->getByGroupId($room->livechat->group_id);
              return new RecordNotFoundException("Current agent has not joined on group $group->name");
            }
          } else {
            return new RecordNotFoundException("No unserved room for group of this agent");
          }
        }
      } else {
        // IF CURRENT AGENT DOEN'T HAVE GROUP
        $group = $this->groupRepository->getByGroupId($room->livechat->group_id);
        return new RecordNotFoundException("Your account not able to get customer from group $group->name");
      }
    } else {
      // IF LIVE CHAT FROM GENERAL
      // CHECK CURRENT AGENT HAVE ANY GROUPS
      if (count($groups) > 0) {
        // IF HAVE GROUP
        // IF GROUP MORE THAN ONE
        if (count($groups) > 1) {
          // IF HAVE GROUP MORE THAN ONE
          // GET SMALLEST SERVED ROOM BY GROUP OF CURRENT AGENT
          $servedByApp = $this->livechatRepository->getServedByAppId($app_id);
          \Log::info('served by app => '.json_encode($servedByApp));
          // IF HAVE SERVED GROUP OF CURRENT AGENT
          if (count($servedByApp) > 0) {
            $servedGroup = collect($servedByApp)->whereIn('group_id', $groupsId)->all();
            if (count($servedGroup) > 0) {
              $servedGroup = collect($servedGroup)->countBy('group_id');
              \Log::info('groups id => '.json_encode($servedGroup));
              $sortMinimumGroup = [];
              foreach ($servedGroup as $key => $value){
                array_push($sortMinimumGroup, ['id' => $key, 'count' => $value]);
              }
              usort($sortMinimumGroup, function ($item1, $item2) {
                return $item1['count'] <=> $item2['count'];
              });

              $countEveryGroup = collect($sortMinimumGroup)->pluck('count')->toArray();
              if((count(array_unique($countEveryGroup)) === 1)) {
                // JIKA DATA SERVED ROOM YANG DIHANDLE OLEH GROUP MEMILIKI JUMLAH YANG SETARA
                // SERVED ROOM BY CURRENT AGENT AND RANDOM GROUP OF CURRENT AGENT
                $indexRandomGroupId = array_rand($groupsId);
                $randomGroup = $groupsId[$indexRandomGroupId];
                $payload = [
                  'roomId' => $room->id,
                  'groupId' => $randomGroup,
                  'agentId' => $agent_id
                ];
                $this->handleGeneralWithGroup($payload);
              } else {
                // JIKA DATA SERVED ROOM YANG DIHANDLE OLEH GROUP MEMILIKI JUMLAH YANG TIDAK SETARA
                // SERVED READY TO HANDLE ROOM BY SMALLEST GROUP SERVED ROOM OF CURRENT AGENT
                $smalestServedGroup = $sortMinimumGroup[0];
                $payload = [
                  'roomId' => $room->id,
                  'groupId' => $smalestServedGroup['id'],
                  'agentId' => $agent_id
                ];
                $this->handleGeneralWithGroup($payload);
              }
            } else {
              // IF NONE SERVED GROUP OF CURRENT AGENT
              // SERVED ROOM BY CURRENT AGENT AND RANDOM GROUP OF CURRENT AGENT
              $indexRandomGroupId = array_rand($groupsId);
              $randomGroup = $groupsId[$indexRandomGroupId];
              $payload = [
                'roomId' => $room->id,
                'groupId' => $randomGroup,
                'agentId' => $agent_id
              ];
              $this->handleGeneralWithGroup($payload);
            }
          } else {
            // IF NONE SERVED GROUP OF CURRENT APP
            // SERVED ROOM BY CURRENT AGENT AND RANDOM GROUP OF CURRENT AGENT
            $indexRandomGroupId = array_rand($groupsId);
            $randomGroup = $groupsId[$indexRandomGroupId];
            $payload = [
              'roomId' => $room->id,
              'groupId' => $randomGroup,
              'agentId' => $agent_id
            ];
            $this->handleGeneralWithGroup($payload);
          }
        } else {
          // IF GROUP ONLY ONE
          // SERVED READY TO HANDLE ROOM BY CURRENT AGENT AND OWNED GROUP
          $payload = [
            'roomId' => $room->id,
            'groupId' => $groups[0]->id,
            'agentId' => $agent_id
          ];
          $this->handleGeneralWithGroup($payload);
        }
      } else {
        // IF CURRENT AGENT DOESN'T HAVE ANY GROUP
        // SERVED READY TO HANDLE ROOM TO CURRENT AGENT WITHOUT GROUP
        $this->livechatRepository->update([
            "room_id" => $room->id
          ],
          [
            "updated_by" => $agent_id,
            "handle_by" => $agent_id,
            "status" => "live",
            "start_at" => $this->getCurrentTimeInTimestamp()
          ]
        );
      }
    }

    $updated_room = $this->roomRepository->getById($room->id);

    $unserved_room = $this->roomRepository->getAllUnservedRoom($app_id);

    $this->dispatch(new LivechatHandle($updated_room,$unserved_room));

    return true;

  }

  public function assignAgent($room_id,$agent_id,$group_id,$assignee, $assignor, $assignor_role) {

    if(is_null($room_id) || is_null($agent_id) || is_null($assignee)) {
      return new BadRequestParameterException("room_id,agent_id or assignee id cannot be null");
    }

    $room = $this->roomRepository->getById($room_id);



    if(is_null($room)) {
      return new RecordNotFoundException("room with id id ".$room_id." is not found");
    }

     // check agent is online
     $selected_agent = DB::table("omnichannel.app_user_platform")
     ->where("app_id",$room->app_id)
     ->where("user_id", $agent_id)
     ->where("online", true)
     ->first();

    if(is_null($selected_agent)) {
      return new RecordNotExpectedException("Agen offline");
    }

    if(is_null($room->livechat)) {
      $this->livechatRepository->insert([
          "room_id" => $room_id,
          "handle_by" => $agent_id,
          "updated_by" => $agent_id,
          "request_at" => $this->getCurrentTimeInTimestamp(),
          "start_at" => $this->getCurrentTimeInTimestamp(),
          "group_id" => $group_id,
          "status" => "live"
        ]);
    }

    if(!is_null($room->livechat)) {
      $this->livechatRepository->update(
          [
            "room_id" => $room_id,
          ],
          [
          "handle_by" => $agent_id,
          "updated_by" => $agent_id,
          "status" => "live",
          "start_at" => $this->getCurrentTimeInTimestamp()
          ]
        );
    }

    $updated_room = $this->roomRepository->getById($room_id);

    $unserved_room = $this->roomRepository->getAllUnservedRoom($room->app_id);

    $this->dispatch(new LivechatAssigned($updated_room,$unserved_room, $assignee, $assignor, $assignor_role, true));

    return true;
  }

  public function assignGroup($group_id, $room_id, $assignor, $assignor_role) {
    if(is_null($group_id) || is_null($room_id)) {
      return new BadRequestParameterException("group_id or room id cannot be null");
    }

    $room = $this->roomRepository->getById($room_id);

    if(is_null($room)) {
      return new RecordNotFoundException("room with id ". $room_id . " is not found");
    }

    if(is_null($room->livechat)) {
      $this->livechatRepository->insert([
        "room_id" => $room_id,
        "status" => "request",
        "request_at" => $this->getCurrentTimeInTimestamp(),
        "group_id" => $group_id
      ]);
    }


    if(!is_null($room->livechat)) {
      $this->livechatRepository->update(
        [
          "room_id" => $room_id
        ],
        [
        "status" => "request",
        "handle_by" => null,
        "updated_by" => null,
        "start_at" => null,
        "group_id" => $group_id
      ]);
    }

    $updated_room = $this->roomRepository->getById($room_id);

    $unserved_room = $this->roomRepository->getAllUnservedRoom($room->app_id);
    $this->dispatch(new LivechatAssigned($updated_room,$unserved_room, null, $assignor, $assignor_role, false, $group_id, true));

    return true;
  }

  public function unservedRoom($app_id) {
    return $this->roomRepository->getAllUnservedRoom($app_id);
  }

  public function cancel($user_id) {
    if(is_null($user_id)) {
      return new BadRequestParameterException("field user_id cannot be null");
    }

    $room = $this->roomRepository->getByUserId($user_id);

    // validate if user has not requested for live agent
    if(is_null($room->livechat)) {
        return new RecordNotExpectedException("cannot cancel live agent");
    }

    // validate if current status not request for livechat;
    if($room->livechat->status !== "request") {
        return new RecordNotExpectedException("cannot cancel live  agent, cause current status not request for livechat to agent");
    }

    // update livechat
    $this->livechatRepository->delete(["room_id" => $room->id]);


    // get updated room
    $updated_room = $this->roomRepository->getByUserId($user_id);

    // unserved_room
    $unserved_room = $this->roomRepository->getAllUnservedRoom($room->app_id);

    // trigger an event
    $this->dispatch(new LivechatCancel($updated_room,$unserved_room));

    return true;
  }

  public function recordAssignment($room, $assignee,$assignor, $assignor_role) {
    if(is_null($room) || is_null($assignee)) {
      return new BadRequestParameterException("room or assignee cannot be null");
    }

    return $this->livechatRepository->createHistoryAssignment($room->created_by->name,$room->created_by->email, $assignor,$assignor_role,$assignee,$room->app_id);

  }

  public function recordAssignmentGroup($room, $assignor, $assignor_role, $group_id) {
    if(is_null($room)) {
      return new BadRequestParameterException("room cannot be null");
    }

    return $this->livechatRepository->createHistoryAssignmentGroup($room->created_by->name, $room->created_by->email, $assignor, $assignor_role, $room->app_id, $group_id);
  }

  public function reportingAssignment($app_id, $paginated, $start, $end, $search) {
    if(is_null($app_id)) {
      return new BadRequestParameterException("app id cannot be null");
    }

    return $this->livechatRepository->getHistoryAssignment($app_id,$paginated, function($query) use($start,$end, $search) {
      if(!is_null($start) || !is_null($end)) {
        $query->whereDate("ha.created_at",">=",$start);
        $query->whereDate("ha.created_at","<=",$end);
      }
      if(!is_null($search)) {
        $query->where('ha.assignor','ILIKE', '%'.trim($search).'%')->orWhere('ha.assignee','ILIKE', '%'.trim($search).'%')->orWhere('ha.customer_name','ILIKE', '%'.trim($search).'%')->orWhere('ha.customer_email','ILIKE', '%'.trim($search).'%');
      }
    });
  }

  public function handleByGroup($data = [])
  {
    $this->livechatRepository->update(
      [
        "room_id" => $data['roomId'],
        "group_id" => $data['groupId']
      ],
      [
        "updated_by" => $data['agentId'],
        "handle_by" => $data['agentId'],
        "status" => "live",
        "start_at" => $this->getCurrentTimeInTimestamp()
      ]
    );
  }

  public function handleGeneralWithGroup($data = [])
  {
    $this->livechatRepository->update(
      [
        "room_id" => $data['roomId']
      ],
      [
        "group_id" => $data['groupId'],
        "updated_by" => $data['agentId'],
        "handle_by" => $data['agentId'],
        "status" => "live",
        "start_at" => $this->getCurrentTimeInTimestamp()
      ]
    );
  }

}
