<?php

namespace App\Modules\Webchat\Controllers;
// ini_set('memory_limit', '-1');
use Hashids\Hashids;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Faker;

use App\Modules\Log\Models\WebhookLog;
use App\Modules\Integration\Models\Integration;
use App\Modules\App\Models\App;
use App\Modules\User\Models\User;
use App\Modules\Room\Models\Room;
use App\Modules\Message\Models\Message;
use App\Modules\Webchat\Models\WebchatStyle;
use App\Modules\Livechat\Models\Livechat;
use App\Modules\Channel\Models\Channel;

use App\Modules\Mobile\Helpers\LennaMessageType;
use App\Modules\Chat\Helpers\ChatHelper;
use App\Modules\Log\Helpers\WebhookHelper;

use App\Modules\Bot\Services\BotService;
use App\Modules\Mobile\Services\BackendService;
use App\Modules\BlockUser\Services\BlockUserService;
use App\Modules\CustomIntegration\Services\CustomIntegrationService;
use App\Modules\QontakTicketing\Services\QontakTicketingService;

use App\Events\NewMessage;
use App\Events\RequestLive;
use App\Events\NewUser;

use DB;

use App\Http\Controllers\ApiController;

class WebchatWebhookController extends ApiController
{
    protected $app;
    protected $integration;
    protected $channel;
    protected $blockUserService;
    protected  $customIntegrationService;
    protected $qontakTicketingService;

    public function __construct(
      BlockUserService $blockUserService,
      CustomIntegrationService  $customIntegrationService,
      QontakTicketingService $qontakTicketingService
    )
    {
        $this->blockUserService = $blockUserService;
        $this->channel = Channel::where('name', 'webchat')->first();
        $this->hashids = new Hashids('', 6);
        $this->customIntegrationService = $customIntegrationService;
        $this->qontakTicketingService = $qontakTicketingService;
        $this->defaultWebchatStyle = (object)[
          'avatar'                  => "/images/pictures/webchat/lennalogoblue.png",
          'background_value'        => "#F4F4F4",
          'background_type'         => "color",
          'branding'                => "yes",
          'bubble_self'             => "#003275",
          'bubble_other'            => "#e5e5e5",
          'text_self'               => "#FFFFFF",
          'text_other'              => "#343a40",
          'header_logo'             => "/images/pictures/webchat/lennalogo.png",
          'header_text'             => "Hi, Welcome to Lenna.ai",
          'header_background_type'  => "image",
          'header_background_value' => "/images/pictures/webchat/header.jpeg",
          'launcher'                => "#186AB3",
          'app'                     => (object)[
              'name'                => "Lenna"
          ],
          "is_use_dynamic_fields" => false,
          "fields" => [
            (object)[
              "name" => "name",
              "type" => "password",
              "is_required" => true,
              "label" => "name",
              "validation" => "required",
              "value" => ""
            ],
            (object)[
              "type" => "type",
              "name" => "name",
              "is_required" => false,
              "label" => "name",
              "validation" => "",
              "value" => ""
            ]
          ]
        ];
    }
    public function webhookTest(Request $request)
    {
        $webhookLog = WebhookLog::create([
            "source" => "webchat",
            "app_id" => "333",
            "events" => "webchat webhook test",
            "payload" => json_encode($request->all()),
            "response" => json_encode([
                'success' => true,
                'data' => "testing webhook"
            ])
        ]);
        return $this->successResponse(['events' => 'testing webhook']);

    }
    public function webhook(Request $request, $appId)
    {
        if (!$this->integration = WebhookHelper::getIntegrationData('webchat', $appId))
            return $this->errorResponse(null, 'integration not active');
        // check if room has request live agent
        $hasRequestLiveAgent = false;
        $this->app = App::find($appId);
        $botService = new BotService(['appId' => $appId]);
        $backendService = new BackendService(['appId' => $appId]);
        switch ($request->events) {
            case 'message':
                $response = $this->incomingMessage($request, $this->app->id);
                // check if user blocked
                if(is_bool($response) && $response == true) {
                  return $this->successResponse(["data" => $response],"user blocked");
                }
                // if status resolved
                if (WebhookHelper::isResolved(['roomId' => $response['room']->id])) {
                  // change status to bot by deleting livechat key
                  $this->deleteLiveChat($response['room']->id);
                }
                broadcast(new NewMessage($response['message'], $response['room'], $this->app));
                // if not live
                if (!WebhookHelper::isLive(['roomId' => $response['room']->id])) {
                  // if status not request
                  if (!WebhookHelper::isRequest(['roomId' => $response['room']->id])) {
                    // bot active
                    $backendResponse = $backendService->getUserToken([
                        'email' => $response['user']->email,
                        "password" => BackendService::encryptText(env("ENCRYPT_KEY"), "$appId:" . $response['user']->email),
                        "fcm_token" => null,
                        "client" => "webchat",
                        "userId" => $response['user']->id
                    ]);

                    $botResponse = $botService->replyMessage([
                        'userId' => $response['user']->id,
                        'content' => $response['message']->content[0],
                        'room' => $response['room'],
                        'lat' => $request->lat ?? 0,
                        'lon' => $request->lon ?? 0,
                        'channel' => 'webchat',
                        "headers" => ["Authorization" => "$backendResponse->token_type $backendResponse->access_token"],
                        'lang' => $response['user']->lang
                    ]);
                  }else{
                      $hasRequestLiveAgent = true;
                  }
                }

                // custom integration
                // qontak crm
                if($this->customIntegrationService->isIntegrated($appId,"qontak-crm")) {
                  $this->qontakTicketingService->createTicket([
                      "email" => $response["user"]->email,
                      "room_id" => $response["room"]->id,
                      "name" => $response["user"]->name,
                      "phone" => $response["user"]->phone,
                      "nickname" => $response["user"]->nickname,
                      "contact_id" =>  $response["user"]->contact_id,
                      "type" => "create"
                    ],
                    $response["room"]->id
                  );
                }
                break;
        }
        // if bot not integrated, change status to request
        $botIntegration = $botService->getIntegration();
        // dd($response);
        if (!$botIntegration) {
          if (!WebhookHelper::isLive(['roomId' => $response['room']->id])) {
            if (!WebhookHelper::isRequest(['roomId' => $response['room']->id])) {
              WebhookHelper::requestLivechat($response['user']->id, $appId);
            }
          }
        }
        // $backendService->updateLocationAndIp([
        //   'ip' => $request->ipinfo->ip,
        //   'city' => $request->ipinfo->city,
        //   'location' => $request->ipinfo->loc,
        //   'user_id' => $response['user']->id,
        //   'last_update' => $response['user']->updated_at
        // ]);
        $responseLog = (object)[
            'output' => $botResponse['nlp']->response ?? null,
            'quickbutton' => $botResponse['nlp']->quickbutton ?? [],
            'type' => $botResponse['nlp']->type ?? null
        ];
        $webhookLog = WebhookLog::create([
            "source" => "webchat",
            "app_id" => $this->app->id,
            "events" => $request->events,
            "payload" => json_encode($request->all()),
            "response" => json_encode([
                'success' => true,
                'data' => $responseLog
            ]),
            "user_id" => $response['user']->id,
            "bot_id" => $botIntegration ? $botIntegration->integration_data['botId'] : NULL
        ]);


        return $this->successResponse([
            'user' => (object)[
                'message' => $response['message'],
                'temporaryId' => $request->message['temporary_id'],
                "isRequestLiveAgent" => $hasRequestLiveAgent
            ],
            'bot' => (object)[
                'message' => $botResponse['message']->original['data']['message'] ?? null,
                'nlp' => $botResponse['nlp']->response ?? null,
                'quickbutton' => $botResponse['nlp']->quickbutton ?? [],
                'entities' => $botResponse['nlp']->entities ?? null
            ]
        ]);
    }

    public function registerUser(Request $request, $appId)
    {
        $pictureUrl = env("APP_URL") . "/images/pictures/no_avatar.jpg";
        // $email = BackendService::encryptText(env("ENCRYPT_PROFILE_KEY"), $request->email);
        // $phone = BackendService::encryptText(env("ENCRYPT_PROFILE_KEY"), $request->phone);
        $email = strtolower($request->email);
        $phone = $request->phone;

        $fields = [
            'app_id' => $appId,
            'channel_user_id' => null,
            'name' => $request->name,
            'nickname' => $request->name,
            'password' => bcrypt("$appId:$email"),
            'picture' => $pictureUrl,
            'email' => $email,
            'phone' => $phone,
            'fields' => json_encode($request->fields)
        ];

        $user = $this->channel->users()->firstOrCreate(
            ['email' => $email, 'app_id' => $appId],
            $fields
        );
        return $this->successResponse(['user' => $user], 'user registered');
    }

    public function incomingMessage($request, $appId)
    {
        $user = User::where([['id', "=", $request->senderId], ['app_id', "=", $appId]])->first();

        // check if user blocked
        if($this->blockUserService->isBlocked($user->email, $appId)) {
          return true;
        }
        // $user = User::find($request->senderId);
        // register user if not registered to this app
        if ($user == null || $user->app_id !== $appId || !$user) {
            $data = $this->registerAndChangeUser($appId);
            $user = User::where('id', $data->id)->first();
        }
        $chatRoom = ChatHelper::createRoomAndParticipate(['channel' => $this->channel, 'user' => $user, 'appId' => $appId]);
        $channelData = [];
        $content[] = $this->setMessageType($request->message['content'][0]);
        $message = ChatHelper::createMessage(['channelData' => $channelData, 'content' => $content, 'user' => $user, 'room' => $chatRoom]);
        return [
            'message' => $message,
            'user' => $user,
            'room' => $chatRoom
        ];
    }

    public function setMessageType($message)
    {
        $lennaMessageType = new LennaMessageType;

        switch ($message['type']) {
            case 'text':
                $content = $lennaMessageType->text(['text' => $message['text']]);
                break;
            case 'image':
                $content = $lennaMessageType->image($message);
                break;
            case 'file':
                $content = $lennaMessageType->file(['fileUrl' => $message['fileUrl'], 'fileName' => $message['fileName']]);
                break;
            case 'video':
                $content = $lennaMessageType->video(['originalContentUrl' => $message['originalContentUrl']]);
                break;
            case 'audio':
                $content = $lennaMessageType->audio(['originalContentUrl' => $message['originalContentUrl']]);
                break;
            case 'callback':
                $content = $lennaMessageType->text(['text' => $message['text']]);
                break;
        }
        return $content;
    }

    public function getStyle(Request $request, $appId)
    {
      $data = WebchatStyle::with(['app' => function($q) {
        $q->select('id','name');
      }])->where('app_id', $appId)->first();
      if(!$data){
        return response()->json([
          "error"       => true,
          "success"     => false,
          "message"     => "Style not available, using default style",
          "data"        => $this->defaultWebchatStyle
        ]);
      }
      $data->app_id       = $this->hashids->encode($data->app_id);
      $data->created_by   = $this->hashids->encode($data->created_by);
      $data->updated_by   = $this->hashids->encode($data->updated_by);
      return $this->successResponse($data);
    }

    public function getMessages(Request $request, $appId)
    {
      $firstMessageId       = $request->get('firstMessageId');
      $per_page             = $request->get('per_page');
      $room = Room::where('created_by', $request->id)->first();
      if (!$room) {
          return response()->json([
              'success'   => false,
              'data'      => null,
              'message'   => "The room hasn't been created or the user hasn't registered yet"
          ]);
      }
      $start = Carbon::now()->startOfDay()->toDateTimeString();
      $end = Carbon::now()->endOfDay()->toDateTimeString();
      $messages = Message::where('room_id', $room->id)->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end)->orderBy('id', 'desc');
      // $messages = Message::where('room_id', $room->id)->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end);
      if ($request->filled('firstMessageId')) {
        $messages = $messages->where('id', '<', $firstMessageId);
      }
      if (!$request->filled('per_page')) {
        $per_page = 10;
      }
      // $data = $this->paginate($data, $per_page);
      $data = $messages->take($per_page)->get();
      $data = collect($data);
      // $order = collect($data['data'])->sortBy('id');
      $order = $data->sortBy('id');
      // $data['data'] = $order->values()->all();
      $data = $order->values()->all();
      // $data = collect($data);
      // $data = $data->toArray();
      // if ($data['total'] == 0) {
      //   return response()->json([
      //     'success'   => false,
      //     'data'      => null,
      //     'message'   => "No message"
      //   ]);
      // }
      return response()->json([
        'success' => true,
        'data'    => $data,
        'message' => 'Success get messages'
      ]);
    }

    public function paginate($items, $perPage, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage)->values(), $items->count(), $perPage, $page, [
          'path' => LengthAwarePaginator::resolveCurrentPath()
        ]);
    }

    public function requestLivechat($senderId, $appId)
    {
        $room = User::find($senderId)->rooms->first();
        $liveChat = $room->liveChats()->firstOrCreate(
            ['room_id' => $room->id, 'status' => 'request'],
            [
                "request_at" => Carbon::now()->toDateTimeString(),
                "status" => 'request',
            ]
        );
        broadcast(new RequestLive($room->id, $appId, $senderId, $liveChat->id));
    }

    public function deleteLiveChat($roomId)
    {
        Livechat::where('room_id', $roomId)->delete();
    }

    public function registerAndChangeUser($appId)
    {
        $faker      = Faker\Factory::create('id_ID');
        $name       = $faker->firstName.' '.$faker->lastName;
        $nickname   = $faker->userName;
        // $fakerEmail = $faker->email;
        // $fakerPhone = '08'.$faker->randomNumber(5, true).$faker->randomNumber(5, true);
        // $phone      = BackendService::encryptText(env("ENCRYPT_PROFILE_KEY"), $fakerPhone);
        // $email      = BackendService::encryptText(env("ENCRYPT_PROFILE_KEY"), $fakerEmail);
        // $password   = bcrypt("$appId:$fakerEmail");
        $email      = $faker->email;
        $phone      = '08'.$faker->randomNumber(5, true).$faker->randomNumber(5, true);
        $password   = bcrypt("$appId:$email");
        $pictureUrl = env("APP_URL") . "/images/pictures/no_avatar.jpg";

        $fields = [
            'app_id' => $appId,
            'channel_user_id' => null,
            'name' => $name,
            'nickname' => $nickname,
            'password' => $password,
            'picture' => $pictureUrl,
            'email' => $email,
            'phone' => $phone
        ];

        $user = $this->channel->users()->firstOrCreate(
            ['email' => $email, 'app_id' => $appId],
            $fields
        );
        // $id = $user->id;
        // $data = User::find($id);
        broadcast(new NewUser($user, $this->app));
        return $user;
    }

    public function checkRegisteredUser(Request $request, $appId)
    {
        $user = User::where('app_id', $appId)->where('channel_id', $this->channel->id)->where('id', $request->userId)->first();
        // return true if user registered to this app
        if ($user) {
          return response()->json(true);
        } else {
          return response()->json(false);
        }
    }

    public function checkLivechat(Request $request, $appId)
    {
      $call_db = DB::table("omnichannel.rooms as or")
               ->where("or.created_by",$request->user_id)
               ->join("omnichannel.livechats as ol",function($query) {
                 $query->on("ol.room_id", "=", "or.id");
               })
               ->where("ol.status","request")
               ->exists();
      return response()->json($call_db);
    }

    public function checkQueue(Request $request, $appId)
    {
        $queue = WebhookHelper::getQueue($appId);
        // return true if user registered to this app
        if (count($queue) > 0) {
          return $this->successResponse($queue, "Success get queue");
        } else {
          return response()->json([
            'success' => false,
            'message' => "No queue livechat",
            'data' => null
          ]);
        }
    }

    public function checkLocationConfig(Request $request, $appId)
    {
      $appConfig = App::select('webchat_location')->where('id', $appId)->first();
      return $this->successResponse($appConfig['webchat_location']);
    }
}
