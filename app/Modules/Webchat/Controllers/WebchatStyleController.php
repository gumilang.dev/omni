<?php

namespace App\Modules\Webchat\Controllers;

use Carbon\Carbon;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\ApiController;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use App\Modules\App\Models\App;
use App\Modules\Webchat\Models\WebchatStyle;
use Illuminate\Support\Str;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class WebchatStyleController extends ApiController
{
    public function __construct()
    {
        $this->hashids = new Hashids('', 6);
    }

    public function index($appId)
    {
      $custom_webchat = WebchatStyle::where('app_id', $appId)->get();
      if(count($custom_webchat) == 0){
        return response()->json([
          "error"       => true,
          "success"     => false,
          "message"     => "Style not available"
        ]);
      }
      $data = $custom_webchat->each(function($item, $key){
        $item['app_id']                   = $this->hashids->encode($item['app_id']);
        $item['created_by']               = $this->hashids->encode($item['created_by']);
        $item['updated_by']               = $this->hashids->encode($item['updated_by']);
      })[0];
      return response()->json([
        "error"   => false,
        "success" => true,
        "message" => "Success get style",
        "data"    => $data
      ]);
    }

    public function uploadFile(Request $request)
    {
      if ($request->hasFile('file'))
      {
        $validator = Validator::make($request->all(), [
          'credit_image' => 'file|max:512|mimes:jpeg,jpg,png,gif,svg|dimensions:max_width=87,max_height=21',
          'header_logo' => 'file|max:512|mimes:jpeg,jpg,png,gif,svg|dimensions:max_width=150,max_height=150',
          'header_background_value' => 'file|max:512|mimes:jpeg,jpg,png,gif,svg|dimensions:max_width=1300,max_height=300',
          'avatar' => 'file|max:512|mimes:jpeg,jpg,png,gif,svg|dimensions:max_width=150,max_height=150',
          'launcher_close' => 'file|max:512|mimes:jpeg,jpg,png,gif,svg',
          'launcher_open' => 'file|max:512|mimes:jpeg,jpg,png,gif,svg'
        ]);
        if ($validator->fails()) {
          return $response = [
            'success' => false,
            'message' => $validator->errors()->first()
          ];
        }
        $file = $request->file('file');
        $extension = $request->file('file')->getClientOriginalExtension();
        $timestamp = Carbon::now('Asia/Jakarta')->timestamp;
        $allowedExension = ['jpeg','jpg','gif','png','svg','PNG','JPG','JPEG'];
        if (in_array($extension, $allowedExension))
        {
          $name = $timestamp.'.'.$extension;
          Storage::disk('public_webchat')->put($name,  File::get($file));
          $url = '/upload/webchat/'.$name;
          $response = [
            'success' => true,
            'data' => $url
          ];
        } else {
          $response = [
            'success' => false,
            'message' => 'Extension not allowed'
          ];
        }
      } else {
        $response = [
          'success' => false,
          'message' => 'File cannot be null'
        ];
      }

      if ($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg' || $extension == 'JPEG' || $extension == 'PNG' || $extension == 'JPG') {
        $optimizerChain = OptimizerChainFactory::create();
        $optimizerChain->optimize(public_path('/upload/webchat/').$name, public_path('/upload/webchat/').$name);
      }
      return $response;
    }

    public function deleteFile(Request $request)
    {
        $name = str_replace('/upload/webchat/', '', $request->fileToDelete);
        Storage::disk('public_webchat')->delete($name);
        return $this->successResponse(null, 'file deleted');
    }

    public function store(Request $request, $appId)
    {
      $app = App::find($appId);
      if (!$app) {
        return $this->errorResponse(null, 'app not found');
      }

      $fields = json_decode(json_encode($request->fields));

      foreach($fields as $each_field) {
        $each_field->label = $each_field->name;
        $each_field->name =  str_replace(' ','_',strtolower($each_field->name));
        $each_field->validation = $each_field->is_required  ? "required" : "";
        $each_field->value = "";
      }

      $webchatStyle                           = $app->webchatStyle;
      $webchatStyle->updated_by               = $request->decodedUserId;
      $webchatStyle->created_by               = $request->decodedUserId;
      $webchatStyle->avatar                   = $request->avatar;
      $webchatStyle->background_value         = $request->background_value;
      $webchatStyle->background_type          = $request->background_type;
      $webchatStyle->branding                 = $request->branding;
      $webchatStyle->bubble_self              = $request->bubble_self;
      $webchatStyle->bubble_other             = $request->bubble_other;
      $webchatStyle->text_self                = $request->text_self;
      $webchatStyle->text_other               = $request->text_other;
      $webchatStyle->header_text              = $request->header_text;
      $webchatStyle->header_logo              = $request->header_logo;
      $webchatStyle->header_background_value  = $request->header_background_value;
      $webchatStyle->header_background_type   = $request->header_background_type;
      $webchatStyle->launcher_open            = $request->launcher_open;
      $webchatStyle->launcher_close           = $request->launcher_close;
      $webchatStyle->launcher_open_type       = $request->launcher_open_type;
      $webchatStyle->launcher_close_type      = $request->launcher_close_type;
      $webchatStyle->credit_image             = $request->credit_image;
      $webchatStyle->is_use_dynamic_fields    = $request->is_use_dynamic_fields;
      $webchatStyle->fields = $fields;
      $webchatStyle->save();
      $webchatStyle->fresh();
      return $this->successResponse($webchatStyle, 'Success change webchat style');
    }

    // public function defaultStyle($userId, $appId) {
    //   $style                          = new WebchatStyle;
    //   $style->app_id                  = $appId;
    //   $style->avatar                  = "/images/pictures/webchat/lennalogoblue.png";
    //   $style->background_value        = "#F4F4F4";
    //   $style->background_type         = "color";
    //   $style->branding                = "yes";
    //   $style->bubble_self             = "#003275";
    //   $style->bubble_other            = "#E5E5E5";
    //   $style->text_self               = "#FFFFFF";
    //   $style->text_other              = "#343A40";
    //   $style->header_logo             = "/images/pictures/webchat/lennalogo.png";
    //   $style->header_text             = "Hi, Welcome to Lenna.ai";
    //   $style->header_background_type  = "image";
    //   $style->header_background_value = "/images/pictures/webchat/header.jpeg";
    //   $style->launcher                = "#186AB3";
    //   $style->created_by              = $userId;
    //   $style->save();
    // }

    public function restoreStyle(Request $request, $appId)
    {
      $app = App::find($appId);
      if (!$app) {
        return $this->errorResponse(null, 'app not found');
      }
      $webchatStyle                           = $app->webchatStyle;
      $webchatStyle->avatar                   = '/images/pictures/webchat/lennalogoblue.png';
      $webchatStyle->background_value         = '#ffffff';
      $webchatStyle->background_type          = 'color';
      $webchatStyle->branding                 = 'yes';
      $webchatStyle->bubble_self              = '#003275';
      $webchatStyle->bubble_other             = '#e5e5e5';
      $webchatStyle->text_self                = '#ffffff';
      $webchatStyle->text_other               = '#343a40';
      $webchatStyle->header_logo              = '/images/pictures/webchat/lennalogo.png';
      $webchatStyle->header_text              = 'Hi, Welcome to Lenna.ai';
      $webchatStyle->header_background_value  = '/images/pictures/webchat/header.jpeg';
      $webchatStyle->header_background_type   = 'image';
      $webchatStyle->launcher_open            = '#186AB3';
      $webchatStyle->launcher_open_type       = 'color';
      $webchatStyle->launcher_close           = '#186AB3';
      $webchatStyle->launcher_close_type      = 'color';
      $webchatStyle->credit_image             = '/images/pictures/webchat/lenna_credit.png';
      $webchatStyle->is_use_dynamic_fields    = false;
      $webchatStyle->updated_by               = $request->decodedUserId;
      $webchatStyle->save();
      $webchatStyle->fresh();
      return $this->successResponse($webchatStyle, 'Webchat style restored to default');
    }

    public function updateCreditImage(Request $request,$appId)
    {
      if ($request->hasfile('credit_image')) {
        $response = $this->uploadFile($request);
        if (!$response['success']) {
          return $this->errorResponse(null, $response['message']);
        } else {
          return $this->successResponse($response['data'], 'file uploaded');
        }
      }
    }

    public function updateHeaderLogo(Request $request, $appId)
    {
      if ($request->hasfile('header_logo')) {
        $response = $this->uploadFile($request);
        if (!$response['success']) {
          return $this->errorResponse(null, $response['message']);
        } else {
          return $this->successResponse($response['data'], 'file uploaded');
        }
      }
    }

    public function updateHeaderBackground(Request $request, $appId)
    {
      if ($request->hasfile('header_background_value')) {
        $response = $this->uploadFile($request);
        if (!$response['success']) {
          return $this->errorResponse(null, $response['message']);
        } else {
          return $this->successResponse($response['data'], 'file uploaded');
        }
      }
    }

    public function updateAvatar(Request $request, $appId)
    {
      if ($request->hasfile('avatar')) {
        $response = $this->uploadFile($request);
        if (!$response['success']) {
          return $this->errorResponse(null, $response['message']);
        } else {
          return $this->successResponse($response['data'], 'file uploaded');
        }
      }
    }

    public function updateLauncherOpen(Request $request, $appId)
    {
      if ($request->hasfile('launcher_open')) {
        $response = $this->uploadFile($request);
        if (!$response['success']) {
          return $this->errorResponse(null, $response['message']);
        } else {
          return $this->successResponse($response['data'], 'file uploaded');
        }
      }
    }

    public function updateLauncherClose(Request $request, $appId)
    {
      if ($request->hasfile('launcher_close')) {
        $response = $this->uploadFile($request);
        if (!$response['success']) {
          return $this->errorResponse(null, $response['message']);
        } else {
          return $this->successResponse($response['data'], 'file uploaded');
        }
      }
    }
}
