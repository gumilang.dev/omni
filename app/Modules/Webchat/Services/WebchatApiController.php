<?php

namespace App\Modules\Webchat\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\ExternalApi\Models\ExternalApi;
use GuzzleHttp\Client;
use App\Helpers\MessageType\WebchatMessageType;

class WebchatApiController extends Controller
{
    protected $client;
    protected $webchatApi;

    public function __construct($data)
    {
        $this->data = $data;

        $this->webchatApi = ExternalApi::where(['category' => 'channel', 'provider' => 'webchat'])->first();
        $guzzleProp = [
            'base_uri' => $this->webchatApi->base_url
        ];
        $this->client = new Client($guzzleProp);
    }

    public function replyMessage($lastMessage, $newMessage)
    {
        $apiProp = $this->webchatApi->endpoints()->where('name', 'reply-message')->first();
        foreach ($$newMessage->content as $key => $content) {
            $message = $this->convertToWebchatMessageType($content);
            $requestData = [
                'replyToken' => $lastMessage->channel_data['reply_token'],
                'messages' => [
                    $message
                ]
            ];
            $response = $this->client->request(
                $apiProp->method,
                $apiProp->endpoint,
                [
                    'headers' => ['Authorization' => 'Bearer ' . $this->data['integration']->integration_data['accessToken']],
                    "json" => $requestData
                ]
            );
            $response = json_decode($response->getBody()->getContents());
        }
        return $response;
    }

    public function pushMessage($newMessage)
    {
        $apiProp = $this->webchatApi->endpoints()->where('name', 'push-message')->first();

        foreach ($newMessage->content as $key => $content) {
            $message = $this->convertToWebchatMessageType($content);
            $requestData = [
                'to' => $this->data['chatRoom']->created_by->channel_user_id,
                'messages' => [
                    $message
                ]
            ];
            $response = $this->client->request(
                $apiProp->method,
                $apiProp->endpoint,
                [
                    'headers' => ['Authorization' => 'Bearer ' . $this->data['integration']->integration_data['accessToken']],
                    "json" => $requestData
                ]
            );
            $response = json_decode($response->getBody()->getContents());
        }
        return $response;
    }

    public function getUserProfile($channelUserId)
    {
        $apiProp = $this->webchatApi->endpoints()->where('name', 'get-profile')->first();
        $response = $this->client->request(
            $apiProp->method,
            str_replace('{user-id}', $channelUserId, $apiProp->endpoint),
      // $apiProp->endpoint.$webchatData['source']['userId'],
            ['headers' => ['Authorization' => 'Bearer ' . $this->data['integration']->integration_data['accessToken']]]
        );
        $response = json_decode($response->getBody()->getContents());
        return $response;
    }

    public function getMessageContent($messageId)
    {
        $publicPath = env("PUBLIC_PATH");
        $apiProp = $this->webchatApi->endpoints()->where('name', 'get-content')->first();
    // get file name and format
        $response = $this->client->request(
            $apiProp->method,
            str_replace('{message-id}', $messageId, $apiProp->endpoint),
            [
                'headers' => ['Authorization' => 'Bearer ' . $this->data['integration']->integration_data['accessToken']],
            ]
        );
    // set filename with format
        $response = explode("/", $response->getHeader("content-type")[0]);
        $category = $response[0];
        $format = str_replace("x-", "", $response[1]);

        $filename = $category . "-$messageId." . $format;
    // set path to upload
        $path = fopen($publicPath . '/upload/webchat/' . $filename, 'w');
    // save file to server
        $response = $this->client->request(
            $apiProp->method,
            str_replace('{message-id}', $messageId, $apiProp->endpoint),
            [
                'headers' => ['Authorization' => 'Bearer ' . $this->data['integration']->integration_data['accessToken']],
                'sink' => $path
            ]
        );
        return $filename;
    }

    public function convertToWebchatMessageType($message)
    {
        $webchatMessageType = new WebchatMessageType;
        switch ($message->type) {
            case 'text':
                $message = $webchatMessageType->text(['text' => $message->text]);
                break;
            case 'image':
                $message = $webchatMessageType->image(['previewImageUrl' => $message->previewImageUrl]);
                break;
            case 'audio':
                $message = $webchatMessageType->audio(['originalContentUrl' => $message->originalContentUrl]);
                break;
            case 'carousel':
                $message = $webchatMessageType->carousel([
                    'columns' => $message->columns,
                    'imageAspectRatio' => $message->imageAspectRatio,
                    'imageSize' => $message->imageSize,
                ]);
                break;
            case 'confirm':
                $message = $webchatMessageType->confirm([
                    'actions' => $message->actions,
                    'text' => $message->text,
                ]);
                break;
      // case 'buttons':
      //   $message =$webchatMessageType->carousel([
      //     'columns' => $message->columns,
      //     'imageAspectRatio'=>$message->imageAspectRatio,
      //     'imageSize'=>$message->imageSize,
      //   ]);
      //   break;
            default:
                $message = $webchatMessageType->text(['text' => "Sent $message->type message"]);
                break;
        }
        return (object)$message;
    }
}
