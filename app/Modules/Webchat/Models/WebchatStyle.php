<?php

namespace App\Modules\Webchat\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class WebchatStyle extends Model
{
    use SoftDeletes;
    protected $table = "webchat_styles";
    protected $fillable = ['app_id'];
    protected $dates = ['deleted_at'];
    // protected $fillable = ['app_id','avatar','background','branding','bubble','text','header','launcher','created_by','updated_by'];
    // protected $guarded = [];
    protected $casts = [
      // 'background'  => 'array',
      'fields'      => 'array'
    ];

    public function app()
    {
        return $this->belongsTo("App\Modules\App\Models\App");
    }
}
