<?php
namespace App\Modules\Dashboard\Controllers;
ini_set('memory_limit', '-1');

use Illuminate\Http\Request;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use DB;
use DateTime;

//BOT
use App\Modules\Bot\Models\Story;
use App\Modules\Bot\Models\BotRequest;
use App\Modules\Bot\Models\Interaction;
//BOT

use App\Http\Controllers\ApiController;

use App\Modules\Transaction\Models\Transaction;
use App\Modules\Message\Models\Message;
use App\Modules\User\Models\User;
use App\Modules\Room\Models\Room;
use App\Modules\Livechat\Models\Livechat;
use App\Modules\Livechat\Models\Participable;
use App\Modules\User\Models\UserRole;
use App\Modules\User\Models\UserApp;
use App\Modules\User\Models\UserPlatform;
use App\Modules\Log\Models\ApiLog;
use App\Modules\Integration\Models\Integration;
use App\Modules\Tag\Model\Tag;
use App\Modules\RoomTag\Model\RoomTag;

use App\Modules\Role\Helpers\RoleHelper;

class DashboardController extends ApiController
{
  public function __construct(){
    $this->dbOmni = "omnichannel";
    $this->dbBot = "bot";
    $this->dbApp = "app";
    $this->dbAuth = "auth";
  }

  public function getConversationAndUser(Request $request, $appId) {
    $start = $request->get('start');
    $end = $request->get('end');

    ###################################### CONVERSATION DATA ####################################
    // new chat user
    $newChat = \Illuminate\Support\Facades\DB::table($this->dbOmni . '.livechats as l')
      ->join($this->dbOmni . '.messages as m', 'm.room_id', '=', 'l.room_id')
      ->join($this->dbApp . '.users as u', 'u.id', '=', 'm.messageable_id')
      ->select('m.messageable_id')
      ->where('m.messageable_type', '=', 'user')
      ->whereDate('m.created_at', '>=', $start)
      ->whereDate('m.created_at', '<=', $end)
      ->groupBy('m.messageable_id')
      ->get();

    foreach ($newChat as $n) {
      $userId[] = $n->messageable_id;
    }

    $where = 'id = ' . $userId[0];
    for ($i = 1; $i < count($userId); $i++) {
      $where .= ' OR id = ' . $userId[$i];
    }

    // get created time of each user
    $createdTime = \Illuminate\Support\Facades\DB::table($this->dbApp . '.users as u')
      ->select('created_at as Time')
      ->whereRaw($where)
      ->get();

    foreach($createdTime as $c) {
      $time[] = [
        'time' => date('Y-m-d', strtotime($c->Time))
      ];
    }

    // filter created time user depandon date selection
    $collectTime = collect($time);
    $filter = $collectTime->whereBetween('time', [$start, $end]);
    $filtered = $filter->all();
    $newChatUser = count($filtered);
    // end new user

    ##################################### LIVE CONVERSATION DATA ###############################
    // incoming request
    $incoming = \Illuminate\Support\Facades\DB::table($this->dbOmni . '.livechats as l')
      ->join($this->dbOmni . '.rooms as r', 'r.id', '=', 'l.room_id')
      ->where('r.app_id', '=', $appId)
      ->where('l.status', 'request')
      ->whereDate('l.request_at', '>=', $start)
      ->whereDate('l.request_at', '<=', $end)
      ->count();

    // served by agent
    $served = \Illuminate\Support\Facades\DB::table($this->dbOmni . '.livechats as l')
      ->join($this->dbOmni . '.rooms as r', 'r.id', '=', 'l.room_id')
      ->where('r.app_id', '=', $appId)
      ->where('l.status', 'live')
      ->whereDate('l.request_at', '>=', $start)
      ->whereDate('l.request_at', '<=', $end)
      ->count();

    // resolved
    $resolved = \Illuminate\Support\Facades\DB::table($this->dbOmni . '.livechats as l')
      ->join($this->dbOmni . '.rooms as r', 'r.id', '=', 'l.room_id')
      ->where('r.app_id', '=', $appId)
      ->where('l.status', 'resolved')
      ->whereDate('l.request_at', '>=', $start)
      ->whereDate('l.request_at', '<=', $end)
      ->count();

    // avg response
    $raw_avg = \Illuminate\Support\Facades\DB::table($this->dbOmni . '.livechats as l')
      ->join($this->dbOmni . '.rooms as r', 'r.id', '=', 'l.room_id')
      ->where('r.app_id', '=', $appId)
      ->select('l.start_at', 'l.end_at')
      ->whereDate('l.start_at', '2021-03-10')
      ->where('l.status', 'resolved')
      ->get();

    if (count($raw_avg) == 0) {
      $avg = 0;
    } else {
      foreach($raw_avg as $r) {
        $start[] = Carbon::parse($r->start_at);
        $end[] =  Carbon::parse($r->end_at);
        $diff[] = Carbon::parse($r->start_at)->diffInMinutes(Carbon::parse($r->end_at));
      }

      if ($diff === []) {
        $avg = 0;
      }

      $avg = floor(array_sum($diff) / count($diff));
    }

//    $return = [
//      [
//        'liveConversationData' => [
//          'title' => 'incoming request',
//          'mount' => $incoming,
//          'percentage' => '5%',
//          'status' => 'down',
//          'dod' => '10.600'
//        ],
//        [
//          'title' => 'served by agent',
//          'mount' => $served,
//          'percentage' => '5%',
//          'status' => 'down',
//          'dod' => '10.600'
//        ],
//        [
//          'title' => 'resolved conversation',
//          'mount' => $resolved,
//          'percentage' => '5%',
//          'status' => 'down',
//          'dod' => '10.600'
//        ],
//        [
//          'title' => 'avg. response time',
//          'mount' => $avg,
//          'percentage' => '5%',
//          'status' => 'down',
//          'dod' => '10.600'
//        ],
//      ],
//      [
//        'conversationData' => [
//          'newChatUser' => [
//            'mount' => $newChatUser,
//            'DoD' => 0,
//            'status' => 'up',
//            'percentageDoD' => 0
//          ]
//        ],
//        [
//          'incomingMessage' => [
//            'mount' => 0,
//            'DoD' => 0,
//            'status' => 'up',
//            'percentageDoD' => 0
//          ]
//        ]
//      ]
//    ];

    $return = [
      [
        'title' => 'incoming request',
        'mount' => $incoming,
        'percentage' => '5%',
        'status' => 'down',
        'dod' => '10.600'
      ],
      [
        'title' => 'served by agent',
        'mount' => $served,
        'percentage' => '5%',
        'status' => 'down',
        'dod' => '10.600'
      ],
      [
        'title' => 'resolved conversation',
        'mount' => $resolved,
        'percentage' => '5%',
        'status' => 'down',
        'dod' => '10.600'
      ],
      [
        'title' => 'avg. response time',
        'mount' => $avg,
        'percentage' => '5%',
        'status' => 'down',
        'dod' => '10.600'
      ],
    ];

    return response()->json($return);
  }

  public function getTrafficChannel() {
    $raw = \Illuminate\Support\Facades\DB::table($this->dbOmni . '.messages as m')
      ->join($this->dbApp . '.users as u', 'u.id', '=', 'm.messageable_id')
      ->join($this->dbOmni . '.channels as c', 'c.id', '=', 'u.channel_id')
      ->select('u.channel_id')
      ->whereDate('m.created_at', '2021-06-05')
      ->get();

    if (count($raw) == 0)
      return response()->json([
        'message' => 'Data not found',
        'data' => []
      ], 422);

    $channelList = \Illuminate\Support\Facades\DB::table($this->dbOmni . '.channels')
      ->select('id', 'name')
      ->whereRaw('id < 5 AND status = 1')
      ->get();

    foreach ($channelList as $channel) {
      $idChannel[] = $channel->id;
      $nameChannel[] = $channel->name;
    }

    $return = [];
    foreach ($raw as $r) {
      for ($i = 0; $i < count($idChannel); $i++) {
        if ($idChannel[$i] == $r->channel_id) {
          $return[$i][] = $r->channel_id;
        } else {
          $return[$i] = 0;
        }
      }
    }

    // values
    for ($x = 0; $x < count($return); $x++) {
      if ($return[$x] != 0) {
        $return[$x] = count($return[$x]);
      }
    }

    $values = [];
    $divider = max($return);
    for ($p = 0; $p < count($return); $p++) {
      if ($return[$p] != 0) {
        $color[] = '#' . $this->generateColor() . $this->generateColor() . $this->generateColor();
        $valueChart[] = $return[$p];
        $values[] = [
          'name' => $nameChannel[$p],
          'value' => $return[$p],
          'percentage' => $return[$p] / $divider * 100
        ];
      } else {
        $color[] = '#' . $this->generateColor() . $this->generateColor() . $this->generateColor();
        $valueChart[] = 0;
        $values[] = [
          'name' => $nameChannel[$p],
          'value' => 0,
          'percentage' => 0
        ];
      }
    }

    // custom color
    for ($s = 0; $s < count($values); $s++) {
      $values[$s]['color'] = $color[$s];
    }

    // data for chart
    $chart = [
      'chart' => [
        'type' => 'donut'
      ],
      'dataLabels' => [
        'enabled' => false
      ],
      'legend' => [
        'show' => false
      ],
      'responsive' => [
        [
          'breakpoint' => 480,
          'options' => [
            'chart' => [
              'width' => 200
            ],
            'legend' => [
              'position' => 'bottom'
            ]
          ]
        ]
      ],
      'fill' => [
        'colors' => $color
      ],
      'plotOptions' => [
        'pie' => [
          'donut' => [
            'labels' => [
              'show' => true,
              'total' => [
                'show' => true,
                'label' => 'Chat users',
                'fontSize' => '12px',
                'fontWeight' => 400,
//                  'formatter' => '() => ' . $divider
              ]
            ]
          ]
        ]
      ]
    ];

    return response()->json([
      'data' => $values,
      'chart' => $chart,
      'valueChart' => $valueChart
    ], 200);
  }

  public function generateColor() {
    $lineUp = str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);

    return $lineUp;
  }

  public function generalRecentUser() {
    $raw = \Illuminate\Support\Facades\DB::table($this->dbApp . '.users as u')
      ->join($this->dbOmni . '.channels as c', 'c.id' , '=', 'u.channel_id')
      ->select('u.name', 'c.icon', 'u.id', 'u.email')
      ->whereRaw("u.name IS NOT NULL AND u.name != '-'")
      ->whereDate('u.created_at', '2021-03-15')
      ->orderByDesc('u.created_at')
      ->limit(10)
      ->get();

    if (count($raw) == 0) {
      $data = array();
    }

    foreach ($raw as $r) {
      $splitName = explode(' ', $r->name);
      $data[] = [
        'name' => $splitName[0],
        'icon' => $r->icon,
        'status' => 1,
        'id' => $r->id,
        'email' => $r->email,
        'time' => rand(0, 60) . ' second ago'
      ];
    }

    return response()->json([
      'user' => $data,
      'userNum' => count($raw)
    ]);
  }

  public function getData(Request $request, $appId)
  {
    $start = $request->get('start');
    $end = $request->get('end');
    // return ["average_agent_first_response_time" => $this->_getAverageAgentFirstResponseTime($appId, $start, $end)];
    return $this->successResponse([
      "start" => $request->get('start'),
      "end" => $request->get('end'),
      "chart" => (object)[
        'consumer_base' => $this->_getConsumerBase($appId),
        'daily_user' => $this->_getDailyUser($appId, $start, $end),
        'tags' => $this->_getTags($appId, $start, $end),
      ],
      'total_user' => $this->_getTotalUser($appId, $start, $end),
      'total_conversation' => $this->_getTotalConversation($appId, $start, $end),
      'last_user' => $this->_getLastUser($appId, $start, $end),
      'agent_availability' => $this->_agentAvailability($appId),
      'total_of_conversations' => $this->_getTotalOfConversation($appId, $start, $end),
      // 'total_on_going_conversation' => $this->getTotalOnGoingConversation($appId, $start, $end),
      // 'total_resolved_conversation' => $this->_getTotalResolvedConversation($appId, $start, $end),
      // 'average_agent_first_response_time' => $this->_getAverageAgentFirstResponseTime($appId, $start, $end),
      // 'average_agent_first_response_time' => 0,
      'active_users' => $this->_getActiveUser($appId, $start, $end) //query builder blm sesuai
    ]);

  }

  // public function testing(Request $request, $appId)
  // {
  //     return $this->successResponse($request->get('getTest'));
  // }

  public function getDataChat($appId)
  {
    return $this->successResponse([
      'chart' => (object) [
      ],
    ]);
  }

  public function _getAgentMessage($appId)
  {
    $data = Room::where('app_id', $appId)
      ->take(10)
      ->get();
    return $data;
  }

  public function getAverageConversationDuration(Request $request, $appId)
  {
    $start = $request->get('start');
    $end = $request->get('end');

    $data = Livechat::whereHas('room', function ($q) use ($appId){
      $q->where('app_id', $appId);
    })
      ->whereDate('start_at', '>=', $start)
      ->whereDate('start_at', '<=', $end)
      ->where('status', 'resolved')
      ->get();

    $start = $end = $diff = [];
    if($data != null) {
      foreach ($data as $s) {
        $start[] = Carbon::parse($s->start_at);
        $end[] =  Carbon::parse($s->end_at);
        $diff[] = Carbon::parse($s->start_at)->diffInMinutes(Carbon::parse($s->end_at));
      }
      if ($diff === []) {
        return 0;
      }
      $avg = array_sum($diff) / count($diff);
      return $this->successResponse(round($avg));
      return round($avg);
    }

  }

  public function getAverageMessagesPerConversation(Request $request, $appId)
  {
    $start = $request->get('start');
    $end = $request->get('end');

    $datas = DB::table($this->dbOmni.".rooms as r")
      ->join($this->dbOmni.".messages as m", "m.room_id", "=", "r.id")
      ->where("r.app_id", $appId)
      ->whereDate("r.created_at", ">=", $start)
      ->whereDate("r.created_at", "<=", $end)
      // ->orderBy("r.id", "DESC")
      ->select("m.room_id as room_id", DB::raw("count(m.id)as total"))
      ->groupBy("room_id")
      ->get();

    if ($datas->isEmpty()) {
      return $this->successResponse(0);
    }


    foreach ($datas as $key => $value) {
      $data[] = $value->total;
    }

    $avg = array_sum($data) / count($data);
    return $this->successResponse(round($avg));
  }

  public function getTotalHsm(Request $request, $appId)
  {
    $start = $request->get('start');
    $end = $request->get('end');

    $hsm = ApiLog::where('app_id', $appId)
      ->where(function ($q) {
        $q->where('request', 'ILIKE', '%' . 'hsm:' . '%');
      })
      ->whereDate('created_at', '>=', $start)
      ->whereDate('created_at', '<=', $end)
      ->count();

    $data =
      [
        [
          'title' => 'all time HSM sent',
          'mount' => $hsm,
          'percentage' => '10%',
          'status' => "",
          'mom' => '',
          'icon' => 'hsmsent.png',
          'date' => 'since july 12, 2020',
          'template' => '',
          'active' => true
        ],
        [
          'title' => 'monthly quota',
          'mount' => '6.123',
          'percentage' => '7%',
          'status' => "up",
          'mom' => '5.694',
          'icon' => 'quota.png',
          'date' => '',
          'template' => '',
          'active' => false
        ],
        [
          'title' => 'approved template',
          'mount' => '2',
          'percentage' => '-5%',
          'status' => "up",
          'mom' => '',
          'icon' => 'template.png',
          'date' => '',
          'template' => 'adding 2 templates from last month',
          'active' => false
        ],
      ];

    return response()->json([
      'data' => $data
    ], 200);
  }

  public function _getAverageAgentFirstResponseTime($appId, $start, $end)
  {
    return 0;
    if(!$end) {
      $end = $start;
    }

    $datas = DB::table("livechats as l")
      ->join("rooms as r", "l.room_id", "=", "r.id")
      // ->join($this->dbOmni.".messages as m", "m.room_id", "=", "r.id")
      ->where("r.app_id", $appId)
      ->where("l.status", "resolved")
      ->whereDate('l.start_at', '>=', $start)
      ->whereDate('l.start_at', '<=', $end)
      // ->select("")
      ->get();
    if ($datas->isEmpty()) {
      return 0;
    }
    foreach ($datas as $key => $value) {
      $requestAt[$key] = DateTime::createFromFormat('Y-m-d H:i:s',$value->request_at);
      $startAt[$key] = DateTime::createFromFormat('Y-m-d H:i:s', $value->start_at);
      $diff[] = Carbon::parse($requestAt[$key])->diffInMinutes(Carbon::parse($startAt[$key]));
    }
    // return $diff;
    $avg = array_sum($diff) / count($diff);
    return number_format((float)$avg, 2, '.', '');

  }

  public function __getAverageAgentFirstResponseTime($appId, $start, $end)
  {
    if(!$end) {
      $end = $start;
    }

    // $rooms = DB::table('rooms')->select('id')
    //          ->where('app_id', $appId)
    //          ->get();

    // $livechat = Livechat::whereHas('room', function ($q) use ($appId) {
    //                                     $q->where('app_id', $appId);
    //                             })
    //                     ->where('status', 'resolved')
    //                     ->whereDate('start_at', '>=', $start)
    //                     ->whereDate('start_at', '<=', $end)
    //                     ->get();

    $livechat = DB::table("livechats as l")
      ->rightJoin("rooms as r", "l.room_id", "=", "r.id")
      // ->rightJoin("messages as m", "m.room_id", "=", "r.id")
      ->where("r.app_id", $appId)
      ->where("l.status", "resolved")
      ->whereDate('start_at', '>=', $start)
      ->whereDate('start_at', '<=', $end)
      ->get();
    // return $livechat;

    if($livechat->isEmpty()){
      return 0;
    }

    // foreach ($livechat as $key => $d) {
    //     $join[$key] = $d;
    //     $joiningRoom[$key] = $d->start_at;
    //     $exitingRoom[$key] = $d->end_at;
    // }
    // return $join;

    foreach ($livechat as $key => $j) {
      $rr[] = $j->start_at;
      // $msg[$key] = Message::whereHas('room', function ($q) use ($appId) {
      //                                 $q->where('app_id', $appId);
      //                             })
      //                         ->whereBetween('created_at', [$j->start_at, $j->end_at])
      //                         // ->whereDate('created_at', '>=', $j->start_at)
      //                         // ->whereDate('created_at', '<=', $j->end_at)
      //                         ->where('messageable_type', 'user_platform')
      //                         ->orderBy('id', 'ASC')
      //                         ->first();
      $msg[$key] = DB::table("messages as m")
        ->join("rooms as r", "m.room_id", "=", "r.id")
        ->where("r.app_id", $appId)
        ->whereBetween("m.created_at", [$j->start_at, $j->end_at])
        ->where("m.messageable_type", "user_platform")
        ->orderBy("m.id", "ASC")
        ->first();

      if($msg[$key] === null) {
        // $msg[$key]['created_at'] = $j->start_at;
        $msg[$key]["created_at"] = $j->start_at;
      }

      $agentResponse[$key] = $msg[$key]["created_at"];
      $diffInMinutes[$key] = Carbon::parse($j->start_at)->diffInMinutes($msg[$key]['created_at']);

    }
    return $msg;
    // return $msg[2]->created_at;
    $avg = array_sum($diffInMinutes) / count($diffInMinutes);
    return number_format((float)$avg, 2, '.', '');


  }

  public function _getLastUser($appId, $start, $end)
  {
    // $data = User::where("app_id", $appId)
    //             ->orderBy('created_at', "DESC")
    //             ->limit(20)
    //             ->get();

    $data = DB::table($this->dbApp. ".users")
      ->where("app_id", $appId)
      ->orderBy('created_at', "DESC")
      ->limit(15)
      ->select('name', 'email', 'created_at')
      ->get();
    return $data;
  }

  public function _getTotalConversation($appId, $start, $end)
  {
    return 0;
    if(!$end) {
      $end = $start;
    }

    // $data = Message::whereHas('room', function ($q) use ($appId) {
    //     $q->where('app_id', $appId);
    // })
    // ->whereDate('created_at', '>=', $start)
    // ->whereDate('created_at', '<=', $end)
    // ->count();

    $data = DB::table($this->dbOmni. ".messages as m")
      ->join($this->dbOmni. ".rooms as r", "m.room_id", "=", "r.id")
      ->where("r.app_id", $appId)
      ->whereDate('m.created_at', '>=', $start)
      ->whereDate('m.created_at', '<=', $end)
      ->count();

    return $data;
  }

  public function getOverallNumberConversation(Request $request, $appId)
  {
    // $data = Message::whereHas('room', function ($q) use ($appId) {
    //     $q->where('app_id', $appId);
    // })->count();
    $start = $request->get('start');
    $end = $request->get('end');

    $data = DB::table($this->dbOmni.".messages as m")
      ->select('m.id')
      ->join("omnichannel.rooms as r", "m.room_id", "=", "r.id")
      ->where("app_id", $appId)
      ->whereDate('m.created_at', '>=', $start)
      ->whereDate('m.created_at', '<=', $end)
      ->count();

    // mom
    $oldStart = $this->dateConvert($start);
    $oldEnd = $this->dateConvert($end);
    $dataMom = DB::table($this->dbOmni.".messages as m")
      ->select('m.id')
      ->join("omnichannel.rooms as r", "m.room_id", "=", "r.id")
      ->where("app_id", $appId)
      ->whereDate('m.created_at', '>=', $oldStart)
      ->whereDate('m.created_at', '<=', $oldEnd)
      ->count();

    $MoM = $this->MoM([
      'data' => $data,
      'dataMoM' => $dataMom
    ]);

    return response()->json([
      'success' => ($data == 0) ? false : true,
      'message' => null,
      'data' => [
        'totalMessages' => ($data == 0) ? number_format($data) : $data,
        'MoM' => $dataMom,
        'percentageMoM' => $MoM['percentageMoM'],
        'status' => $MoM['status']
      ]
    ]);
  }

  public function MoM($param = []) {
    $data = $param['data'];
    $dataMoM = $param['dataMoM'];

    if ($data > $dataMoM) {
      if ($dataMoM == 0) {
        $percentageMoM = 100;
        $status = 'up';
      } else {
        $a = $data - $dataMoM;
        $percentageMoM = number_format(ceil($a / $dataMoM * 100));
        $status = 'up';
      }

    } else if ($data == $dataMoM) {
      $percentageMoM = 0;
      $status = 'same';
    } else {
      if ($data == 0) {
        $percentageMoM = 100;
        $status = 'down';
      } else {
        $a = $dataMoM - $data;
        $percentageMoM = number_format(ceil($a / $data * 100));
        $status = 'down';
      }
    }

    $return = [
      'percentageMoM' => $percentageMoM,
      'status' => $status
    ];

    return $return;
  }

  public function dateConvert($param = "") {
    $oldDate = date('Y-m-d', strtotime($param . ' -1 months'));
    $a = date('d', strtotime($param));
    $b = date('d', strtotime($oldDate));

    if ($a != $b) {
      if ($a == 31 && $b == 01) {
        $oldest = date('Y-m-d', strtotime($param . ' -31 days'));
        $c = date('d', strtotime($oldest));
        if ($c == 30) {
          return $oldest;
        }

        return $oldest;
      }
    }

    return $oldDate;
  }

  public function _getTotalOfConversation($appId, $start, $end)
  {
    if(!$end) {
      $end = $start;
    }

    // $data = Livechat::whereHas('room', function ($q) use ($appId){
    //                         $q->where('app_id', $appId);
    //                     })
    //                 ->whereDate('start_at', '>=', $start)
    //                 ->whereDate('start_at', '<=', $end)
    //                 ->count();

    $data = DB::table($this->dbOmni. ".livechats as l")
      ->join($this->dbOmni. ".rooms as r", "l.room_id", "=", "r.id")
      ->where("r.app_id", $appId)
      ->whereDate('l.start_at', '>=', $start)
      ->whereDate('l.start_at', '<=', $end)
      ->select('l.id')
      ->count();

    return $data;
  }

  public function getOverallLiveConversations($appId)
  {
    // $data = Livechat::whereHas('room', function ($q) use ($appId) {
    //     $q->where('app_id', $appId);
    // })->count();
    $data = DB::table($this->dbOmni.".livechats as l")
      ->join($this->dbOmni.".rooms as r", "r.id", "=", "l.room_id")
      ->where("r.app_id", $appId)->count();
    return $data;
  }

  public function _getTotalUser($appId, $start, $end)
  {
    if(!$end) {
      $end = $start;
    }

    // $data = User::where('app_id', $appId)
    //             ->whereDate('created_at', '>=' ,$start)
    //             ->whereDate('created_at', '<=', $end)
    //             ->count();
    $data = DB::table($this->dbApp. ".users as u")
      ->where("u.app_id", $appId)
      ->whereDate("u.created_at", ">=", $start)
      ->whereDate("u.created_at", "<=", $end)
      ->select('id')
      ->count();
    return $data;
  }

  public function getOverallUser(Request $request, $appId)
  {
    // $overallUser = User::where('app_id', $appId)->count();
    $start = $request->get('start');
    $end = $request->get('end');

    $oldStart = $this->dateConvert($start);
    $oldEnd = $this->dateConvert($end);

    $overallUser = DB::table($this->dbApp.".users as u")
      ->select('u.id')
      ->where("app_id", $appId)
      ->whereDate('created_at', '>=', $start)
      ->whereDate('created_at', '<=', $end)
      ->count();

    // mom
    $dataMoM = DB::table($this->dbApp.".users as u")
      ->select('u.id')
      ->where("app_id", $appId)
      ->whereDate('created_at', '>=', $oldStart)
      ->whereDate('created_at', '<=', $oldEnd)
      ->count();
    $MoM = $this->MoM([
      'data' => $overallUser,
      'dataMoM' => $dataMoM
    ]);

    return response()->json([
      'success' => true,
      'message' => null,
      'data' => [
        'overallUsers' => number_format($overallUser),
        'MoM' => number_format($dataMoM),
        'percetangeMoM' => $MoM['percentageMoM'],
        'status' => $MoM['status']
      ]
    ]);
  }

  /**
   * @param start
   * @param end
   * @return ressponse json
  */
  public function totalLiveConversations(Request  $request, $appId) {
    $start = $request->get('start');
    $end = $request->get('end');

    $oldStart = $this->dateConvert($start);
    $oldEnd = $this->dateConvert($end);

    $data = \Illuminate\Support\Facades\DB::table($this->dbOmni . '.messages as m')
      ->join($this->dbApp . '.users as u', 'u.id', '=', 'm.messageable_id')
      ->join($this->dbOmni . '.livechats as l', 'l.room_id', '=', 'm.room_id')
      ->where('u.app_id', $appId)
      ->where('l.status', '=', 'live')
      ->whereDate('m.created_at', '>=', $start)
      ->whereDate('m.created_at', '<=', $end)
      ->count();

    $dataMoM = \Illuminate\Support\Facades\DB::table($this->dbOmni . '.messages as m')
      ->join($this->dbApp . '.users as u', 'u.id', '=', 'm.messageable_id')
      ->join($this->dbOmni . '.livechats as l', 'l.room_id', '=', 'm.room_id')
      ->where('u.app_id', $appId)
      ->where('l.status', '=', 'live')
      ->whereDate('m.created_at', '>=', $oldStart)
      ->whereDate('m.created_at', '<=', $oldEnd)
      ->count();
    $MoM = $this->MoM([
      'data' => $data,
      'dataMoM' => $dataMoM
    ]);

    return response()->json([
      'status' => true,
      'message' => null,
      'data' => [
        'liveConversation' => number_format($data),
        'MoM' => number_format($dataMoM),
        'percentageMoM' => $MoM['percentageMoM'],
        'status' => $MoM['status']
      ]
    ]);
  }

  public function _getTotalTransaction($appId)
  {
    $data = Transaction::whereHas('user', function ($q) use ($appId) {
      $q->where('app_id', $appId);
    })->count();
    return $data;
  }

  public function _getTotalAmount($appId)
  {
    $data = Transaction::whereHas('user', function ($q) use ($appId) {
      $q->where('app_id', $appId);
    })->sum('amount');
    return $data;
  }

  public function _getConsumerBase($appId)
  {
    $data = DB::table($this->dbApp.".users as u")
      ->join('channels as c', 'c.id', '=', "u.channel_id")
      ->select(DB::raw('count(c.name) as channel_count, c.name'))
      ->where('u.app_id', $appId)
      ->groupBy('c.name')
      ->get();
    return $data;
  }

  public function getTotalHsmPertemplate(Request $request, $appId)
  {
    $start = $request->get('start');
    $end = $request->get('end');

    $check = DB::table('broadcast_messages')->where('app_id', $appId)->first();
    $msg = 'there is no broadcast hsm';
    if (!$check) {
      return $this->successResponse(null, $msg);
    }

    $data = DB::table('broadcast_messages')
      ->where("category", "hsm")
      ->where("channel_id", 4) //channel id 4 whatsapp
      ->where('app_id', $appId)
      ->whereDate('created_at', '>=', $start)
      ->whereDate('created_at', '<=', $end)
      ->select(DB::raw("count(*) as template_count, topics as template_name"))
      ->groupBy('template_name')
      ->get();
    return $this->successResponse($data);
  }

  public function _getTags($appId, $start, $end){
    $data = DB::table("room_tag as r")
      ->join('tags as t', 'r.tag_id', '=', 't.id')
      ->where('app_id', $appId)
      ->select(DB::raw("count('t.name') as tag_count, t.name"))
      ->groupBy('t.name')
      ->get();
    return $data;
  }

  public function getDailyMessage(Request $request, $appId)
  {
    $start = $request->get('start');
    $end = $request->get('end');

    $data = Message::select('created_at')
      // ->where('created_at', '>=', Carbon::now('Asia/Jakarta')->subMonth())
      ->whereDate('created_at', '>=', $start)
      ->whereDate('created_at', '<=', $end)
      ->whereHas('room', function ($q) use ($appId) {
        $q->where('app_id', $appId);
      })
      ->orderBy('created_at', 'ASC')

      ->get()->groupBy(function ($val) {
        return Carbon::parse($val->created_at)->format('M d Y');
      });
    $counted = $data->map(function ($item, $key) {
      return collect($item)->count();
    });
    return $this->successResponse($counted);

  }

  public function getDailyHsm(Request $request, $appId)
  {
    $start = $request->get('start');
    $end = $request->get('end');

    $hsm = ApiLog::select('created_at','request')
      ->where('app_id', $appId)
      ->where(function ($q) {
        $q->where('request', 'ILIKE', '%' . 'hsm:' . '%');
      })
      //  ->where('created_at', '>=', Carbon::now('Asia/Jakarta')->subMonth())
      ->whereDate('created_at', '>=', $start)
      ->whereDate('created_at', '<=', $end)
      ->orderBy('created_at', 'ASC')
      ->get()
      ->groupBy(function ($val) {
        return Carbon::parse($val->created_at)->format('M d Y');
      });
    $counted = $hsm->map(function ($item, $key) {
      return collect($item)->count();
    });
    return $this->successResponse($counted);
  }

  public function getTotalOnGoingConversation($appId, $start, $end)
  {
    if(!$end) {
      $end = $start;
    }

    // $data = Livechat::whereHas('room', function ($q) use ($appId) {
    //                          $q->where('app_id', $appId);
    //                     })
    //                 ->where('status', 'live')
    //                 ->whereDate('start_at', '>=', $start)
    //                 ->whereDate('start_at', '<=', $end)
    //                 ->count();
    $data = DB::table($this->dbOmni. ".livechats as l")
      ->join($this->dbOmni. ".rooms as r", "l.room_id", "=", "r.id")
      ->where("r.app_id", $appId)
      ->where('l.status', 'live')
      ->whereDate('l.start_at', '>=', $start)
      ->whereDate('l.start_at', '<=', $end)
      ->select('l.id')
      ->count();
    return $data;
  }

  public function _getTotalResolvedConversation($appId, $start, $end)
  {
    if(!$end) {
      $end = $start;
    }
    // $data = Livechat::where('status', 'resolved')
    //                 ->whereHas('room', function ($q) use ($appId) {
    //                     $q->where('app_id', $appId);
    //                 })
    //                 ->whereDate('start_at', '>=', $start)
    //                 ->whereDate('start_at', '<=', $end)
    //                 ->count();

    $data = DB::table($this->dbOmni. ".livechats as l")
      ->join($this->dbOmni. ".rooms as r", "l.room_id", "=", "r.id")
      ->where("r.app_id", $appId)
      ->where("l.status", "resolved")
      ->whereDate('l.start_at', '>=', $start)
      ->whereDate('l.start_at', '<=', $end)
      ->select('l.id')
      ->count();
    return $data;
  }

  public function getDailyResolveConversation(Request $request, $appId)
  {
    $start = $request->get('start');
    $end = $request->get('end');

    $data = Livechat::select('end_at')
      // ->where('end_at', '>=', Carbon::now('Asia/Jakarta')->subMonth()) //Carbon::now('Asia/Jakarta')->subMonth() get bulan sebelumnya
      ->whereDate('start_at', '>=', $start)
      ->whereDate('start_at', '<=', $end)
      ->where('status', 'resolved')
      ->orderBy('end_at', 'ASC')
      ->whereHas('room', function ($q) use ($appId) {
        $q->where('app_id', $appId);
      })->get()->groupBy(function ($val) {
        return Carbon::parse($val->end_at)->format('M d Y');
      });
    $counted = $data->map(function ($item, $key){
      return collect($item)->count();
    });
    return $this->successResponse($counted);
  }

  public function _getTotalMessagesPerAgent($appId)
  {
    $data = Message::where('messageable_id', 18)
      ->whereHas('room', function ($q) use ($appId) {
        $q->where('app_id', $appId);
      })->count();
    return $data;
  }

  public function getDailyOnGoingConversation(Request $request, $appId)
  {
    $start = $request->get('start');
    $end = $request->get('end');

    $data = Livechat::select('start_at')
      ->whereDate('start_at', '>=', $start)
      ->whereDate('start_at', '<=', $end)
      // ->where('start_at', '>=', Carbon::now('Asia/Jakarta')->subMonth())
      // ->where('status', 'live')
      ->orderBy('start_at', 'ASC')
      ->whereHas('room', function ($q) use ($appId) {
        $q->where('app_id', $appId);
      })->get()->groupBy(function ($val) {
        return Carbon::parse($val->start_at)->format('M d Y');
      });
    $counted = $data->map(function ($item, $key){
      return collect($item)->count();
    });
    return $this->successResponse($counted);
  }

  public function _getDailyUser($appId, $start, $end)
  {
    if(!$end){
      $end = $start;
    }

    // $data = User::select('created_at')
    //             ->where('app_id', $appId)
    //             // ->where('created_at', '>=', Carbon::now('Asia/Jakarta')->subMonth())
    //             ->whereDate('created_at', '>=', $start)
    //             ->whereDate('created_at', '<=',$end)
    //             ->orderBy('created_at', 'ASC')
    //             ->get()
    //             ->groupBy(function ($q) {
    //                     return Carbon::parse($q->created_at)->format('M d Y');
    //                 });

    $data = DB::table($this->dbApp. ".users as u")
      ->where("u.app_id", $appId)
      ->whereDate('u.created_at', '>=', $start)
      ->whereDate('u.created_at', '<=',$end)
      ->orderBy("u.created_at", "ASC")
      ->select('u.id','u.created_at')
      ->get()
      ->groupBy(function ($q) {
        return Carbon::parse($q->created_at)->format("M d Y");
      });

    $counted = $data->map(function ($item, $key) {
      return collect($item)->count();
    });
    return $counted;

  }

  public function _agentAvailability($appId)
  {
    $agent = RoleHelper::agents($appId, null);
    return $agent;
  }

  public function _firstResponse()
  {
    $messages = Message::where('room_id', 16)->orderBy('id', 'desc')->take(10)->get();
    $messages = $messages->sortBy('id');
    return $this->successResponse(['messages' => $messages->values()->all()]);
  }

  public function getTest($appId)
  {
    $data = Message::select('created_at')
      ->where('created_at', '>=', Carbon::now('Asia/Jakarta')->subMonth())
      ->orderBy('created_at', 'ASC')
      ->whereHas('room', function ($q) use ($appId) {
        $q->where('app_id', $appId);
      })->get()->groupBy(function ($val) {
        return Carbon::parse($val->created_at)->format('M d');
      });
    $counted = $data->map(function ($item, $key) {
      return collect($item)->count();
    });
    return $counted;
  }


  public function totalMessageAgent(Request $request, $appId)
  {
    $start = $request->get('start');
    $end = $request->get('end');
    $agentId = $request->get('agentId');
    $agents = RoleHelper::agents($appId);


    if ($agentId) {
      $agent = RoleHelper::agentId($agentId, $appId);
      $tmp_msg = DB::table("messages as m")
        ->join("rooms as r", "m.room_id", "=", "r.id")
        ->where("r.app_id", $appId)
        ->whereDate("m.created_at", ">=", $start)
        ->whereDate("m.created_at", "<=", $end)
        ->where("messageable_type", "=", "user_platform")->where("messageable_id", "=", $agentId)->count();
      if (count($agent) > 0) {
        $filtered =
          (array)[
            'name' => $agent[0]->name,
            'total_message' => $tmp_msg ?? 0
          ];
        return $this->successResponse(["agent" => [$filtered]]);
      }

      return response()->json([
        'success' => false,
        'message' => 'Agent tidak ditemukan',
        'data' => [
          'agent' => array()
        ]
      ], 422);

    }

    if (count($agents) !== 0) {
      foreach ($agents as $key => $agent) {
        $tmp_msg[$key] = DB::table("messages as m")
          ->join("rooms as r", "m.room_id", "=", "r.id")
          ->where("r.app_id", $appId)
          ->whereDate("m.created_at", ">=", $start)
          ->whereDate("m.created_at", "<=", $end)
          ->where("messageable_type", "=", "user_platform")->where("messageable_id", "=", $agent->id)->count();
        $agent->total_message = $tmp_msg[$key];
        $tmp_data[] = $agent;
      }

      $filtered = collect($tmp_data)->map(function($item,$key){
        return (object)[
          'name' => $item->name,
          'total_message' => $item->total_message
        ];
      });
      return $this->successResponse(["agent" => $filtered]);
    };
    return $this->successResponse(["agent" => null]);
  }

  public function newConversationPerAgent(Request $request, $appId)
  {
    $start = $request->get('start');
    $end = $request->get('end');
    $agentId = $request->get('agentId');
    $agents = RoleHelper::agents($appId, false);

    if ($agentId) {
      $agent = RoleHelper::agentId($agentId, $appId);
      // $data2 = DB::table("livechats as l")
      //     ->join("rooms as r", "l.room_id", "=", "r.id")
      //     ->where("r.app_id", $appId)
      //     ->whereDate('start_at', '>=', $start)
      //     ->whereDate('start_at', '<=',$end)
      //     ->where('handle_by', $agentId)->count();

      if (count($agent) == 0) {
        return response()->json([
          'success' => false,
          'message' => 'Percakapan baru tidak ditemukan',
          'data' => [
            'new_conversation_per_agent' => []
          ]
        ], 422);
      }

      $data = DB::table("time_report_served")->where('app_id', $appId)
        ->whereDate('created_at', '>=', $start)
        ->whereDate('created_at', '<=',$end)
        ->where('agent_id', $agentId)->count();

      $filtered =
        (array)[
          'name' => $agent[0]->name,
          'new_conversation_per_agent' => $data ?? 0
        ];
      return $this->successResponse(['new_conversation_per_agent' => [$filtered]]);
    }


    if (count($agents) !== 0) {
      foreach ($agents as $key => $agent) {

        // $data2[] = DB::table("livechats as l")
        //             ->join("rooms as r", "l.room_id", "=", "r.id")
        //             ->where("r.app_id", $appId)
        //             ->whereDate('start_at', '>=', $start)
        //             ->whereDate('start_at', '<=',$end)
        //             ->where('handle_by', $agent->id)->count();
        $data[] = DB::table("time_report_served")->where('app_id', $appId)
          ->whereDate('created_at', '>=', $start)
          ->whereDate('created_at', '>=', $end)
          ->where('agent_id', $agent->id)
          ->count();

        $agent->new_conversation_per_agent = $data[$key];
        $datas[] = $agent;
      }

      $filtered = collect($datas)->map(function($item,$key){
        return (object)[
          'name' => $item->name,
          'new_conversation_per_agent' => $item->new_conversation_per_agent
        ];
      });
      return $this->successResponse(['new_conversation_per_agent' => $filtered]);
    }
    return $this->successResponse(["new_conversation_per_agent" => null]);
  }

  public function onGoingConversationPerAgent(Request $request, $appId)
  {
    $start = $request->get('start');
    $end = $request->get('end');
    $agentId = $request->get('agentId');
    $agents = RoleHelper::agents($appId, false);

    if ($agentId) {
      $agent = RoleHelper::agentId($agentId, $appId);
      // $tmp_data = DB::table("livechats as l")
      //             ->join("rooms as r", "l.room_id", "=", "r.id")
      //             ->where("r.app_id", $appId)
      //             ->whereDate('start_at', '>=', $start)
      //             ->whereDate('start_at', '<=',$end)
      //             ->where('handle_by', $agentId)->where('status', 'live')->count();

      $tmp_data = DB::table('time_report_served')
        ->where('app_id', $appId)
        ->whereDate('created_at', '>=', $start)
        ->whereDate('created_at', '<=', $end)
        ->where('agent_id', $agentId)
        ->count();

      $filtered =
        (array)[
          'name' => $agent[0]->name,
          'total_ongoing_livechat' => $tmp_data
        ];
      return $this->successResponse(['total_ongoing_room_conversation' => [$filtered]]);

    }

    if (count($agents) !== 0) {
      foreach ($agents as $key => $agent) {
        // $data[] = DB::table("livechats as l")
        //             ->join("rooms as r", "l.room_id", "=", "r.id")
        //             ->where("r.app_id", $appId)
        //             ->whereDate('start_at', '>=', $start)
        //             ->whereDate('start_at', '<=',$end)
        //             ->where('handle_by', $agent->id)->where('status', 'live')->count();

        $data[] = DB::table('time_report_served')
          ->where('app_id', $appId)
          ->whereDate('created_at', '>=', $start)
          ->whereDate('created_at', '<=', $end)
          ->where('agent_id', $agent->id)
          ->count();

        $agent->total_ongoing_livechat = $data[$key];
        $datas[] = $agent;
      }

      $filtered = collect($datas)->map(function($item,$key) {
        return (object)[
          'name' => $item->name,
          'total_ongoing_livechat' => $item->total_ongoing_livechat
        ];
      });

      return $this->successResponse(['total_ongoing_room_conversation' => $filtered]);
    }
    return $this->successResponse(["total_ongoing_room_conversation" => null]);
  }

  public function servedRoomConvPerGroupAgent(Request $request, $appId) {
    $start = $request->get('start');
    $end = $request->get('end');
    $agentId = $request->get('agentId');
    // $agents = RoleHelper::agents($appId, false);
    $groups = DB::table("groups")->where("app_id", $appId)->get();

    if (count($groups) !== 0) {
      foreach ($groups as $key => $group) {
        $data[] = DB::table("history_group_assignments")
          // ->join("groups as g", "h.group_id", "=", "g.id")
          ->where("group_id", $group->id)
          // ->where("h.app_id", $appId)
          ->whereDate("created_at", ">=", $start)
          ->whereDate("created_at", "<=", $end)
          // ->select("g.name as group_name", DB::raw("count('*') as total"))
          // ->groupBy("g.name")
          ->count();
        // ->get();
        $group->total = $data[$key];
        $datas[] = $group;
      }

      $filtered = collect($datas)->map(function($item,$key) {
        return (object)[
          "group_name" => $item->name,
          "total" => $item->total
        ];
      });
      return $this->successResponse(['served_room_conv_group_agent' => $filtered]);

    }

    return $this->successResponse(['served_room_conv_group_agent' => null]);
  }

  public function resolvedConversationPerAgent(Request $request, $appId)
  {
    $start = $request->get('start');
    $end = $request->get('end');
    $agentId = $request->get('agentId');
    $agents = RoleHelper::agents($appId, false);


    if ($agentId) {
      $agent = RoleHelper::agentId($agentId, $appId);
      if (count($agent) == 0) {
        return response()->json([
          'success' => false,
          'message' => 'Percakapan tidak ditemukan',
          'data' => [
            'total_resolved_room_conversation' => []
          ]
        ]);
      }

      $data = DB::table("session as s")
        ->join("rooms as r", "r.id", "=", "s.room_id")
        ->where("r.app_id", $appId)
        ->whereDate("s.created_at", ">=", $start)
        ->whereDate("s.created_at", "<=", $end)
        ->where("handle_by", $agentId)
        // ->orderBy('s.created_at', 'desc')
        // ->take(10)
        ->count();

      $filtered =
        (array)[
          'name' => $agent[0]->name,
          'total_resolved_livechat' => $data
        ];

      return $this->successResponse(['total_resolved_room_conversation' => [$filtered]]);
    }

    if (count($agents) !== 0) {
      foreach ($agents as $key => $agent) {
        // $data2[] = DB::table("livechats as l")
        //             ->join("rooms as r", "l.room_id", "=", "r.id")
        //             ->where("r.app_id", $appId)
        //             ->whereDate('l.start_at', '>=', $start)
        //             ->whereDate('l.start_at', '<=',$end)
        //             ->where('l.handle_by', $agent->id)->where('l.status', 'resolved')->count();

        $data[] = DB::table("session as s")
          ->join("rooms as r", "r.id", "=", "s.room_id")
          ->where("r.app_id", $appId)
          ->whereDate("s.created_at", ">=", $start)
          ->whereDate("s.created_at", "<=", $end)
          ->where("handle_by", $agent->id)
          // ->orderBy('s.created_at', 'desc')
          // ->take(10)
          ->count();

        $agent->total_resolved_livechat = $data[$key];
        $datas[] = $agent;
      }

      $filtered = collect($datas)->map(function($item,$key) {
        return (object)[
          'name' => $item->name,
          'total_resolved_livechat' => $item->total_resolved_livechat
        ];
      });

      return $this->successResponse(['total_resolved_room_conversation' =>$filtered]);
    }
    return $this->successResponse(['total_resolved_room_conversation' => null]);
  }

  public function dailyMessagesPerAgent(Request $request, $appId)
  {
    $agents = RoleHelper::agents($appId, false);
    $agentId = $request->get('agentId');
    $period = CarbonPeriod::create(Carbon::now('Asia/Jakarta')->subDays(2)->format('Y-m-d') , Carbon::now('Asia/Jakarta')->format('Y-m-d'));
    foreach($period as $date){
      $days[] =  $date->format('d M Y');
    }

    if ($agentId) {
      $agent = RoleHelper::agentId($agentId, $appId);

      $tmp_msg = DB::table("messages")->where('messageable_type','=','user_platform')->where('messageable_id','=', $agentId)
        ->orderBy('created_at', 'ASC')
        ->whereDate('created_at', Carbon::today('Asia/Jakarta'))->count();

      $tmp_msg2 = DB::table("messages")->where('messageable_type','=','user_platform')->where('messageable_id','=', $agentId)
        ->orderBy('created_at', 'ASC')
        ->whereDate('created_at', Carbon::today('Asia/Jakarta')->subDays(1))->count();

      $tmp_msg3 = DB::table("messages")->where('messageable_type','=','user_platform')->where('messageable_id','=', $agentId)
        ->orderBy('created_at', 'ASC')
        ->whereDate('created_at', Carbon::today('Asia/Jakarta')->subDays(2))->count();

      $output['series'][] = [
        'name' => $agent[0]->name,
        'data' => [$tmp_msg3 ,$tmp_msg2 ,$tmp_msg]
      ];

      $output['categories'] = $days;
      return $this->successResponse(['daily_messages_per_agent' => $output]);
    }

    foreach ($agents as $key => $agent) {
      $tmp_msg[$key] = DB::table("messages")->where('messageable_type','=','user_platform')->where('messageable_id','=',$agent->id)
        // ->where('created_at', '>=', Carbon::now('Asia/Jakarta')->subWeek())
        ->orderBy('created_at', 'ASC')
        ->whereDate('created_at', Carbon::today('Asia/Jakarta'))->count();
      $tmp_msg2[$key] = DB::table("messages")->where('messageable_type','=','user_platform')->where('messageable_id','=',$agent->id)
        // ->where('created_at', '>=', Carbon::now('Asia/Jakarta')->subWeek())
        ->orderBy('created_at', 'ASC')
        ->whereDate('created_at', Carbon::today('Asia/Jakarta')->subDays(1))->count();
      $tmp_msg3[$key] = DB::table("messages")->where('messageable_type','=','user_platform')->where('messageable_id','=',$agent->id)
        // ->where('created_at', '>=', Carbon::now('Asia/Jakarta')->subWeek())
        ->orderBy('created_at', 'ASC')
        ->whereDate('created_at', Carbon::today('Asia/Jakarta')->subDays(2))->count();


      $output['series'][] = [
        'name' => $agent->name,
        'data' => [$tmp_msg3[$key],$tmp_msg2[$key],$tmp_msg[$key]]
      ];
      $output['categories'] = $days;

    }

    return $this->successResponse(['daily_messages_per_agent' => $output]);
  }

  public function dailyNewConversationPerAgent2($appId)
  {

    $agents = RoleHelper::agents($appId, false);

    foreach ($agents as $key => $agent) {
      $data[$key] = Livechat::where('handle_by', $agent->id)
        ->whereHas('room', function ($q) use ($appId) {
          $q->where('app_id', $appId);
        })
        ->whereDate('start_at', Carbon::today('Asia/Jakarta'))
        ->orderBy('start_at', 'ASC')
        ->count();
      $data2[$key] = Livechat::where('handle_by', $agent->id)
        ->whereHas('room', function ($q) use ($appId) {
          $q->where('app_id', $appId);
        })
        ->whereDate('start_at', Carbon::today('Asia/Jakarta')->subDays(1))
        ->orderBy('start_at', 'ASC')
        ->count();
      $data3[$key] = Livechat::where('handle_by', $agent->id)
        ->whereHas('room', function ($q) use ($appId) {
          $q->where('app_id', $appId);
        })
        ->whereDate('start_at', Carbon::today('Asia/Jakarta')->subDays(2))
        ->orderBy('start_at', 'ASC')
        ->count();
    }

    $period = CarbonPeriod::create(Carbon::now('Asia/Jakarta')->subDays(2)->format('Y-m-d') , Carbon::now('Asia/Jakarta')->format('Y-m-d'));
    foreach($period as $date){
      $days[] = $date->format('d M Y');
    }

    foreach ($agents as $keys => $outputs) {
      $output['series'][] = [
        'name' => $outputs->name,
        'data' => [$data3[$keys], $data2[$keys], $data[$keys]]
      ];
      $output['categories'] = $days;
    }
    return $this->successResponse(['daily_new_conversation_per_agent' => $output]);
  }

  public function dailyNewConversationPerAgent(Request $request, $appId)
  {
    $agents = RoleHelper::agents($appId, false);
    $agentId = $request->get('agentId');

    $period = CarbonPeriod::create(Carbon::now("Asia/Jakarta")->subDays(2)->format("Y-m-d"), Carbon::now("Asia/Jakarta")->format("Y-m-d"));
    foreach($period as $date) {
      $days[] = $date->format("d M Y");
    }

    if ($agentId) {
      $agent = RoleHelper::agentId($agentId, $appId);
      $data = DB::table("livechats as l")
        ->join("rooms as r", "l.room_id", "=", "r.id")
        ->where("r.app_id", $appId)
        ->where("l.handle_by", $agentId)
        ->whereDate("l.start_at", Carbon::today("Asia/Jakarta"))
        ->orderBy("l.start_at", "ASC")
        ->count();
      $data2 = DB::table("livechats as l")
        ->join("rooms as r", "l.room_id", "=", "r.id")
        ->where("r.app_id", $appId)
        ->where("l.handle_by", $agentId)
        ->whereDate("l.start_at", Carbon::today("Asia/Jakarta")->subDays(1))
        ->orderBy("l.start_at", "ASC")
        ->count();

      $data3 = DB::table("livechats as l")
        ->join("rooms as r", "l.room_id", "=", "r.id")
        ->where("r.app_id", $appId)
        ->where("l.handle_by", $agentId)
        ->whereDate("l.start_at", Carbon::today("Asia/Jakarta")->subDays(2))
        ->orderBy("l.start_at", "ASC")
        ->count();


      $outputs["series"][] = [
        "name" => $agent[0]->name,
        "data" => [$data3, $data2, $data]
      ];
      $outputs["categories"] = $days;
      return $this->successResponse(["daily_new_conversation_per_agent" => $outputs]);
    }

    foreach($agents as $key => $agent) {
      $data[$key] = DB::table("livechats as l")
        ->join("rooms as r", "l.room_id", "=", "r.id")
        ->where("r.app_id", $appId)
        ->where("l.handle_by", $agent->id)
        ->whereDate("l.start_at", Carbon::today("Asia/Jakarta"))
        ->orderBy("l.start_at", "ASC")
        ->count();
      $data2[$key] = DB::table("livechats as l")
        ->join("rooms as r", "l.room_id", "=", "r.id")
        ->where("r.app_id", $appId)
        ->where("l.handle_by", $agent->id)
        ->whereDate("l.start_at", Carbon::today("Asia/Jakarta")->subDays(1))
        ->orderBy("l.start_at", "ASC")
        ->count();
      $data3[$key] = DB::table("livechats as l")
        ->join("rooms as r", "l.room_id", "=", "r.id")
        ->where("r.app_id", $appId)
        ->where("l.handle_by", $agent->id)
        ->whereDate("l.start_at", Carbon::today("Asia/Jakarta")->subDays(2))
        ->orderBy("l.start_at", "ASC")
        ->count();


      $outputs["series"][] = [
        "name" => $agent->name,
        "data" => [$data3[$key], $data2[$key], $data[$key]]
      ];
      $outputs["categories"] = $days;
    }

    return $this->successResponse(["daily_new_conversation_per_agent" => $outputs]);
  }

  public function averageConversationDurationPerAgent(Request $request, $appId)
  {
    $start = $request->get('start');
    $end = $request->get('end');
    $agentId = $request->get('agentId');
    $agents = RoleHelper::agents($appId, false);


    if ($agentId) {
      $agent = RoleHelper::agentId($agentId, $appId);

      if (count($agent) == 0) {
        return response()->json([
          'success' => false,
          'message' => 'Perakaapn yang terlalau diucapkan',
          'data' => [
            'average_conversation_duration_per_agent' => array()
          ]
        ]);
      }

      $datas = DB::table("session as s")
        ->join("rooms as r", "r.id", "=", "s.room_id")
        ->where("r.app_id", $appId)
        ->whereDate("s.created_at", ">=", $start)
        ->whereDate("s.created_at", "<=", $end)
        ->where("s.handle_by", $agentId)
        ->get();

      if($datas->isEmpty()){
        $avg = 0;
      } else {
        // $avg[$key] = $datas[$key];
        foreach ($datas as $key2 => $value) {
          $diff[$key2] = Carbon::parse($value->start_at)->diffInMinutes(Carbon::parse($value->end_at));
        }
        $avg = round(array_sum($diff) / count($diff));
      }

      $output["series"]["data"] = [$avg];
      $output["categories"] = [$agent[0]->name];
      return $this->successResponse(["average_conversation_duration_per_agent" => $output]);
    }
    if (count($agents) !== 0) {
      foreach ($agents as $key => $agent) {
        $agentName[$key] = $agent->name;
        // $datas[$key] = DB::table("livechats as l")
        //                  ->join("rooms as r", "l.room_id", "=", "r.id")
        //                  ->where("r.app_id", $appId)
        //                  ->whereDate("l.start_at", ">=", $start)
        //                  ->whereDate("l.start_at", "<=", $end)
        //                  ->where("l.handle_by", $agent->id)->where("l.status", "resolved")
        //                  ->get();
        $datas[$key] = DB::table("session as s")
          ->join("rooms as r", "r.id", "=", "s.room_id")
          ->where("r.app_id", $appId)
          ->whereDate("s.created_at", ">=", $start)
          ->whereDate("s.created_at", "<=", $end)
          ->where("s.handle_by", $agent->id)
          ->get();

        if($datas[$key]->isEmpty()){
          $avg[$key] = 0;
        } else {
          // $avg[$key] = $datas[$key];
          foreach ($datas[$key] as $key2 => $value) {
            $diff[$key][$key2] = Carbon::parse($value->start_at)->diffInMinutes(Carbon::parse($value->end_at));
          }
          $avg[$key] = round(array_sum($diff[$key]) / count($diff[$key]));
        }

      }
    }
    // return $avg;
    $output["series"]["data"] = $avg;
    $output["categories"] = $agentName;
    return $this->successResponse(["average_conversation_duration_per_agent" => $output]);
  }

  public function averageFirstResponseTimePerAgent2(Request $request, $appId)
  {
    $start = $request->get('start');
    $end = $request->get('end');

    $agents = RoleHelper::agents($appId, false);

    foreach ($agents as $a){
      $dataAgents[] = $a->name;
    }

    $data = Livechat::whereHas('room', function ($q) use ($appId) {
      $q->where('app_id', $appId);
    })->get();

    foreach ($data as $key => $d) {
      $join[$key] = $d;
      $joiningRoom[$key] = $d->start_at;
      $exitingRoom[$key] = $d->end_at;
    }

    foreach ($agents as $key => $agent) {
      $datas[$key] = Livechat::whereHas('room', function ($q) use ($appId) {
        $q->where('app_id', $appId);
      })
        ->whereDate('start_at', '>=', $start)
        ->whereDate('start_at', '<=',$end)
        ->where('handle_by', $agent->id)->where('status', 'resolved')
        ->get();

      if($datas[$key]->isEmpty()){
        $avg[$key] = 0;
      }
      else {
        foreach ($datas[$key] as $key2 => $d) {
          $join[$key2] = $d;
          $msg[$key][$key2] = Message::whereHas('room', function ($q) use ($appId) {
            $q->where('app_id', $appId);
          })
            ->whereBetween('created_at', [$d->start_at, $d->end_at])
            ->where('messageable_type', 'user_platform')
            ->orderBy('id', 'ASC')
            ->first();
          if($msg[$key][$key2] === null){
            $msg[$key][$key2]['created_at'] = $d->start_at;
          }
          $diffInMinutes[$key][$key2] = Carbon::parse($d->start_at)->diffInMinutes(Carbon::parse($msg[$key][$key2]['created_at']));
        }
        $avg[$key] = number_format((float)(array_sum($diffInMinutes[$key]) / count($diffInMinutes[$key])), 1, '.', '');

      }


    }



    $output['series']['data'] = $avg;
    $output['categories'] = $dataAgents;
    return $this->successResponse(['average_first_response_time_per_agent' => $output ]);
  }

  public function averageFirstResponseTimePerAgentOld(Request $request, $appId)
  {
    $start = $request->get('start');
    $end = $request->get('end');
    $agents = RoleHelper::agents($appId, false);

    foreach ($agents as $key => $agent) {
      $agentName[$key] = $agent->name;
      $datas[$key] = DB::table("livechats as l")
        ->join("rooms as r", "l.room_id", "=", "r.id")
        ->where("r.app_id", $appId)
        ->whereDate('l.start_at', '>=', $start)
        ->whereDate('l.start_at', '<=',$end)
        ->where('l.handle_by', $agent->id)->where('l.status', 'resolved')
        ->get();
      // $requestAt[$key] = $datas[$key]["request_at"];
      if($datas[$key]->isEmpty()){
        $avg[$key] = 0;
      } else {
        foreach ($datas[$key] as $key2 => $value) {
          $diff[$key][$key2] = Carbon::parse($value->request_at)->diffInMinutes(Carbon::parse($value->start_at));
        }
        $avg[$key] = round(array_sum($diff[$key]) / count($diff[$key]));
      }
    }
    // return $datas;

    $output["series"]["data"] = $avg;
    $output["categories"] = $agentName;
    return $this->successResponse(["average_first_response_time_per_agent" => $output]);
  }

  public function averageFirstResponseTimePerAgent(Request $request, $appId)
  {
    $start = $request->get('start');
    $end = $request->get('end');
    $agentId = $request->get('agentId');
    $agents = RoleHelper::agents($appId, false);

    if ($agentId) {
      $agent = RoleHelper::agentId($agentId, $appId);

      $datas = DB::table("time_report_served")
        ->where('app_id', $appId)
        ->where('agent_id', $agentId)
        ->whereDate('created_at', '>=', $start)
        ->whereDate('created_at', '<=',$end)
        ->get();

      if($datas->isEmpty()){
        $avg = 0;
      } else {
        foreach ($datas as $key2 => $value) {
          // $diff[$key2] = Carbon::parse($value->request_at)->diffInMinutes(Carbon::parse($value->served_at));
          $diff[$key2] = (int)($value->diff);
        }
        $avg = round(array_sum($diff) / count($diff));
      }

      $output["series"]["data"] = [$avg];
      $output["categories"] = [$agent[0]->name];
      return $this->successResponse(["average_first_response_time_per_agent" => $output]);
    }

    if (count($agents) !== 0) {
      foreach ($agents as $key => $agent) {
        $agentName[$key] = $agent->name;
        $datas[$key] = DB::table("time_report_served")
          ->where('app_id', $appId)
          ->where('agent_id', $agent->id)
          ->whereDate('created_at', '>=', $start)
          ->whereDate('created_at', '<=',$end)
          ->get();

        if($datas[$key]->isEmpty()){
          $avg[$key] = 0;
        } else {
          foreach ($datas[$key] as $key2 => $value) {
            $diff[$key][$key2] = (int) $value->diff;
          }
          $avg[$key] = round(array_sum($diff[$key]) / count($datas[$key]));
        }
      }
      // return $diff;
      $output["series"]["data"] = $avg;
      $output["categories"] = $agentName;
      return $this->successResponse(["average_first_response_time_per_agent" => $output]);
    }
    return $this->successResponse(["average_first_response_time_per_agent" => null]);
  }

  public function getTopStory(Request $request, $appId)
  {
    $integration = Integration::where('app_id', $appId)->where('channel_id', 6)->first();

    $errortotalMessageAgent = 'Please connect your own bot';
    if (!$integration) {
      return $this->successResponse(null, $error);
    } else if ($integration->status == false) {
      return $this->successResponse(null, $error);
    }

    $botId = $integration->integration_data['botId'];

    $totalRequest = DB::table('bot.requests as r')
      ->join('bot.stories as s', 'r.story_id', '=', 's.id')
      ->join('app.users as u', 'r.user_id', '=', 'u.id')
      ->where('u.app_id', '=', $appId)
      ->where('s.bot_id', $botId)
      ->select('r.id')
      ->count();

    $data = DB::table('bot.stories as s')
      ->join('bot.requests as r', 's.id', 'r.story_id')
      ->join('app.users as u', 'u.id', '=', 'r.user_id')
      ->where('s.bot_id', '=', $botId)
      ->where('u.app_id', '=', $appId)
      ->select('s.title as story', DB::raw("count('r.story_id') as total"))
      ->groupBy('s.title')
      ->orderBy('total', 'DESC')
      //   ->take()
      ->get();

    return response()->json([
      "success" => true,
      "message" => null,
      "data" => [
        'total_request' => $totalRequest,
        'data' => $data
      ]
    ]);
  }

  public function getTopStory2(Request $request, $appId)
  {
    $integration = Integration::where('app_id', $appId)->where('channel_id', 6)->first();
    $error = 'Please connect your own bot';
    if (!$integration) {
      return $this->successResponse(null, $error);
    } else if ($integration->status == false) {
      return $this->successResponse(null, $error);
    }

    $start = $request->get('start');
    $end = $request->get('end');

    $botId = $integration->integration_data['botId'];
    //get bot id

    // get total story
    $totalStory = DB::table(env("DB_BOT_SCHEMA") . ".stories")->where("bot_id", $botId)->count();
    $totalRequest = DB::table('bot.requests as r')
      ->join('bot.stories as s', 'r.story_id', '=', 's.id')
      ->where('s.bot_id', $botId)
      ->whereDate('r.created_at', '>=', $start)
      ->whereDate('r.created_at', '<=', $end)
      ->count();
    $story = DB::table(env("DB_BOT_SCHEMA") . ".stories")->where("bot_id", $botId)->get();
    $getTopRequestStory = $datas = [];

    foreach ($story as $key => $value) {
      $getTopRequestStory =  DB::table("bot.requests as r")
        // ->join("bot.interactions as i", "r.story_id", "=", "i.id")
        ->join("bot.stories as s", "s.id", "=", "r.story_id")
        ->join('app.users as u', 'u.id' ,'=', 'r.user_id')
        ->where('u.app_id', '=', $appId)
        ->where('r.story_id', $value->id)
        ->count();
      // ->take(1)->get();

      $datas[] = [
        'story' => $value->title,
        'total' => $getTopRequestStory
      ];
    }

    $collectDatas = collect($datas);
    $sortDatas =  $collectDatas->sortByDesc('total')->values()->take(35);
    return response()->json([
      "success" => true,
      "message" => null,
      "data" => [
        'appId' => $appId,
        'total_story' => $totalStory,
        'total_request' => $totalRequest,
        'story' => $story,
        'data' => $sortDatas
      ]
    ]);
  }

  public function _getActiveUser($appId, $start, $end)
  {
    if(!$end) {
      $end = $start;
    }

    // $from = Carbon::now('Asia/Jakarta')->month;
    // $to = Carbon::today('Asia/Jakarta')->subMonth()->toDateTimeString();

    // $lastMonth = Carbon::now('Asia/Jakarta')->subMonth();
    // $now = Carbon::now('Asia/Jakarta');

    $data = Message::whereHas('room', function ($q) use ($appId) {
      $q->where('app_id', $appId);
    })
      ->where('messageable_type', 'user')
      ->whereDate('created_at', '>=', $start)
      ->whereDate('created_at', '<=', $end)
      ->select('messageable_id')
      ->get()->groupBy('messageable_id')->count();

    return $data;
  }
  public function getMau(Request $request, $appId)
  {
    $start = $request->get('start') == null ? '' : $request->get('start');
    $end = $request->get('end') == null ? $request->get('start') : $request->get('end');

    $data = DB::table("messages as m")
      ->join("rooms as r", 'r.id', '=', 'm.room_id')
      ->where('r.app_id', $appId)
      ->where('m.messageable_type', 'user')
      ->whereDate('m.created_at', '>=', $start)
      ->whereDate('m.created_at', '<=', $end)
      ->select('messageable_id')
      ->get()->groupBy('messageable_id')->count();
    return $data;
  }
  public function getAgentList($appId)
  {
    return $agent = RoleHelper::agents($appId, false);
  }

}
