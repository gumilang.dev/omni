<?php

namespace App\Modules\Integration\Controllers;

use Illuminate\Http\Request;
use Hashids\Hashids;
use Symfony\Component\HttpFoundation\Response;
use App\Modules\Log\Helpers\WebhookHelper;
use App\Modules\Whatsapp\Services\WaOfficialApiController;

use App\Modules\Facebook\Services\FacebookApiController;
use App\Modules\Telegram\Services\TelegramApiController;

use App\Http\Controllers\ApiController;

use App\Modules\Role\Helpers\RoleHelper;

use App\Modules\Integration\Models\Integration;
use App\Modules\Channel\Models\Channel;
use App\Modules\App\Models\App;
use App\Modules\Webchat\Models\WebchatStyle;
use Abraham\TwitterOAuth\TwitterOAuth;

class IntegrationController extends ApiController
{
  protected $facebookController;
  protected $telegramController;

  public function __construct(Request $request)
  {
    $this->facebookController = new FacebookApiController();
    $this->telegramController = new TelegramApiController([
        'botToken' => $request->botToken
    ]);
    $this->hashids = new Hashids('', 6);
  }

  public function index($appId)
  {
    $channel = Channel::with(['integration' => function ($query) use ($appId) {
      $query->where(['app_id' => $appId]);
    }])->get();
    $response = [
      'status' => "success",
      'data' => $channel
    ];
    return response()->json($response, Response::HTTP_OK);
  }

  public function store(Request $request, $appId)
  {
    switch ($request->channelName) {
      case 'mobile':
        $listToSave = ['status', 'serverKey'];
        break;
      case 'bot':
        $listToSave = ['botId', 'botName'];
        break;
      case 'line':
        $listToSave = ['accessToken', 'secretKey', 'webhookUrl', 'registerUrl'];
        break;
      case 'twitter':
        $listToSave = ['apiKey', 'apiKeySecret', 'bearerToken', 'accessToken', 'accessTokenSecret', 'environtment', 'webhookId'];
        break;
      case 'whatsapp':
        $listToSave = ['apiKey', 'webhookUrl', 'apiService', 'scenarioKey', 'whatsappBaseUrl', 'username', 'password', 'token', 'namespaceHsm', 'secretKey', 'clientKey', 'clientId', 'projectId'];
        break;
      case 'webchat':
        $listToSave = ['initScript', 'pluginScript'];
        $checkTrash = WebchatStyle::onlyTrashed()->where('app_id', $appId)->get();
        if (count($checkTrash) > 0) {
          WebchatStyle::onlyTrashed()->where('app_id', $appId)->restore();
        } else {
          $customWebchat = WebchatStyle::where('app_id', $appId)->first();
          if(!$customWebchat){
            $style = new WebchatStyle;
            $style->app_id                    = $appId;
            $style->avatar                    = "/images/pictures/webchat/lennalogoblue.png";
            $style->background_value          = "#F4F4F4";
            $style->background_type           = "color";
            $style->branding                  = "yes";
            $style->bubble_self               = "#003275";
            $style->bubble_other              = "#e5e5e5";
            $style->text_self                 = "#FFFFFF";
            $style->text_other                = "#343a40";
            $style->header_logo               = "/images/pictures/webchat/lennalogo.png";
            $style->header_text               = "Hi, Welcome to Lenna.ai";
            $style->header_background_value   = "/images/pictures/webchat/header.jpeg";
            $style->header_background_type    = "image";
            $style->launcher_open             = '#186AB3';
            $style->launcher_open_type        = 'color';
            $style->launcher_close            = '#186AB3';
            $style->launcher_close_type       = 'color';
            $style->credit_image              = '/images/pictures/webchat/lenna_credit.png';
            $style->created_by                = $request->decodedUserId;
            $style->save();
          }
        }
        break;
      case 'telegram':
        $isPageIntegrated = Integration::where([
            ['integration_data->botToken', $request->botToken],
            ['app_id', '!=', $appId],
            ['status', true]
        ])->first();
        if ($isPageIntegrated) {
            return $this->errorResponse('Bot already integrated with another App', 'Bot already integrated with another App');
        } else {
            $listToSave = ['botToken', 'botUsername', 'botName', 'webhookUrl'];
        // subscribe new page
            $response = $this->telegramController->setBotWebhook($request->botToken, $request->webhookUrl);
        }
        break;
      case 'facebook':
        $isPageIntegrated = Integration::where([
          ['integration_data->pageId', $request->pageId],
          ['status', true],
          ['app_id', '!=', $appId]
        ])->first();
        if ($isPageIntegrated) {
          return $this->errorResponse(null, 'FB Page already integrated with another App');
        } else {
          $listToSave = ['pageId', 'pageAccessToken', 'pageList', 'UserAccessToken'];
          // is already integrated unsubscribe old page
          $integration = Integration::where(['app_id' => $request->decodedAppId, 'status' => true])->whereHas('channel', function ($query) use ($request) {
              $query->where('name', '=', "facebook");
          })->first();
          if ($integration) {
            $this->facebookController->unsubscribeApp($integration->integration_data['pageId'], $integration->integration_data['pageAccessToken']);
          }
          // subscribe new page
          $subscribeApp = $this->facebookController->subscribeApp($request->pageId, $request->pageAccessToken);
          if (isset($subscribeApp['error']['message'])) {
            return $this->errorResponse(null, 'You dont have permission to acces this page');
            // return $this->errorResponse(null, $subscribeApp['error']['message']);
          }
        }
        break;
      case 'mobile-omni':
        $listToSave = ['status'];
        break;
      default:
        break;
    }
    $integrationData = array_only($request->all(), $listToSave);
    $data = [
      'integration_data' => $integrationData,
      'app_id' => $request->decodedAppId,
      'status' => $request->status,
    ];
    $integrationData = Integration::whereHas('channel', function ($query) use ($request) {
      $query->where('name', '=', $request->channelName);
    })->updateOrCreate(
      ['app_id' => $request->appId],
      $data
    );
    $channel = Channel::where("name", "=", $request->channelName)->first();
    $channel->integration()->save($integrationData);
    return $this->successResponse(['channel' => $channel], $request->channelName . ' connected, you are ready to go!');
  }

  public function update(Request $request, Integration $integration, $appId)
  {
    // return response()->json(['message'=>'test'], Response::HTTP_OK);
    $data = [
      'status' => $request->status
    ];

    $integration = Integration::where(['app_id' => $appId])->whereHas('channel', function ($query) use ($request) {
      $query->where('name', '=', $request->channelName);
    })->first();

    if (!$integration) {
      $response = [
        'success' => false,
        'status' => "failed",
        'message' => "You must connect channel first",
        'data' => $integration,
      ];
      return response()->json($response, Response::HTTP_FORBIDDEN);
    }
    // check for channel available
    if ($isNotAvailable = $this->isNotAvailable($request, $appId)) {
      return $this->errorResponse(null, $isNotAvailable['message']);
    }

    // do update
    switch ($request->channelName) {
      case 'facebook':
        // if FB disconect channel, unsubscribe old page from app
        if ($request->status == false) {
            $this->facebookController->unsubscribeApp($integration->integration_data['pageId'], $integration->integration_data['pageAccessToken']);
        }
        $listToSave = ['pageId', 'pageAccessToken', 'pageList', 'UserAccessToken'];
        $integrationData = array_only($request->all(), $listToSave);
        $integration->update(['integration_data' => $integrationData]);
        break;
      case 'webchat':
        if ($request->status == false) {
          WebchatStyle::where('app_id', $appId)->delete();
        }
        break;
      case 'telegram':
        if ($request->status == false) {
            $this->telegramController->deleteWebhook($integration->integration_data['webhookUrl']);
        }
        break;
      case 'bot':
        if ($request->status == false) {
          // if dont have role to current integrated bot
          if (!RoleHelper::haveRoleBot(['userId' => $request->decodedUserId, 'appId' => $appId])) {
            return $this->errorResponse(null, 'Access not permitted to current integrated bot');
          }
        }
        break;
      default:
        break;
    }

    $integration->update($data);
    $integration = $integration;

    $response = [
      'success' => true,
      'status' => "success",
      'message' => $integration->status ? 'Channel connected' : 'Channel disconnected',
      'data' => $integration,
    ];
    return response()->json($response, Response::HTTP_OK);
  }


  public function isNotAvailable($request, $appId)
  {
    switch ($request->channelName) {
      case 'facebook':
        if ($request->status == 'true') {
          // check is page already integrated with another app & only check when user activating channel
          $integration = Integration::where(['app_id' => $appId])->whereHas('channel', function ($query) use ($request) {
            $query->where('name', '=', $request->channelName);
          })->first();
          $isPageIntegrated = Integration::where([
            ['integration_data->pageId', $integration->integration_data['pageId']],
            ['status', true]
          ])->first();
          if ($isPageIntegrated) {
            return ['message' => 'Page already integrated with another App'];
          }
        }
        break;
    }
    return false;
  }

  public function getLongLivedUserToken(Request $request)
  {
    $response = $this->facebookController->getLongLivedToken($request->userAccessToken);
    return response()->json($response, Response::HTTP_OK);
  }

  public function deleteAllIntegration(Request $request, $appId)
  {
    // unsubs facebook
    $fbIntegration = Integration::where(['app_id' => $appId])->whereHas('channel', function ($query) use ($request) {
      $query->where('name', '=', 'facebook');
    })->first();
    if ($fbIntegration) {
      $this->facebookController->unsubscribeApp($fbIntegration->integration_data['pageId'], $fbIntegration->integration_data['pageAccessToken']);
    }
    $app = App::find($appId);
    $app->integrations()->update(['status' => false]);
    return $this->successResponse(null, 'all integrations deleted');
  }

  public function setWebhookTwitter(Request $request, $appId)
  {
    $integration = Integration::where([['app_id', "=", $appId], ['status', "=", true], ['channel_id', "=", 9]])->first();
    if (!$integration) {
      return $this->errorResponse('Integration not found','Integration not found');
    } else {
      $connection = new TwitterOAuth(
        $integration['integration_data']['apiKey'],
        $integration['integration_data']['apiKeySecret'],
        $integration['integration_data']['accessToken'],
        $integration['integration_data']['accessTokenSecret']
      );
      $url = $request->webhookUrl;
      $setWebhook = $connection->post("account_activity/all/".$request->environtment."/webhooks", ["url" => $url]);

      if (!empty($setWebhook->errors[0])) {
        return $this->errorResponse($setWebhook->errors[0]->message);
      } else {
        $subscribe = $connection->post("account_activity/all/". $request->environtment ."/subscriptions"); //subscribe user to webhook
        return $this->successResponse($setWebhook, 'Webhook subscribed');
      }
    }
  }

  public function deleteWebhookTwitter(Request $request, $appId)
  {
    $integration = Integration::where([['app_id', "=", $appId], ['status', "=", true], ['channel_id', "=", 9]])->first();
    if (!$integration) {
      return $this->errorResponse('Integration not found','Integration not found');
    } else {
      $connection = new TwitterOAuth(
        $integration['integration_data']['apiKey'],
        $integration['integration_data']['apiKeySecret'],
        $integration['integration_data']['accessToken'],
        $integration['integration_data']['accessTokenSecret']
      );
      // delete webhook
      $connection->setTimeouts(10, 15);
      $deleteWebhook = $connection->delete("account_activity/all/".$request->environtment."/webhooks/".$request->webhookId);
      if ($deleteWebhook == null) {
        return $this->successResponse($deleteWebhook, 'deleting webhook');
      } else {
        return $this->errorResponse($connection->getLastHttpCode(), $deleteWebhook->errors[0]->message);
      }
    }
  }

  public function addWhatsappAccount(Request $request, $appId)
  {
    $listToSave = ['whatsappName', 'whatsappNumber','apiKey', 'webhookUrl', 'apiService', 'scenarioKey', 'whatsappBaseUrl', 'username', 'password', 'token', 'namespaceHsm', 'secretKey', 'clientKey', 'clientId', 'projectId'];
    $integrationData = array_only($request->all(), $listToSave);
    // return $integrationData;
    $data = [
      'app_id' => $appId,
      'channel_id' => 4,
      'integration_data' => $integrationData,
      'status' => $request->status,
    ];

    $integration = Integration::create($data);

    return $this->successResponse($integration, 'Whatsapp account added successfully');
  }

  public function getWhatsappAccount(Request $request, $appId)
  {
    if ($request->status) {
      $integration = Integration::where(['app_id' => $appId, 'channel_id' => 4 , 'status' => $request->status])
                                ->orderBy('id', 'ASC')
                                ->get();
      return $this->successResponse($integration, null);
    }
    $integration = Integration::where(['app_id' => $appId, 'channel_id' => 4])
                              ->orderBy('id', 'ASC')
                              ->get();
    return $this->successResponse($integration, null);
  }

  public function updateWhatsappAccount(Request $request, $appId)
  {
    $integration = Integration::find($request->id);

    if ($request->updateStatus) {
      $integration->status = $request->status;
      $integration->save();
      $message = $integration->status ? 'Connected' : 'Disconnected';
      return $this->successResponse($integration, $message);
    }
    // return $integration;
    $listToSave = ['whatsappName', 'whatsappNumber','apiKey', 'webhookUrl', 'apiService', 'scenarioKey', 'whatsappBaseUrl', 'username', 'password', 'token', 'namespaceHsm', 'secretKey', 'clientKey', 'clientId', 'projectId'];
    $integrationData = array_only($request->all(), $listToSave);

    if ($integration == null) {
      return $this->errorResponse('Integration not found', 'Integration not found');
    }

    $integration->integration_data = $integrationData;
    $integration->status = $request->status;
    $integration->save();
    $message = $integration->status ? 'Channel connected' : 'Channel disconnected';
    return $this->successResponse($integration, $message);
  }

  public function setWaWebhook(Request $request, $appId)
  {
    $integrationId = $request->integrationId;
    if (!$integration = WebhookHelper::getIntegrationDataById('whatsapp', $appId, $integrationId)) {
      return $this->errorResponse(null, 'wrong app / integration not active');
    }
    $data = [ "integration" => $integration ];
    $WaOfficialApiController = new WaOfficialApiController($data);
    $response = $WaOfficialApiController->setWebhook($request->webhookUrl);

    $message = 'webhook already set';
    if (isset($response->details)) {
      $message = $response->details;
    }
    return $this->successResponse($response, $message);
  }

  public function _getIntegrationdataById($appId, $channelId)
  {
    $integration = Integration::where('app_id', $appId)->where('channel_id', $channelId)->get();

    return $integration;
  }


}
