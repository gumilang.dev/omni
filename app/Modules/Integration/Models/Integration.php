<?php

namespace App\Modules\Integration\Models;

use Illuminate\Database\Eloquent\Model;

class Integration extends Model
{
    protected $connection = 'pgsql';

    protected $guarded = [];
    protected $casts = [
        'integration_data' => 'array',
    ];
    public function channel()
    {
        return $this->belongsTo("App\Modules\Channel\Models\Channel");
    }

    public function App()
    {
        return $this->belongsTo("App\Modules\App\Models\App");
    }
}
