<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

use App\Modules\User\Models\UserRoleChannel;
use App\Modules\Role\Models\Role;

use OwenIt\Auditing\Contracts\Auditable;

class UserRole extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    protected $connection = 'auth';
    protected $table = 'user_roles';
    // protected $visible = ['role', 'user_platform'];
    protected $hidden = [
      'created_by', 'updated_by', 'deleted_by', 'updated_at', 'deleted_at','platform_id'
    ];
    protected $appends = [
      'role',
      // 'user_channel'
    ];
    protected $fillable = ['role_id'];
    public function userPlatform()
    {
        return $this->hasOne('App\Modules\User\Models\UserPlatform', 'id', 'user_id');
    }
    public function role()
    {
        return $this->belongsTo('App\Modules\Role\Models\Role');
    }
    public function getRoleAttribute()
    {
        $role = Role::where('id', $this->attributes['role_id'])->first();
        return $role;
    }
    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\UserPlatform');
    }
    // public function getUserChannelAttribute()
    // {
    //     $channel = UserRoleChannel::where('user_role_id', $this->attributes['id'])->with('channel')->get();
    //     return $channel;
    // }
    public function userRoleChannel()
    {
      return $this->hasMany('App\Modules\User\Models\UserRoleChannel', 'user_role_id');
    }

    // public function messageUserPlatforms(){
    //     return $this->belongsTo('App\Modules\Message\Models\Message');
    // }
}
