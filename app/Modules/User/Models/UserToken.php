<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class UserToken extends Model
{
    protected $connection = 'pgsql';

    protected $guarded = [];

}
