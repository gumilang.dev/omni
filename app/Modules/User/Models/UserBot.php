<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class UserBot extends Model
{
    protected $connection = 'bot';
    protected $table = 'user_bots';
    protected $hidden = ['id', 'bot_id', 'user_id', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\UserPlatform', 'user_id', 'id');
    }
    public function bot()
    {
        return $this->hasOne('App\Modules\Bot\Models\Bot', 'id', 'bot_id');
    }
}
