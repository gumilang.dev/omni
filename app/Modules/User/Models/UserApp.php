<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\Role\Models\Role;

class UserApp extends Model
{
    use SoftDeletes;
    protected $table = 'app_user_platform';
    protected $appends = ['role'];
    protected $hidden = ['role_id'];
    protected $fillable = ['role_id'];

    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\UserPlatform', 'user_id', 'id');
    }
    public function app()
    {
        return $this->hasOne('App\Modules\App\Models\App', 'id', 'app_id');
    }
    public function getRoleAttribute()
    {
        $role = Role::where('id', $this->attributes['role_id'])->first();
        return $role;
    }
    public function role()
    {
      return $this->belongsTo('App\Modules\Role\Models\Role', 'role_id');
    }
    public function userChannels()
    {
      return $this->hasMany('App\Modules\User\Models\UserRoleChannel', 'app_user_platform_id');
    }
}
