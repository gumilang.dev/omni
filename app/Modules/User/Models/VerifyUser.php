<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class VerifyUser extends Model
{
    use Notifiable;
    protected $connection = 'auth';
    protected $table = 'verify_users';
    protected $fillable = [
      'id', 'email', 'token'
    ];

    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\UserPlatform');
    }
}
