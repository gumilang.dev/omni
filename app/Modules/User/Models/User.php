<?php

namespace App\Modules\User\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hashids\Hashids;
use App\Modules\Mobile\Services\BackendService;
use App\Modules\AdditionalInformation\Model\AdditionalInformation;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    protected $connection = 'backend';
    protected $table = 'users';
    // protected $fillable = ['channel_id'];
    protected $guarded = [];

    protected $hidden = [
        'password', 'remember_token', 'old_password','fields'
    ];

    // protected $appends = ['hashed_id', 'assigned_story_groups', 'email', 'phone', 'additional_information'];
    protected $appends = ['hashed_id', 'assigned_story_groups', 'additional_information'];

    public function __construct($attribute = [])
    {
        parent::__construct($attribute);
        $this->table = env('DB_BACKEND_SCHEMA') . ".users";
    }
    public function storyGroups()
    {
        return $this->belongsToMany('App\Modules\Bot\Models\StoryGroup', env('DB_SCHEMA') . '.story_group_user');
    }
    public function getAssignedStoryGroupsAttribute()
    {
        return $this->storyGroups->pluck('id');
    }
    public function getAdditionalInformationAttribute() {
        return AdditionalInformation::where(["user_id" => $this->attributes['id']])->get();
    }
    // public function storyGroups()
    // {
    //     return $this->belongsToMany(
    //         'App\Modules\Bot\Models\StoryGroup',
    //         'story_group_user',
    //         "user_id",
    //         "story_group_id"
    //     );
    // }
    // M to M polymorph
    public function participatedIn()
    {
        return $this->morphToMany('App\Modules\Room\Models\Room', 'participable');
    }
    // 1 to M polymorph
    public function messages()
    {
        return $this->morphMany('App\Modules\Message\Models\Message', 'messageable');
    }
    public function messageStatus()
    {
        return $this->morphMany('App\Modules\Message\Models\MessageStatus', 'messageable');
    }
    // 1 to M
    public function rooms()
    {
        return $this->hasMany('App\Modules\Room\Models\Room', 'created_by');
    }
    public function broadcastMessages()
    {
        return $this->hasMany('App\Modules\BroadcastMessage\Models\BroadcastMessage');
    }
    public function userToken()
    {
        return $this->hasMany('App\Modules\User\Models\UserToken');
    }
    // 1 to M inverse
    public function channel()
    {
        return $this->belongsTo('App\Modules\Channel\Models\Channel');
    }

    public function app()
    {
        return $this->belongsTo('App\Modules\App\Models\App');
    }

    public function transactions()
    {
        return $this->hasMany('App\Modules\Transaction\Models\Transaction');
    }
    public function getHashedIdAttribute()
    {
        $backendHashids = new Hashids(env("SECRET_KEY"), '6');
        $hashedId = $backendHashids->encode($this->id);
        return $hashedId;
    }

    // mutator
    // public function setEmailAttribute($value)
    // {
    //     $email = strtolower($value);
    //     $encryptedEmail = BackendService::encryptText(env("ENCRYPT_PROFILE_KEY"), $email);
    //     $this->attributes['email'] = $encryptedEmail;
    // }
    // public function setPhoneAttribute($value)
    // {
    //     $encryptedPhone = BackendService::encryptText(env("ENCRYPT_PROFILE_KEY"), $value);
    //     $this->attributes['phone'] = $encryptedPhone;
    // }

    // assesor
    // public function getEmailAttribute()
    // {
    //     $decryptedEmail = BackendService::decryptText(env("ENCRYPT_PROFILE_KEY"), $this->attributes['email']);
    //     return $decryptedEmail;
    // }
    // public function getPhoneAttribute()
    // {
    //     $decryptedPhone = BackendService::decryptText(env("ENCRYPT_PROFILE_KEY"), $this->attributes['phone']);
    //     return $decryptedPhone;
    // }

    public function apiLogs()
    {
        return $this->morphMany('App\Modules\Log\Models\ApiLog', 'userable');
    }
}
