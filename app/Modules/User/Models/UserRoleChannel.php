<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class UserRoleChannel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public $timestamps = true;
    protected $table = 'user_role_channels';
    protected $connection = 'pgsql';
    protected $hidden = [
      'channel_id',
      'created_at',
      'updated_at',
    ];
    protected $fillable = [
      'user_role_id', 'channel_id', 'app_user_platform_id'
    ];

    // public function userRole()
    // {
    //     return $this->hasOne('App\Modules\User\Models\UserRole', 'id', 'user_role_id');
    // }
    public function channel()
    {
        // return $this->hasOne('App\Modules\Channel\Models\Channel', 'id', 'channel_id');
        return $this->belongsTo('App\Modules\Channel\Models\Channel', 'channel_id');
    }
    public function userApp()
    {
        return $this->belongsTo('App\Modules\User\Models\UserApp', 'app_user_platform_id');
    }
}
