<?php

namespace App\Modules\User\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Hashids\Hashids;
use OwenIt\Auditing\Contracts\Auditable;

// this model related to user from auth schema
class UserPlatform extends Authenticatable implements Auditable
{
    use Notifiable, HasApiTokens;
    use \OwenIt\Auditing\Auditable;
    protected $connection = 'auth';
    protected $table = 'users';
    protected $hidden = [
      'password', 'remember_token', 'updated_at'
    ];
    protected $appends = [
      'hashed_id'
    ];
    public function participatedIn()
    {
        return $this->morphToMany('App\Modules\Room\Models\Room', 'participable');
    }
    public function messages()
    {
        return $this->morphMany('App\Modules\Message\Models\Message', 'messageable');
    }
    public function apps()
    {
        return $this->belongsToMany('App\Modules\App\Models\App', 'app_user_platform', 'user_id', 'app_id');
    }
    public function getHashedIdAttribute()
    {
        $hashids = new Hashids('', '6');
        $hashedId = $hashids->encode($this->id);
        return $hashedId;
    }

    /* Deprecated */
    public function userRole()
    {
        return $this->hasOne('App\Modules\User\Models\UserRole', 'user_id');
    }
    public function userRoles(){
      return $this->hasOne('App\Modules\User\Models\UserRole');
    }
    /* End of Deprecated */

    public function role()
    {
        return $this->hasOne('App\Modules\Role\Models\Role' ,'id');
        // return $this->belongsTo('App\Modules\Role\Models\Role', 'id', 'role_id');
    }

    public function apiLogs()
    {
        return $this->morphMany('App\Modules\Log\Models\ApiLog', 'userable');
    }

    public function agent()
    {
        return $this->belongsToMany('App\Models\Group', 'agent_groups','user_id','group_id')->withPivot('value');
    }

    public function bots()
    {
        return $this->belongsToMany('App\Modules\Bot\Models\Bot', 'user_bots', 'user_id', 'bot_id');
    }
}
