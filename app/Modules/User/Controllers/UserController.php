<?php

namespace App\Modules\User\Controllers;
ini_set("memory_limit", "-1");
use Illuminate\Http\Request;
use App\Modules\User\Models\User;
use Carbon\Carbon;
use App\Modules\Channel\Models\Channel;
use App\Http\Controllers\ApiController;
use App\Modules\Bot\Models\StoryGroup;
use App\Modules\Message\Models\Message;
use App\Modules\Room\Models\Room;
use Hoyvoy\CrossDatabase\Eloquent\Model;
use Carbon\CarbonPeriod;
use DB;
use DateTime;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Modules\AdditionalInformation\Model\AdditionalInformation;

class UserController extends ApiController
{

    public function index(Request $request, $appId)
    {
      $channel    = $request->get('channel');
      $filterText = $request->get('filterText');
      $sort       = $request->get('sort');
      $per_page   = $request->get('per_page');

      $user       = User::with('channel')->where('app_id', $appId)->orderBy("created_at", "DESC")->get();

      if ($channel) {
        $user = $user->whereIn('channel.name', $channel)->values();
      }

      if ($filterText) {
        $user = $user->filter(function ($item) use ($filterText) {
            return false !== stristr($item->name, $filterText) || false !== stristr($item->email, $filterText) || false !== stristr($item->phone, $filterText);
        })->values();
      }

      if ($sort) {
        $sort = explode('|', $sort);
        if ($sort[1] == 'desc') {
          $user = $user->sortByDesc($sort[0], SORT_NATURAL|SORT_FLAG_CASE);
        }else{
          $user = $user->sortBy($sort[0], SORT_NATURAL|SORT_FLAG_CASE);
        }
      }

      if (!$per_page) {
        $per_page = 15;
      }

      $data = $this->paginate($user, $per_page);

      $data->appends($request->only(['filterText','channel','sort']));

      return response()->json($data);
    }





    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:50',
            'email' => 'required|email|unique:users,email',
        ]);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt(str_random()),
        ]);
        return response()->json($user);
    }





    public function edit($id)
    {
        return $id;
    }


    public function update(Request $request, $appId, $userId)
    {
        $backendSchema = env("DB_BACKEND_SCHEMA");
        $validatedData = $request->validate([
            // 'email' => "required|unique:$backendSchema.users,email," . $request->id,
            'name' => 'required',
            'nickname' => 'required',
        ]);

        $data = [
            'name' => $request->name,
            'nickname' => $request->nickname,
            'phone' => $request->phone,
            'email_verified_at' => Carbon::now()->toDateTimeString(),
            'phone_verified_at' => Carbon::now()->toDateTimeString(),
            'gender' => $request->gender ?? 'm',
            'phone' => $request->phone,
        ];

        $additional_information = $request->post("additional_information");
        foreach($additional_information as $each_additional_information) {
          AdditionalInformation::where([
              "id" => $each_additional_information["id"]
            , "user_id" => $userId]
            )->update([
              "title" => $each_additional_information["title"],
              "description" => $each_additional_information["description"]
            ]);
        }
        $user = User::where('id', $userId)->update($data);
        $user = User::find($userId);
        $user->storyGroups()->sync($request->assigned_story_groups);

        return $this->successResponse(['user' => $user], 'user updated');

    }


    public function destroy($appId, $userId)
    {
        $user = User::findOrFail($userId);
        $user->delete();
        return $this->successResponse('user deleted');

    }

    public function getUserList(Request $request, $appId)
    {
        if ($request->filter == '') {
            return [];
        }
        $users = User::where(['app_id' => $appId])->where('email', 'like', $request->filter . "%");

        return $users->get();
    }

    public function getWaUserList24Hour(Request $request, $appId) {
        $data = DB::table("app.users as u")
                    ->join("rooms as r", 'r.created_by', '=', 'u.id')
                    ->join('messages as m', 'm.room_id', '=', 'r.id')
                    ->where('u.integration_id', $request->integrationId)
                    ->where(function ($q) use ($request) {
                        $q->where('u.email', 'ILIKE', '%' . $request->filter . '%')->orWhere('u.name', 'ILIKE', '%' . $request->filter . '%');
                        })
                    ->where('m.messageable_type', 'user')
                    ->where('r.app_id', $appId)
                    ->whereDate('m.created_at', '>=' , Carbon::now()->subHours(23))
                    ->select('u.phone as number', 'u.name', 'u.id',DB::raw("count('u.phone') as total"))
                    ->groupBy('u.phone', 'u.name', 'u.id')
                    ->get();
         return $data;
    }

    public function getAllWaUserListLast24Hour(Request $request, $appId)
    {
        $data = DB::table("app.users as u")
                    ->join("rooms as r", "r.created_by", "=", "u.id")
                    ->join("messages as m", "m.room_id", "=", "r.id")
                    ->where("u.integration_id", $request->integrationId)
                    ->where("m.messageable_type", "user")
                    ->where("r.app_id", $appId)
                    ->whereDate("m.created_at", ">=", Carbon::now()->subHours(24))
                    ->select("u.phone as number", "u.name", "u.id", DB::raw("count('u.phone') as total"))
                    ->groupBy("u.phone", "u.name", 'u.id')
                    ->get();
            return $data;
    }

    public function getWaUserList(Request $request, $appId)
    {
        if ($request->filter == '') {
            return [];
        }
        $users = User::where(['app_id' => $appId])->where('email', 'like', $request->filter . "%")->get();
        if($users->isEmpty()){ //if $users == [] atau kosong
            return [];
        } else {
                foreach ($users as $key => $value) {
                    $dataUser[] = preg_replace('/\D/', '', $value->email);
                }
        }
        return $dataUser;

    }

    public function assignStoryGroup(Request $request, $appId)
    {
        $user = User::find($request->userId);
        $user->storyGroups()->sync($request->storyGroups);
        return $this->successResponse(['user' => $user], 'user assigned to bot story');

    }

    public function paginate($items, $perPage, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage)->values(), $items->count(), $perPage, $page, [
          'path' => LengthAwarePaginator::resolveCurrentPath()
        ]);
    }

    public function updateUserInformation(Request $request, $appId, $userId ) {
        $data = [
            'phone' => $request->input('phone'),
            'city' => $request->post("city")
        ];

        $user = User::where('id',$userId)->update($data);
        return $this->successResponse($user,'Email or phone updated');
    }

    public function updateLocation(Request $request) {
      $user = User::find($request['userId']);
      if (!$user) {
        return $this->errorResponse(null, 'User not found');
      }
      $user->location = $request['location'];
      $user->save();
      return $this->successResponse($user->location, 'User location updated');
  }

}
