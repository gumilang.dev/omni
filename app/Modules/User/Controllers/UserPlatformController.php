<?php

namespace App\Modules\User\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Modules\User\Models\UserPlatform;
use App\Modules\User\Models\UserApp;
use App\Modules\User\Models\User;
use App\Modules\User\Models\UserRole;
use App\Modules\Channel\Models\Channel;
use App\Modules\App\Models\App;
use Hashids\Hashids;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class UserPlatformController extends ApiController
{

    public function __construct()
    {
        $this->hashids = new Hashids('', 6);
    }

    // public function index(Request $request, $appId)
    // {
    //     $sort = $request->get('sort');
    //     $filterText = $request->get('filterText');
    //     $per_page = $request->get('per_page');

    //     $user = UserPlatform::with(['apps' => function($q) use ($appId){
    //       $q->where('app_id', $appId);
    //     }, 'userRole.role'])->get();

    //     if ($sort) {
    //         $sort = explode('|', $sort);
    //         $user = $user->orderBy($sort[0], $sort[1]);
    //     }

    //     if ($filterText) {
    //         $user = $user->where(function ($query) use ($filterText) {
    //             $query->where('name', 'ILIKE', '%' . $filterText . '%')->orWhere('email', 'ILIKE', '%' . $filterText . '%');
    //         });
    //     }

    //     if (!$per_page) {
    //         $per_page = 15;
    //     }

    //     $user = $user->filter(function ($value, $key) {
    //       return count($value['apps']) > 0;
    //     })->values()->map(function ($value, $key) {
    //       $map = (object)[
    //           'id' => $value['hashed_id'],
    //           'email' => $value['email'],
    //           'name' => $value['name'],
    //           'role' => (object)[
    //             'id' => array_get($value->toArray(), 'user_role.role.hashed_id'),
    //             'name' => array_get($value->toArray(), 'user_role.role.name'),
    //           ]
    //       ];
    //       return collect($map)->except([
    //         'email_verified_at',
    //         'created_at',
    //         'apps',
    //         'user_role.user_id',
    //         'user_role.role_id',
    //         'user_role.id',
    //         'user_role.role.id',
    //         'user_role.created_at',
    //       ])->toArray();
    //     })->all();

    //     $total = count($user);

    //     $user = new Paginator($user, $total, $per_page, Paginator::resolveCurrentPage(), [
    //         'path' => Paginator::resolveCurrentPath()
    //     ]);

    //     return response()->json($user);
    // }

    public function getUserPlatformByEmail(Request $request){
      if ($request->filter == '') {
          return [];
      }
      $users = UserPlatform::where('email', 'like', $request->filter . "%")->get();

      $users = $users->map(function ($value, $key) {
        $map = (object)[
            'id' => $value['hashed_id'],
            'email' => $value['email'],
            'name' => $value['name']
        ];
        return collect($map)->except([
          'email_verified_at',
          'created_at',
        ])->toArray();
      })->all();

      return $users;
    }

    public function getUserData(Request $request){
      $userPlatform = UserPlatform::where('id', $request->decodedUserId)->first();
      return $this->successResponse($userPlatform);
    }

    public function updateUserData(Request $request){
      // return $request;
      $authSchema = env('DB_AUTH_SCHEMA');
      $validator = Validator::make($request->all(), [
        'firstName' => 'required',
        'lastName' => 'required',
        'username' => 'required|alpha_num|unique:auth.users,username,' . $request->decodedUserId
      ]);

      if ($validator->fails()) {
        if ($validator->errors()->first('firstName')) {
          return $this->errorResponse($validator->errors()->first('firstName'), $validator->errors()->first('firstName'));
        }
        else if($validator->errors()->first('lastName')) {
          return $this->errorResponse($validator->errors()->first('lastName'), $validator->errors()->first('lastName'));
        }
        else if ($validator->errors()->first('username')) {
          return $this->errorResponse($validator->errors()->first('username'), $validator->errors()->first('username'));
        }
      }

      $updateUser = UserPlatform::where('id',$request->decodedUserId)->first();
      if (!$updateUser) {
        return $this->errorResponse(null, 'User not found');
      }

      $updateUser->name = $request->firstName;
      $updateUser->last_name = $request->lastName;
      $updateUser->username = $request->username;
      $updateUser->save();
      return $this->successResponse($updateUser, 'Success Update User');
      // return $request;
    }

    public function uploadImage(Request $request){
      // return $request;
      $name = time() . $request->file('image')->getClientOriginalName();

      $request->image->move(public_path('/upload/avatar'), $name);
      // $path = $request->file('image')->store('uploaded','public');

      $user = UserPlatform::where('id', $request->decodedUserId)->first();
      $oldAvatar = $user->avatar;
      // return $oldAvatar;

      if (!$user) {
        return $this->errorResponse(null, 'User not found');
      }

      // Storage::delete(public_path("/upload/avatar"), $oldAvatar);
      Storage::disk('public_avatar')->delete([$oldAvatar]);

      $user->avatar = $name;
      $user->save();

      return response()->json(['image_url' => env("APP_URL") . "/upload/avatar/" . $name], 200);
    }

    public function getAll()
    {
        $userPlatforms = UserPlatform::all();
        return $this->successResponse(['userPlatforms' => $userPlatforms]);
    }

    public function deleteAccessToApp(Request $request, $appId){
      $userApp = UserApp::find($appId);
      $userApp->delete();
      return $this->successResponse(null, "access to app deleted");
    }
}
