<?php

namespace App\Modules\Plan\Controllers;

use Illuminate\Http\Request;
use App\Modules\Menu\Models\Menu;
use App\Modules\Plan\Models\Plan;
use App\Modules\Role\Models\RoleMenu;
use App\Modules\Menu\Models\Submenu;
use App\Modules\App\Models\App;
use DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;

class PlanController extends ApiController
{

    public function getMenus($appId)
    {
        $app = intval(App::find($appId)->plan_id);
        $menus = Menu::select('id','name','icon','slug','plans')->with(['submenus' => function($q) {
          $q->select('id','menu_id','name','icon','slug','sub_submenu','plans','description')->orderBy('id', 'ASC');
        }])->orderBy('id', 'ASC')->get();
        foreach ($menus as $key => $value) {
          foreach ($value->plans as $k => $v) {
            if ($v == $app){
              unset($menus[$key]);
            }
          }
        }
        // $menus = Menu::select('id','name','icon','slug','plans')
        //   ->whereNotIn('plans', [$app])
        //   ->addSelect(DB::raw("
        //     (
        //       SELECT
        //         json_agg (
        //           json_build_object (
        //             'id', s.id,
        //             'menu_id', s.menu_id,
        //             'name', s.name,
        //             'icon', s.icon,
        //             'slug', s.slug,
        //             'sub_submenu', s.sub_submenu,
        //             'plans', s.plans,
        //             'description', s.description
        //           )
        //         )
        //       FROM omnichannel.submenus as s
        //       JOIN omnichannel.menus as m ON s.menu_id = m.id
        //       WHERE m.id = s.menu_id
        //     ) as submenus
        //   "))
        //   ->orderBy('id', 'ASC')
        //   ->get()
        //   ->map(function($q) use ($app){
        //     $submenus = json_decode($q->submenus);
        //     $q->submenus = collect($submenus)->where('menu_id', $q->id)->sortBy('id')->map(function($s) {
        //       $s->plans = json_decode($s->plans);
        //       return $s;
        //     })->values();
        //     return $q;
        //   });
        return response()->json($menus->values());
    }
}
