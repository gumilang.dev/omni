<?php

namespace App\Modules\Facebook\Helpers;

class FacebookMessageType
{
    protected $chatRoom;

    public function __construct($chatRoom)
    {
        $this->chatRoom = $chatRoom;
    }

    public function text($input = [])
    {
        $output = [
            'messaging_type' => "RESPONSE",
            'recipient' =>
                (object)[
                'id' => $this->chatRoom->created_by->channel_user_id,
            ],
            'message' => (object)[
                'text' => $input['text']
            ]
        ];

        return $output;

    }

    public function image($input = [])
    {
        $output = [
            'messaging_type' => "RESPONSE",
            'recipient' =>
                (object)[
                'id' => $this->chatRoom->created_by->channel_user_id,
            ],
            'message' => (object)[
                'attachment' => (object)[
                    'type' => "image",
                    'payload' => (object)[
                        'url' => $input['imageUrl'] ?? '',
                        'is_reusable' => $input['is_reusable'] ?? false
                    ]
                ]
            ]
        ];

        return $output;
    }

    public function file($input = [])
    {
        $output = [
            'messaging_type' => "RESPONSE",
            'recipient' =>
                (object)[
                'id' => $this->chatRoom->created_by->channel_user_id,
            ],
            'message' => (object)[
                'attachment' => (object)[
                    'type' => "file",
                    'payload' => (object)[
                        'url' => $input['fileUrl'] ?? '',
                        'is_reusable' => $input['is_reusable'] ?? false
                    ]
                ]
            ]
        ];

        return $output;
    }
    public function audio($input = [])
    {
        $output = [
            'messaging_type' => "RESPONSE",
            'recipient' =>
                (object)[
                'id' => $this->chatRoom->created_by->channel_user_id,
            ],
            'message' => (object)[
                'attachment' => (object)[
                    'type' => "audio",
                    'payload' => (object)[
                        'url' => $input['audioUrl'] ?? '',
                        'is_reusable' => $input['is_reusable'] ?? false
                    ]
                ]
            ]
        ];

        return $output;
    }
    public function video($input = [])
    {
        $output = [
            'messaging_type' => "RESPONSE",
            'recipient' =>
                (object)[
                'id' => $this->chatRoom->created_by->channel_user_id,
            ],
            'message' => (object)[
                'attachment' => (object)[
                    'type' => "video",
                    'payload' => (object)[
                        'url' => $input['videoUrl'] ?? '',
                        'is_reusable' => $input['is_reusable'] ?? false
                    ]
                ]
            ]
        ];

        return $output;
    }

    public function confirm($input = [])
    {
        $output = [
            'messaging_type' => "RESPONSE",
            'recipient' =>
                (object)[
                'id' => $this->chatRoom->created_by->channel_user_id,
            ],
            'message' => (object)[
                'attachment' => (object)[
                    'type' => "template",
                    'payload' => (object)[
                        'template_type' => 'button',
                        'text' => $input['text'] ?? '',
                        'buttons' => [
                            (object)[
                                'type' => 'postback',
                                'title' => $input['actions'][0]->label ?? 'Ya',
                                'payload' => $input['actions'][0]->text ?? 'ya',
                            ],
                            (object)[
                                'type' => 'postback',
                                'title' => $input['actions'][1]->label ?? 'Tidak',
                                'payload' => $input['actions'][1]->text ?? 'tidak',
                            ]
                        ]
                    ]
                ]
            ]
        ];

        return $output;
    }
    public function buttons($input = [])
    {
        $output = null;
        return $output;

    }
    public function carousel($input = [])
    {

        $output = [
            'messaging_type' => "RESPONSE",
            'recipient' =>
                (object)[
                'id' => $this->chatRoom->created_by->channel_user_id,
            ],
            'message' => (object)[
                'attachment' => (object)[
                    'type' => "template",
                    'payload' => (object)[
                        'template_type' => 'generic',
                        'image_aspect_ratio' => $input['image_aspect_ratio'] ?? 'horizontal'
                    ]
                ]
            ]
        ];
        if (!isset($input['columns']))
            return $output;

        foreach ($input['columns'] as $data) {
            if (isset($data->actions)) {
                foreach ($data->actions as $action) {
                    $buttons[] = (object)[
                        'type' => 'postback',
                        'title' => $action->label,
                        "payload" => $action->label ?? 'default text payload'
                    ];
                }
            }
            $output['message']->attachment->payload->elements[] = (object)[
                'title' => $data->title ?? '',
                'subtitle' => $data->text ?? '',
                'image_url' => $data->thumbnailImageUrl ?? '',
        // 'default_action' => $data->defaultAction ?? '',
                'buttons' => $buttons ?? [],
            ];
            unset($buttons);
        }
    // die(print_r($output));
        return $output;


    }
}