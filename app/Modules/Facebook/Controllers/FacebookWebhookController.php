<?php

namespace App\Modules\Facebook\Controllers;

use Hashids\Hashids;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Modules\Log\Models\WebhookLog;
use App\Modules\Integration\Models\Integration;
use App\Modules\App\Models\App;
use App\Modules\User\Models\User;
use App\Modules\Room\Models\Room;
use App\Modules\Message\Models\Message;
use App\Modules\Livechat\Models\Livechat;
use App\Modules\Channel\Models\Channel;

use App\Events\NewUser;
use App\Events\NewMessage;
use App\Events\RequestLive;

use App\Modules\Facebook\Services\FacebookApiController;
use App\Modules\Bot\Services\BotService;
use App\Modules\Mobile\Services\BackendService;

use App\Modules\Mobile\Helpers\LennaMessageType;
use App\Modules\Chat\Helpers\ChatHelper;
use App\Modules\Log\Helpers\WebhookHelper;

use App\Http\Controllers\ApiController;

class FacebookWebhookController extends ApiController
{
    protected $app;
    protected $integration;
    protected $channel;

    public function __construct()
    {
        $this->channel = Channel::where('name', 'facebook')->first();
    }
    public function webhookVerify(Request $request)
    {
        $hashids = new Hashids(env("SECRET_KEY"), 50);
        $webhookLog = WebhookLog::create([
            "source" => "facebook",
            "app_id" => 1,
            "events" => 'facebook verify',
            "payload" => json_encode($request->all()),
            "response" => json_encode([
                'success' => true,
                'data' => "verify webhook from facebook"
            ])
        ]);
    // no telp kantor 22213200
        $secret = $hashids->encode(22213200);
        if ($request->hub_verify_token == $secret) {
            return $request->hub_challenge;
        }
        return response()->json([
            "success" => true,
            "events" => "testing webhook"
        ], 200);
    }

    public function webhookPostTest(Request $request)
    {
        $webhookLog = WebhookLog::create([
            "source" => "facebook",
            "app_id" => '1',
            "events" => 'facebook post webhook',
            "payload" => json_encode($request->all()),
            "response" => json_encode([
                'success' => true,
                'data' => json_encode($request->all())
            ])
        ]);
        if (isset($request->hub_challenge)) {
            return $request->hub_challenge;
        }
        return response()->json(['data' => 'daaa'], 200);
    }
    public function webhook(Request $request)
    {
        if (!$this->integration = WebhookHelper::getIntegrationData('facebook', $request->entry[0]['id']))
            return $this->errorResponse(null, 'integration not active');

        $this->app = App::find($this->integration->app_id);
        $appId = $this->integration->app_id;

         $webhookLog = WebhookLog::create([
            "source" => "facebook",
            "app_id" => $appId,
            "events" => 'all_webhook_fb_test',
            "payload" => json_encode($request->all()),
            "response" => json_encode([
                'success' => true,
                'data' => ($request->all())
            ]),
        ]);

        $entryData = $request->entry[0];
        $events = $this->getEvents($entryData);
        $botService = new BotService(['appId' => $this->app->id]);
        switch ($events) {
            case 'incoming_message':
                $response = $this->incomingMessage($entryData, $this->app->id);

                // check user is blocked
                if(is_bool($response) && $response = true){
                    return $this->successResponse(["data" => $response],"user blocked");
                }
                // if status resolved
                if (WebhookHelper::isResolved(['roomId' => $response['room']->id])) {
                  // change status to bot by deleting livechat key
                  $this->deleteLiveChat($response['room']->id);
                }
                broadcast(new NewMessage($response['message'], $response['room'], $this->app));
                if (!WebhookHelper::isLive(['roomId' => $response['room']->id])) {
                  if (!WebhookHelper::isRequest(['roomId' => $response['room']->id])) {
                    $backendService = new BackendService(['appId' => $appId]);
                    $backendResponse = $backendService->getUserToken([
                        'email' => $response['user']->email,
                        "password" => BackendService::encryptText(env("ENCRYPT_KEY"), "$appId:" . $response['user']->email),
                        "fcm_token" => null,
                        "client" => "whatsapp",
                        "userId" => $response['user']->id
                    ]);
                    $botResponse = $botService->replyMessage([
                        'userId' => $response['user']->id,
                        'content' => $response['message']->content[0],
                        'room' => $response['room'],
                        'lat' => $request->lat ?? 0,
                        'lon' => $request->lon ?? 0,
                        'channel' => 'whatsapp',
                        "headers" => ["Authorization" => "$backendResponse->token_type $backendResponse->access_token"],
                        'lang' => $response['user']->lang
                    ]);
                  }
                }
                 // if bot not integrated, change status to request
                 $botIntegration = $botService->getIntegration();
                 if (!$botIntegration) {
                   if (!WebhookHelper::isLive(['roomId' => $response['room']->id])) {
                     if (!WebhookHelper::isRequest(['roomId' => $response['room']->id])) {
                       WebhookHelper::requestLivechat($response['user']->id, $appId);
                     }
                   }
                 }
                $responseLog = (object)[
                    'output' => $botResponse['nlp']->response ?? null,
                    'quickbutton' => $botResponse['nlp']->quickbutton ?? [],
                    'type' => $botResponse['nlp']->type ?? null
                ];
                break;
            case 'read':
                return 'this is read';

                break;
            case 'delivery':
                return 'this is delivery';

                break;
            case 'echo':
                return 'this is unfollow';

                break;
        }


        $webhookLog = WebhookLog::create([
            "source" => "facebook",
            "app_id" => $this->app->id,
            "events" => $events,
            "payload" => json_encode($request->all()),
            "response" => json_encode([
                'success' => true,
                'data' => $responseLog
            ]),
            "user_id" => $response['user']->id,
            "bot_id" => $botIntegration ? $botIntegration->integration_data['botId'] : NULL
        ]);

        return $this->successResponse([
            "events" => $events,
            'result' => (object)[
                'output' => $botResponse['nlp']->response ?? null,
                'quickbutton' => $botResponse['nlp']->quickbutton ?? [],
            ]
        ]);


    }

    public function registerUser($facebookData, $appId)
    {
        $facebookController = new FacebookApiController(['integration' => $this->integration]);
        $response = $facebookController->getUserProfile($facebookData['sender']['id']);
        // dd($response);
        // if user who register with phone number
        if (isset($response->error)) {
            $unique = uniqid("fb-user");
            $response->name = $unique;
            $response->nickname = $unique;
            $response->id = $facebookData['sender']['id'];
        }
        $plainName = strtolower(preg_replace('/\s+/', '', $response->name));
        $email = $plainName . $response->id . "@facebook.com";
        $fields = [
            'app_id' => $appId,
            'channel_user_id' => $response->id,
            'name' => $response->name,
            'nickname' => $response->middle_name ?? $response->first_name ?? $response->name,
            'password' => bcrypt("$appId:$email"),
            'picture' => $response->profile_pic ?? env("APP_URL") . "/images/pictures/no_avatar.jpg",
            'email' => $email,
        ];
        $user = $this->channel->users()->firstOrCreate(
            ['channel_user_id' => $response->id, 'app_id' => $appId],
            $fields
        );
        return $user;
    }

    public function updateUser($facebookData, $appId)
    {
        $facebookController = new FacebookApiController(['integration' => $this->integration]);
        $response = $facebookController->getUserProfile($facebookData['sender']['id']);
        // dd($response);
        $plainName = strtolower(preg_replace('/\s+/', '', $response->name));
        $email = $plainName . $response->id . "@facebook.com";
        $fields = [
            'app_id' => $appId,
            'channel_user_id' => $response->id,
            'name' => $response->name,
            'nickname' => $response->middle_name ?? $response->first_name ?? $response->name,
            'password' => bcrypt("$appId:$email"),
            'picture' => $response->profile_pic ?? env("APP_URL") . "/images/pictures/no_avatar.jpg",
            'email' => $email,
        ];
        return $fields;
    }

    public function incomingMessage($entryData, $appId)
    {
        $messaging = $entryData['messaging'][0];
        $user = User::where([['channel_user_id', "=", $messaging['sender']['id']], ['app_id', "=", $appId]])->first();
        // register user if not registered
        if (!$user) {
            $user = $this->registerUser($messaging, $appId);
        } else if (strstr($user->name, 'fb-user')) {
            $updateUser = $this->updateUser($messaging, $appId);
            $user->name = $updateUser['name'];
            $user->nickname = $updateUser['nickname'];
            $user->save();

        }

        $chatRoom = ChatHelper::createRoomAndParticipate(['channel' => $this->channel, 'user' => $user, 'appId' => $appId]);

        $channelData = [
            'message_id' => $entryData['id']
        ];
        $content[] = $this->setMessageType($messaging);
        $message = ChatHelper::createMessage(['channelData' => $channelData, 'content' => $content, 'user' => $user, 'room' => $chatRoom]);

        return [
            'message' => $message,
            'user' => $user,
            'room' => $chatRoom
        ];


    }
    public function setMessageType($messaging)
    {
        $lennaMessageType = new LennaMessageType;
        if (Arr::has($messaging, 'postback')) {
            $messageType = 'text';
            $isPostback = true;
        } else {
            $messageType = $this->checkMessageType($messaging['message']);
            $isPostback = false;

        }

        switch ($messageType) {
            case 'text':
                if ($isPostback) {
                    $content = $lennaMessageType->text(['text' => $messaging['postback']['payload']]);
                } else {
                    $content = $lennaMessageType->text(['text' => $messaging['message']['text']]);
                }
                break;
            case 'attachments':
                $attachments = $messaging['message']['attachments'][0];
                switch ($attachments['type']) {
                    case 'image':
                        $content = $lennaMessageType->image(['previewImageUrl' => $attachments['payload']['url']]);
                        break;
                    case 'video':
                        $content = $lennaMessageType->video(['originalContentUrl' => $attachments['payload']['url']]);
                        break;
                    case 'file':
                        $fileName = explode("/", $attachments['payload']['url']);
                        $fileName = strtok(end($fileName), "?");
                        $content = $lennaMessageType->file(['fileUrl' => $attachments['payload']['url'], 'fileName' => $fileName]);

                        break;
                    case 'audio':
                        $content = $lennaMessageType->audio(["originalContentUrl" => $attachments['payload']['url']]);
                        break;
                    case 'location':
                        $content = $lennaMessageType->location(
                            [
                                "title" => $attachments['title'],
                                "locationUrl" => $attachments['url'],
                                'latitude' => $attachments['payload']['coordinates']['lat'],
                                'longitude' => $attachments['payload']['coordinates']['long']
                            ]
                        );
                        break;
                    default:
                        $content = $lennaMessageType->text(['text' => 'this message type not supported yet']);

                        break;
                }


                break;
            default:
                $content = $lennaMessageType->text(['text' => 'this message type not supported yet']);

                break;
        }
        return $content;
    }

    public function getEvents($entryData)
    {
    // page send message to user
        $messaging = $entryData['messaging'][0];
        if (Arr::has($messaging, 'message.is_echo')) {
            return 'echo';
        }
    // incoming message from user
        elseif (Arr::has($messaging, 'message') || Arr::has($messaging, 'postback')) {
            return 'incoming_message';
        }
    // message has been readed by user
        elseif (Arr::has($messaging, 'read')) {
            return 'delivery';
        }
    // message has been delivered to user
        elseif (Arr::has($messaging, 'delivery')) {
            return 'read';
        }

    }
    public function checkMessageType($messageData)
    {
    // page send message to user
        if (Arr::has($messageData, 'text')) {
            return 'text';
        }
    // incoming message from user
        elseif (Arr::has($messageData, 'attachments')) {
            return 'attachments';
        }
    }

    public function requestLivechat($senderId, $appId)
    {
        $room = User::find($senderId)->rooms->first();
        $liveChat = $room->liveChats()->firstOrCreate(
            ['room_id' => $room->id, 'status' => 'request'],
            [
                "request_at" => Carbon::now()->toDateTimeString(),
                "status" => 'request',
            ]
        );
        broadcast(new RequestLive($room->id, $appId, $senderId, $liveChat->id));
    }

    public function deleteLiveChat($roomId)
    {
        Livechat::where('room_id', $roomId)->delete();
    }
}