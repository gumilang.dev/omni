<?php

namespace App\Modules\Facebook\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\ExternalApi\Models\ExternalApi;
use GuzzleHttp\Client;
use App\Modules\Facebook\Helpers\FacebookMessageType;
use GuzzleHttp\Exception\ClientException;
use App\Modules\Common\Services\EncryptService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\RequestGuzzle;

class FacebookApiController extends Controller
{
    protected $client;
    protected $facebookApi;

    public function __construct($data = null)
    {
        $this->data = $data ?? null;

        $this->facebookApi = ExternalApi::where(['category' => 'channel', 'provider' => 'facebook'])->first();
        $guzzleProp = [
            'base_uri' => $this->facebookApi->base_url
        ];
        $this->client = new Client($guzzleProp);
    }

    public function subscribeApp($pageId, $pageAccessToken)
    {
        $apiProp = $this->facebookApi->endpoints()->where('name', 'subscribe-app')->first();
        $param = [
            'access_token' => $pageAccessToken,
            'subscribed_fields' => 'messages,message_deliveries,message_reads,messaging_postbacks'
        ];
        try {
            $response = $this->client->request(
                $apiProp->method,
                str_replace('{nodes}', $pageId, $apiProp->endpoint),
                [
                    // 'http_errors' => false,
                    'query' => $param
                ]
            );
        } catch (\Exception $e) {
            $error = json_decode($e->getResponse()->getBody(), true);
            return $error;
            // $error = $error['error']['message'];
        }
        $response = json_decode($response->getBody()->getContents(), true);
        return $response;
    }
    public function unsubscribeApp($pageId, $pageAccessToken)
    {
        $apiProp = $this->facebookApi->endpoints()->where('name', 'unsubscribe-app')->first();
        $param = [
            'access_token' => $pageAccessToken
        ];

        try {
            $response = $this->client->request(
                $apiProp->method,
                str_replace('{nodes}', $pageId, $apiProp->endpoint),
                [
                    'query' => $param
                ]
            );
            $response = json_decode($response->getBody()->getContents());

        } catch (ClientException $exception) {
            $response = json_decode($exception->getResponse()->getBody()->getContents());
        }
        return $response;
    }
    public function getUserPicture($userId)
    {
        $apiProp = $this->facebookApi->endpoints()->where('name', 'get-user-picture')->first();
        $param = [
            'redirect' => 0,
            'height' => 500
        ];
        $response = $this->client->request(
            $apiProp->method,
            str_replace('{nodes}', $userId, $apiProp->endpoint),
            [
                'query' => $param
            ]
        );
        $response = json_decode($response->getBody()->getContents());
        return $response;
    }
    public function getUserProfile($fbUserId)
    {
        $apiProp = $this->facebookApi->endpoints()->where('name', 'get-user-profile')->first();
        $param = [
            'access_token' => $this->data['integration']->integration_data['pageAccessToken'],
            'fields' => "middle_name, last_name, first_name, name, profile_pic"
        ];

        try {
            $response = $this->client->request(
                $apiProp->method,
                str_replace('{nodes}', $fbUserId, $apiProp->endpoint),
                [
                    'query' => $param
                ]
            );
            $response = json_decode($response->getBody()->getContents());

        } catch (ClientException $exception) {
            $response = json_decode($exception->getResponse()->getBody()->getContents());
        }
        // dd($response);
        return $response;
    }


    public function echoMessage($newMessage)
    {
        $apiProp = $this->facebookApi->endpoints()->where('name', 'echo-message')->first();
        $param = [
            'access_token' => $this->data['integration']->integration_data['pageAccessToken']
        ];
        // $newMessage = EncryptService::decrypt($newMessage, env('ENCRYPT_MESSAGE_KEY'), 20);
        // $newMessage = json_decode($newMessage, true);
        foreach ($newMessage->content as $key => $content) {
            $message = $this->convertToFbMessageType($content, $this->data['chatRoom']);
            // dd($message);
            $response = $this->client->request(
                $apiProp->method,
                $apiProp->endpoint,
                [
                    'query' => $param,
                    "json" => $message
                ]
            );
            $response = json_decode($response->getBody()->getContents());
        }

        return $response;
    }
    public function getLongLivedToken($userAccessToken)
    {
        $apiProp = $this->facebookApi->endpoints()->where('name', 'get-longlived-token')->first();
        $param = [
            'grant_type' => "fb_exchange_token",
            'client_id' => env("FB_APP_ID"),
            'client_secret' => env("FB_APP_SECRET"),
            'fb_exchange_token' => $userAccessToken
        ];
        try {
            $response = $this->client->request(
                $apiProp->method,
                $apiProp->endpoint,
                [
                    'query' => $param,
                ]
            );
        } catch (\Exception $e) {
            $error = json_decode($e->getResponse()->getBody());
            return $response = $error;
        }

        $response = json_decode($response->getBody()->getContents());
        return $response;
    }
    public function convertToFbMessageType($message, $chatRoom)
    {
        $fbMessageType = new FacebookMessageType($chatRoom);
        switch ($message->type) {
            case 'text':
                $message = $fbMessageType->text(['text' => $message->text]);
                break;
            case 'image':
                $message = $fbMessageType->image(['imageUrl' => $message->previewImageUrl]);
                break;
            case 'video':
                $message = $fbMessageType->video(['videoUrl' => $message->originalContentUrl]);
                break;
            case 'audio':
                $message = $fbMessageType->audio(['audioUrl' => $message->originalContentUrl]);
                break;
            case 'file':
                $message = $fbMessageType->file(['fileUrl' => $message->fileUrl]);
                break;
            case 'carousel':
                $message = $fbMessageType->carousel([
                    'columns' => $message->columns,
                    'imageAspectRatio' => $message->imageAspectRatio,
                    'imageSize' => $message->imageSize,
                ]);
                break;
            case 'confirm':
                $message = $fbMessageType->confirm([
                    'actions' => $message->actions,
                    'text' => $message->text,
                ]);
                break;
            default:
                $message = $fbMessageType->text(['text' => "Sent $message->type message"]);
                break;
        }
    // dd($message);
        return (object)$message;
    }
}
