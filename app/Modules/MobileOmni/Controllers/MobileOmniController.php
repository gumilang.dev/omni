<?php

namespace App\Modules\MobileOmni\Controllers;

use Hashids\Hashids;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

use App\Modules\Integration\Models\Integration;
use App\Modules\User\Models\UserApp;
use App\Modules\Channel\Models\Channel;

use App\Modules\MobileOmni\Services\MobileOmniApiController;
use App\Modules\Mobile\Services\BackendService;

use App\Modules\Log\Helpers\WebhookHelper;

use App\Http\Controllers\ApiController;

class MobileOmniController extends ApiController
{
    protected $app;
    protected $integration;
    protected $channel;

    public function __construct()
    {
        $this->channel = Channel::where('name', 'mobile-omni')->first();
    }

    // save token mobile omni data
    public function store(Request $request, $appId)
    {
        if (!$this->integration = WebhookHelper::getIntegrationData('mobile-omni', $appId))
        {
          return $this->errorResponse(null, 'integration not active');
        }
        $user = UserApp::where('app_id', $appId)->where('user_id', $request->decodedUserId);
        $user->update([
          'token' => $request->header('token'),
        ]);
        $data = $user->first();
        $id = BackendService::encryptText(env("ENCRYPT_KEY"), $data->id);
        return $this->successResponse($id, 'Save mobile omni user data success');
    }

    // while scanning qr code omni data
    public function getData(Request $request)
    {
        $id = BackendService::decryptText(env("ENCRYPT_KEY"), $request->id);
        $userMobile = UserApp::findOrFail($id);
        if (!$this->integration = WebhookHelper::getIntegrationData('mobile-omni', $userMobile->app_id))
        {
          return $this->errorResponse(null, 'integration not active');
        }
        $userMobile->fcm_key = $request->fcm_key;
        $userMobile->save();
        return $this->successResponse($userMobile, 'Get mobile omni user data success');
    }
}
