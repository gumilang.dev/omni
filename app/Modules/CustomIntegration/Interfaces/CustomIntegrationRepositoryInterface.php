<?php


namespace App\Modules\CustomIntegration\Interfaces;


interface CustomIntegrationRepositoryInterface
{
  public function get();
  public function getByAppId($app_id);
  public function create();
  public function getByAppIdAndCustomIntegrationSlug($app_id,$slug);
  public function update();
  public function isCustomIntegrationIntegrated($app_id,$slug);

}
