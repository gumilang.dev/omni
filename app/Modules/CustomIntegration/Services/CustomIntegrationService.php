<?php


namespace App\Modules\CustomIntegration\Services;


use App\Modules\CustomIntegration\Interfaces\CustomIntegrationRepositoryInterface;
use App\Modules\CustomIntegration\Interfaces\CustomIntegrationInterfaces;

class CustomIntegrationService
{

  /**
   * @var CustomIntegrationInterfaces
   */
  private $repo;

  /**
   * CustomIntegrationService constructor.
   * @param CustomIntegrationRepositoryInterface $repo
   */
  public function __construct(CustomIntegrationRepositoryInterface $repo)
  {
    $this->repo = $repo;
  }

  /**
   * @param $app_id
   * @return mixed
   */
  public function getCustomIntegrationByAppId($app_id) {
    return $this->repo->getByAppId($app_id);
  }

  /**
   * @param $app_id
   * @param $slug
   * @return mixed
   */
  public function getCustomIntegrationByAppIdAndSlug($app_id,$slug) {
    return $this->repo->getByAppIdAndCustomIntegrationSlug($app_id,$slug);
  }

  public function isIntegrated($app_id,$slug) {
    return $this->repo->isCustomIntegrationIntegrated($app_id,$slug);
  }
}
