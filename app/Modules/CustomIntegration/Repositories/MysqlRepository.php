<?php

namespace App\Modules\CustomIntegration\Repositories;

use App\Modules\CustomIntegration\Interfaces\CustomIntegrationRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class MysqlRepository implements CustomIntegrationRepositoryInterface
{

  /**
   * @var int
   * @desc 24 hour in second
   */
  protected $cache_expire = 3600 * 30;
  protected $cache_key = "custom__integraiton__";

  public function get()
  {
    // TODO: Implement get() method.
  }


  /**
   * @param $app_id
   * @return mixed
   */
  public function getByAppId($app_id)
  {
    return Cache::remember($this->cache_key."".$app_id,$this->cache_expire,function() use ($app_id) {
      return DB::table("omnichannel.custom_integration_list as cil")
        ->join("omnichannel.custom_integration as ci",function ($join) {
          $join->on("cil.custom_integration_id","=","ci.id");
        })
        ->select(["cil.id","cil.credential","cil.is_active","ci.name","ci.icon","ci.slug","ci.subMenuName"])
        ->where("cil.app_id",$app_id)
        ->where("cil.is_active",true)
        ->get();
    });

  }

  public function create()
  {
    // TODO: Implement create() method.
  }

  public function update()
  {
    // TODO: Implement update() method.
  }

  /**
   * @param $app_id
   * @param $slug
   * @return mixed
   */
  public function getByAppIdAndCustomIntegrationSlug($app_id, $slug)
  {
    return Cache::remember($this->cache_key."".$app_id."__".$slug,$this->cache_expire,function() use ($app_id,$slug) {
      return DB::table("omnichannel.custom_integration_list as cil")
        ->join("omnichannel.custom_integration as ci",function ($join) {
          $join->on("cil.custom_integration_id","=","ci.id");
        })
        ->select(["cil.id","cil.credential","cil.is_active","ci.name","ci.icon","ci.slug","ci.subMenuName"])
        ->where("cil.app_id",$app_id)
        ->where("ci.slug",$slug)
        ->where("cil.is_active",true)
        ->first();
    });
  }

  public function isCustomIntegrationIntegrated($app_id,$slug) {
    $response = $this->getByAppIdAndCustomIntegrationSlug($app_id,$slug);
    if(is_null($response)) return false;
    return true;
  }

}
