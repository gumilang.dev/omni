<?php


namespace App\Modules\CustomIntegration\Controllers;


use App\Http\Controllers\ApiController;
use App\Modules\CustomIntegration\Services\CustomIntegrationService;
use Illuminate\Http\Request;


class CustomIntegrationController extends ApiController
{

  /**
   * @var
   */
  private $customIntegrationService;

  public function __construct(CustomIntegrationService $customIntegrationService)
  {
    $this->customIntegrationService = $customIntegrationService;
  }

  /**
   * @param Request $request
   * @param $appId
   */
  public function getByAppId(Request $request,$appId) {
    $response_from_service = $this->customIntegrationService->getCustomIntegrationByAppId($appId);
    return $this->successResponse($response_from_service,"FETCH_CUSTOM_INTEGRATION_LIST_SUCCESS");
  }
}
