<?php

namespace App\Modules\Module\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $connection = 'pgsql';

    protected $guarded = [];

    protected $hidden = [
      'created_at',
      'updated_at'
    ];

    public function menus()
    {
        return $this->hasMany('App\Modules\Menu\Models\Menu', 'module_id');
    }
}
