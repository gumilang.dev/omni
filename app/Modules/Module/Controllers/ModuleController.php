<?php

namespace App\Modules\Module\Controllers;

use Illuminate\Http\Request;
use Hashids\Hashids;
use Symfony\Component\HttpFoundation\Response;
use App\Modules\Log\Helpers\WebhookHelper;
use App\Modules\Whatsapp\Services\WaOfficialApiController;

use App\Modules\Facebook\Services\FacebookApiController;
use App\Modules\Telegram\Services\TelegramApiController;

use App\Http\Controllers\ApiController;

use App\Modules\Role\Helpers\RoleHelper;

use App\Modules\Integration\Models\Integration;
use App\Modules\Module\Models\Module;
use App\Modules\App\Models\App;
use App\Modules\Webchat\Models\WebchatStyle;
use Abraham\TwitterOAuth\TwitterOAuth;

class ModuleController extends ApiController
{
  // protected $facebookController;
  // protected $telegramController;

  // public function __construct(Request $request)
  // {
  //   $this->facebookController = new FacebookApiController();
  //   $this->telegramController = new TelegramApiController([
  //       'botToken' => $request->botToken
  //   ]);
  //   $this->hashids = new Hashids('', 6);
  // }

  public function index(Request $request)
  {
    $result = Module::with(['menus','menus.submenus', 'menus.submenus.subSubmenus'])->get();
    return $this->successResponse($result);
  }

  public function getAllWithMap(Request $request)
  {
    $result = Module::with(['menus','menus.submenus', 'menus.submenus.subSubmenus'])->get();
    foreach ($result as $key => $module) {
      $menus = [];
      if (count($module['menus']) > 0) {
        foreach ($module['menus'] as $key => $menu) {
          $submenus = [];
          if (count($menu['submenus']) > 0) {
            foreach ($menu['submenus'] as $key => $submenu) {
              $subSubmenus = [];
              if (count($submenu['sub_submenus']) > 0) {
                foreach ($submenu['sub_submenus'] as $key => $value) {
                  $subSubmenu['id'] = 'sub_submenus-'.$subSubmenu['id'];
                  $subSubmenu['label'] = $subSubmenu['name'];
                }
              } else {
                break;
              }
              $submenu['id'] = 'submenu-'.$submenu['id'];
              $submenu['label'] = $submenu['name'];
              $submenu['children'] = $subSubmenus;
            }
          } else {
            break;
          }
          $menu['id'] = 'menu-'.$menu['id'];
          $menu['label'] = $menu['name'];
          $menu['children'] = $submenus;
        }
      } else {
        break;
      }
      $module['id'] = 'module-'.$module['id'];
      $module['label'] = $module['name'];
      $module['children'] = $menus;
    }
    // $result = $result->map(function($module) {
    //   $menus = [];
    //   if (count($module['menus']) > 0) {
    //     $menus = $module->menus->map(function($menu) {
    //       $submenus = [];
    //       if (count($menu['submenus']) > 0) {
    //         $submenus = $menu->submenus->map(function($submenu) {
    //           $subSubmenus = [];
    //           if (count($submenu['sub_submenus']) > 0) {
    //             $subSubmenus = $submenu->sub_submenus->map(function($subSubmenu) {
    //               return [
    //                 'id' => 'sub_submenus-'.$subSubmenu['id'],
    //                 'label' => $subSubmenu['name']
    //               ];
    //             });
    //           }
    //           return [
    //             'id' => 'submenu-'.$submenu['id'],
    //             'label' => $submenu['name'],
    //             'children' => $subSubmenus
    //           ];
    //         });
    //       }
    //       return [
    //         'id' => 'menu-'.$menu['id'],
    //         'label' => $menu['name'],
    //         'children' => $submenus,
    //       ];
    //     });
    //   }
    //   return [
    //     'id' => 'module-'.$module['id'],
    //     'label' => $module['name'],
    //     'children' => $menus,
    //   ];
    // });
    return $this->successResponse($result);
  }
}
