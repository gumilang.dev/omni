<?php

namespace App\Modules\App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hashids\Hashids;

class App extends Model
{
    use SoftDeletes;
    protected $connection = 'pgsql';
    // getters
    protected $appends = ['hashed_id'];
    protected $casts = [
        'settings' => 'object'
    ];
    protected $guarded = [];
    public function getHashedIdAttribute()
    {
        $hash = new Hashids('',6);
        return $hash->encode($this->id);
    }
    // relation
    public function users()
    {
        return $this->belongsToMany("App\Modules\User\Models\User");
    }
    public function integrations()
    {
        return $this->hasMany("App\Modules\Integration\Models\Integration");
    }
    public function rooms()
    {
        return $this->hasMany("App\Modules\Room\Models\Room");
    }
    public function platformUsers()
    {
      return $this->belongsToMany("App\Modules\User\Models\UserPlatform", "app_user_platform", "app_id", "user_id");
    }
    public function webchatStyle()
    {
      return $this->hasOne("App\Modules\Webchat\Models\WebchatStyle", "app_id");
    }
}
