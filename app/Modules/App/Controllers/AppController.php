<?php

namespace App\Modules\App\Controllers;

use Illuminate\Http\Request;
use App\Modules\App\Models\App;
use Carbon\Carbon;
use App\Http\Controllers\ApiController;


class AppController extends ApiController
{

    public function index(Request $request, $appId)
    {

    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:50',
            'email' => 'required|email|unique:apps,email',
        ]);
        $app = app::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt(str_random()),
        ]);

        return response()->json($app);
    }


    public function show($appId)
    {
        $app = App::where('id', $appId)->first();

        return $this->successResponse(['app' => $app]);
    }


    public function edit($appId)
    {
        $app = App::where('id', $appId)->first();

        return $this->successResponse(['app' => $app]);
    }


    public function update(Request $request, $appId)
    {
        // $validatedData = $request->validate([
        //     'name' => 'required',
        // ]);
        // $data = [
        //   'name' => $request->name,
        //   'description' => $request->description,
        //   'settings' => json_encode($request->settings),
        //   'agent_allocation_automation' => $request->agent_allocation_automation,
        //   'approval_agent' => $request->approval_agent,
        //   'auto_resolve' => $request->auto_resolve,
        //   'auto_resolve_time' => $request->auto_resolve_time,
        //   'auto_resolve_message' => $request->auto_resolve_message,
        //   'auto_send_rating_message' => $request->auto_send_rating_message,
        //   'keyword_for_trigger_rating' => $request->keyword_for_trigger_rating,
        //   'webchat_location' => $request->webchat_location
        // ];
        // $appId = App::where('id', $appId)->update($data);
        // $app = App::where(['id' => $appId])->first();
        $app = App::find($appId);
        $app->name = $request->name;
        $app->description = $request->description;
        $app->settings = json_encode($request->settings);
        $app->agent_allocation_automation = $request->agent_allocation_automation;
        $app->approval_agent = $request->approval_agent;
        $app->auto_resolve = $request->auto_resolve;
        $app->auto_resolve_time = $request->auto_resolve_time;
        $app->auto_resolve_message = $request->auto_resolve_message;
        $app->auto_send_rating_message = $request->auto_send_rating_message;
        $app->keyword_for_trigger_rating = $request->keyword_for_trigger_rating;
        $app->webchat_location = $request->webchat_location;
        $app->save();
        return $this->successResponse(['app' => $app], 'app updated');
    }


    public function destroy($appId)
    {
        $app = app::findOrFail($appId);
        $app->delete();

        return $this->successResponse(null, 'app deleted');
    }
}
