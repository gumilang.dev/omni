<?php

namespace App\Modules\Test\Controllers;

use Hashids\Hashids;
use Carbon\Carbon;
use PeterPetrus\Auth\PassportToken;
use Stevebauman\Location\Position;
use Abraham\TwitterOAuth\TwitterOAuth;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ClientException;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Notification;
use Location;
use DB;
use Exception;
use Faker;

use App\Modules\Livechat\Models\Livechat;
use App\Modules\User\Models\UserToken;
use App\Modules\User\Models\User;
use App\Modules\User\Models\UserBot;
use App\Modules\User\Models\UserApp;
use App\Modules\Transaction\Models\Transaction;
use App\Modules\ExternalApi\Models\ExternalApi;
use App\Modules\Channel\Models\Channel;
use App\Modules\Integration\Models\Integration;
use App\Modules\Room\Models\Room;
use App\Modules\Message\Models\Message;
use App\Modules\App\Models\App;
use App\Modules\Bot\Models\Bot;
use App\Modules\Log\Models\ApiLog;
use App\Modules\User\Models\UserPlatform;
use App\Modules\User\Models\VerifyUser;
use App\Modules\Webchat\Models\WebchatStyle;
use App\Modules\User\Models\UserRole;
use App\Modules\Log\Models\WebhookLog;
use App\Modules\Role\Models\RoleMenu;
use App\Modules\Bot\Models\StoryGroup;
use App\Modules\RoomTag\Model\RoomTag;
use App\Modules\Session\Models\Session;
use App\Modules\TimeReportServed\Model\TimeReportServed;
use App\Modules\Group\Models\Group;

use App\Modules\Bot\Services\BotService;
use App\Modules\Mobile\Services\BackendService;
use App\Modules\Common\Services\EncryptService;

use App\Modules\Role\Helpers\RoleHelper;
use App\Modules\Log\Helpers\WebhookHelper;

use App\Http\Controllers\Controller;

use App\Events\TestPresence;
use App\Events\TestEvent;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiController;
use App\Notifications\SocketNotification;

class TestController extends ApiController
{
    public function __construct()
    {
        $this->channel = Channel::where('name', 'webchat')->first();
        $this->hashids = new Hashids('', 6);
        $this->appId = 45;
        $this->lennaApi = ExternalApi::where(['category' => 'internal', 'provider' => 'lenna'])->first();
        $guzzleProp = ['base_uri' => $this->lennaApi->base_url, 'verify' => false];
        $this->client = new Client($guzzleProp);
        // hashed with backend methods
        $backendHashids = new Hashids(env("SECRET_KEY"), '6');
        $this->backendAppId = $backendHashids->encode($this->appId);
    }
    public function syncRoleId(Request $request)
    {
      $userRoles = UserRole::select(['product_id','user_id','role_id'])->where('platform_id', env('OMNI_PLATFORM_ID'))->get();
      $userApps = UserApp::all();
      foreach ($userApps as $userApp) {
        foreach ($userRoles as $userRole) {
          if ($userApp->app_id == $userRole->product_id && $userApp->user_id == $userRole->user_id) {
            UserApp::where('app_id', $userRole->product_id)->where('user_id', $userRole->user_id)->update(['role_id' => $userRole->role_id]);
            continue;
          } else {
            continue;
          }
        }
      }
      return $userApps;
    }

    public function broadcastAuth(Request $request)
    {
    }

    public function test(Request $request)
    {
      $liveChat = Livechat::whereHas('room', function($q) {
        $q->where('app_id', 6);
      })->where('status', 'live')->get();
      $liveChat = collect($liveChat)->countBy('group_id');
      $arrTemp = [];
      $arr = 0;
      // $liveChat = new ArrayObject($liveChat);
      foreach( $liveChat as $key => $value){
        array_push($arrTemp, ['value' => $key, 'count' => $value]);
      }
      // dd(array_multisort(array_column($arrTemp, 'value'), SORT_DESC, $arrTemp));
      usort($arrTemp, function ($item1, $item2) {
        return $item1['count'] <=> $item2['count'];
      });
      dd($arrTemp[0]['value']);
      for ($i=0; $i < count($liveChat); $i++) {
        dd($i);
        if ($i == (count($liveChat) - 1)) {
          break;
        }
        if ($liveChat[$i++] > $liveChat[$i]) {
          $arr = $liveChat[$i++];
        }
      }
      // $liveChat->asort();
      // $liveChat = array_values($liveChat);
      // return json_decode(json_encode($liveChat), true);
      return response()->json($arr);
      $min = array_keys($liveChat, min($liveChat));
      return response()->json($min);
      // $room = DB::table("omnichannel.rooms as r")
      //           ->leftJoin("omnichannel.livechats as l",function($join) {
      //             $join->on("r.id","=","l.room_id");
      //           })
      //           ->where("l.status","live")
      //           ->whereNull("l.end_at")
      //           ->whereNotNull("l.start_at")
      //           ->whereNotNull("l.request_at")
      //           ->where("r.app_id",6)
      //           ->orderBy("l.id","ASC")
      //           ->get();
      return response()->json($liveChat, 200);
      $groups = Group::where('app_id', 6)->whereHas('agentGroup', function($q) {
        $q->where('user_id',109);
      })->get();
      $groups = collect($groups)->pluck('id')->toArray();
      $rand = array_rand($groups);
      return response()->json($groups[$rand]);
      $to = Carbon::createFromFormat("Y-m-d H:i:s",Carbon::now('Asia/Jakarta')->toDateTimeString());
      $from = Carbon::createFromFormat("Y-m-d H:i:s","2021-07-15 14:32:38");
      $diffInMinutes = $from->diffInMinutes($to, false);
      if ($diffInMinutes > 10) {
        return 'resolve';
      }
      return response()->json($diffInMinutes);
      $users = ['ival@lenna.ai','agus@lenna.ai','haidar@lenna.ai','yusuf@lenna.ai'];
      foreach ($users as $key => $value) {
        $data[] = UserPlatform::where('email', $value)->first();
      }

      Notification::send($data, new SocketNotification(['url' =>'$lennaApi->base_url','status' => 'Down']));
      return response()->json(['data' => $data]);
      $appId = 128;
      $channel = 2;
      $app = collect(config('encrypt.enabled.apps'));
      $app = $app->where('id', $appId)->values()->all();
      if (count($app) > 0) {
        if (collect($app[0]['channels'])->contains($channel)) {
          return $app;
        } else {
          return "no channel";
        }
      } else {
        return "false";
      }
      $room = Room::where('app_id', $appId)->whereHas('liveChats', function ($q) {
        $q->where('status', 'request')->where('end_at', NULL)->where('request_at','!=',NULL);
      })->get()->sortBy(function($o){
        return $o->livechat->request_at;
      })->map(function($val){
        return [
          'user_id' => $val->created_by->id,
          'request_at' => $val->livechat->request_at
        ];
      })->values();
      return response()->json($room);
      $referer = parse_url($request->referer);
      $array = explode("/",$referer['path']);
      $menu = $array[4] ?? [];
      $submenu = $array[5] ?? [];
      $role = UserApp::where('user_id', 18)->where('app_id', 6)->first();
      if(!$role){
        return response()->json([
          'success' => false,
          'error'   => null,
          'message' => 'You need authorized for this app, contact your admin',
          'data'    => null,
        ]);
      }
      $menus = RoleHelper::menuPrivileges($role->role_id, 6);
      $menus = collect($menus)->map(function($val){
        return [
          "menu"=>strtolower($val->name),
          "submenus"=>collect($val->submenus)->map(function($q){
            return Str::slug($q->name);
          })
        ];
      });
      $detailMenu = $menus->filter(function($q) use ($menu) {
        return $q['menu'] == $menu;
      })->values();
      if (count($detailMenu) > 0) {
        $sub = collect($detailMenu)->flatMap(function($q) {
          return $q['submenus'];
        });
        if (count($sub) > 0 && $submenu) {
          $data = collect($sub)->contains($submenu);
          if ($data) {
            return response()->json([
              'success'   => true,
              'data'      => $detailMenu,
            ]);
          } else {
            return response()->json([
              'success'   => false,
              'data'      => null,
              'message'   => 'submenu '.$submenu.' not found'
            ]);
          }
        }
        return response()->json([
          'success'   => true,
          'data'      => $detailMenu,
        ]);
      }
      return response()->json([
        'success' => false,
        'data'  => $detailMenu,
        'message'   => 'menu not found'
      ]);
      $appId = 1;
      $user = DB::table('omnichannel.app_user_platform')
      ->where('omnichannel.app_user_platform.app_id', 6)
      ->where('omnichannel.app_user_platform.user_id', 201)
      ->join('auth.users', 'omnichannel.app_user_platform.user_id', '=', 'auth.users.id')
      ->join('auth.roles', function($q) {
        $q->on('omnichannel.app_user_platform.role_id', '=', 'auth.roles.id')
          ->where('auth.roles.app_id', 6)
          ->where('auth.roles.slug', 'staff');
      })
      ->select('auth.users.id','auth.users.name','auth.users.email')
      ->orderByRaw('UPPER(auth.users.name) ASC')
      ->get()->count();
      // $user = UserApp::where('app_id',$appId)->where("role_id",4)->where("online",true)->get();
      return $user;
      // $agents = UserPlatform::with(['apps' => function($q) use ($appId){
      //   $q->where('app_id', $appId)->where('role_id', 4);
      // }])->get()->reject(function ($item) {
      //   return count($item->apps) == 0;
      // })->values();
      $agents = DB::table('omnichannel.app_user_platform')->where('omnichannel.app_user_platform.app_id', $appId)
        ->join('auth.users', 'omnichannel.app_user_platform.user_id', '=', 'auth.users.id')
        ->join('auth.roles', function($q) use ($appId) {
          $q->on('omnichannel.app_user_platform.role_id', '=', 'auth.roles.id')
            ->where('auth.roles.app_id', $appId)
            ->where('auth.roles.slug', 'staff');
        })
        ->select('auth.users.id','auth.users.name','auth.users.email')
        ->get();
      return $agents;

      $messages = Message::with(['room.channel', 'room.app', 'room.notes', 'room.additional_informations', 'room.tags'])->whereHas('room', function($q){
        $q->where('app_id', 6);
      })->orderBy('created_at', 'desc')->take(10)->get();
      return response()->json($messages);
      if (Carbon::parse(Carbon::now())->diffInSeconds($userToken->expires_at, false) < 2) {
        return 'sudah kadaluarsa';
      } else {
        return 'blm kadaluarsa';
      }
      $userToken = UserToken::updateOrCreate(
        [
          'user_id' => 29561,
          // 'access_token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjE2YTM1MzgwN2IyZmExZTY5ODk0YTZhMDNiNDQ2NTMxMzZhN2UyMTFlNjQ3YzYzNmFhMmJhMTc3MmQxMTYzMDc4NjNhMWQ0MWQ1NGZmODkwIn0.eyJhdWQiOiIxIiwianRpIjoiMTZhMzUzODA3YjJmYTFlNjk4OTRhNmEwM2I0NDY1MzEzNmE3ZTIxMWU2NDdjNjM2YWEyYmExNzcyZDExNjMwNzg2M2ExZDQxZDU0ZmY4OTAiLCJpYXQiOjE1NzYwNjAxMTgsIm5iZiI6MTU3NjA2MDExOCwiZXhwIjoxNjA3NjgyNTE4LCJzdWIiOiIyOTU2MSIsInNjb3BlcyI6W119.FbXSblNV4k4zkyq4-aVoZanXGl4hpFARD7Lc13U03Ml_vdW6VoHtYSoLSIeRwxDHveFXbHKoQaywElwujAGUH2BytRWW88IjHPL5hMJOblDBuPndoTlykxl5QWFFmPc1IU8ow2FT3AVO7_BKLKYS4QpKVPeQGPmCZ4P5fqKX5SKjmFkIm98ttBTJaEHTDfBiJh4c8ofqYFRymHIOkM49srAei4IftmbPuO-oysvoQEGBHP5U7tHvjeJd-uginPTHO_PmVclnmmdyCVuxKhsx6A-1zM6EcMkmcu9pGbmvXJNNsB4WBwhMaA18KVMscpistN6_guDetXl8BzupwncXf92kkc3ONMXoUyWaVUBxnrHyTN8HaRhiYlXCb6YvA7Bi75cGOmo4C6MIN5mwlwje4TPhrF91XGkuM0pldjBoHWL8lw1JwmlCEqLX1i9fhdXHiRrpZ8h3E27Qk4mrdXSgngoyNuFJSVnYGUX8OI4bK-4IN1PL0hwMDloeeXGBf76tRV04oeLKKfaAfwY8nvwVCJjWRb6r3ab_CyrgdcgNwXAqhRlIDtXYx92W7m4kbVWyLVbloWmdCbzZMZJrJNSmQkb39e418rNJsbI32mdVsuWmujJESipzNXXDmhWV9o74Vyeui1xTgrPR_Jd6FzqqKlTsDoNFGI-jX2q375Ji2MQ',
          // 'token_type' => 'Bearer',
          // 'expires_at' => Carbon::now()->toDateTimeString(),
        ],
        [
          // 'user_id' => 29561,
          'access_token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjE2YTM1MzgwN2IyZmExZTY5ODk0YTZhMDNiNDQ2NTMxMzZhN2UyMTFlNjQ3YzYzNmFhMmJhMTc3MmQxMTYzMDc4NjNhMWQ0MWQ1NGZmODkwIn0.eyJhdWQiOiIxIiwianRpIjoiMTZhMzUzODA3YjJmYTFlNjk4OTRhNmEwM2I0NDY1MzEzNmE3ZTIxMWU2NDdjNjM2YWEyYmExNzcyZDExNjMwNzg2M2ExZDQxZDU0ZmY4OTAiLCJpYXQiOjE1NzYwNjAxMTgsIm5iZiI6MTU3NjA2MDExOCwiZXhwIjoxNjA3NjgyNTE4LCJzdWIiOiIyOTU2MSIsInNjb3BlcyI6W119.FbXSblNV4k4zkyq4-aVoZanXGl4hpFARD7Lc13U03Ml_vdW6VoHtYSoLSIeRwxDHveFXbHKoQaywElwujAGUH2BytRWW88IjHPL5hMJOblDBuPndoTlykxl5QWFFmPc1IU8ow2FT3AVO7_BKLKYS4QpKVPeQGPmCZ4P5fqKX5SKjmFkIm98ttBTJaEHTDfBiJh4c8ofqYFRymHIOkM49srAei4IftmbPuO-oysvoQEGBHP5U7tHvjeJd-uginPTHO_PmVclnmmdyCVuxKhsx6A-1zM6EcMkmcu9pGbmvXJNNsB4WBwhMaA18KVMscpistN6_guDetXl8BzupwncXf92kkc3ONMXoUyWaVUBxnrHyTN8HaRhiYlXCb6YvA7Bi75cGOmo4C6MIN5mwlwje4TPhrF91XGkuM0pldjBoHWL8lw1JwmlCEqLX1i9fhdXHiRrpZ8h3E27Qk4mrdXSgngoyNuFJSVnYGUX8OI4bK-4IN1PL0hwMDloeeXGBf76tRV04oeLKKfaAfwY8nvwVCJjWRb6r3ab_CyrgdcgNwXAqhRlIDtXYx92W7m4kbVWyLVbloWmdCbzZMZJrJNSmQkb39e418rNJsbI32mdVsuWmujJESipzNXXDmhWV9o74Vyeui1xTgrPR_Jd6FzqqKlTsDoNFGI-jX2q375Ji2MQ',
          'token_type' => 'Bearer',
          'expires_at' => Carbon::now()->toDateTimeString(),
        ]
      );
      return response()->json($userToken);
      $appId=6;
      $agent = UserPlatform::with(['apps' => function($q) use ($appId){
        $q->where('app_id', $appId)->where('role_id', 4);
      }])->get()->reject(function ($item) {
        return count($item->apps) == 0;
      })->values();
      return $agent;
      try{
        $redis=Redis::connect('127.0.0.1',6379);
        return response('redis working');
      }catch(\Predis\Connection\ConnectionException $e){
          return response('error connection redis');
      }
    }

    public function otherTest(Request $request)
    {
      $user = User::where('email', $request->email)->first();
      return response()->json($user);
    }

    public function encode(Request $request)
    {
        $data = BackendService::encryptText(env("ENCRYPT_KEY"), $request->value);
        return $data;
    }
    public function decodeBackend(Request $request)
    {
        $data = BackendService::decryptText(env("ENCRYPT_KEY"), $request->value);
        return $this->successResponse($data, "Success decode");
    }
    public function decrypt(Request $request)
    {
        if ($request->type == "message") {
          $data = EncryptService::decrypt($request->value, env('ENCRYPT_MESSAGE_KEY'), 20);
        }
        if ($request->type == "profile") {
          $data = BackendService::decryptText(env("ENCRYPT_PROFILE_KEY"), $request->value);
        }
        if ($request->type == "single") {
          $data = BackendService::decryptText(env("ENCRYPT_MESSAGE_KEY"), $request->value);
        }
        return $this->successResponse($data, "Success decrypt");
    }
    public function encrypt(Request $request)
    {
        if ($request->type == "message") {
          $data = EncryptService::encrypt($request->value, env('ENCRYPT_MESSAGE_KEY'), 20);
        }
        if ($request->type == "profile") {
          $data = BackendService::encryptText(env("ENCRYPT_PROFILE_KEY"), $request->value);
        }
        if ($request->type == "single") {
          $data = BackendService::encryptText(env("ENCRYPT_MESSAGE_KEY"), $request->value);
        }
        return $this->successResponse($data, "Success encrypt");
    }

    public function hitCounter(Request $request)
    {
      $user = UserPlatform::find($request->user_id);
      $apiLog = ApiLog::where('user_id',$request->user_id)->get();
      $remarks = json_decode($user->remarks,TRUE);
      foreach ($apiLog as $key => $value) {
        $response = json_decode($value->response,TRUE);
        $responses[] = $response;
      }
      $success = $failed = 0;
      foreach ($responses as $key => $value) {
        $status = $value['success'];
        if($status==true)
          $success++;

        if($status==false)
          $failed++;
      }
      return [
        'name'=>$user->name,
        'provider'=>$remarks['provider'],
        'limit'=>$remarks['limit'],
        'ipAddress'=>$remarks['ip_address'],
        'hitCount'=>count($apiLog),
        'successResponse'=>$success,
        'failedResponse'=>$failed,
        'tokenAccess'=>$user->email_verified_at==null ? false : true
      ];
    }

    public function testImage()
    {
      // $apiProp = $this->qontakApi->endpoints()->where('name', 'v1-media')->first();
        // $integrationData = json_decode($this->data['integration']);
        // $jar = new \GuzzleHttp\Cookie\CookieJar();

        // dd($mediaId = $message['image']['id']);
        // $mediaId = $message['image']['id'];
        // $path = public_path('/upload/whatsapp', 'test.jpg');
        // $file_path = fopen($path,'w');
        // $resource = fopen('/upload/whatsapp', 'w');
        // $stream = GuzzleHttp\Psr7\stream_for($resource);
      $client = new Client(['base_uri' => 'https://18.139.236.223:9103/', 'stream' => true]);

        try {
            $response = $client->request(
                'get',
                '/v1/media/6cbf2d8d-6501-4e8b-95e3-603c1c86dc95',
                [
                  'stream' => true,
                    // 'sink' => '/upload',
                    'http_errors' => true,
                    'verify' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer eyJhbGciOiAiSFMyNTYiLCAidHlwIjogIkpXVCJ9.eyJ1c2VyIjoiYWRtaW4iLCJpYXQiOjE1OTMwNTg0ODMsImV4cCI6MTU5MzY2MzI4Mywid2E6cmFuZCI6LTQ2NDA3MjY5Mjc5NDAyNDIwNzJ9.0ZdKDYIoEi7mggscwplG0a8uADGqA5xrcNtmkeH-mwQ'
                    ],
                ]
            );
            // fclose($resource);
        } catch (\Exception $e) {
            // return $e;
            $resp = json_decode($e->getResponse()->getBody());
            // return $resp->errors[0];
        }
        // dd($response->getBody());
        dd($response->getHeaders());
       $data = $response->getBody()->getContents();

       $getName = $response->getHeader('Content-Disposition')[0];
       $imgName = json_decode(str_replace("attachment; filename=", "", $getName));
       $name = $imgName."_wa.mp4";

        // $base64 = 'data:video/mp4;base64,' . base64_encode($data);
        // echo `<img src="'.$base64.'">`;

        // $name = time() . 'wa_.mp4';
        // $path = public_path().'/uploads/';
        Storage::disk('public_chat')->put($name, $data);
        return env("APP_URL") . "/upload/chat/" . $name;


        // return $data;
        // return  $response = json_decode($response->getBody()->getContents());
        // dd($response);

    }

    public function backendLogin(Request $request)
    {
      DB::beginTransaction();
      $backendHashids = new Hashids(env("SECRET_KEY"), '6');
      $apiProp = $this->lennaApi->endpoints()->where('name', 'login')->first();
      $appId = $this->appId = $request->appId;
      $requestData = [
        'email' => $request->email,
        'password' => BackendService::encryptText(env("ENCRYPT_KEY"), "$appId:" . $request->email),
        'fcm_token' => null,
        'client' => $request->channel
      ];
      try {
          $response = $this->client->request(
              $apiProp->method,
              str_replace("{app-id}", $this->backendAppId, $apiProp->endpoint),
              [
                  'json' => $requestData,
                  'allow_redirects' => false,
                  'debug' => true
              ]
          );
          $response = json_decode($response->getBody()->getContents());
      } catch (ServerException $e) {
          $endpoint = str_replace("{app-id}", $this->backendAppId, $apiProp->endpoint);
          ApiLog::create([
              'url'           => $this->lennaApi->base_url.$endpoint,
              'app_id'        => $this->appId,
              'user_id'       => 1,
              'request'       => json_encode($requestData),
              // 'response'    => $e->getMessage(),
              'response'      => $e->getResponse()->getBody(true),
              'ip_address'    => \Request::ip(),
              'user_agent'    => \Request::header('User-Agent'),
              'userable_type' => 'user',
              'userable_id'   => 1,
          ]);
          $response = false;
      } catch (ClientException $e) {
          $endpoint = str_replace("{app-id}", $this->backendAppId, $apiProp->endpoint);
          ApiLog::create([
              'url'           => $this->lennaApi->base_url.$endpoint,
              'app_id'        => $this->appId,
              'user_id'       => 1,
              'request'       => json_encode($requestData),
              // 'response'    => $e->getMessage(),
              'response'      => $e->getResponse()->getBody(true),
              'ip_address'    => \Request::ip(),
              'user_agent'    => \Request::header('User-Agent'),
              'userable_type' => 'user',
              'userable_id'   => 1,
          ]);
          $response = false;
      }
      if ($response == false) {
          DB::rollback();
          return response()->json(["success" => false]);
      }
      $userToken = UserToken::updateOrCreate(
        [
            'user_id' => $backendHashids->decode($response->data->id)[0]
        ],
        [
            'access_token' => $response->access_token,
            'token_type' => $response->token_type,
            'expires_at' => $response->expires_at,
        ]
      );
      DB::commit();
      return response()->json($response);
    }

    public function getTopStory(Request $request)
    {
      // $start = '2020-04-01';
      // $end = '2020-04-15';
      $appId = 72;
      $start = $request->start;
      $end = $request->end;
      $integration = Integration::where('app_id', $appId)->where('channel_id', 6)->first();
        $error = 'Please connect your own bot';
        if (!$integration) {
            return $this->successResponse(null, $error);
        } else if ($integration->status == false) {
            return $this->successResponse(null, $error);
        }

        $botId = $integration->integration_data['botId'];
        //get bot id

        // get total story
        $totalStory = DB::table(env("DB_BOT_SCHEMA") . ".stories")->where("bot_id", $botId)->count();
        $totalRequest = DB::table('bot.requests as r')->join('bot.stories as s', 'r.story_id', '=', 's.id')->where('s.bot_id', $botId)->count();

        $story = DB::table(env("DB_BOT_SCHEMA") . ".stories")->where("bot_id", $botId)->get();
        $getTopRequestStory = $datas = [];

        foreach ($story as $key => $value) {
            $getTopRequestStory =  DB::table("bot.requests as r")
                                            ->join("bot.interactions as i", "r.story_id", "=", "i.id")
                                            ->join("bot.stories as s", "s.id", "=", "r.story_id")
                                            ->where('r.story_id', $value->id)
                                            ->whereBetween('r.created_at', [$start, $end])
                                            // ->whereDate('r.created_at', '=>', '2020-04-01')
                                            // ->whereDate('r.created_at', '=>', '2020-04-30')
                                            // ->take(10)
                                            ->count();
                                            // ->take(1)->get();

            $datas[] = [
                'story' => $value->title,
                'total' => $getTopRequestStory
            ];
        }

        $collectDatas = collect($datas);
        $sortDatas =  $collectDatas->sortByDesc('total')->values()->take(35);
        return response()->json([
            "success" => true,
            "message" => null,
            "data" => [
                'total_story' => $totalStory,
                'total_request' => $totalRequest,
                'data' => $sortDatas
            ]
        ]);
    }

    public function mobileDecrypt(Request $request) {
      return response()->json($request->all());
    }

    public function updateSubMenu(Request $request) {
      $data = RoleMenu::where('platform_id', env('OMNI_PLATFORM_ID'))->where('menu_id', 4)->update([
        'submenu_id' => json_encode([21,22,23,24,25,26,27,28,29])
      ]);
      return response()->json($data);
    }
    public function webhookTwitter(Request $request)
    {
      // $consumerKey2 = 'sIIRT0NOHGzerGWUQOtU7LTUs';
      // $consumerSecret2 = 'zZdt5xe8Qoq0qzl453bd7U8Jbti6enKOsNauiZbQFVlaBMTG32';
      // $access_token2 = '359636327-vOYkquJagovdAiPowG5KB93PHej7cLtyytMzXOkZ';
      // $access_token_secret2 = '1U3ufeFFvz0d54F1LZeFtz7CrqiEAYWx4jHQgk2Ya0oxO';
      // $connection2 = new TwitterOAuth($consumerKey2, $consumerSecret2, $access_token2, $access_token_secret2);

      // $consumerKey = 'KDhlFvNGETjYckmEk5Zjvglno';
      // $consumerSecret = 'weTZp15YWxKPdqg2HMUS30U6ltcaDNOJGmwFs7lNhC4G3Dzpen';
      // $access_token = '359636327-laqr2I7Vkdw6Dpr0p3jm850AXnvR6ZbvDJ7eUNCC';
      // $access_token_secret = 'BANeuIGYc8qyVSLgWgWPdbimszYuwQCkGNtqSLFsaI6Xj';

      // lenna
      $consumerKey = 'Uvihkn2wNDOMrxJDKOVJmFjqA';
      $consumerSecret = 'VfJ9Y6NQsCSvPM1joluHME4siMvfH84vEIZfTHxZtEi7Dwc9pQ';
      $access_token = '1335888582788009987-VC985attFFNyi06TubjEchr86UFnUB';
      $access_token_secret = '2vGt4keobqo1tQQJMFeCzgPZJfgLhxnmvf7WA0icISO7h';

      $connection = new TwitterOAuth($consumerKey, $consumerSecret, $access_token, $access_token_secret);


      // $content = $connection->get("account/verify_credentials");
      // $statues = $connection->post("statuses/update", ["status" => "test api"]);
      // $statues = $connection->post("statuses/update", ["status" => "hello world"]);
      // $userId = '1300232920611790850';
      // $data = [
      //   'event' => [
      //       'type' => 'message_create',
      //       'message_create' => [
      //           'target' => [
      //               'recipient_id' => $userId
      //           ],
      //           'message_data' => [
      //               'text' => 'Hello bima!'
      //           ]
      //       ]
      //   ]
      // ];
      // $result = $connection->post('direct_messages/events/new', $data, true); // Note the true
      // $showDM = $connection->get('direct_messages/events/list');
      // return response()->json($showDM);
      // $url = 'https://dev.lenna.ai/app/public/api/lejRej/webhook/twitter';
      // $setWebhook = $connection->post('account_activity/all/dev6/webhooks', ['url' => $url]);
      // return response()->json($setWebhook);

      // $connection = new TwitterOAuth($consumerKey, $consumerSecret);
      // $request_token = $connection->oauth2('oauth2/token', ['grant_type' => 'client_credentials']);
      // $connection = new TwitterOAuth($consumerKey, $consumerSecret, $access_token);

      //delete webhook
      // $content = $connection->delete("account_activity/all/devLenna/webhooks/1337000214557720577");
      // return response()->json($content);

      //check webhook
      $content = $connection->get("account_activity/all/webhooks");
      return response()->json($content);

      // $content = $connection->get("account_activity/all/webhooks");
      // print_r($content);

      // $content = $connection->post("account_activity/all/dev6/subscriptions");
      // print_r($content);

      // $request_token = $connection->oauth2('oauth2/token', ['grant_type' => 'client_credentials']);
      // $connection = new TwitterOAuth($consumerKey, $consumerSecret, $request_token->access_token);
      // $content = $connection->get("account_activity/all/dev6/subscriptions/list");
      // print_r($content);
    }

    public function webhookTwitter2(Request $request)
    {
      // $consumerKey = 'KDhlFvNGETjYckmEk5Zjvglno';
      // $consumerSecret = 'weTZp15YWxKPdqg2HMUS30U6ltcaDNOJGmwFs7lNhC4G3Dzpen';
      // $access_token = '359636327-laqr2I7Vkdw6Dpr0p3jm850AXnvR6ZbvDJ7eUNCC';
      // $access_token_secret = 'BANeuIGYc8qyVSLgWgWPdbimszYuwQCkGNtqSLFsaI6Xj';

      // lenna
      $consumerKey = 'Uvihkn2wNDOMrxJDKOVJmFjqA';
      $consumerSecret = 'VfJ9Y6NQsCSvPM1joluHME4siMvfH84vEIZfTHxZtEi7Dwc9pQ';
      $access_token = '1335888582788009987-VC985attFFNyi06TubjEchr86UFnUB';
      $access_token_secret = '2vGt4keobqo1tQQJMFeCzgPZJfgLhxnmvf7WA0icISO7h';



      if (($request['crc_token'])) {
        $signature = hash_hmac('sha256', $request['crc_token'], $consumerSecret, true);
        $response['response_token'] = 'sha256='.base64_encode($signature);
        print json_encode($response);
        // $token = "sha256=".base64_encode($signature);
        // $resp =  array ("response_token" => $token);
        // return response()->json(['response_token' => $token]);
      } else {

        // $eventJSON = file_get_contents('php://input');
        // $myfile = fopen(storage_path("events.txt"), "w") or die("Unable to open file!");
        // fwrite($myfile, $eventJSON . PHP_EOL);
        // fclose($myfile);

        $glitch = 'https://abrupt-familiar-feta.glitch.me/';
        $guzzleProp = [
          'base_uri' => $glitch
        ];

        $client = new Client($guzzleProp);

        $response =  $client->request(
          'POST',
          'webhook',
          [
            'json' => [
              $request->all()
            ]
          ]
        );

        $responses = json_decode($response->getBody());
        return response()->json([
              'response' => $responses,
              'request' => $request->input()
        ]);

      }

    }

    public function getMedia(Request $request)
    {
      // lenna
      $consumerKey = 'Uvihkn2wNDOMrxJDKOVJmFjqA';
      $consumerSecret = 'VfJ9Y6NQsCSvPM1joluHME4siMvfH84vEIZfTHxZtEi7Dwc9pQ';
      $access_token = '1335888582788009987-VC985attFFNyi06TubjEchr86UFnUB';
      $access_token_secret = '2vGt4keobqo1tQQJMFeCzgPZJfgLhxnmvf7WA0icISO7h';
      $connection = new TwitterOAuth($consumerKey, $consumerSecret, $access_token, $access_token_secret);

      // $baseUrlImage = "https://ton.twitter.com/";
      // $guzzleProp = ['base_uri' => $baseUrlImage];

      $stack = HandlerStack::create();
      $middleware = new Oauth1([
          'consumer_key'    => $consumerKey,
          'consumer_secret' => $consumerSecret,
          'token'           => $access_token,
          'token_secret'    => $access_token_secret
      ]);
      $stack->push($middleware);

      $client = new Client([
          'base_uri' => 'https://ton.twitter.com/1.1/',
          'handler' => $stack,
          'auth' => 'oauth'
      ]);

      try {
        $response = $client->get("ton/data/dm/1337230441837776902/1337230423219265536/s4IPQMWs.png");
      } catch (\Exception $e) {
        $resp = json_decode($e->getResponse()->getBody());
            return $resp;
      }

      return $getName = $response->getHeader();

      return $responses = ($response->getBody()->getContents());
      return response()->json([
        'response' => $responses
      ]);
    }


    public function getHsmReport(Request $request) {
      $hsmReport = DB::table("broadcast_messages")->where('app_id', $request->appId)
                      ->where("category", 'hsm')
                      ->where("channel_id", 4)
                      ->whereDate('created_at', '>=', $request->start)
                      ->whereDate('created_at', '<=', $request->end)
                      ->orderBy("created_at", "DESC")
                      ->get();
        return response()->json([
          $hsmReport
        ]);
    }

    public function migrateStoryGroupIcon(Request $request) {
      $storyGroups = StoryGroup::all();
      $result = [];
      try {
        DB::beginTransaction();
        foreach ($storyGroups as $key => $value) {
          $story = StoryGroup::find($value['id']);
          $story->color = rand(1,4).'-'.rand(1,10);
          $story->save();
          $result[] = $story;
        }
      } catch (Exception $e) {
        DB::rollback();
        return ["error" => $e];
      }
      DB::commit();
      return $result;
    }
    public function testInterval(Request $request)
    {
      // $test = 10;
      // return $test % 10;
      // return $request;
      // $data = DB::table('broadcast_messages')->take(100)->get();
      // return $data;


      $glitch = 'https://fork-shell-kettle.glitch.me/';
      $guzzleProp = [
        'base_uri' => $glitch
      ];
      $client = new Client($guzzleProp);



      foreach (array_chunk($request->phone,5) as $key => $value) {

        foreach ($value as $key2 => $v) {
          $response =  $client->request(
            'POST',
            'webhook',
            [
              'json' => [
                $v, "total memory usage ",
              ]
            ]
          );
        }
        sleep(5);


      }



      //   foreach ($request->phone as $key => $value) {

      //     $response =  $client->request(
      //       'POST',
      //       'webhook',
      //       [
      //         'json' => [
      //           $value
      //         ]
      //       ]
      //     );

        if(($key !== 0) && ($key%10) == 0){ // % = sisa bagi
          // echo "===DELAY=== " . $count . "<br>";
          $response =  $client->request(
            'POST',
            'webhook',
            [
              'json' => [
                "===DELAY=== : ".$key
              ]
            ]
          );
          sleep(5); // this should halt for 3 seconds for every loop
        }

    //   }

    }

    public function clearDoubleRoom()
    {
      $select = DB::select('select * from (
        select id, created_by, app_id, channel_id, created_at, updated_at, count(*)
        over(partition by created_by) as cnt
        from omnichannel.rooms) t
        where t.cnt>=2
        order by created_by ,id asc;');
      DB::beginTransaction();
      foreach ($select as $key1 => $value1) {
        foreach ($select as $key2 => $value2) {
          if ($value2->created_by == $value1->created_by) {
            Message::where('room_id', $value2->id)->update(['room_id' => $value1->id]);
            TimeReportServed::where('room_id', $value2->id)->update(['room_id' => $value1->id]);
            Session::where('room_id', $value2->id)->update(['room_id' => $value1->id]);
            RoomTag::where('room_id', $value2->id)->update(['room_id' => $value1->id]);
            Livechat::where('room_id', $value2->id)->update(['room_id' => $value1->id]);
            DB::table('submit_ticket')->where('room_id', $value2->id)->update(['room_id' => $value1->id]);
            DB::table('additional_informations')->where('room_id', $value2->id)->update(['room_id' => $value1->id]);
            DB::table('notes')->where('room_id', $value2->id)->update(['room_id' => $value1->id]);
            DB::table('log_handle_room')->where('room_id', $value2->id)->update(['room_id' => $value1->id]);
            Room::where('id', $value2->id)->delete();
          }
        }
      }
      DB::commit();
      $select2 = DB::select('select * from (
        select id, created_by, app_id, channel_id, created_at, updated_at, count(*)
        over(partition by created_by) as cnt
        from omnichannel.rooms) t
        where t.cnt>=2
        order by created_by ,id asc;');
      return response()->json($select2);
    }

    public function clearDoubleLivechat()
    {
      $select = DB::select('select * from (
        select id, room_id, end_at, request_at, start_at, status, handle_by, updated_by, group_id, count(*)
        over(partition by room_id) as cnt
        from omnichannel.livechats) t
        where t.cnt>=2
        order by room_id,id asc;');
      DB::beginTransaction();
      $roomNotFound = [];
      foreach ($select as $key1 => $value1) {
        $room = Room::select('id')->where('id', $value1->room_id)->first();
        if (!$room) {
          $roomNotFound[] = $value1->room_id;
        }
        foreach ($select as $key2 => $value2) {
          if ($value2->room_id == $value1->room_id) {
            Livechat::where('room_id', $value2->room_id)->update(['room_id' => $value1->room_id]);
            Livechat::where('room_id', $value2->room_id)->delete();
          }
        }
      }
      $select2 = DB::select('select * from (
        select id, room_id, end_at, request_at, start_at, status, handle_by, updated_by, group_id, count(*)
        over(partition by room_id) as cnt
        from omnichannel.livechats) t
        where t.cnt>=2
        order by room_id,id asc;');
      if (count($roomNotFound) > 0) {
        $uniqueRoomNotFound = array_unique($roomNotFound);
        $changed = [];
        foreach ($uniqueRoomNotFound as $key => $value) {
          $msg = Message::where('room_id', $value)->where('messageable_type','user')->first();
          if ($msg) {
            $oldRoom = Room::where('created_by', $msg->messageable_id)->first();
            if ($oldRoom == null) {
              $u = User::find('id', $msg->messageable_id);
              if ($u) {
                $newRoom = new Room;
                $newRoom->id = $value;
                $newRoom->app_id = $u->app_id;
                $newRoom->channel_id = $u->channel_id;
                $newRoom->save();
                // Room::create([
                //   'id' => $value,
                //   'app_id' => $u->app_id,
                //   'channel_id' => $u->channel_id
                // ]);
              } else {
                Message::where('room_id', $value)->delete();
                TimeReportServed::where('room_id', $value)->delete();
                Session::where('room_id', $value)->delete();
                RoomTag::where('room_id', $value)->delete();
                Livechat::where('room_id', $value)->delete();
                DB::table('submit_ticket')->where('room_id', $value)->delete();
                DB::table('additional_informations')->where('room_id', $value)->delete();
                DB::table('notes')->where('room_id', $value)->delete();
                DB::table('log_handle_room')->where('room_id', $value)->delete();
              }
            } else {
              if ($oldRoom->id !== $value) {
                Message::where('room_id', $value)->update(['room_id' => $oldRoom->id]);
                TimeReportServed::where('room_id', $value)->update(['room_id' => $oldRoom->id]);
                Session::where('room_id', $value)->update(['room_id' => $oldRoom->id]);
                RoomTag::where('room_id', $value)->update(['room_id' => $oldRoom->id]);
                Livechat::where('room_id', $value)->update(['room_id' => $oldRoom->id]);
                DB::table('submit_ticket')->where('room_id', $value)->update(['room_id' => $oldRoom->id]);
                DB::table('additional_informations')->where('room_id', $value)->update(['room_id' => $oldRoom->id]);
                DB::table('notes')->where('room_id', $value)->update(['room_id' => $oldRoom->id]);
                DB::table('log_handle_room')->where('room_id', $value)->update(['room_id' => $oldRoom->id]);
              }
              $changed[] = [
                'before' => $value,
                'after' => $oldRoom->id
              ];
            }
          }
        }
      } else {
        $uniqueRoomNotFound = 0;
      }
      DB::commit();
      return response()->json([
        'current_double_data' => $select2,
        'room_not_found' => $uniqueRoomNotFound,
        'changed_room' => $changed
      ]);
    }

    public function createroom(Request $request)
    {
      $rooms = array_unique($request->rooms);
      $changed = [];
      $freshRoom = [];
      $deletedRoom = [];
      foreach ($rooms as $key => $value) {
        $msg = Message::where('room_id', $value)->where('messageable_type','user')->first();
        if ($msg) {
          $oldRoom = Room::where('created_by', $msg->messageable_id)->first();
          if ($oldRoom == null) {
            $u = User::find($msg->messageable_id);
            if ($u) {
              $newRoom = new Room;
              $newRoom->id = $value;
              $newRoom->app_id = $u->app_id;
              $newRoom->channel_id = $u->channel_id;
              $newRoom->created_by = $u->id;
              $newRoom->save();
              $freshRoom[] = $newRoom;
              // Room::insert([
              //   'id' => $value,
              //   'app_id' => $u->app_id,
              //   'channel_id' => $u->channel_id,
              //   'created_at' => Carbon::now('Asia/Jakarta')->toDateTimeString(),
              //   'updated_at' => Carbon::now('Asia/Jakarta')->toDateTimeString(),
              //   'created_by' => $u->id
              // ]);
            } else {
              Message::where('room_id', $value)->delete();
              TimeReportServed::where('room_id', $value)->delete();
              Session::where('room_id', $value)->delete();
              RoomTag::where('room_id', $value)->delete();
              Livechat::where('room_id', $value)->delete();
              DB::table('submit_ticket')->where('room_id', $value)->delete();
              DB::table('additional_informations')->where('room_id', $value)->delete();
              DB::table('notes')->where('room_id', $value)->delete();
              DB::table('log_handle_room')->where('room_id', $value)->delete();
              $deletedRoom[] = $value;
            }
          } else {
            if ($oldRoom->id !== $value) {
              Message::where('room_id', $value)->update(['room_id' => $oldRoom->id]);
              TimeReportServed::where('room_id', $value)->update(['room_id' => $oldRoom->id]);
              Session::where('room_id', $value)->update(['room_id' => $oldRoom->id]);
              RoomTag::where('room_id', $value)->update(['room_id' => $oldRoom->id]);
              Livechat::where('room_id', $value)->update(['room_id' => $oldRoom->id]);
              DB::table('submit_ticket')->where('room_id', $value)->update(['room_id' => $oldRoom->id]);
              DB::table('additional_informations')->where('room_id', $value)->update(['room_id' => $oldRoom->id]);
              DB::table('notes')->where('room_id', $value)->update(['room_id' => $oldRoom->id]);
              DB::table('log_handle_room')->where('room_id', $value)->update(['room_id' => $oldRoom->id]);
            }
            $changed[] = [
              'before' => $value,
              'after' => $oldRoom->id
            ];
          }
        }
      }
      $result = [
        'changed' => $changed,
        'newRoom' => $freshRoom,
        'deletedRoom' => $deletedRoom
      ];
      return response()->json($result);
    }

    public function getIkeaChatHistory(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'type' => 'required|string',
        'per_page' => 'required|string'
      ]);
			if ($validator->fails()) {
        return $this->errorResponse(null, $validator->errors());
      }
      switch ($request->type) {
        case 'csc':
          $appId = 404;
          break;
        case 'als':
          $appId = 95;
          break;
        case 'stl':
          $appId = 97;
          break;
        default:
          $appId = 0;
          break;
      }
      if ($appId == 0) {
        return $this->errorResponse(null, "type $request->type not found");
      }
      $room = Room::with(['messages' => function($q) {
        $q->select('id','room_id','messageable_type','messageable_id','content','created_at','encrypt');
      }])
        ->where('app_id', $appId)
        ->where('created_at','>=', '2021-09-18 00:00:00')
        ->where('created_at', '<=', '2021-09-21 21:00:00')
        ->orderBy('created_at', 'desc')
        ->simplePaginate($request->per_page);
      return $this->successResponse($room, "ok");
    }

}
