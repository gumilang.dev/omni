<?php

namespace App\Modules\GeneralValue\Controllers;

use Illuminate\Http\Request;
use App\Modules\GeneralValue\Models\GeneralValue;
use App\Http\Controllers\ApiController;

class GeneralValueController extends ApiController
{
    public function index(Request $request, $appId)
    {
        $sort = $request->get('sort');
        $filterText = $request->get('filterText');
        $per_page = $request->get('per_page');
        $generalValue = GeneralValue::whereNotIn('key', ['auto-response-office-hour','auto-response-outside-office-hour']);

        if ($sort) {
            $sort = explode('|', $sort);
            $generalValue = $generalValue->orderBy($sort[0], $sort[1]);
        }

        if ($filterText) {
            $generalValue = $generalValue->where(function ($query) use ($filterText) {
                $query->where('key', 'ILIKE', '%' . $filterText . '%')->orWhere('value', 'ILIKE', '%' . $filterText . '%');
            });
        }

        // if ($category) {
        //     $generalValue = $generalValue->where("category",$category);
        // }

        if (!$per_page) {
            $per_page = 15;
        }

        $generalValue = $generalValue->where('app_id', $appId);

        return response()->json($generalValue->paginate($per_page));
    }

    public function store(Request $request, $appId)
    {
        $this->validate($request, [
            'key' => 'required|string',
            'value' => 'required',
        ]);

        $generalValue = GeneralValue::create([
            'app_id' => $appId,
            'key' => $request->key,
            'value' => $request->value,
            'value_type' => $request->valueType,
            'description' => $request->description,
            'created_by' => $request->decodedUserId,

        ]);

        return $this->successResponse(['generalValue' => $generalValue], 'general value created');

    }

    public function edit($id)
    {
        return $id;
    }

    public function update(Request $request, $appId, $generalValueId)
    {
        $validatedData = $request->validate([
            'key' => 'required',
            'value' => 'required',
        ]);

        $data = [
            'key' => $request->key,
            'value' => $request->value,
            'value_type' => $request->value_type,
            'description' => $request->description,
            'is_active' => $request->is_active,
            'updated_by' => $request->decodedUserId,

        ];

        $generalValue = GeneralValue::where('id', $generalValueId)->update($data);
        return $this->successResponse(['generalValue' => $generalValue], 'general value updated');
    }

    public function destroy($appId, $generalValueId)
    {
        $generalValue = GeneralValue::findOrFail($generalValueId);
        $generalValue->delete();
        return $this->successResponse(null, 'general value deleted');

    }

    public function getByValue(Request $request, $appId)
    {
        $result = GeneralValue::where('app_id', $appId)->where('key', $request->key)->first();
        return $this->successResponse($result);
    }
}
