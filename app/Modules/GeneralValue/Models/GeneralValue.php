<?php

namespace App\Modules\GeneralValue\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GeneralValue extends Model
{
    use SoftDeletes;
    protected $connection = 'pgsql';

    protected $guarded = [];

    public function App()
    {
        return $this->belongsTo("App\Modules\App\Models\App");
    }
}
