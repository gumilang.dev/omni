<?php

namespace App\Modules\Audit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;


class Audit extends Model
{
    protected $casts = [
			'old_values' 			=> 'json',
			'new_values' 			=> 'json'
    ];

    public function getAuditableTypeAttribute($value)
    {
        return str_replace("App\\", "", $value);
		}

		public function getUserTypeAttribute($value)
    {
        return str_replace("App\\", "", $value);
		}
}
