<?php

namespace App\Modules\Role\Models;

use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;
    protected $connection = 'auth';
    // protected $hidden = [
    //   'id', 'name', 'hashed_id', 'ranking', 'privilege', 'slug'
    // ];
    protected $appends = ['hashed_id',];
    protected $casts = [
      'ranking' => 'double',
      'privilege' => 'array',
      'accessibilities' => 'array',
    ];
    protected $fillable = ['app_id','deleted_by'];

    public function getHashedIdAttribute()
    {
        $hash = new Hashids('',6);
        return $hash->encode($this->id);
    }
    public function userRole()
    {
        return $this->hasMany('App\Modules\User\Models\UserRole');
    }
    public function userApps()
    {
        return $this->hasMany('App\Modules\User\UserApp');
    }
    public function roleMenus()
    {
        return $this->hasMany('App\Modules\Role\Models\RoleMenu');
    }
}
