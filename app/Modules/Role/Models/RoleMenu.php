<?php

namespace App\Modules\Role\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Menu\Models\Menu;

class RoleMenu extends Model
{
    protected $connection = 'auth';
    protected $casts = [
        'submenu_id' => 'array',
    ];
    protected $appends = ['menu'];
    protected $hidden = ['submenu_id','menu_id'];
    protected $fillable = [
      'menu_id',
      'role_id',
      'created_by',
      'updated_by'
    ];
    public function getMenuAttribute()
    {
        $menu = Menu::with(['submenus' => function($q) {
            $q->whereIn('id', json_decode($this->attributes['submenu_id'], true));
        }])->where('id', $this->attributes['menu_id'])->first();
        return $menu;
    }
    public function submenu()
    {
        return $this->hasOne('App\Modules\Menu\Models\SubMenu', 'id', 'menu_id');
    }
    public function menu()
    {
        return $this->belongsTo('App\Modules\Menu\Models\Menu', 'menu_id');
    }
    public function role()
    {
      return $this->belongsTo('App\Modules\Role\Models\Role');
    }
}
