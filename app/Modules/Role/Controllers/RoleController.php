<?php

namespace App\Modules\Role\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Modules\Role\Models\Role;
use App\Modules\Role\Models\RoleMenu;
use Hashids\Hashids;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

class RoleController extends ApiController
{
    public function __construct()
    {
        $this->hashids = new Hashids('', 6);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($appId)
    {
      $roles = Role::where('app_id', $appId)->get();
      // $roles = $roles->map(function($value, $key) {
      //   return [
      //     'id' => $this->hashids->encode($value['id']),
      //     'name' => $value['name']
      //   ];
      // });

      foreach ($roles as $key => $value) {
        if ($value['name'] == 'Staff') {
          $value['name'] = 'Agent';
        }
      }
      return $roles;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $appId)
    {
      $validator = Validator::make($request->all(), [
        'roleName'    => [
          'required',
          'bail',
          'max:50',
          Rule::unique('auth.roles', 'name')->where(function ($q) use ($appId){
            return $q->where('app_id', $appId)->whereNull('deleted_at');
          })
        ],
        'roleSlug'    => [
          'required',
          'max:50',
          'bail',
          'alpha_dash',
          Rule::unique('auth.roles', 'slug')->where(function ($q) use ($appId){
            return $q->where('app_id', $appId)->whereNull('deleted_at');
          })
        ],
        'roleRanking' => 'required|numeric|max:10|min:2|bail',
        'privileges' => 'present|bail',
        'accessibilities' => 'present|bail'
      ]);
      if ($validator->fails()) {
        return $this->errorResponse(null, $validator->errors()->first());
      } else {
        $role                   = new Role;
        $role->app_id           = $appId;
        $role->slug             = htmlspecialchars($request->roleSlug);
        $role->name             = htmlspecialchars($request->roleName);
        $role->ranking          = htmlspecialchars($request->roleRanking);
        $role->privilege        = $request->privileges ?? [];
        $role->accessibilities  = $request->accessibilities ?? [];
        $role->created_by       = $request->decodedUserId;
        $role->updated_by       = $request->decodedUserId;
        $role->new_platform     = true;
        $role->save();
      //   foreach ($request->menus as $key => $value) {
      //     if ($value['enable']) {
      //       $submenuId = collect($value['submenus'])->reject(function ($q) {
      //         return $q['enable'] == false;
      //       })->map(function($map) {
      //         return $map['id'];
      //       })->values();
      //       RoleMenu::insert([
      //         'role_id'     => $role->id,
      //         'menu_id'     => $value['id'],
      //         'created_by'  => $request->decodedUserId,
      //         'updated_by'  => $request->decodedUserId,
      //         'platform_id' => env('OMNI_PLATFORM_ID'),
      //         'submenu_id'  => json_encode($submenuId),
      //         'product_id'  => $appId,
      //       ]);
      //     }
      //   }
      }
      return $this->successResponse($role, "Success create new role");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($appId, $roleId)
    {
        // $role = Role::whereHas('roleMenus')->where('id', $roleId)->get();
        // return $this->successResponse($role, "Success get role");
        $roleMenu = RoleMenu::where('platform_id', env('OMNI_PLATFORM_ID'))->where('product_id', $appId)->where('role_id', $roleId)->get()->pluck('menu');
        return $this->successResponse($roleMenu, "Success get role menu");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $appId, $id)
    {
      $validator = Validator::make($request->all(), [
        'roleName'    => [
          'required',
          'bail',
          'max:50',
          Rule::unique('auth.roles', 'name')->where(function ($q) use ($appId){
            return $q->where('app_id', $appId)->whereNull('deleted_at');
          })->ignore($id)
        ],
        'roleSlug'    => [
          'required',
          'max:50',
          'bail',
          'alpha_dash',
          Rule::unique('auth.roles', 'slug')->where(function ($q) use ($appId){
            return $q->where('app_id', $appId)->whereNull('deleted_at');
          })->ignore($id)
        ],
        'roleRanking' => 'required|numeric|max:10|bail',
        'roleAccessibilities' => 'present|bail',
        'rolePrivilege' => 'present|bail'
      ]);
      if ($validator->fails()) {
        return $this->errorResponse(null, $validator->errors()->first());
      } else {
        DB::beginTransaction();
        $role             = Role::find($id);
        if (!$role) {
          return $this->errorResponse(null, 'Role not found');
        }
        $role->slug             = htmlspecialchars($request->roleSlug);
        $role->name             = htmlspecialchars($request->roleName);
        $role->ranking          = htmlspecialchars($request->roleRanking);
        $role->privilege        = $request->rolePrivilege;
        $role->accessibilities  = $request->roleAccessibilities;
        $role->updated_by       = $request->decodedUserId;
        $role->new_platform     = true;
        $role->save();
        // RoleMenu::where('platform_id', env('OMNI_PLATFORM_ID'))->where('product_id', $appId)->where('role_id', $role->id)->delete();
        // foreach ($request->menus as $key => $value) {
        //   if ($value['enable']) {
        //     $submenuId = collect($value['submenus'])->reject(function ($q) {
        //       return $q['enable'] == false;
        //     })->map(function($map) {
        //       return $map['id'];
        //     })->values();
        //     RoleMenu::insert([
        //       'role_id'     => $role->id,
        //       'menu_id'     => $value['id'],
        //       'created_by'  => $request->decodedUserId,
        //       'updated_by'  => $request->decodedUserId,
        //       'platform_id' => env('OMNI_PLATFORM_ID'),
        //       'submenu_id'  => json_encode($submenuId),
        //       'product_id'  => $appId,
        //     ]);
        //   }
        // }
        DB::commit();
      }
      return $this->successResponse(null, "Success update role");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $appId)
    {
        DB::beginTransaction();
        RoleMenu::where('role_id', $request->id)->update([
          'deleted_by' => $request->decodedUserId
        ]);
        Role::where('app_id', $appId)->where('id', $request->id)->update([
          'deleted_by' => $request->decodedUserId
        ]);
        Role::where('app_id', $appId)->where('id', $request->id)->delete();
        RoleMenu::where('role_id', $request->id)->delete();
        DB::commit();
        return $this->successResponse(null, "Success delete role");
    }

    public function paginate(Request $request, $appId)
    {
      $sort = $request->get('sort');
      $filterText = $request->get('filterText');
      $per_page = $request->get('per_page');

      $roles = Role::select('id','name','slug','ranking','privilege','app_id','accessibilities')->where('app_id', $appId);

      if($filterText) {
        $roles = $roles->where('name', 'ilike', '%'.$filterText.'%');
      }

      if ($sort) {
        $sort = explode('|', $sort);
        $roles = $roles->orderBy($sort[0], $sort[1]);
      }

      if (!$per_page) {
        $per_page = 15;
      }

      $roles = $roles->paginate($per_page);

      return $roles;
    }

    public function getDetailRole(Request $request, $appId, $roleId)
    {
      $role = Role::find($roleId);
      if (!$role) {
        return $this->errorResponse(null, 'Role not found');
      }
      // $roleMenu = RoleMenu::with(['menu','menu.submenus'])->where('platform_id', env('OMNI_PLATFORM_ID'))->where('product_id', $appId)->where('role_id', $roleId)->get();
      // $data = [
      //   'menu' => collect($roleMenu)->map(function($q) {
      //     return $q->menu;
      //   }),
      //   'role' => $role
      // ];
      return $this->successResponse($role, "Success get role");
    }
}
