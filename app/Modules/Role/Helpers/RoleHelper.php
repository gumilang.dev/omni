<?php

namespace App\Modules\Role\Helpers;

use Carbon\Carbon;

use DB;
use App\Modules\User\Models\UserRole;
use App\Modules\Role\Models\Role;
use App\Modules\User\Models\UserApp;
use App\Modules\User\Models\UserPlatform;
use App\Modules\App\Models\App;
use App\Modules\Role\Models\RoleMenu;
use App\Modules\Module\Models\Module;
use App\Modules\Menu\Models\Menu;
use App\Modules\Integration\Models\Integration;

class RoleHelper
{
  public static function isAgent($data = [])
  {
    $user = DB::table('omnichannel.app_user_platform')
      ->where('omnichannel.app_user_platform.app_id', $data['appId'])
      ->where('omnichannel.app_user_platform.user_id', $data['userId'])
      ->join('auth.roles', function($q) use ($data) {
        $q->on('omnichannel.app_user_platform.role_id', '=', 'auth.roles.id')
          ->where('auth.roles.app_id', $data['appId'])
          ->where('auth.roles.slug', 'staff');
      })
      ->select('omnichannel.app_user_platform.user_id')
      ->get();
    // $user = UserApp::where('app_id', $data['appId'])->where('role_id', 4)->where('user_id', $data['userId'])->get();
    return count($user) > 0;
  }

  public static function isCapableToChangeRole($data = [])
  {
    // $editor_role  = UserRole::where('platform_id', env('OMNI_PLATFORM_ID'))->where('product_id', $data['appId'])->where('user_id', $data['editorId'])->first();
    // $user_role    = UserRole::where('platform_id', env('OMNI_PLATFORM_ID'))->where('product_id', $data['appId'])->where('user_id', $data['userId'])->first();
    $editor_role  = UserApp::where('app_id', $data['appId'])->where('user_id', $data['editorId'])->first();
    $user_role    = UserApp::where('app_id', $data['appId'])->where('user_id', $data['userId'])->first();
    $app          = App::find($data['appId']);

    $editorRoleCheck = Role::where('id', $editor_role->role_id)->first();
    $userRoleCheck = Role::where('id', $user_role->role_id)->first();

    if ($editorRoleCheck->ranking < $userRoleCheck->ranking) { // jika urutan ranking role editor lebih rendah dengan role target
      if ($app->created_by == $data['userId']) { // jika target yang ingin rolenya diedit adalah sang pembuat app
        return false;
      } else if ($data['editorId'] == $data['userId']) { // jika id user yang ingin rolenya diedit sama dengan id user sang pengedit role
        return false;
      }
      return true;
    }
    return false;
  }

  public static function haveRoleBot($data = [])
  {
    $integration   = Integration::where('app_id', $data['appId'])->whereHas('channel', function($q){
      $q->where('name', 'bot');
    })->first();
    $userBot = UserPlatform::where('id', $data['userId'])->with(['bots' => function($q) use ($integration) {
      $q->where('bots.id', $integration->integration_data['botId']);
    }])->first();
    if ($userBot->bots->count()) {
      return true;
    } else {
      return false;
    }
  }

  public static function menuPrivileges($roleId, $appId)
  {
    $menus = RoleMenu::where('role_id', $roleId)
      ->where('platform_id', env('OMNI_PLATFORM_ID'))
      ->where('product_id', $appId)
      ->get();

    $menus = $menus->map(function ($value) {
      return $value->menu;
    });
    // $module = Module::where('id', $menus[0]['module_id'])->first();
    $menus = $menus->sortBy('id')->values();
    // $data = [
    //   'id' => $module->id,
    //   'name' => $module->name,
    //   'slug' => $module->slug,
    //   'menu' => $menus
    // ];
    /* custom agent menu for everbest */
    // if ($appId == 101 && $roleId == 4) {
    //   $menus = Menu::with('submenus')->whereIn('id', [1,2,3,4,6,8])->get();
    // }

    return $menus;
  }

  // ambil semua list agent pada appId tertentu
  public static function agents($appId, $status = true)
  {
    // $agents = UserPlatform::with(['apps' => function($q) use ($appId){
    //   $q->where('app_id', $appId)->where('role_id', 4);
    // }])->get()->reject(function ($item) {
    //   return count($item->apps) == 0;
    // })->values();
    // return $agents;
    $agents = DB::table('omnichannel.app_user_platform')
      ->where('omnichannel.app_user_platform.app_id', $appId)
      ->when($status, function ($q) use ($status) {
          $q->where('omnichannel.app_user_platform.online', $status);
      })
      ->join('auth.users', 'omnichannel.app_user_platform.user_id', '=', 'auth.users.id')
      ->join('auth.roles', function($q) use ($appId) {
        $q->on('omnichannel.app_user_platform.role_id', '=', 'auth.roles.id')
          ->where('auth.roles.app_id', $appId)
          ->where('auth.roles.slug', 'staff');
      })
      ->select('auth.users.id','auth.users.name','auth.users.email','auth.users.created_at','omnichannel.app_user_platform.online')
      ->orderByRaw('UPPER(auth.users.name) ASC')
      ->get()
      ->values();
    return $agents;
  }

  public static function agentId($userId, $appId) {
    $agent = DB::table('app_user_platform as aup')
                ->where('aup.app_id', $appId)
                ->where('aup.user_id', $userId)
                ->join('auth.users as auser', 'aup.user_id', '=', 'auser.id')
                ->select('auser.id','auser.name','auser.email','auser.created_at','aup.online')
                ->get();
                return $agent;


  }

  public static function checkCurrentRole($userId, $appId)
  {
    $userApp = UserApp::where('app_id', $appId)->where('user_id', $userId)->with('role')->first();
    $role = $userApp->role;
    return $role;
  }

  public static function canEditToRole($data = [])
  {
    $rankingRoleTarget = $data['roleTarget']->ranking;
    $rankingEditorRole = $data['roleEditor']->ranking;
    // Jika role target akan diubah ke role yang memiliki ranking lebih tinggi daripada role editor saat ini
    if ($rankingRoleTarget < $rankingEditorRole ) {
      return false;
    } else {
      return true;
    }
  }

  public static function modulePrivileges($roleId)
  {
    $role = Role::find($roleId);
    if (!$role) {
      return false;
    }
    $module = collect($role->accessibilities)->map(function($mod) {
      $access = explode("-", $mod);
      $data = [
        'id' => $access[1],
        'type' => $access[0]
      ];
      return $data;
    });

    $moduleId = $menuId = $submenuId = $subSubmenuId = [];
    foreach ($module as $key => $value) {
      if ($value['type'] == 'module') {
        $moduleId[] = $value['id'];
      }
      if ($value['type'] == 'menu') {
        $menuId[] = $value['id'];
      }
      if ($value['type'] == 'submenu') {
        $submenuId[] = $value['id'];
      }
      if ($value['type'] == 'sub_submenu') {
        $subSubmenuId[] = $value['id'];
      }
    }
    $res = [
      [
        'type' => 'module',
        'id' => $moduleId
      ],
      [
        'type' => 'menu',
        'id' => $menuId
      ],
      [
        'type' => 'submenu',
        'id' => $submenuId
      ],
      [
        'type' => 'sub_submenu',
        'id' => $subSubmenuId
      ]
    ];
    $modules = Module::whereIn('id', $moduleId)->with(['menus' => function($q) use ($menuId) {
      $q->whereIn('id', $menuId);
    },'menus.submenus' => function($q) use ($submenuId) {
      $q->whereIn('id', $submenuId);
    }, 'menus.submenus.subSubmenus' => function($q) use ($subSubmenuId) {
      $q->whereIn('id', $subSubmenuId);
    }])->get();
    return $modules;
  }
}
