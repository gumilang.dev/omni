<?php namespace App\Modules\TimeReportServed\Model;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Room\Models\Room;

class TimeReportServed extends Model
{
  protected $table = "time_report_served";
  protected $fillable = ["request_at","served_at","room_id","agent_id","app_id","diff","agent_name","agent_email"];
  protected $appends = ["room"];

  public function getRoomAttribute() {
    return Room::where('id', $this->attributes['room_id'])->with(["channel"])->first();
  }
}
