<?php namespace App\Modules\TimeReportServed\Controller;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Services\TimeReportServedImpl as Service;

class TimeReportServedController extends ApiController
{
    public function __construct(Service $service) {
        $this->service = $service;
    }

    public function index(Request $request,$appId) {
      $response = $this->service->getTimeReportServed(
        $request,
        ['app_id' => $appId],
        ['room_id','agent_id','created_at',"agent_name","agent_email"]
      );
      return response()->json($response);
    }

    public function export(Request $request,$appId) {
      $response = $this->service->getTimeReportServedExport(
        $request,
        ['app_id' => $appId],
        ['room_id','agent_id','created_at',"agent_name","agent_email"]
      );
      return response()->json($response);
    }
}
