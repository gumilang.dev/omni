<?php

namespace App\Modules\FunctionList\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Events\MaskingInputText;

class FunctionListController extends ApiController
{
    public function maskingText(Request $request, $appId)
    {
        broadcast(new MaskingInputText($request->userId, $appId));
        return $this->successResponse(null, 'Broadcast masking input');
    }
}
