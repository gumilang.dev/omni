<?php
namespace App\Modules\LogHandleRoom\Controllers;

use Illuminate\Http\Request;
use App\Services\LogHandleRoomServiceImpl;
use App\Http\Controllers\ApiController;

class LogHandleRoomController extends ApiController
{
    public function __construct() {
      $this->service = new LogHandleRoomServiceImpl();
    }

    public function report(Request $request,$appId) {
      $response = $this->service->reportLog(
        $request,
        ["app_id" => $appId],
        ["room_id"]
      );
      return response()->json($response);
    }


}
