<?php

namespace App\Modules\Mobile\Controllers;

use Carbon\Carbon;
use Hashids\Hashids;
use Illuminate\Http\Request;

use App\Modules\Log\Models\WebhookLog;
use App\Modules\Integration\Models\Integration;
use App\Modules\App\Models\App;
use App\Modules\User\Models\User;
use App\Modules\Room\Models\Room;
use App\Modules\Message\Models\Message;
use App\Modules\Channel\Models\Channel;
use App\Modules\Livechat\Models\Livechat;

use App\Events\NewUser;
use App\Events\NewMessage;
use App\Events\RequestLive;

use App\Modules\Mobile\Helpers\LennaMessageType;
use App\Modules\Chat\Helpers\ChatHelper;
use App\Modules\Log\Helpers\WebhookHelper;

use App\Modules\Bot\Services\BotService;
use App\Modules\Mobile\Services\BackendService;
use App\Modules\BlockUser\Services\BlockUserService;
use App\Modules\Common\Services\EncryptService;
use App\Http\Controllers\ApiController;

class MobileWebhookController extends ApiController
{
    protected $app;
    protected $channel;
    protected $mobileController;
    protected $integration;
    protected $hashids;
    protected $blockUserService;

    public function __construct(
      BlockUserService $blockUserService
    )
    {
        $this->channel = Channel::where('name', 'mobile')->first();
        $this->hashids = new Hashids(env("SECRET_KEY"), 6);
        $this->blockUserService = $blockUserService;
    }
    public function webhookTest(Request $request, $appId)
    {
        $webhookLog = WebhookLog::create([
            "source" => "mobile",
            "app_id" => $appId,
            "events" => \Request::ip(),
            "payload" => json_encode($request->all()),
            "response" => json_encode([
                'success' => true,
                'data' => "testing webhook"
            ])
        ]);

        return $this->successResponse(null, 'testing webhook');
    }
    public function webhook(Request $request, $appId)
    {
      // dd($request->all());
        if (!$this->isIntegrationActive($request, $appId))
            return $this->errorResponse(null, 'integration not active');

        $this->app = App::find($appId);
        $this->integration = Integration::where(['app_id' => $this->app->id, 'status' => true])->whereHas('channel', function ($q) {
          $q->where('name', 'mobile');
        })->first();

        $incomingMessage = $this->incomingMessage($request, $this->app->id);
        if(is_bool($incomingMessage) && $incomingMessage == true) {
          return $this->successResponse(["data" => $incomingMessage],"user blocked");
        }
        $dataResponseLog = $incomingMessage;

        // if status resolved
        if (WebhookHelper::isResolved(['roomId' => $incomingMessage['room']->id])) {
          // change status to bot by deleting livechat key
          $this->deleteLiveChat($incomingMessage['room']->id);
        }

        broadcast(new NewMessage($incomingMessage['message'], $incomingMessage['room'], $this->app));

        $botService = new BotService(['appId' => $appId]);

        if (!WebhookHelper::isLive(['roomId' => $incomingMessage['room']->id])) {
          if (!WebhookHelper::isRequest(['roomId' => $incomingMessage['room']->id])) {
            $backendService = new BackendService(['appId' => $appId]);
            $backendResponse = $backendService->saveUserToken([
                "accessToken" => str_replace("Bearer ", "", $request->header('Authorization')),
                "tokenType" => 'Bearer',
                "userId" => $incomingMessage['user']->id
            ]);

            $botResponse = $botService->replyMessage([
              'userId' => $incomingMessage['user']->id,
              'content' => $incomingMessage['message']->content[0],
              'room' => $incomingMessage['room'],
              'lat' => $request->lat,
              'lon' => $request->lon,
              'channel' => 'android',
              "headers" => ["Authorization" => $request->header('Authorization') ?? "Bearer $backendResponse->access_token"],
              'lang' => $incomingMessage['user']->lang
            ]);
          }
        }

        // if bot not integrated, change status to request
        $botIntegration = $botService->getIntegration();
        if (!$botIntegration) {
          if (!WebhookHelper::isLive(['roomId' => $incomingMessage['room']->id])) {
            if (!WebhookHelper::isRequest(['roomId' => $incomingMessage['room']->id])) {
              WebhookHelper::requestLivechat($incomingMessage['user']->id, $appId);
            }
          }
        }

        $webhookLog = WebhookLog::create([
            "source" => "mobile",
            "app_id" => $this->app->id,
            "events" => "message",
            "payload" => json_encode($request->all()),
            "response" => json_encode([
                'success' => true,
                'data' => $dataResponseLog,
                'botResponse' => $botResponse ?? null
            ]),
            "user_id" => $incomingMessage['user']->id,
            "bot_id" => $botIntegration ? $botIntegration->integration_data['botId'] : NULL
        ]);

        if (!WebhookHelper::isLive(['roomId' => $incomingMessage['room']->id])) {
          if (!WebhookHelper::isRequest(['roomId' => $incomingMessage['room']->id])) {
            $response = [
              'date' => Carbon::now()->format('d F Y'),
              'time' => Carbon::now()->format('h:m'),
              'result' => (object)[
                  'output' => $botResponse['nlp']->response,
                  'quickbutton' => $botResponse['nlp']->quickbutton ?? [],
              ]
            ];
            $response = EncryptService::encryptResponse($response, $appId, $this->channel->id);
            return $this->successResponse([], null, $response);
          }
        }

        $response = [
          'date' => Carbon::now()->format('d F Y'),
          'time' => Carbon::now()->format('h:m'),
          'result' => (object)[
              'output' => null,
              'quickbutton' => null,
          ]
        ];
        $response = EncryptService::encryptResponse($response, $appId, $this->channel->id);
        return $this->successResponse([], null, $response);
    }

    public function incomingMessage($request, $appId)
    {
        $userId = $this->hashids->decode($request->user_id)[0];
        $user = User::where('app_id', $appId)->where('channel_id', $this->channel->id)->where('id', $userId)->first();

        if($this->blockUserService->isBlocked($user->email, $appId)) {
          return true;
        }

        $chatRoom = ChatHelper::createRoomAndParticipate(['channel' => $this->channel, 'user' => $user, 'appId' => $appId]);

        $content[] = $this->setMessageType($request['query']);
        $message = ChatHelper::createMessage(['content' => $content, 'user' => $user, 'room' => $chatRoom]);

        return [
            'message' => $message,
            'user' => $user,
            'room' => $chatRoom
        ];

    }

    public function setMessageType($message)
    {
        $lennaMessageType = new LennaMessageType;
        $content = $lennaMessageType->text(['text' => $message]);
        return $content;
    }
    public function isIntegrationActive($request, $appId)
    {
        $isActive = Integration::where([
            ['app_id', $appId],
            ['status', true]
        ])->whereHas('channel', function ($q) {
            $q->where('name', 'mobile');
        })->get()->isNotEmpty();
        return $isActive;
    }

    public function requestLivechat($senderId, $appId)
    {
        $room = User::find($senderId)->rooms->first();
        $liveChat = $room->liveChats()->firstOrCreate(
            ['room_id' => $room->id, 'status' => 'request'],
            [
                "request_at" => Carbon::now()->toDateTimeString(),
                "status" => 'request',
            ]
        );
        broadcast(new RequestLive($room->id, $appId, $senderId, $liveChat->id));
    }

    public function deleteLiveChat($roomId)
    {
        Livechat::where('room_id', $roomId)->delete();
    }

}
