<?php

namespace App\Modules\Mobile\Services;

use Illuminate\Http\Request;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Hashids\Hashids;
use App\Modules\Integration\Models\Integration;
use App\Modules\ExternalApi\Models\ExternalApi;
use App\Modules\Log\Models\ApiLog;
use App\Modules\User\Models\UserToken;
use App\Modules\User\Models\User;

use App\Modules\Chat\Controllers\ChatController;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class BackendService
{
    protected $data;
    protected $lennaApi;
    protected $client;
    protected $appId;
    protected $backendAppId;

    protected static $initVector = '0000000000000000';
    protected static $cipher = 'AES-256-CBC';

    public function __construct($data = [])
    {
        $this->data = $data;
        $this->lennaApi = ExternalApi::where(['category' => 'internal', 'provider' => 'lenna'])->first();
        $guzzleProp = ['base_uri' => $this->lennaApi->base_url, 'verify' => false];
        $this->client = new Client($guzzleProp);
        $this->appId = $this->data['appId'];

        // hashed with backend methods
        $backendHashids = new Hashids(env("SECRET_KEY"), '6');
        $this->backendAppId = $backendHashids->encode($this->appId);
    }

    public function login($data = [])
    {
        $apiProp = $this->lennaApi->endpoints()->where('name', 'login')->first();
        $requestData = [
            'email' => $data['email'],
            'password' => $data['password'],
            'fcm_token' => $data['fcm_token'],
            'client' => $data['client']
        ];
        try {
            $response = $this->client->request(
                $apiProp->method,
                str_replace("{app-id}", $this->backendAppId, $apiProp->endpoint),
                [
                    'json' => $requestData,
                    'allow_redirects' => false
                ]
            );
            $response = json_decode($response->getBody()->getContents());
        } catch (ServerException $e) {
            $endpoint = str_replace("{app-id}", $this->backendAppId, $apiProp->endpoint);
            ApiLog::create([
                'url'           => $this->lennaApi->base_url.$endpoint,
                'app_id'        => $this->appId,
                'user_id'       => $data['user_id'],
                'request'       => json_encode($requestData),
                // 'response'    => $e->getMessage(),
                'response'      => $e->getResponse()->getBody(true),
                'ip_address'    => \Request::ip(),
                'user_agent'    => \Request::header('User-Agent'),
                'userable_type' => 'user',
                'userable_id'   => $data['user_id'],
            ]);
            $response = false;
        } catch (ClientException $e) {
            $endpoint = str_replace("{app-id}", $this->backendAppId, $apiProp->endpoint);
            ApiLog::create([
                'url'           => $this->lennaApi->base_url.$endpoint,
                'app_id'        => $this->appId,
                'user_id'       => $data['user_id'],
                'request'       => json_encode($requestData),
                // 'response'    => $e->getMessage(),
                'response'      => $e->getResponse()->getBody(true),
                'ip_address'    => \Request::ip(),
                'user_agent'    => \Request::header('User-Agent'),
                'userable_type' => 'user',
                'userable_id'   => $data['user_id'],
            ]);
            $response = false;
        } catch (RequestException $e) {
          $endpoint = str_replace("{app-id}", $this->backendAppId, $apiProp->endpoint);
          ApiLog::create([
              'url'           => $this->lennaApi->base_url.$endpoint,
              'app_id'        => $this->appId,
              'user_id'       => $data['user_id'],
              'request'       => json_encode($requestData),
              // 'response'    => $e->getMessage(),
              'response'      => $e->getResponse()->getBody(true),
              'ip_address'    => \Request::ip(),
              'user_agent'    => \Request::header('User-Agent'),
              'userable_type' => 'user',
              'userable_id'   => $data['user_id'],
          ]);
          $response = false;
        }
        return $response;
    }

    public function updateLocationAndIp($data = []) {
      $last_update = $data['last_update'];
      $diff = $last_update->diffInDays(Carbon::now('Asia/Jakarta'));
      if ($diff > 1) {
        User::where(['id' => $data['user_id']])->update([
          'ip_address' => $data['ip'],
          'city' => $data['city'],
          'location' => $data['location']
        ]);
      }
    }

    public function getUserToken($data = [])
    {
        $userToken = UserToken::where('user_id', $data['userId'])->orderBy('id', 'DESC')->first();
        // already login
        if ($userToken) {
            // if expired, re login
            if (Carbon::parse(Carbon::now('Asia/Jakarta'))->diffInSeconds($userToken->expires_at, false) < 2) {
                $response = $this->login([
                    'email' => $data['email'],
                    'password' => $data['password'],
                    'fcm_token' => $data['fcm_token'] ?? null,
                    'client' => $data['client'] ?? 'channel',
                    'user_id' => $data['userId']
                ]);
                $userToken = UserToken::updateOrCreate(
                    [
                        'user_id' => $data['userId']
                    ],
                    [
                        'access_token' => $response->access_token,
                        'token_type' => $response->token_type,
                        'expires_at' => $response->expires_at,
                    ]
                );
            }
        } else {
            $response = $this->login([
                'email' => $data['email'],
                'password' => $data['password'],
                'fcm_token' => $data['fcm_token'] ?? null,
                'client' => $data['client'] ?? 'channel',
                'user_id' => $data['userId']
            ]);
            $userToken = UserToken::create(
                [
                    'user_id' => $data['userId'],
                    'access_token' => $response->access_token,
                    'token_type' => $response->token_type,
                    'expires_at' => $response->expires_at,
                ]
            );
        }
        return $userToken;
    }

    public static function encryptText($key, $plainText)
    {
        $key = str_pad($key, 32, '0');
        $encrypted = base64_encode(openssl_encrypt($plainText, self::$cipher, $key, OPENSSL_RAW_DATA, self::$initVector));
        return $encrypted;
    }

    public static function decryptText($key, $encryptedText)
    {
        $key = str_pad($key, 32, '0');
        $plain = openssl_decrypt(base64_decode($encryptedText), self::$cipher, $key, OPENSSL_RAW_DATA, self::$initVector);
        return $plain;
    }

    public function saveUserToken($data = [])
    {
        $userToken = UserToken::updateOrCreate(
            [
                'user_id' => $data['userId']
            ],
            [
                'access_token' => $data['accessToken'],
                'token_type' => $data['tokenType'],
                'expires_at' => Carbon::now('Asia/Jakarta')->addDays(90),
            ]
        );
        return $userToken;
    }

}
