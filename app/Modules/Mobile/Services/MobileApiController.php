<?php

namespace App\Modules\Mobile\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\ExternalApi\Models\ExternalApi;
use GuzzleHttp\Client;
use App\Modules\Mobile\Helpers\LennaMessageType;
use App\Modules\User\Models\User;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\RequestGuzzle;
// use Hashids\Hashids;
use Vinkla\Hashids\Facades\Hashids; //backend hashids


class MobileApiController extends Controller
{
    protected $client;
    protected $googleApi;

    public function __construct($data)
    {
        $this->data = $data;
        $this->googleApi = ExternalApi::where(['category' => 'broadcast', 'provider' => 'google'])->first();
        $guzzleProp = [
            'base_uri' => $this->googleApi->base_url
        ];
        $this->client = new Client($guzzleProp);
    }
    public function sendToAndroid($newMessage, $appId)
    {
        $androidNotifData = [
            'notification' => (object)array_merge((array)$newMessage["notification"], (array)['body' => $newMessage['notification']['title'], 'body' => $newMessage['notification']['body']]),
            'data' => (object)array_merge((array)$newMessage["data"], (array)['notificationTitle' => $newMessage["notification"]["title"], 'notificationBody' => $newMessage["notification"]["body"]])
        ];

        if ($newMessage["type"] == 'single') {

            if ($newMessage['user_email']) {
                $user = ($newMessage["type"] == 'single') ? User::where(['app_id'=>$appId,'email'=>$newMessage["user_email"]])->first() : null;
                $newMessage['receiver_id'] = $user->id ?? null;
            } else {
                $user = ($newMessage["type"] == 'single') ? User::find($newMessage["receiver_id"]) : null;
            }

            if ($newMessage["receiver_id"] == null) {
                return 'Email not Found';
            }
            $requestData = array_merge(['to' => $user->fcm_token], $androidNotifData);
            return $this->callBroadcastApi($requestData, $newMessage, $appId);

        } else if ($newMessage["type"] == 'broadcast') {

            $requestData = array_merge(['to' => "/topics/$newMessage->topics"], $androidNotifData);
            return $this->callBroadcastApi($requestData);
        } else if ($newMessage["type"] == 'segment') {
            if ($newMessage['user_email']) {
                $user = ($newMessage["type"] == 'segment') ? User::where(['app_id'=>$appId,'email'=>$newMessage["user_email"]])->first() : null;
                $newMessage['receiver_id'] = $user->id ?? null;
            } else {
                $user = ($newMessage["type"] == 'segment') ? User::find($newMessage["receiver_id"]) : null;
            }
            $requestData = array_merge(['to' => $user->fcm_token], $androidNotifData);
            return $this->callBroadcastApi($requestData, $newMessage, $appId);
        } else if ($newMessage["type"] == 'multiple') {
            // dd($newMessage['fcm_token']);
            $requestData = array_merge(['to' => $newMessage['fcm_token'], $androidNotifData]);
            return $this->callBroadcastApi($requestData,$newMessage,$appId);
        }
    }
    public function sendToIos($newMessage, $appId)
    {
        $iosNotifData = [
            'notification' => $newMessage['notification'],
            'data' => $newMessage['data']
        ];
        if ($newMessage['type'] == 'single') {
            $user = ($newMessage['type'] == 'single') ? User::find($newMessage['receiver_id']) : null;
            $requestData = array_merge(['to' => $user->fcm_token],$iosNotifData);
            return $this->callBroadcastApi($requestData,$newMessage,$appId);
        } else if ($newMessage['type'] == 'broadcast') {
            $requestData = array_merge(['to' => `/topics/`.$newMessage['topics']], $iosNotifData);
            return $this->callBroadcastApi($requestData,$newMessage,$appId);
        } else if ($newMessage['type'] == 'multiple') {
            $requestData = array_merge(['to' => $newMessage['fcm_token']], $iosNotifData);
            return $this->callBroadcastApi($requestData,$newMessage,$appId);
        }
    }
    public function sendBroadcast($newMessage, $appId)
    {
        // dd($newMessage);
        if ($newMessage["client"] == 'android') {
            return $this->sendToAndroid($newMessage, $appId);
        } else {
            return $this->sendToIos($newMessage, $appId);
        }
    }

    public function callBroadcastApi($requestData, $newMessage, $appId)
    {
        $apiProp = $this->googleApi->endpoints()->where('name', 'send-message')->first();

        try {
            $response = $this->client->request(
                $apiProp->method,
                $apiProp->endpoint,
                [
                    'headers' => ['Authorization' => 'key=' . $this->data['integration']->integration_data['serverKey']],
                    "json" => $requestData
                ]
            );
        } catch (RequestException $e) {
            if ($e->getResponse()->getStatusCode() == '400') {
                return "400 Bad Request";
            }
        }
        $responses = json_decode($response->getBody()->getContents());
        if ($responses->success == 1) {
            if ($newMessage['category'] == "inbox") {
                $backend =  $this->_sendToBackend($newMessage, $appId);
            }
        }
        // dd($responses);
        return $responses;
    }

    public function _sendToBackend($newMessage, $appId)
    {
        $hashedAppId = Hashids::connection('main')->encode($appId);
        $client = new Client();
        $be = env("BACKEND_URL");
        $url = $be . '/api/' . $hashedAppId . "/notification/save_notification";
        try {
            $response = $client->request('POST', $url,
            [
                "form_params" => [
                    'receiver_id' => json_encode($newMessage['receiver_id']),
                    'notification_title' => json_encode($newMessage['notification']['title']),
                    'notification_body' => json_encode($newMessage['notification']['body']),
                    'message_title' => json_encode($newMessage['data']['title']),
                    'message_body' => json_encode($newMessage['data']['message']),
                    'app' => $appId

                ]
            ]
        );
        } catch (Exception $e) {
            return ($resp = json_decode($e->getResponse()->getBody()));
        }

        return $responses = json_decode($response->getBody()->getContents());

    }

}
