<?php

namespace App\Modules\Mobile\Helpers;

class LennaMessageType
{
	public function __construct()
	{

	}

	public function text($input = [])
	{
		$output = [
			'type' => 'text',
			'text' => $input['text'] ?? '',
			'speech' => $input['speech'] ?? ($input['text'] ?? '')
		];
		return $output;
	}

	public function image($input = [])
  {
      $output = [
          'type' => 'image',
          'text' => $input['previewImageUrl'],
          'caption' => $input['caption'] ?? '',
          'speech' => 'image',
          'originalContentUrl' => $input['originalContentUrl'] ?? ($input['previewImageUrl'] ?? ''),
          'previewImageUrl' => $input['previewImageUrl'] ?? '',
      ];
      return $output;
  }

	public function video($input = [])
	{
		$output = [
			'type' => 'video',
			'originalContentUrl' => $input['originalContentUrl'] ?? ($input['previewVideoUrl'] ?? ''),
			'previewVideoUrl' => $input['previewVideoUrl'] ?? '',
			'caption' => $input["caption"] ?? '',
			'duration' => $input['duration'] ?? 0,
		];

		return $output;
	}

	public function audio($input = [])
	{
		$output = [
			'type' => 'audio',
			'originalContentUrl' => $input['originalContentUrl'] ?? '',
			'duration' => $input['duration'] ?? 0,
		];

		return $output;
	}

	public function confirm($input = [])
	{
		$output = [
			'type' => 'confirm',
			'text' => $input['text'] ?? 'Apakah Anda yakin?',
			'actions' => [
				(object)[
					'type' => 'message',
					'label' => $input['actions'][0]['label'] ?? 'Ya',
					'text' => $input['actions'][0]['text'] ?? 'ya'
				],
				(object)[
					'type' => 'message',
					'label' => $input['actions'][1]['label'] ?? 'Tidak',
					'text' => $input['actions'][1]['text'] ?? 'tidak'
				]
			]
		];

		return $output;


	}

	public function carousel($input = [])
	{
		$output = [
			'type' => 'carousel',
			'imageAspectRatio' => $input['imageAspectRatio'] ?? 'rectangle',
			'imageSize' => $input['imageSize'] ?? 'cover',
			'columns' => []
		];
		if (!isset($input['columns']))
			return $output;

		foreach ($input['columns'] as $data) {
			$output['columns'][] = (object)[
				'thumbnailImageUrl' => $data['thumbnailImageUrl'] ?? 'https://s.mxmcdn.net/site/images/albums/nocover_new-500x500.png',
				'imageBackgroundColor' => $data['imageBackgroundColor'] ?? '#FFFFFF',
				'title' => strip_tags($data['title']) ?? '',
				'text' => strip_tags($data['text']) ?? '',
				'defaultAction' => $data['defaultAction'] ?? '',
				'actions' => $data['actions'] ?? '',
			];
		}

		return $output;


	}
	public function button($input = [])
	{
		$output = [
			'type' => 'button',
			'text' => $input['text'] ?? 'title',
			'actions' => $input['actions'] ?? []
		];

		return $output;


	}
	public function file($input = [])
	{
		$output = [
			'type' => 'file',
			'fileUrl' => $input['fileUrl'] ?? '',
			'fileName' => $input['fileName'] ?? '',
			'fileSize' => $input['fileSize'] ?? '',
		];

		return $output;

	}
	public function html($input = [])
	{
		$output = [
			'type' => 'html',
			'content' => $input['content'] ?? '',
		];

		return $output;

	}
	public function location($input = [])
	{
		$mapStaticKey = env('GOOGLE_MAPS_STATIC_KEY');
		$output = [
      'type' => 'location',
      'title' => $input['title'] ?? '',
      'text' => '@sys.location',
			'address' => $input['address'] ?? '',
			'latitude' => $input['latitude'] ?? '',
			'longitude' => $input['longitude'] ?? '',
			'locationUrl' => $input['locationUrl'] ?? "https://maps.google.com",
			'thumbnailImageUrl' => $input['thumbnailImageUrl'] ?? null,
		];
		if ($output['thumbnailImageUrl'] == null) {
			$output['thumbnailImageUrl'] = "https://maps.googleapis.com/maps/api/staticmap?zoom=17&scale=false&size=400x300&maptype=roadmap&format=jpg&visual_refresh=true&markers=label:1%7C" . $output['latitude'] . "," . $output['longitude'] . "&key=$mapStaticKey";
		}
		return $output;

	}
	public function grid($input = [])
	{
		$output = [
			'type' => 'grid',
			'title' => $input['title'] ?? '',
			'subTitle' => $input['subTitle'] ?? '',
			'imageUrl' => $input['imageUrl'] ?? '',
			'columns' => $input['columns'] ?? [],
		];

		return $output;

	}
	public function summary($input = [])
	{
		$output = [
			'type' => 'summary',
			'title' => $input['title'] ?? '',
			'subType' => $input['subType'] ?? '',
			'imageUrl' => $input['imageUrl'] ?? '',
			'columns' => $input['columns'] ?? [],
		];

		return $output;

	}
	public function weather($input = [])
	{
		$output = [
			'type' => 'weather',
			'area' => $input['area'] ?? '',
			'country' => $input['country'] ?? '',
			'countryCode' => $input['countryCode'] ?? '',
			'columns' => $input['columns'] ?? [],
		];

		return $output;
	}
	public function template($input = [])
	{
		$template = $input['template'] ?? '';
    $template = strip_tags($template, '<br><p><b><i><em><s><strong><u><del><h1><h2><h3><h4><h5><h6>');
    // $template = strip_tags($template);
		$output = [
		  'type' => 'template',
		  'template' => $template ?? ''
    ];
    // $output = [
			// 'type' => 'text',
			// 'text' => $template ?? '',
			// 'speech' => $template ?? ''
		// ];
		return $output;

	}
}
