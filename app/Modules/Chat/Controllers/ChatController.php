<?php

namespace App\Modules\Chat\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use DB;
use Hashids\Hashids;
use Carbon\Carbon;

use App\Services\PaginatorServiceImpl;
use App\Events\NewMessage;

use App\Modules\User\Models\User;
use App\Modules\Message\Models\Message;
use App\Modules\App\Models\App;
use App\Modules\Integration\Models\Integration;
use App\Modules\Room\Models\Room;
use App\Modules\Bot\Models\Bot;
use App\Modules\User\Models\UserPlatform;
use App\Modules\ExternalApi\Models\ExternalApi;

use App\Http\Controllers\ApiController;

use App\Modules\Chat\Helpers\ChatHelper;
use App\Modules\Mobile\Helpers\LennaMessageType;
use App\Modules\BroadcastMessage\Helpers\BroadcastMessageHelper;

use App\Modules\Common\Services\EncryptService;
use App\Modules\Line\Services\LineApiController;
use App\Modules\Mobile\Services\MobileApiController;
use App\Modules\Twitter\Services\TwitterApiController;
use App\Modules\Facebook\Services\FacebookApiController;
use App\Modules\Telegram\Services\TelegramApiController;
use App\Modules\Whatsapp\Services\InfobipApiController;
use App\Modules\Whatsapp\Services\ApiWhaApiController;
use App\Modules\Whatsapp\Services\DamcorpApiController;
use App\Modules\Whatsapp\Services\WaOfficialApiController;
use App\Modules\Whatsapp\Services\WappinApiController;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class ChatController extends ApiController
{
    public function deleteFile(Request $request, $appId)
    {
        $fileToDelete = [];

        Storage::disk('public_chat')->delete([$request->fileToDelete]);

        return $this->successResponse(null, 'file deleted');
    }

    public function searchMessage(Request $request,$appId) {
        $room_id = $request->post("room_id");
        $type    = $request->post("type");
        $per_page = 10;
        $paginator = new PaginatorServiceImpl();
        try {
          $keyword = "%".$request->post("search")."%";
          $data = $type == "text" ? $this->searchMessageByText($keyword,$room_id) : $this->searchMessageByDate($request->post("search"),$room_id);
          $response = $paginator->paginate($data,$per_page);
          return response()->json($response);
        }catch(Error $err) {
          return $this->errorResponse($err->getTrace(),"SEACH_MESSAGE_FAILED");
        }
    }

    public function searchMessageByText($keyword,$room_id) {
      return DB::select(DB::raw("SELECT * FROM omnichannel.messages WHERE LOWER(content->0 ->> 'text') LIKE :keyword AND room_id = :room_id ORDER BY id ASC"), [
        "keyword" => strtolower($keyword),
        "room_id" => $room_id
      ]);
    }

    public function searchMessageByDate($date,$room_id) {
      return Message::where('room_id',$room_id)->whereDate("created_at",$date)->orderBy("id","ASC")->get();
    }

    public function uploadFile(Request $request)
    {

        $validator = Validator::make($request->all(), [
          'file' => 'file|max:5120|required'
        ]);

        if ($validator->fails()) {
          return $this->errorResponse($validator->errors()->first('file'), "Fail upload image");
        }

        $name = $request->file('file')->getClientOriginalName();
        $file = $request->file('file');
        $extension = $request->file('file')->getClientOriginalExtension();




        if ($request->hasFile('file')){
          $allowedExension = ["jpeg","jpg","gif","png","webm","mpg","ogg","mp4","avi","mov","doc","dot","wbk","docx","docm","dotx","docb","xls","xlt","xlm","xlsx","ppt","pot","pps","pdf","PNG","JPG","JPEG"];
          if(in_array($extension,$allowedExension)) {
            Storage::disk('public_chat')->put($name, file_get_contents($file));
            return $this->successResponse(env('APP_URL').'/upload/chat/'.$name, 'file '.$name.' uploaded with extention '.$extension.'');
          }else{
            return $this->errorResponse(null,"extension not allowed");
          }
        }else{
          return $this->errorResponse(null,"file cannot be null");
        }

        if ($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg') {
          $optimizerChain = OptimizerChainFactory::create();
          $optimizerChain->optimize(public_path('/upload/chat/').$name, public_path('/upload/chat/').$name);
        }
        // $img = Image::make($file->getRealPath());
        // $destinationPath = public_path('/upload');
        // $img = $img->resize(300, null, function ($constraint) {
          // $constraint->aspectRatio();
        // });
        // app(Spatie\ImageOptimizer\OptimizerChain::class)->optimize(Storage::disk('public_chat'));
        // Storage::disk('public_chat')->put($name, $img);
    }





    public function sendMessage(Request $request, $appId)
    {
        $chatRoom = Room::where('id', $request->roomId)->with('channel')->first();

        if (count($request->message) == 0) {
            return $this->errorResponse(null, 'message empty');
        }
        if ($chatRoom->created_by->integration_id == null) {
             // check integration status
            if (!$integration = $this->_getIntegrationData($chatRoom->channel->id, $appId))
                return $this->errorResponse(null, "Plase connect your {$chatRoom->channel->name} channel first");
        } else {
             // check integration status
            if (!$integration = $this->_getIntegrationDataById($chatRoom->channel->id, $appId, $chatRoom->created_by->integration_id))
                return $this->errorResponse(null, "Plase connect your {$chatRoom->channel->name} channel first");
        }


        // check sender type, BOT or USER PLATFORM
        if ($request->senderType == 'bot') {
            $participable = Bot::find($request->senderId);
        } else {
            $participable = UserPlatform::find($request->senderId);
        }


    // participate sender to room
        ChatHelper::participateUser($participable, $request->roomId);
    // set content
        foreach ($request->message as $key => $message) {
            $content[] = $this->_setMessageType($message);
        }
    // create message
        $newMessage = ChatHelper::createMessage(['content' => $content, 'user' => $participable, 'room' => $chatRoom]);
    // single file for channel chat controller,ex: line chat controller
        switch ($chatRoom->channel->name) {
            case 'line':
                $data = [
                    'chatRoom' => $chatRoom,
                    'integration' => $integration
                ];

                // get last message before insert new message for token (line)
                $lastMessage = Message::where(['room_id' => $request->roomId])->orderBy('id', 'desc')->first();

                $lineController = new LineApiController($data);
                $diffSecond = $this->_getDiffInSeconds($lastMessage->channel_data['timestamp']);

                if ($diffSecond <= 30 && $lastMessage->channel_data['reply_token'] != null) {
                    $apiType = 'reply';
                    $response = $lineController->replyMessage($lastMessage, $newMessage, $request->quickbutton);
                } else {
                    $apiType = 'push';
                    $response = $lineController->pushMessage($newMessage, $request->quickbutton);
                    // if (!$response->success) {
                    //     $apiType = 'reply';
                    //     $response = $lineController->replyMessage($lastMessage, $newMessage);
                    // }
                }

                break;
            case 'twitter':
                $data = [
                    'chatRoom' => $chatRoom,
                    'integration' => $integration
                ];
                $twitterController = new TwitterApiController($data);
                $response = $twitterController->sendDirectMessage($newMessage);

                break;
            case 'facebook':
                $data = [
                    'chatRoom' => $chatRoom,
                    'integration' => $integration
                ];

                $facebookController = new FacebookApiController($data);
                $response = $facebookController->echoMessage($newMessage);
                $apiType = 'echo';

                break;
            case 'telegram':
                $data = [
                    'chatRoom' => $chatRoom,
                    'botToken' => $integration->integration_data['botToken']
                ];

                $telegramApiController = new TelegramApiController($data);
                $response = $telegramApiController->sendMessage($newMessage);
                $apiType = 'send';

                break;
            case 'whatsapp':
                $data = [
                    'chatRoom' => $chatRoom,
                    'integration' => $integration
                ];

                switch ($integration->integration_data['apiService']) {
                    case 'infobip':
                        $infobipApiController = new InfobipApiController($data);
                        $response = $infobipApiController->sendAdvanceMessage($newMessage);

                        break;
                    case 'damcorp':
                        $damcorpApi = new DamcorpApiController($data);
                        $response = $damcorpApi->sendMessage($newMessage);
                        //update msg id from whatsapp
                        $updateMsgIdWa = Message::find($newMessage->id);
                        $updateMsgIdWa->channel_data = [['id' => $response->data[0]->msgId]];
                        $updateMsgIdWa->save();
                        //update msg id from whatsapp
                        break;
                    case 'apiwha':
                        $ApiWhaApiController = new ApiWhaApiController($data);
                        $response = $ApiWhaApiController->sendMessage($newMessage);
                        if (!$response->success) {
                            return $this->errorResponse(null, $response->description);
                        }
                        break;
                    case 'official':
                        $WaOfficialApiController = new WaOfficialApiController($data);
                        $response = $WaOfficialApiController->sendMessage($newMessage);
                        //update msg id from whatsapp
                        $updateMsgIdWa = Message::find($newMessage->id);
                        $updateMsgIdWa->channel_data = $response->messages;
                        $updateMsgIdWa->save();
                        //update msg id from whatsapp
                        break;
                    case 'wappin':
                        $wappinApiQontroller = new WappinApiController($data);
                        $response = $wappinApiQontroller->sendMessage($newMessage);
                        if ($response->success == false) {
                          return $this->errorResponse($response->error, $response->message);
                        }
                        break;


                }


                $apiType = 'send';
                break;
            case 'mobile':
                // jika sendertype dari bot, jangan lakukan fcm broadcast
                if ($request->senderType != 'bot') {

                    $data = [
                        'integration' => $integration
                    ];
                    switch ($newMessage->type) {
                        case 'text':
                            $body = $newMessage->content->text;
                            break;
                        case 'template':
                            $body = strip_tags($newMessage->content->template);
                        default:
                            $body = "Send $newMessage->type message";

                            break;
                    }


                    $newBroadcastMessage = BroadcastMessageHelper::createMessage([
                        'data' => [
                            'type' => "single",
                            'topics' => null,
                            'category' => "chat",
                            'sub_category' => "message",
                            'title' => "New Message",
                            'message' => $newMessage->content,
                            'iconType' => "info",
                        ],
                        'notification' => [
                            'title' => "New Message",
                            'body' => $newMessage->content,
                        ],
                        'channel' => "mobile",
                        'receiver_id' => $chatRoom->created_by->id,
                        'broadcastType' => "single",
                        'broadcastCategory' => "chat",
                        'broadcastSubCategory' => "message",
                        'topics' => null,
                        'client' => 'android',
                    ]);

                    $mobileApiController = new MobileApiController($data);

                    $response = $mobileApiController->sendBroadcast($newBroadcastMessage, ['sendTo' => $chatRoom->created_by->fcm_token]);

                }
            $apiType = 'send';
            break;
        }
    // broadcast new message
        $app = App::where('id', $appId)->first();
        // if ($newMessage->encrypt) {
          // $decryptedContent = EncryptService::decrypt($newMessage->content[0]->data, env('ENCRYPT_MESSAGE_KEY'), 20);
          // $newMessage->content = json_decode($decryptedContent);
        // }
        broadcast(new NewMessage($newMessage, $chatRoom, $app))->toOthers();
        if (isset($request->broadcast)) {
        }

        return $this->successResponse([
            'channel' => $chatRoom->channel->name,
            'apiType' => $apiType ?? 'send',
            'message' => $newMessage,
            'temporaryId' => $request->temporaryId,
            // 'waMsg' => $response
        ], 'message sent');


    }

    public function wa24HourCheck(Request $request, $appId){
        $checkLastMessage = DB::table('messages as m')
                              ->where('room_id', $request->room_id)
                              ->where('messageable_type', 'user')
                              ->where('created_at', ">=", Carbon::now()->subHours(24))
                              ->orderBy('id', 'DESC')
                              ->limit(1)
                              ->get();
        if (!$checkLastMessage->isEmpty()) {
            return $this->successResponse($checkLastMessage, 'can send message');
        } else {
            return $this->successResponse($checkLastMessage, 'this user may not receive the message');
        }
    }


    public function _getIntegrationData($channelId, $appId)
    {
        $integration = Integration::where([['app_id', "=", $appId], ['status', "=", true], ['channel_id', "=", $channelId]])
                                  ->orderBy('id', 'ASC')
                                  ->first();
        return $integration;
    }

    public function _getIntegrationDataById($channelId, $appId, $id)
    {
        $integration = Integration::where(['app_id' => $appId, 'channel_id' => $channelId, 'id' => $id, 'status' => true])->first();
        return $integration;
    }

    public function _getDiffInSeconds($messageTimestamp, $timeB = null)
    {
        if ($timeB == null) {
            $timeB = Carbon::now();
        }
        $timeA = Carbon::createFromTimestampMs($messageTimestamp);

        $diff = $timeA->diffInSeconds($timeB);
        return $diff;
    }


    public function _setMessageType($data)
    {
        $lennaMessageType = new LennaMessageType;
        switch ($data['type']) {
            case 'text':
                $output = $lennaMessageType->text([
                    'text' => $data['text']
                ]);

                break;
            case 'file':
                $output = $lennaMessageType->file([
                    'fileUrl' => $data['fileUrl'],
                    'fileName' => $data['fileName'],

                ]);
                break;
            case 'image':
                $output = $lennaMessageType->image([
                    'previewImageUrl' => $data['previewImageUrl']
                ]);
                break;
            case 'audio':
                $output = $lennaMessageType->audio([
                    'originalContentUrl' => $data['originalContentUrl']
                ]);
                break;
            case 'video':
                $output = $lennaMessageType->video([
                    'originalContentUrl' => $data['originalContentUrl'],
                    'previewImageUrl' => $data['previewImageUrl']
                ]);
                break;
            case 'confirm':
                $output = $lennaMessageType->confirm([
                    'text' => $data['text'],
                    'actions' => $data['actions'],
                ]);
                break;
            case 'html':
                $output = $lennaMessageType->html([
                    'content' => $data['html']
                ]);
                break;
            case 'grid':
                $output = $lennaMessageType->grid([
                    'title' => $data['title'],
                    'subTitle' => $data['subTitle'],
                    'imageUrl' => $data['imageUrl'],
                    'columns' => $data['columns']
                ]);
                break;
            // case 'transaction':
            //     $output = $lennaMessageType->summary([
            //         'title' => $data['title'] ?? '',
            //         'subType' => $data['subType'] ?? '',
            //         'imageUrl' => $data['imageUrl'] ?? '',
            //         'columns' => $data['columns'] ?? $data['elements']
            //     ]);
            //     break;
            // case 'summary':
            //     $output = $lennaMessageType->summary([
            //         'title' => $data['title'] ?? '',
            //         'subType' => $data['subType'] ?? '',
            //         'imageUrl' => $data['imageUrl'] ?? '',
            //         'columns' => $data['columns'] ?? $data['elements']
            //     ]);
            //     break;
            case 'carousel':
                $output = $lennaMessageType->carousel([
                    'imageAspectRatio' => $data['imageAspectRatio'],
                    'imageSize' => $data['imageSize'],
                    'columns' => $data['columns'],
                ]);
                break;
            case 'button':
                $output = $lennaMessageType->button([
                    'text' => $data['text'],
                    'actions' => $data['actions'],
                ]);
                break;
            case 'weather':
                $output = $lennaMessageType->weather([
                    'area' => $data['area'],
                    'country' => $data['country'],
                    'countryCode' => $data['countryCode'],
                    'columns' => $data['columns'],
                ]);
                break;
            case 'template':
                $output = $lennaMessageType->template([
                  'template' => $data['template'],
                ]);
                break;
            default:
                $output = $data;

                break;
        }
        return $output;
    }

}
