<?php

namespace App\Modules\Chat\Helpers;

use Carbon\Carbon;
use App\Modules\Room\Models\Room;
use App\Modules\Message\Models\Message;
use Illuminate\Support\Facades\Log;
use App\Modules\Common\Services\EncryptService;


class ChatHelper
{

    public static function createRoomAndParticipate($data = [])
    {
      // create room if not exist
        $chatRoom = $data['channel']->rooms()->firstOrCreate(
            ['created_by' => $data['user']->id],
            ['app_id' => $data['appId'], 'created_by' => $data['user']->id]
        );
      // participate user to that room
        self::participateUser($data['user'], $chatRoom->id);
        return $chatRoom;
    }
    public static function participateUser($participable, $chatRoomId)
    {
        $participable->participatedIn()->sync([$chatRoomId]);

    }
    public static function createMessage($data = [])
    {
        $encryptedContent = EncryptService::encrypt($data['content'], env('ENCRYPT_MESSAGE_KEY'), 20);
        // Log::info('encrypted content: '.$encrypted);
        // check contains app
        $app = collect(config('encrypt.enabled.apps'));
        $app = $app->where('id', $data['room']->app_id)->values()->all();
        if (count($app) > 0) {
          if (collect($app[0]['channels'])->contains($data['room']->channel_id)) {
            $fields = [
              'channel_data' => $data['channelData'] ?? null,
              'room_id' => $data['room']->id,
              'content' => $encryptedContent,
              'encrypt'  => true
            ];
          } else {
            $fields = [
              'channel_data' => $data['channelData'] ?? null,
              'room_id' => $data['room']->id,
              'content' => $data['content'],
              'encrypt'  => false
            ];
          }
        } else {
          $fields = [
            'channel_data' => $data['channelData'] ?? null,
            'room_id' => $data['room']->id,
            'content' => $data['content'],
            'encrypt'  => false
          ];
        }

        $message = new Message($fields);
        $data['user']->messages()->save($message);
        //  create message status
        self::createMessageStatus($message, $data['room']);
        // update room
        Room::where('id', $data['room']->id)->update(['updated_at' => Carbon::now()]);
        return $message;
    }

    public static function createMessageOnly($data = [])
    {
      $fields = [
        'channel_data' => $data['channelData'] ?? null,
        'content' => $data['content'],
        'encrypt' => false
      ];
      $message = new Message($fields);
      return $message;
    }

    public static function createMessageStatus($message, $room)
    {
        $status = [];
        foreach ($room->participants as $key => $value) {
            $status[] = [
                'message_id' => $message->id,
                'messageable_id' => $value['pivot']['participable_id'],
                'messageable_type' => $value['pivot']['participable_type'],
                'read_at' => $message["messageable_type"] == "user_platform"  ?  Carbon::now() : NULL
            ];
        }

        $messageStatus = $message->messageStatus()->createMany($status);
        return $messageStatus;
    }
}
