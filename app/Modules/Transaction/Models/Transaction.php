<?php

namespace App\Modules\Transaction\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $connection = 'backend';

    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\User');
    }
    public function getTransactionNoAttribute($value)
    {
        return (string)$value;
    }
}
