<?php

namespace App\Modules\Transaction\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Modules\Transaction\Models\Transaction;
use App\Modules\ExternalApi\Models\ExternalApi;
use GuzzleHttp\Client;
use Hashids\Hashids;
use App\Http\Controllers\ApiController;

class TransactionController extends ApiController
{

    public function index(Request $request, $appId)
    {
        $lennaApi = ExternalApi::where(['category' => 'internal', 'provider' => 'lenna'])->first();
        $guzzleProp = ['base_uri' => $lennaApi->base_url];
        $client = new Client($guzzleProp);

        $apiProp = $lennaApi->endpoints()->where('name', 'transaction')->first();
        $response = $client->request(
            $apiProp->method,
            $apiProp->endpoint,
            [
                'query' => array_merge((array)$request->all(), ['appId' => $appId])
            ]
        );
        $response = json_decode($response->getBody()->getContents());
        return response()->json($response);
    }






    public function store(Request $request)
    {

    }


    public function show($id)
    {
        $trx = Transaction::find($id);

        return $this->successResponse([
            'transaction' => $trx,
        ]);
    }


    public function edit($id)
    {

    }


    public function update(Request $request, $transaction, $id)
    {

    }


    public function destroy($transaction, $id)
    {
        $transaction = Transaction::findOrFail($id);
        $transaction->delete();
    }

    public function getTransactionDetail(Request $request, $appId)
    {
        $lennaApi = ExternalApi::where(['category' => 'internal', 'provider' => 'lenna'])->first();
        $guzzleProp = ['base_uri' => $lennaApi->base_url];
        $client = new Client($guzzleProp);

        $apiProp = $lennaApi->endpoints()->where('name', 'transaction-detail')->first();

        $requestBody = [
            'transactionNo' => $request->transactionNo,
        ];
        $response = $client->request(
            $apiProp->method,
            $apiProp->endpoint,
            [
                'json' => $requestBody
            ]
        );
        $response = json_decode($response->getBody()->getContents());

        return $this->successResponse([
            'transactionDetail' => $response->transactionable,
        ]);

    }
}
