<?php

namespace App\Modules\Home\Controllers;

use Illuminate\Http\Request;
use App\Modules\User\Models\User;
use App\Modules\Room\Models\Room;
use App\Http\Controllers\ApiController;
use App\Modules\Room\Controllers\RoomController;

class HomeController extends ApiController
{


    public $roomController;

    public function __construct(RoomController $roomController) {
        $this->roomController = $roomController;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layout/main2');
    }

    public function getNotificationList(Request $request, $appId)
    {
        $room = Room::where('app_id', $appId)->whereHas('users', function ($query) use ($request) {
            $query->where('deleted_at', '=', null);
        })->get();
        $user = [
            'id' => $request->decodedUserId,
            'type' => 'user_platform'
        ];
        $notifList = [];
        // foreach ($room as $key => $item) {
        //     $unreadCount = $this->roomController->getNotif($item->id, $user = [
        //         'id' => $request->decodedUserId,
        //         'type' => 'user'
        //     ]);
        //     if ($unreadCount > 0) {
        //         $notifList[] = (object)[
        //             "id" => "chat-message" . $item->id,
        //             "type" => "chat-message",
        //             "unread_count" => $unreadCount,
        //             "data" => (object)[
        //                 "room" => $item
        //             ],
        //             "items" => [
        //                 (object)[
        //                     "title" => $item->created_by->name,
        //                     "body" => 'New Message',
        //                     "created_at" => $item->created_at->diffForHumans(),
        //                 ]
        //             ]
        //         ];
        //     }
        // }

        return $this->successResponse(['notif_list' => $notifList]);
    }

}
