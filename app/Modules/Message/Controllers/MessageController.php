<?php

namespace App\Modules\Message\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Http\Response;

use App\Modules\Message\Models\Message;
use App\Modules\Room\Models\Room;

use App\Modules\Common\Services\EncryptService;

use App\Http\Controllers\ApiController;

class MessageController extends ApiController
{


    public function updateUnreadMessage(Request $request, $appId)
    {
        $chatRoom = Room::where('id', $request->roomId)->first();
        $senderId = $request->senderId;
        $messageStatus = $chatRoom->messageStatus()->update([
            'read_at' => Carbon::now()->toDateTimeString()
        ]);

        return $this->successResponse(null, 'updated unread message');
    }

    public function getMoreMessage(Request $request, $appId)
    {
        $messages = Message::select('id','content','created_at','messageable_id','messageable_type','room_id','encrypt')->where('room_id',$request->roomId)->where('id', '<', $request->firstMessageId)->orderBy('id', 'desc')->take(10)->get();
        // $messages = Message::where('room_id',$request->roomId)->where('id', '<', $request->firstMessageId)->orderBy('id', 'desc')->take(10)->get();
        // $messages = $messages->transform(function($item){
        //   if ($item->encrypt) {
        //     $decryptedContent = EncryptService::decrypt($item->content, env('ENCRYPT_MESSAGE_KEY'), 20);
        //     $item->content = json_decode($decryptedContent, true);
        //     return $item;
        //   }
        // });
        $messages = $messages->sortBy('id');

        return $this->successResponse(['messages' => $messages->values()->all()]);

    }

    public function getMoreMessageMobile(Request $request, $appId)
    {
      $hashid = new Hashids(env("HASHID_MAIN_SALT"), 6);

      $decoded_user_id = $hashid->decode($request->user_id);

      $is_valid_user_id = count($decoded_user_id) > 0 ? true : false;
      $per_page = 10;
      if(!is_null($per_page)) {
        $per_page = $request->per_page;
      }
      if($is_valid_user_id) {
          $messages = Message::whereHas('room', function($q) use ($request,$appId,$decoded_user_id){
            $q->where('created_by', $decoded_user_id[0]);
          })->orderBy('id', 'desc')->paginate($per_page)->map(function ($message) {
            return [
              "resolveQuery" => $message->content[0]->type,
              "statusCode" => 200,
              "type" => $message->type,
              "date" => Carbon::createFromFormat('Y-m-d H:i:s', $message->created_at)->format('d M Y'),
              "time" => Carbon::createFromFormat('Y-m-d H:i:s', $message->created_at)->format('H:i'),
              "user_type" => $message->messageable_type,
              "result" => [
                "output" => $message->content
              ]
            ];
          });
          return response()->json($messages);
      }else {
        return $this->successResponse(null, "Invalid user id");
      }

    }

    public function getMoreMessageInSearch(Request $request,$appId) {
      $messages = Message::where('room_id', $request->roomId)->where('id', '>=', $request->lastMessageId)->orderBy('id', 'desc')->get();

      $messages = $messages->sortBy('id');

      return $this->successResponse(['messages' => $messages->values()->all()]);
    }
}
