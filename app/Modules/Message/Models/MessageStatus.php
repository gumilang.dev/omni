<?php

namespace App\Modules\Message\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\Relation;

Relation::morphMap([
    'user' => 'App\Modules\User\Models\User',
    'user_platform' => 'App\Modules\User\Models\UserPlatform',
    'bot' => 'App\Modules\Bot\Models\Bot',
]);

class MessageStatus extends Model
{
    protected $connection = 'pgsql';

    protected $guarded = [];

    public function messageable()
    {
        return $this->morphTo();
    }

}
