<?php

namespace App\Modules\Message\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\Relation;
use App\Modules\Common\Services\EncryptService;

Relation::morphMap([
    'user' => 'App\Modules\User\Models\User',
    'user_platform' => 'App\Modules\User\Models\UserPlatform',
    'bot' => 'App\Modules\Bot\Models\Bot',
]);

class Message extends Model
{
    protected $connection = 'pgsql';

    protected $guarded = [];
    protected $appends = [
        'messageable'
    ];
    protected $casts = [
        'status' => 'object',
        'channel_data' => 'array',
        'content' => 'array',
    ];
    public function messageable()
    {
        return $this->morphTo();
    }

    public function getContentAttribute()
    {
        if ($this->attributes['encrypt']) {
          $decryptedContent = EncryptService::decrypt($this->attributes['content'], env('ENCRYPT_MESSAGE_KEY'), 20);
          return $decryptedContent;
        }
        return json_decode($this->attributes['content']);
    }

    public function getMessageableAttribute()
    {
        $messageable = $this->messageable()->first();
        return $messageable;
    }
    public function room()
    {
        return $this->belongsTo('App\Modules\Room\Models\Room');
    }
    public function messageStatus()
    {
        return $this->hasMany('App\Modules\Message\Models\MessageStatus');
    }
}
