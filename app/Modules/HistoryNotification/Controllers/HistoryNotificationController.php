<?php

namespace App\Modules\HistoryNotification\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use App\Modules\Integration\Models\Integration;
use App\Http\Controllers\ApiController;
use App\Modules\ExternalApi\Models\ExternalApi;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\RequestGuzzle;
use Hashids\Hashids;

class HistoryNotificationController extends ApiController
{
    public function getHistoryNotification(Request $request, $appId)
    {

        $sort = $request->get('sort');
        $filterText = $request->get('filterText');
        $per_page = $request->get('per_page');

        $data = DB::table('tripa.notifications as n')
                  ->join('app.users as u', 'n.user_id', '=', 'u.id')->select('n.*','u.id', 'u.name', 'u.nickname', 'u.email')
                  ->orderBy('n.id', 'DESC');

        if ($sort) {
            $sort = explode('|', $sort);
            $data = $data->orderBy($sort[0], $sort[1]);
        }

        if ($filterText) {
            $data = $data->where(function ($query) use ($filterText) {
              $query->where('title', 'ILIKE', '%'.$filterText.'%');
              $query->orWhere('body', 'ILIKE', '%'.$filterText.'%');
              $query->orWhere('message_title', 'ILIKE', '%'.$filterText.'%');
              $query->orWhere('message_body', 'ILIKE', '%'.$filterText.'%');
            });
        }

        if (!$per_page) {
            $per_page = 15;
        }

        return response()->json($data->paginate($per_page));

    }
}
