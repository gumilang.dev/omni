<?php

namespace App\Modules\Twitter\Controllers;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Modules\Log\Models\WebhookLog;
use App\Modules\Integration\Models\Integration;
use App\Modules\App\Models\App;
use App\Modules\User\Models\User;
use App\Modules\Room\Models\Room;
use App\Modules\Message\Models\Message;
use App\Modules\Channel\Models\Channel;
use App\Modules\Livechat\Models\Livechat;

use App\Events\NewUser;
use App\Events\NewMessage;
use App\Events\RequestLive;
use GuzzleHttp\Client;


use App\Modules\Mobile\Helpers\LennaMessageType;
use App\Modules\Log\Helpers\WebhookHelper;
use App\Modules\Chat\Helpers\ChatHelper;

// use App\Modules\Line\Services\LineApiController;
use App\Modules\Twitter\Services\TwitterApiController;
use App\Modules\Bot\Services\BotService;
use App\Modules\Mobile\Services\BackendService;
use App\Modules\BlockUser\Services\BlockUserService;
use App\Http\Controllers\ApiController;
use Abraham\TwitterOAuth\TwitterOAuth;


class TwitterWebhookController extends ApiController
{
    protected $app;
    protected $channel;
    protected $twitterController;
    protected $integration;
    protected $blockUserService;
    public function __construct(
      BlockUserService $blockUserService
    )
    {
        $this->blockUserService = $blockUserService;
        $this->channel = Channel::where('name', 'twitter')->first();
    }
    public function webhookTest(Request $request, $appId)
    {
        $webhookLog = WebhookLog::create([
            "source" => "twitter",
            "app_id" => $appId,
            "events" => \Request::ip(),
            "payload" => json_encode($request->all()),
            "response" => json_encode([
                'success' => true,
                'data' => "testing webhook"
            ])
        ]);
        return $this->successResponse([
            'events' => "testing webhook"
        ]);
    }

    public function webhookCRC(Request $request, $appId)
    {
        if (!$this->integration = WebhookHelper::getIntegrationData('twitter', $appId)) {
            return $this->errorResponse(null, 'integration not active');
        }
        // Challenge-Response Check (CRC) twitter
        $consumerSecret = $this->integration->integration_data['apiKeySecret'];
        if (($request['crc_token'])) {
            $signature = hash_hmac('sha256', $request['crc_token'], $consumerSecret, true);
            $response['response_token'] = 'sha256='.base64_encode($signature);
            print json_encode($response);
          } else {
            $webhookLog = WebhookLog::create([
                "source" => "twitter",
                "app_id" => $appId,
                "events" => 'other-twitter-webhook-get',
                "payload" => json_encode($request->all()),
                "response" => json_encode([
                    'success' => true,
                    'data' => []
                ])
            ]);
            return $this->successResponse([
            ]);

        }

    }

    public function webhook(Request $request, $appId)
    {
        if (!$this->integration = WebhookHelper::getIntegrationData('twitter', $appId)) {
            return $this->errorResponse(null, 'integration not active');
        }

        $this->app = App::find($appId);
        $this->twitterController = new TwitterApiController(['integration' => $this->integration]);
        $botService = new BotService(['appId' => $appId]);
        $backendService = new BackendService(['appId' => $appId]);
        $events = $request;

        if (!empty($events['direct_message_indicate_typing_events']) || !empty($events['direct_message_mark_read_events'])) {
            $webhookLog = WebhookLog::create([
                "source" => "twitter",
                "app_id" => $this->app->id,
                "events" => 'other-twitter-webhook',
                "payload" => json_encode($request->all()),
                "response" => json_encode([
                    'success' => true,
                    'data' => []
                ])
            ]);
            return $this->successResponse([
            ]);
        }

        if (!empty($events['apps'])) {
            $webhookLog = WebhookLog::create([
                "source" => "twitter",
                "app_id" => $this->app->id,
                "events" => 'twitter-webhook-sending',
                "payload" => json_encode($request->all()),
                "response" => json_encode([
                    'success' => true,
                    'data' => []
                ])
            ]);
            return $this->successResponse([
            ]);
        }


        // dd($events);

        if (!empty($events['direct_message_events'])) {
            $response = $this->_incomingMessage($events, $this->app->id);
            // check user is blocked
            if(is_bool($response) && $response = true){
                return $this->successResponse(["data" => $response],"user blocked");
            }
           // if status resolved
            if (WebhookHelper::isResolved(['roomId' => $response['room']->id])) {
                // change status to bot by deleting livechat key
                $this->_deleteLiveChat($response['room']->id);
            }

            broadcast(new NewMessage($response['message'], $response['room'], $this->app));
            $botService = new BotService(['appId' => $appId]);

            if (!WebhookHelper::isLive(['roomId' => $response['room']->id])){
                if (!WebhookHelper::isRequest(['roomId' => $response['room']->id])) {
                    $backendService = new BackendService(['appId' => $appId]);
                    $backendResponse = $backendService->getUserToken([
                        'email' => $response['user']->email,
                        "password" => BackendService::encryptText(env("ENCRYPT_KEY"), "$appId:" . $response['user']->email),
                        "fcm_token" => null,
                        "client" => "twitter",
                        "userId" => $response['user']->id
                    ]);
                    // $backendService->updateLocationAndIp([
                    //   'ip' => $request->ipinfo->ip,
                    //   'city' => $request->ipinfo->city,
                    //   'location' => $request->ipinfo->loc,
                    //   'user_id' => $response['user']->id,
                    //   'last_update' => $response['user']->updated_at
                    // ]);


                    // $botService = new BotService(['appId' => $appId]);
                    $botResponse = $botService->replyMessage([
                        'userId' => $response['user']->id,
                        'content' => $response['message']->content[0],
                        'room' => $response['room'],
                        'lat' => $request->lat ?? 0,
                        'lon' => $request->lon ?? 0,
                        'channel' => 'twitter',
                        "headers" => ["Authorization" => "$backendResponse->token_type $backendResponse->access_token"],
                        'lang' => $response['user']->lang
                    ]);

                  }
                }

                 // if bot not integrated, change status to request
                $botIntegration = $botService->getIntegration();
                if (!$botIntegration) {
                  if (!WebhookHelper::isLive(['roomId' => $response['room']->id])) {
                    if (!WebhookHelper::isRequest(['roomId' => $response['room']->id])) {
                      WebhookHelper::requestLivechat($response['user']->id, $appId);
                    }
                  }
                }

                // $webhookLog = WebhookLog::create([
                //     "source" => "twitter",
                //     "app_id" => $this->app->id,
                //     "events" => 'other-twitter-webhook-bot',
                //     "payload" => json_encode($request->all()),
                //     "response" => json_encode([
                //         'success' => true,
                //         'data' => $botResponse['nlp']
                //     ])
                // ]);

                $responseLog = (object)[
                    'output' => $botResponse['nlp']->response ?? null,
                    'quickbutton' => $botResponse['nlp']->quickbutton ?? [],
                    'type' => $botResponse['nlp']->type ?? null
                ];

                $webhookLog = WebhookLog::create([
                    "source" => "twitter",
                    "app_id" => $this->app->id,
                    "events" => 'message',
                    "payload" => json_encode($request->all()),
                    "response" => json_encode([
                        'success' => true,
                        'data' => $responseLog
                    ]),
                    "user_id" => $response['user']->id,
                    "bot_id" => $botIntegration ? $botIntegration->integration_data['botId'] : NULL
                ]);

                return $this->successResponse([
                    'events' => 'message',
                    'result' => (object)[
                        'output' => $botResponse['nlp']->response ?? null,
                        'quickbutton' => $botResponse['nlp']->quickbutton ?? [],
                    ],
                    // 'bot_response' => $botResponse, //testing to see bot response & response from qontak
                    // 'backend_response' => $backendResponse //testing to see response

                ]);


        } else {
            $webhookLog = WebhookLog::create([
                "source" => "twitter",
                "app_id" => $this->app->id,
                "events" => 'other-twitter-webhook-else',
                "payload" => json_encode($request->all()),
                "response" => json_encode([
                    'success' => true,
                    'data' => []
                ])
            ]);
            return $this->successResponse([
            ]);
        }


    }

    public function _incomingMessage($twitterData, $appId)
    {
        $senderId = $twitterData['direct_message_events'][0]['message_create']['sender_id'];
        $users = ($twitterData['users'][$senderId]); //get user data from webhook
        $user = User::where([['channel_user_id', "=", $users['id']], ['app_id', "=", $appId]])->first();

        // register user if not registered
        if (!$user) {
            $user = $this->registerUser($users, $appId);
        }

         // check if user blocked
        if($this->blockUserService->isBlocked($user->email, $appId)) {
          return true;
        }

        $chatRoom = ChatHelper::createRoomAndParticipate([
                'channel' => $this->channel,
                'user' => $user,
                'appId' => $appId
              ]);
        $msgData = $twitterData['direct_message_events'][0];

        $channelData = $msgData;
        $content[] = $this->_setMessageType($msgData, $appId);
        $message = ChatHelper::createMessage(['channelData' => $channelData, 'content' => $content, 'user' => $user, 'room' => $chatRoom]);

        return [
            'message' => $message,
            'user' => $user,
            'room' => $chatRoom
        ];
    }

    public function registerUser($twitterData, $appId)
    {
        $name = $twitterData['name'];
        $plainName = strtolower(preg_replace('/\s+/', '', $twitterData['name']));
        $email = strtolower($plainName . $twitterData['id'] . "@twitter.com");
        // $email = BackendService::encryptText(env("ENCRYPT_PROFILE_KEY"), $email);
        $fields = [
            'app_id' => $appId,
            'channel_user_id' => $twitterData['id'],
            'name' => $twitterData['name'],
            'nickname' => $twitterData['name'],
            'password' => bcrypt("$appId:$email"),
            'picture' => $response->pictureUrl ?? env("APP_URL") . "/images/pictures/no_avatar.jpg",
            'email' => $email
        ];
        $user = $this->channel->users()->firstOrCreate(
            ['channel_user_id' => $twitterData['id'], 'app_id' => $appId],
            $fields
        );
        return $user;
    }

    public function _setMessageType($message, $appId)
    {
        // return $message['message_create']['message_data']['attachment']['media']['media_url_https'];
        $lennaMessageType = new LennaMessageType;
        $integration = Integration::where([['app_id', "=", $appId], ['status', "=", true], ['channel_id', "=", 9]])->first(); //id 9 = twitter
        $data = ['integration' => $integration];
        $twitterApiController = new TwitterApiController($data);

        if (empty($message['message_create']['message_data']['attachment'])) {
            $content = $lennaMessageType->text(['text' => $message['message_create']['message_data']['text']]);
        } else if ($message['message_create']['message_data']['attachment']['media']['type'] == 'photo') {
            $twitterApiController = new TwitterApiController($data);
            $mediaUrl = $message['message_create']['message_data'];
            $getMedia = $twitterApiController->getImage($mediaUrl);
            $content = $lennaMessageType->image([
                'previewImageUrl' => $getMedia['imgUrl'],
                'caption' => $getMedia['caption'] ?? ""
            ]);
        } else if ($message['message_create']['message_data']['attachment']['media']['type'] == 'video'){
            // $twitterApiController = new TwitterApiController($data);
            // $mediaUrl = $message['message_create']['message_data'];
            // $getMedia = $twitterApiController->getVideo($mediaUrl);
            // return $getMedia;
            // $content = $lennaMessageType->video([
            //     'originalContentUrl' => $getMedia['videoUrl'],
            //     'previewVideoUrl' => $getMedia['videoUrl'],
            //     'caption' => $message['message_create']['message_data']['text'] ?? ""
            //     // 'duration'=> $getMedia['duration']
            // ]);
            $content = $lennaMessageType->text(['text' => 'Video Type under development']);
        } else {
            $content = $lennaMessageType->text(['text' => 'This message type under development']);
        }

        return $content;
    }

    public function _deleteLiveChat($roomId)
    {
        Livechat::where('room_id', $roomId)->delete();
    }




}
