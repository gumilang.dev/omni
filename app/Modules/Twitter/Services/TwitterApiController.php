<?php

namespace App\Modules\Twitter\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\ExternalApi\Models\ExternalApi;
use GuzzleHttp\Client;
use App\Modules\Line\Helpers\LineMessageType;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use App\Modules\Common\Services\EncryptService;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\RequestGuzzle;
use Abraham\TwitterOAuth\TwitterOAuth;
use App\Modules\Twitter\Helpers\TwitterMessageType;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;



class TwitterApiController extends Controller
{
    protected $client;
    protected $twitterApi;

    public function __construct($data)
    {
        $this->data = $data;
        $this->apiKey = $this->data['integration']['integration_data']['apiKey'];
        $this->apiKeySecret = $this->data['integration']['integration_data']['apiKeySecret'];
        $this->bearerToken = $this->data['integration']['integration_data']['bearerToken'];
        $this->accessToken = $this->data['integration']['integration_data']['accessToken'];
        $this->accessTokenSecret = $this->data['integration']['integration_data']['accessTokenSecret'];
        $this->environtment = $this->data['integration']['integration_data']['environtment'];
        $this->connection = new TwitterOAuth($this->apiKey, $this->apiKeySecret, $this->accessToken, $this->accessTokenSecret);

        // $this->twitterApi = ExternalApi::where(['category' => 'channel', 'provider' => 'twitter'])->first();
        // $guzzleProp = [
        //     'base_uri' => $this->twitterApi->base_url
        // ];
        // $this->client = new Client($guzzleProp);
    }

    public function sendDirectMessage($newMessage)
    {
        foreach ($newMessage->content as $key => $content) {
             $converted = $this->_convertToTwitterMessageType($content);
             if (isset($converted['multiple'])) {

                foreach ($converted['multiple'] as $key => $text) {
                    $response = $this->callSendDirectMessage(['message' => $text], $converted['type']);
                }
            } else {
                $response = $this->callSendDirectMessage($converted, $converted['type']);
            }
        }

        return $response;
    }

    public function callSendDirectMessage($newMessage, $type)
    {
        switch ($type) {
            case 'text':
                $this->sendDmText($newMessage, $type);
                break;
            case 'image':
                $this->sendDmImage($newMessage, $type);
                break;
            default:
                $this->sendDmText($newMessage, $type);
                break;
        }
    }

    public function sendDmText($message, $type)
    {
        $userId = $this->data['chatRoom']->created_by->channel_user_id;
        $requestData = [
            'event' => [
                'type' => 'message_create',
                'message_create' => [
                    'target' => [
                        'recipient_id' => $userId
                    ],
                    'message_data' => [
                        'text' => $message['message']
                    ]
                ]
            ]
        ];
        $result = $this->connection->post('direct_messages/events/new', $requestData, true);
        return $result;
    }

    public function sendDmImage($message, $type)
    {
        $checkImgUrl = strpos($message['image'], 'botstudio/public/storage/');

        //check if image from botstudio
        if ($checkImgUrl) {
            $imgName = substr($message['image'], -15, 15);
            $contents = file_get_contents($message['image']);
            Storage::disk('public_chat')->put($imgName, $contents);
        	$media1 = $this->connection->upload('media/upload', ['media' => public_path('/upload/chat/').$imgName]);
        } else {
            $imgName = str_replace(env("APP_URL")."/upload/chat/" ,'',$message['image']);
            $media1 = $this->connection->upload('media/upload', ['media' => public_path('/upload/chat/').$imgName]);
        }

        $userId = $this->data['chatRoom']->created_by->channel_user_id;
        $parameters = [
            'id' => $media1->media_id_string
        ];
        $requestData = [
            'event' => [
                'type' => 'message_create',
                'message_create' => [
                    'target' => [
                        'recipient_id' => $userId
                    ],
                    'message_data' => [
                        'text' => '',
                        'attachment' => [
                            'type' => "media",
                            'media' => $parameters
                        ]
                    ],

                ]
            ]
        ];
        $result = $this->connection->post('direct_messages/events/new', $requestData, true);
        return $result;


    }

    public function _convertToTwitterMessageType($message)
    {

        $twitterMessageType = new TwitterMessageType;
        $dmMessageType = $twitterMessageType->{$message->type}($message);
        // convert to qontak api need
        switch ($message->type) {
            // case 'html':
            //     $converted = [
            //         'message' => $dmMessageType['text'],
            //         'type' => 'text'
            //     ];
            //     break;
            case 'image':
                $converted = [
                    'image' => $dmMessageType['imageUrl'],
                    'caption' => $dmMessageType['caption'],
                    'type' => 'image'
                ];
                break;
            case 'file':
                $converted = [
                    'document' => $dmMessageType['fileUrl'],
                    'caption' => $dmMessageType['fileName'],
                    'type' => 'document'
                ];
                break;
            case 'template':
                $converted = [
                    'message' => $dmMessageType['text'],
                    'type' => 'text'
                ];
                break;
            default:
                if (isset($dmMessageType['multiple'])) {
                    $converted = [
                        'multiple' => $dmMessageType['multiple'],
                        'type' => 'text'
                    ];
                } else {
                    $converted = [
                        'message' => $dmMessageType['text'],
                        'type' => 'text'
                    ];
                }
                break;
        }

        return $converted;
    }

    public function getImage($media)
    {
        $mediaUrl = $media['attachment']['media']['media_url_https'];
        // dd($mediaUrl);
        $caption = str_replace($media['attachment']['media']['url'] ,"", $media['text']);

        $stack = HandlerStack::create();
        $middleware = new Oauth1([
            'consumer_key'    => $this->apiKey,
            'consumer_secret' => $this->apiKeySecret,
            'token'           => $this->accessToken,
            'token_secret'    => $this->accessTokenSecret
        ]);
        $stack->push($middleware);

        $client = new Client([
            // 'base_uri' => 'https://ton.twitter.com/1.1/',
            'handler' => $stack,
            'auth' => 'oauth'
        ]);

        try {
            $response = $client->get($mediaUrl);
        } catch (\Exception $e) {
            $resp = json_decode($e->getResponse()->getBody());
            return $resp;
        }

        $current_timestamp = Carbon::now()->timestamp;
        $ext = substr($mediaUrl, -8, 8);
        $name = $current_timestamp.$ext;

        // return $getName = $response->getHeader();
        $data = ($response->getBody()->getContents());
        Storage::disk('public_chat')->put($name, $data);
        $imgUrl =  env("APP_URL") . "/upload/chat/" . $name;
        return [
            'imgUrl' => $imgUrl,
            'caption' => $caption
        ];
    }

    public function getVideo($media)
    {
        // $mediaUrl = $media['attachment']['media']['video_info']['variants'][1];
        $mediaUrl = $media['attachment']['media']['expanded_url'];
        // dd($mediaUrl);
        $caption = str_replace($media['attachment']['media']['url'] ,"", $media['text']);
        $stack = HandlerStack::create();
        $middleware = new Oauth1([
            'consumer_key'    => $this->apiKey,
            'consumer_secret' => $this->apiKeySecret,
            'token'           => $this->accessToken,
            'token_secret'    => $this->accessTokenSecret
        ]);
        $stack->push($middleware);

        $client = new Client([
            // 'base_uri' => 'https://ton.twitter.com/1.1/',
            'handler' => $stack,
            'auth' => 'oauth'
        ]);

        try {
            $response = $client->get($mediaUrl);
        } catch (\Exception $e) {
            $resp = $e;
            return 'error';
        }
        $current_timestamp = Carbon::now()->timestamp;
        // $ext = substr($mediaUrl, -8, 8); //get extension from last url
        $name = $current_timestamp.".mp4";

        // return $getName = $response->getHeader();
        return $data = ($response->getBody()->getContents());
        Storage::disk('public_chat')->put($name, $data);
        $videoUrl =  env("APP_URL") . "/upload/chat/" . $name;
        return [
            'imgUrl' => $videoUrl,
            'caption' => $caption
        ];
    }


}
