<?php

namespace App\Modules\Line\Helpers;

class LineMessageType
{
    public function __construct(){}

    public function _cutText($text, $limit = 18, $cutTo = 16)
    {
        return strlen($text) > $limit ? substr($text, 0, $cutTo) . " ..." : $text;
    }

    public function text($input = [])
    {
        $output = [
            'type' => 'text',
            // 'text' => $input['text'] ?? '',
            'text' => $input->text ?? '',
        ];
        return $output;
    }

    public function image($input = [])
    {
        $output = [
            'type' => 'image',
            'originalContentUrl' => $input->originalContentUrl ?? ($input->previewImageUrl ?? env('APP_URL').'/images/pictures/image_not_found.jpg'),
            'previewImageUrl' => $input->previewImageUrl ?? env('APP_URL').'/images/pictures/image_not_found.jpg',
        ];
        return $output;
    }

    public function file($input = [])
    {
        $output = [
            'type' => 'flex',
            'altText' => 'Ticket file',
            'contents' => [
                "type" => "bubble",
                "hero" => [
                    "type" => "image",
                    "url" => "https://app.lenna.ai/app/public/images/pictures/plane-ticket.png",
                    "size" => "sm",
                    "aspectRatio" => "10:10",
                    "aspectMode" => "cover",
                    "action" => [
                        "type" => "uri",
                        "uri" => $input['fileUrl']
                    ]
                ],
                "body" => [
                    "type" => "box",
                    "layout" => "vertical",
                    "contents" => [
                        [
                            "type" => "text",
                            "text" => $input['fileName'] ?? "File",
                            "weight" => "bold",
                            "size" => "md"
                        ],
                        [
                            "type" => "text",
                            "text" => $input['fileCaption'] ?? 'Click button below to download your file',
                            "size" => "sm",
                            "wrap" => true
                        ]
                    ]
                ],
                "footer" => [
                    "type" => "box",
                    "layout" => "vertical",
                    "spacing" => "sm",
                    "contents" => [

                        [
                            "type" => "button",
                            "style" => "link",
                            "height" => "sm",
                            "action" => [
                                "type" => "uri",
                                "label" => "Download",
                                "uri" => $input['fileUrl']
                            ]
                        ]
                    ],
                    "flex" => 0
                ]
            ]
        ];
        // $output = [
        //     'type' => 'text',
        //     'text' => $input->fileUrl ?? '',
        // ];

        return $output;
    }

    public function video($input = [])
    {
        $output = [
            'type' => 'video',
            'originalContentUrl' => $input['originalContentUrl'] ?? env('APP_URL').'/images/videos/default.mp4',
            'previewImageUrl' => $input['previewImageUrl'] ?? env('APP_URL').'/images/videos/default.mp4',
        ];

        return $output;
    }

    public function audio($input = [])
    {
        $output = [
            'type' => 'audio',
            'originalContentUrl' => $input['originalContentUrl'] ?? env('APP_URL').'/images/audios/default.m4a',
            'duration' => $input['duration'] ?? 7000,
        ];
        return $output;
    }

    public function confirm($input = [])
    {
        $output = [
            'type' => 'template',
            'altText' => $input->altText ?? 'Confirm Message',
            'template' => (object)[
                'type' => 'confirm',
                'text' => $input->text ?? '',
                'actions' => [
                    (object)[
                        'type' => 'message',
                        'label' => $input->actions[0]->label ?? 'Ya',
                        'text' => $input->actions[0]->text ?? 'ya'
                    ],
                    (object)[
                        'type' => 'message',
                        'label' => $input->actions[1]->label ?? 'Tidak',
                        'text' => $input->actions[1]->text ?? 'tidak'
                    ]
                ]
            ]

        ];

        return $output;


    }

    public function button($input = [])
    {
        $actions = [];
        foreach ($input->actions as $key => $actionVal) {

            $actions[] = (object)[
                'type' => $actionVal->type,
                'text' => $actionVal->text,
                'label' => strlen($actionVal->label) > 18 ? substr($actionVal->label, 0, 15) . "..." : $actionVal->label,
                'data' => http_build_query(['action' => $actionVal->text])
            ];

        }
        $output = [
            'type' => 'template',
            'altText' => 'buttons',
            'template' => (object)[
                'type' => 'buttons',
                'text' => $input->text ?? '',
                'actions' => $actions,
            ]

        ];
        return $output;

    }

    public function carousel($input = [])
    {
        $output = [
            'type' => 'template',
            'altText' => 'Carousel',
            'template' => (object)[
                'type' => 'carousel',
                'imageAspectRatio' => $input->imageAspectRatio,
                'imageSize' => $input->imageSize
            ]

        ];
        if (!isset($input->columns))
            return $output;

        $input->columns = array_slice($input->columns, 0, 5);
        // dd(count($input->columns

        foreach ($input->columns as $data) {
            // dd($data->actions);
            if (count($data->actions) == 0) {
                $actions = [
                    (object)[
                        'type' => 'postback',
                        'label' => 'Default Button',
                        'text' => 'default',
                        'data' => 'action=default',
                    ]
                ];
            } else {
            // convert to line actions
                $actions = collect($data->actions)->map(function ($act) {
                    switch ($act->type) {
                        case 'postback':
                            $act->label = $this->_cutText($act->label);
                            $act->text = $act->data ?? $act->label ?? 'default';
                            $act->data = http_build_query(['action' => $act->data ?? $act->label ?? 'default']);
                            break;
                        case 'uri':
                            $act->label = $this->_cutText($act->label);

                            if (is_null($act->data)) {
                                $act->uri = 'https://google.com';
                            } else {
                                $act->uri = strpos($act->data, 'http') ? $act->data : "https://{$act->data}";
                            }
                            unset($act->data);
                            break;
                        default:
                            $act->type = 'postback';
                            $act->label = $this->_cutText($act->label);

                            $act->text = $act->data ?? $act->label ?? 'default';
                            $act->data = http_build_query(['action' => $act->data ?? $act->label ?? 'default']);
                            break;
                    }

                    return $act;

                })->toArray();
            }
            $output['template']->columns[] = (object)[
                'thumbnailImageUrl' => $data->thumbnailImageUrl ?? '',
                'imageBackgroundColor' => $data->imageBackgroundColor ?? '#FFFFFF',
                'title' => $this->_cutText($data->title) ?? '',
                'text' => $this->_cutText($data->text, 50, 50) ?? '...',
                'actions' => $actions,
            ];
        }
        // dd($output);

        return $output;


    }

    public function flightScheduleCarousel($input = [])
    {
        $output = [
            'type' => 'flex',
            'altText' => 'flextMessage',
            'contents' => (object)[
                'type' => 'carousel',

            ]

        ];
        if (!isset($input->columns))
            return $output;

        // $input->columns = array_slice($input->columns, 0, 5);

        foreach ($input->columns as $col) {


            $horizontalColumn = (object)[
                'type' => 'box',
                'layout' => 'horizontal',
                'margin' => 'md',
                'contents' => [

                    (object)[
                        'type' => 'box',
                        'layout' => 'vertical',
                        'margin' => 'md',
                        'contents' => [
                            (object)[
                                'type' => 'text',
                                'text' => $col->airlineName,
                                'color' => '#000000',
                                'align' => 'center',
                                'gravity' => 'top',
                                'wrap' => false,
                                'size' => 'xs',
                                'wrap' => true
                            ],
                            (object)[
                                'type' => 'text',
                                'text' => $col->flightNo,
                                'color' => '#8f8f90',
                                'align' => 'center',
                                'gravity' => 'top',
                                'wrap' => false,
                                'size' => 'xxs',
                                'flex' => 2
                            ],
                            (object)[
                                'type' => 'text',
                                'text' => $col->flightType,
                                'color' => '#8f8f90',
                                'align' => 'center',
                                'gravity' => 'top',
                                'wrap' => false,
                                'size' => 'xxs',
                                'flex' => 2,
                            ],
                        ]

                    ],
                    (object)[
                        'type' => 'box',
                        'layout' => 'vertical',
                        'margin' => 'md',
                        'contents' => [

                            (object)[
                                'type' => 'text',
                                'text' => $col->orgCity,
                                'color' => '#000000',
                                'align' => 'center',
                                'gravity' => 'top',
                                'wrap' => true,
                                'size' => 'xxs',
                                'flex' => 2
                            ],
                            (object)[
                                'type' => 'text',
                                'text' => $col->departTime,
                                'color' => '#8f8f90',
                                'align' => 'center',
                                'gravity' => 'top',
                                'wrap' => false,
                                'size' => 'xxs',
                                'flex' => 2,
                            ],
                        ]

                    ],

                    // https://cdn2.iconfinder.com/data/icons/logistics-delivery-19/64/logistics_delivery-18-512.png
                    // icon

                    (object)[

                        "type" => "image",
                        "url" => "https://cdn2.iconfinder.com/data/icons/logistics-delivery-19/64/logistics_delivery-18-512.png",
                        "size" => "xxs",
                        "flex" => 0,
                        "gravity" => "center"

                    ],
                    // DESTINATION
                    (object)[
                        'type' => 'box',
                        'layout' => 'vertical',
                        'margin' => 'md',
                        'contents' => [

                            (object)[
                                'type' => 'text',
                                'text' => $col->destCity,
                                'color' => '#000000',
                                'align' => 'center',
                                'gravity' => 'top',
                                'wrap' => true,
                                'size' => 'xxs',
                                'flex' => 2

                            ],
                            (object)[
                                'type' => 'text',
                                'text' => $col->arriveTime,
                                'color' => '#8f8f90',
                                'align' => 'center',
                                'gravity' => 'top',
                                'wrap' => false,
                                'size' => 'xxs',
                                'flex' => 2

                            ],
                        ]

                    ],
                    // PRICE AND ACTION
                    (object)[
                        'type' => 'box',
                        'layout' => 'vertical',
                        'margin' => 'md',
                        'contents' => [
                            (object)[
                                'type' => 'text',
                                'text' => $col->price,
                                'color' => '#000000',
                                'align' => 'center',
                                'gravity' => 'top',
                                'wrap' => true,
                                'size' => 'xs',
                            ],
                            (object)[
                                'type' => 'text',
                                'text' => 'Beli',
                                'color' => '#2689F2',
                                'align' => 'center',
                                'gravity' => 'bottom',
                                'wrap' => true,
                                'size' => 'sm',
                                'action' => (object)[
                                    'type' => 'message',
                                    'label' => 'Pilih',
                                    'text' => $col->actions->data,
                                ]
                            ],

                        ]

                    ]
                ]
            ];
            $verticalRow[] = (object)[
                'type' => 'box',
                'layout' => 'vertical',
                'margin' => 'sm',
                'contents' => [$horizontalColumn]
            ];
        }


        $chunkOfRow = array_chunk($verticalRow, 5);

        foreach ($chunkOfRow as $key => $singleVertical) {
            $flexTemplate = (object)[
                'type' => 'bubble',
                'body' => (object)[
                    'type' => 'box',
                    'layout' => 'vertical',
                    'margin' => 'sm',

                ]
            ];
            $flexTemplate->body->contents = $singleVertical;
            $multiple[] = $flexTemplate;
        }
        $output['contents']->contents = $multiple;

        // dd($output);
        return $output;
    }

    public function summary($input = [])
    {
        $output = [
            'type' => 'flex',
            'altText' => 'flextMessage',
            'contents' => (object)[
                'type' => 'bubble',
                'body' => (object)[
                    'type' => 'box',
                    'layout' => 'vertical',
                ]
            ]
        ];

        foreach ($input->columns as $key => $col) {
            $summaryData[] = (object)[
                "type" => "box",
                "layout" => "horizontal",
                "contents" => [
                    (object)[
                        "type" => "text",
                        "text" => "$col->field",
                        "size" => "sm",
                        "color" => "#555555",
                        "weight" => "bold",
                        "wrap" => true,
                        "flex" => 1
                    ],
                    (object)[
                        "type" => "text",
                        "text" => "$col->value",
                        "size" => "sm",
                        "color" => "#111111",
                        "align" => "end",
                        "wrap" => true,
                    ],
                ]
            ];
        }
        $verticalRow = [
            (object)[
                "type" => "text",
                "text" => $input->title ?? 'Order Summary',
                "weight" => "bold",
                "color" => "#488AC7",
                "size" => "xl"
            ],
            (object)[
                "type" => "separator",
                "margin" => "md"
            ],
            (object)[
                "type" => "box",
                "layout" => "vertical",
                "spacing" => "sm",
                "margin" => "md",
                "contents" => $summaryData
            ]
        ];

        $output['contents']->body->contents = $verticalRow;
        // dd($output);
        return $output;

    }

    public function transaction($input = [])
    {
        return $this->summary($input);
    }

    public function quickbutton($quickbutton)
    {
        $items = [];
        foreach ($quickbutton as $data) {
            if (empty($data) || $data == " ") {
                $data = 'batal';
            }
            $items[] = (object)[
                'type' => 'action',
                'action' => (object)[
                    'type' => 'message',
                    'label' => strlen($data) > 18 ? substr($data, 0, 15) . " ..." : $data,
                    'text' => $data
                ]
            ];
        }
        $output = [
            'quickReply' => (object)[
                'items' => $items
            ]
        ];

        return $output;

    }

    public function template($input = [])
    {
      $template = $input->template ?? '';
      $template = str_replace('</p>', "\n", $template);
      $template = str_replace('</br>', "\n", $template);
      $template = strip_tags($template);
      $output = [
        'type' => 'text',
        'text' => $template,
      ];
      return $output;
    }
}
