<?php

namespace App\Modules\Line\Controllers;

use Hashids\Hashids;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Modules\Log\Models\WebhookLog;
use App\Modules\Integration\Models\Integration;
use App\Modules\App\Models\App;
use App\Modules\User\Models\User;
use App\Modules\Room\Models\Room;
use App\Modules\Message\Models\Message;
use App\Modules\Channel\Models\Channel;
use App\Modules\Livechat\Models\Livechat;

use App\Events\NewUser;
use App\Events\NewMessage;
use App\Events\RequestLive;

use App\Modules\Mobile\Helpers\LennaMessageType;
use App\Modules\Log\Helpers\WebhookHelper;
use App\Modules\Chat\Helpers\ChatHelper;

use App\Modules\Line\Services\LineApiController;
use App\Modules\Bot\Services\BotService;
use App\Modules\Mobile\Services\BackendService;
use App\Modules\BlockUser\Services\BlockUserService;
use App\Http\Controllers\ApiController;

class LineWebhookController extends ApiController
{
    protected $app;
    protected $channel;
    protected $lineController;
    protected $integration;
    protected $blockUserService;
    public function __construct(
      BlockUserService $blockUserService
    )
    {
        $this->blockUserService = $blockUserService;
        $this->channel = Channel::where('name', 'line')->first();
    }
    public function webhookTest(Request $request, $appId)
    {
        $webhookLog = WebhookLog::create([
            "source" => "line",
            "app_id" => $appId,
            "events" => \Request::ip(),
            "payload" => json_encode($request->all()),
            "response" => json_encode([
                'success' => true,
                'data' => "testing webhook"
            ])
        ]);
        return $this->successResponse([
            'events' => "testing webhook"
        ]);
    }
    public function webhook(Request $request, $appId)
    {
        if (count($request->events) == 0) {
          return response()->json([
              "success" => true,
              "events" => "verify webhook"
          ], 200);
        }
        $events = array_first($request->events);
        if ($events['replyToken'] == "00000000000000000000000000000000") {
            return response()->json([
                "success" => true,
                "events" => "verify webhook"
            ], 200);
        }
        if (!$this->integration = WebhookHelper::getIntegrationData('line', $appId))
            return $this->errorResponse(null, "integration not active");

        $this->app = App::find($appId);
        $this->lineController = new LineApiController(['integration' => $this->integration]);
        $botService = new BotService(['appId' => $appId]);
        $backendService = new BackendService(['appId' => $appId]);

        switch ($events['type']) {
            case 'follow':
                $user = $this->registerUser($events, $this->app->id);

                broadcast(new NewUser($user, $this->app));
                $responseLog = $user;
                break;
            case 'message':
                $response = $this->incomingMessage($events, $this->app->id);
                // check if user already blocked
                if(is_bool($response) && $response == true) {
                  return $this->successResponse(["data" => $response],"user blocked");
                }
                // if status resolved
                if (WebhookHelper::isResolved(['roomId' => $response['room']->id])) {
                  // change status to bot by deleting livechat key
                  $this->deleteLiveChat($response['room']->id);
                }

                broadcast(new NewMessage($response['message'], $response['room'], $this->app));

                // if not live or status is not request
                if (!WebhookHelper::isLive(['roomId' => $response['room']->id]))
                {
                  if (!WebhookHelper::isRequest(['roomId' => $response['room']->id]))
                  {
                    // $backendService = new BackendService(['appId' => $appId]);
                    // $backendService->updateLocationAndIp([
                    //   'ip' => $request->ipinfo->ip,
                    //   'city' => $request->ipinfo->city,
                    //   'location' => $request->ipinfo->loc,
                    //   'user_id' => $response['user']->id,
                    //   'last_update' => $response['user']->updated_at
                    // ]);

                    $backendResponse = $backendService->getUserToken([
                        'email' => $response['user']->email,
                        "password" => BackendService::encryptText(env("ENCRYPT_KEY"), "$appId:" . $response['user']->email),
                        "fcm_token" => null,
                        "client" => "line",
                        "userId" => $response['user']->id
                    ]);

                    // $botService = new BotService(['appId' => $appId]);
                    $botResponse = $botService->replyMessage([
                        'userId' => $response['user']->id,
                        'content' => $response['message']->content[0],
                        'room' => $response['room'],
                        'lat' => $request->lat ?? 0,
                        'lon' => $request->lon ?? 0,
                        'channel' => 'line',
                        "headers" => ["Authorization" => "$backendResponse->token_type $backendResponse->access_token"],
                        'lang' => $response['user']->lang
                    ]);
                  }
                }
                $responseLog = (object)[
                    'output' => $botResponse['nlp']->response ?? null,
                    'quickbutton' => $botResponse['nlp']->quickbutton ?? [],
                    'type' => $botResponse['nlp']->type ?? null
                ];
                break;
            case 'unfollow':
                return 'this is unfollow';

                break;
        }

        // if bot not integrated, change status to request
        $botIntegration = $botService->getIntegration();
        if (!$botIntegration) {
          if (!WebhookHelper::isLive(['roomId' => $response['room']->id])) {
            if (!WebhookHelper::isRequest(['roomId' => $response['room']->id])) {
              WebhookHelper::requestLivechat($response['user']->id, $appId);
            }
          }
        }

        // $last_update = $response['user']->updated_at;
        // $diff = $last_update->diffInDays(Carbon::now('Asia/Jakarta'));
        // if ($diff > 1) {
        //   $backendService->updateLocationAndIp([
        //     'ip' => $request->ipinfo->ip ?? NULL,
        //     'city' => $request->ipinfo->city ?? NULL,
        //     'location' => $request->ipinfo->loc ?? NULL,
        //     'user_id' => $response['user']->id,
        //     'last_update' => $response['user']->updated_at
        //   ]);
        // }

        $webhookLog = WebhookLog::create([
            "source" => "line",
            "app_id" => $this->app->id,
            "events" => $events['type'],
            "payload" => json_encode($request->all()),
            "response" => json_encode([
                'success' => true,
                'data' => $responseLog ?? 'unfollow'
            ]),
            "user_id" => $response['user']->id,
            "bot_id" => $botIntegration ? $botIntegration->integration_data['botId'] : NULL
        ]);

        return $this->successResponse([
            'events' => $events['type'],
            'result' => (object)[
                'output' => $botResponse['nlp']->response ?? null,
                'quickbutton' => $botResponse['nlp']->quickbutton ?? [],
            ]
        ]);

    }

    public function registerUser($lineData, $appId)
    {
        $response = $this->lineController->getUserProfile($lineData['source']['userId']);
        $plainName = strtolower(preg_replace('/\s+/', '', $response->displayName));
        $email = strtolower($plainName . $response->userId . "@line.com");
        // $email = BackendService::encryptText(env("ENCRYPT_PROFILE_KEY"), $email);
        $fields = [
            'app_id' => $appId,
            'channel_user_id' => $response->userId,
            'name' => $response->displayName,
            'nickname' => $response->displayName,
            'password' => bcrypt("$appId:$email"),
            'picture' => $response->pictureUrl ?? env("APP_URL") . "/images/pictures/no_avatar.jpg",
            'email' => $email
        ];
        $user = $this->channel->users()->firstOrCreate(
            ['channel_user_id' => $response->userId, 'app_id' => $appId],
            $fields
        );
        return $user;
    }

    public function incomingMessage($lineData, $appId)
    {
        $user = User::where([['channel_user_id', "=", $lineData['source']['userId']], ['app_id', "=", $appId]])->first();
        // register user if not registered
        if (!$user) {
            $user = $this->registerUser($lineData, $appId);
        }

         // check if user blocked
         if($this->blockUserService->isBlocked($user->email, $appId)) {
          return true;
        }

        $chatRoom = ChatHelper::createRoomAndParticipate(['channel' => $this->channel, 'user' => $user, 'appId' => $appId]);

        $channelData = [
            'message_id' => $lineData['message']['id'],
            'reply_token' => $lineData['replyToken'],
            'timestamp' => $lineData['timestamp'],
        ];
        $content[] = $this->setMessageType($lineData['message']);
        $message = ChatHelper::createMessage(['channelData' => $channelData, 'content' => $content, 'user' => $user, 'room' => $chatRoom]);

        return [
            'message' => $message,
            'user' => $user,
            'room' => $chatRoom
        ];
    }

    public function setMessageType($message)
    {
      // dd($message['text']);
        $lennaMessageType = new LennaMessageType;
        switch ($message['type']) {
            case 'text':
                $content = $lennaMessageType->text(['text' => $message['text']]);
                break;
            case 'image':

                $filename = $this->lineController->getMessageContent($message['id']);

                $previewImageUrl = asset('upload/line/' . $filename);
                $content = $lennaMessageType->image(['previewImageUrl' => $previewImageUrl]);

                break;
            case 'video':

                $filename = $this->lineController->getMessageContent($message['id']);

                $originalContentUrl = asset('upload/line/' . $filename);
                $content = $lennaMessageType->video(['originalContentUrl' => $originalContentUrl, "duration" => $message['duration']]);

                break;
            case 'audio':

                $filename = $this->lineController->getMessageContent($message['id']);

                $originalContentUrl = asset('upload/line/' . $filename);
                $content = $lennaMessageType->audio(['originalContentUrl' => $originalContentUrl, "duration" => $message['duration']]);

                break;
            case 'sticker':
                $content = $lennaMessageType->text(['text' => "sent Line Sticker ID: " . $message['stickerId']]);
                break;
            case 'location':
                $content = $lennaMessageType->location(
                    [
                        "title" => $message['address'],
                        'latitude' => $message['latitude'],
                        'longitude' => $message['longitude']
                    ]
                );
                break;
            default:
                $content = $lennaMessageType->text(['text' => "Other message type"]);
                break;
        }
        return $content;
    }

    public function isIntegrationActive($request, $appId)
    {
        $isActive = Integration::where([
            ['app_id', $appId],
            ['status', true]
        ])->whereHas('channel', function ($q) {
            $q->where('name', 'line');
        })->get()->isNotEmpty();
        return $isActive;
    }

    public function requestLivechat($senderId, $appId)
    {
        $room = User::find($senderId)->rooms->first();
        $liveChat = $room->liveChats()->firstOrCreate(
            ['room_id' => $room->id, 'status' => 'request'],
            [
                "request_at" => Carbon::now()->toDateTimeString(),
                "status" => 'request',
            ]
        );
        broadcast(new RequestLive($room->id, $appId, $senderId, $liveChat->id));
    }

    public function deleteLiveChat($roomId)
    {
        Livechat::where('room_id', $roomId)->delete();
    }
}
