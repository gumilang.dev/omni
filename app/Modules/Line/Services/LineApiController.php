<?php

namespace App\Modules\Line\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\ExternalApi\Models\ExternalApi;
use GuzzleHttp\Client;
use App\Modules\Line\Helpers\LineMessageType;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use App\Modules\Common\Services\EncryptService;

class LineApiController extends Controller
{
    protected $client;
    protected $lineApi;

    public function __construct($data)
    {
        $this->data = $data;

        $this->lineApi = ExternalApi::where(['category' => 'channel', 'provider' => 'line'])->first();
        $guzzleProp = [
            "base_uri" => $this->lineApi->base_url
        ];
        $guzzlePropAPIData = [
          "base_uri" => str_replace("api", "api-data", $this->lineApi->base_url)
        ];
        $this->client = new Client($guzzleProp);
        $this->clientApiData = new Client($guzzlePropAPIData);
    }

    public function replyMessage($lastMessage, $newMessage, $quickbutton)
    {
        $apiProp = $this->lineApi->endpoints()->where('name', 'reply-message')->first();
        // $decryptedContent = EncryptService::decrypt($newMessage->content, env('ENCRYPT_MESSAGE_KEY'), 20);
        // $decryptedContent = json_decode($decryptedContent, true);
        foreach ($newMessage->content as $key => $content) {
            $message[] = $this->convertToLineMessageType($content, $quickbutton);
        }
        $requestData = [
            'replyToken' => $lastMessage->channel_data['reply_token'],
            'messages' => $message
        ];
        try {
          $response = $this->client->request(
            $apiProp->method,
            $apiProp->endpoint,
            [
                'headers' => ['Authorization' => 'Bearer ' . $this->data['integration']->integration_data['accessToken']],
                "json" => $requestData
            ]
          );
          $response = json_decode($response->getBody()->getContents());
        } catch (RequestException $e) {
          // ApiLog::create([
          //   'url'           => $this->lineApi->base_url.$apiProp->endpoint,
          //   'app_id'        => $this->data['appId'],
          //   'user_id'       => $data['userId'],
          //   'request'       => json_encode($formData),
          //   'response'      => $e->getMessage(),
          //   // 'response'      => $e->getResponse()->getBody(true),
          //   'ip_address'    => \Request::ip(),
          //   'user_agent'    => \Request::header('User-Agent'),
          //   'userable_type' => 'user',
          //   'userable_id'   => $data['userId'],
          // ]);
          \Log::error($e->getMessage());
        }
        return $response;
    }

    public function pushMessage($newMessage, $quickbutton)
    {
        $apiProp = $this->lineApi->endpoints()->where('name', 'push-message')->first();
        // $decryptedContent = EncryptService::decrypt($newMessage->content, env('ENCRYPT_MESSAGE_KEY'), 20);
        // $decryptedContent = json_decode($decryptedContent, true);
        foreach ($newMessage->content as $key => $content) {
            $message[] = $this->convertToLineMessageType($content, $quickbutton);
        }
        $requestData = [
          'to' => $this->data['chatRoom']->created_by->channel_user_id,
          'messages' => $message

        ];
        try {

            $response = $this->client->request(
                $apiProp->method,
                $apiProp->endpoint,
                [
                    'headers' => ['Authorization' => 'Bearer ' . $this->data['integration']->integration_data['accessToken']],
                    "json" => $requestData
                ]
            );
            $response = (object)[
                'data' => json_decode($response->getBody()->getContents()),
                'success' => true
            ];
        } catch (ClientException $exception) {
            $response = (object)[
                'data' => json_decode($exception->getResponse()->getBody()->getContents()),
                'success' => false
            ];
        }

        return $response;
    }

    public function getUserProfile($channelUserId)
    {
        $apiProp = $this->lineApi->endpoints()->where('name', 'get-profile')->first();
        $response = $this->client->request(
            $apiProp->method,
            str_replace('{user-id}', $channelUserId, $apiProp->endpoint),
      // $apiProp->endpoint.$lineData['source']['userId'],
            ['headers' => ['Authorization' => 'Bearer ' . $this->data['integration']->integration_data['accessToken']]]
        );
        $response = json_decode($response->getBody()->getContents());
        return $response;
    }

    public function getMessageContent($messageId)
    {
        $publicPath = env("PUBLIC_PATH");
        $apiProp = $this->lineApi->endpoints()->where('name', 'get-content')->first();
    // get file name and format
        $response = $this->clientApiData->request(
            $apiProp->method,
            str_replace('{message-id}', $messageId, $apiProp->endpoint),
            [
                'headers' => ['Authorization' => 'Bearer ' . $this->data['integration']->integration_data['accessToken']],
            ]
        );
    // set filename with format
        $response = explode("/", $response->getHeader("content-type")[0]);
        $category = $response[0];
        $format = str_replace("x-", "", $response[1]);

        $filename = $category . "-$messageId." . $format;
    // set path to upload
        $path = fopen($publicPath . '/upload/line/' . $filename, 'w');
    // save file to server
        $response = $this->clientApiData->request(
            $apiProp->method,
            str_replace('{message-id}', $messageId, $apiProp->endpoint),
            [
                'headers' => ['Authorization' => 'Bearer ' . $this->data['integration']->integration_data['accessToken']],
                'sink' => $path
            ]
        );
        return $filename;
    }

    public function convertToLineMessageType($message, $quickbutton)
    {
        $lineMessageType = new LineMessageType;
        // $message = $lineMessageType->{$message['type']}($message);
        $message = $lineMessageType->{$message->type}($message);
        if ($quickbutton == null) {
          return (object)$message;
        }

        $quickbutton = $lineMessageType->quickbutton($quickbutton);
        if (empty($quickbutton['quickReply']->items)) {
            return (object)$message;
        }
        return (object)array_merge($message, $quickbutton);
    }
}