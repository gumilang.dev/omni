<?php

namespace App\Modules\Group\Controllers;

use Hashids\Hashids;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Controllers\ApiController;
use App\Modules\Group\Models\Group;
use App\Modules\Group\Models\AgentGroup;
use App\Modules\User\Models\UserRole;
use App\Modules\Role\Models\Role;
use App\Modules\User\Models\UserPlatform;
use App\Modules\Livechat\Models\Livechat;
use App\Modules\Room\Models\Room;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Modules\Role\Helpers\RoleHelper;

use Illuminate\Support\Collection;

class GroupController extends ApiController
{
    public function __construct()
    {
        $this->hashids = new Hashids('', 6);
    }

    public function index(Request $request, $appId)
    {
        $sort = $request->get('sort');
        $filterText = $request->get('filterText');
        $per_page = $request->get('per_page');
        $paginated = $request->get("paginated") == "false" ? false  : true ;

        // return $user = UserRole::with(['role','userPlatform','userRoleChannel.channel'])->first();

        $groups = Group::with(['agentGroup'])->where("app_id", $appId)->orderBy('id', 'DESC');

        if ($sort) {
            $sort = explode('|', $sort);
            $user = $groups->orderBy($sort[0], $sort[1]);
        }

         //fix search / filter channel
         if ($filterText) {
            $groups = $groups->where(function ($query) use ($filterText) {
                $query->where('name', 'ILIKE', '%' . $filterText . '%')->orWhere('description', 'ILIKE', '%' . $filterText . '%');
            });
        }

        if (!$per_page) {
            $per_page = 15;
        }

        if($paginated) {
          return response()->json($groups->paginate($per_page));
        }else {
          $group = $groups->get();
          return $this->successResponse($group,"success");
        }



    }

    public function getAgent(Request $request, $appId)
    {
        // $users = UserPlatform::with(['role','apps'])->where('email', 'ILIKE', '%' .$request->email. '%');
        // $users = UserRole::with(['role','userPlatform'])->where('product_id', $appId)
        //                 ->where('email')
        //                 ->where('platform_id', env('OMNI_PLATFORM_ID'));
        $users = RoleHelper::agents($appId, false);
        // $users = UserRole::with(['role','userPlatform','userRoleChannel.channel'])
        //                 ->where('role_id', 4)
        //                 ->where('product_id', $appId)
        //                 ->where('platform_id', env('OMNI_PLATFORM_ID'))->get();
        // $dataUser = [];

        // foreach ($users as $key => $u) {
        //     unset($u->apps);
        // }
        // return $dataUser;
        return $users;
      }

      public function createGroup(Request $request, $appId)
      {
          $this->validate($request, [
              'group_name'        => 'required',
              'group_description' => 'required',
              'agent'             => 'required',
              ]);

            $checkGroupName = Group::where('name', $request->group_name)->first();
            if ($checkGroupName) {
                return $this->errorResponse(null, 'Group already exist');;
            }

            $groups = Group::create([
                'app_id' => $appId,
                'name' => $request->group_name,
                'description' => $request->group_description,
            ]);

            $dataAgentId = $dataGroupId = [];
            foreach ($request->agent as $key => $a) {
                 $agentGroup = AgentGroup::create([
                                    'group_id' => $groups->id,
                                    'user_id' => $a['id']
                                ]);
            }
            // return $dataAgent;
            // $agentGroup = AgentGroup::create([
            //     'group_id' => $groups->id,
            //     'user_id' =>
            // ]);

            return $this->successResponse($groups->id, "Group added");

      }

      public function updateGroup(Request $request, $appId)
      {
        $this->validate($request, [
            'group_name'        => 'required',
            'group_description' => 'required',
            // 'agent'             => 'required',
            ]);

        $group = Group::findOrFail($request->id);
        // $group->update([
        //     'name' => $request->group_name,
        //     'description' => $request->group_description,
        // ]);
        if ($request->agent == 'null') {
            // return 'null agent';
             $group->update([
                'name' => $request->group_name,
                'description' => $request->group_description,
            ]);
        } else {
            $group->update([
                'name' => $request->group_name,
                'description' => $request->group_description,
            ]);
            $dataAgentId = $dataGroupId = [];
            foreach ($request->agent as $key => $a) {
                 $agentGroup = AgentGroup::firstOrCreate([          //dont create if exist
                                    'group_id' => $group->id,
                                    'user_id' => $a['id']
                                ]);
            }
        }


        return $this->successResponse($group, "Group Edited");

      }

      public function updateGroupAgent(Request $request, $appId)
      {
        // return $request->remove_agents['id'];
        $agentGroup = AgentGroup::find($request->remove_agents['id']);
        if (!$agentGroup) {
            return $this->errorResponse(null, 'Not Found');
        }

        $agentGroup->delete();

        // return $agentGroup;
        return $this->successResponse(null, "Deleted");
      }

      public function deleteGroup(Request $request, $appId)
      {
        $group = Group::findOrFail($request->id);
        // $agentGrouop = AgentGroup::destroy($request->id);
        // $agentGrouop->delete();
        $group->delete();
        return $this->successResponse($group, "Deleted");
      }
}
