<?php

namespace App\Modules\Group\Models;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['name','description', 'app_id'];
    protected $hidden = ['created_at','updated_at'];
    // protected $connection = 'pgsql';

    // public function rooms() {
    //     return $this->belongsToMany('App\Modules\Room\Models\Room')->withPivot('id');
    // }

    public function agentGroup()
    {
        return $this->hasMany("App\Modules\Group\Models\AgentGroup", "group_id","id")->with(['userPlatform']);
        // return $this->hasMany("App\Modules\Group\Models\AgentGroup", "group_id","id")->with(['userRole','role']);
    }

    // public function userPlatform()
    // {
    //   return $this->hasOne('App\Modules\User\Models\UserPlatform', 'id', 'user_id');
    // }

    // public function role()
    // {
    //     return $this->belongsTo('App\Modules\Role\Models\Role');
    // }

}
