<?php

namespace App\Modules\Group\Models;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Role\Models\Role;


class AgentGroup extends Model
{
    protected $connection = 'pgsql';
    protected $fillable = ['group_id','user_id'];
    protected $hidden = ['created_at','updated_at'];

    // public function rooms() {
    //     return $this->belongsToMany('App\Modules\Room\Models\Room')->withPivot('id');
    // }

    public function group()
    {
        return $this->hasOne("App\Modules\Group\Models\Group");
    }

    public function userRole()
    {
        return $this->hasOne('App\Modules\User\Models\UserPlatform', 'id', 'user_id');
    }

    public function userPlatform()
    {
        return $this->hasOne('App\Modules\User\Models\UserPlatform', 'id', 'user_id');
    }

    public function role()
    {
        return $this->belongsTo('App\Modules\Role\Models\Role');
    }

    public function getRoleAttribute()
    {
        $role = Role::where('id', $this->attributes['role_id'])->first();
        return $role;
    }
}
