<?php namespace App\Modules\Group\Repositories\Impl;

use App\Shared\Repositories\AbstractRepositoryImpl;
use App\Modules\Group\Repositories\GroupRepository;
use App\Modules\Group\Models\Group;
use DB;

class GroupRepositoryImpl extends AbstractRepositoryImpl implements GroupRepository {

  public function __construct() {
    $this->model(new Group());
  }

  public function getByGroupId($groupId) {
    return $this->model::find($groupId);
  }

  public function getGroupByAgentId($appId,$agentId) {
    $groups = $this->model::where('app_id', $appId)->whereHas('agentGroup', function($q) use ($agentId) {
      $q->where('user_id', $agentId);
    })->get();
    return $groups;
  }
}
