<?php namespace App\Modules\Group\Repositories;

use App\Shared\Repositories\AbstractRepository;

interface GroupRepository extends AbstractRepository {
  public function getByGroupId($groupId);
  public function getGroupByAgentId($appId,$agentId);
}
