<?php namespace App\Modules\CustomFilter;

use App\Repositories\BaseRepository as Repository;
use App\Modules\CustomFilter\CustomFilter;

class CustomFilterRepo extends Repository {
  public function __construct(
    CustomFilter $entity
  ) {
    $this->entity = $entity;
    $this->rules = [
      "app_id" => "required",
      "schema_query" => "required",
      "filter_name" => "required"
    ];
  }
}
