<?php namespace App\Modules\CustomFilter\Controllers;

use App\Http\Controllers\ApiController;
use App\Modules\CustomFilter\CustomFilterService;
use Illuminate\Http\Request;

class CustomFilterController extends ApiController {

  public $service;

  public function __construct(
    CustomFilterService $service
  ) {
    $this->service = $service;
  }

  public function create(Request $request,$appId) {
		$request->merge(["app_id" => $appId]);
    $response = $this->service->createCustomFilter($request);
    if($response["error"]) {
      return $this->errorResponse([
        "messages" => $response['message']
      ],"FAILED_CREATE_CUSTOM_FILTER");
    }
    return $this->successResponse($response,"CREATE_CUSTOM_FILTER_SUCCESS");

  }

  public function get(Request $request,$appId,$userId) {
    $request->merge(["app_id" => $appId,"user_id" => $userId]);
    $response = $this->service->getCustomFilter($request);
    return response()->json($response);
  }

  public function destroy(Request $request,$appId) {
    $custom_filter_id = $request->id;
    try {
      $response = $this->service->deleteCustomFilter($custom_filter_id);
      return $this->successResponse($response,"DELETE_CUSTOM_FILTER_SUCCESS");
    }catch(Error $e) {
      return $this->errorResponse($e->getTrace(),"DELETE_CUSTOM_FILTER_FAILED");
    }
  }

  public function update(Request $request,$appId) {
    try {
      $response = $this->service->updateCustomFilter($request);
      return $this->successResponse($response,"UPDATE_CUSTOM_FILTER_SUCCESS");
    }catch(Error $e) {
      return $this->successResponse($e->getTrace(),"UPDATE_CUSTOM_FILTER_FAILED");
    }
  }
}
