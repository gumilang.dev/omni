<?php namespace App\Modules\CustomFilter;

use App\Modules\CustomFilter\CustomFilterRepo;
use App\Modules\CustomFilter\CustomFilter;

class CustomFilterService {
  private $repo;

  public function __construct(CustomFilterRepo $repo) {
    $this->repo = $repo;
  }

  public function createCustomFilter($request) {
    $data =  (new CustomFilter)
                ->setAppId($request->post("app_id"))
                ->setFilterName($request->post("filter_name"))
                ->setSchemaQuery($request->post("schema_query"))
                ->setUserId($request->post("user_id"))
                ->build();
    return $this->repo->store($data);
  }

  public function getCustomFilter($request) {
    return $this->repo->paginate($request,["app_id"=> $request->app_id,"user_id" => $request->user_id],["filter_name"]);
  }

  public function deleteCustomFilter($custom_filter_id) {
    return $this->repo->destroy(["id" => $custom_filter_id]);
  }

  public function updateCustomFilter($request) {
    $custom_filter = [
                    "schema_query" => $request->post("schema_query"),
                    "filter_name" => $request->post("filter_name")
                  ];
    $condition = ["id" => $request->post("id")];
    return $this->repo->update($condition,$custom_filter);
  }
}
