<?php namespace App\Modules\CustomFilter;

use Illuminate\Database\Eloquent\Model;
class CustomFilter extends Model {

  private $filter_name;
  private $schema_query;
  private $app_id;
  private $user_id;

  protected $table = "custom_filter";
  protected $cast = [
    "schema_query" => "array"
  ];
  protected $hidden = [
    "created_at", "updated_at", "app_id"
  ];
  protected $fillable = ["filter_name","schema_query","app_id","user_id"];

  public function getFilterName() {
    return $this->filter_name;
  }

  public function setFilterName($filter_name) {
    $this->filter_name = $filter_name;
    return $this;
  }

  public function getAppId() {
    return $this->filter_name;
  }


  public function setAppId($appId) {
    $this->app_id = $appId;
    return $this;
  }

  public function setUserId($user_id) {
    $this->user_id = $user_id;
    return $this;
  }

  public function getSchemaQuery() {
    return $this->schema_query;
  }

  public function setSchemaQuery($schema_query) {
    $this->schema_query = $schema_query;
    return $this;
  }

  public function build() {
    return [
      "app_id" => $this->app_id,
      "filter_name" => $this->filter_name,
      "schema_query" => $this->schema_query,
      "user_id" => $this->user_id
    ];
  }

}
