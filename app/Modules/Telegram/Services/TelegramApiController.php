<?php

namespace App\Modules\Telegram\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\ExternalApi\Models\ExternalApi;
use GuzzleHttp\Client;
use App\Modules\Telegram\Helpers\TelegramMessageType;
use App\Modules\Common\Services\EncryptService;

class TelegramApiController extends Controller
{
    protected $client;
    protected $telegramApi;

    public function __construct($data = null)
    {
        $this->data = $data;

        $this->telegramApi = ExternalApi::where(['category' => 'channel', 'provider' => 'telegram'])->first();
        $guzzleProp = [
            'base_uri' => str_replace('{params}', "bot{$data['botToken']}/", $this->telegramApi->base_url)
        ];
        $this->client = new Client($guzzleProp);
    }

    public function sendMessage($newMessage)
    {
        $apiProp = $this->telegramApi->endpoints()->where('name', 'send-message')->first();
        $initData = [
            'chat_id' => $this->data['chatRoom']->created_by->channel_user_id,
        ];
        // $newMessage->content = clone $newMessage;
        // dd($newMessage->content);
        // $decryptedContent = EncryptService::decrypt($message->content, env('ENCRYPT_MESSAGE_KEY'), 20);
        // $decryptedContent = json_decode($decryptedContent, true);
        foreach ($newMessage->content as $key => $content) {
            $message = $this->convertToTelegramMessageType($content);
            if ($content->type == 'carousel') {
                foreach ($message as $key => $singleMsg) {
                    $requestData = array_merge($initData, $singleMsg['content']);

                    $response = $this->client->request(
                        $apiProp->method,
                        str_replace("{message-type}", $singleMsg['type'], $apiProp->endpoint),
                        [
                            "json" => $requestData
                        ]
                    );
                    $response = json_decode($response->getBody()->getContents());
                }
            } else {
                $requestData = array_merge($initData, $message['content']);

                $response = $this->client->request(
                    $apiProp->method,
                    str_replace("{message-type}", $message['type'], $apiProp->endpoint),
                    [
                        "json" => $requestData
                    ]
                );
                $response = json_decode($response->getBody()->getContents());
            }


        }
        return $response;
    }

    public function getUserProfile($chatId)
    {
        $apiProp = $this->telegramApi->endpoints()->where('name', 'get-user-profile')->first();
        $requestData = [
            'chat_id' => $chatId
        ];
        $response = $this->client->request(
            $apiProp->method,
            $apiProp->endpoint,
            ['json' => $requestData]
        );
        $response = json_decode($response->getBody()->getContents());
        return $response->result;
    }

    public function setBotWebhook($botToken, $webhookUrl)
    {
        $apiProp = $this->telegramApi->endpoints()->where('name', 'set-webhook')->first();
        $requestData = [
            'url' => $webhookUrl,
        ];
        $response = $this->client->request(
            $apiProp->method,
            $apiProp->endpoint,
            ['json' => $requestData]
        );
        $response = json_decode($response->getBody()->getContents());

        return $response;
    }

    public function deleteWebhook($webhookUrl)
    {
      $apiProp = $this->telegramApi->endpoints()->where('name', 'delete-webhook')->first();
      $response = $this->client->request(
        $apiProp->method,
        $apiProp->endpoint
      );
     return $response;

    }

    public function getFile($data = [])
    {
        $apiProp = $this->telegramApi->endpoints()->where('name', 'get-file')->first();
    // get file name and format
        $response = $this->client->request(
            $apiProp->method,
            $apiProp->endpoint,
            [
                'json' => ['file_id' => $data['fileId']]
            ]
        );

        $response = json_decode($response->getBody()->getContents())->result;

        $publicPath = env("PUBLIC_PATH");
        $format = explode(".", $response->file_path)[1];
        $date = date("dmY");

        $filename = (isset($data['userId'])) ? "profilepic-{$data['userId']}." . $format : "{$data['fileId']}-$date." . $format;

    // set path to upload
        $path = fopen($publicPath . '/upload/telegram/' . $filename, 'w');
    // save file to server
        $apiProp = $this->telegramApi->endpoints()->where('name', 'download-file')->first();
        $response = $this->client->request(
            $apiProp->method,
            str_replace("{params}", "file/bot{$this->data['botToken']}/{$response->file_path}", $this->telegramApi->base_url),
            [
                'sink' => $path
            ]
        );
        $fileUrl = env("APP_URL") . "/upload/telegram/$filename";
        return $fileUrl;
    }
    public function getUserProfilePic($fileId, $userId)
    {
        $apiProp = $this->telegramApi->endpoints()->where('name', 'get-file')->first();
    // get file name and format
        $response = $this->client->request(
            $apiProp->method,
            $apiProp->endpoint,
            [
                'json' => ['file_id' => $fileId]
            ]
        );

        $response = json_decode($response->getBody()->getContents());

        $publicPath = env("PUBLIC_PATH");
        $format = 'jpg';
        $filename = "profilepic" . "-$userId." . $format;
    // set path to upload
        $path = fopen($publicPath . '/upload/telegram/' . $filename, 'w');
    // save file to server
        $apiProp = $this->telegramApi->endpoints()->where('name', 'download-file')->first();

        $response = $this->client->request(
            $apiProp->method,
            str_replace("{params}", "file/bot{$this->data['botToken']}/{$response->result->file_path}", $this->telegramApi->base_url),
            [
                'sink' => $path
            ]
        );
        $pictureUrl = env("APP_URL") . "/upload/telegram/$filename";
        return $pictureUrl;
    }

    public function convertToTelegramMessageType($message)
    {
        // dd($message['type']);
        $telegramMessageType = new TelegramMessageType;
        switch ($message->type) {
            case 'text':
                $message = $telegramMessageType->text(['text' => $message->text]);
                break;
            case 'image':
                $message = $telegramMessageType->image(['previewImageUrl' => $message->previewImageUrl]);
                break;
            case 'file':
                $message = $telegramMessageType->file([
                    'fileUrl' => $message->fileUrl
                ]);
                break;
            case 'audio':
                $message = $telegramMessageType->audio(['originalContentUrl' => $message->originalContentUrl]);
                break;
            case 'video':
                $message = $telegramMessageType->video(['originalContentUrl' => $message->originalContentUrl]);
                break;
            case 'location':
                $message = $telegramMessageType->location(['latitude' => $message->latitude, 'longitude' => $message->longitude]);
                break;
            case 'carousel':
                $message = $telegramMessageType->carousel([
                    'columns' => $message->columns,
                    'imageAspectRatio' => $message->imageAspectRatio,
                    'imageSize' => $message->imageSize,
                ]);
                break;
            case 'confirm':
                $message = $telegramMessageType->confirm([
                    'actions' => $message->actions,
                    'text' => $message->text,
                ]);
            case 'template':
                $message = $telegramMessageType->template([
                    'template' => $message->template
                ]);
                break;
            case 'summary':
                $message = $telegramMessageType->summary([
                  'title' => $message->title,
                  'columns' => $message->columns,
                  'imageUrl' => $message->imageUrl,
                ]);
                break;
            default:
                $message = $telegramMessageType->text(['text' => "Sent ".$message->type." message"]);
                break;
        }
        return $message;
    }

}
