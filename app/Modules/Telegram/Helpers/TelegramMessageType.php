<?php

namespace App\Modules\Telegram\Helpers;

class TelegramMessageType
{


    public function __construct()
    {

    }

    public function text($input = [])
    {
        $output = [
            'type' => "Message",
            'content' => [
                'text' => $input['text'] ?? '',
            ]
        ];

        return $output;

    }

    public function image($input = [])
    {
        $output = [
            'type' => "Photo",
            'content' => [
                'photo' => $input['originalContentUrl'] ?? ($input['previewImageUrl'] ?? '')
            ]
        ];

        return $output;
    }

    public function file($input = [])
    {
        $output = [
            'type' => "Document",
            'content' => [
                'document' => $input['fileUrl'] ?? ''
            ]
        ];

        return $output;
    }

    public function video($input = [])
    {
        $output = [
            'type' => 'Video',
            'content' => [
                'video' => $input['originalContentUrl'] ?? ''
            ]
        ];

        return $output;
    }
    public function location($input = [])
    {
        $output = [
            'type' => 'Location',
            'content' => [
                'latitude' => $input['latitude'] ?? '',
                'longitude' => $input['longitude'] ?? ''
            ]
        ];

        return $output;
    }
    public function audio($input = [])
    {
        $output = [
            'type' => 'audio',
            'content' => [
                'audio' => $input['originalContentUrl'] ?? '',
                'caption' => 'caption'
            ]
        ];

        return $output;
    }

    public function confirm($input = [])
    {
        $output = [
            'type' => 'Message',
            'content' => [
                'text' => $input['text'] ?? '',
                'reply_markup' => (object)[
                    'inline_keyboard' => [
                        [
                            (object)[
                                // 'type' => 'message',
                                'text' => $input['actions'][0]->text ?? 'Ya',
                                'callback_data' => $input['actions'][0]->action ?? 'ya'
                            ],
                            (object)[
                                // 'type' => 'message',
                                'text' => $input['actions'][1]->text ?? 'Tidak',
                                'callback_data' => $input['actions'][1]->action ?? 'tidak'
                            ]
                        ]
                    ],
                    "resize_keyboard" => true,
                    "one_time_keyboard" => true
                ],
            ]

        ];

        return $output;


    }


    public function carousel($input = [])
    {



        if (!isset($input['columns']))
            return $output;
        foreach ($input['columns'] as $colKey => $col) {
            $output[] = [
                'type' => 'Photo',
                'content' => [
                    'photo' => $col->thumbnailImageUrl,
                    'caption' => $col->title ?? $col->text,
                    'reply_markup' => (object)[
                        // "resize_keyboard" => true,
                        "one_time_keyboard" => true
                    ],
                ]

            ];
            if (count($col->actions) != 0) {
                foreach ($col->actions as $actKey => $act) {
                    if ($act->type == 'uri') {
                        $output[$colKey]['content']['reply_markup']->inline_keyboard[$actKey][] = (object)[
                            'text' => $act->label ?? '',
                            'url' => $act->data ?? ''
                        ];
                    } else {
                        $output[$colKey]['content']['reply_markup']->inline_keyboard[$actKey][] = (object)[
                            'text' => $act->label ?? '',
                            'callback_data' => $act->data ?? $act->label ?? ''
                        ];
                    }
                }
            }
        }
        return $output;

        // // $maxColumn = 1;
        // // $row = floor(count($input['columns']) / $maxColumn);
        // // $columnIndex = 0;

        // // for ($i = 0; $i <= $row; $i++) {
        // //     for ($j = 1; $j <= $maxColumn; $j++) {
        // //         if ($columnIndex == count($input['columns']))
        // //             break;
        // //         $output['content']['reply_markup']->inline_keyboard[$i][] = (object)[
        // //             'text' => $input['columns'][$columnIndex]->text ?? '',
        // //             'callback_data' => $input['columns'][$columnIndex]->text ?? '',
        // //         ];
        // //         $columnIndex++;
        // //     }
        // // }

        // return $output;


    }
    public function confirm_keyboard($input = [])
    {
        $output = [
            'type' => 'Message',
            'content' => [
                'text' => $input['text'] ?? '',
                'reply_markup' => (object)[
                    'keyboard' => [
                        [
                            (object)[
                                'type' => 'message',
                                'label' => $input['actions'][0]->label ?? 'Ya',
                                'text' => $input['actions'][0]->action ?? 'ya'
                            ],
                            (object)[
                                'type' => 'message',
                                'label' => $input['actions'][1]->label ?? 'Tidak',
                                'text' => $input['actions'][1]->action ?? 'tidak'
                            ]
                        ]
                    ],
                    "resize_keyboard" => true,
                    "one_time_keyboard" => true
                ],
            ]

        ];

        return $output;


    }


    public function carousel_keyboard($input = [])
    {
        $output = [
            'type' => 'Message',
            'content' => [
                'text' => $input['text'] ?? 'Carousel',
                'reply_markup' => (object)[
                    // "resize_keyboard" => true,
                    "one_time_keyboard" => true
                ],
            ]

        ];


        if (!isset($input['columns']))
            return $output;

        $row = floor(count($input['columns']) / 4);
        $columnIndex = 0;

        for ($i = 0; $i <= $row; $i++) {
            for ($j = 1; $j <= 4; $j++) {
                if ($columnIndex == count($input['columns']))
                    break;
                $output['content']['reply_markup']->keyboard[$i][] = (object)[
                    'text' => $input['columns'][$columnIndex]->text ?? '',
                    'callback_data' => $input['columns'][$columnIndex]->text ?? '',
                ];
                $columnIndex++;
            }
        }

        return $output;


    }

    public function template($input = [])
    {
      $template = $input['template'] ?? 'Template';
      $template = str_replace('<br>', "\n", $template);
      $template = str_replace('</br>', "", $template);
      $template = str_replace('<p>', "\n", $template);
      $template = str_replace('</p>', "", $template);
      $template = strip_tags($template, '<b><strong><em><i><u><s><strike><del><pre><code>');
      $output = [
        'type' => 'Message',
        'content' => [
          'text' => $template,
          'parse_mode' => 'HTML'
        ]
      ];
      return $output;
    }

    public function summary($input = [])
    {
        $output = "{$input['title']} \n\n" ?? "Summary \n\n";
        foreach ($input['columns'] as $data => $val) {
          $output .= "<b>{$val->field}</b>: {$val->value}\n";
        }
        return [
            'type' => 'Message',
            'content' => [
              'text' => $output,
              'parse_mode' => 'HTML'
            ]
        ];
    }
}
