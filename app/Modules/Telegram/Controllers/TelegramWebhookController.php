<?php

namespace App\Modules\Telegram\Controllers;

use Hashids\Hashids;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Modules\Log\Models\WebhookLog;
use App\Modules\Integration\Models\Integration;
use App\Modules\App\Models\App;
use App\Modules\User\Models\User;
use App\Modules\Room\Models\Room;
use App\Modules\Message\Models\Message;
use App\Modules\Channel\Models\Channel;
use App\Modules\Livechat\Models\Livechat;

use App\Events\NewMessage;
use App\Events\NewUser;
use App\Events\RequestLive;

use App\Modules\Telegram\Services\TelegramApiController;
use App\Modules\Bot\Services\BotService;
use App\Modules\Mobile\Services\BackendService;

use App\Modules\Mobile\Helpers\LennaMessageType;
use App\Modules\Chat\Helpers\ChatHelper;
use App\Modules\Log\Helpers\WebhookHelper;
use App\Modules\BlockUser\Services\BlockUserService;

use App\Http\Controllers\ApiController;

class TelegramWebhookController extends ApiController
{
    protected $app;
    protected $integration;
    protected $telegramController;
    protected $channel;
    protected $blockUserService;

    public function __construct(
      BlockUserService $blockUserService
    )
    {
      $this->channel = Channel::where('name', 'telegram')->first();
      $this->blockUserService = $blockUserService;
    }
    public function webhookTest(Request $request)
    {
        $webhookLog = WebhookLog::create([
            "source" => "telegram",
            "app_id" => "333",
            "events" => "telegram webhook test",
            "payload" => json_encode($request->all()),
            "response" => json_encode([
                'success' => true,
                'data' => "testing webhook"
            ])
        ]);

        return $this->successResponse(null, 'testing webhook');
    }
    public function webhook(Request $request, $appId)
    {
        if (!$this->integration = WebhookHelper::getIntegrationData('telegram', $appId))
            return $this->errorResponse(null, 'integration not active');

        $this->app = App::find($appId);
        $this->telegramController = new TelegramApiController(['botToken' => $this->integration->integration_data['botToken']]);
        $eventsType = $this->getEvents($request);
        $botService = new BotService(['appId' => $appId]);
        $backendService = new BackendService(['appId' => $appId]);
        switch ($eventsType) {
            case 'incoming_message':
                $response = $this->incomingMessage($request, $this->app->id);
                // check if user is blocked
                if(is_bool($response) && $response == true) {
                  return $this->successResponse(["data" => $response],"user blocked");
                }
                // if status resolved
                if (WebhookHelper::isResolved(['roomId' => $response['room']->id])) {
                  // change status to bot by deleting livechat key
                  $this->deleteLiveChat($response['room']->id);
                }
                broadcast(new NewMessage($response['message'], $response['room'], $this->app));
                if (!WebhookHelper::isLive(['roomId' => $response['room']->id])) {
                  if (!WebhookHelper::isRequest(['roomId' => $response['room']->id])) {
                    // $backendService = new BackendService(['appId' => $appId]);
                    $backendResponse = $backendService->getUserToken([
                        "email" => $response['user']->email,
                        "password" => BackendService::encryptText(env("ENCRYPT_KEY"), "$appId:" . $response['user']->email),
                        "fcm_token" => null,
                        "client" => "telegram",
                        "userId" => $response['user']->id
                    ]);
                    // $botService = new BotService(['appId' => $appId]);
                    $botResponse = $botService->replyMessage([
                      'userId' => $response['user']->id,
                      'content' => $response['message']->content[0],
                      'room' => $response['room'],
                      'channel' => 'telegram',
                      'headers' => ["Authorization" => "$backendResponse->token_type $backendResponse->access_token"],
                      'lang' => $response['user']->lang
                    ]);
                  }
                }
                break;
            case 'callback_message':
                $response = $this->callbackMessage($request, $this->app->id);
                // check if user is blocked
                if(is_bool($response) && $response == true) {
                  return $this->successResponse(["data" => $response],"user blocked");
                }
                // if status resolved
                if (!WebhookHelper::isResolved(['roomId' => $response['room']->id])) {
                  // change status to bot by deleting livechat key
                  $this->deleteLiveChat($response['room']->id);
                }
                broadcast(new NewMessage($response['message'], $response['room'], $this->app));
                if (!WebhookHelper::isLive(['roomId' => $response['room']->id])) {
                  if (!WebhookHelper::isRequest(['roomId' => $response['room']->id])) {
                    // $backendService = new BackendService(['appId' => $appId]);
                    $backendResponse = $backendService->getUserToken([
                        "email" => $response['user']->email,
                        "password" => BackendService::encryptText(env("ENCRYPT_KEY"), "$appId:" . $response['user']->email),
                        "fcm_token" => null,
                        "client" => "telegram",
                        "userId" => $response['user']->id
                    ]);
                    // $botService = new BotService(['appId' => $appId]);
                    $botResponse = $botService->replyMessage([
                        'userId' => $response['user']->id,
                        'content' => $response['message']->content[0],
                        'room' => $response['room'],
                        'channel' => 'telegram',
                        'headers' => ["Authorization" => "$backendResponse->token_type $backendResponse->access_token"],
                        'lang' => $response['user']->lang
                    ]);
                  }
                }
                break;
        }

        // if bot not integrated change status to request
        $botIntegration = $botService->getIntegration();
        if (!$botIntegration) {
          if (!WebhookHelper::isLive(['roomId' => $response['room']->id])) {
            if (!WebhookHelper::isRequest(['roomId' => $response['room']->id])) {
              WebhookHelper::requestLivechat($response['user']->id, $appId);
            }
          }
        }

        // $last_update = $response['user']->updated_at;
        // $diff = $last_update->diffInDays(Carbon::now('Asia/Jakarta'));
        // if ($diff > 1) {
        //   $backendService->updateLocationAndIp([
        //     'ip' => $request->ipinfo->ip,
        //     'city' => $request->ipinfo->city,
        //     'location' => $request->ipinfo->loc,
        //     'user_id' => $response['user']->id,
        //     'last_update' => $response['user']->updated_at
        //   ]);
        // }

        $responseLog = (object)[
            'output' => $botResponse['nlp']->response ?? null,
            'quickbutton' => $botResponse['nlp']->quickbutton ?? [],
            'type' => $botResponse['nlp']->type ?? null
        ];
        $webhookLog = WebhookLog::create([
            "source" => "telegram",
            "app_id" => $this->app->id,
            "events" => $eventsType,
            "payload" => json_encode($request->all()),
            "response" => json_encode([
                'success' => true,
                'data' => $responseLog
            ]),
            "user_id" => $response['user']->id,
            "bot_id" => $botIntegration ? $botIntegration->integration_data['botId'] : NULL
        ]);

        return $this->successResponse([
            'events' => $eventsType,
            'result' => (object)[
                'output' => $botResponse['nlp']->response ?? null,
                'quickbutton' => $botResponse['nlp']->quickbutton ?? [],

            ]
        ]);

    }

    public function registerUser($message, $appId)
    {

        $responseProfile = $this->telegramController->getUserProfile($message['from']['id']);

        if (isset($responseProfile->photo)) {
            $pictureUrl = $this->telegramController->getFile(["fileId" => $responseProfile->photo->big_file_id, "userId" => $responseProfile->id]);
        } else {
            $pictureUrl = env("APP_URL") . "/images/pictures/no_avatar.jpg";
        }
        $plainName = strtolower(preg_replace('/\s+/', '', $responseProfile->first_name));
        $fullName = (isset($responseProfile->last_name)) ? $responseProfile->first_name . " " . $responseProfile->last_name : $responseProfile->first_name;
        $email = strtolower(preg_replace('/\s+/', '', $fullName . $responseProfile->id . "@telegram.com"));
        // $encryptedEmail = BackendService::encryptText(env("ENCRYPT_PROFILE_KEY"), $email);

        $fields = [
            'app_id' => $appId,
            'channel_user_id' => $responseProfile->id,
            'name' => $fullName,
            'nickname' => $responseProfile->first_name,
            'password' => bcrypt("$appId:$email"),
            'picture' => $pictureUrl,
            'email' => $email
        ];
        $user = $this->channel->users()->firstOrCreate(
            ['channel_user_id' => $responseProfile->id, 'app_id' => $appId],
            $fields
        );
        return $user;
    }

    public function incomingMessage($telegramData, $appId)
    {

      $user = User::where([['channel_user_id', "=", $telegramData->message['from']['id']], ['app_id', "=", $appId]])->first();
  // register user if not registered
      if (!$user) {
          $user = $this->registerUser($telegramData->message, $appId);
      }

       if($this->blockUserService->isBlocked($user->email, $appId)) {
          return true;
       }

        $chatRoom = ChatHelper::createRoomAndParticipate(['channel' => $this->channel, 'user' => $user, 'appId' => $appId]);

        $channelData = [
            'update_id' => $telegramData->update_id,
            'message_id' => $telegramData->message['message_id'],
            'timestamp' => $telegramData->message['date'],
        ];
        $content[] = $this->setMessageType($telegramData['message']);
        $message = ChatHelper::createMessage(['channelData' => $channelData, 'content' => $content, 'user' => $user, 'room' => $chatRoom]);

        return [
            'message' => $message,
            'user' => $user,
            'room' => $chatRoom
        ];


    }

    public function callbackMessage($telegramData, $appId)
    {

        $user = User::where([['channel_user_id', "=", $telegramData->callback_query['from']['id']], ['app_id', "=", $appId]])->first();
    // register user if not registered
        if (!$user) {
            $user = $this->registerUser($telegramData->callback_query, $appId);
        }

        if($this->blockUserService->isBlocked($user->email, $appId)) {
          return true;
       }

        $chatRoom = ChatHelper::createRoomAndParticipate(['channel' => $this->channel, 'user' => $user, 'appId' => $appId]);

        $channelData = [
            'update_id' => $telegramData->update_id,
            'message_id' => $telegramData->message['message_id'],
            'timestamp' => $telegramData->message['date'],
        ];
        $content[] = $this->setMessageType($telegramData->callback_query['data']);
        $message = ChatHelper::createMessage(['channelData' => $channelData, 'content' => $content, 'user' => $user, 'room' => $chatRoom]);

        return [
            'message' => $message,
            'user' => $user,
            'room' => $chatRoom
        ];


    }

    public function setMessageType($message)
    {
        $lennaMessageType = new LennaMessageType;
        $messageType = $this->checkMessageType($message);

        switch ($messageType) {
            case 'text':
                $content = $lennaMessageType->text(['text' => $message['text']]);
                break;
            case 'image':

                $fileUrl = $this->telegramController->getFile(["fileId" => $message['photo'][1]['file_id']]);

                $content = $lennaMessageType->image(['previewImageUrl' => $fileUrl]);

                break;
            case 'file':

                $fileUrl = $this->telegramController->getFile(["fileId" => $message['document']['file_id']]);
                $content = $lennaMessageType->file(['fileUrl' => $fileUrl, 'fileName' => $message['document']['file_name']]);

                break;
            case 'video':

                $fileUrl = $this->telegramController->getFile(["fileId" => $message['video']['file_id']]);

                $content = $lennaMessageType->video(['originalContentUrl' => $fileUrl, "duration" => $message['video']['duration']]);

                break;
            case 'audio':

                $fileUrl = $this->telegramController->getFile(["fileId" => $message['voice']['file_id']]);

                $content = $lennaMessageType->audio(['originalContentUrl' => $fileUrl, "duration" => $message['voice']['duration']]);

                break;
            case 'sticker':
                $fileUrl = $this->telegramController->getFile(["fileId" => $message['sticker']['thumb']['file_id']]);

                $content = $lennaMessageType->image(['previewImageUrl' => $fileUrl]);

                break;
            case 'location':
                $content = $lennaMessageType->location(
                    [
                        "title" => "Location",
                        'latitude' => $message['location']['latitude'],
                        'longitude' => $message['location']['longitude']
                    ]
                );
                break;
            case 'callback':

                $content = $lennaMessageType->text(['text' => $message]);
                break;
        }
        return $content;
    }

    public function getEvents($payload)
    {
        if (Arr::has($payload, 'message')) {
            return 'incoming_message';
        } elseif (Arr::has($payload, 'callback_query')) {
            return 'callback_message';
        } else {
            return 'undefined_type';
        }

    }
    public function checkMessageType($messageData)
    {
        if (is_string($messageData)) {
            return 'callback';
        } elseif (Arr::has($messageData, 'text')) {
            return 'text';
        }
    // incoming message from user
        elseif (Arr::has($messageData, 'photo')) {
            return 'image';
        } elseif (Arr::has($messageData, 'video')) {
            return 'video';
        } elseif (Arr::has($messageData, 'location')) {
            return 'location';
        } elseif (Arr::has($messageData, 'voice')) {
            return 'audio';
        } elseif (Arr::has($messageData, 'sticker')) {
            return 'sticker';
        } elseif (Arr::has($messageData, 'document')) {
            return 'file';
        } else {
            return 'undefined';
        }

    }

    public function requestLivechat($senderId, $appId)
    {

    }

    public function deleteLiveChat($roomId)
    {
        Livechat::where('room_id', $roomId)->delete();
    }
}
