<?php

namespace App\Modules\Ppob\Models;

use Illuminate\Database\Eloquent\Model;

class PpobProduct extends Model
{
    protected $connection = 'backend';

    protected $guarded = ['id'];
    protected $with = ['category', 'operator', 'type'];

    public function type()
    {
        return $this->belongsTo(PpobProductType::class, 'product_type_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(PpobProductCategory::class, 'product_category_id', 'id');
    }

    public function operator()
    {
        return $this->belongsTo(PpobProductOperator::class, 'product_operator_id', 'id');
    }

    public function getFinalPriceAttribute()
    {
        return $this->price + $this->fee;
    }
}
