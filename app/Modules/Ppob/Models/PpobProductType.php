<?php

namespace App\Modules\Ppob\Models;

use Illuminate\Database\Eloquent\Model;

class PpobProductType extends Model
{
    public function products()
    {
        return $this->hasMany("App\Modules\Ppob\Models\PpobProduct", 'product_type_id', 'id');
    }
}
