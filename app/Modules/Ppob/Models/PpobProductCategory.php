<?php

namespace App\Modules\Ppob\Models;

use Illuminate\Database\Eloquent\Model;

class PpobProductCategory extends Model
{
    public function products()
    {
        return $this->hasMany("App\Modules\Ppob\Models\PpobProduct", 'product_category_id', 'id');
    }
}
