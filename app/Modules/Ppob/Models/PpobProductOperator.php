<?php

namespace App\Modules\Ppob\Models;

use Illuminate\Database\Eloquent\Model;

class PpobProductOperator extends Model
{
    public function products()
    {
        return $this->hasMany("App\Modules\Ppob\Models\PpobProduct", 'product_operator_id', 'id');
    }
}
