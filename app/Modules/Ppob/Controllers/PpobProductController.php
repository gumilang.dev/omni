<?php

namespace App\Modules\Ppob\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Modules\Ppob\Models\PpobProduct;
use App\Modules\ExternalApi\Models\ExternalApi;
use GuzzleHttp\Client;
use Hashids\Hashids;
use App\Http\Controllers\ApiController;

class PpobProductController extends ApiController
{

    public function index(Request $request)
    {
        $sort = $request->get('sort');
        $per_page = $request->get('per_page');
        $filterText = $request->get('filterText');
        // $startDate = $request->get('startDate');
        // $endDate = $request->get('endDate');

        $ppobProduct = new PpobProduct;
        if ($sort) {
            $sort = explode('|', $sort);
            $ppobProduct = $ppobProduct->orderBy($sort[0], $sort[1]);
        }

        if ($filterText) {
            $ppobProduct = $ppobProduct->where(function ($query) use ($filterText) {
                $query->where('name', 'ILIKE', '%' . $filterText . '%')
                    ->orWhere('nominal', 'ILIKE', '%' . $filterText . '%');
            });
        }

        // if ($startDate && $endDate) {
        //     // return $startDate;
        //     $ppobProduct = $ppobProduct->whereBetween('created_at', [$startDate, $endDate])->orWhere('created_at', '>=', $startDate);
        // }

        if (!$per_page) {
            $per_page = 15;
        }

    //   $ppobProduct = $ppobProduct->with(['ppobProduct'])->where('app_id',$appId);
        $ppobProduct = $ppobProduct->orderBy("id", 'desc');

        return response()->json($ppobProduct->paginate($per_page));
    }





    public function store(Request $request)
    {

    }


    public function show($id)
    {
        return $id;
    }


    public function edit($id)
    {
        return $id;
    }


    public function update(Request $request, $appId, $ppobProductId)
    {
        $data = [
            'fee' => $request->fee,
            'name' => $request->name,
            'description' => $request->description,
            'is_active' => $request->is_active,

        ];

        $ppobProduct = PpobProduct::where('id', $ppobProductId)->update($data);

        return $this->successResponse([
            'ppob_product' => $ppobProduct,
        ], 'ppob product updated');

    }


    public function destroy($ppobProduct, $id)
    {
        $ppobProduct = PpobProduct::findOrFail($id);
        $ppobProduct->delete();
    }

}
