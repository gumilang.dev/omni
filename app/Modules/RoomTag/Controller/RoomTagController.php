<?php namespace App\Modules\RoomTag\Controller;

use App\Modules\RoomTag\Model\RoomTag;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

class RoomTagController extends ApiController
{
    private $roomTag;
    public function __construct(RoomTag $roomTag) {
        $this->roomTag = $roomTag;
    }
    /**

     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$appId)
    {
        $this->validate($request, [
            'room_id' => 'required|integer',
            'tag_id' => 'required_|integer'
        ]);
        $room_tags = $this->roomTag::create([
            'room_id' => $request->input('room_id'),
            'tag_id' => $request->input('tag_id')
        ]);
        return $this->successResponse($room_tags,'RoomTag created');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RoomTag  $roomTag
     * @return \Illuminate\Http\Response
     */
    public function destroy($appId,$id)
    {
        $data = $this->roomTag::where('id',$id)->delete();
        return $this->successResponse(['deleted_id' => $id],'Tag Deleted');
    }
}
