<?php

namespace App\Modules\ClaimTripa\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use App\Modules\Integration\Models\Integration;
use App\Http\Controllers\ApiController;
use App\Modules\ExternalApi\Models\ExternalApi;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\RequestGuzzle;
use Hashids\Hashids;




class ClaimTripaController extends ApiController
{
  protected $client;
  protected $internalApi;

  public function __construct()
  {
    $this->internalApi = ExternalApi::where(['category'=>'internal', 'provider'=>'lenna'])->first();
    $guzzleProp = [
      'base_uri' => $this->internalApi->base_url
    ];
    $this->client = new Client($guzzleProp);
  }

  public function getClaimPersonalAccidents(Request $request, $appId)
  {
    // return $request;
    $sort = $request->get('sort');
    $filterText = $request->get('filterText');
    $per_page = $request->get('per_page');
    $status = $request->get('status');

    $data = DB::table('tripa.klaim_personal_accidents as k')
              ->join('app.users as u', 'k.user_id', '=', 'u.id')->select('k.*','u.id', 'u.name', 'u.nickname', 'u.email')
              ->orderBy('k.id', 'DESC');
              // ->get();
    if ($sort) {
        $sort = explode('|', $sort);
        $data = $data->orderBy($sort[0], $sort[1]);
    }

    if ($status) {
        $data = $data->where('k.status', $status);
    }

    if ($filterText) {
        $data = $data->where(function ($query) use ($filterText) {
          $query->where('name', 'ILIKE', '%'.$filterText.'%');
          $query->orWhere('no_polis_asuransi', 'ILIKE', '%'.$filterText.'%');
          $query->orWhere('email', 'ILIKE', '%'.$filterText.'%');
          $query->orWhere('penyebab_kerugian', 'ILIKE', '%'.$filterText.'%');
          $query->orWhere('kronologis_kejadian', 'ILIKE', '%'.$filterText.'%');
        });
    }

    if (!$per_page) {
        $per_page = 15;
    }

    return response()->json($data->paginate($per_page));

  }

  public function getClaimTravels(Request $request, $appId)
  {
    $sort = $request->get('sort');
    $filterText = $request->get('filterText');
    $per_page = $request->get('per_page');
    $status = $request->get('status');

    $data = DB::table('tripa.klaim_travels as k')
              ->join('app.users as u', 'k.user_id', '=', 'u.id')->select('k.*','u.id', 'u.name', 'u.nickname', 'u.email')
              ->orderBy('k.id', 'DESC');
              // ->get();

    if ($sort) {
        $sort = explode('|', $sort);
        $data = $data->orderBy($sort[0], $sort[1]);
    }

    if ($status) {
        $data = $data->where('k.status', $status);
    }

    if ($filterText) {
        $data = $data->where(function ($query) use ($filterText) {
          $query->where('no_polis_asuransi', 'ILIKE', '%'.$filterText.'%');
          $query->orWhere('email', 'ILIKE', '%'.$filterText.'%');
          $query->orWhere('name', 'ILIKE', '%'.$filterText.'%');
          $query->orWhere('penyebab_kerugian', 'ILIKE', '%'.$filterText.'%');
          $query->orWhere('estimasi_kerugian', 'ILIKE', '%'.$filterText.'%');
        });
    }

    if (!$per_page) {
        $per_page = 15;
    }

    return response()->json($data->paginate($per_page));
  }

  public function getClaimVehicles(Request $request, $appId)
  {
    $sort = $request->get('sort');
    $filterText = $request->get('filterText');
    $per_page = $request->get('per_page');
    $status = $request->get('status');


    $data = DB::table('tripa.klaim_vehicles as k')
              ->join('app.users as u', 'k.user_id', '=', 'u.id')->select('k.*','u.id', 'u.name', 'u.nickname', 'u.email')
              ->orderBy('k.id', 'DESC');

    if ($sort) {
        $sort = explode('|', $sort);
        $data = $data->orderBy($sort[0], $sort[1]);
    }

    if ($status) {
      $data = $data->where('k.status', $status);
    }

    if ($filterText) {
        $data = $data->where(function ($query) use ($filterText) {
          $query->where('no_polis_asuransi', 'ILIKE', '%'.$filterText.'%');
          $query->orWhere('name', 'ILIKE', '%'.$filterText.'%');
          $query->orWhere('email', 'ILIKE', '%'.$filterText.'%');
          $query->orWhere('kerusakan', 'ILIKE', '%'.$filterText.'%');
          $query->orWhere('estimasi_kerugian', 'ILIKE', '%'.$filterText.'%');
          // ->orWhere('name', 'ILIKE', '%'.$filterText.'%')
          // ->orWhere('email', 'ILIKE', '%'.$filterText.'%')
          // ->orWhere('estimasi_kerugian', 'ILIKE', '%'.$filterText.'%')
          // ->orWhere('kerusakan', 'ILIKE', '%'.$filterText.'%');
        });
    }

    if (!$per_page) {
        $per_page = 15;
    }

    return response()->json($data->paginate($per_page));
  }

  public function getClaimWildfires(Request $request, $appId)
  {
    $sort = $request->get('sort');
    $filterText = $request->get('filterText');
    $per_page = $request->get('per_page');
    $status = $request->get('status');

    $data = DB::table('tripa.klaim_wildfires as k')
              ->join('app.users as u', 'k.user_id', '=', 'u.id')->select('k.*','u.id', 'u.name', 'u.nickname', 'u.email')
              ->orderBy('k.id', 'DESC');

    if ($sort) {
        $sort = explode('|', $sort);
        $data = $data->orderBy($sort[0], $sort[1]);
    }

    if ($status) {
        $data = $data->where('k.status', $status);
    }

    if ($filterText) {
        $data = $data->where(function ($query) use ($filterText) {
          $query->where('no_polis_asuransi', 'ILIKE', '%'.$filterText.'%');
          $query->orWhere('name', 'ILIKE', '%'.$filterText.'%');
          $query->orWhere('email', 'ILIKE', '%'.$filterText.'%');
          $query->orWhere('estimasi_kerugian', 'ILIKE', '%'.$filterText.'%');
          $query->orWhere('kerusakan', 'ILIKE', '%'.$filterText.'%');
          $query->orWhere('penyebab_kerugian', 'ILIKE', '%'.$filterText.'%');

          // ->orWhere('name', 'ILIKE', '%'.$filterText.'%')
          // ->orWhere('email', 'ILIKE', '%'.$filterText.'%')
          // ->orWhere('estimasi_kerugian', 'ILIKE', '%'.$filterText.'%')
          // ->orWhere('kerusakan', 'ILIKE', '%'.$filterText.'%')
          // ->orWhere('penyebab_kerugian', 'ILIKE', '%'.$filterText.'%');
        });
    }

    if (!$per_page) {
        $per_page = 15;
    }

    return response()->json($data->paginate($per_page));
  }

  public function updateStatusClaim(Request $request, $appId)
  {
    $claim = $request->claimName;
    $noPolisAsuransi = $request->noPolis;
    $status = $request->status;
    // return $appId;
    // $hashid = new Hashids('', 6);
    // $encodeAppId = $hashid->encode($appId);
    $requestData = [
      'nama_klaim' => $claim,
      'no_polis_asuransi' => $noPolisAsuransi,
      'status' => $status
    ];
    try {
      $response = $this->client->request(
        'POST',
        "backend/api/dBmKK5/tripa/update_klaim",
        [
          'json' => $requestData
        ]
      );
    } catch (\Exception $e) {
        return $resp = json_decode($e->getResponse()->getBody(),TRUE);
    }

    $responses = json_decode($response->getBody(),TRUE);
    // return $responses;

    $responseData = [
      'status' => $responses['data']['status']
    ];
    return $this->successResponse($responseData,$responses['message']);
    // return $responses;

  }

  public function updateStatusAgent(Request $request, $appId)
  {
    $userId = $request->user_id;
    $status = $request->status;
    // return $appId;
    // $hashid = new Hashids('', 6);
    // $encodeAppId = $hashid->encode($appId);
    $requestData = [
      'id' => $userId,
      'status' => $status
    ];
    try {
      $response = $this->client->request(
        'POST',
        "backend/api/dBmKK5/tripa/update_agent",
        [
          'json' => $requestData
        ]
      );
    } catch (\Exception $e) {
        return $resp = json_decode($e->getResponse()->getBody(),TRUE);
    }
    // return $response->getBody();
    $responses = json_decode($response->getBody(),TRUE);
    // return $responses;

    $responseData = [
      'status' => $responses['data']['status']
    ];
    return $this->successResponse($responseData,$responses['message']);
  }

  public function listApprovalAgent(Request $request, $appId)
  {
    $sort = $request->get('sort');
    $filterText = $request->get('filterText');
    $per_page = $request->get('per_page');
    $status = $request->get('status');


    $data = DB::table('tripa.agents as a')
              ->orderBy('id', 'DESC');


    if ($sort) {
        $sort = explode('|', $sort);
        $data = $data->orderBy($sort[0], $sort[1]);
    }

    if ($status) {
      $data = $data->where('a.status', $status);
  }

    if ($filterText){
        $data = $data->where(function ($q) use ($filterText){
            $q->where('nama','ILIKE', '%'.$filterText.'%');
            $q->orWhere('user_id', 'ILIKE', '%'.$filterText.'%');
            $q->orWhere('nik', 'ILIKE', '%'.$filterText.'%');
            $q->orWhere('tempat_lahir', 'ILIKE', '%'.$filterText.'%');
            $q->orWhere('tanggal_lahir', 'ILIKE', '%'.$filterText.'%');
            $q->orWhere('alamat_tinggal', 'ILIKE', '%'.$filterText.'%');
            $q->orWhere('telepon', 'ILIKE', '%'.$filterText.'%');
            $q->orWhere('handphone', 'ILIKE', '%'.$filterText.'%');
            $q->orWhere('no_anggota_agen', 'ILIKE', '%'.$filterText.'%');
            $q->orWhere('no_rekening', 'ILIKE', '%'.$filterText.'%');
            $q->orWhere('jabatan', 'ILIKE', '%'.$filterText.'%');
            $q->orWhere('nama_perusahaan', 'ILIKE', '%'.$filterText.'%');
        });
    }

    if (!$per_page) {
        $per_page = 15;
    }

    return response()->json($data->paginate($per_page));
  }

}
