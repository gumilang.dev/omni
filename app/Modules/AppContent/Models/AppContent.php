<?php

namespace App\Modules\AppContent\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppContent extends Model
{
    use SoftDeletes;
    protected $connection = 'pgsql';

    protected $guarded = [];
    protected $casts = [
        'actions' => 'object',
    ];

    public function App()
    {
        return $this->belongsTo("App\Modules\App\Models\App");
    }
}
