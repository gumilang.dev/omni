<?php

namespace App\Modules\AppContent\Controllers;

use Illuminate\Http\Request;
use App\Modules\AppContent\Models\AppContent;
use App\Http\Controllers\ApiController;

class AppContentController extends ApiController
{

    public function index(Request $request, $appId)
    {
        $sort = $request->get('sort');
        $filterText = $request->get('filterText');
        $per_page = $request->get('per_page');

        $appContent = new \App\Modules\AppContent\Models\AppContent;

        if ($sort) {
            $sort = explode('|', $sort);
            $appContent = $appContent->orderBy($sort[0], $sort[1]);
        }

        if ($filterText) {
            $appContent = $appContent->where(function ($query) use ($filterText) {
                $query->where('title', 'ILIKE', '%' . $filterText . '%')->orWhere('category', 'ILIKE', '%' . $filterText . '%');
            });
        }

        // if ($category) {
        //     $appContent = $appContent->where("category",$category);
        // }

        if (!$per_page) {
            $per_page = 15;
        }

        $appContent = $appContent->where('app_id', $appId);

        return response()->json($appContent->paginate($per_page));
    }


    public function store(Request $request, $appId)
    {

        $this->validate($request, [
            // 'title' => 'required|string',
            'description' => 'required',
        ]);

        $appContent = AppContent::create([
            'title' => $request->title,
            'description' => $request->description,
            'category' => $request->category,
            'image_url' => $request->image_url ?? 'defaultImage',
            'actions' => $request->actions,
            'app_id' => $appId,
            'created_by' => $request->decodedUserId,

        ]);
        return $this->successResponse(['content' => $appContent], 'content created');

    }





    public function edit($id)
    {
        return $id;
    }


    public function update(Request $request, $appId, $appContentId)
    {
        // return $request->all();
        $validatedData = $request->validate([
            // 'title' => 'required',
            'description' => 'required',
        ]);


        $data = [
            'title' => $request->title,
            'description' => $request->description,
            'image_url' => $request->image_url,
            'actions' => json_encode($request->actions),
            'category' => $request->category,
            'is_active' => $request->status,
            'updated_by' => $request->decodedUserId,

        ];
        // dd($data);
        $appContent = AppContent::where('id', $appContentId)->update($data);

        return $this->successResponse(['content' => $appContent], 'content updated');
    }


    public function destroy($appId, $appContentId)
    {
        $appContent = AppContent::findOrFail($appContentId);
        $appContent->delete();

        return $this->successResponse(null, 'content deleted');
    }

    public function uploadImage(Request $request, $appId)

    {
        $name = time() . $request->file('image')->getClientOriginalName();

        $request->image->move(public_path('/upload/content'), $name);
        // $path = $request->file('image')->store('uploaded','public');

        return response()->json(['image_url' => env("APP_URL") . "/upload/content/" . $name], 200);
    }
}
