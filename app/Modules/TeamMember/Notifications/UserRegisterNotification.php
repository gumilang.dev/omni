<?php

namespace App\Modules\TeamMember\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use Config;
use \Illuminate\Support\Facades\URL;

class UserRegisterNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token, $app, $role, $invitor, $pass)
    {
        $this->token    = $token;
        $this->app      = $app;
        $this->role     = $role;
        $this->invitor  = $invitor;
        $this->pass     = $pass;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
      $url = env("AUTH_URL");
      return (new MailMessage)
        ->subject('Verify Email Address')
        ->line('Please click the button below to verify your email address')
        ->action('Verify Email Address', url($url.'/api/verify/'.$this->token))
        ->line('Your password is, "'.$this->pass.'"')
        ->line('You also invited to "'.$this->app->name.'" app, with role "'.$this->role->name.'" by "'.$this->invitor->name.'"');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
