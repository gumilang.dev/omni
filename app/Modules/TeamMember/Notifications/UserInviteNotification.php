<?php

namespace App\Modules\TeamMember\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use Config;
use \Illuminate\Support\Facades\URL;

class UserInviteNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($app, $role, $invitor)
    {
        $this->app      = $app;
        $this->role     = $role;
        $this->invitor  = $invitor;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
      $authUrl = env("AUTH_URL");
      return (new MailMessage)
        ->from('noreply@lenna.ai')
        ->subject('Team Member Invite Notification')
        ->line('You invited to '.$this->app->name.' app')
        ->line('with role '.$this->role->name)
        ->line('by '.$this->invitor->name)
        ->action('Check landing page', url($authUrl));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
