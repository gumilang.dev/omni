<?php

namespace App\Modules\TeamMember\Controllers;

use App\Http\Controllers\ApiController;

use App\Modules\User\Models\UserPlatform;
use App\Modules\User\Models\UserApp;
use App\Modules\User\Models\UserRoleChannel;
use App\Modules\User\Models\VerifyUser;
use App\Modules\User\Models\UserRole;
use App\Modules\App\Models\App;
use App\Modules\Role\Models\Role;
use App\Modules\TeamMember\Models\HistoryActivityUserRole;
use App\Modules\ExternalApi\Models\ExternalApi;
use DB;
use Hash;
use Illuminate\Support\Facades\Validator;


use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Modules\TeamMember\Notifications\UserRegisterNotification;
use App\Modules\TeamMember\Notifications\UserDeleteNotification;
use App\Modules\TeamMember\Notifications\UserInviteNotification;
use App\Services\PaginatorServiceImpl;

use App\Modules\Role\Helpers\RoleHelper;

use Hashids\Hashids;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Faker;

class TeamMemberController extends ApiController
{

    public function __construct()
    {
      $this->hashids = new Hashids('', 6);
      $this->paginator = new PaginatorServiceImpl();
    }


    /**
     * invite member
     * @param string appId
     * @return array data
     */
    public function store(Request $request, $appId){
      $isUseUsername= $request->username == "" ? false : true;
      if($isUseUsername) {
        $this->validate($request, [
          'username'     => 'required',
          'role'      => 'required',
          'invitor'   => 'required',
        ]);
      }else {
        $this->validate($request, [
          'email'     => 'required|email',
          'role'      => 'required',
          'invitor'   => 'required',
        ]);
      }


      if($isUseUsername) {
        // check coworker id is already register
        $user_platform = UserPlatform::where('username', $request->username)->first();
        if($user_platform) {
          // check already have access to this app
          $check_userapp = UserApp::where('user_id', $user_platform->id)->where('app_id', $appId)->first();
          if($check_userapp) {
            return $this->errorResponse(null, 'User already have access to this app');
          }else{

            $user_role = $this->inviteMember($user_platform->id, $appId, $this->hashids->decode($request->role)[0]);

            // add channel to role
            $this->addChannelAssignment($request->channel, $user_role);

            //create history role activity
            $addHistoryActivityData = [
              "request" => $request,
              "activity" => "ADD",
              "account_modified_name" => $user_platform->name,
              "account_modified_email" => $user_platform->email,
              "account_modified_previous_role" => null,
              "account_modified_current_role" => $user_role->role->name,
              "app_id" => $appId
            ];
            $this->addHistoryActivityTeamMember($addHistoryActivityData);

            // kirim notifikasi email
            $this->sendInviteNotification($appId, $request->role, $request->invitor, $request->email);
            return $this->successResponse(null, 'User have been given access to this app');
          }
        }else{
          return $this->registerAndInviteMember($request, $appId);
        }
      }else{
          // check user platform exist
        $user_platform = UserPlatform::where('email', $request->email)->first();
        if (!$user_platform) { //jika user platform belum terregister
          return $this->registerAndInviteMember($request, $appId);
        }

        $check_verified = UserPlatform::where('email', $request->email)->where('email_verified_at', NULL)->first();
        if ($check_verified) {
          return $this->errorResponse(null, "User has been invited but hasn't verified email");
        }

        // check user sudah di invite di platform
        $check_user_app = UserApp::where('user_id', $user_platform->id)->where('app_id', $appId)->first();

        // jika user belum ada pada user app
        if (!$check_user_app) {
          // return 'checkuserapp';
          $user_role = $this->inviteMember($user_platform->id, $appId, $this->hashids->decode($request->role)[0]);
          // add channel to role
          $this->addChannelAssignment($request->channel, $user_role);

          //create history role activity
          $addHistoryActivityData = [
            "request" => $request,
            "activity" => "ADD",
            "account_modified_name" => $user_platform->name,
            "account_modified_email" => $user_platform->email,
            "account_modified_previous_role" => null,
            "account_modified_current_role" => $user_role->role->name,
            "app_id" => $appId
          ];
          $this->addHistoryActivityTeamMember($addHistoryActivityData);

          // kirim notifikasi email
          $this->sendInviteNotification($appId, $request->role, $request->invitor, $request->email);
          return $this->successResponse(null, 'User have been given access to this app');
        }
        return $this->errorResponse(null, 'User already have access to this app');
      }
    }

    public function registerAndInviteMember($request, $appId){
      $faker      = Faker\Factory::create();
      $password   = $faker->password;
      $email      = $request->email;
      $name       = $request->name;
      $username = $request->username;
      // register user platform
      $userPlatform  = new UserPlatform;
      if ($username !== "" || $username !== null || $username){
        $userPlatform->username = $username ?? null;
      }
      $userPlatform->password = bcrypt($password);
      $userPlatform->email = $email;
      $userPlatform->name = $name;
      $userPlatform->save();

      if($username == "" || $username == null || !$username) {
        $verifyUser = $this->checkTokenUserPlatform($email);
      }

		  // check user sudah di invite di platform
      $checkUserApp = UserApp::where('user_id', $userPlatform->id)->where('app_id', $appId)->first();
      if (!$checkUserApp) {

        $userRole = $this->inviteMember($userPlatform->id, $appId, $this->hashids->decode($request->role)[0]);

          // jika channelnya tidak null
          $this->addChannelAssignment($request->channel, $userRole);
          if($username == "") {
            $this->sendRegisterNotification($verifyUser->token, $appId, $this->hashids->decode($request->role)[0], $request->invitor, $request->email, $password);
          }

          //create history role activity
          $addHistoryActivityData = [
            "request" => $request,
            "activity" => "ADD",
            "account_modified_name" => $userPlatform->name,
            "account_modified_email" => $userPlatform->email,
            "account_modified_previous_role" => null,
            "account_modified_current_role" => $userRole->role->name,
            "app_id" => $appId
          ];
          $this->addHistoryActivityTeamMember($addHistoryActivityData);
          return $this->successResponse(null, 'User have been given access to this app');
        }
    }

	public function getTeamMember(Request $request, $appId)
	{
        $sort = $request->get('sort');
        $filterText = $request->get('filterText');
        $per_page = $request->get('per_page');

        // $user = UserRole::with(['role','userPlatform', 'userRoleChannel.channel'])->where('product_id', $appId)->where('platform_id', env('OMNI_PLATFORM_ID'))->orderBy('updated_at', 'DESC');
        $user = UserApp::with(['role','userChannels.channel','user'])->where('app_id', $appId)->orderBy('updated_at', 'DESC');





        // $user = $user->where('product_id', $appId)->where('platform_id', env('OMNI_PLATFORM_ID'))->orderBy('updated_at', 'DESC');
        $user = $user->where('app_id', $appId)->orderBy('updated_at', 'DESC')->get();

        if($filterText) {
          $user = $user->filter(function ($item) use ($filterText) {
              return false !== stristr($item->user["name"], $filterText) || false !== stristr($item->user["email"], $filterText) || false !== stristr($item->role["name"], $filterText) ;
          })->values();
        }

        if (!$per_page) {
          $per_page = 100;
      }

        return response()->json($this->paginator->paginate($user,$per_page));
    }

	public function getRegisteredMember(Request $request, $appId)
	{
		if ($request->email == '') {
			return [];
		}
		$users = UserPlatform::select('name','email')->where('email', 'ILIKE', '%' .$request->email. '%')->get();
		return $users;
    }

	public function deleteAccess(Request $request, $appId)
	{
		$decodedId = $this->hashids->decode($request->id)[0];
		$userApp = UserApp::where('user_id', $decodedId)->where('app_id', $appId)->with("user")->first();
		// if ($userApp->role_id == 4) {
    if (RoleHelper::isAgent(['userId' => $decodedId, 'appId' => $appId])) {
			UserRoleChannel::where('app_user_platform_id', $userApp->id)->delete();
		}

		/*
		/* params 1 = appId
		/* params 2 = deletorId
		/* params 3 = targetId
		*/

    //create history role activity
    $addHistoryActivityData = [
      "request" => $request,
      "activity" => "DELETE",
      "account_modified_name" => $userApp->user->name,
      "account_modified_email" => $userApp->user->email,
      "account_modified_previous_role" => $userApp->role->name,
      "account_modified_current_role" => null,
      "app_id" => $appId
    ];
    $this->addHistoryActivityTeamMember($addHistoryActivityData);

		// $this->sendDeleteNotification($appId, $request->decodedUserId, $decodedId);
		// force delete user_app_platform
		UserApp::where('user_id', $decodedId)->where('app_id', $appId)->forceDelete();
		return $this->successResponse(null, 'Access deleted');
  }

    public function editAccess(Request $request, $appId)
    {
      // return $request;
      $editor     = UserPlatform::find($request->decodedUserId);
      $user       = UserPlatform::find($this->hashids->decode($request->id)[0]);
      $previousRole = RoleHelper::checkCurrentRole($this->hashids->decode($request->id)[0], $appId);
      if (!RoleHelper::isCapableToChangeRole(['userId' => $user->id, 'editorId' => $editor->id, 'appId' => $appId]))
      {
        return response()->json([
          'success' => false,
          'message' => 'Your role is not capable to edit this member'
        ]);
      }
      $response = $this->editMember($request, $editor, $user, $appId);
      if (!$response['success']){
        return response()->json([
          'success' => false,
          'message' => $response['message'],
        ]);
      } else {
        //create history role activity
         $addHistoryActivityData = [
          "request" => $request,
          "activity" => "EDIT",
          "account_modified_name" => $user->name,
          "account_modified_email" => $user->email,
          "account_modified_previous_role" => $previousRole->name,
          "account_modified_current_role" => $response['data']->role->name,
          "app_id" => $appId
        ];
        $this->addHistoryActivityTeamMember($addHistoryActivityData);

        return $this->successResponse($response['data'], 'Role Edited');
      }
    }

    public function editMember($request, $editor, $user, $appId)
    {
      $roleName = $request->role == "Agent" ? "Staff" : $request->role;
      $role     = Role::where('app_id', $appId)->where('name', $roleName)->first();
      if (!$role || $role == null)
      {
        return [
          'success' => false,
          'message' => 'Role not found'
        ];
      }

      $roleEditor = RoleHelper::checkCurrentRole($editor->id, $appId);
      // return $roleEditor;
      if (!RoleHelper::canEditToRole(['roleTarget' => $role, 'roleEditor' => $roleEditor]))
      {
        return [
          'success' => false,
          'message' => "Sorry, you can't edit role to ".$role->name
        ];
      }

      if ($role->ranking < $editor->id)
      {
        $userRole = UserApp::updateOrCreate(
          [
          'user_id'     => $user->id,
          'app_id'      => $appId
          ],
          [
          'role_id'     => $role->id
          ]
        );
      }
      // if role agent, check channel edited or not
      if ($roleName == "Staff")
      {
        // if have selected channel to edit
        if (count($request->selectedChannel) > 0)
        {
        // delete past UserRoleChannel then insert new UserRoleChannel
        // UserRoleChannel::where('user_role_id', $userRole->id)->delete();
        UserRoleChannel::where('app_user_platform_id', $userRole->id)->delete();
        $channel_data = [];
        $channels = $request->selectedChannel;
        foreach($channels as $channel)
        {
          if(!empty($channel))
          {
          $channel_data[] = [
            'app_user_platform_id'  => $userRole->id,
            'channel_id'            => $channel['id'],
            'created_at'            => Carbon::now()->toDateTimeString(),
            'updated_at'            => Carbon::now()->toDateTimeString()
          ];
          }
        }
        UserRoleChannel::insert($channel_data);
        }
      } else {
        UserRoleChannel::where('app_user_platform_id', $userRole->id)->delete();
      }
      return [
        'success' => true,
        'data' => $userRole
      ];
    }

    // public function deleteRole(Request $request, $appId){
    //   // delete all user role
    //   UserRole::where('platform_id', env('OMNI_PLATFORM_ID'))->where('product_id', $appId)->delete();

    //   $user_role = UserRole::where('platform_id', env('OMNI_PLATFORM_ID'))->where('product_id', $appId)->get();

    //   // delete user channel
    //   $user_channel = UserRoleChannel::where('user_role_id', $user_role->id)->delete();
    //   return $this->successResponse(null, 'Success delete all user role');
    // }

	public function checkTokenUserPlatform($email)
	{
		// check token user
		$verifyUser = VerifyUser::updateOrCreate(
			[
			'email' => $email
			],
			[
			'token' => str_random(60)
			]
		);
		return $verifyUser;
    }


    /**
     * invite member
     * @param userId
     * @param appId
     * @param roleId
     * @param invitor
     */
    public function inviteMember($userId, $appId, $roleId)
    {
		// invite user dan tambahkan role
		$new_user_app           = new UserApp;
		$new_user_app->user_id  = $userId;
		$new_user_app->app_id   = $appId;
		$new_user_app->role_id  = $roleId;
		$new_user_app->save();

		// tambahkan user role, tempatkan pada product dan platform
		// $user_role              = new UserRole;
		// $user_role->user_id     = $userId;
		// $user_role->role_id     = $roleId;
		// $user_role->created_by  = $invitor;
		// $user_role->platform_id = env('OMNI_PLATFORM_ID');
		// $user_role->product_id  = $appId;
		// $user_role->save();

		return $new_user_app;
    }

    public function addChannelAssignment($channels, $userRole)
    {
		if (count($channels) > 0){
			$channel_data = [];
			foreach($channels as $channel) {
			if(!empty($channel))
			{
				$channel_data[] = [
				'channel_id'    => $channel,
				'app_user_platform_id'  => $userRole->id,
				];
			}
			}
			UserRoleChannel::insert($channel_data);
		}
    }

    public function sendInviteNotification($appId, $roleId, $invitorId, $email)
    {
		$app = App::where('id', $appId)->first();
		$role = Role::where('app_id', $appId)->where('id', $this->hashids->decode($roleId)[0])->first();
		if ($role->slug == 'staff') {
			$role->name = "Staff";
		}
		$invitor = UserPlatform::where('id', $this->hashids->decode($invitorId)[0])->first();
		$user = UserPlatform::where('email', $email)->first();
		$user->notify(new UserInviteNotification($app, $role, $invitor));
    }

    public function sendRegisterNotification($token, $appId, $roleId, $invitorId, $targetEmail, $password)
    {
      $app = App::where('id', $appId)->first();
      $role = Role::where('app_id', $appId)->where('id', $roleId)->first();
      if ($role->slug == 'staff') {
        $role->name = "Agent";
      }
      $invitor = UserPlatform::where('id', $this->hashids->decode($invitorId)[0])->first();
      $user = UserPlatform::where('email', $targetEmail)->first();
      $user->notify(new UserRegisterNotification($token, $app, $role, $invitor, $password));
    }

    public function sendDeleteNotification($appId, $deletorId, $targetId)
    {
      $app = App::where('id', $appId)->first();
      $deletor = UserPlatform::where('id', $deletorId)->first();
      $user = UserPlatform::where('id', $targetId)->first();
      $user->notify(new UserDeleteNotification($app, $deletor));
    }

    public function resetDefaultPassword(Request $request, $appId)
    {
		$newPassword = $request->password;
		$userToBeChanged = UserApp::with(['role','user'])
									->where('app_id', $appId)
									->where('user_id', $this->hashids->decode($request->id)[0])
									->first();

		$userPlatform = UserApp::with(['role','user'])
								->where('app_id', $appId)
								->where('user_id', $request->decodedUserId)
								->orderBy('updated_at', 'DESC')
                ->first();

    $privilegeUserPlatform = $userPlatform->role->privilege;

    $user = UserPlatform::find($this->hashids->decode($request->id)[0]);

    $validator = Validator::make($request->all(), [
      'password' => 'required'
    ]);

    if($validator->fails()){
      if ($validator->errors()->first('password')) {
        return $this->errorResponse(null, $validator->errors()->first('old_password'));
      }
    }

    $uppercase = preg_match('@[A-Z]@', $newPassword);
    $lowercase = preg_match('@[a-z]@', $newPassword);
    $number    = preg_match('@[0-9]@', $newPassword);
    // $special   = preg_match('/\W|_/g', $newPassword); //not working
    $special   = preg_match('/^(?=(?:.*[A-Z]){1,})(?=(?:.*[a-z]){1,})(?=(?:.*\d){1,})(?=(?:.*[!@#$%^&*()\-_=+{};:,<.>]){1,})(?!.*(.)\1{2})([A-Za-z0-9!@#$%^&*()\-_=+{};:,<.>]{8,})$/', $newPassword);
    $length    = strlen($newPassword) >= 8;

    if (!$uppercase) {
     return $this->errorResponse(null, "New password needs at least 1 uppercase letter");
    }
    else if(!$lowercase) {
      return $this->errorResponse(null, "New password needs at least 1 lowercase letter");
    }
    else if (!$number) {
      return $this->errorResponse(null, "New password needs at least 1 number");
    }
    else if (!$length) {
      return $this->errorResponse(null, "Password is too short (minimum is 8 characters)");
    }
    else if (!$special) {
      return $this->errorResponse(null, "New password needs at least 1 special character");
    }


		if (!$userPlatform) {
			return $this->errorResponse(null, 'User not found');
    }
    if (!in_array("can-reset-password", $privilegeUserPlatform)) {
      return $this->errorResponse(null, 'You dont have access to reset.');
    }
		if ($userPlatform->role->ranking >= $userToBeChanged->role->ranking) {
			return $this->errorResponse(null, 'Access Denied');
		}
		if (!$user) {
			return $this->errorResponse(null, 'User not found');
		}
		if (Hash::check($newPassword, $user->password)) {
			return $this->errorResponse(null, 'New password same as the old one');
		}

		$user->password = bcrypt($newPassword);
		$user->update();
		return $this->successResponse($user, 'Password changed.');

  }

  public function addHistoryActivityTeamMember($data)
  {
    // return $data;
    $userPlatform = UserPlatform::where('id', $data["request"]->decodedUserId)->first();
    $userPlatformRole = RoleHelper::checkCurrentRole($userPlatform->id, $data['app_id']);
    $historyActivity = new HistoryActivityUserRole;
    // return $data;
    // $invitorRole = $this->hashids->decode($data['request']->invitor_role)[0];
    // $historyActivity->user_platform_id = $data['request']->decodedUserId;
    // $historyActivity->role_id = $invitorRole;
    // $historyActivity->activity = $data['activity'];
    // $historyActivity->account_modified_id = $data['account_modified_id'];
    // $historyActivity->previous_role_id = $data['previous_role_id'];
    // $historyActivity->current_role_id = $data['current_role_id'];

    $historyActivity->user_platform_name = $userPlatform->name;
    $historyActivity->user_platform_email = $userPlatform->email;
    $historyActivity->user_platform_role = $userPlatformRole->name;
    $historyActivity->activity = $data['activity'];
    $historyActivity->account_modified_name = $data['account_modified_name'];
    $historyActivity->account_modified_email = $data['account_modified_email'];
    $historyActivity->account_modified_previous_role = $data['account_modified_previous_role'];
    $historyActivity->account_modified_current_role = $data['account_modified_current_role'];
    $historyActivity->app_id = $data['app_id'];
    $historyActivity->save();
    return $historyActivity;

  }


}
