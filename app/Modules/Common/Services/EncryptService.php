<?php

namespace App\Modules\Common\Services;

use App\Modules\Mobile\Services\BackendService;

class EncryptService
{
    public static function cipher($ch, $key)
    {
      if (!ctype_alpha($ch)) {
        return $ch;
      }
      $offset = ord(ctype_upper($ch) ? 'A' : 'a');
      return chr(fmod(((ord($ch) + $key) - $offset), 26) + $offset);
    }

    public static function encrypt($plainText, $key, $sizeKey = 20)
    {
      $text = json_encode($plainText);
      // $output = self::encipher($text, $sizeKey);
      $output = BackendService::encryptText($key, $text);
      return [(object)['data' => $output]];
    }

    public static function encipher($input, $key)
    {
      $output = "";
      $inputArr = str_split($input);
      foreach ($inputArr as $ch) {
        $output .= self::cipher($ch, $key);
      }
      return $output;
    }

    public static function decrypt($encryptedText, $key, $sizeKey = 20)
    {
      $content = json_decode($encryptedText);
      $output = BackendService::decryptText($key, $content[0]->data);
      // $output = self::encipher($decryptedText, 26 - $sizeKey);
      return json_decode($output);
    }

    public static function encryptResponse($data, $appId, $channelId) {
      $app          = collect(config('encrypt.enabled.apps'));
      $app          = $app->where('id', $appId)->values()->all();
      if (count($app) > 0) {
        if (collect($app[0]['channels'])->contains($channelId)) {
          $data = BackendService::encryptText(env('ENCRYPT_MESSAGE_KEY'), json_encode($data));
          return ['encryption' => $data];
        }
        return $data;
      }
      return $data;
    }
}
