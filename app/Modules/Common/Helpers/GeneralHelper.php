<?php

namespace App\Modules\Common\Helpers;

class GeneralHelper
{
    public static function objectToArray($obj)
    {
        return json_decode(json_encode($obj), true);
    }
}