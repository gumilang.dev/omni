<?php

namespace App\Modules\AdditionalInformation\Model;

use Illuminate\Database\Eloquent\Model;

class AdditionalInformation extends Model
{
    protected $table = "additional_informations";
    protected $fillable = ["title","description","room_id","user_id"];
}
