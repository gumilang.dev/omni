<?php

namespace App\Modules\AdditionalInformation\Controller;

use App\Modules\AdditionalInformation\Model\AdditionalInformation;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class AdditionalInformationController extends ApiController
{
    private $additionalInformation;
    public function __construct(AdditionalInformation $additionalInformation) {
        $this->additionalInformation = $additionalInformation;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * build data.
     *
     * @return \Illuminate\Http\Response
     */
    public function makeData($request) {
       return [
           'room_id' => $request->input('room_id'),
           'title' => $request->input('title'),
           'description' => $request->input('description'),
           "user_id" => $request->input("user_id")
       ];
    }

    public function create(Request $request,$appId)
    {
        $this->validate($request, [
            'title' => 'required|string|max:50',
            'description' => 'required',
            'room_id' => 'required',
            "user_id" => 'required'
        ]);

        $data = $this->makeData($request);
        $additional_information = $this->additionalInformation::create($data);
        return $this->successResponse($additional_information,'success create addional information');
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdditionalInformation  $additionalInformation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$appId,$id)
    {
        $this->validate($request, [
            'title' => 'required|string|max:50',
            'description' => 'required',
        ]);

        $data = $this->makeData($request);
        $data['id'] = $id;
        $additional_information = $this->additionalInformation::where('id',$id)->update($data);
        return $this->successResponse($data,'success edit addional information');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdditionalInformation  $additionalInformation
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdditionalInformation $additionalInformation,$appId,$id)
    {
        $deletedAdditonalInformation = $this->additionalInformation::where('id',$id)->delete();
        return $this->successResponse($deletedAdditonalInformation,'success delete addional information');
    }
}
