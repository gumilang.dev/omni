<?php

namespace App\Modules\Agent\Controller;

use Illuminate\Http\Request;
use App\Services\AgentServiceImpl;
use App\Http\Controllers\ApiController;
use Carbon\Carbon;
use App\Events\AssigntAgent;
use App\Modules\Room\Models\Room;
use App\Modules\Room\Controllers\RoomController;
use App\Events\ApprovalAssignToAgent;
use App\Events\RejectAssign;
use App\Events\OnlineOffline;

class AgentController extends ApiController
{

    protected $service;
    protected $roomController;

    public function __construct(AgentServiceImpl $service,RoomController $roomController) {
        $this->service = $service;
        $this->roomController = $roomController;
    }

    public function assignTo(Request $request, $appId) {
      // $room = Room::where("app_id",$appId)->with(['channel','tags','additional_informations','notes'])->get();

      $agent_id = $request->input("agent_id");
      $room_id  = $request->input("room_id");
      $livechat = $request->input("livechat");
      $isOnline = $this->service->isAgentOnline([
        "app_id" => $appId,
        "user_id" => $agent_id
      ]);
      // check agent isOnline
      if($isOnline) {
        $response = $this->service->assign(
          [
            "room_id" => $room_id,
          ],
          [
            "handle_by" => $agent_id,
            "updated_by" => $agent_id,
            "start_at" => Carbon::now()->toDateTimeString(),
            "status" => "live"
          ],
          $livechat
        );
        $total = $this->roomController->getRoomByStatus($appId,"request");
        $currentRoom = Room::where("id",$room_id)->with(['channel','tags',"additional_informations","notes"])->first();
        broadcast(new AssigntAgent($appId,$currentRoom,$total));
        return $this->successResponse([
          "success" => true
        ],'Assign room to agent '.$agent_id);
      }else {
        return $this->successResponse([
          "success" => false,
          "message" => "Failed assign to agent currenty agent is offline"
        ],"failed");
      }


    }

    public function approvelAssign(Request $request, $appId) {
      $room  = $request->room;
      $agent = $request->agent;
      $assignor = $request->assignor;
      $assignor_role = $request->assignor_role;
      broadcast(new ApprovalAssignToAgent($appId,$room,$agent,Carbon::now()->toDateTimeString(),$assignor, $assignor_role))->toOthers();
      return $this->successResponse([
        "success" => true,
        "room" => $room,
        "agent" => $agent
      ],'Assign room to agent');
    }

    public function rejectAssign(Request $request,$appId) {
      $room  = $request->room;
      $agent = $request->agent;
      $message = $request->message;
      broadcast(new RejectAssign($appId,$room,$agent,$message))->toOthers();
      return $this->successResponse([
        "success" => true,
      ],'Reject Assign room to agent');
    }

    public function changeStatus(Request $request, $appId) {
      $data = [
        "app_id" => $appId,
        "agent_id" => $request->agent_id,
        "reason_offline" => $request->reason_offline,
        "online" => $request->online
      ];
      $response = $this->service->changeStatus($data);
      broadcast(new OnlineOffline($appId,$request->agent_id,$request->online));
      return $this->successResponse(["online" => $request->online],'change status success');
    }

}
