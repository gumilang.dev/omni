<?php

namespace App\Modules\AutoResponseOfficeHour\Controllers;

use Illuminate\Http\Request;
use App\Modules\AutoResponseOfficeHour\Models\OfficeHour;
use App\Modules\AutoResponseOfficeHour\Models\Days;
use App\Http\Controllers\ApiController;
use App\Modules\GeneralValue\Models\GeneralValue;
use App\Modules\App\Models\App;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use Illuminate\Support\Facades\Storage;





use App\Modules\Integration\Models\Integration;




class AutoResponseController extends ApiController
{

   public function index($appId)
   {
      $data = OfficeHour::where(['app_id' => $appId, 'is_active' => true])->get();
      return $this->successResponse($data);
   }

   public function getAutoResponseData(Request $request, $appId)
   {
      $officeHour = GeneralValue::where('app_id', $appId)
                                 ->where('key', 'auto-response-office-hour')
                                 ->first();
      $outsideOfficeHour = GeneralValue::where('app_id', $appId)
                                       ->where('key', 'auto-response-outside-office-hour')
                                       ->first();

      if (!$officeHour) {
         $generalValue = new GeneralValue;
         $generalValue->app_id = $request->decodedAppId;
         $generalValue->key = "auto-response-office-hour";
         $generalValue->value = "Baik agent akan segera menghubungi anda";
         $generalValue->value_type = "text";
         $generalValue->is_active = true;
         $generalValue->created_by = $request->decodedUserId;
         $generalValue->updated_by = $request->decodedUserId;
         $generalValue->save();
      }
      if (!$outsideOfficeHour) {
         $apps = App::find($request->decodedAppId);
         $generalValue = new GeneralValue;
         $generalValue->app_id = $request->decodedAppId;
         $generalValue->key = "auto-response-outside-office-hour";
         $generalValue->value = "Terimakasih sudah menghubungi layanan chat ".$apps->name.". Silahkan menghubungi kami kembali dari senin-jumat 08:30-17.30 wib. Selamat Beraktifitas Kembali";
         $generalValue->value_type = "text";
         $generalValue->is_active = true;
         $generalValue->created_by = $request->decodedUserId;
         $generalValue->updated_by = $request->decodedUserId;
         $generalValue->save();
      }

      return $this->_getAutoResponse($appId);
   }
   public function _getAutoResponse($appId)
   {
      $officeHour = GeneralValue::where('app_id', $appId)
                                 ->where('key', 'auto-response-office-hour')
                                 ->first();
      $outsideOfficeHour = GeneralValue::where('app_id', $appId)
                                       ->where('key', 'auto-response-outside-office-hour')
                                       ->first();

      return response()->json([
         "success" => true,
         "message" => null,
         "data" => [
            'office_hour' => $officeHour,
            'outside_office_hour' => $outsideOfficeHour
         ]
      ]);
   }

   public function updateAutoResponseData(Request $request, $appId)
   {
      $this->validate($request, [
         'idOfficeHour' => 'required',
         'valueOfficeHour' => 'required',
         'idOutsideOfficeHour' => 'required',
         'valueOutsideOfficeHour' => 'required',
         'valueTypeOfficeHour' => 'required',
         'valueTypeOutsideOfficeHour' => 'required'
      ]);


      $officeHour = GeneralValue::find($request->idOfficeHour);
      $officeHour->value = $request->valueOfficeHour;
      $officeHour->updated_by = $request->decodedUserId;
      $officeHour->value_type = $request->valueTypeOfficeHour;
      $officeHour->save();

      $outsideOfficeHour = GeneralValue::find($request->idOutsideOfficeHour);
      $outsideOfficeHour->value = $request->valueOutsideOfficeHour;
      $outsideOfficeHour->updated_by = $request->decodedUserId;
      $outsideOfficeHour->value_type = $request->valueTypeOutsideOfficeHour;
      $outsideOfficeHour->save();

      return response()->json([
         "success" => true,
         "message" => "Updated",
         "data" => [
            'office_hour' => $officeHour,
            'outisde_office_hour' => $outsideOfficeHour
         ]
      ]);


   }

   public function uploadFile(Request $request)
   {
      // $generalValue = GeneralValue::find($request->idOutsideOfficeHour);
      // $data = json_decode($generalValue->files, TRUE);
      // if (isset($data['test'])) {
      //    return 'test';
      // }
      // return ' nul';

      // return json_decode($generalValue->files, TRUE)['image_url'];
      // $oldFile = json_decode($generalValue->files, TRUE);
      // $oldFile = $oldFile['file_url'];
      // return json_decode($generalValue->files, true)['file_url'];

      if ($request->hasFile('image')) {
         $this->validate($request, [
            "image" => 'file|max:5120|required'
         ]);
         $name = time() . $request->file('image')->getClientOriginalName();
         $generalValue = GeneralValue::find($request->idOutsideOfficeHour);
         if ($generalValue) {
            if (isset(json_decode($generalValue->files, TRUE)['image_url'])) {
               $oldFile = json_decode($generalValue->files, TRUE)['image_name'];
               Storage::disk('public_general_value')->delete([$oldFile]); //delete previous file
            }
            $request->image->move(public_path('/upload/general-value/'),$name);
            $optimizerChain = OptimizerChainFactory::create();
            $optimizerChain->optimize(public_path('/upload/general-value/'.$name, public_path('/upload/general-value/').$name));
            $generalValue->files = json_encode([
                  "image_url" => env("APP_URL")."/upload/general-value/".$name,
                  "image_name" => $name,
                  "video_url" => json_decode($generalValue->files, TRUE)['video_url'] ?? "",
                  "video_name" => json_decode($generalValue->files, TRUE)['video_name'] ?? "",
               ]);
            $generalValue->save();
            return response()->json([
                  "image_url" => env("APP_URL")."/upload/general-value/".$name,
                  // "old_image_url" => $oldFile
            ], 200);
         }
      } else if ($request->hasFile('video')) {
         $this->validate($request, [
            "video" => 'file|max:10000|required'
         ]);
         $name = time() . $request->file('video')->getClientOriginalName();
         $generalValue = GeneralValue::find($request->idOutsideOfficeHour);
         if ($generalValue) {
            if (isset(json_decode($generalValue->files, TRUE)['video_url'])) {
               $oldFile = json_decode($generalValue->files, TRUE)['video_name'];
               Storage::disk('public_general_value')->delete([$oldFile]); //delete previous file
            }
            $request->video->move(public_path('/upload/general-value/'),$name);
            $optimizerChain = OptimizerChainFactory::create();
            $optimizerChain->optimize(public_path('/upload/general-value/'.$name, public_path('/upload/general-value/').$name));
            $generalValue->files = json_encode([
                  "image_url" => json_decode($generalValue->files, TRUE)['image_url'] ?? "",
                  "image_name" => json_decode($generalValue->files, TRUE)['image_name'] ?? "",
                  "video_url" => env("APP_URL")."/upload/general-value/".$name,
                  "video_name" => $name
               ]);
            $generalValue->save();
            return response()->json([
                  "video_url" => env("APP_URL")."/upload/general-value/".$name,
                  // "old_video_url" => $oldFile
            ], 200);
         }
      }

      return $this->errorResponse(null, "value not found");
      // return $this->successResponse($generalValue);
   }

   public function createAutoResponseData(Request $request, $appId)
   {
      $officeHour = GeneralValue::find($request->idOfficeHour);
   }

   public function getOfficeHour(Request $request, $appId)
   {
      $datas = OfficeHour::where('app_id', $appId)->orderBy('id', 'ASC')->get();
      if ($datas->isEmpty()) {
         $newOfficeHour = new OfficeHour;
         $newOfficeHour->app_id = $request->decodedAppId;
         $newOfficeHour->is_active = true;
         $newOfficeHour->days = 'Monday';
         $newOfficeHour->hour = json_encode([["start"=>"08:00","end"=>"17:00"]]);
         $newOfficeHour->created_by = $request->decodedUserId;
         $newOfficeHour->updated_by = $request->decodedUserId;
         $newOfficeHour->save();

         $newOfficeHour = new OfficeHour;
         $newOfficeHour->app_id = $request->decodedAppId;
         $newOfficeHour->is_active = true;
         $newOfficeHour->days = 'Tuesday';
         $newOfficeHour->hour = json_encode([["start"=>"08:00","end"=>"17:00"]]);
         $newOfficeHour->created_by = $request->decodedUserId;
         $newOfficeHour->updated_by = $request->decodedUserId;
         $newOfficeHour->save();

         $newOfficeHour = new OfficeHour;
         $newOfficeHour->app_id = $request->decodedAppId;
         $newOfficeHour->is_active = true;
         $newOfficeHour->days = 'Wednesday';
         $newOfficeHour->hour = json_encode([["start"=>"08:00","end"=>"17:00"]]);
         $newOfficeHour->created_by = $request->decodedUserId;
         $newOfficeHour->updated_by = $request->decodedUserId;
         $newOfficeHour->save();

         $newOfficeHour = new OfficeHour;
         $newOfficeHour->app_id = $request->decodedAppId;
         $newOfficeHour->is_active = true;
         $newOfficeHour->days = 'Thursday';
         $newOfficeHour->hour = json_encode([["start"=>"08:00","end"=>"17:00"]]);
         $newOfficeHour->created_by = $request->decodedUserId;
         $newOfficeHour->updated_by = $request->decodedUserId;
         $newOfficeHour->save();

         $newOfficeHour = new OfficeHour;
         $newOfficeHour->app_id = $request->decodedAppId;
         $newOfficeHour->is_active = true;
         $newOfficeHour->days = 'Friday';
         $newOfficeHour->hour = json_encode([["start"=>"08:00","end"=>"17:00"]]);
         $newOfficeHour->created_by = $request->decodedUserId;
         $newOfficeHour->updated_by = $request->decodedUserId;
         $newOfficeHour->save();

         $newOfficeHour = new OfficeHour;
         $newOfficeHour->app_id = $request->decodedAppId;
         $newOfficeHour->is_active = true;
         $newOfficeHour->days = 'Saturday';
         $newOfficeHour->hour = json_encode([["start"=>"08:00","end"=>"17:00"]]);
         $newOfficeHour->created_by = $request->decodedUserId;
         $newOfficeHour->updated_by = $request->decodedUserId;
         $newOfficeHour->save();

         $newOfficeHour = new OfficeHour;
         $newOfficeHour->app_id = $request->decodedAppId;
         $newOfficeHour->is_active = true;
         $newOfficeHour->days = 'Sunday';
         $newOfficeHour->hour = json_encode([["start"=>"08:00","end"=>"17:00"]]);
         $newOfficeHour->created_by = $request->decodedUserId;
         $newOfficeHour->updated_by = $request->decodedUserId;
         $newOfficeHour->save();

         $getNewData = OfficeHour::where('app_id', $appId)->get();
         $data = [];
         foreach ($getNewData as $key => $value) {
            $value->hour = json_decode($value->hour);
            $data[$key] = $value;
         }
         return $this->successResponse($data);

      } else {

         $data = [];
         foreach ($datas as $key => $value) {
            $value->hour = json_decode($value->hour);
            $data[$key] = $value;
         }
         return $this->successResponse($data);

      }

   }

   public function updateOfficeHour(Request $request, $appId)
   {
      $data = $request->data;
      foreach ($data as $key => $row) {
         $officeHour = OfficeHour::find($row['id']);
         $officeHour->hour = json_encode($row['hour']);
         $officeHour->is_active = $row['is_active'];
         $officeHour->updated_by = $request->decodedUserId;
         $officeHour->save();
      }
      return $this->successResponse(null, 'Updated');
   }


}
