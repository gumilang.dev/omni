<?php

namespace App\Modules\AutoResponseOfficeHour\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Days extends Model
{
    // use SoftDeletes;
    // protected $connection = 'pgsql';

    protected $guarded = [];

    public function operationalHours()
    {
      return $this->hasMany('App\Modules\AutoResponse\Models\OperationalHours', 'day_id', 'id');
    }
    // public function App()
    // {
    //     return $this->belongsTo("App\Modules\App\Models\App");
    // }
}
