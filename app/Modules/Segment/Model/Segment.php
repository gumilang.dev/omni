<?php

namespace App\Modules\Segment\Model;

use Illuminate\Database\Eloquent\Model;

class Segment extends Model
{
    //
    protected $fillable = ["name","query","app_id",'slug'];
}
