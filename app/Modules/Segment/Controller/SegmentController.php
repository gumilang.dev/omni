<?php

namespace App\Modules\Segment\Controller;

use App\Http\Controllers\ApiController;
use App\Services\SegmentServiceImpl;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SegmentController extends ApiController
{
    private $service;

    public function __construct(SegmentServiceImpl $service) {
        $this->service = $service;
    }

    public function show(Request $request, $appId, $id) {
        $response = $this->service->show($appId,$id);
        return response()->json($response);
    }

    public function showAll(Request $request, $appId, $id) {
        $response = $this->service->showAll($appId,$id);
        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$appId)
    {
        $data = [
            'name' => $request->input('name'),
            'query' => $request->input('query'),
            'app_id' => $appId,
            'slug' => Str::slug($request->input('name'), '-')
        ];
        $response = $this->service->store($data);
        return $this->successResponse($response,'Segment Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Segment  $segment
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $appId)
    {
        $response = $this->service->index(['app_id' => $appId]);
        return $this->successResponse($response,'Segment fetched');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Segment  $segment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$appId)
    {
        $data = [
            'name' => $request->input('name'),
            'query' => $request->input('query'),
        ];

        $response = $this->service->update(['id' => $request->post("id")],$data);
        return $this->successResponse($response,'Segment edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Segment  $segment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$appId)
    {
        $id = $request->post("id");
        $response = $this->service->destroy(['id' => $id]);
        return $this->successResponse($response,'Segment deleted');
    }
}
