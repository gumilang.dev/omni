<?php
namespace App\Modules\BlockUser\Services;

use App\Modules\BlockUser\Repositories\BlockUserRepo;
use App\Contract\Services\IBlockUserService;
use Cache;
use Carbon\Carbon;
use Config;

class BlockUserService implements IBlockUserService{
  const BLOCKED_USER = "BLOCKED_USER";
  public function __construct(
    BlockUserRepo $blockUserRepo
  ) {
    $this->blockUserRepo = $blockUserRepo;
  }

  public function block($data) {
    return $this->blockUserRepo->store($data);
  }

  public function unblock($param) {
    return $this->blockUserRepo->destroy($param);
  }

  public function getBlockedUser($request,$whereClause,$search) {
    return $this->blockUserRepo->paginate($request,$whereClause,$search);
  }

  public function isBlocked($email, $app_id) {
    // $config_minutes = config('cache.general_expiry_minutes');
		// $expiresAt = Carbon::now('Asia/Jakarta')->addMinutes($config_minutes);
    $data = $this->blockUserRepo->findBy(["email" => $email, "app_id" => $app_id]);
    return count($data) > 0 ? true : false;
  }

}
