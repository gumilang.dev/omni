<?php

namespace App\Modules\BlockUser\Models;

use Illuminate\Database\Eloquent\Model;

class BlockUser extends Model
{
    protected $table = "blocked_user";
    protected $fillable = ["app_id","email"];
}
