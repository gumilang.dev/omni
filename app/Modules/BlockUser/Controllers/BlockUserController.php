<?php namespace App\Modules\BlockUser\Controllers;

use Illuminate\Http\Request;
use App\Modules\BlockUser\Services\BlockUserService;
use App\Http\Controllers\ApiController;

class BlockUserController extends ApiController
{
  public function __construct(
    BlockUserService $blockUserService
  ) {
    $this->blockUserService = $blockUserService;
  }

  public function getBlockedUser(Request $request,$appId) {
    try {
      $paginatedBlockedUser = $this->blockUserService->getBlockedUser(
          $request,
          ["app_id" => $appId],
          ["app_id"]
      );
      return response()->json($paginatedBlockedUser);
    }catch(Exception $e) {
      return $this->errorResponse([],"Error while fetching data blocked user");
    }
  }

  public function block(Request $request,$appId) {
    try {
      $data = [
        "app_id" => $appId,
        "email" => $request->email
      ];
      $blockUser = $this->blockUserService->block($data);
      return $this->successResponse([],"user blocked");
    }catch(Exception $e) {
      return $this->errorResponse([],"Error while block user please try again");
    }
  }

  public function unblock(Request $request,$appId) {
    $email = $request->input("email");
    try {
      $data = [
        "email" => $email
      ];
      $blockUser = $this->blockUserService->unblock($data);
      return $this->successResponse([],"user unblocked");
    }catch(Exception $e) {
      return $this->errorResponse([],"Error while unblock user please try again");
    }
  }

  public function isUserBlocked(Request $request,$appId) {
    try {
      $data = [
        "email" => $request->email
      ];
      $isUserBlocked = $this->blockUserService->isBlocked($request->email, $appId);
      return $this->successResponse($isUserBlocked,"user is blocked");
    }catch(Exception $e) {
      return $this->errorResponse([],"Error while unb lock user please try again");
    }
  }
}
