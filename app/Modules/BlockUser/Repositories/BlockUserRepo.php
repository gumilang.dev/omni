<?php
namespace App\Modules\BlockUser\Repositories;

use App\Repositories\BaseRepository;
use App\Modules\BlockUser\Models\BlockUser;

class BlockUserRepo extends BaseRepository{

  public function __construct(
    BlockUser $entity
  ) {
    $this->entity = $entity;
    $this->rules = [
      'email' => 'required',
      'app_id' => 'required'
    ];
  }

}
