<?php

namespace App\Modules\Accessibility\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Modules\User\Models\UserPlatform;
use App\Modules\User\Models\UserApp;
use App\Modules\Group\Models\AgentGroup;
// use App\Modules\User\Models\UserRole;
use App\Modules\Role\Models\RoleMenu;
use App\Modules\Menu\Models\Menu;
use App\Modules\Module\Models\Module;

use Hashids\Hashids;
use Carbon\Carbon;

use App\Modules\Role\Helpers\RoleHelper;

class AccessibilityController extends ApiController
{
    public function __construct()
    {
        $this->hashids = new Hashids('', 6);
    }

    public function index(Request $request, $appId)
    {
      $role = UserApp::select('id','role_id')->where('user_id', $request->decodedUserId)->where('app_id', $appId)->first();

      if(!$role){
        return $this->errorResponse(null, 'You need authorized for this app, contact your admin');
      }
      // $menus = RoleHelper::menuPrivileges($role->role_id, $appId);
      $modules = RoleHelper::modulePrivileges($role->role_id);
      if (!$modules) {
        return $this->errorResponse(null, 'Role for this user not found');
      }
      $apps = UserApp::where('user_id', $request->decodedUserId)->get();
      $groups  = AgentGroup::where("user_id" ,$request->decodedUserId)->get();
      $apps = $apps->map(function ($value) {
        return [
          'id' => $this->hashids->encode($value->app_id)
        ];
      });

      return response()->json([
        'success' => true,
        'error'   => null,
        'message' => 'Success get accessibility',
        'data'    => [
          // 'menu'       => $menus,
          'module'       => $modules,
          'apps'       => $apps,
          'groups' => $groups
        ]
      ]);
    }

    public function store(Request $request, $appId)
    {
      $this->validate($request, [
        'email' => 'required|email',
      ]);

      $user_platform = UserPlatform::where('email', $request->email)->first();
      if (!$user_platform) {
        return $this->errorResponse(null, 'User Not Registered');
      }

      $check_user_app = UserApp::where('user_id', $user_platform->id)->where('app_id', $appId)->first();
      if (!$check_user_app) {
        $user_app = new UserApp;
        $user_app->user_id = $user_platform->id;
        $user_app->app_id = $appId;
        $user_app->save();

        // $user = UserPlatform::where('id', $user_app->user_id)->with(['apps' => function($q) use ($appId){
        //   $q->where('app_id', $appId);
        // }])->get();
        // $user = $user->filter(function ($value, $key) {
        //   return count($value['apps']) > 0;
        // })->values()->map(function ($value, $key) {
        //   $map = (object)[
        //       'id' => $value['hashed_id'],
        //       'email' => $value['email'],
        //       'name' => $value['name']
        //   ];
        //   return collect($map)->except([
        //     'email_verified_at',
        //     'created_at',
        //   ])->toArray();
        // })->all();

        return $this->successResponse(null, 'User have been given access to this app');
      }
      return $this->errorResponse(null, 'User already have access to this app');
    }

    public function deleteAccess(Request $request, $appId)
    {
      $decodedId = $this->hashids->decode($request->id);
      // return $decodedId;
      $user_app = UserApp::where('user_id', $decodedId)->where('app_id', $appId)->update([
        'deleted_at' => Carbon::now()->toDateTimeString()
      ]);
      return $this->successResponse(null, 'User have been given access to this app');
    }

    public function checkAccess(Request $request, $appId)
    {
      $user_role = UserApp::where("app_id", $appId)->where("user_id", $request->decodedUserId)->first();
      if(!$user_role || $user_role->role_id == NULL){
        return response()->json([
          'success' => false,
          'error'   => null,
          'message' => 'No Access'
        ]);
      }
      if($user_role->role_id == 1 || $user_role->role_id == 2){
        return $this->successResponse([
          "role_id" => $this->hashids->encode($user_role->role_id),
        ], 'Have Access');
      }
      return $this->errorResponse(null, 'No Access');
    }

    public function getRole(Request $request, $appId)
    {
      // $userId = $this->hashids->decode($request->header('userId'))[0];
      $role = UserApp::with('role')->where("app_id", $appId)->where("user_id", $request->decodedUserId)->first();
      $role = $role->role;
      // $role = (object)[
      //   'id' => $role->hashed_id,
      //   'role' => $role->name,
      // ];
      return response()->json($role);
    }
}
