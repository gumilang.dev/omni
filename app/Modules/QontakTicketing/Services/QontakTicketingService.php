<?php

namespace App\Modules\QontakTicketing\Services;
use App\Shared\Traits\Publisher;
use Illuminate\Support\Facades\Cache;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use App\Modules\Message\Models\Message;

class QontakTicketingService
{

    use Publisher;

    protected $cache_key = "qontak_ticket_room_id__";

    public function isTicketAlreadyCreated($room_id)
    {
        $is_exist = Cache::get($this->cache_key . "" . $room_id);
        if (is_null($is_exist)) return false;
        return true;
    }

    public function createTicketInCache($room_id)
    {
        return Cache::put($this->cache_key . "" . $room_id, $room_id);
    }

    public function updateTicketInCache($room_id)
    {
        return Cache::forget($this->cache_key . "" . $room_id);
    }

    public function createTicket($data, $room_id)
    {
        $is_ticket_already_created = $this->isTicketAlreadyCreated($room_id);

        if (!$is_ticket_already_created)
        {
            try
            {
                $this->publish("rabbitmq")
                    ->setExchange("direct")
                    ->setQueue("create")
                    ->setExchangeType(AMQPExchangeType::DIRECT)
                    ->publish(json_encode($data));
                \Log::debug("success create titkcet");
                $this->createTicketInCache($room_id);
            }
            catch(\Exception $e)
            {
                \Log::debug("error publishing create ticket".$e->getMessage());
            }
        }
        else
        {
        }

    }

    public function extractMessageContent($content) {
      $new_content = "";
      foreach ($content as $each_content) {
        if($each_content->type == "text") {
          $new_content .="<div class='comment-text'>$each_content->text</div>";
        }else if($each_content->type == "image") {
          $new_content .="
                        <div class='comment-attachment img'>
                            <a href='$each_content->previewImageUrl'></a>
                        </div>";
        }else if($each_content->type == "file") {
          $new_content .="
                        <div class='comment-attachment file'>
                            <a href='$each_content->fileUrl'></a>
                            <div class='caption'>$each_content->fileName</div>
                        </div>";
        }
      }
      return $new_content .= "";
    }

    public function getHistoryChat($room_id, $start_at, $end_at)
    {
        $format_message = "";
        $format_message .= "<div class='qon-comments'>";
        $messages = Message::where("room_id",$room_id)
        ->where('created_at', '>=', $start_at)
        ->where('created_at', '<=', $end_at)
        ->get();

        foreach ($messages as $message)
        {
            $name = $message
                ->messageable->name;
            $content = $this->extractMessageContent($message->content);
            if ($message->messageable_type == "user")
            {
                $format_message .= "<div class='qon-comment comment-me'>
                    <div class='qon-comment-container'>
                      <div class='qon-comment-sender'>
                            <span class='name'>Send by $name</span>
                        </div>
                        <div class='qon-comment-body'>
                            <div class='comment-date'>$message->created_at</div>
                            $content;
                        </div>
                    </div>
                </div>";
            }
            else
            {
                $format_message .= "<div class='qon-comment comment-sender'>
            <div class='qon-comment-container'>
                <div class='qon-comment-sender'>
                     <span class='name'>Send by $name</span>
                </div>
                <div class='qon-comment-body'>
                    <div class='comment-date'>$message->created_at</div>
                    $content
                </div>
            </div>
        </div>";
            }
        }

        $format_message .= "</div>";
        return $format_message;
    }

    public function updateTicket($data, $room_id)
    {
        $is_ticket_already_created = $this->isTicketAlreadyCreated($room_id);

        if ($is_ticket_already_created)
        {
            try
            {
                $this->publish("rabbitmq")
                    ->setExchange("direct")
                    ->setQueue("update")
                    ->setExchangeType(AMQPExchangeType::DIRECT)
                    ->publish(json_encode($data));
                $this->updateTicketInCache($room_id);
            }
            catch(\Exception $e)
            {
            }
        }
    }
}

