<?php namespace App\Modules\Session\Models;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
   protected $table = "session";
   protected $fillable = ["room_id","handle_by","start_at","end_at","user_id"];
}
