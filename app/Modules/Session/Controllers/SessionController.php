<?php namespace App\Modules\Session\Controllers;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Services\SessionServiceImpl;

class SessionController extends ApiController
{
   public function __construct(SessionServiceImpl $service) {
     $this->service = $service;
   }

   public function create(Request $request, $appId) {
      $data = $this->service->transform($request);
      $response = $this->service->createSession($data);
      if($response["error"]) {
        return $this->errorResponse($response["message"],"Submit ticket failed",400);
      }else{
        return $this->successResponse($response,"Session Created");
      }
   }
}
