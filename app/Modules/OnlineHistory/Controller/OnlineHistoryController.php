<?php namespace App\Modules\OnlineHistory\Controller;

use Illuminate\Http\Request;
use App\Services\OnlineHistoryServiceImpl;

use App\Http\Controllers\ApiController;

class OnlineHistoryController extends ApiController
{
    public function __construct(OnlineHistoryServiceImpl $service) {
      $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$appId)
    {
      $data = [
        "app_id" => $appId,
        "status" => $request->status,
        "user_id" => $request->user_id,
        "start_at" => $request->start_at,
        "end_at" => $request->end_at,
        "reason_offline" => $request->reason_offline
      ];
      return $this->service->createHistory($data);
    }

}
