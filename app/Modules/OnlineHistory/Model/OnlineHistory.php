<?php

namespace App\Modules\OnlineHistory\Model;

use Illuminate\Database\Eloquent\Model;

class OnlineHistory extends Model
{
    protected $fillable = ["status","start_at","end_at","reason_offline","user_id","app_id"];
}
