<?php

namespace App\Modules\Room\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use App\Modules\Message\Models\Message;
use App\Modules\User\Models\User;
use App\Modules\Livechat\Models\Livechat;
use App\Modules\Common\Services\EncryptService;
use App\Modules\Channel\Models\Channel;
use App\Modules\Integration\Models\Integration;
use DB;

Relation::morphMap([
    'user' => 'App\Modules\User\Models\User',
    'user_platform' => 'App\Modules\User\Models\UserPlatform',
]);

class Room extends Model
{
    protected $connection = 'pgsql';
    // protected $fillable   = [];
    protected $guarded    = [];
    protected $appends    = ['participants','livechat','messages','channel','is_blocked', 'integration'];

  // M to M polymorph relationship
    public function users()
    {
        return $this->morphedByMany(
            'App\Modules\User\Models\User',
            'participable',
            env('DB_SCHEMA') . ".participables"
        );
    }
    public function userPlatform()
    {
        return $this->morphedByMany(
            'App\Modules\User\Models\UserPlatform',
            'participable',
            env('DB_SCHEMA') . ".participables"
        );
    }
  // 1 to M
    public function messages()
    {
        return $this->hasMany('App\Modules\Message\Models\Message')->orderBy('id');
    }
    public function liveChats()
    {
        return $this->hasMany('App\Modules\Livechat\Models\Livechat');
    }
    public function channel()
    {
        return $this->belongsTo('App\Modules\Channel\Models\Channel');
    }
    public function messageStatus()
    {
        return $this->hasManyThrough('App\Modules\Message\Models\MessageStatus', 'App\Modules\Message\Models\Message');
    }

  // accessor
    public function getMessagesAttribute()
    {
        $msgs = Message::where('room_id', $this->attributes['id'])->orderBy('id', 'desc')->take(1)->get();
        // foreach ($msgs as $item => $value) {
        //   if ($value->encrypt) {
        //     $decodedMessage = EncryptService::decrypt($value->content, env('ENCRYPT_MESSAGE_KEY'), 20);
        //     $value->content = json_decode($decodedMessage);
        //     return array($value);
        //   } else {
        //     return array($value);
        //   }
        // }

        // $msgs = array($this->encryptMessage($item->messages[0]));
        $msgs = $msgs->sortBy('id');
        return $msgs->values()->all();
    }
    public function getIntegrationAttribute()
    {
        $user = User::where('id', $this->attributes['created_by'])->first();

        if ($user['integration_id'] == null) {
            return [];
        }

        $integration = Integration::where('id', $user['integration_id'])->first();

        if (!$integration['integration_data']) {
            return [];
        }
        $integration = $integration['integration_data'];
        $listToShow = ['whatsappName', 'whatsappNumber'];
        $integration = array_only($integration, $listToShow);
        return $integration;
    }
    public function getCreatedByAttribute()
    {
        $user = User::where('id', $this->attributes['created_by'])->with('channel')->first();
        return $user;
    }

    public function getLivechatAttribute()
    {
        // $user = Livechat::where('room_id', $this->attributes['id'])->where('status', '!=', 'resolved')->orderBy('id', "DESC")->first();
        $user = Livechat::where('room_id', $this->attributes['id'])->orderBy('id', "DESC")->first();
        return $user;
    }

    public function getChannelAttribute()
    {
      $data = Channel::where('id', $this->attributes['channel_id'])->first();
      return $data;
    }

    public function getIsBlockedAttribute() {
      $user = DB::table("app.users as au")
                ->join("omnichannel.blocked_user as ob",function($join) {
                  $join->on("ob.email","=","au.email");
                })
                ->where("au.id",$this->attributes['created_by'])
                ->select("au.id")
                ->get();
      if(count($user) > 0) return true;
      return false;
    }

    public function getParticipantsAttribute()
    {
        $user = $this->users()->get()->toArray();
        $userPlatform = $this->userPlatform()->get()->toArray();
        $merged = array_merge($userPlatform, $user);
        return $merged;
    }
    public static function getUnreadCount($roomId, $user)
    {
        $unreadCount = Self::find($roomId)->messages()->whereHas('messageStatus', function ($q) use ($user) {
            $q->where('messageable_type', '=', $user['type'])
              ->where('messageable_id', '=', $user['id'])
              ->where('read_at', '=', null);
        })->count();
        return $unreadCount;
    }

    public function tags() {
        return $this->belongsToMany('App\Modules\Tag\Model\Tag')
        ->withPivot('id');
    }

    public function additional_informations()
    {
        return $this->hasMany('App\Modules\AdditionalInformation\Model\AdditionalInformation')->orderBy('id');
    }

    public function notes()
    {
        return $this->hasOne('App\Modules\Notes\Model\Notes')->orderBy('id');
    }

    public function app()
    {
       return $this->belongsTo('App\Modules\App\Models\App');
    }

}
