<?php namespace App\Modules\Room\Response;

class RoomResponse {

  public $unreadCount;
  public $id;
  public $app_id;
  public $createdAt;
  public $updatedAt;
  public $created_by;
  public $channel;
  public $notes;
  public $additionalInformations;
  public $livechat;
  public $messages;
  public $tags;

}
