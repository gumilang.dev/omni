<?php namespace App\Modules\Room\Request;

class GetAllRoomRequest {

  public $appId;
  public $page;
  public $status;
  public $search;
  public $channel;
  public $isAgent;
  public $perPage;

  public function __construct($appId, $page,$status,$search,$channel,$isAgent,$perPage) {
    $this->appId = $appId;
    $this->page = $page;
    $this->status = $status;
    $this->search = $search;
    $this->channel = $channel;
    $this->isAgent = $isAgent;
    $this->perPage = $perPage;
  }


}
