<?php namespace App\Modules\Room\Interfaces;

interface RoomRepositoryInterface  {
  public function get();
  public function create();
  public function getById();
  public function update();
  public function delete();
}
