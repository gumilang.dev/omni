<?php

namespace App\Modules\Room\Controllers;

use App\Modules\Room\Models\Room;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use App\Modules\User\Models\UserPlatform;

class RoomController extends Apicontroller
{

    public function getRoomList(Request $request, $appId)
    {
        $room = Room::where('app_id', $appId)->with(['channel','tags'])->whereHas('users', function ($query) use ($request) {
            $query->where('deleted_at', '=', null);
        })->orderBy("updated_at", "DESC")->limit(20)->get();
        $user = [
            'id' => $request->decodedUserId,
            'type' => 'user_platform'
        ];

        $room = $room->map(function ($item, $key) use ($user) {
            $unreadCount = Room::getUnreadCount($item->id, $user);
            $item->unread_count = $unreadCount;
            $item->agent_info = $item->livechat == null ? null : $this->getAgentInfo($item->livechat["handle_by"]);
            return $item;
        });

        return $this->successResponse([
            'room_list' => $room
        ]);

    }

    public function getNotif($room_id, $data) {
      $response = Room::with(['messageStatus'])->where("id",$room_id)->first();
      $message = $response["messageStatus"];
      $unreadCount = [];

      foreach($message as $key => $value) {
        if($value['read_at'] == null && $value["messageable_type"] == $data["type"] && strtotime($value["created_at"]) >= strtotime("2020-05-24 14:55:37")) {
          array_push($unreadCount,$message[$key]);

        }
      }

      return count($unreadCount);
    }


    public function getMoreRoom(Request $request, $appId)
    {
        // return $request->all();
        $rooms = Room::where('app_id', $appId)->with(['channel','tags'])->whereHas('users', function ($query) use ($request) {
            $query->where('deleted_at', '=', null);
        })->where('updated_at', '<', $request->firstRoomUpdatedAt)->orderBy('updated_at', 'desc')->limit(20)->get();
        $user = [
            'id' => $request->decodedUserId,
            'type' => 'user_platform'
        ];

        $rooms = $rooms->map(function ($item, $key) use ($user) {
            $unreadCount = Room::getUnreadCount($item->id, $user);
            $item->unread_count = $unreadCount;
            $item->agent_info = $item->livechat == null ? null : $this->getAgentInfo($item->livechat["handle_by"]);
            return $item;
        });

        // $rooms = Room::where('app_id', $appId)->where('id', '<', $request->firstRoomId)->orderBy('id', 'desc')->take(10)->get();
        // $rooms = $rooms->sortBy('id');

        return $this->successResponse([
            'room_list' => $rooms
        ]);
    }

    public function getAgentInfo($agentInfo) {
      $user = UserPlatform::find($agentInfo);
      return $user;
    }

    public function getRoomListPaginate(Request $request, $appId)
    {
        $sort = $request->get('sort');
        $name = $request->get('name');
        $channel = $request->get('channel');
        $per_page = $request->get('per_page');
        $status = $request->get('status');

        $room = Room::where('app_id', $appId)->with(['channel','tags','additional_informations','notes'])->whereHas('users', function ($query) use ($request) {
          $query->where('deleted_at', '=', null);
        })->orderBy("updated_at", "DESC")->get();

        $user = [
            'id' => $request->decodedUserId,
            'type' => 'user_platform'
        ];

        $room = $room->map(function ($item, $key) use ($user,$request) {
            // $unreadCount = Room::getUnreadCount($item->id, $user);
            $unreadCount = $this->getNotif($item->id, [
              "id" => $request->decodedUserId,
              "type" => "user"
            ]);
            $item->unread_count = $unreadCount;
            $item->agent_info = $item->livechat == null ? null : $this->getAgentInfo($item->livechat["handle_by"]);
            return $item;
        });

        if (!$per_page) {
          $per_page = 10;
        }

        if ($channel) {
          // $channel = explode(', ', $channel);
          $room = $room->whereIn('created_by.channel.name', $channel)->values();
        }

        if ($status) {
          // if ($status == '') {
          //   $room = $room->where('livechat', '!=', null)->values();
          // } else {
            $room = $room->filter(function ($item) use ($status) {
              return false !== stristr($item->livechat['status'], $status);
            })->values();
          // }
        }

        if ($name) {
          $room = $room->filter(function ($item) use ($name) {
              // replace stristr with your choice of matching function
              return false !== stristr($item->created_by->name, $name);
          })->values();
        }

        $data = $this->paginate($room, $per_page);

        $data->appends($request->only(['channel','status','name']));

        return response()->json($data);
    }

    public function paginate($items, $perPage, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage)->values(), $items->count(), $perPage, $page, [
          'path' => LengthAwarePaginator::resolveCurrentPath()
        ]);
    }

    public function totalUnservedRoom(Request $request, $appId)
    {
      $room = Room::where('app_id', $appId)->whereHas('users', function ($query) {
        $query->where('deleted_at', '=', null);
      })->get();
      $room = $room->where('livechat', '!=', null)->where('livechat.status', 'request')->values();
      return response()->json(count($room));
    }
}
