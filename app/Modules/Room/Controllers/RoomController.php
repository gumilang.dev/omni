<?php
namespace App\Modules\Room\Controllers;
ini_set('memory_limit', '-1');

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

use App\Modules\Room\Models\Room;
use App\Modules\User\Models\UserPlatform;

use App\Http\Controllers\ApiController;


use App\Modules\Role\Helpers\RoleHelper;
use App\Services\ParserService;
use App\Modules\Common\Services\EncryptService;
use Illuminate\Support\Arr;

class RoomController extends Apicontroller
{
    protected $parser;
    public function getRoomList(Request $request, $appId)
    {
        $room = Room::where('app_id', $appId)->with(['channel','tags'])->whereHas('users', function ($query) use ($request) {
            $query->where('deleted_at', '=', null);
        })->orderBy("updated_at", "DESC")->limit(20)->get();
        $user = [
            'id' => $request->decodedUserId,
            'type' => 'user_platform'
        ];

        $room = $room->map(function ($item, $key) use ($user) {
            $unreadCount = Room::getUnreadCount($item->id, $user);
            $item->unread_count = $unreadCount;
            $item->agent_info = $item->livechat == null ? null : $this->getAgentInfo($item->livechat["handle_by"]);
            return $item;
        });

        return $this->successResponse([
            'room_list' => $room
        ]);

    }

    public function getNotif($room_id, $data) {
      $response = Room::with(['messageStatus'])->where("id",$room_id)->first();
      $message = $response["messageStatus"];
      $unreadCount = [];

      foreach($message as $key => $value) {
        if($value['read_at'] == null && $value["messageable_type"] == $data["type"] && strtotime($value["created_at"]) >= strtotime("2020-05-24 14:55:37")) {
          array_push($unreadCount,$message[$key]);

        }
      }

      return count($unreadCount);
    }


    public function getMoreRoom(Request $request, $appId)
    {
        // return $request->all();
        $rooms = Room::where('app_id', $appId)->with(['channel','tags'])->whereHas('users', function ($query) use ($request) {
            $query->where('deleted_at', '=', null);
        })->where('updated_at', '<', $request->firstRoomUpdatedAt)->orderBy('updated_at', 'desc')->limit(20)->get();
        $user = [
            'id' => $request->decodedUserId,
            'type' => 'user_platform'
        ];

        $rooms = $rooms->map(function ($item, $key) use ($user) {
            $unreadCount = Room::getUnreadCount($item->id, $user);
            $item->unread_count = $unreadCount;
            $item->agent_info = $item->livechat == null ? null : $this->getAgentInfo($item->livechat["handle_by"]);
            return $item;
        });

        // $rooms = Room::where('app_id', $appId)->where('id', '<', $request->firstRoomId)->orderBy('id', 'desc')->take(10)->get();
        // $rooms = $rooms->sortBy('id');

        return $this->successResponse([
            'room_list' => $rooms
        ]);
    }

    public function getAgentInfo($agentInfo) {
      $user = UserPlatform::find($agentInfo);
      return $user;
    }

    public function getRoomListTest(Request $request,$appId) {
      $sort = $request->get('sort');
      $name = $request->get('name');
      $channel = $request->get('channel');
      $per_page = $request->get('per_page');
      $status = $request->get('status');
      $lastRoom = $request->get('lastRoom');
      $custom_filter  = json_decode($request->get("custom_filter"));

      if (!$per_page) {
        $per_page = 7;
      }
      $room = DB::select(DB::raw("
        SELECT r.id as room_id,r.created_by as user_id,u.name, r.created_at as created_at, r.updated_at as updated_at, u.nickname as nickname ,u.phone as phone  FROM omnichannel.rooms as r
        INNER JOIN app.users as  u
        ON r.created_by = u.id
        WHERE r.app_id = 6
        ORDER BY r.updated_at DESC
        LIMIT 3
      "));
      // $converted = json_decode($room[0]->json_agg,0);
      return $this->successResponse($room);
    }
    public function getRoomListNewOptimize(Request $request,$appId) {
      $sort = $request->get('sort');
      $name = $request->get('name');
      $channel = $request->get('channel');
      $per_page = $request->get('per_page');
      $status = $request->get('status');
      $custom_filter  = json_decode($request->get("custom_filter"));

      if (!$per_page) {
        $per_page = 7;
      }

      $room = DB::table("omnichannel.rooms as r")
              ->leftJoin("omnichannel.livechats as l",function($join) {
                $join->on("r.id","=","l.room_id");
              })
              ->join("omnichannel.channels as c",function($query) {
                $query->on("r.channel_id","=","c.id");
              })
              ->leftJoin("omnichannel.notes as n",function($join){
                $join->on("r.id","=","n.room_id");
              })

              ->join("app.users as u",function($join) {
                $join->on("u.id","=","r.created_by");
              })
              ->leftJoin("omnichannel.blocked_user as bu",function($join) use($appId) {
                $join->on("bu.email","=","u.email");
                $join->on("bu.app_id","=","r.app_id");
              })
              ->leftJoin("omnichannel.integrations as i",function($query) {
                $query->on("i.id","=","u.integration_id");
              })
              ->addSelect(DB::raw("
                (
                  select count(m.id)
                  from omnichannel.messages m INNER JOIN omnichannel.message_statuses om ON m.id = om.message_id WHERE om.read_at is null and om.messageable_type = 'user' and om.created_at >= '2020-10-24 14:55:37' and m.room_id = r.id
                ) as unread_count
              "))
              ->addSelect("r.id","r.created_at","r.updated_at")
              ->addSelect(DB::raw("
                json_build_object(
                  'id', u.id,
                  'name', u.name,
                  'nickname', u.nickname,
                  'phone', u.phone,
                  'city', u.city,
                  'email', u.email,
                  'fcm_token', u.fcm_token
                ) as created_by
              "))
              ->addSelect(DB::raw("
                json_build_object(
                  'id', c.id,
                  'name', c.name
                ) as channel
              "))
              ->addSelect(DB::raw("
                (
                  CASE
                    when n.id is null
                    then null
                    else (
                      json_build_object(
                        'id', n.id,
                        'content', n.content,
                        'room_id', n.room_id
                      )
                    )
                  END
                ) as notes
              "))
              ->addSelect(DB::raw("
                  (
                    SELECT json_agg(
                      json_build_object(
                        'id', ai.id,
                        'room_id', ai.room_id,
                        'title', ai.title,
                        'description', ai.description
                      )
                    ) FROM omnichannel.additional_informations as ai WHERE ai.room_id = r.id
                  ) as additional_informations
              "))
              ->addSelect(DB::raw("
                  (
                    case
                      when l.status is null
                      then null
                      else (
                        json_build_object(
                          'id', l.id,
                          'room_id', l.room_id,
                          'handle_by', l.handle_by,
                          'updated_by', l.updated_by,
                          'start_at',l.start_at,
                          'status', l.status,
                          'end_at', l.end_at,
                          'request_at',l.request_at,
                          'group_id', l.group_id
                        )
                      )
                    END
                  ) as livechat
                "))
              ->addSelect(DB::raw("
                (
                  SELECT
                    json_build_array (
                      json_build_object(
                        'id', m.id,
                        'channel_data', m.channel_data,
                        'room_id', m.room_id,
                        'messageable_type', m.messageable_type,
                        'messageable_id', m.messageable_id,
                        'content', m.content,
                        'data', m.data,
                        'deleted_at', m.deleted_at,
                        'created_at', m.created_at,
                        'updated_at', m.updated_at,
                        'encrypt', m.encrypt
                      )
                    )
                 FROM omnichannel.messages as m WHERE m.room_id = r.id ORDER BY m.id DESC LIMIT 1
                ) as messages
              "))
              ->addSelect(DB::raw("
                  (
                    json_build_object(
                      'id', i.id,
                      'whatsappNumber', i.integration_data->'whatsappNumber',
                      'whatsappName', i.integration_data->'whatsappName'
                  )
                ) as integration
              "))
              ->addSelect(DB::raw("
                 (
                  SELECT
                    json_agg (
                      json_build_object(
                        'id', tt.id,
                        'name', tt.name,
                        'pivot', json_build_object(
                          'id', rt.id,
                          'tag_id', rt.tag_id,
                          'room_id', rt.room_id
                        )
                      )
                    )
                 FROM omnichannel.room_tag as rt
                 INNER JOIN omnichannel.tags as tt ON rt.tag_id = tt.id
                 WHERE rt.room_id = r.id
                ) as tags
              "))
              ->addSelect(DB::raw("
                  case
                    when bu.id is null
                    then false
                    else true
                  end as is_blocked
              "))
              ->where("r.app_id",$appId)
              ->where(function($query)  use ($request, $appId, $status, $name,$channel) {
                if($status) {
                  if($status == "live") {
                    if (RoleHelper::isAgent(['userId' => $request->decodedUserId, 'appId' => $appId])) {
                      $query->where('l.status', 'live')->whereNull('l.end_at')->whereNotNull('l.request_at')->where('l.handle_by', $request->decodedUserId);
                    } else {
                      $query->where('l.status', 'live')->whereNull('l.end_at')->whereNotNull('l.request_at');
                    }
                  }

                  if($status == "resolved") {
                    if (RoleHelper::isAgent(['userId' => $request->decodedUserId, 'appId' => $appId])) {
                      $query->where('l.status', 'resolved')->where('l.request_at', '!=', NULL)->where('l.handle_by', $request->decodedUserId);
                    } else {
                      $query->where('l.status', 'resolved')->where('l.request_at', '!=', NULL);
                    }
                  }

                  if($status == "request"){
                    $query->where('l.status', 'request')->where('l.request_at', '!=', NULL)->where('l.start_at', NULL);
                  }

                  if($status == "bot") {
                    $query->where('l.status', null);
                  }
                }
              })
              ->where(function($q) use($name) {
                if($name) {
                  $q->where("u.name","ilike","%".$name."%")->orWhere("u.email","ilike","%".$name."%")->orWhere("u.phone","ilike","%".$name."%");
                }
              })
              ->where(function($query) use ($channel) {
                  if($channel) {
                    $query->whereIn('c.name', $channel);
                  }
              })
              ->where(function($query) use ($custom_filter) {
                if(is_array($custom_filter) && count($custom_filter) > 0) {
                  $data = [];
                  foreach($custom_filter as $x) {
                    array_push($data,["column" => $x->column ,"condition" => $this->changeCondition($x->condition), "value" => $this->changeValue($x)]);
                  }
                  foreach($data as $value) {
                    if($value["column"] == "note") {
                        $query->where("n.content",$value["condition"],$value["value"]);
                    }else if($value["column"] == "date created") {
                      if($value["condition"] == "single") {
                         $query->whereDate("r.updated_at",$value["value"]);
                      }
                    }else {
                      $query->where("u.name","ilike","%".$value["value"]."%")->orWhere("u.email","ilike","%".$value["value"]."%")->orWhere("u.phone","ilike","%".$value["value"]."%");
                    }

                  }
                }
              })
              ->orderBy("r.updated_at","desc")
              ->paginate($per_page)
              ->map(function($room) {
                  $room->messages = json_decode($room->messages);
                  $room->channel =  json_decode($room->channel);
                  $room->livechat =  json_decode($room->livechat);
                  $room->created_by = json_decode($room->created_by);
                  $room->integration = json_decode($room->integration);
                  $room->notes = json_decode($room->notes);
                  $room->additional_informations = is_null($room->additional_informations) ? [] : json_decode($room->additional_informations);
                  $room->tags = is_null($room->tags) ? [] : json_decode($room->tags);
                return $room;
              });
      return $this->successResponse($room);
    }


    public function getRoomListNew(Request $request, $appId)
    {
      $sort = $request->get('sort');
      $name = $request->get('name');
      $channel = $request->get('channel');
      $per_page = $request->get('per_page');
      $status = $request->get('status');
      $lastRoom = $request->get('lastRoom');
      $custom_filter  = json_decode($request->get("custom_filter"));

      if (!$per_page) {
        $per_page = 7;
      }

      // $room = Room::take(5)->get();

      // return response()->json($room);

      $room = Room::where('app_id', $appId)->with(['channel','tags','additional_informations','notes']);

      if ($name) {
        $room = $room->whereHas('users', function ($q) use ($name) {
          $q->where('name', 'ilike', '%'.$name.'%')->orWhere('email','ilike','%'.$name.'%')->orWhere('phone','ilike','%'.$name.'%');
        });
      }

      $room = $room->whereHas("users");

      // custom filter
      if(is_array($custom_filter) && count($custom_filter) > 0) {
        $data = [];
        foreach($custom_filter as $x) {
          array_push($data,["column" => $x->column ,"condition" => $this->changeCondition($x->condition), "value" => $this->changeValue($x)]);
        }
        foreach($data as $value) {
          if($value["column"] == "note") {
            $room = $room->whereHas("notes",function($q) use($value) {
              $q->where("content",$value["condition"],$value["value"]);
            });
          }
          if($value["column"] == "tag") {
            $room = $room->whereHas("tags",function($q) use($value) {
              $q->where("name",$value["condition"],$value["value"]);
            });
          }
          if($value["column"] == "date created") {
            if($value["condition"] == "single") {
              $room = $room->whereDate("updated_at",$value["value"]);
            }
          }
          if($value["column"] == "phone") {
            $room = $room->whereHas("users",function($q) use($value) {
              $q->where("phone",$value["condition"],$value["value"])->orWhere("phone",$value["condition"],$value["value"]);
            });
          }
          if($value["column"] == "name") {
            $room = $room->whereHas("users",function($q) use($value) {
              $q->where("name",$value["condition"],$value["value"]);;
            });
          }
          if($value["column"] == "email") {
            $room = $room->whereHas("users",function($q) use($value) {
              $q->where("email",$value["condition"],$value["value"]);;
            });
          }
        }
      }


      if ($channel) {
        // $channel = explode(', ', $channel);
        $room = $room->whereHas('channel', function($q) use ($channel) {
          $q->whereIn('name', $channel);
        });
      }

      if ($status) {
        if($status == "bot") {
          $room = $room->doesnthave("liveChats");
        }else{
          $room = $room->whereHas('liveChats', function ($query) use ($status, $request, $appId) {
            if ($status == "live") {
              if (RoleHelper::isAgent(['userId' => $request->decodedUserId, 'appId' => $appId])) {
                $query->where('status', 'live')->whereNull('end_at')->whereNotNull('request_at')->where('handle_by', $request->decodedUserId);
              } else {
                $query->where('status', 'live')->whereNull('end_at')->whereNotNull('request_at');
              }
            }
            if ($status == "resolved") {
              if (RoleHelper::isAgent(['userId' => $request->decodedUserId, 'appId' => $appId])) {
                $query->where('status', 'resolved')->where('request_at', '!=', NULL)->where('handle_by', $request->decodedUserId);
              } else {
                $query->where('status', 'resolved')->where('request_at', '!=', NULL);
              }
            }
            if ($status == "request") {
              $query->where('status', 'request')->where('request_at', '!=', NULL)->where('start_at', NULL);
            }
          });
        }

      }

      if ($request->filled('lastRoom')) {
        $room = $room->where('updated_at', '<', $lastRoom);
      }

      $room = $room->limit($per_page)->orderBy('updated_at', 'DESC')->get();

      $user = [
        'id'    => $request->decodedUserId,
        'type'  => 'user_platform'
      ];

      $room = $room->map(function ($item, $key) use ($user, $request) {
        $unreadCount = $this->getNotif($item->id, [
          "id" => $request->decodedUserId,
          "type" => "user"
        ]);
        $item->unread_count = $unreadCount;
        $item->agent_info = $item->livechat == null ? null : $this->getAgentInfo($item->livechat["handle_by"]);
        return $item;
      });
      return $this->successResponse($room);
    }

    public function changeCondition($value) {
      if($value == "containts" || $value == "containt" || $value == "contain") {
        return "ilike";
      }elseif($value == "equals") {
        return "=";
      }elseif($value == "not equals") {
        return "!=";
      }else if($value == "not containts" || $value == "not containt" || $value == "not contain") {
        return "NOT LIKE";
      }else{
        return $value;
      }
    }

    public function changeValue($value) {
      if($value->condition == "containts" || $value->condition == "not containts") {
        return "%".$value->value."%";
      }else{
        return $value->value;
      }
    }

    public function paginate($items, $perPage, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage)->values(), $items->count(), $perPage, $page, [
          'path' => LengthAwarePaginator::resolveCurrentPath()
        ]);
    }

    public function getRoomByStatus($appId,$status,$params = []) {
      if($status == "bot") {
        return Room::where('app_id', $appId)->doesnthave('liveChats')->count();
      }else{
        return Room::where('app_id', $appId)->whereHas('liveChats', function ($q) use($status,$params) {
          if($status == "request") {
            $q->where('status', "request")->where('end_at', NULL)->where('request_at','!=',NULL);
          }
          if(count($params) < 1) {
            if($status == "live") {
              $q->where('status', 'live')->whereNull('end_at')->whereNotNull('request_at');
            }
            if($status == "resolved") {
              $q->where('status', 'resolved')->where('request_at', '!=', NULL)->whereNotNull('end_at');
            }
          }else{
            if($status == "live") {
              $q->where('status', 'live')->whereNull('end_at')->whereNotNull('request_at')->where('handle_by', $params["agent_id"]);
            }
            if($status == "resolved") {
              $q->where('status', 'resolved')->where('handle_by', $params["agent_id"])->where('request_at', '!=', NULL);
            }
          }
        })->count();
      }

    }


    public function totalUnservedRoom(Request $request, $appId)
    {
      $room = $this->getRoomByStatus($appId,"request");
      return response()->json($room);
    }

    public function getCountRoomByStatus(Request $request, $appId) {
      $hasAgentId = $request->agent_id == null || "" ? false : true;
      return response()->json([
        "live" => $hasAgentId ? $this->getRoomByStatus($appId,"live",["agent_id" => $request->agent_id])  :$this->getRoomByStatus($appId,"live") ,
        "resolved" => $hasAgentId ? $this->getRoomByStatus($appId,"resolved",["agent_id" => $request->agent_id])  :$this->getRoomByStatus($appId,"resolved") ,
        "request" => $this->getRoomByStatus($appId,"request"),
        "bot" => $this->getRoomByStatus($appId,"bot")
      ]);
    }
}
