<?php namespace App\Modules\Room\Repositories\Impl;

use App\Modules\Room\Repositories\RoomRepository;
use App\Shared\Repositories\AbstractRepositoryImpl;
use DB;

class RoomRepositoryImpl extends AbstractRepositoryImpl implements RoomRepository {

  public function getByAppID($get_all_room_request) {
    $per_page = $get_all_room_request->perPage;
    if (!$get_all_room_request->perPage) {
      $per_page = 7;
    }
    return DB::table("omnichannel.rooms as r")
            ->leftJoin("omnichannel.livechats as l",function($join) {
              $join->on("r.id","=","l.room_id");
            })
            ->join("omnichannel.channels as c",function($query) {
              $query->on("r.channel_id","=","c.id");
            })
            ->leftJoin("omnichannel.notes as n",function($join){
              $join->on("r.id","=","n.room_id");
            })
            ->join("app.users as u",function($join) {
              $join->on("u.id","=","r.created_by");
            })
            ->leftJoin("omnichannel.blocked_user as bu",function($join) {
              $join->on("bu.email","=","u.email");
              $join->on("bu.app_id","=","r.app_id");
            })
            ->leftJoin("omnichannel.integrations as i",function($query) {
              $query->on("i.id","=","u.integration_id");
            })
            ->addSelect(DB::raw("
                  (
                    json_build_object(
                      'id', i.id,
                      'whatsappNumber', i.integration_data->'whatsappNumber',
                      'whatsappName', i.integration_data->'whatsappName'
                  )
                ) as integration
            "))
            ->addSelect(DB::raw("
                  case
                    when bu.id is null
                    then false
                    else true
                  end as is_blocked
            "))
            ->addSelect(DB::raw("
              (
                select count(m.id)
                from omnichannel.messages m INNER JOIN omnichannel.message_statuses om ON m.id = om.message_id WHERE om.read_at is null and om.messageable_type = 'user' and om.created_at >= '2020-10-24 14:55:37' and m.room_id = r.id
              ) as unread_count
            "))
            ->addSelect("r.id","r.app_id","r.created_at","r.updated_at")
            ->addSelect(DB::raw("
              json_build_object(
                'id', u.id,
                'name', u.name,
                'nickname', u.nickname,
                'phone', u.phone,
                'city', u.city,
                'email', u.email,
                'fcm_token', u.fcm_token,
                'contact_id', u.contact_id
              ) as created_by
            "))
            ->addSelect(DB::raw("
              json_build_object(
                'id', c.id,
                'name', c.name
              ) as channel
            "))
            ->addSelect(DB::raw("
              (
                CASE
                  when n.id is null
                  then null
                  else (
                    json_build_object(
                      'id', n.id,
                      'content', n.content,
                      'room_id', n.room_id
                    )
                  )
                END
              ) as notes
            "))
            ->addSelect(DB::raw("
                (
                  SELECT json_agg(
                    json_build_object(
                      'id', ai.id,
                      'room_id', ai.room_id,
                      'title', ai.title,
                      'description', ai.description
                    )
                  ) FROM omnichannel.additional_informations as ai WHERE ai.room_id = r.id
                ) as additional_informations
            "))
            ->addSelect(DB::raw("
                (
                  case
                    when l.status is null
                    then null
                    else (
                      json_build_object(
                        'id', l.id,
                        'room_id', l.room_id,
                        'handle_by', l.handle_by,
                        'updated_by', l.updated_by,
                        'start_at',l.start_at,
                        'status', l.status,
                        'end_at', l.end_at,
                        'request_at',l.request_at,
                        'group_id', l.group_id
                      )
                    )
                  END
                ) as livechat
              "))
            ->addSelect(DB::raw("
              (
                SELECT
                  json_build_array (
                    json_build_object(
                      'id', m.id,
                      'channel_data', m.channel_data,
                      'room_id', m.room_id,
                      'messageable_type', m.messageable_type,
                      'messageable_id', m.messageable_id,
                      'content', m.content,
                      'data', m.data,
                      'deleted_at', m.deleted_at,
                      'created_at', m.created_at,
                      'updated_at', m.updated_at,
                      'encrypt', m.encrypt
                    )
                  )
                FROM omnichannel.messages as m WHERE m.room_id = r.id ORDER BY m.id DESC LIMIT 1
              ) as messages
            "))
            ->addSelect(DB::raw("
                (
                SELECT
                  json_agg (
                    json_build_object(
                      'id', tt.id,
                      'name', tt.name,
                      'pivot', json_build_object(
                        'id', rt.id,
                        'tag_id', rt.tag_id,
                        'room_id', rt.room_id
                      )
                    )
                  )
                FROM omnichannel.room_tag as rt
                INNER JOIN omnichannel.tags as tt ON rt.tag_id = tt.id
                WHERE rt.room_id = r.id
              ) as tags
            "))
            ->where("r.app_id",$get_all_room_request->appId)
            ->where(function($query)  use ($get_all_room_request) {
              if($get_all_room_request->status) {
                if($get_all_room_request->status == "live") {
                  if ($get_all_room_request->isAgent) {
                    $query->where('l.status', 'live')->whereNull('l.end_at')->whereNotNull('l.request_at')->where('l.handle_by', $get_all_room_request->user_id);
                  } else {
                    $query->where('l.status', 'live')->whereNull('l.end_at')->whereNotNull('l.request_at');
                  }
                }

                if($status == "resolved") {
                  if ($get_all_room_request->isAgent) {
                    $query->where('l.status', 'resolved')->where('l.request_at', '!=', NULL)->where('l.handle_by', $user_id);
                  } else {
                    $query->where('l.status', 'resolved')->where('l.request_at', '!=', NULL);
                  }
                }

                if($get_all_room_request->status == "request"){
                  $query->where('l.status', 'request')->where('l.request_at', '!=', NULL)->where('l.start_at', NULL);
                }

                if($get_all_room_request->status == "bot") {
                  $query->where('l.status', null);
                }
              }
            })
            ->where(function($q) use($get_all_room_request) {
              if($get_all_room_request->search) {
                $q->where("u.name","ilike","%".$get_all_room_request->search."%")->orWhere("u.email","ilike","%".$keyword."%")->orWhere("u.phone","ilike","%".$keyword."%");
              }
            })
            ->where(function($query) use ($get_all_room_request) {
                if($get_all_room_request->channel) {
                  $query->whereIn('c.name', $get_all_room_request->channel);
                }
            })
            ->orderBy("r.updated_at","desc")
            ->paginate($per_page)
            ->map(function($room) {
                $room->messages = json_decode($room->messages);
                $room->channel =  json_decode($room->channel);
                $room->livechat =  json_decode($room->livechat);
                $room->created_by = json_decode($room->created_by);
                $room->notes = json_decode($room->notes);
                $room->integration = is_null($room->integration) ? $room->integration : json_decode($room->integration);
                $room->additional_informations = is_null($room->additional_informations) ? [] : json_decode($room->additional_informations);
                $room->tags = is_null($room->tags) ? [] : json_decode($room->tags);
              return $room;
            });
  }

  public function getByUserId($user_id) {
    $room = DB::table("omnichannel.rooms as r")
                ->leftJoin("omnichannel.livechats as l",function($join) {
                  $join->on("r.id","=","l.room_id");
                })
                ->join("omnichannel.channels as c",function($query) {
                  $query->on("r.channel_id","=","c.id");
                })
                ->leftJoin("omnichannel.notes as n",function($join){
                  $join->on("r.id","=","n.room_id");
                })
                ->join("app.users as u",function($join) {
                  $join->on("u.id","=","r.created_by");
                })
                ->leftJoin("omnichannel.blocked_user as bu",function($join) {
                  $join->on("bu.email","=","u.email");
                  $join->on("bu.app_id","=","r.app_id");
                })
                ->leftJoin("omnichannel.integrations as i",function($query) {
                  $query->on("i.id","=","u.integration_id");
                })
                ->addSelect(DB::raw("
                  case
                    when bu.id is null
                    then false
                    else true
                  end as is_blocked
               "))
               ->addSelect(DB::raw("
                  (
                    json_build_object(
                      'id', i.id,
                      'whatsappNumber', i.integration_data->'whatsappNumber',
                      'whatsappName', i.integration_data->'whatsappName'
                  )
                ) as integration
              "))
                ->addSelect(DB::raw("
                  (
                    select count(m.id)
                    from omnichannel.messages m INNER JOIN omnichannel.message_statuses om ON m.id = om.message_id WHERE om.read_at is null and om.messageable_type = 'user' and om.created_at >= '2020-10-24 14:55:37' and m.room_id = r.id
                  ) as unread_count
                "))
                ->addSelect("r.id","r.created_at","r.updated_at","r.app_id")
                ->addSelect(DB::raw("
                  json_build_object(
                    'id', u.id,
                    'name', u.name,
                    'nickname', u.nickname,
                    'phone', u.phone,
                    'city', u.city,
                    'email', u.email,
                    'fcm_token', u.fcm_token,
                    'contact_id', u.contact_id
                  ) as created_by
                "))
                ->addSelect(DB::raw("
                  json_build_object(
                    'id', c.id,
                    'name', c.name
                  ) as channel
                "))
                ->addSelect(DB::raw("
                  (
                    CASE
                      when n.id is null
                      then null
                      else (
                        json_build_object(
                          'id', n.id,
                          'content', n.content,
                          'room_id', n.room_id
                        )
                      )
                    END
                  ) as notes
                "))
                ->addSelect(DB::raw("
                    (
                      SELECT json_agg(
                        json_build_object(
                          'id', ai.id,
                          'room_id', ai.room_id,
                          'title', ai.title,
                          'description', ai.description
                        )
                      ) FROM omnichannel.additional_informations as ai WHERE ai.room_id = r.id
                    ) as additional_informations
                "))
                ->addSelect(DB::raw("
                    (
                      case
                        when l.status is null
                        then null
                        else (
                          json_build_object(
                            'id', l.id,
                            'room_id', l.room_id,
                            'handle_by', l.handle_by,
                            'updated_by', l.updated_by,
                            'start_at',l.start_at,
                            'status', l.status,
                            'end_at', l.end_at,
                            'request_at',l.request_at,
                            'group_id', l.group_id
                          )
                        )
                      END
                    ) as livechat
                  "))
                ->addSelect(DB::raw("
                  (
                    SELECT
                      json_build_array (
                        json_build_object(
                          'id', m.id,
                          'channel_data', m.channel_data,
                          'room_id', m.room_id,
                          'messageable_type', m.messageable_type,
                          'messageable_id', m.messageable_id,
                          'content', m.content,
                          'data', m.data,
                          'deleted_at', m.deleted_at,
                          'created_at', m.created_at,
                          'updated_at', m.updated_at,
                          'encrypt', m.encrypt
                        )
                      )
                  FROM omnichannel.messages as m WHERE m.room_id = r.id ORDER BY m.id DESC LIMIT 1
                  ) as messages
                "))
                ->addSelect(DB::raw("
                  (
                    SELECT
                      json_agg (
                        json_build_object(
                          'id', tt.id,
                          'name', tt.name,
                          'pivot', json_build_object(
                            'id', rt.id,
                            'tag_id', rt.tag_id,
                            'room_id', rt.room_id
                          )
                        )
                      )
                  FROM omnichannel.room_tag as rt
                  INNER JOIN omnichannel.tags as tt ON rt.tag_id = tt.id
                  WHERE rt.room_id = r.id
                  ) as tags
                "))
                ->where("r.created_by",$user_id)
                ->first();
                if(!is_null($room)) {
                  $room->messages = json_decode($room->messages);
                  $room->channel =  json_decode($room->channel);
                  $room->livechat =  json_decode($room->livechat);
                  $room->created_by = json_decode($room->created_by);
                  $room->notes = json_decode($room->notes);
                  $room->integration = json_decode($room->integration);
                  $room->additional_informations = is_null($room->additional_informations) ? [] : json_decode($room->additional_informations);
                  $room->tags = is_null($room->tags) ? [] : json_decode($room->tags);
                }

      return $room;
  }

  public function getById($id) {
    $room = DB::table("omnichannel.rooms as r")
                ->leftJoin("omnichannel.livechats as l",function($join) {
                  $join->on("r.id","=","l.room_id");
                })
                ->join("omnichannel.channels as c",function($query) {
                  $query->on("r.channel_id","=","c.id");
                })
                ->leftJoin("omnichannel.notes as n",function($join){
                  $join->on("r.id","=","n.room_id");
                })
                ->join("app.users as u",function($join) {
                  $join->on("u.id","=","r.created_by");
                })
                ->leftJoin("omnichannel.blocked_user as bu",function($join) {
                  $join->on("bu.email","=","u.email");
                  $join->on("bu.app_id","=","r.app_id");
                })
                ->leftJoin("omnichannel.integrations as i",function($query) {
                  $query->on("i.id","=","u.integration_id");
                })
                ->addSelect(DB::raw("
                    case
                      when bu.id is null
                      then false
                      else true
                    end as is_blocked
                "))
                ->addSelect(DB::raw("
                    (
                      json_build_object(
                        'id', i.id,
                        'whatsappNumber', i.integration_data->'whatsappNumber',
                        'whatsappName', i.integration_data->'whatsappName'
                    )
                  ) as integration
                "))
                ->addSelect(DB::raw("
                  (
                    select count(m.id)
                    from omnichannel.messages m INNER JOIN omnichannel.message_statuses om ON m.id = om.message_id WHERE om.read_at is null and om.messageable_type = 'user' and om.created_at >= '2020-10-24 14:55:37' and m.room_id = r.id
                  ) as unread_count
                "))
                ->addSelect("r.id","r.created_at","r.updated_at","r.app_id")
                ->addSelect(DB::raw("
                  json_build_object(
                    'id', u.id,
                    'name', u.name,
                    'nickname', u.nickname,
                    'phone', u.phone,
                    'city', u.city,
                    'email', u.email,
                    'fcm_token', u.fcm_token,
                    'contact_id', u.contact_id
                  ) as created_by
                "))
                ->addSelect(DB::raw("
                  json_build_object(
                    'id', c.id,
                    'name', c.name
                  ) as channel
                "))
                ->addSelect(DB::raw("
                  (
                    CASE
                      when n.id is null
                      then null
                      else (
                        json_build_object(
                          'id', n.id,
                          'content', n.content,
                          'room_id', n.room_id
                        )
                      )
                    END
                  ) as notes
                "))
                ->addSelect(DB::raw("
                    (
                      SELECT json_agg(
                        json_build_object(
                          'id', ai.id,
                          'room_id', ai.room_id,
                          'title', ai.title,
                          'description', ai.description
                        )
                      ) FROM omnichannel.additional_informations as ai WHERE ai.room_id = r.id
                    ) as additional_informations
                "))
                ->addSelect(DB::raw("
                    (
                      case
                        when l.status is null
                        then null
                        else (
                          json_build_object(
                            'id', l.id,
                            'room_id', l.room_id,
                            'handle_by', l.handle_by,
                            'updated_by', l.updated_by,
                            'start_at',l.start_at,
                            'status', l.status,
                            'end_at', l.end_at,
                            'group_id', l.group_id,
                            'request_at',l.request_at
                          )
                        )
                      END
                    ) as livechat
                  "))
                ->addSelect(DB::raw("
                  (
                    SELECT
                      json_build_array (
                        json_build_object(
                          'id', m.id,
                          'channel_data', m.channel_data,
                          'room_id', m.room_id,
                          'messageable_type', m.messageable_type,
                          'messageable_id', m.messageable_id,
                          'content', m.content,
                          'data', m.data,
                          'deleted_at', m.deleted_at,
                          'created_at', m.created_at,
                          'updated_at', m.updated_at,
                          'encrypt', m.encrypt
                        )
                      )
                  FROM omnichannel.messages as m WHERE m.room_id = r.id ORDER BY m.id DESC LIMIT 1
                  ) as messages
                "))
                ->addSelect(DB::raw("
                  (
                    SELECT
                      json_agg (
                        json_build_object(
                          'id', tt.id,
                          'name', tt.name,
                          'pivot', json_build_object(
                            'id', rt.id,
                            'tag_id', rt.tag_id,
                            'room_id', rt.room_id
                          )
                        )
                      )
                  FROM omnichannel.room_tag as rt
                  INNER JOIN omnichannel.tags as tt ON rt.tag_id = tt.id
                  WHERE rt.room_id = r.id
                  ) as tags
                "))
                ->where("r.id",$id)
                ->first();
                if(!is_null($room)) {
                  $room->messages = json_decode($room->messages);
                  $room->channel =  json_decode($room->channel);
                  $room->livechat =  json_decode($room->livechat);
                  $room->created_by = json_decode($room->created_by);
                  $room->notes = json_decode($room->notes);
                  $room->integration = json_decode($room->integration);
                  $room->additional_informations = is_null($room->additional_informations) ? [] : json_decode($room->additional_informations);
                  $room->tags = is_null($room->tags) ? [] : json_decode($room->tags);
                }

      return $room;
  }

  public function getByUnservedRoom($app_id) {
    $room = DB::table("omnichannel.rooms as r")
                ->leftJoin("omnichannel.livechats as l",function($join) {
                  $join->on("r.id","=","l.room_id");
                })
                ->join("omnichannel.channels as c",function($query) {
                  $query->on("r.channel_id","=","c.id");
                })
                ->leftJoin("omnichannel.notes as n",function($join){
                  $join->on("r.id","=","n.room_id");
                })
                ->join("app.users as u",function($join) {
                  $join->on("u.id","=","r.created_by");
                })
                ->leftJoin("omnichannel.blocked_user as bu",function($join) {
                  $join->on("bu.email","=","u.email");
                  $join->on("bu.app_id","=","r.app_id");
                })
                ->leftJoin("omnichannel.integrations as i",function($query) {
                  $query->on("i.id","=","u.integration_id");
                })
                ->addSelect(DB::raw("
                    case
                      when bu.id is null
                      then false
                      else true
                    end as is_blocked
                "))
                ->addSelect(DB::raw("
                    (
                      json_build_object(
                        'id', i.id,
                        'whatsappNumber', i.integration_data->'whatsappNumber',
                        'whatsappName', i.integration_data->'whatsappName'
                    )
                  ) as integration
                "))
                ->addSelect(DB::raw("
                  (
                    select count(m.id)
                    from omnichannel.messages m INNER JOIN omnichannel.message_statuses om ON m.id = om.message_id WHERE om.read_at is null and om.messageable_type = 'user' and om.created_at >= '2020-10-24 14:55:37' and m.room_id = r.id
                  ) as unread_count
                "))
                ->addSelect("r.id","r.created_at","r.updated_at","r.app_id")
                ->addSelect(DB::raw("
                  json_build_object(
                    'id', u.id,
                    'name', u.name,
                    'nickname', u.nickname,
                    'phone', u.phone,
                    'city', u.city,
                    'email', u.email,
                    'fcm_token', u.fcm_token,
                    'contact_id', u.contact_id
                  ) as created_by
                "))
                ->addSelect(DB::raw("
                  json_build_object(
                    'id', c.id,
                    'name', c.name
                  ) as channel
                "))
                ->addSelect(DB::raw("
                  (
                    CASE
                      when n.id is null
                      then null
                      else (
                        json_build_object(
                          'id', n.id,
                          'content', n.content,
                          'room_id', n.room_id
                        )
                      )
                    END
                  ) as notes
                "))
                ->addSelect(DB::raw("
                    (
                      SELECT json_agg(
                        json_build_object(
                          'id', ai.id,
                          'room_id', ai.room_id,
                          'title', ai.title,
                          'description', ai.description
                        )
                      ) FROM omnichannel.additional_informations as ai WHERE ai.room_id = r.id
                    ) as additional_informations
                "))
                ->addSelect(DB::raw("
                    (
                      case
                        when l.status is null
                        then null
                        else (
                          json_build_object(
                            'id', l.id,
                            'room_id', l.room_id,
                            'handle_by', l.handle_by,
                            'updated_by', l.updated_by,
                            'start_at',l.start_at,
                            'status', l.status,
                            'end_at', l.end_at,
                            'request_at',l.request_at,
                            'group_id', l.group_id
                          )
                        )
                      END
                    ) as livechat
                  "))
                ->addSelect(DB::raw("
                  (
                    SELECT
                      json_build_array (
                        json_build_object(
                          'id', m.id,
                          'channel_data', m.channel_data,
                          'room_id', m.room_id,
                          'messageable_type', m.messageable_type,
                          'messageable_id', m.messageable_id,
                          'content', m.content,
                          'data', m.data,
                          'deleted_at', m.deleted_at,
                          'created_at', m.created_at,
                          'updated_at', m.updated_at,
                          'encrypt', m.encrypt
                        )
                      )
                  FROM omnichannel.messages as m WHERE m.room_id = r.id ORDER BY m.id DESC LIMIT 1
                  ) as messages
                "))
                ->addSelect(DB::raw("
                  (
                    SELECT
                      json_agg (
                        json_build_object(
                          'id', tt.id,
                          'name', tt.name,
                          'pivot', json_build_object(
                            'id', rt.id,
                            'tag_id', rt.tag_id,
                            'room_id', rt.room_id
                          )
                        )
                      )
                  FROM omnichannel.room_tag as rt
                  INNER JOIN omnichannel.tags as tt ON rt.tag_id = tt.id
                  WHERE rt.room_id = r.id
                  ) as tags
                "))
                ->where("l.status","request")
                ->whereNull("l.end_at")
                ->whereNull("l.start_at")
                ->whereNotNull("l.request_at")
                ->where("r.app_id",$app_id)
                ->orderBy("l.id","ASC")
                ->first();
                if(!is_null($room)) {
                  $room->messages = json_decode($room->messages);
                  $room->channel =  json_decode($room->channel);
                  $room->livechat =  json_decode($room->livechat);
                  $room->created_by = json_decode($room->created_by);
                  $room->integration = json_decode($room->integration);
                  $room->notes = json_decode($room->notes);
                  $room->additional_informations = is_null($room->additional_informations) ? [] : json_decode($room->additional_informations);
                  $room->tags = is_null($room->tags) ? [] : json_decode($room->tags);
                }
      return $room;
  }

  public function getAllUnservedRoom($app_id) {
    return DB::table("omnichannel.rooms as r")
          ->leftJoin("omnichannel.livechats as l",function($join) {
            $join->on("r.id","=","l.room_id");
          })
          ->join("omnichannel.channels as c",function($query) {
            $query->on("r.channel_id","=","c.id");
          })
          ->leftJoin("omnichannel.notes as n",function($join){
            $join->on("r.id","=","n.room_id");
          })
          ->join("app.users as u",function($join) {
            $join->on("u.id","=","r.created_by");
          })
          ->leftJoin("omnichannel.blocked_user as bu",function($join) {
            $join->on("bu.email","=","u.email");
            $join->on("bu.app_id","=","r.app_id");
          })
          ->leftJoin("omnichannel.integrations as i",function($query) {
            $query->on("i.id","=","u.integration_id");
          })
          ->addSelect(DB::raw("
              case
                when bu.id is null
                then false
                else true
              end as is_blocked
          "))
          ->addSelect(DB::raw("
              (
                json_build_object(
                  'id', i.id,
                  'whatsappNumber', i.integration_data->'whatsappNumber',
                  'whatsappName', i.integration_data->'whatsappName'
              )
            ) as integration
          "))
          ->addSelect(DB::raw("
            (
              select count(m.id)
              from omnichannel.messages m INNER JOIN omnichannel.message_statuses om ON m.id = om.message_id WHERE om.read_at is null and om.messageable_type = 'user' and om.created_at >= '2020-10-24 14:55:37' and m.room_id = r.id
            ) as unread_count
          "))
          ->addSelect("r.id","r.app_id","r.created_at","r.updated_at")
          ->addSelect(DB::raw("
            json_build_object(
              'id', u.id,
              'name', u.name,
              'nickname', u.nickname,
              'phone', u.phone,
              'city', u.city,
              'email', u.email,
              'fcm_token', u.fcm_token,
              'contact_id', u.contact_id
            ) as created_by
          "))
          ->addSelect(DB::raw("
            json_build_object(
              'id', c.id,
              'name', c.name
            ) as channel
          "))
          ->addSelect(DB::raw("
            (
              CASE
                when n.id is null
                then null
                else (
                  json_build_object(
                    'id', n.id,
                    'content', n.content,
                    'room_id', n.room_id
                  )
                )
              END
            ) as notes
          "))
          ->addSelect(DB::raw("
              (
                SELECT json_agg(
                  json_build_object(
                    'id', ai.id,
                    'room_id', ai.room_id,
                    'title', ai.title,
                    'description', ai.description
                  )
                ) FROM omnichannel.additional_informations as ai WHERE ai.room_id = r.id
              ) as additional_informations
          "))
          ->addSelect(DB::raw("
              (
                case
                  when l.status is null
                  then null
                  else (
                    json_build_object(
                      'id', l.id,
                      'room_id', l.room_id,
                      'handle_by', l.handle_by,
                      'updated_by', l.updated_by,
                      'start_at',l.start_at,
                      'status', l.status,
                      'end_at', l.end_at,
                      'request_at',l.request_at,
                      'group_id', l.group_id
                    )
                  )
                END
              ) as livechat
            "))
          ->addSelect(DB::raw("
            (
              SELECT
                json_build_array (
                  json_build_object(
                    'id', m.id,
                    'channel_data', m.channel_data,
                    'room_id', m.room_id,
                    'messageable_type', m.messageable_type,
                    'messageable_id', m.messageable_id,
                    'content', m.content,
                    'data', m.data,
                    'deleted_at', m.deleted_at,
                    'created_at', m.created_at,
                    'updated_at', m.updated_at,
                    'encrypt', m.encrypt
                  )
                )
              FROM omnichannel.messages as m WHERE m.room_id = r.id ORDER BY m.id DESC LIMIT 1
            ) as messages
          "))
          ->addSelect(DB::raw("
              (
              SELECT
                json_agg (
                  json_build_object(
                    'id', tt.id,
                    'name', tt.name,
                    'pivot', json_build_object(
                      'id', rt.id,
                      'tag_id', rt.tag_id,
                      'room_id', rt.room_id
                    )
                  )
                )
              FROM omnichannel.room_tag as rt
              INNER JOIN omnichannel.tags as tt ON rt.tag_id = tt.id
              WHERE rt.room_id = r.id
            ) as tags
          "))
            ->where("r.app_id",$app_id)
            ->where("l.status","request")
            ->whereNotNull("l.request_at")
            ->whereNull("l.start_at")
            ->whereNull("l.end_at")
            ->orderBy("l.id","asc")
            ->get()
            ->map(function($each_room) {
              $each_room->messages = json_decode($each_room->messages);
              $each_room->channel =  json_decode($each_room->channel);
              $each_room->livechat =  json_decode($each_room->livechat);
              $each_room->created_by = json_decode($each_room->created_by);
              $each_room->notes = json_decode($each_room->notes);
              $each_room->integration = json_decode($each_room->integration);
              $each_room->additional_informations = is_null($each_room->additional_informations) ? [] : json_decode($each_room->additional_informations);
              $each_room->tags = is_null($each_room->tags) ? [] : json_decode($each_room->tags);
              return $each_room;
            });
  }

  public function getServedRoomByGroups($appId, $groupsId) {
    $room = DB::table("omnichannel.rooms as r")
                ->leftJoin("omnichannel.livechats as l",function($join) {
                  $join->on("r.id","=","l.room_id");
                })
                ->where("l.status","live")
                ->whereNull("l.end_at")
                ->whereNotNull("l.start_at")
                ->whereNotNull("l.request_at")
                ->whereIn("l.group_id", $groupsId)
                ->where("r.app_id",$appId)
                ->orderBy("l.id","ASC")
                ->get();
      return $room;
  }

  public function getUnservedRoomByGroupsId($appId, $groupsId) {
    $room = DB::table("omnichannel.rooms as r")
                ->leftJoin("omnichannel.livechats as l",function($join) {
                  $join->on("r.id","=","l.room_id");
                })
                ->join("omnichannel.channels as c",function($query) {
                  $query->on("r.channel_id","=","c.id");
                })
                ->leftJoin("omnichannel.notes as n",function($join){
                  $join->on("r.id","=","n.room_id");
                })
                ->join("app.users as u",function($join) {
                  $join->on("u.id","=","r.created_by");
                })
                ->leftJoin("omnichannel.blocked_user as bu",function($join) {
                  $join->on("bu.email","=","u.email");
                  $join->on("bu.app_id","=","r.app_id");
                })
                ->leftJoin("omnichannel.integrations as i",function($query) {
                  $query->on("i.id","=","u.integration_id");
                })
                ->addSelect(DB::raw("
                    case
                      when bu.id is null
                      then false
                      else true
                    end as is_blocked
                "))
                ->addSelect(DB::raw("
                    (
                      json_build_object(
                        'id', i.id,
                        'whatsappNumber', i.integration_data->'whatsappNumber',
                        'whatsappName', i.integration_data->'whatsappName'
                    )
                  ) as integration
                "))
                ->addSelect(DB::raw("
                  (
                    select count(m.id)
                    from omnichannel.messages m INNER JOIN omnichannel.message_statuses om ON m.id = om.message_id WHERE om.read_at is null and om.messageable_type = 'user' and om.created_at >= '2020-10-24 14:55:37' and m.room_id = r.id
                  ) as unread_count
                "))
                ->addSelect("r.id","r.created_at","r.updated_at","r.app_id")
                ->addSelect(DB::raw("
                  json_build_object(
                    'id', u.id,
                    'name', u.name,
                    'nickname', u.nickname,
                    'phone', u.phone,
                    'city', u.city,
                    'email', u.email,
                    'fcm_token', u.fcm_token,
                    'contact_id', u.contact_id
                  ) as created_by
                "))
                ->addSelect(DB::raw("
                  json_build_object(
                    'id', c.id,
                    'name', c.name
                  ) as channel
                "))
                ->addSelect(DB::raw("
                  (
                    CASE
                      when n.id is null
                      then null
                      else (
                        json_build_object(
                          'id', n.id,
                          'content', n.content,
                          'room_id', n.room_id
                        )
                      )
                    END
                  ) as notes
                "))
                ->addSelect(DB::raw("
                    (
                      SELECT json_agg(
                        json_build_object(
                          'id', ai.id,
                          'room_id', ai.room_id,
                          'title', ai.title,
                          'description', ai.description
                        )
                      ) FROM omnichannel.additional_informations as ai WHERE ai.room_id = r.id
                    ) as additional_informations
                "))
                ->addSelect(DB::raw("
                    (
                      case
                        when l.status is null
                        then null
                        else (
                          json_build_object(
                            'id', l.id,
                            'room_id', l.room_id,
                            'handle_by', l.handle_by,
                            'updated_by', l.updated_by,
                            'start_at',l.start_at,
                            'status', l.status,
                            'end_at', l.end_at,
                            'request_at',l.request_at,
                            'group_id', l.group_id
                          )
                        )
                      END
                    ) as livechat
                  "))
                ->addSelect(DB::raw("
                  (
                    SELECT
                      json_build_array (
                        json_build_object(
                          'id', m.id,
                          'channel_data', m.channel_data,
                          'room_id', m.room_id,
                          'messageable_type', m.messageable_type,
                          'messageable_id', m.messageable_id,
                          'content', m.content,
                          'data', m.data,
                          'deleted_at', m.deleted_at,
                          'created_at', m.created_at,
                          'updated_at', m.updated_at,
                          'encrypt', m.encrypt
                        )
                      )
                  FROM omnichannel.messages as m WHERE m.room_id = r.id ORDER BY m.id DESC LIMIT 1
                  ) as messages
                "))
                ->addSelect(DB::raw("
                  (
                    SELECT
                      json_agg (
                        json_build_object(
                          'id', tt.id,
                          'name', tt.name,
                          'pivot', json_build_object(
                            'id', rt.id,
                            'tag_id', rt.tag_id,
                            'room_id', rt.room_id
                          )
                        )
                      )
                  FROM omnichannel.room_tag as rt
                  INNER JOIN omnichannel.tags as tt ON rt.tag_id = tt.id
                  WHERE rt.room_id = r.id
                  ) as tags
                "))
                ->where("l.status","request")
                ->whereNull("l.end_at")
                ->whereNull("l.start_at")
                ->whereNotNull("l.request_at")
                ->whereIn("l.group_id", $groupsId)
                ->where("r.app_id",$appId)
                ->orderBy("l.id","ASC")
                ->first();
                if(!is_null($room)) {
                  $room->messages = json_decode($room->messages);
                  $room->channel =  json_decode($room->channel);
                  $room->livechat =  json_decode($room->livechat);
                  $room->created_by = json_decode($room->created_by);
                  $room->integration = json_decode($room->integration);
                  $room->notes = json_decode($room->notes);
                  $room->additional_informations = is_null($room->additional_informations) ? [] : json_decode($room->additional_informations);
                  $room->tags = is_null($room->tags) ? [] : json_decode($room->tags);
                }
      return $room;
  }
}
