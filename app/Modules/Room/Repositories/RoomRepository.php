<?php namespace App\Modules\Room\Repositories;

use App\Shared\Repositories\AbstractRepository;
use App\Modules\Room\Request\GetAllRoomRequest;

interface RoomRepository extends AbstractRepository{
  public function getByUserId(int $user_id);
  public function getByAppID($getAllRoom);
  public function getById($room_id);
  public function getByUnservedRoom($app_id);
  public function getAllUnservedRoom($app_id);
  public function getServedRoomByGroups($appId,$groupsId);
  public function getUnservedRoomByGroupsId($app_id, $groupsId);
}
