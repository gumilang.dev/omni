<?php namespace App\Modules\AgentRating\Controllers;

use App\Services\AgentRatingServiceImpl;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class AgentRatingController extends ApiController {

  public function __construct(AgentRatingServiceImpl $service) {
    $this->service = $service;
  }

  public function store(Request $request, $appId) {
    $response = $this->service->createRating([
      "agent_id" => $request->agent_id,
      "user_id" => $request->user_id,
      "rate" => $request->rate,
      "app_id" => $appId
    ]);
    if($response["error"]) return $this->errorResponse($response["message"],"CREATE_RATING_FAILED");
    return $this->successResponse($response,"CREATE_RATING_SUCCESS");
  }

}
