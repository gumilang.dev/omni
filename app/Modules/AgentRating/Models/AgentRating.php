<?php

namespace App\Modules\AgentRating\Models;

use Illuminate\Database\Eloquent\Model;

class AgentRating extends Model
{
  protected $table = "agent_rating";
  protected $fillable = ["app_id","agent_id","user_id","rate"];
  protected $hidden = ["id","created_at","updated_at"];
}
