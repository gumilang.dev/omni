<?php

namespace App\Modules\Analytics\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\App;
use Illuminate\Support\Facades\Validator;

use App\Modules\User\Models\VerifyUser;
use App\Modules\Message\Models\Message;

use App\Http\Controllers\Controller;

class AnalyticsController extends Controller
{
  public function getMessages(Request $request)
  {
    // $from = $request->from;
    // $to   = $request->to;

    // $validator = Validator::make($request->all(), [
    //   'from'  => 'required',
    //   'to'    => 'required',
    // ]);

    // if($validator->fails()){
    //   return $this->errorResponse($validator->errors(), 'Request data not valid', 406);
    // }

    $messages = Message::orderBy('id', 'ASC')->get();
    return response()->json($messages);
  }

  public function subscribeUser(Request $request)
  {
    $user = new VerifyUser;
    $user->email = $request->email;
    $user->token = $request->token;
    $user->save();
    return $this->successResponse($user, 'Success subscribe user');
  }
}
