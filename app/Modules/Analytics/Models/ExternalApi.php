<?php

namespace App\Modules\Analytics\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ExternalApi extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $connection = 'pgsql';

    protected $guarded = [];
    public function endpoints()
    {
        return $this->hasMany("App\Modules\ExternalApi\Models\ExternalApiEndpoint");
    }
}
