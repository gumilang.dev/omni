<?php

namespace App\Modules\Analytics\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ExternalApiEndpoint extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $connection = 'pgsql';

    protected $guarded = [];
    public function api()
    {
        return $this->belongsTo("App\Modules\ExternalApi\Models\ExternalApi");
    }
}
