<?php

namespace App\Modules\Menu\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\App;
use App\Modules\Menu\Models\Menu;
use App\Modules\Role\Models\RoleMenu;
use App\Modules\Menu\Models\Submenu;
use App\Http\Controllers\Controller;

class MenuController extends Controller
{

    public function getMenus()
    {
        $menus = Menu::with(['submenus' => function($q) {
          $q->orderBy('id', 'ASC');
        }])->orderBy('id', 'ASC')->get();
        return response()->json($menus);

    }
    public function getSubmenus()
    {
        $submenus = Submenu::with('menu')->orderBy('id', 'ASC')->get();
        return response()->json($submenus);

    }
    public function getParent(Request $request)
    {
        $slug = $request->post('slug');
        $parentId = Submenu::with('menu')->where('slug', '=', $slug)->first()->menu->id;

        $menus = Menu::with('submenus')->where('id', '=', $parentId)->get();
        return response()->json($menus);

    }
}
