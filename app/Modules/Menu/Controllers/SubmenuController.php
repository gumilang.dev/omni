<?php

namespace App\Modules\Menu\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\App;
use App\Modules\Menu\Models\Submenu;
use App\Http\Controllers\Controller;

class SubmenuController extends Controller
{

    public function index()
    {
        $submenus = \App\Modules\Menu\Models\Submenu::with('menu')->paginate(request('limit', 20));
        if (request()->all()) {
            $submenus->appends(request()->all());
        }
        return response()->json($submenus);
    }

    public function getParent(Request $request)
    {
        $slug = $request->post('slug');
        $parentId = \App\Modules\Menu\Models\Submenu::with('menu')->where('slug', '=', $slug)->first()->menu->id;

        $menus = \App\Modules\Menu\Models\Menu::with('submenu')->where('id', '=', $parentId)->get();
        return response()->json($menus);

    }





    public function store(Request $request)
    {
        //
    }


    public function show(Request $request)
    {
        $id = $request->route('id');

        $submenu = \App\Modules\Menu\Models\Submenu::findOrFail($id);
        return response()->json($submenu);
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
