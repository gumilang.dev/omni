<?php

namespace App\Modules\Menu\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuBot extends Model
{
    use SoftDeletes;

    protected $connection = 'bot';
    protected $table = 'menus';
    protected $guarded = [];
    protected $hidden = [
      'module_id',
      'created_at',
      'updated_at',
      'deleted_at',
      'created_by',
      'updated_by',
      'deleted_by,'
    ];

    public function submenus()
    {
        return $this->hasMany('App\Modules\Menu\Models\SubmenuBot');
    }

    public function module()
    {
        return $this->belongsTo('App\Modules\Module\Models\Module');
    }

    public function roleMenus()
    {
        return $this->hasMany('App\Modules\Role\Models\RoleMenu', 'menu_id');
    }
}
