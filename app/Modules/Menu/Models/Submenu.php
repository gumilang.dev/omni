<?php

namespace App\Modules\Menu\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Submenu extends Model
{
    use SoftDeletes;
    protected $connection = 'pgsql';
    protected $casts = [
      'plans' => 'array'
    ];
    protected $hidden = [
      'created_at',
      'updated_at',
      'deleted_at',
      'menu_id',
      'sub_submenu'
    ];
    public function menu()
    {
        return $this->belongsTo('App\Modules\Menu\Models\Menu');
    }
    public function subSubmenus()
    {
        return $this->hasMany('App\Modules\Menu\Models\SubSubmenu', 'sub_submenu_id');
    }
}
