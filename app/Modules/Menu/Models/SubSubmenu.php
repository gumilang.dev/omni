<?php

namespace App\Modules\Menu\Models;

use Illuminate\Database\Eloquent\Model;

class SubSubmenu extends Model
{
    protected $connection = 'pgsql';
    protected $hidden = [
      'created_at',
      'updated_at',
      'sub_submenu_id'
    ];
    protected $casts = [
      'plans' => 'array'
    ];
    public function submenu()
    {
        return $this->belongsTo('App\Modules\Menu\Models\Submenu', 'sub_submenu_id');
    }
}
