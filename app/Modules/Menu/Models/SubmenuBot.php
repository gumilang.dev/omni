<?php

namespace App\Modules\Menu\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Submenu extends Model
{
    use SoftDeletes;
    protected $connection = 'bot';
    protected $table = 'submenus';
    protected $guarded = [];

    public function menu()
    {
        return $this->belongsTo('App\Modules\Menu\Models\MenuBot');
    }
}
