<?php

namespace App\Modules\Menu\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    protected $connection = 'pgsql';

    use SoftDeletes;
    protected $guarded = [];
    protected $casts = [
      'plans' => 'array'
    ];
    protected $hidden = [
      'created_at',
      'updated_at',
      'deleted_at',
      'module_id'
    ];
    public function submenus()
    {
        return $this->hasMany('App\Modules\Menu\Models\Submenu');
    }

    public function module()
    {
        return $this->belongsTo('App\Modules\Module\Models\Module', 'module_id');
    }

    public function roleMenus()
    {
        return $this->hasMany('App\Modules\Role\Models\RoleMenu', 'menu_id');
    }

    public function menuTitle()
    {
        return $this->belongsTo('App\Modules\Menu\Models\MenuTitle', 'menu_title_id');
    }
}
