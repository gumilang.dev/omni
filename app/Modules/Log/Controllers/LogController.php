<?php

namespace App\Modules\Log\Controllers;

use Hashids\Hashids;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Controllers\ApiController;

use App\Modules\Log\Models\ApiLog;

use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class LogController extends ApiController
{
    public function saveApiLog(Request $request)
    {
      ApiLog::create([
          'url'           => $request->url,
          'app_id'        => $request->app_id,
          'user_id'       => $request->user_id,
          'request'       => $request->payload,
          'response'      => $request->response,
          'ip_address'    => $request->ip,
          'user_agent'    => $request->header,
          'userable_type' => $request->userable_type,
          'userable_id'   => $request->userable_id,
      ]);
    }
}
