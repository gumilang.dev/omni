<?php

namespace App\Modules\Log\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

Relation::morphMap([
    'user' => 'App\Modules\User\Models\User',
    'user_platform' => 'App\Modules\User\Models\UserPlatform',
    // ''
]);
class ApiLog extends Model
{
    protected $connection = 'pgsql';
    // protected $appends = [
    //   'userable'
    // ];

    protected $guarded = [];

    public function userable()
    {
        return $this->morphTo();
    }
}
