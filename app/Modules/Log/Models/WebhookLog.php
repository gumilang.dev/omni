<?php

namespace App\Modules\Log\Models;

use Illuminate\Database\Eloquent\Model;

class WebhookLog extends Model
{
    protected $connection = 'pgsql';

    protected $guarded = [];
}
