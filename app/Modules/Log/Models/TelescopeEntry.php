<?php

namespace App\Modules\Log\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class TelescopeEntry extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'telescope_entries';

    protected $guarded = [];

    protected $casts = [
      'content' => 'json',
		];

    public function userable()
    {
        return $this->morphTo();
    }
}
