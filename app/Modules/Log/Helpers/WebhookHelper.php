<?php

namespace App\Modules\Log\Helpers;

use Carbon\Carbon;
use App\Modules\Room\Models\Room;
use App\Modules\Room\Controllers\RoomController;
use App\Modules\Message\Models\Message;
use App\Modules\Integration\Models\Integration;
use App\Modules\User\Models\User;
use App\Modules\User\Models\UserApp;
use App\Events\RequestLive;
use App\Events\RequestHandled;
use App\Modules\Livechat\Controllers\LivechatController;
use App\Modules\App\Models\App;

class WebhookHelper
{

    public static function isLive($data = [])
    {
      $chatRoom = Room::where("id", $data['roomId'])->whereHas('liveChats', function ($query) use ($data) {
          $query->where('status', 'live');
      })->get();
      return count($chatRoom) > 0;
    }

    public static function requestLivechat($senderId,$appId) {
      $app = App::find($appId);
      $room = User::find($senderId)->rooms->first();
      $liveChat = $room->liveChats()->firstOrCreate(
          ['room_id' => $room->id, 'status' => 'request'],
          [
              "request_at" => Carbon::now()->toDateTimeString(),
              "status" => 'request',
          ]
      );
      $queue = Room::where('app_id', $appId)->whereHas('liveChats', function ($q) {
        $q->where('status', 'request')->where('end_at', NULL)->where('request_at','!=',NULL);
      })->get();
      $queue = collect($queue)->map(function($val){
        return [
          'user_id' => $val->created_by->id,
          'request_at' => $val->livechat->request_at
        ];
      });
      $queue = $queue->sortBy('request_at');
      broadcast(new RequestLive($room->id, $appId, $senderId, $liveChat->id, $queue));

      if($app->auto_agent_allocation) {
        self::autoAgentAllocation($appId);
      }
    }


    public static function autoAgentAllocation($appId) {
      $rooms  = Room::where('app_id', $appId)->with(['channel','tags','additional_informations','notes'])->whereHas('liveChats', function ($q) {
        $q->where('status', 'request')->where('end_at', NULL)->where('request_at','!=',NULL);
      })->get()->sortBy('livechat.request_at')->values();
      $roomController = new RoomController();
      foreach($rooms as $value) {
        $user = UserApp::where('app_id',$appId)->where("role_id",4)->where("online",true)->get();
        $data = [];
        for($i = 0;  $i < count($user); $i++) {
          array_push($data,[
            "user_id" => $user[$i]["user_id"],
            "chat_live_count" => $roomController->getRoomByStatus($appId,"live",["agent_id" => $user[$i]["user_id"]])
          ]);
        }

        $minLivechatCount = array_reduce($data,function($a,$b) {
          return $a["chat_live_count"] < $b["chat_live_count"] ? $a : $b;
        },array_shift($data));

        if(count($user) > 0) {
          if($value->livechat == null) {
            $liveChat = Livechat::create([
              'status' => 'live',
              'room_id' => $value->id,
              'request_at' => Carbon::now()->toDateTimeString(),
              'start_at' => Carbon::now()->toDateTimeString(),
              'handle_by' => $minLivechatCount["user_id"],
              'updated_by' => $minLivechatCount["user_id"]
            ]);
          } else {
            $liveChat = Livechat::where('id', $value->livechat->id)->update(
              [
                'status' => 'live',
                'start_at' => Carbon::now()->toDateTimeString(),
                'handle_by' => $minLivechatCount["user_id"],
                'updated_by' => $minLivechatCount["user_id"]
              ]
            );
          }

          $currentLivechat = Livechat::find($value->livechat->id);
          $user = [
            'id'    => $value->created_by->id,
            'type'  => 'user'
          ];

          $value->unread_count = Room::getUnreadCount($value->id, $user);

          $value->updated_at = Carbon::now()->toDateTimeString();

          // $total = $roomController->getRoomByStatus($appId,"request");
          $queue = Room::where('app_id', $appId)->whereHas('liveChats', function ($q) {
            $q->where('status', 'request')->where('end_at', NULL)->where('request_at','!=',NULL);
          })->get()->sortBy('livechat.request_at')->map(function($val){
            return [
              'user_id' => $val->created_by->id,
              'request_at' => $val->livechat->request_at
            ];
          });
          broadcast(new RequestHandled($appId, $value, $currentLivechat, count($queue), $queue));
        }
      }
    }

    public static function getIntegrationData($channelName, $appId)
    {
        if ($channelName == 'facebook') {
            $integration = Integration::where(['integration_data->pageId' => $appId, 'status' => true])->whereHas('channel', function ($q) {
                $q->where('name', 'facebook');
            })->first();
        } else {
            $integration = Integration::where(['app_id' => $appId, 'status' => true])
                ->whereHas('channel', function ($q) use ($channelName) {
                    $q->where('name', $channelName);
                })->first();
        }
        return $integration;
    }

    public static function getIntegrationDataById($channelName, $appId, $id)
    {
          $integration = Integration::where(['app_id' => $appId,'id' => $id, 'status' => true])
                ->whereHas('channel', function ($q) use ($channelName) {
                  $q->where('name', $channelName);
                })->first();
          return $integration;
    }

    public static function isResolved($data = [])
    {
        $chatRoom = Room::where("id", $data['roomId'])->whereHas('liveChats', function ($query) {
            $query->where('status', 'resolved');
        })->get();
        return count($chatRoom) > 0;
    }

    public static function isRequest($data = [])
    {
      $chatRoom = Room::where("id", $data['roomId'])->whereHas('liveChats', function ($query) {
        $query->where('status', 'request')->where('end_at', NULL)->where('request_at','!=',NULL);
      })->get();
      return count($chatRoom) > 0;
    }

    public static function getQueue($appId) {
      $queue = Room::where('app_id', $appId)->whereHas('liveChats', function ($q) {
        $q->where('status', 'request')->where('end_at', NULL)->where('request_at','!=',NULL);
      })->get()->sortBy('livechat.request_at')->map(function($val){
        return [
          'user_id' => $val->created_by->id,
          'request_at' => $val->livechat->request_at
        ];
      });
      return $queue;
    }

}
