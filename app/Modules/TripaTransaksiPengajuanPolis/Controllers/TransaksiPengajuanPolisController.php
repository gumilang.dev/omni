<?php

namespace App\Modules\TripaTransaksiPengajuanPolis\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use App\Modules\Integration\Models\Integration;
use App\Http\Controllers\ApiController;
use App\Modules\ExternalApi\Models\ExternalApi;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\RequestGuzzle;
class TransaksiPengajuanPolisController extends ApiController
{
    public function historyTransactionPersonalAccidents(Request $request, $appId)
    {
        $sort = $request->get('sort');
        $filterText = $request->get('filterText');
        $per_page = $request->get('per_page');

        $data = DB::table('tripa.personal_accidents as p')->orderBy('p.id', 'DESC');

         if ($sort) {
            $sort = explode('|', $sort);
            $data = $data->orderBy($sort[0], $sort[1]);
        }

        if ($filterText) {
            $data = $data->where(function ($query) use ($filterText) {
              $query->where('name', 'ILIKE', '%'.$filterText.'%');
              $query->orWhere('insurance_type', 'ILIKE', '%'.$filterText.'%');
              $query->orWhere('insurance_value', 'ILIKE', '%'.$filterText.'%');
              $query->orWhere('identity_number', 'ILIKE', '%'.$filterText.'%');
              $query->orWhere('heir_name', 'ILIKE', '%'.$filterText.'%');
              $query->orWhere('month_period', 'ILIKE', '%'.$filterText.'%');
            });
        }

        if (!$per_page) {
            $per_page = 15;
        }

        return response()->json($data->paginate($per_page));
    }

    public function historyTransactionTravels(Request $request, $appId)
    {
        $sort = $request->get('sort');
        $filterText = $request->get('filterText');
        $per_page = $request->get('per_page');

        $data = DB::table('tripa.travels as t')->orderBy('t.id', 'DESC');

         if ($sort) {
            $sort = explode('|', $sort);
            $data = $data->orderBy($sort[0], $sort[1]);
        }

        if ($filterText) {
            $data = $data->where(function ($query) use ($filterText) {
              $query->where('name', 'ILIKE', '%'.$filterText.'%');
              $query->orWhere('insurance_type', 'ILIKE', '%'.$filterText.'%');
              $query->orWhere('destination', 'ILIKE', '%'.$filterText.'%');
              $query->orWhere('heir_name', 'ILIKE', '%'.$filterText.'%');
              $query->orWhere('identity_number', 'ILIKE', '%'.$filterText.'%');
            });
        }

        if (!$per_page) {
            $per_page = 15;
        }

        return response()->json($data->paginate($per_page));
    }

    public function historyTransactionVehicles(Request $request, $appId)
    {
        $sort = $request->get('sort');
        $filterText = $request->get('filterText');
        $per_page = $request->get('per_page');

        $data = DB::table('tripa.vehicles as v')->orderBy('v.id', 'DESC');

         if ($sort) {
            $sort = explode('|', $sort);
            $data = $data->orderBy($sort[0], $sort[1]);
        }

        if ($filterText) {
            $data = $data->where(function ($query) use ($filterText) {
              $query->where('name', 'ILIKE', '%'.$filterText.'%');
              $query->orWhere('insurance_type', 'ILIKE', '%'.$filterText.'%');
              $query->orWhere('vehicle_number', 'ILIKE', '%'.$filterText.'%');
              $query->orWhere('brand', 'ILIKE', '%'.$filterText.'%');
              $query->orWhere('build_year', 'ILIKE', '%'.$filterText.'%');
              $query->orWhere('frame_number', 'ILIKE', '%'.$filterText.'%');
              $query->orWhere('vehicle_region_code', 'ILIKE', '%'.$filterText.'%');
              $query->orWhere('machine_number', 'ILIKE', '%'.$filterText.'%');
            });
        }

        if (!$per_page) {
            $per_page = 15;
        }

        return response()->json($data->paginate($per_page));
    }

    public function historyTransactionWildfires(Request $request, $appId)
    {
        $sort = $request->get('sort');
        $filterText = $request->get('filterText');
        $per_page = $request->get('per_page');

        $data = DB::table('tripa.wildfires as w')->orderBy('w.id', 'DESC');

         if ($sort) {
            $sort = explode('|', $sort);
            $data = $data->orderBy($sort[0], $sort[1]);
        }

        if ($filterText) {
            $data = $data->where(function ($query) use ($filterText) {
              $query->where('title', 'ILIKE', '%'.$filterText.'%')
              ->orWhere('body', 'ILIKE', '%'.$filterText.'$')
              ->orWhere('message_title', 'ILIKE', '%'.$filterText.'$')
              ->orWhere('message_body', 'ILIKE', '%'.$filterText.'$');
            });
        }

        if (!$per_page) {
            $per_page = 15;
        }

        return response()->json($data->paginate($per_page));
    }
}
