<?php

namespace App\Modules\StaticToken\Models;

use Illuminate\Database\Eloquent\Model;

class StaticToken extends Model
{
    protected $fillable = ['app_id', 'token'];
}
