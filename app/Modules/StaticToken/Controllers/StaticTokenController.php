<?php

namespace App\Modules\StaticToken\Controllers;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Modules\App\Models\App;
use App\Modules\User\Models\User;
use App\Modules\StaticToken\Models\StaticToken;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use App\Modules\Mobile\Services\BackendService;
use App\Modules\Role\Helpers\RoleHelper;


class StaticTokenController extends ApiController
{
    public function generateToken(Request $request)
    {
        $appId = $request->decodedAppId;
        $token = "LennaApp".$appId.Str::random(60);
        $hashToken = Hash::make($token); //save db
        $encrypt = BackendService::encryptText(env("ENCRYPT_KEY"), $hashToken); //for show
        // $decrypt = BackendService::decryptText(env("ENCRYPT_KEY"), $encrypt);

        //check
        $checkStaticToken = StaticToken::where('app_id', $appId)->first();
        if (!$checkStaticToken) {

            $staticToken = StaticToken::create([
                "app_id" => $appId,
                "token" => $hashToken
            ]);
            return $this->successResponse($encrypt, 'Token Created');
        } else {
            $roleHelper = RoleHelper::checkCurrentRole($request->decodedUserId, $appId);
            if ($roleHelper->ranking != 1) {
                return $this->errorResponse(null, "You dont have permission to update this token");
            }
            $checkStaticToken->token = $hashToken;
            $checkStaticToken->save();
            return $this->successResponse($encrypt, 'Token Updated');
        }

    }

    public function getToken(Request $request)
    {
        $appId = $request->decodedAppId;
        $staticToken = StaticToken::where('app_id', $appId)->first();
        if (!$staticToken) {
            return $this->generateToken($request);
        }
        $data = BackendService::encryptText(env("ENCRYPT_KEY"), $staticToken->token);
        // $decrypt = BackendService::decryptText(env("ENCRYPT_KEY"), $data);
        return $this->successResponse($data);
    }
}
