<?php namespace App\Shared\Repositories;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

abstract class AbstractRepositoryImpl implements AbstractRepository{
  protected $model;

  public function validateModel() {
    if(is_null($this->model)) {
      throw new \Exception("Set model first");
    }
  }

  public function getCurrentTimeInTimestamp() {
    return Carbon::now()->toDateTimeString();
  }

  public function insert($data_to_insert) {
    $this->validateModel();
    $is_success = $this->model::create($data_to_insert);
    if($is_success) return true;
    return false;
  }

  public function update($param,$data_to_update) {
    $this->validateModel();
    $is_success = $this->model::where($param)->update($data_to_update);
    if($is_success) return true;
    return false;
  }

  public function delete($param) {
    $this->validateModel();
    return $this->model::where($param)->delete();
  }

  public function model($model) {
    $this->model = $model;
  }
}
