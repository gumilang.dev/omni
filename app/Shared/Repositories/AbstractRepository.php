<?php namespace App\Shared\Repositories;

use Illuminate\Database\Eloquent\Model;

interface AbstractRepository {
  public function insert($data);
  public function update($param,$data_to_update);
  public function delete($param);
  public function model($model);
}
