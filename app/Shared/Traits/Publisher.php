<?php


namespace App\Shared\Traits;

use App\Shared\Messaging\RabbitMQ;

trait Publisher {
  /**
   * @param $broker
   * @return RabbitMQ|Error
   */
  public function publish($broker) {
    if($broker == "rabbitmq") {
      return new RabbitMQ();
    }else {
      throw new Error("Message broker not implemented");
    }
  }
}

