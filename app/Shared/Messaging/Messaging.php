<?php


namespace App\Shared\Messaging;

interface Messaging
{
  public function publish($message);
  public function setExchange($exchange);
  public function setQueue($queue);
  public function setExchangeType($exchange_type);
}
