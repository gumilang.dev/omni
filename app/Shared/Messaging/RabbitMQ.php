<?php


namespace App\Shared\Messaging;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class RabbitMQ
 * @package App\Shared\Messaging
 */
class RabbitMQ implements  Messaging
{
  /**
   * @var
   */
  protected  $exchange;
  protected  $queue;
  protected  $exchange_type;

  /**
   * @param $message
   * @throws \Exception
   */
  public function publish($message)
  {
    $connection = new AMQPStreamConnection(config("rabbitmq.host"), config("rabbitmq.port"), config("rabbitmq.user"), config("rabbitmq.password"), config("rabbitmq.vhost"));
    $channel = $connection->channel();

    $channel->queue_declare($this->queue, false, true, false, false);

    $channel->exchange_declare($this->exchange,$this->exchange_type, false, true, false);

    $channel->queue_bind($this->queue,$this->exchange);

    $message_body = json_encode($message);

    $message_to_sent = new AMQPMessage($message_body, array('content_type' => 'text/plain', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT));
    $channel->basic_publish($message_to_sent,$this->exchange);

    $channel->close();
    $connection->close();
  }

  /**
   * @param $exchange
   * @return $this
   */
  public function setExchange($exchange)
  {
    $this->exchange = $exchange;
    return $this;
  }

  /**
   * @param $queue
   * @return $this
   */
  public function setQueue($queue)
  {
    $this->queue = $queue;
    return $this;
  }

  /**
   * @param $exchange_type
   * @return $this
   */
  public function setExchangeType($exchange_type)
  {
    $this->exchange_type = $exchange_type;
    return $this;
  }
}
