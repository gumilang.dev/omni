<?php namespace app\Shared\Common;

use Carbon\Carbon;

class BaseService {
  protected function getCurrentTimeInTimestamp() {
    return Carbon::now()->toDateTimeString();
  }

  protected function dispatch($class) {
    broadcast($class);
  }
}
