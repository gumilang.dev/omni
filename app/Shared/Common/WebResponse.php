<?php namespace app\Shared\Common;

class WebResponse {
  public static function create($code = 200, $status = "OK", $data  = [], $message = "") {
    return response()->json([
      "code" => $code,
      "status" => $status,
      "data" => $data,
      "message" => $message
    ], $code);
  }

  public static function clean($response) {
    return response()->json($response,200);
  }
}
