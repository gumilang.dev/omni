<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RequestHsmTemplateNotification extends Notification
{
    use Queueable;
    private $data;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = env("APP_URL");
        $authUrl = env("AUTH_URL");
        return (new MailMessage)
                    ->from('noreply@lenna.ai')
                    ->subject('Request whatsapp HSM Template')
                    ->greeting('Hallo!')
                    ->line('A new whatsapp HSM Template has been requested from \'' .$this->data['app_name']. '\' app')
                    // ->line('Template Name : '.$this->data['template_name'])
                    ->markdown('mail.customMail', ['data' => $this->data])
                    ->action('Approve', url($authUrl.'/approve'.'/'.$this->data['token']))
                    ->line('Thank you');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
