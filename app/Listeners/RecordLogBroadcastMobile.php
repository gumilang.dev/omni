<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Modules\BroadcastMessage\Services\BroadcastMessageServiceImpl;

class RecordLogBroadcastMobile
{

    private $broadcastMessageServiceImpl;

    public function __construct(BroadcastMessageServiceImpl $broadcastMessageServiceImpl)
    {
      $this->broadcastMessageServiceImpl =  $broadcastMessageServiceImpl;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($data)
    {
      if($data->broadcasted_data["type"]  == "single") {
        $this->broadcastMessageServiceImpl->recordLog($data->status, [
          "channel_id" => 7,
          "type" => $data->broadcasted_data["type"],
          "category" => $data->broadcasted_data["category"],
          "sub_category" => $data->broadcasted_data["sub_category"],
          "client" => "mobile",
          "topics" => $data->broadcasted_data["topics"],
          "receiver_id" => $data->broadcasted_data["receiver_id"],
          "notification" => json_encode($data->broadcasted_data["notification"]),
          "data" => json_encode($data->broadcasted_data["data"]),
          "status" => $data->status,
          "app_id" => $data->broadcasted_data["app_id"],
          "integration_id" => $data->broadcasted_data["integration_id"],
          "send_by" => $data->send_by
        ]);
      }else if($data->broadcasted_data["type"] == "multiple") {
        $this->broadcastMessageServiceImpl->recordLog($data->status, [
          "channel_id" => 7,
          "type" => $data->broadcasted_data["type"],
          "category" => $data->broadcasted_data["category"],
          "sub_category" => $data->broadcasted_data["sub_category"],
          "client" => "mobile",
          "topics" => $data->broadcasted_data["topics"],
          "receiver_id" => $data->broadcasted_data["receiver_id"],
          "notification" => $data->broadcasted_data["notification"],
          "data" => $data->broadcasted_data["data"],
          "status" => $data->status,
          "app_id" => $data->broadcasted_data["app_id"],
          "integration_id" => $data->broadcasted_data["integration_id"],
          "send_by" => $data->send_by
        ]);
      }else {
        $this->broadcastMessageServiceImpl->recordLog($data->status, [
          "channel_id" => 7,
          "type" => $data->broadcasted_data["type"],
          "category" => $data->broadcasted_data["category"],
          "sub_category" => $data->broadcasted_data["sub_category"],
          "client" => "mobile",
          "topics" => $data->broadcasted_data["topics"],
          "receiver_id" => $data->broadcasted_data["receiver_id"],
          "notification" => $data->broadcasted_data["notification"],
          "data" => $data->broadcasted_data["data"],
          "status" => $data->status,
          "app_id" => $data->broadcasted_data["app_id"],
          "integration_id" => $data->broadcasted_data["integration_id"],
          "send_by" => $data->send_by
        ]);
      }
    }
}
