<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\TimeReportServedImpl;
use Carbon\Carbon;
use DB;


class RecordTimeServedByAgent
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    private $timeReportServed;

    public function __construct(TimeReportServedImpl $timeReportServed)
    {
      $this->timeReportServed = $timeReportServed;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($data)
    {

      if(!is_null($data->room->livechat))
      {

        // Todo: use cache
        $selected_agent = DB::table("auth.users")
                            ->where("id",$data->room->livechat->handle_by)
                            ->first();

        $start_time = Carbon::parse($data->room->livechat->request_at);
        $finish_time = Carbon::parse($data->room->livechat->start_at);

        $this->timeReportServed->createReport([
          "request_at" => $data->room->livechat->request_at,
          "served_at" => $data->room->livechat->start_at,
          "room_id" => $data->room->id,
          "agent_id" => $data->room->livechat->handle_by,
          "diff" => $finish_time->diffInMinutes($start_time),
          "app_id" => $data->room->app_id,
          "agent_name" => $selected_agent->name,
          "agent_email" => $selected_agent->email
        ]);
      }

    }
}
