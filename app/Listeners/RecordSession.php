<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\SessionServiceImpl;

class RecordSession
{
    /**
     * Create the event listener.
     *
     * @return void
     */

     private $sessionService;

    public function __construct(SessionServiceImpl $sessionService)
    {
      $this->sessionService = $sessionService;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($data)
    {
      $this->sessionService->createSession(["room_id" => $data->room->id, "start_at" => $data->room->livechat->start_at, "end_at" => $data->room->livechat->end_at, "handle_by" => $data->room->livechat->handle_by,'user_id' => $data->room->created_by->id]);
    }
}
