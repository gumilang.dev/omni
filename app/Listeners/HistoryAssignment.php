<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Modules\Livechat\Services\LivechatServiceImpl;

class HistoryAssignment
{

    private $livechatService;

    public function __construct(LivechatServiceImpl $livechatService)
    {
      $this->livechatService = $livechatService;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($data)
    {
      if($data->is_record_assignment) {
        $this->livechatService->recordAssignment($data->room, $data->assignee, $data->assignor, $data->assignor_role);
      } else if ($data->is_record_assignment_group) {
        $this->livechatService->recordAssignmentGroup($data->room, $data->assignor, $data->assignor_role, $data->group_id);
      }
    }
}
