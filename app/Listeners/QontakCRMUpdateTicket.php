<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Modules\QontakTicketing\Services\QontakTicketingService;
use App\Modules\CustomIntegration\Services\CustomIntegrationService;

class QontakCRMUpdateTicket
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    private $qontakTicketingService;
    private $customIntegrationService;

    public function __construct(QontakTicketingService $qontakTicketingService,CustomIntegrationService $customIntegrationService)
    {
      $this->qontakTicketingService = $qontakTicketingService;
      $this->customIntegrationService = $customIntegrationService;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($data)
    {
      if($this->customIntegrationService->isIntegrated($data->room->app_id,"qontak-crm")) {
        $this->qontakTicketingService->updateTicket([
          "email" => $data->room->created_by->email,
          "room_id" => $data->room->id,
          "name" => $data->room->created_by->name,
          "chat_history" => $this->qontakTicketingService->getHistoryChat($data->room->id,$data->room->livechat->start_at,$data->room->livechat->end_at),
          "phone" => $data->room->created_by->phone,
          "nickname" => $data->room->created_by->nickname,
          "contact_id" => $data->room->created_by->contact_id,
          "type" => "update",
          "session" => [
            "start_at" => $data->room->livechat->start_at,
            "end_at" => $data->room->livechat->end_at,
          ]
        ],
          $data->room->id
        );
      }
    }
}
