<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Modules\BroadcastMessage\Services\BroadcastMessageServiceImpl;

class RecordLogBroadcastWhatsapp
{

    private $broadcastMessageService;

    public function __construct(BroadcastMessageServiceImpl $broadcastMessageService)
    {
      $this->broadcastMessageService = $broadcastMessageService;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($data)
    {
      $this->broadcastMessageService->recordLogWhatsapp(
        $data->type,
        $data->user,
        $data->response_from_provider,
        $data->data,
        $data->app_id,
        $data->integration_id
      );
    }
}
