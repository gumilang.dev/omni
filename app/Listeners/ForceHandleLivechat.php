<?php

namespace App\Listeners;

use App\Services\LogHandleRoomServiceImpl;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForceHandleLivechat
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $logHandleRoom;

    public function __construct(LogHandleRoomServiceImpl $logHandleRoom)
    {
      $this->logHandleRoom = $logHandleRoom;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($data)
    {
      if(!is_null($data->administrator_id)) {
        $this->logHandleRoom->createLog([
          "app_id" => $data->room->app_id,
          "room_id" => $data->room->id,
          "user_id" => $data->administrator_id
        ]);
      }
    }
}
