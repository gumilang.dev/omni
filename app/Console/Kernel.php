<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Modules\Log\Models\WebhookLog;
use App\Modules\Log\Models\ApiLog;
use App\Modules\Audit\Models\Audit;
use App\Modules\Log\Models\TelescopeEntry;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
      // delete laravel entries
      $schedule->command('telescope:prune --hours=24')->dailyAt('1:00');

      // delete audit
      $schedule->command('command:removeaudit')->weeklyOn(6, '1:00');

      // delete api log
      // $schedule->command('command:removeapilog')->weeklyOn(6, '1:00');

      // delete webhook log
      // $schedule->command('command:removewebhooklog')->weeklyOn(6, '1:00');

      // auto resolve livechat
      $schedule->command('command:autoresolvelivechat')->everyMinute();

      // check socket
      // $schedule->command('command:checksocket')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
