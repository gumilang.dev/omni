<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Modules\Room\Models\Room;
use Carbon\Carbon;
use Hashids\Hashids;
use GuzzleHttp\Client;
use App\Jobs\AutoFollowUpLivechatAndResolve;
use App\Modules\App\Models\App;



class AutoResolveLivechat extends Command
{

    public function __construct(Client $httpClient) {
      parent::__construct();
      $this->hashService = new Hashids('',6);
      $this->httpClient = $httpClient;
    }
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:autoresolvelivechat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for auto resolve livechat';


    public function executeAutoResolve($auto_resolve_time,$auto_resolve_message,$lastMessage,$room,$appId) {
      $to = Carbon::createFromFormat("Y-m-d H:i:s",Carbon::now('Asia/Jakarta')->toDateTimeString());
      $from = Carbon::createFromFormat("Y-m-d H:i:s",$lastMessage->created_at);
      $diffInMinutes = $from->diffInMinutes($to, false);
      if ($lastMessage->messageable_type  == "user_platform") {
        if ($diffInMinutes > $auto_resolve_time) {
          $this->autoResolve($room, $appId, $auto_resolve_message, $lastMessage);
          // AutoFollowUpLivechatAndResolve::dispatch($room,$appId,$auto_resolve_message,$lastMessage);
        }
      }
    }

    public function autoResolve($room, $appId, $auto_resolve_message, $lastMessage)
    {
      $hashService = new Hashids("",6);
      $httpClient = new Client();
      $urlSendMessage = config("app.url")."/api/".$hashService->encode($appId)."/chat/send-message-auto-followup";
      $responseSendMessage = $httpClient->request(
        "POST",
        $urlSendMessage,
        [
          "json" => [
            "roomId" => $room->id,
            "senderId" => $room->livechat->handle_by,
            "temporary_id" => Carbon::now('Asia/Jakarta')->timestamp,
            "message" => [(object)[
              "type" => "text",
              "speech" => $auto_resolve_message,
              "text" => $auto_resolve_message
            ]]
          ]
        ]
      );

      $urlAutoResolve = config("app.url")."/api/".$hashService->encode($appId)."/livechat/resolve";
      $responseAutoResolve = $httpClient->request(
        "POST",
        $urlAutoResolve,
        [
          "json" => [
            "room_id"  => $room->id,
            "updated_by" => $lastMessage->messageable_id,
          ]
        ]
      );
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $apps = App::where("auto_resolve",true)->get();

      foreach($apps as $each_app) {
        $rooms = Room::where("app_id",$each_app->id)->whereHas("liveChats",function($query) {
          $query->where('status', 'live')->whereNull('end_at')->whereNotNull('request_at');
        })->whereHas("users")->orderBy("updated_at","DESC")->get(["id","created_by","created_at"]);
        foreach($rooms as $each_room) {
          $this->executeAutoResolve($each_app->auto_resolve_time,$each_app->auto_resolve_message,$each_room->messages[0],$each_room,$each_app->id);
        }
      }
    }
}
