<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use App\Modules\ExternalApi\Models\ExternalApi;
use App\Notifications\SocketNotification;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use App\Modules\User\Models\UserPlatform;
use Illuminate\Support\Facades\Notification;

class SocketChecker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:checksocket';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Socket';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      if (env('APP_ENV') == 'production') {
        $lennaApi = ExternalApi::where(['category' => 'internal', 'provider' => 'socket'])->first();
        $guzzleProp = ['base_uri' => $lennaApi->base_url, 'verify' => false];
        $client = new Client($guzzleProp);
        $apiProp = $lennaApi->endpoints()->where('name', 'check-status')->first();
        $socketKey = env('SOCKET_KEY');
        $socketAppId = env('SOCKET_APP_ID');
        $endpoint = str_replace("{socket-appid}", $socketAppId, $apiProp->endpoint);
        try {
          $response = $client->request(
              $apiProp->method,
              $endpoint,
              [
                  'headers' => [
                    'Authorization' => 'Bearer '.$socketKey,
                  ]
              ]
          );
          $response = json_decode($response->getBody()->getContents());
        } catch (RequestException $e) {
          $response = false;
        } catch (ServerException $e) {
          $response = false;
        } catch (ClientException $e) {
          $response = false;
        }
        if ($response == false) {
          if (env('APP_ENV') == 'production') {
            $users = ['ival@lenna.ai','agus@lenna.ai','haidar@lenna.ai','yusuf@lenna.ai'];
            foreach ($users as $key => $value) {
              $data[] = UserPlatform::where('email', $value)->first();
            }
            Notification::send($data, new SocketNotification(['url' => $lennaApi->base_url,'status' => 'Down']));
            // foreach ($user as $key => $value) {
            //   $value->notify(new SocketNotification(['url' => $lennaApi->base_url,'status' => 'Down']));
            // }
            $process = new Process('sh /var/www/sites/echo-server/socket.sh');
            $process->run();
            // exec("cd /var/www/sites/echo-server && pm2 restart socket.sh");
          }
        }
      }
    }
}
