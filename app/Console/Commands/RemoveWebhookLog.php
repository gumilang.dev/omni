<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Modules\Log\Models\WebhookLog;
use Carbon\Carbon;

class RemoveWebhookLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:removewebhooklog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove table webhook log on schema omnichannel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("Cron remove Webhook Log started");
        WebhookLog::where('created_at', '<', Carbon::now('Asia/Jakarta')->subMonths(3))->delete();
        \Log::info("Cron remove Webhook Log successfully");
        $this->info('remove:webhooklog Command Run successfully!');
    }
}
