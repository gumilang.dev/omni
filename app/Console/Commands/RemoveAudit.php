<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Modules\Audit\Models\Audit;
use Carbon\Carbon;

class RemoveAudit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:removeaudit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove table audit on schema omnichannel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("Cron remove Audit started");
        Audit::where('created_at', '<', Carbon::now('Asia/Jakarta')->subDays(14))->delete();
        \Log::info("Cron remove Audit successfully");
        $this->info('remove:audit Command Run successfully!');
    }
}
