<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Modules\Log\Models\ApiLog;
use Carbon\Carbon;

class RemoveApiLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:removeapilog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove table apilog on schema omnichannel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("Cron remove Api Log started");
        ApiLog::where('created_at', '<', Carbon::now('Asia/Jakarta')->subMonths(3))->delete();
        \Log::info("Cron remove Api Log run Successfully");
        $this->info('remove:apilog Command Run successfully!');
    }
}
