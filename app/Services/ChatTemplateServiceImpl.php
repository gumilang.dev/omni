<?php namespace App\Services;

use App\Contract\Services\ChatTemplateService;
use App\Repositories\ChatTemplateRepository;

class ChatTemplateServiceImpl implements ChatTemplateService {

  public $chatTemplateRepository;

  public function __construct(ChatTemplateRepository $chatTemplateRepository)
  {
    $this->chatTemplateRepository = $chatTemplateRepository;
  }

  public function paginate($request,$whereClause,$search)
  {
    return $this->chatTemplateRepository->paginate($request,$whereClause,$search);
  }

  public function store($data)
  {
      return $this->chatTemplateRepository->store($data);
  }

  public function destroy($param)
  {
    return $this->chatTemplateRepository->destroy($param);
  }

  public function update($param,$data)
  {
    return $this->chatTemplateRepository->update($param,$data);
  }

}
