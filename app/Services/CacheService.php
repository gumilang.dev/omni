<?php  namespace App\Services;

use App\Contract\Services\CacheWriter;
use Illuminate\Support\Facades\Redis;
class CacheService implements CacheWriter {

  public $intance = null;

  public function getInstance() {
    if($this->instance == null) {
      return new Redis();
    }
    return $this->instance;
  }
  public function provider() {
    return $this->getInstance();
  }
  public  function create($data) {
    return Redis::set($data["value"],$data["key"],"EX",2592000);
  }
  public  function find() {
    return Redis::keys("*");
  }
  public  function findBy($key) {
    return Redis::get($key);
  }
  public  function update($param,$data) {
    $this->destroy($param);
    return $this->create([
      $param => $data
    ]);
  }
  public  function destroy($key) {
    return Redis::del($key);
  }
}
