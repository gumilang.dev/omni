<?php namespace App\Services;

use App\Contract\Services\SegmentService;
use App\Repositories\SegmentRepository;
use App\Services\ParserServiceImpl;
use App\Modules\User\Models\User;

class SegmentServiceImpl implements SegmentService {

  public $repo;
  private $parserService;
  private $user;

  public function __construct(SegmentRepository $repo,ParserServiceImpl $parserService,User $user) {
      $this->repo = $repo;
      $this->parserService = $parserService;
      $this->user = $user;
  }


  public function show($appId,$id) {
    $response = $this->repo->findBy(['id' => $id])->first();
    $schema   = $this->createSchema($response);
    $data = $this->user::with('channel')->where(function($query) use ($appId,$schema){
      $query->where('app_id',$appId);
      foreach($schema as $value) {
        $modifiedValue;
        if($value[1] == "LIKE") {
          $modifiedValue = "%".$value[2]."%";
        }else if($value[1] == "not like") {
          $modifiedValue = "%".$value[2]."%";
        }else{
          $modifiedValue = $value[2];
        }
        $query->where($value[0],$value[1],$modifiedValue);
      }
    })->paginate(15);
    return $data;
  }

  public function showAll($appId,$id) {
    $response = $this->repo->findBy(['id' => $id])->first();
    $schema   = $this->createSchema($response);
    $data = $this->user::with('channel')->where(function($query) use ($appId,$schema) {
      $query->where('app_id', $appId);
      foreach ($schema as $value) {
        $modifiedValue;
        if($value[1] == 'LIKE') {
          $modifiedValue = "%".$value[2]."%";
        }else if($value[1] ==  "not like") {
          $modifiedValue = "%".$value[2]."%";
        }else {
          $modifiedValue = $value[2];
        }
        $query->where($value[0],$value[1],$modifiedValue);
      }
    })->get();
    return $data;
  }

  public function createSchema($value) {
    $parsedToJson = $this->parserService->toJSON($value["query"]);
    $data = [];
    foreach($parsedToJson as $value) {
      array_push($data,[$value["column"] ,$this->changeCondition($value["condition"]), $value["value"]]);
    }
    return $data;
  }

  public function changeCondition($value) {
    if($value == "containts" || $value == "containt" || $value == "contain") {
      return "LIKE";
    }elseif($value == "equals") {
      return "=";
    }elseif($value == "not equals") {
      return "!=";
    }else if($value == "not contains" || $value == "not containt" || $value == "not contain") {
      return "not like";
    }
  }

  public function index($param) {
    return $this->repo->findBy($param);
  }

  public function store($data)
  {
    return $this->repo->store($data);
  }

  public function destroy($param)
  {
    return $this->repo->destroy($param);
  }

  public function update($param,$data)
  {
    return $this->repo->update($param,$data);
  }

}
