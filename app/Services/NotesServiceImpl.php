<?php namespace App\Services;

use App\Contract\Services\NotesService;
use App\Repositories\NotesRepository;

class NotesServiceImpl implements NotesService {

  public $repo;

  public function __construct(NotesRepository $repo) {
    $this->repo = $repo;
  }

  public function store($data) {
    return $this->repo->store($data);
  }

  public function update($param,$data) {
    return $this->repo->update($param,$data);
  }

}
