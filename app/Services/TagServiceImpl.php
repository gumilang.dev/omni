<?php namespace App\Services;

use App\Contract\Services\TagService;
use App\Repositories\TagRepository;

class TagServiceImpl implements TagService {
  public $tagRepository;

  public function __construct(TagRepository $tagRepository) {
      $this->tagRepository = $tagRepository;
  }

  public function paginate($request,$whereClause,$search) {
    return $this->tagRepository->paginate($request,$whereClause,$search);
  }

  public function store($data)
  {
      return $this->tagRepository->store($data);
  }

  public function destroy($param)
  {
    return $this->tagRepository->destroy($param);
  }

  public function update($param,$data)
  {
    return $this->tagRepository->update($param,$data);
  }

}
