<?php namespace App\Services;

use App\Contract\Services\TicketService;
use App\Repositories\TicketRepository;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class TicketServiceImpl implements TicketService {

  protected $repo;

  public function __construct(TicketRepository $repo){
    $this->repo = $repo;
  }

  public function createTicketId() {
    $time = date("m-d-Y");
    return $time."-".str_random(5);
  }

  public function filterTicket($app_id,$condition,$per_page) {
    return $this->repo->entity::where("app_id",$app_id)
		    ->when($condition["status"],function($query) use($condition) {
				  $query->where("status",$condition["status"]);
				})
				->when($condition["priority"],function($query) use($condition) {
				  $query->where("priority",$condition["priority"]);
				})
				->when($condition["type"],function($query) use($condition){
				  $query->where("type",$condition["type"]);
				})
				->when($condition["agent_id"],function($query) use($condition) {
				  $query->where("agent_id",$condition["agent_id"]);
				})
				->when($condition["solved_at"],function($query) use($condition) {
          if($condition["solved_at"]->start !== "" && $condition["solved_at"]->end !== "") {
            $query->whereDate('solved_at', '>=', $condition["solved_at"]->start);
            $query->whereDate('solved_at', '<=', $condition["solved_at"]->end);
          }
				})
				->when($condition["created_at"],function($query) use($condition){
          if($condition["created_at"]->start !== "" && $condition["created_at"]->end !== "") {
            $query->whereDate('created_at', '>=', $condition["created_at"]->start);
            $query->whereDate('created_at', '<=', $condition["created_at"]->end);
          }
				})
        ->paginate($per_page);
// 				  $query->whereDate("solved_at",$condition["solved_at"]);
// 				})
// 				->when($condition["created_at"],function($query) use($condition){
// 				  $query->whereDate("created_at",$condition["created_at"]);
// 				})
//                                ->paginate($per_page);
  }

  public function createTicket($request,$appId) {
    $ticket_id = is_null($request->ticket_id) ? $this->createTicketId() : $request->ticket_id ;
    if($request->media == "chat") {
      $data = [
        "subject" => $request->subject,
        "status" => $request->status,
        "room_id" => $request->room_id,
        "type" => $request->type,
        "agent_id" => $request->agent_id,
        "app_id" => $appId,
        "priority" => $request->priority,
        "media" => $request->media,
        "user_id" => $request->user_id,
        "start_at" => $request->start_at,
        "description" => $request->description,
        "ticket_id" => $ticket_id
      ];
    }else {
      $data = [
        "subject" => $request->subject,
        "status" => $request->status,
        "type" => $request->type,
        "agent_id" => $request->agent_id,
        "app_id" => $appId,
        "priority" => $request->priority,
        "media" => $request->media,
        "user_info" => $request->input("user_info"),
        "files" => $request->input("files"),
        "start_at" => $request->start_at,
        "description" => $request->description,
        "ticket_id" => $ticket_id
      ];
    }
    return $this->repo->store($data);
  }

  public function updateTicket($param,$request) {
    $current_time = Carbon::now()->toDateTimeString();
    $solved_at = $request->status == "closed"  ? $current_time : "";
    $resolution_time = $request->status == "closed" ? Carbon::parse($current_time)->diffForHumans(Carbon::parse($request->created_at)) : "";
    if($request->media == "chat") {
      $data = [
        "subject" => $request->subject,
        "status" => $request->status,
        "agent_id" => $request->agent_id,
        "type" => $request->type,
        "room_id" => $request->room_id,
        "priority" => $request->priority,
	      "start_at" => $request->start_at,
	      "solved_at" => $solved_at,
        "description" => $request->description,
        "resolution_time" => $resolution_time,
        "ticket_id" => $request->ticket_id
      ];
    }else{

      $deleted_files = $request->input("deleted_files");
      foreach($deleted_files as $each_deleted_files) {
        Storage::disk('public_chat')->delete([$each_deleted_files["fileName"]]);
      }

      $data = [
        "subject" => $request->subject,
        "status" => $request->status,
        "type" => $request->type,
        "priority" => $request->priority,
        "agent_id" => $request->agent_id,
        "user_info" => json_encode($request->input("user_info")),
        "files" => json_encode($request->input("files")),
        "description" => $request->description,
	      "solved_at" => $solved_at,
        "resolution_time" => $resolution_time,
        "ticket_id" => $request->ticket_id
      ];
    }

    return $this->repo->update($param,$data,[
      "subject" => "required"
    ]);
  }

  public function deleteTicket($param) {
    return $this->repo->destroy($param);
  }

  public function getTicket($request,$whereClause,$search) {
    return $this->repo->paginate($request,$whereClause,$search);
  }

  public function getBy($param) {
   return $this->repo->findBy($param);
  }

  public function getInformationByMedia($id) {
    return $this->repo->getInformationByMedia($this->repo->find($id));
  }

  public function exportTicket($app_id,$condition) {
     return $this->repo->entity::where("app_id",$app_id)
        ->when($condition["status"],function($query) use($condition) {
          $query->where("status",$condition["status"]);
        })
        ->when($condition["priority"],function($query) use($condition) {
          $query->where("priority",$condition["priority"]);
        })
        ->when($condition["type"],function($query) use($condition){
          $query->where("type",$condition["type"]);
        })
        ->when($condition["agent_id"],function($query) use($condition) {
          $query->where("agent_id",$condition["agent_id"]);
        })
        ->when($condition["solved_at"],function($query) use($condition) {
          if($condition["solved_at"]->start !== "" && $condition["solved_at"]->end !== "") {
            $query->whereDate('solved_at', '>=', $condition["solved_at"]->start);
            $query->whereDate('solved_at', '<=', $condition["solved_at"]->end);
          }
        })
        ->when($condition["created_at"],function($query) use($condition){
          if($condition["created_at"]->start !== "" && $condition["created_at"]->end !== "") {
            $query->whereDate('created_at', '>=', $condition["created_at"]->start);
            $query->whereDate('created_at', '<=', $condition["created_at"]->end);
          }
        })
        ->get();
  }
}
