<?php namespace App\Services;

use App\Contract\Services\AgentService;
use App\Repositories\AgentRepository;
use Carbon\Carbon;

class AgentServiceImpl implements AgentService{

  public $repo;

  public function __construct(AgentRepository $repo) {
    $this->repo = $repo;
  }

  public function assign($param,$data,$livechat) {
    if($livechat == null) {
      return $this->repo->store([
        "room_id" => $param["room_id"],
        "handle_by" => $data["handle_by"],
        "updated_by" => $data["updated_by"],
        "status" => $data["status"],
        "start_at" => Carbon::now()->toDateTimeString(),
        "request_at" => Carbon::now()->toDateTimeString()
      ]);
    }else{
      return $this->repo->update($param,$data);
    }
  }

  public function changeStatus($data){
    return $this->repo->changeStatus($data);
  }

  public function isAgentOnline($data) {
    return $this->repo->isOnline($data);
  }
}
