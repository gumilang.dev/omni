<?php namespace App\Services;

use App\Contract\Services\LogHandleRoomService;
use App\Repositories\LogHandleRoomRepository;
use App\Services\PaginatorServiceImpl;

class LogHandleRoomServiceImpl implements LogHandleRoomService{

  public function __construct() {
    $this->repo = new LogHandleRoomRepository();
    $this->paginator = new PaginatorServiceImpl();
  }

  public function createLog($data) {
    return $this->repo->store($data);
  }

  public function reportLog($request,$whereClause,$search = []) {
    $sort = $request->get('sort');
    $filterText = $request->get('filterText');
    $per_page = $request->get('per_page');
    $startFilterDate = $request->get('start');
    $endFilterDate = $request->get('end');

    $data = $this->repo->raw()::where('app_id', $whereClause["app_id"])->with(['user','room'])->where(function($query) use ($request,$startFilterDate,$endFilterDate){
      if($startFilterDate && $endFilterDate) {
        $query->whereDate('created_at', '>=', $startFilterDate);
        $query->whereDate('created_at', '<=', $endFilterDate);
      }
    })->orderBy("created_at","DESC")->get();


    if($filterText) {
      $data = $data->filter(function ($item) use ($filterText) {
          return false !== stristr($item->room->created_by["name"], $filterText) || false !== stristr($item->user["name"], $filterText)  || false !== stristr($item->user["email"], $filterText);
      })->values();
    }

    if (!$per_page) {
        $per_page = 15;
    }

    return $this->paginator->paginate($data,$per_page);
  }

}
