<?php namespace App\Services;

use App\Contract\Services\SessionService;
use App\Repositories\SessionRepository;

class SessionServiceImpl implements SessionService {

  protected $repo;
  public function __construct(SessionRepository $sessionRepo)  {
    $this->repo = $sessionRepo;
  }
  public function transform($request) {
    return [
      "room_id" => $request->room_id,
      "handle_by" => $request->handle_by,
      "start_at" => $request->start_at,
      "end_at" => $request->end_at
    ];
  }
  public function createSession($data) {
    return $this->repo->store($data);
  }
}
