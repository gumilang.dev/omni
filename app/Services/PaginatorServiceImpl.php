<?php namespace App\Services;

use App\Contract\Services\PaginatorService;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class PaginatorServiceImpl implements PaginatorService {
  public function paginate($items, $perPage, $page = null, $options = []) {
    $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
    $items = $items instanceof Collection ? $items : Collection::make($items);
    return new LengthAwarePaginator($items->forPage($page, $perPage)->values(), $items->count(), $perPage, $page, [
      'path' => LengthAwarePaginator::resolveCurrentPath()
    ]);
  }
}
