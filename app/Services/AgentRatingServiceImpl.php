<?php namespace App\Services;

use App\Repositories\AgentRatingRepository;

class AgentRatingServiceImpl {
  private $repo;

  public function __construct(AgentRatingRepository $repo) {
    $this->repo = $repo;
  }

  public function createRating($data) {
   return $this->repo->store($data);
  }
}
