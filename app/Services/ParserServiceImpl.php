<?php namespace App\Services;

class ParserServiceImpl {
  public function toJSON($value) {
    return json_decode($value,true);
  }
}