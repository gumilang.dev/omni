<?php namespace App\Services;

use App\Services\CacheService;
use Log;
class BlockUserService {

  const CACHE_KEY = "BLOCKED_USER";

  public function __construct(CacheService $cache) {
    $this->cache = $cache;
  }

  public  function get() {
    $blockedUser = $this->cache->find();
    $data = [];
    for($i = 0; $i < count($blockedUser); $i++) {
      array_push($data,[
        "key" => $this->cache->findBy($blockedUser[$i]),
        "value" => $blockedUser[$i]
      ]);
    }
    return $data;
  }

  public function isBlocked($credential) {
    $isBlocked = $this->cache->findBy($credential);
    if($isBlocked) return true;
    return false;
  }

  public function block($data) {
    $alreadyBlocked = $this->isBlocked($data["value"]);
    if($alreadyBlocked) {
      return false;
    }else{
      $block = $this->cache->create($data);
      if($block) return true;
      return false;
    }

  }

  public function unblock($param) {
    $unblock = $this->cache->destroy($param);
    if($unblock) return true;
    return false;
  }

}
