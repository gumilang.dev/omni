<?php namespace App\Services;

use App\Contract\Services\TimeReportServedService as Service;
use App\Repositories\TimeReportServedRepository as Repository;

class TimeReportServedImpl implements Service{
  public function __construct(Repository $repository) {
    $this->repo = $repository;
    $this->rules = [
      "request_at" => "required",
      "served_at" => "required",
      "room_id" => "required",
      "agent_id" => "required"
    ];
  }

  public function createReport($data) {
    return $this->repo->store($data);
  }

  public function getTimeReportServed($request,$whereClause,$search) {
    return $this->repo->paginate($request,$whereClause,$search);
  }

  public function getTimeReportServedExport($request,$whereClause,$search) {
    $filterText = $request->get('filterText');
    $per_page = $request->get('per_page');
    $start = $request->get("start");
    $end = $request->get("end");
    $data = $this->repo->entity;

    if ($filterText) {
      $data = $this->repo->search($search,$filterText);
    }

    if (!empty($start) && !empty($end)) {
      $data = $data->whereDate("created_at",">=",$start)->whereDate("created_at","<=",$end);
    }

    return $data->where($whereClause)->orderBy("created_at","DESC")->get();
  }
}
