<?php namespace App\Services;

use App\Contract\Services\OnlineHistoryService;
use App\Repositories\OnlineHistoryRepository as Repository;

class OnlineHistoryServiceImpl implements OnlineHistoryService {

  public function __construct(Repository $repo) {
    $this->repo = $repo;
  }

  public function findHistory($param) {
    return $this->repo->findBy($param);
  }

  public function createHistory($data) {
    // $history = $this->findHistory([
    //   "app_id" => $data["app_id"],
    //   "user_id" => $data["user_id"],
    //   "status" => $data["status"]
    // ]);
    // return $history;
    return $this->repo->store($data);
  }

  public function updateHistory($data) {

  }
}
