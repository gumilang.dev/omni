<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Hashids\Hashids;
use GuzzleHttp\Client;
use Carbon\Carbon;
use App\Modules\Livechat\Controllers\LivechatController;

class AutoFollowUpLivechatAndResolve implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $room;
    protected $appId;
    protected $message;
    protected $lastMessage;

    public function __construct($room,$appId,$message,$lastMessage)
    {
        $this->room = $room;
        $this->appId = $appId;
        $this->message = $message;
        $this->lastMessage = $lastMessage;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $hashService = new Hashids("",6);
      $httpClient = new Client();
      $urlSendMessage = Config("app.url")."/api/".$hashService->encode($this->appId)."/chat/send-message-auto-followup";

      $responseSendMessage = $httpClient->request(
        "POST",
        $urlSendMessage,
        [
          "json" => [
            "roomId" => $this->room->id,
            "senderId" => $this->room->livechat->handle_by,
            "temporary_id" => Carbon::now('Asia/Jakarta')->timestamp,
            "message" => [(object)[
              "type" => "text",
              "speech" => $this->message,
              "text" => $this->message
            ]]
          ]
        ]
      );


      $urlAutoResolve = Config("app.url")."/api/".$hashService->encode($this->appId)."/livechat/resolve";
      $responseAutoResolve = $httpClient->request(
        "POST",
        $urlAutoResolve,
        [
          "json" => [
            // "agentId"  => $this->room->livechat["handle_by"],
            // "description" => "resolved",
            // "liveChatId" => $this->room->livechat["id"],
            // "userId" => $this->room->created_by["id"]
            "room_id"  => $this->room->id,
            "updated_by" => $this->lastMessage->messageable_id,
          ]
        ]
      );

    }
}
