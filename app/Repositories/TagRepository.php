<?php namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Modules\Tag\Model\Tag;

class TagRepository extends BaseRepository{


  public function __construct(Tag $entity) {
    $this->entity = $entity;
    $this->rules = [
      'name' => 'required|string|max:50',
      'app_id' => 'required'
    ];
  }

}
