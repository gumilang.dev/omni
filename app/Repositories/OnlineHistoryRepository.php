<?php namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Modules\OnlineHistory\Model\OnlineHistory;

class OnlineHistoryRepository extends BaseRepository{
  public function __construct(OnlineHistory $entity) {
    $this->entity = $entity;
    $this->rules  = [
      "app_id" => "required",
      "status" => "required"
    ];
  }
}
