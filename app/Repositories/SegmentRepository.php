<?php namespace App\Repositories;

use App\Modules\Segment\Model\Segment;
use App\Repositories\BaseRepository;

class SegmentRepository extends BaseRepository {

  public function __construct(Segment $entity) {
    $this->entity = $entity;
    $this->rules = [
        'name' => 'required|string|max:50',
        'query' => 'required'
    ];
  }

}
