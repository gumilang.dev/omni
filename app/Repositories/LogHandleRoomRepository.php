<?php namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\LogHandleRoom;

class LogHandleRoomRepository extends BaseRepository{
  public function __construct() {
    $this->entity = new LogHandleRoom();
    $this->rules = [
      "room_id" => function($attribute,$value,$fail) {
        if($value == "") {
          return $fail($attribute . ' should be required.');
        }
        if (!is_int($value)) {
          return $fail($attribute . ' should be integer.');
        }
      },
      "user_id" => function($attribute,$value,$fail) {
        if($value == "") {
          return $fail($attribute . ' should be required.');
        }
        if (!is_int($value)) {
          return $fail($attribute . ' should be integer.');
        }
      },
      "app_id" => function($attribute,$value,$fail) {
        if($value == "") {
          return $fail($attribute . ' should be required.');
        }
        if (!is_int($value)) {
          return $fail($attribute . ' should be integer.');
        }
      }
    ];
  }
}
