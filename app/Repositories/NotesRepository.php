<?php namespace App\Repositories;

use App\Modules\Notes\Model\Notes;
use App\Repositories\BaseRepository;

class NotesRepository extends BaseRepository {

  public function __construct(Notes $entity) {
    $this->entity = $entity;
    $this->rules = [
      'room_id' => "required",
      'content' => 'required|string'
    ];
  }

}