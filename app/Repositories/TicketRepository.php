<?php namespace App\Repositories;

use App\Modules\Ticket\Models\Ticket;
use App\Repositories\BaseRepository;

class TicketRepository extends BaseRepository {

  public function __construct(Ticket $entity) {
    $this->entity = $entity;
    $this->rules = [
        'subject' => 'required',
        'status' => 'required',
        'type' => 'required',
        'agent_id' => 'required',
        'priority' => 'required',
        'media' => 'required'
    ];
  }

  public function getInformationByMedia($data) {
    if($data["media"] == "chat") {
      return $this->entity->chat($data);
    }
  }

  public function getFilteredTicket($app_id,$status,$priority,$type,$agent_id,$solved_at,$created_at) {

  }

}
