<?php namespace App\Repositories;


use App\Repositories\BaseRepository;
use App\Modules\ChatTemplate\Models\ChatTemplate;

class ChatTemplateRepository extends BaseRepository {
  public function __construct(ChatTemplate $entity) {
    $this->entity = $entity;
    $this->rules = [
        'key' => 'required|string|max:50',
        'content' => 'required'
    ];
  }

}
