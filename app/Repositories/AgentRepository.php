<?php namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Modules\Livechat\Models\Livechat;
use App\Modules\User\Models\UserApp;
use DB;
use App\Modules\Group\Models\Group;

class AgentRepository extends BaseRepository {
  public function __construct(Livechat $entity) {
    $this->entity = $entity;
    $this->rules =  [
      'room_id' => 'required',
      'handle_by' => 'required'
    ];
  }


  public function changeStatus($data) {
    return UserApp::where([
      "user_id" => $data["agent_id"],
      "app_id" => $data["app_id"]
    ])->update([
      "online" => $data["online"],
      "reason_offline" => $data["reason_offline"]
    ]);
  }

  public function isOnline($data) {
    $user = DB::table("omnichannel.app_user_platform as o")
            ->where("o.app_id", $data["app_id"])
            ->where("o.user_id",$data["user_id"])
            ->first();
    return $user->online;
  }
}
