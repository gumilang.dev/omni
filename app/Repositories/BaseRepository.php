<?php namespace App\Repositories;

use App\Contract\Repositories\IBaseRepository;
use Illuminate\Support\Facades\Validator;

class BaseRepository implements IBaseRepository{


  public function validate($data) {
    $validator = Validator::make(
      $data,
      $this->rules
    );
    if($validator->fails()) {
      return [
        'status' => false,
        'messages' => $validator->messages()
      ];
    }
    return [
      'status' => true
    ];
  }

  public function raw() {
    return $this->entity;
  }

  public function store($data)
  {
    $validate = $this->validate($data);
    if($validate['status']) {
      return $this->entity::create($data);
    }
    return [
      "error" => true,
      "message" => $validate["messages"]
    ];
  }

  public function all()
  {
    return $this->entity::all();
  }

  public function find($id) {
    return $this->entity->find($id);
  }

  public function findBy($param)
  {
    return $this->entity::where($param)->get();
  }

  public function destroy($param)
  {
    return $this->entity::where($param)->delete();
  }

  public function update($param,$data,$validation = [])
  {
    $currentRules = $this->rules;
    if(count($validation) > 0) {
      $this->rules = $validation;
      $validate =  $this->validate($data);
      if($validate['status']) {
      return $this->entity::where($param)->update($data);
      }
      return [
        "error" => true,
        "message" => $validate["messages"]
      ];
    }
    return $this->entity::where($param)->update($data);
    $this->rules = $currentRules;
  }

  public function search($param,$keyword)
  {
    return $this->entity->where(function($query) use ($param,$keyword) {
      foreach($param as $key => $value) {
        if($key == 0) {
          $query->where($value , 'ILIKE', '%'. $keyword . '%');
        }else {
          $query->orWhere($value, 'ILIKE' , '%'. $keyword . '%');
        }
      }
    });
  }

  public function paginate($request,$whereClause = null,$search)
  {
    $sort = $request->get('sort');
    $filterText = $request->get('filterText');
    $paginate = $request->get('paginate') ?? false;
    $per_page = $request->get('per_page');
    $start = $request->get("start");
    $end = $request->get("end");
    $data = $this->entity;

    if ($sort) {
        $sort = explode('|', $sort);
        $data = $data->orderBy($sort[0], $sort[1]);
    }

    $data = $data->orderBy("created_at","DESC");


    if ($filterText) {
        $data = $this->search($search,$filterText);
    }

    if (!$per_page) {
        $per_page = 15;
    }
    if($start) {
      $data = $data->whereDate("created_at",">=",$start)->whereDate("created_at","<=",$end);
    }

    if($whereClause === null) {
      return $data->paginate($per_page);
    }

    return $data->where($whereClause)->paginate($per_page);
  }

  public function simplePaginate($request,$whereClause = null,$search)
  {
    $sort = $request->get('sort');
    $filterText = $request->get('filterText');
    $paginate = $request->get('paginate') ?? false;
    $per_page = $request->get('per_page');
    $start = $request->get("start");
    $end = $request->get("end");
    $data = $this->entity;

    if ($sort) {
        $sort = explode('|', $sort);
        $data = $data->orderBy($sort[0], $sort[1]);
    }

    $data = $data->orderBy("created_at","DESC");


    if ($filterText) {
        $data = $this->search($search,$filterText);
    }

    if (!$per_page) {
        $per_page = 15;
    }

    if($start) {
      $data = $data->whereDate("created_at",">=",$start)->whereDate("created_at","<=",$end);
    }

    if($whereClause === null) {
      return $data->simplePaginate($per_page);
    }

    return $data->where($whereClause)->simplePaginate($per_page);
  }
}
