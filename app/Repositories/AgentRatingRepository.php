<?php namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Modules\AgentRating\Models\AgentRating;

class AgentRatingRepository extends BaseRepository {
  public function __construct(AgentRating $entity) {
    $this->entity = $entity;
    $this->rules = [
      "agent_id" => "required",
      "user_id" => "required",
      "rate" => "required",
      "app_id" => "required"
    ];
  }
}
