<?php namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Modules\TimeReportServed\Model\TimeReportServed as Entity;

class TimeReportServedRepository extends BaseRepository {
  public function __construct(Entity $entity) {
    $this->entity = $entity;
    $this->rules = [
      "request_at" => "required",
      "served_at" => "required",
      "agent_id" => "required",
      "room_id" => "required"
    ];
  }
}
