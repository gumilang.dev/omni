<?php namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Modules\Session\Models\Session;

class SessionRepository extends BaseRepository {
  

  public function __construct(Session $session) {
    $this->entity = $session;
    $this->rules = [
      "room_id" => "required",
      "handle_by" => "required",
      "start_at" => "required",
      "end_at" => "required"
    ];
  }

}