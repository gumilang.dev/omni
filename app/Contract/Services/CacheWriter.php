<?php namespace App\Contract\Services;

interface CacheWriter {
  public  function create($data);
  public  function find();
  public  function findBy($key);
  public  function destroy($key);
  public  function update($param,$data);
  public function provider();
}
