<?php namespace App\Contract\Services;

interface TicketService {
  public function createTicket($data,$appId);
  public function deleteTicket($param);
  public function updateTicket($param,$data);
  public function getTicket($request,$whereClause,$search);
  public function getBy($param);
  public function getInformationByMedia($data);
  public function createTicketId();
  public function filterTicket($app_id, $condition,$per_page);
}
