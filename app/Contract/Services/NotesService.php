<?php namespace App\Contract\Services;

interface NotesService {
  public function store($data);
  public function update($param,$data);
}