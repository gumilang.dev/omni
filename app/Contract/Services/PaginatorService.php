<?php namespace App\Contract\Services;

interface PaginatorService {
  public function paginate($items, $perPage, $page = null, $options = []);
}
