<?php namespace App\Contract\Services;

interface LogHandleRoomService {
  public function createLog($data);
  public function reportLog($request,$whereClause,$search);
}
