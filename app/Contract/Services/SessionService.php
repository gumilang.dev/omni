<?php namespace App\Contract\Services;

interface SessionService  {
  public function createSession($data);
}