<?php namespace App\Contract\Services;

interface TimeReportServedService {
  public function createReport($data);
  public function getTimeReportServed($request,$whereClause,$search);
  public function getTimeReportServedExport($request,$whereClause,$search);
}
