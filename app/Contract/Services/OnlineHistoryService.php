<?php namespace App\Contract\Services;

interface OnlineHistoryService {
  public function createHistory($data);
  public function updateHistory($data);
}
