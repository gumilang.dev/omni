<?php
namespace App\Contract\Services;

interface IBlockUserService {
  public function block($data);
  public function unblock($data);
  public function getBlockedUser($request,$whereClause,$search);
  public function isBlocked($email,$app_id);
}
