<?php namespace App\Contract\Services;

interface AgentService  {
  public function assign($param,$data,$livechat);
  public function changeStatus($data);
}
