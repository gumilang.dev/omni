<?php namespace App\Contract\Services;

interface ChatTemplateService {
  public function paginate($request,$whereClause,$search);
  public function store($data);
  public function destroy($param);
  public function update($param,$data);
}
