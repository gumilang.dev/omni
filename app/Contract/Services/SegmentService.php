<?php namespace App\Contract\Services;

interface SegmentService {
  public function store($data);
  public function show($appId,$id);
  public function index($param);
  public function destroy ($param);
  public function update($param,$data);
  public function changeCondition($value);
  public function createSchema($value);
}
