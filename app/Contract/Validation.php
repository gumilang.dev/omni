<?php namespace App\Contract;

interface Validation
{
  public function validate($data);
}
