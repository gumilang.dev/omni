<?php namespace App\Contract\Repositories;

interface IBaseRepository
{
  public function raw();
  public function store($data);
  public function destroy($param);
  public function update($param,$data,$useValidation);
  public function all();
  public function search($params,$keyword);
  public function findBy($param);
  public function paginate($request,$whereClause,$search);
  public function validate($data);
}
