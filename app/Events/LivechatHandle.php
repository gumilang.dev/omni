<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class LivechatHandle implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $room;
    public $unserved_room;

    public function __construct($room,$unserved_room)
    {
      $this->room = $room;
      $this->unserved_room = $unserved_room;
    }

    public function broadcastOn()
    {
      return new Channel('ChannelApp.'.$this->room->app_id);
    }

    public function broadcastWith() {
      return [
        "room" => $this->room,
        "unserved_room" => $this->unserved_room
      ];
    }
}
