<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use App\Modules\User\Models\User;
use App\Modules\App\Models\App;

class NewUser implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $user;
    public $app;

    public function __construct(User $user, App $app)
    {
      $this->user = $user;
      $this->app = $app;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
      return new Channel('ChannelApp.'.$this->app->id);
      // return new PrivateChannel('ChannelApp.'.$this->app->id);
    }
    public function broadcastWith()
    {
      return [
        'user'=>$this->user,
        'app'=>$this->app,
      ];
    }
}
