<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use App\Modules\Message\Models\Message;
use App\Modules\App\Models\App;
use App\Modules\User\Models\User;
use App\Modules\Room\Models\Room;

class NewMessage implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $message;
    public $app;
    public $room;
    public $integrationData;
    public function __construct(Message $message, Room $room, App $app, $integrationData = [])
    {
        $this->message = $message;
        $this->app = $app;
        $this->room = $room;
        $this->integrationData = $integrationData;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('ChannelApp.'.$this->app->id);
        // return new PrivateChannel('ChannelApp.' . $this->app->id);
    }

    public function broadcastWith()
    {
        if ($this->integrationData !== []) {
            $this->room['integration'] = $this->integrationData;
            return [
                'message' => $this->message,
                'app' => $this->app,
                'room' => $this->room,
                'user' => $this->message->messageable,
                // 'integration' => $this->integrationData
            ];
        }
        return [
            'message' => $this->message,
            'app' => $this->app,
            'room' => $this->room,
            'user' => $this->message->messageable
        ];
    }

}
