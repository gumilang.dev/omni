<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class SendBroadcastWhatsapp implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $type;
    public $user;
    public $response_from_provider;
    public $data;
    public $app_id;
    public $integration_id;




    public function __construct($type, $user, $response_from_provider,$data, $app_id, $integration_id)
    {
      $this->type = $type;
      $this->user = $user;
      $this->response_from_provider = $response_from_provider;
      $this->data = $data;
      $this->app_id = $app_id;
      $this->integration_id = $integration_id;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('lenna-platform');
    }
}
