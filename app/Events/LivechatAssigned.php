<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;



class LivechatAssigned implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $room;
    public $unserved_room;
    public $is_record_assignment;
    public $is_record_assignment_group;
    public $group_id;
    public $assignee;
    public $assignor;
    public $assignor_role;

    public function __construct($room,$unserved_room,$assignee = null,$assignor = null, $assignor_role = null, $is_record_assignment = false, $group_id = null, $is_record_assignment_group = false)
    {
      $this->room = $room;
      $this->unserved_room = $unserved_room;
      $this->is_record_assignment = $is_record_assignment;
      $this->is_record_assignment_group = $is_record_assignment_group;
      $this->group_id = $group_id;
      $this->assignee = $assignee;
      $this->assignor = $assignor;
      $this->assignor_role = $assignor_role;
    }

    public function broadcastOn()
    {
      return new Channel('ChannelApp.'.$this->room->app_id);
    }

    public function broadcastWith() {
      return [
        "room" => $this->room,
        "unserved_room" => $this->unserved_room,
        "is_record_assignment" => $this->is_record_assignment,
        "is_record_assignment_group" => $this->is_record_assignment_group,
        "group_id" => $this->group_id,
        "assignee" => $this->assignee,
        "assignor" => $this->assignor,
        "assignor_role" => $this->assignor_role
      ];
    }
}
