<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use App\Modules\Message\Models\Message;
use App\Modules\App\Models\App;
use App\Modules\User\Models\User;
use App\Modules\Room\Models\Room;
use App\Modules\Livechat\Models\Livechat;

class RequestLive implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $appId;
    public $userId;
    public $roomId;
    public $liveChatId;
    public $queue;
    public function __construct($roomId, $appId, $userId, $liveChatId, $queue)
    {
        $this->appId = $appId;
        $this->userId = $userId;
        $this->roomId = $roomId;
        $this->liveChatId = $liveChatId;
        $this->queue = $queue;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('ChannelApp.'.$this->appId);
        // return new PrivateChannel('ChannelApp.' . $this->appId);
    }

    public function broadcastWith()
    {
        return [
          'app' => App::find($this->appId),
          'room' => Room::where('id', $this->roomId)->with(['messages','channel','tags','additional_informations','notes'])->first(),
          'user' => User::find($this->userId),
          'liveChat' => Livechat::find($this->liveChatId),
          'queue' => $this->queue
        ];
    }

}
