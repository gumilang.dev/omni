<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class RejectAssign implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    protected $appId;
    protected $room;
    protected $agent;
    protected $message;

    public function __construct($appId,$room,$agent,$message)
    {
      $this->appId = $appId;
      $this->room = $room;
      $this->agent = $agent;
      $this->message = $message;
    }


    public function broadcastWith() {
      return [
        "room" => $this->room,
        "agent" => $this->agent,
        "appId" => $this->appId,
        "message" => $this->message
      ];
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
      return new Channel('ChannelApp.'.$this->appId);
    }
}
