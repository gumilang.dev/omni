<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class RequestHandled implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $appId;
    public $room;
    public $liveChat;
    public $total;
    public $queue;

    public function __construct($appId,$room, $liveChat,$total,$queue)
    {
        $this->appId = $appId;
        $this->room = $room;
        $this->liveChat = $liveChat;
        $this->total = $total;
        $this->queue = $queue;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('ChannelApp.'.$this->appId);
        // return new PrivateChannel('ChannelApp.' . $this->appId);
    }

    public function broadcastWith()
    {
        return [
            'appId' => $this->appId,
            'room' => $this->room,
            'livechat' => $this->liveChat,
            'total' => $this->total,
            'queue' => $this->queue
        ];
    }

}
