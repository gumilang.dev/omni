<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class ChatResolved implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $appId;
    public $livechat;
    public $agentInfo;
    public $userId;
    public $description;

    public function __construct($appId,$livechat,$agentInfo,$userId,$description)
    {
        $this->appId = $appId;
        $this->livechat = $livechat;
        $this->agentInfo = $agentInfo;
        $this->userId = $userId;
        $this->description = $description;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('ChannelApp.'.$this->appId);
    }

    public function broadcastWith()
    {
        return [
            'appId' => $this->appId,
            'livechat' => $this->livechat,
            'agent_info' => $this->agentInfo,
            'userId' => $this->userId,
            'description' => $this->description
        ];
    }
}
