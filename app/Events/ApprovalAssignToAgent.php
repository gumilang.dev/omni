<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Log;
class ApprovalAssignToAgent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $appId;
    public $room;
    public $agent;
    public $assigned_at;
    public $assignor;
    public $assignor_role;

    public function __construct($appId,$room,$agent,$assigned_at,$assignor,$assignor_role)
    {
        $this->appId = $appId;
        $this->room = $room;
        $this->agent = $agent;
        $this->assigned_at = $assigned_at;
        $this->assignor = $assignor;
        $this->assignor_role = $assignor_role;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
      return new Channel('ChannelApp.'.$this->appId);
    }


    public function broadcastWith() {
      return [
        "appId" => $this->appId,
        "room" => $this->room,
        "agent" => $this->agent,
        "assigned_at" => $this->assigned_at,
        "assignor" => $this->assignor,
        "assignor_role" => $this->assignor_role
      ];
    }
}
