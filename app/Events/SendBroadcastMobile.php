<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class SendBroadcastMobile implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $broadcasted_data;
    public $status;
    public $send_by;

    public function __construct($status,$broadcasted_data,$send_by)
    {
      $this->broadcasted_data = $broadcasted_data;
      $this->status = $status;
      $this->send_by = $send_by;
    }

    public function broadcastOn()
    {
      return new Channel('lenna-platfrom');
    }
}
