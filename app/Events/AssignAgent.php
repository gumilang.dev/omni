<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AssignAgent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $appId;
    public $roomId;
    public $agentId;
    public $total;

    public function __construct($appId,$roomId,$agentId,$total)
    {
        $this->appId = $appId;
        $this->roomId = $roomId;
        $this->agentId = $agentId;
        $this->total = $total;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('ChannelApp.'.$this->appId);
    }

    public function broadcastWith()
    {
        return [
            'appId' => $this->appId,
            'roomId' => $this->roomId,
            'agentId' => $this->agentId,
            'total' => $this->total
        ];
    }
}
