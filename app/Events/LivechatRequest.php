<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class LivechatRequest implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $room;
    public $queue;
    public $administrator_id;

    public function __construct($room,$queue,$administrator_id)
    {
        $this->room = $room;
        $this->queue = $queue;
        $this->administrator_id = $administrator_id;
    }

    public function broadcastOn()
    {
      return new Channel('ChannelApp.'.$this->room->app_id);
    }

    public function broadcastWith() {
      return [
        "room" => $this->room,
        "queue" => $this->queue,
        "administrator_id" => $this->administrator_id
      ];
    }
}
