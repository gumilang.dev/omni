<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Modules\App\Models\App;
use App\Modules\User\Models\User;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class MaskingInputText implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $userId;
    public $appId;

    public function __construct($userId, $appId)
    {
        $this->userId = $userId;
        $this->appId = $appId;
    }

    public function broadcastOn()
    {
        return new Channel('ChannelApp.'.$this->appId);
    }

    public function broadcastWith()
    {
        return [
          'user' => User::find($this->userId)
        ];
    }
}
