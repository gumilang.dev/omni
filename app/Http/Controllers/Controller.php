<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Exceptions\RecordNotFoundException;
use App\Exceptions\RecordDuplicateException;
use App\Exceptions\RecordNotExpectedException;
use App\Exceptions\BadRequestParameterException;
use App\Shared\Common\WebResponse;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function compose($fn) {
      if($fn instanceof RecordNotFoundException) {
        throw new RecordNotFoundException($fn->getMessage());
      }else if($fn instanceof RecordDuplicateException) {
        throw new RecordDuplicateException($fn->getMessage());
      }else if($fn instanceof RecordNotExpectedException) {
        throw new RecordNotExpectedException($fn->getMessage());
      }else if($fn instanceof BadRequestParameterException) {
        throw new BadRequestParameterException($fn->getMessage());
      }elseif($fn instanceof \Exception) {
        throw new \Exception($fn->getMessage());
      }else {
        return $fn;
      }
    }

    public function ok($code, $status, $data) {
      return WebResponse::create($code,$status,$data);
    }

    public function catchError($fn,$cb) {
      try {
        $response = $this->compose($fn);
        return $cb($response);
      }catch(RecordNotFoundException $e) {
        return WebResponse::create(404,"ERROR",null,$e->getMessage());
      }catch(RecordDuplicateException $e) {
        return WebResponse::create(409,"ERROR",null,$e->getMessage());
      }catch(RecordNotExpectedException $e) {
        return WebResponse::create(417,"ERROR",null,$e->getMessage());
      }catch(BadRequestParameterException $e) {
        return WebResponse::create(400,"ERROR",null,$e->getMessage());
      }catch(\Exception $e) {
        return WebResponse::create(500,"ERROR",null,$e->getMessage());
      }
    }
}
