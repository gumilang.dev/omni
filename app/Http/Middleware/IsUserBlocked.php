<?php

namespace App\Http\Middleware;

use Closure;
use Log;
use App\Services\CacheService;
use Illuminate\Support\Facades\Redis;

class IsUserBlocked
{
    public function __construct(CacheService $cache) {
      $this->cache = $cache;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(
      $request,
      Closure $next
    ){
      return $next($request);
    }
}
