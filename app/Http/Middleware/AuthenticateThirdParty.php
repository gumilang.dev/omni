<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Validator;

use App\Modules\User\Models\VerifyUser;

class AuthenticateThirdParty
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->email = $request->email;
        $this->token = $request->get('x-token');

        $validator = Validator::make($request->all(), [
          'email'  => 'required|email',
          'token'  => 'required',
        ]);

        if($validator->fails()){
          return $this->_errorResponse(null, 'Request not acceptable', 406);
        }

        if (!$this->verifiedUser()){
          return $this->_errorResponse(null, 'Unauthorized user', 401);
        }

        return $next($request);
    }

    protected function verifiedUser()
    {
        $verifiedUser = VerifyUser::where('email', $this->email)->where('token', $this->token)->first();
        return $verifiedUser;
    }

    public function _successResponse($result = null, $message = null)
    {
        $response = [
            'success' => true,
            'message' => $message,
            'data' => $result,
        ];

        return response()->json($response, 200);
    }

    public function _errorResponse($error = null, $message = null, $code = 403)
    {
        $response = [
            'success' => false,
            'message' => $message,
            'error' => $error
        ];

        return response()->json($response, $code);
    }
}
