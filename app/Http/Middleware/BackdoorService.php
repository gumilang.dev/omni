<?php

namespace App\Http\Middleware;

use Closure;

class BackdoorService
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('X-Backdoor-Token') == "Lenna333") {
          return $next($request);
        }
        return response()->json([
          'success' => false
        ], 403);
    }
}
