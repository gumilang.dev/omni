<?php

namespace App\Http\Middleware;

use Closure;

class SecureHeaders
{
		/*
			Enumerate headers which you do not want in your application's responses.
			Great starting point would be to go check out: https://securityheaders.com/
		*/
    private $unwantedHeaderList = [
			'X-Powered-By',
			'Server',
		];
    public function handle($request, Closure $next)
    {
				$this->removeUnwantedHeaders($this->unwantedHeaderList);
				$response = $next($request);
				$response->headers->set('Referrer-Policy', 'no-referrer-when-downgrade');
				$response->headers->set('X-Content-Type-Options', 'nosniff');
				$response->headers->set('X-XSS-Protection', '1; mode=block');
				$response->headers->set('X-Frame-Options', 'SAMEORIGIN');
				// $response->headers->set('Strict-Transport-Security', 'max-age=16070400; includeSubDomains');
				$response->headers->set('X-Permitted-Cross-Domain-Policies', 'none');
				$response->headers->set('Content-Security-Policy',
					"default-src * data: blob: filesystem: about: ws: wss: 'unsafe-inline' 'unsafe-eval';script-src * data: blob: 'unsafe-inline' 'unsafe-eval';connect-src * data: blob: 'unsafe-inline';img-src * data: blob: 'unsafe-inline';frame-src * data: blob: ;style-src * data: blob: 'unsafe-inline';font-src * data: blob: 'unsafe-inline';"
				); // Clearly, you will be more elaborate here.
				return $response;
		}
		private function removeUnwantedHeaders($headerList)
    {
        foreach ($headerList as $header) {
					header_remove($header);
				}
    }
}
