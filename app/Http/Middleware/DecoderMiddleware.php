<?php

namespace App\Http\Middleware;

use Closure;
use Hashids\Hashids;

class DecoderMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $appId  = $request->route('appId');
        $hashAppId    = new Hashids('',6);
        $decodedAppId =  $hashAppId->decode($appId);
        $request->route()->setParameter('appId',$decodedAppId[0]);

        return $next($request);
    }
}
