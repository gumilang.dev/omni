<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use App\Modules\User\Models\User;
use App\Modules\User\Models\UserPlatform;
use PeterPetrus\Auth\PassportToken;
use Hashids\Hashids;
use Carbon\Carbon;
use Illuminate\Http\Response;

class RemoteAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $hashids;

    public function __construct()
    {
        $this->hashids = new Hashids('', 6);
    }
    public function handle($request, Closure $next)
    {

        $token = $request->header('token');
        $userId = $request->header('userId');
        $appId = $request->route('appId');

        $this->decodedToken = PassportToken::dirtyDecode($token);
        $this->decodedUserId = $this->hashids->decode($userId);
        $this->decodedAppId = $this->hashids->decode($appId);

        if ($this->isNotLogin($token, $userId)) {
            return $this->_errorResponse(null, 'unathorized', 401);
        }

        if ($this->isExpired()) {
            return $this->_errorResponse(null, 'token expired', 401);
        }

        if (!$this->isValidToken()) {
            return $this->_errorResponse(null, 'token not valid', 401);
        }

        if ($this->isRevokedToken()) {
            return $this->_errorResponse(null, 'token revoked', 401);
        }

        if (!$this->isAppBelongsToUser()){
            return $this->_errorResponse(null, 'no permissions to access app', 401);
        }



      // set decoded to param and request
        $request->merge([
            'decodedAppId' => $this->decodedAppId[0],
            'decodedUserId' => $this->decodedUserId[0]
        ]);
        $request->route()->setParameter('appId', $this->decodedAppId[0]);

        return $next($request);

    }
    protected function isRevokedToken(){
        $check = DB::table(env('DB_AUTH_SCHEMA') . ".oauth_access_tokens")
                    ->where('id', $this->decodedToken['token_id'])
                    ->first();
                    // dd($check);
        if ($check == null || $check->revoked == true) {
            return true;
        } else {
            return false;
        }
    }
    protected function isNotLogin($token, $userId)
    {
        if (!isset($token) || !isset($userId))
            return true;
    }
    protected function isExpired()
    {
        if (Carbon::now()->timestamp > Carbon::parse($this->decodedToken['expires_at'])->timestamp)
            return true;
    }
    protected function isValidToken()
    {
        if ($this->decodedToken['user_id'] == $this->decodedUserId[0])
            return true;
    }

    protected function isAppBelongsToUser()
    {
        $decodedUserId = $this->decodedUserId[0];
        $decodedAppId = $this->decodedAppId[0];
        $userPlatform = UserPlatform::select('id')->where('id', $decodedUserId)->with(['apps' => function($q) {
          $q->select('apps.id');
        }])->first();
        $apps = collect($userPlatform->apps)->map(function($q) {
          return $q->id;
        })->toArray();
        if (in_array($decodedAppId, $apps)) {
          return true;
        }
        return false;
        // $isExist = $userPlatform->contains(function ($app, $key) use ($decodedAppId) {
        //     return $app->id == $decodedAppId;
        // });
        // return $isExist;

    }


    public function _successResponse($result = null, $message = null)
    {
        $response = [
            'success' => true,
            'message' => $message,
            'data' => $result,
        ];

        return response()->json($response, 200);
    }

    public function _errorResponse($error = null, $message = null, $code = 403)
    {
        $response = [
            'success' => false,
            'message' => $message,
            'error' => $error
        ];

        return response()->json($response, $code);
    }
}
