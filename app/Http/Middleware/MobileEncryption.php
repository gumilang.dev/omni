<?php

namespace App\Http\Middleware;

use Closure;
use App\Modules\Mobile\Services\BackendService;
use App\Modules\Channel\Models\Channel;
use Illuminate\Foundation\Http\Middleware\TransformsRequest;

class MobileEncryption extends TransformsRequest
{
    public function __construct()
    {
        $this->channel = Channel::where('name', 'mobile')->first();
    }

    public function handle($request, Closure $next)
    {
        $appId        = $request->route('appId');
        $app          = collect(config('encrypt.enabled.apps'));
        $app          = $app->where('id', $appId)->values()->all();
        if (count($app) > 0) {
          if (collect($app[0]['channels'])->contains($this->channel->id)) {
            foreach ($request->all() as $key => $value) {
              $output = $request->merge([$key => BackendService::decryptText(env('ENCRYPT_MESSAGE_KEY'), $value)]);
            }
            return $next($output);
          } else {
            return $next($request);
          }
        } else {
          return $next($request);
        }
    }
}
