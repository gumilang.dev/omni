<?php

namespace App\Http\Middleware;

use App\Modules\User\Models\UserApp;
use App\Modules\Role\Helpers\RoleHelper;
use Hashids\Hashids;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Closure;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $hashids;

    public function __construct()
    {
        $this->hashids = new Hashids('', 6);
    }

    public function handle($request, Closure $next)
    {
      if ($request->header('X-Mobile-Omni') || config('role.middleware') == false) {
        return $next($request);
      } else {
        $referer              = parse_url($request->header('referer'));
        $array                = explode("/",$referer['path']);
        $userId               = $request->header('userId');
        $appId                = $array[3] ?? [];
        $refererMenu          = $array[4] ?? [];
        $refererSubmenu       = $array[5] ?? [];
        $this->decodedAppId   = $this->hashids->decode($appId)[0];
        $this->decodedUserId  = $this->hashids->decode($userId)[0];

        if ($this->notHaveRole($this->decodedAppId, $this->decodedUserId)) {
          return $this->_errorResponse(null, 'User role not found', 403);
        }

        $detailMenu = $this->checkDetailMenu($refererMenu);

        if (count($detailMenu) > 0) {
          $sub = collect($detailMenu)->flatMap(function($q) {
            return $q['submenus'];
          });
          if (count($sub) > 0 && $refererSubmenu) {
            $data = collect($sub)->contains($refererSubmenu);
            if ($data) {
              return $next($request);
            } else {
              return $this->_errorResponse(null, 'Submenu '.$refererSubmenu.' not found', 403);
            }
          }
          return $next($request);
        }
        return $this->_errorResponse($detailMenu, 'Menu not found', 403);
      }
    }

    protected function notHaveRole($appId, $userId)
    {
      $role = UserApp::where('user_id', $userId)->where('app_id', $appId)->first();
      $this->role = $role;
      if(!$role){
        return true;
      }
    }

    protected function checkDetailMenu($refererMenu)
    {
      $menus = RoleHelper::menuPrivileges($this->role->role_id, $this->decodedAppId);
      $menus = collect($menus)->map(function($val) {
        return [
          'menu'     => strtolower($val->name),
          'submenus' => collect($val->submenus)->map(function($q) {
            return $q->slug;
          })
        ];
      });

      $detailMenu = $menus->filter(function($q) use ($refererMenu) {
        return $q['menu'] == $refererMenu;
      })->values();

      return $detailMenu;
    }

    public function _errorResponse($error = null, $message = null, $code = 403)
    {
        $response = [
            'success' => false,
            'message' => $message,
            'error' => $error
        ];

        return response()->json($response, $code);
    }
}
