<?php

namespace App\Exports;

use App\Modules\Room\Models\Room;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Hashids\Hashids;
use App\Modules\Log\Models\ApiLog;


use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class HsmExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */

    public function AppId(string $appId, string $start, string $end, string $search, string $integrationId, string $templateName, string $status)
    {
        // $this->decodedUserId = $this->hashids->decode($userId);
        $this->appId = $appId;
        $this->start = $start;
        $this->end = $end;
        $this->search = $search;
        $this->integrationId = $integrationId;
        $this->templateName = $templateName;
        $this->status = $status;
        // print_r($this->appId[0]); die();
        return $this;
    }

    public function collection()
    {
        $appId = $this->appId;
        $start = $this->start;
        $end = $this->end;
        $search = $this->search;
        $integrationId = $this->integrationId;
        $templateName = $this->templateName;
        $status = $this->status;

        $datas = DB::table("broadcast_messages as bm")
                  ->where('bm.app_id', $appId)
                  ->where("bm.category", "hsm")
                  ->where("bm.channel_id", 4)
                  ->leftJoin("auth.users as u", "u.id", "=", "bm.send_by") //still get null data when send_by is null
                  ->select("bm.*", "u.name")
                  ->orderBy("bm.id", "DESC");

        if ($search) {
            $datas = $datas->where(function ($q) use ($search) {
                $q->where("bm.number", "ILIKE", '%'.$search.'%');
            });
        }

        if ($integrationId) {
            $datas = $datas->where(function ($q) use ($integrationId) {
                $q->where('bm.integration_id', $integrationId);
            });
        }

        if ($templateName) {
            $datas = $datas->where(function ($q) use ($templateName) {
                $q->where('bm.topics', $templateName);
            });
        }

        if ($status) {
            $datas = $datas->where(function ($q) use ($status) {
                $q->where('bm.status', $status);
            });
        }

        if ($start && $end) {
            $datas = $datas->where(function ($q) use ($start,$end) {
                $q->whereDate('bm.created_at', '>=', $start);
                $q->whereDate('bm.created_at', '<=', $end);
            });
        }

        return $datas->get();

    }

    public function headings(): array
    {
        return [
            'To',
            'Template Name',
            'Status',
            'Execute By',
            'Date Time'
        ];
    }

    public function map($dataa): array
    {
        return [
            "'".$dataa->number,
            $dataa->topics,
            $dataa->status,
            $dataa->name ?? "-",
            $dataa->created_at
            // Date::dateTimeToExcel($invoice->created_at),
        ];
    }
}
