<?php

namespace App\Exports;

use App\Modules\Room\Models\Room;
use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use App\Modules\TimeReportServed\Model\TimeReportServed;
use Hashids\Hashids;


use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class TimeServedExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */

    public function AppId(string $appId, string $start, string $end,$search)
    {
        // $this->decodedUserId = $this->hashids->decode($userId);
        $this->appId = $appId;
        $this->start = $start;
        $this->end = $end;
        $this->search = $search;
        // print_r($this->appId[0]); die();
        return $this;
    }

    public function collection()
    {
        $appId = $this->appId;
        $start = $this->start;
        $end = $this->end;

        return TimeReportServed::where('app_id', $appId)
        ->where(function($query) {
          if($this->search) {
            $query->where("agent_name" , 'ILIKE', '%'. $this->search . '%');
            $query->where("agent_email" , 'ILIKE', '%'. $this->search . '%');
            // $query->where("room_id" , 'ILIKE', '%'. $this->search . '%');
          }
        })
        ->whereDate('created_at', '>=', $start)
        ->whereDate('created_at', '<=', $end)
        ->orderBy('created_at', 'DESC')
        ->get();
    }

    public function headings(): array
    {
        return [
            'ROOM ID',
            'CUSTOMER NAME',
            'AGENT NAME',
            'AGENT EMAIL',
            'CHANNEL',
            'REQUEST AT',
            'SERVED AT',
            'DIFF IN MINUTES'
        ];
    }

    public function map($data): array
    {
        return [
            $data->room->id ?? "-",
            $data->room->created_by->name ?? "-",
            $data->agent_name,
            $data->agent_email,
            $data->room->channel->name ?? "-",
            $data->request_at,
            $data->served_at,
            $data->diff . " Minutes"
        ];
    }
}
