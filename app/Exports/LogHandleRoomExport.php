<?php

namespace App\Exports;

use App\Modules\Message\Models\Message;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Hashids\Hashids;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use App\Services\LogHandleRoomServiceImpl;

class LogHandleRoomExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */

    public function AppId($request,string $appId, string $start, string $end,$search)
    {
        $this->request = $request;
        $this->appId = $appId;
        $this->start = $start;
        $this->end = $end;
        $this->search = $search;
        // print_r($this->appId[0]); die();
        return $this;
    }

    public function collection()
    {
        $appId = $this->appId;
        $start = $this->start;
        $end = $this->end;
        $search = $this->search;
        $request = $this->request;

        $service = new LogHandleRoomServiceImpl();
        return $service->reportLog($request,["app_id" => $appId]);
    }

    public function headings(): array
    {
      return [
          'ROOM ID',
          'CUSTOMER NAME',
          'USER PLATFORM NAME',
          'USER PALTFORM EMAIL',
          'CREATED AT'
      ];
    }

    public function map($data): array
    {
      return [
        $data->room->id,
        $data->room->created_by->name,
        $data->user->name,
        $data->user->email,
        $data->created_at
      ];
    }

}
