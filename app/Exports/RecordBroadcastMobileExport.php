<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use App\Modules\BroadcastMessage\Repositories\Impl\BroadcastMessageRepositoryImpl;

class RecordBroadcastMobileExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;
    private $app_id;
    private $start;
    private $end;
    private $search;


    public function __construct($app_id, $start, $end,$search)
    {
      $this->app_id = $app_id;
      $this->start = $start;
      $this->end = $end;
      $this->search = $search;
    }

    public function headings(): array
    {
      return [
        "USER ID",
        "NOTIFICATION TITLE",
        "NOTIFICATION BODY",
        "STATUS",
        "EXECUTED BY",
        "DATE"
      ];
    }

    public function collection()
    {
      $repository = new BroadcastMessageRepositoryImpl();
      return $repository->getBy($this->app_id,function ($query){
        $query->when($this->search, function ($fn) {
          $fn->where("ob.status","ilike","%".$this->search."%")->orWhere("au.name","ilike","%".$this->search."%");
        });

        $query->whereIn("ob.client",["android","ios","mobile"]);

        $query->when(!is_null($this->start), function ($fn) {
          $fn->whereDate("ob.created_at",">=",$this->start);
          $fn->whereDate("ob.created_at", "<=", $this->end);
        });
      });
    }

    public function map($data): array
    {
      return [
        $data->id,
        json_decode($data->notification)->title,
        json_decode($data->notification)->body,
        $data->status,
        $data->executed_by,
        $data->created_at
      ];
    }
}
