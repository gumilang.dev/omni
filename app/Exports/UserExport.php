<?php

namespace App\Exports;

use App\Modules\User\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Hashids\Hashids;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;



class UserExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithEvents
{
    use Exportable;
    // protected $hashids;
    /**
    * @return \Illuminate\Support\Collection
    */

    // public function __construct(string $appId)
    // {
    //     $this->appId = $appId;
    // }

    public function AppId(string $appId, string $start, string $end, $search)
    {
        $this->appId = $appId;
        $this->start = $start;
        $this->end = $end;
        $this->search = $search;
        return $this;
    }



    public function collection()
    {
        $appId = $this->appId;
        $start = $this->start;
        $filterText = $this->search;
        $end = $this->end;

        return User::query()->with('channel')
                ->where('app_id', $appId)
                ->where(function($query)  use ($filterText) {
                  if ($filterText) {
                    $query->where('name', 'ILIKE', '%' . $filterText . '%')->orWhere('email', 'ILIKE', '%' . $filterText . '%')->orWhere('phone', 'ILIKE', '%' . $filterText . '%');
                  }
                })
                // ->where(function ($q) use ($this))
                //  ->whereBetween('created_at', [$start, $end])
                ->whereDate('created_at', '>=', $start)
                ->whereDate('created_at', '<=', $end)
                ->orderBy('created_at', 'DESC')
                // ->take(10)
                ->get();
    }

    public function headings(): array
    {
        return [
            'id',
            'name',
            'nickname',
            'email',
            'phone',
            'channel',
            'created_at',

        ];
    }

    public function map($dataa): array
    {
        return [
            $dataa->id,
            "'".$dataa->name,
            "'".$dataa->nickname,
            $dataa->email,
            $dataa->phone,
            $dataa->channel->name,
            $dataa->created_at
            // Date::dateTimeToExcel($invoice->created_at),
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {

                // last column as letter value (e.g., D)
                $last_column = Coordinate::stringFromColumnIndex(count($this->results[0]));

                // set up a style array for cell formatting
                $style_text_center = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ]
                ];

                // at row 1, insert 1 rows
                $event->sheet->insertNewRowBefore(1, 1);

                // merge cells for full-width
                $event->sheet->mergeCells(sprintf('A1:%s1',$last_column));
                $event->sheet->mergeCells(sprintf('A2:%s2',$last_column));
                $event->sheet->mergeCells(sprintf('A%d:%s%d',$last_row,$last_column,$last_row));

                // assign cell values
                $event->sheet->setCellValue('A1','Report User');
                // $event->sheet->setCellValue('A2','SECURITY CLASSIFICATION - UNCLASSIFIED [Generator: Admin]');
                // $event->sheet->setCellValue(sprintf('A%d',$last_row),'SECURITY CLASSIFICATION - UNCLASSIFIED [Generated: ...]');

                // assign cell styles
                $event->sheet->getStyle('A1')->applyFromArray($style_text_center);
                $event->sheet->getStyle(sprintf('A%d',$last_row))->applyFromArray($style_text_center);
            },
        ];
    }


}
