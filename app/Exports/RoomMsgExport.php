<?php

namespace App\Exports;

use App\Modules\Room\Models\Room;
use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Hashids\Hashids;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class RoomMsgExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */

    public function AppId(string $appId, string $start, string $end,$search)
    {
        // $this->decodedUserId = $this->hashids->decode($userId);
        $this->appId = $appId;
        $this->start = $start;
        $this->end = $end;
        $this->search = $search;
        // print_r($this->appId[0]); die();
        return $this;
    }

    public function collection()
    {
        $appId = $this->appId;
        $start = $this->start;
        $end = $this->end;


        $data = Room::where('app_id', $appId)->orderBy('created_at', 'DESC')
                ->where(function ($q) use ($start,$end) {
                  $q->whereDate('created_at', '>=', $start);
                  $q->whereDate('created_at', '<=', $end);
                })->get();

        if ($this->search){
            $data = $data->filter(function ($item) {
                return false !== stristr($item->created_by->name, $this->search) || false !== stristr($item->created_by->channel->name, $this->search);
            })->values();
        }


        return $data;
    }

    public function headings(): array
    {
        return [
            'Name',
            'Cust Id',
            'Channel Name',
            'Created At'
        ];
    }

    public function map($dataa): array
    {
        return [
            $dataa->created_by['name'],
            $dataa->id,
            $dataa->created_by['channel']['name'],
            $dataa->created_at
            // Date::dateTimeToExcel($invoice->created_at),
        ];
    }
}
