<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DB;

class AgentActivitiesExport implements  FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{

    use Exportable;

    private $app_id;
    private $start;
    private $end;
    private $search;

    public function __construct($app_id,$start, $end ,$search)
    {
      $this->app_id = $app_id;
      $this->start = $start;
      $this->end = $end;
      $this->search = $search;
    }


    public function headings(): array
    {
      return [
        "customer name",
        "customer email",
        "assignor",
        "assignor role",
        "assignee",
        "assign at"
      ];
    }

    public function collection()
    {
      return DB::table("omnichannel.history_assignments as ha")
              ->where("ha.app_id",$this->app_id)
              ->where(function($query) {
                if(!is_null($this->search)) {
                  $query->where('ha.assignor','ILIKE', '%'.trim($this->search).'%')->orWhere('ha.assignee','ILIKE', '%'.trim($this->search).'%')->orWhere('ha.customer_name','ILIKE', '%'.trim($this->search).'%')->orWhere('ha.customer_email','ILIKE', '%'.trim($this->search).'%');
                }
                if(!is_null($this->start) || !is_null($this->end)) {
                  $query->whereDate("ha.created_at",">=",$this->start);
                  $query->whereDate("ha.created_at","<=",$this->end);
                }
              })
              ->get();
    }

    public function map($data): array
    {
      return [
        $data->customer_name,
        $data->customer_email,
        $data->assignor,
        $data->assignor_role,
        $data->assignee,
        $data->created_at
      ];
    }
}
