<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Hashids\Hashids;
use App\Modules\Ticket\Models\Ticket;
use App\Services\TicketServiceImpl;
use App\Repositories\TicketRepository;


use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class TicketExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;
    private $appId;
    private $condition;
    /**
    * @return \Illuminate\Support\Collection
    */

    public function AppId(string $appId,$condition)
    {
        // $this->decodedUserId = $this->hashids->decode($userId);
        $this->appId = $appId;
        $this->condition = $condition;
        return $this;
    }

    public function collection()
    {
        $ticketServiceImpl = new TicketServiceImpl(new TicketRepository(new Ticket()));
        return $ticketServiceImpl->exportTicket($this->appId, $this->condition);
    }

    public function headings(): array
    {
        return [
            'ID',
            'TICKET ID',
            'SUBJECT',
            'PRIORITY',
            'STATUS',
            'TYPE',
            'AGENT',
            'CREATED AT',
            'RESOLVED AT',
            'RESOLUTION TIME',
        ];
    }

    public function map($data): array
    {
        return [
            $data->id,
            $data->ticket_id,
            $data->subject,
            $data->priority,
            $data->status,
            $data->type,
            $data->agent->name,
            $data->created_at,
            $data->solved_at,
            $data->resolution_time,
        ];
    }
}
