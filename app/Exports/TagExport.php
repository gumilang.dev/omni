<?php

namespace App\Exports;

use App\Modules\Room\Models\Room;
use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Hashids\Hashids;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class TagExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */

    public function AppId(string $appId, string $start, string $end,$search)
    {
        $this->appId = $appId;
        $this->start = $start;
        $this->end = $end;
        $this->search = $search;
        // print_r($this->appId[0]); die();
        return $this;
    }

    public function collection()
    {
        $appId = $this->appId;
        $start = $this->start;
        $end = $this->end;

        return Room::where('app_id', $appId)
        ->orderBy('created_at', 'DESC')
        ->whereHas('tags',function($q) {
          if($this->search) {
            $q->where("name" , 'ILIKE', '%'. $this->search . '%');
          }
        })
        // ->whereBetween('created_at', [$start, $end])
        ->whereDate('created_at', '>=', $start)
        ->whereDate('created_at', '<=', $end)
        ->get();
    }

    public function headings(): array
    {
        return [
            'Conversation ID',
            'Costumer Name',
            'Channel',
            'Tags',
            'Created At'
        ];
    }

    public function map($data): array
    {
        $tags = '';
        $length = count($data->tags);
        $i= 0;
        foreach ($data->tags as $key => $item ) {
            $tags .= " ".$item->name;
            $i++;
        }
        return [
            $data->id,
            $data->created_by->name,
            $data->created_by->channel->name,
            $tags,
            $data->created_at
        ];
    }
}
