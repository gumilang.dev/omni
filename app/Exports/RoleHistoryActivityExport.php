<?php

namespace App\Exports;

use App\Modules\Room\Models\Room;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Hashids\Hashids;
use App\Modules\Log\Models\ApiLog;


use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class RoleHistoryActivityExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */

    public function AppId(string $appId, string $start, string $end, string $search, string $userPlatformName, string $activity)
    {
        // $this->decodedUserId = $this->hashids->decode($userId);
        $this->appId = $appId;
        $this->start = $start;
        $this->end = $end;
        $this->search = $search;
        $this->userPlatformName = $userPlatformName;
        $this->activity = $activity;
        // print_r($this->appId[0]); die();
        return $this;
    }

    public function collection()
    {
        $appId = $this->appId;
        $start = $this->start;
        $end = $this->end;
        $search = $this->search;
        $userPlatform = $this->userPlatformName;
        $activity = $this->activity;

        $datas = DB::table("history_activity_user_roles")->where('app_id', $appId);

        if ($search) {
            $datas = $datas->where(function ($q) use ($search) {
                $value = "%{$filterText}%";
                $q->where('user_platform_name', "ILIKE", $value);
            });
        }
        if ($userPlatform) {
            $datas = $datas->where(function ($q) use ($userPlatform) {
                            $value = "%{$userPlatform}%";
                            $q->where('user_platform_name', "ILIKE", $value);
                        });
        }
        if ($activity) {
            $datas = $datas->where('activity', $activity);
        }

        if ($start && $end) {
            $datas = $datas->where(function ($q) use ($start,$end) {
                $q->whereDate('created_at', '>=', $start);
                $q->whereDate('created_at', '<=', $end);
            });
        }

        return $datas->get();

    }

    public function headings(): array
    {
        return [
            'User Platform Name',
            'Role',
            'Activity',
            'Account Modified',
            'Previous Role',
            'Current Role',
            'Created At'
        ];
    }

    public function map($dataa): array
    {
        return [
            "'".$dataa->user_platform_name,
            $dataa->user_platform_role,
            $dataa->activity,
            $dataa->account_modified_name,
            $dataa->account_modified_previous_role ?? "-",
            $dataa->account_modified_current_role ?? "-",
            $dataa->created_at
            // Date::dateTimeToExcel($invoice->created_at),
        ];
    }
}
