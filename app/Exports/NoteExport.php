<?php

namespace App\Exports;

use App\Modules\Room\Models\Room;
use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Hashids\Hashids;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class NoteExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */

    public function AppId(string $appId, string $start, string $end,$search)
    {
        // $this->decodedUserId = $this->hashids->decode($userId);
        $this->appId = $appId;
        $this->start = $start;
        $this->end = $end;
        $this->search = $search;
        // print_r($this->appId[0]); die();
        return $this;
    }

    public function collection()
    {
        $appId = $this->appId;
        $start = $this->start;
        $end = $this->end;

        return Room::where('app_id', $appId)
        ->orderBy('created_at', 'DESC')
        ->whereHas('notes',function($q) {
          if($this->search) {
            $q->where("content" , 'ILIKE', '%'. $this->search . '%');
          }
        })
        // ->whereBetween('created_at', [$start, $end])
        ->whereDate('created_at', '>=', $start)
        ->whereDate('created_at', '<=', $end)
        ->get();
    }

    public function headings(): array
    {
        return [
            'Conversation ID',
            'Costumer Name',
            'Channel',
            'Note',
            'Created At'
        ];
    }

    public function map($data): array
    {
        return [
            $data->id,
            $data->created_by->name,
            $data->created_by->channel->name,
            $data->notes->content,
            $data->created_at
        ];
    }
}
