<?php

namespace App\Exports;

use App\Modules\Message\Models\Message;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Hashids\Hashids;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;

class MessageExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */

    public function AppId(string $appId, string $start, string $end,$search)
    {
        $this->appId = $appId;
        $this->start = $start;
        $this->end = $end;
        $this->search = $search;
        // print_r($this->appId[0]); die();
        return $this;
    }

    public function collection()
    {
        $appId = $this->appId;
        $start = $this->start;
        $end = $this->end;
        $search = $this->search;


        return Message::query()->whereHas('room', function ($q) {
            $q->where('app_id', $this->appId);
        })
        ->where('messageable_type','!=', 'bot')
        ->where(function($query) {
          if ($this->search){
              if (preg_match('~[0-9]+~', $this->search)) {
                $query->where('room_id',$this->search);
              }else{
                $query->where('content','ILIKE', '%'.trim($this->search).'%');
              }
          }
        })
        // ->whereBetween('created_at', [$start, $end])
        ->whereDate('created_at', '>=', $start)
        ->whereDate('created_at', '<=', $end)
        ->orderBy('created_at', 'DESC')
        // ->take(150)
        ->get();
    }

    public function headings(): array
    {
        return [
            'User Id',
            'Name',
            'Room Id',
            'User Type',
            'Message',
            'Created At'
        ];
    }

    public function map($dataa): array
    {
        return [
            $dataa->messageable ? $dataa->messageable['id'] : '',
            "'".$dataa->messageable ? $dataa->messageable['name'] : '',
            $dataa->room_id,
            $dataa->messageable_type,
            $dataa->content[0]->type == 'text' ? $dataa->content[0]->text : $dataa->content[0]->type,
            $dataa->created_at
            // Date::dateTimeToExcel($invoice->created_at),
        ];
    }

}
