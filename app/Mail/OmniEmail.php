<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OmniEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('pengirim@fachriza.com')
                   ->view('mail.mail')
                   ->with(
                    [
                        'nama' => 'Fachriza ramanda',
                        'website' => 'www.fachriza.com',
                    ]);
        // return $this->view('view.name');
    }
}
