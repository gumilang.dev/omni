<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use App\Events\LivechatRequest;
use App\Events\LivechatResolved;
use App\Events\LivechatHandle;
use App\Events\SendBroadcastMobile;
use App\Events\SendBroadcastWhatsapp;
use App\Events\LivechatAssigned;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Listeners\AutoAgentAllocation;
use App\Listeners\RecordSession;
use App\Listeners\ForceHandleLivechat;
use App\Listeners\QontakCRMUpdateTicket;
use App\Listeners\RecordLogBroadcastMobile;
use App\Listeners\RecordLogBroadcastWhatsapp;
use App\Listeners\RecordTimeServedByAgent;
use App\Listeners\HistoryAssignment;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
          SendEmailVerificationNotification::class,
        ],
        LivechatRequest::class => [
          AutoAgentAllocation::class,
          ForceHandleLivechat::class
        ],
        LivechatResolved::class => [
          RecordSession::class,
          QontakCRMUpdateTicket::class
        ],
        SendBroadcastMobile::class => [
          RecordLogBroadcastMobile::class
        ],
        SendBroadcastWhatsapp::class => [
          RecordLogBroadcastWhatsapp::class
        ],
        LivechatHandle::class => [
          RecordTimeServedByAgent::class
        ],
        LivechatAssigned::class => [
          HistoryAssignment::class
        ]

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
