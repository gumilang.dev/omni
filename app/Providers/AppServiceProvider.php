<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Resources\Json\Resource;
use Laravel\Telescope\TelescopeServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redis;
use Config;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      Redis::enableEvents();
      // Resource::withoutWrapping();
      // View
      View::composer(['telescope::layout'], function ($view) {
        $view->with('telescopeScriptVariables', ['path' => 'app/public/telescope', 'timezone' => config('app.timezone'), 'recording' => !cache('telescope:pause-recording')]);
      });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
