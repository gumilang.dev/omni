<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
          'App\Modules\Room\Repositories\RoomRepository',
          'App\Modules\Room\Repositories\Impl\RoomRepositoryImpl'
        );

        $this->app->bind(
          "App\Modules\CustomIntegration\Interfaces\CustomIntegrationRepositoryInterface",
          "App\Modules\CustomIntegration\Repositories\MysqlRepository"
        );

        $this->app->bind(
          "App\Modules\Livechat\Repositories\LivechatRepository",
          "App\Modules\Livechat\Repositories\Impl\LivechatRepositoryImpl",
        );

        $this->app->bind(
          "App\Modules\BroadcastMessage\Repositories\BroadcastMessageRepository",
          "App\Modules\BroadcastMessage\Repositories\Impl\BroadcastMessageRepositoryImpl",
        );

        $this->app->bind(
          "App\Modules\Group\Repositories\GroupRepository",
          "App\Modules\Group\Repositories\Impl\GroupRepositoryImpl",
        );
    }
}
