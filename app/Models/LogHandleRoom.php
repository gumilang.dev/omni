<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Room\Models\Room;
use App\Modules\User\Models\UserPlatform;


class LogHandleRoom extends Model
{
  protected $table = "log_handle_room";
  protected $fillable = ["room_id","user_id","app_id"];

  public function user()
  {
    return  $this->hasOne("App\Modules\User\Models\UserPlatform","id","user_id");
  }

  public function room()
  {
    return  $this->hasOne("App\Modules\Room\Models\Room","id","room_id");
  }
}
