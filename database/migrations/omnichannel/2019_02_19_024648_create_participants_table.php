<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participables', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('room_id');
            $table->morphs('participable');
            $table->boolean('is_active')->default(true);
            $table->timestamp('join_at')->nullable();
            $table->timestamp('leave_at')->nullable();

            // foreign room_id on rooms
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participables');
    }
}
