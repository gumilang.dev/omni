<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCustomIntegrationId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table("custom_integration_list", function (Blueprint $table) {
        $table->integer("custom_integration_id");
        $table->foreign("custom_integration_id")->references("id")->on("custom_integration");
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table("custom_integration_list", function (Blueprint $table) {
        $table->json("credential")->nullable()->change();
      });
    }
}
