<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubSubmenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_submenus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sub_submenu_id');
            $table->string('name');
            $table->string('icon');
            $table->string('slug');
            $table->string('category');
            $table->string('plans')->default(json_encode([]));
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_submenus');
    }
}
