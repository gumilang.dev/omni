<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebchatStylesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webchat_styles', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('app_id');
          $table->text('avatar')->nullable();
          $table->text('background_value')->nullable();
          $table->text('background_type')->nullable();
          $table->text('branding')->nullable();
          $table->text('bubble_self')->nullable();
          $table->text('bubble_other')->nullable();
          $table->text('text_self')->nullable();
          $table->text('text_other')->nullable();
          $table->text('header_logo')->nullable();
          $table->text('header_text')->nullable();
          $table->text('header_background_value')->nullable();
          $table->text('header_background_type')->nullable();
          $table->text('launcher')->nullable();
          $table->unsignedInteger('created_by');
          $table->unsignedInteger('updated_by')->nullable();
          $table->timestamps();
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webchat_styles');
    }
}
