<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryActivityUserRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_activity_user_roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            // $table->unsignedInteger('user_platform_id');
            // $table->unsignedInteger('role_id');
            // $table->string('activity');
            // $table->unsignedInteger('account_modified_id');
            // $table->unsignedInteger('previous_role_id')->nullable();
            // $table->unsignedInteger('current_role_id')->nullable();
            // $table->unsignedInteger('app_id');
            $table->string("user_platform_name");
            $table->string("user_platform_email");
            $table->string("user_platform_role");
            $table->string("activity");
            $table->string("account_modified_name");
            $table->string("account_modified_email");
            $table->string("account_modified_previous_role")->nullable();
            $table->string("account_modified_current_role")->nullable();
            $table->unsignedInteger("app_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_activity_user_roles');
    }
}
