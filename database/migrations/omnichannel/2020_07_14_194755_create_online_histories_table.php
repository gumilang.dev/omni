<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnlineHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("app_id");
            $table->string("start_at");
            $table->string("end_at");
            $table->string("status");
            $table->string("reason_offline");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_histories');
    }
}
