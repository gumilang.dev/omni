<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreColumnInTableCustomIntegration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("custom_integration", function (Blueprint $table) {
            $table->string("icon")->nullable();
            $table->string("slug")->nullable();
            $table->string("subMenuName")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('table_custom_integration', function (Blueprint $table) {
            //
        });
    }
}
