<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->json('channel_data')->nullable();
            $table->unsignedInteger('room_id');
            $table->morphs('messageable');
            $table->json('content');
            $table->json('data')->nullable();
            $table->softDeletes();
            $table->timestamps();
            
            // foreign room_id on rooms            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
