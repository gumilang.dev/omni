<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExternalApiEndpointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('external_api_endpoints', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('external_api_id');
            $table->string('name');
            $table->string('endpoint');
            $table->string('method');
            $table->text('request_body')->nullable();
            $table->string('remark')->nullable();
            $table->string('description')->nullable();            
            $table->timestamps();

            $table->foreign('external_api_id')->references('id')->on('external_apis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('external_api_endpoints');
    }
}
