<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubmenusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('submenus', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->unsignedInteger('menu_id')->nullable();
            $table->string('name')->nullable();
            $table->string('icon')->nullable();
			$table->string('slug')->unique();
			$table->timestamps();
			$table->softDeletes();
      
      // foreign menus_id on menus 
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('submenus');
	}

}
