<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHistoryAssignment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_assignments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("customer_name");
            $table->string("customer_email"); // orang yang menerima assign
            $table->string("assignor"); // orang yang mengirim assignment
            $table->string("assignor_role"); // orang yang mengirim assignment
            $table->string("assignee"); // orang yang mengirim assignment
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_assignments');
    }
}
