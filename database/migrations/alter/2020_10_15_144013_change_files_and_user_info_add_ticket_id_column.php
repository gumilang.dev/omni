<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFilesAndUserInfoAddTicketIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("submit_ticket",function(Blueprint $table) {
          $table->string("ticket_id")->nullable();
          $table->json("user_info")->nullable()->change();
          $table->json("files")->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table("submit_ticket",function(Blueprint $table) {
        $table->dropColumn("ticket_id");
      });
    }
}
