<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAutoResolveMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("apps",function(Blueprint $table) {
          $table->string("auto_resolve_message")->default("Apakah ada yang bisa di bantu lagi ka ? jika tidak chatnya saya resolve ya.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("apps",function(Blueprint $table) {
          $table->dropColumn("auto_resolve_message");
        });
    }
}
