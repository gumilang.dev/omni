<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnrAutoResolveAndAutoResolveTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table("apps",function(Blueprint $table) {
        $table->boolean("auto_resolve")->default(false);
        $table->unsignedInteger("auto_resolve_time")->default(5);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table("apps",function(Blueprint $table) {
        $table->dropColumn("auto_resolve");
        $table->dropColumn("auto_resolve_time");
      });
    }
}
