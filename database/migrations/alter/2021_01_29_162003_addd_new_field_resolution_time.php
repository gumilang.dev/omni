<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdddNewFieldResolutionTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	Schema::table("omnichannel.submit_ticket",function(Blueprint $table) {
		$table->timestamp("resolution_time")->nullable();
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table("omnichannel.submit_ticket",function(Blueprint $table){
	    	$table->dropColumn("resolution_time");
	    });
    }
}
