<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFilesPhoneNameEmailInSubmitTicket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('submit_ticket', function (Blueprint $table) {
            $table->json("user_info")->nullable();
            $table->json("files")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('submit_ticket', function (Blueprint $table) {
            $table->dropColumn("user_info");
            $table->dropColumn("files");
        });
    }
}
