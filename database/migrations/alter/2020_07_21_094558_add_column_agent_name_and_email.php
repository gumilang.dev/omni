<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAgentNameAndEmail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('time_report_served', function (Blueprint $table) {
        $table->string("agent_name");
        $table->string("agent_email");
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('time_report_served', function (Blueprint $table) {
        $table->dropColumn("agent_name");
        $table->dropColumn("agent_email");
      });
    }
}
