<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditAppUserPlatformTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('app_user_platform', function (Blueprint $table) {
          $table->text('token')->nullable();
          $table->text('fcm_key')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('app_user_platform', function (Blueprint $table) {
          $table->dropColumn('token');
          $table->dropColumn('fcm_key');
        });
    }
}
