<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDynamicFieldWebchat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("webchat_styles", function (Blueprint $table) {
          $default_value = json_decode('[
              "name" => "name",
              "type" => "type",
              "is_required" => false
            ]');
            $table->boolean("is_use_dynamic_fields")->default(false);
            $table->json("fields")->default($default_value)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table("webchat_styles", function (Blueprint $table) {
        $table->dropColumn("is_use_dynamic_fields");
        $table->dropColumn("fields");
      });
    }
}
