<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDataTypeSolvedAt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table("omnichannel.submit_ticket",function(Blueprint $table) {
        $table->string("solved_at")->change();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table("omnichannel.submit_ticket",function(Blueprint $table) {
        $table->timestamp("solved_at")->change();
      });
    }
}
