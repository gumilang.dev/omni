<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAutoSendSurveyWhenReoslveConvesation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table("apps",function(Blueprint $table) {
        $table->boolean("auto_send_rating_message")->default(false);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table("apps",function(Blueprint $table) {
        $table->dropColumn("auto_send_rating_message");
      });
    }
}
