<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveDynamicFieldWebchat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("webchat_styles", function (Blueprint $table) {
          $table->dropColumn("fields");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table("webchat_styles", function (Blueprint $table) {
        $default_value = json_decode('[
            "name" => "name",
            "type" => "type",
            "is_required" => false
          ]');
        $table->json("fields")->default($default_value)->nullable();
      });
    }
}
