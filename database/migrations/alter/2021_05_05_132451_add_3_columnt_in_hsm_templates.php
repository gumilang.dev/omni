<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add3ColumntInHsmTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hsm_templates', function (Blueprint $table) {
            $table->json("header")->nullable();
            $table->json("footer")->nullable();
            $table->json("button")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hsm_templates', function (Blueprint $table) {
            $table->dropColumn("header");
            $table->dropColumn("footer");
            $table->dropColumn("button");
        });
    }
}
