<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexInMessageStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('message_statuses', function (Blueprint $table) {
            $table->index(["message_id","messageable_type","created_at","read_at"]);
        });
        Schema::table('messages', function (Blueprint $table) {
          $table->index(["room_id"]);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('message_status', function (Blueprint $table) {
            $table->dropIndex(["message_id","messageable_type"]);
        });
        Schema::table('message', function (Blueprint $table) {
          $table->dropIndex(["room_id"]);
      });
    }
}
