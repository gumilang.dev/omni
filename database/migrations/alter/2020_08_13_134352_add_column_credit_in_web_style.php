<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCreditInWebStyle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('webchat_styles', function (Blueprint $table) {
          $table->string("credit_image")->default("/images/pictures/webchat/lenna_credit.png");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('webchat_styles', function (Blueprint $table) {
          $table->dropColumn("credit_image");
        });
    }
}
