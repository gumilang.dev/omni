<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFieldInUserRoleChannels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_role_channels', function (Blueprint $table) {
          $table->unsignedInteger('app_user_platform_id')->nullable();
          $table->unsignedInteger('user_role_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_role_channels', function (Blueprint $table) {
          $table->dropColumn('app_user_platform_id');
          $table->unsignedInteger('user_role_id')->change();
        });
    }
}
