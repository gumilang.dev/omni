<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUserIdAndAndPriority extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('submit_ticket', function (Blueprint $table) {
            $table->string("priority");
            $table->integer("user_id");
            $table->string("media");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropColumn("priority");
        $table->dropColumn("user_id");
        $table->dropColumn("media");
    }
}
