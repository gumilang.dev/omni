<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOnlineInUserPlatform extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('app_user_platform', function (Blueprint $table) {
            $table->boolean("online")->nullable(1);
            $table->string("reason_offline")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('app_user_platform', function (Blueprint $table) {
            $table->dropColumn("online");
            $table->dropColumn("reason_offline");
        });
    }
}
