<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToHsmTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hsm_templates', function (Blueprint $table) {
            $table->string('category')->nullable();
            $table->string('languange')->nullable();
            $table->string('params')->nullable();
            $table->text('preview')->nullable();
            $table->string('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hsm_templates', function (Blueprint $table) {
            $table->dropColumn('category');
            $table->dropColumn('languange');
            $table->dropColumn('params');
            $table->dropColumn('preview');
            $table->dropColumn('status');
        });
    }
}
