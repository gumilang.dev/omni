<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWebchatStylesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table("webchat_styles",function(Blueprint $table) {
          $table->renameColumn('launcher', 'launcher_open');
          $table->string("launcher_open_type")->default('color');
          $table->string("launcher_close_type")->default('color');
          $table->string("launcher_close")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table("webchat_styles", function(Blueprint $table) {
          $table->renameColumn('launcher_open', 'launcher');
          $table->dropColumn("launcher_open_type");
          $table->dropColumn("launcher_close_type");
          $table->dropColumn("launcher_close");
        });
    }
}
