<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIdInReportTimeServed extends Migration
{
    /**
     * Run the migrations.
     *
     */
    public function up()
    {
        Schema::table('time_report_served', function (Blueprint $table) {
            $table->increments('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('time_report_served', function (Blueprint $table) {
            $table->dropColumn("id");
        });
    }
}
