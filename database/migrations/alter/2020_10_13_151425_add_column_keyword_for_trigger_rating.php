<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnKeywordForTriggerRating extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("apps",function(Blueprint $table){
          $table->string("keyword_for_trigger_rating")->default("");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table("apps",function(Blueprint $table){
        $table->dropColumn("keyword_for_trigger_rating");
      });
    }
}
