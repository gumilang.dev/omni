<?php

use Illuminate\Database\Seeder;
use App\Modules\Module\Models\Module;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        DB::table('modules')->truncate();
        $module = [
          [
            'name' => 'Dashboard',
            'slug' => 'dashboard',
            'type' => 'internal',
            'url' => 'dashboard',
            'route' => 'dashboard',
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta')
          ],
          [
            'name' => 'Conversation Studio',
            'slug' => 'conversation-studio',
            'type' => 'external',
            'url' => 'conversation-studio',
            'route' => 'conversation-studio',
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta')
          ],
          [
            'name' => 'Omni Messaging',
            'slug' => 'omni-messaging',
            'type' => 'internal',
            'url' => 'omni-messaging',
            'route' => 'omni-messaging',
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta')
          ],
          // [
          //   'name' => 'Modules',
          //   'slug' => 'modules',
          //   'type' => 'internal',
          //   'url' => 'modules',
          //   'route' => 'modules',
          //   'created_at' => Carbon::now('Asia/Jakarta'),
          //   'updated_at' => Carbon::now('Asia/Jakarta')
          // ],
          [
            'name' => 'Integration',
            'slug' => 'integration',
            'type' => 'internal',
            'url' => 'integration',
            'route' => 'integration',
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta')
          ],
          [
            'name' => 'Sales',
            'slug' => 'sales',
            'type' => 'internal',
            'url' => 'sales',
            'route' => 'sales',
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta')
          ],
          [
            'name' => 'Ticketing',
            'slug' => 'ticketing',
            'type' => 'internal',
            'url' => 'ticketing',
            'route' => 'ticketing',
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta')
          ],
        ];
        Module::insert($module);
        DB::commit();
    }
}
