<?php

use Illuminate\Database\Seeder;
use DB as Database;
use Carbon\Carbon;

class PartnerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Database::table('omnichannel.partner')->truncate();
      $data =  [
        [
          "name" => "Qontak",
          "created_at" => Carbon::now()->toDateTimeString(),
          "updated_at" => Carbon::now()->toDateTimeString()
        ]
      ];

      Database::table("omnichannel.partner")->insert($data);
    }
}
