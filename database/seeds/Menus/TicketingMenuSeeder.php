<?php
namespace Database\Seeds\Menus;

use Illuminate\Database\Seeder;
use App\Modules\Menu\Models\Menu;
use App\Modules\Menu\Models\Submenu;
use Carbon\Carbon;

class TicketingMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = [
          'name' => 'Ticketing',
          'slug' => 'ticketing',
          'icon' => 'images/menu/ticket.svg',
          'created_at' => Carbon::now('Asia/Jakarta'),
          'updated_at' => Carbon::now('Asia/Jakarta'),
          'plans' => json_encode([2,3]),
          'module_id' => 3
        ];
        Menu::insert($menu);
    }
}
