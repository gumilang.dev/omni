<?php
namespace Database\Seeds\Menus;

use Illuminate\Database\Seeder;
use App\Modules\Menu\Models\Menu;
use Carbon\Carbon;

class DashboardMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = [
          'name' => 'Dashboard',
          'slug' => 'dashboard',
          'icon' => 'images/menu/dashboard.svg',
          'created_at' => Carbon::now('Asia/Jakarta'),
          'updated_at' => Carbon::now('Asia/Jakarta'),
          'plans' => json_encode([2,3]),
          'module_id' => 3
        ];
        Menu::insert($menu);
    }
}
