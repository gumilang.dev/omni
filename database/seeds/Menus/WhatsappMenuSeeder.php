<?php
namespace Database\Seeds\Menus;

use App\Modules\Menu\Models\Menu;
use App\Modules\Menu\Models\Submenu;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class WhatsappMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = new Menu;
        $menu->name = 'Whatsapp';
        $menu->slug = 'whatsapp';
        $menu->icon = 'whatsapp.svg';
        $menu->plans = [2,3];
        $menu->module_id = 3;
        $menu->save();

        $submenu = [
          [
              'menu_id' => $menu->id,
              'name' => 'Broadcase Message',
              'slug' => 'broadcast-message',
              'plans' => json_encode([2,3])
          ],
          [
              'menu_id' => $menu->id,
              'name' => 'Broadcast Template',
              'slug' => 'broadcast-template',
              'plans' => json_encode([2,3])
          ]
        ];
        Submenu::insert($submenu);
    }
}
