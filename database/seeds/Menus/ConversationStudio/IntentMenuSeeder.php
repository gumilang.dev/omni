<?php
namespace Database\Seeds\Menus\ConversationStudio;

use Illuminate\Database\Seeder;
use App\Modules\Menu\Models\Menu;
use App\Modules\Menu\Models\Submenu;
use Carbon\Carbon;

class IntentMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $menu = new Menu;
      $menu->name = 'Intent';
      $menu->slug = 'intent';
      $menu->icon = 'intent.svg';
      $menu->plans = [1,2,3];
      $menu->module_id = 2;
      $menu->save();
      $submenu = [
          [
              'menu_id' => $menu->id,
              'name' => 'Story Groups',
              'slug' => 'story-groups',
              'plans' => json_encode([1,2,3])
          ],
          [
              'menu_id' => $menu->id,
              'name' => 'Active Stories',
              'slug' => 'active-stories',
              'plans' => json_encode([1,2,3])
          ],
          [
            'menu_id' => $menu->id,
            'name' => 'Function List',
            'slug' => 'function-list',
            'plans' => json_encode([1,2,3])
          ],
      ];
      Submenu::insert($submenu);
    }
}
