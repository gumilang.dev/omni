<?php
namespace Database\Seeds\Menus\ConversationStudio;

use Illuminate\Database\Seeder;
use App\Modules\Menu\Models\Menu;
use App\Modules\Menu\Models\Submenu;
use Carbon\Carbon;

class SettingMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $menu = new Menu;
      $menu->name = 'Setting';
      $menu->slug = 'setting';
      $menu->icon = 'setting.svg';
      $menu->plans = [1,2,3];
      $menu->module_id = 2;
      $menu->save();
      $submenu = [
          [
              'menu_id' => $menu->id,
              'name' => 'General Setting',
              'slug' => 'general-setting',
              'plans' => json_encode([1,2,3])
          ],
          [
              'menu_id' => $menu->id,
              'name' => 'Persona',
              'slug' => 'persona',
              'plans' => json_encode([1,2,3])
          ],
          // [
          //   'menu_id' => $menu->id,
          //   'name' => 'Team Member',
          //   'slug' => 'team-member',
          //   'plans' => json_encode([1,2,3])
          // ]
      ];
      Submenu::insert($submenu);
    }
}
