<?php
namespace Database\Seeds\Menus\ConversationStudio;

use Illuminate\Database\Seeder;
use App\Modules\Menu\Models\Menu;
use App\Modules\Menu\Models\Submenu;
use Carbon\Carbon;

class NLUMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = new Menu;
        $menu->name = 'NLU';
        $menu->slug = 'nlu';
        $menu->icon = 'nlu.svg';
        $menu->plans = [1,2,3];
        $menu->module_id = 2;
        $menu->save();
        $submenu = [
            [
                'menu_id' => $menu->id,
                'name' => 'Entities',
                'slug' => 'entities',
                'plans' => json_encode([1,2,3])
            ],
            [
                'menu_id' => $menu->id,
                'name' => 'Voice Replace',
                'slug' => 'voice-replace',
                'plans' => json_encode([1,2,3])
            ],
            [
              'menu_id' => $menu->id,
              'name' => 'Synonyms',
              'slug' => 'synonyms',
              'plans' => json_encode([1,2,3])
            ],
            [
              'menu_id' => $menu->id,
              'name' => 'Fallback List',
              'slug' => 'fallback-list',
              'plans' => json_encode([1,2,3])
            ]
        ];
        Submenu::insert($submenu);
    }
}
