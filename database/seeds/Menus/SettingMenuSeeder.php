<?php
namespace Database\Seeds\Menus;

use Illuminate\Database\Seeder;
use App\Modules\Menu\Models\Menu;
use App\Modules\Menu\Models\Submenu;
use App\Modules\Menu\Models\SubSubmenu;
use Carbon\Carbon;

class SettingMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = new Menu;
        $menu->name = 'Setting';
        $menu->slug = 'setting';
        $menu->icon = 'setting.svg';
        $menu->plans = [1,2,3];
        $menu->module_id = 3;
        $menu->save();

        $submenu = new Submenu;
        $submenu->name = 'App Setting';
        $submenu->slug = 'app-setting';
        $submenu->menu_id = $menu->id;
        $submenu->plans = [2,3];
        $submenu->save();

        $subSubmenu = [
            [
              'sub_submenu_id' => $submenu->id,
              'name' => 'App Configuration',
              'slug' => 'app-configuration',
              'icon' => 'app-configuration.png',
              'category' => 'setting',
              'created_at' => Carbon::now('Asia/Jakarta'),
              'updated_at' => Carbon::now('Asia/Jakarta'),
              'plans' => json_encode([2,3])
            ],
            [
              'sub_submenu_id' => $submenu->id,
              'name' => 'App Content',
              'slug' => 'app-content',
              'icon' => 'app-content.png',
              'category' => 'setting',
              'created_at' => Carbon::now('Asia/Jakarta'),
              'updated_at' => Carbon::now('Asia/Jakarta'),
              'plans' => json_encode([2,3])
            ],
            [
              'sub_submenu_id' => $submenu->id,
              'name' => 'General Value',
              'slug' => 'general-value',
              'icon' => 'general-value.png',
              'category' => 'setting',
              'created_at' => Carbon::now('Asia/Jakarta'),
              'updated_at' => Carbon::now('Asia/Jakarta'),
              'plans' => json_encode([2,3])
            ],
            [
              'sub_submenu_id' => $submenu->id,
              'name' => 'PPOB Product',
              'slug' => 'ppob-product',
              'icon' => 'ppob-product.png',
              'category' => 'setting',
              'created_at' => Carbon::now('Asia/Jakarta'),
              'updated_at' => Carbon::now('Asia/Jakarta'),
              'plans' => json_encode([2,3])
            ],
            [
              'sub_submenu_id' => $submenu->id,
              'name' => 'Integration',
              'slug' => 'integration',
              'icon' => 'integration.png',
              'category' => 'setting',
              'created_at' => Carbon::now('Asia/Jakarta'),
              'updated_at' => Carbon::now('Asia/Jakarta'),
              'plans' => json_encode([1,2,3])
            ],
            [
              'sub_submenu_id' => $submenu->id,
              'name' => 'Team Management',
              'slug' => 'team-management',
              'icon' => 'team-management.png',
              'category' => 'agent',
              'created_at' => Carbon::now('Asia/Jakarta'),
              'updated_at' => Carbon::now('Asia/Jakarta'),
              'plans' => json_encode([2,3])
            ],
            [
              'sub_submenu_id' => $submenu->id,
              'name' => 'Group Management',
              'slug' => 'group-management',
              'icon' => 'group-management.png',
              'category' => 'agent',
              'created_at' => Carbon::now('Asia/Jakarta'),
              'updated_at' => Carbon::now('Asia/Jakarta'),
              'plans' => json_encode([2,3])
            ],
            [
              'sub_submenu_id' => $submenu->id,
              'name' => 'Role Management',
              'slug' => 'role-management',
              'icon' => 'role-management.png',
              'category' => 'agent',
              'created_at' => Carbon::now('Asia/Jakarta'),
              'updated_at' => Carbon::now('Asia/Jakarta'),
              'plans' => json_encode([2,3])
            ],
            [
              'sub_submenu_id' => $submenu->id,
              'name' => 'Auto Response & Office Hour',
              'slug' => 'auto-response-office-hour',
              'icon' => 'office-hour.png',
              'category' => 'chat',
              'created_at' => Carbon::now('Asia/Jakarta'),
              'updated_at' => Carbon::now('Asia/Jakarta'),
              'plans' => json_encode([1,2,3])
            ],
            [
              'sub_submenu_id' => $submenu->id,
              'name' => 'Message Template',
              'slug' => 'message-template',
              'icon' => 'msg-template.png',
              'category' => 'chat',
              'created_at' => Carbon::now('Asia/Jakarta'),
              'updated_at' => Carbon::now('Asia/Jakarta'),
              'plans' => json_encode([2,3])
            ],
            [
              'sub_submenu_id' => $submenu->id,
              'name' => 'Tags Configuration',
              'slug' => 'tags',
              'icon' => 'tags-configuration.png',
              'category' => 'chat',
              'created_at' => Carbon::now('Asia/Jakarta'),
              'updated_at' => Carbon::now('Asia/Jakarta'),
              'plans' => json_encode([1,2,3])
            ],
            [
              'sub_submenu_id' => $submenu->id,
              'name' => 'Custom Filter Management',
              'slug' => 'custom-filter',
              'icon' => 'custom-filter.png',
              'category' => 'chat',
              'created_at' => Carbon::now('Asia/Jakarta'),
              'updated_at' => Carbon::now('Asia/Jakarta'),
              'plans' => json_encode([2,3])
            ],
            [
              'sub_submenu_id' => $submenu->id,
              'name' => 'Customer Blocker',
              'slug' => 'customer-blocker',
              'icon' => 'ipblocker.png',
              'category' => 'security',
              'created_at' => Carbon::now('Asia/Jakarta'),
              'updated_at' => Carbon::now('Asia/Jakarta'),
              'plans' => json_encode([2,3])
            ]
        ];
        SubSubmenu::insert($subSubmenu);
    }
}
