<?php
namespace Database\Seeds\Menus;

use Illuminate\Database\Seeder;
use App\Modules\Menu\Models\Menu;
use App\Modules\Menu\Models\Submenu;
use Carbon\Carbon;

class BroadcastMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = new Menu;
        $menu->name = 'Broadcast';
        $menu->slug = 'broadcast';
        $menu->icon = 'broadcast.svg';
        $menu->plans = [2,3];
        $menu->module_id = 3;
        $menu->save();
        $submenu = [
            [
                'menu_id' => $menu->id,
                'name' => 'Broadcast Message',
                'slug' => 'message',
                'plans' => json_encode([2,3])
            ],
            [
                'menu_id' => $menu->id,
                'name' => 'WA Non Hsm',
                'slug' => 'whatsapp-non-hsm',
                'plans' => json_encode([2,3])
            ],
            // [
            //   'menu_id' => $menu->id,
            //   'name' => 'WA Hsm',
            //   'slug' => 'whatsapp',
            //   'plans' => json_encode([2,3])
            // ],
        ];
        Submenu::insert($submenu);
    }
}
