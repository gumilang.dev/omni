<?php
namespace Database\Seeds\Menus;

use Illuminate\Database\Seeder;
use App\Modules\Menu\Models\Menu;
use App\Modules\Menu\Models\Submenu;
use Carbon\Carbon;

class SalesMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = new Menu;
        $menu->name = 'Sales';
        $menu->slug = 'sales';
        $menu->icon = 'sales.svg';
        $menu->plans = [2,3];
        $menu->module_id = 3;
        $menu->save();
        $submenu = [
            [
                'menu_id' => $menu->id,
                'name' => 'Sales',
                'slug' => 'sales',
                // 'created_at' => Carbon::now('Asia/Jakarta'),
                // 'updated_at' => Carbon::now('Asia/Jakarta'),
                'plans' => json_encode([2,3])
            ]
        ];
        Submenu::insert($submenu);
    }
}
