<?php
namespace Database\Seeds\Menus;

use Illuminate\Database\Seeder;
use App\Modules\Menu\Models\Menu;
use App\Modules\Menu\Models\Submenu;
use Carbon\Carbon;

class ChatMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = new Menu;
        $menu->name = 'Conversation';
        $menu->slug = 'conversation';
        $menu->icon = 'conversation.svg';
        $menu->plans = [1,2,3];
        $menu->module_id = 3;
        $menu->save();
        $submenu = [
          [
              'menu_id' => $menu->id,
              'name' => 'Chat Panel',
              'slug' => 'chat-panel',
              'plans' => json_encode([1,2,3]),
          ]
        ];
        Submenu::insert($submenu);
    }
}
