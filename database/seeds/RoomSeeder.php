<?php

use Illuminate\Database\Seeder;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory("App\Modules\Room\Models\Room", 1)->create();
       // seeding participants table
        $data = [
            [
                "room_id" => 1,
                'participable_id' => 1,
                'participable_type' => "user",
            ],
        ];
        DB::table('participables')->insert($data);
    }
}
