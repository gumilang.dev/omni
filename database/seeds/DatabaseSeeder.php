<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MenuSeeder::class);
        // $this->call(AppSeeder::class);
        $this->call(ChannelSeeder::class);
        $this->call(ExternalApiSeeder::class);
        // $this->call(UserSeeder::class);
        // $this->call(AppSeeder::class);
        // $this->call(InterestSeeder::class);
        // $this->call(RoomSeeder::class);
    }
}
