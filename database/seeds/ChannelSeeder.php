<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ChannelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::beginTransaction();
      DB::table('channels')->truncate();
      $data = [
        [ "name"=>"line"],
        [ "name"=>"facebook"],
        [ "name"=>"telegram"],
        [ "name"=>"whatsapp"],
        [ "name"=>"webchat"],
        [ "name"=>"bot"],
        [ "name"=>"mobile"],
        [ "name"=>"mobile-omni"],
        [ "name"=>"twitter"]
      ];
      DB::table('channels')->insert($data);
      DB::commit();
    }
}
