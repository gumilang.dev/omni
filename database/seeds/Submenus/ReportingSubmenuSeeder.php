<?php
namespace Database\Seeds\Submenus;

use App\Modules\Menu\Models\Submenu;
use App\Modules\Menu\Models\Menu;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ReportingSubmenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = Menu::where('slug','reporting')->first();
        $submenu = [
          [
              'menu_id' => $menu->id,
              'name' => 'Customers',
              'slug' => 'user-report',
              'icon' => 'images/submenu/customers.svg',
              'description' => 'Report customers of your entire conversations',
              'created_at' => Carbon::now('Asia/Jakarta'),
              'updated_at' => Carbon::now('Asia/Jakarta'),
              'plans' => json_encode([1,2,3])
          ],
          [
            'menu_id' => $menu->id,
            'name' => 'Messages',
            'slug' => 'messages',
            'icon' => 'images/submenu/messages.svg',
            'description' => 'Report all chat messages of your customers',
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
            'plans' => json_encode([2,3])
          ],
          [
            'menu_id' => $menu->id,
            'name' => 'Room Messages',
            'slug' => 'room-msg',
            'icon' => 'images/submenu/room_messages.svg',
            'description' => 'Report room messages',
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
            'plans' => json_encode([2,3])
          ],
          [
            'menu_id' => $menu->id,
            'name' => 'Agent Performance',
            'slug' => 'agent-performance',
            'icon' => 'images/submenu/agent_performance.svg',
            'description' => 'Report of entire agent performance',
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
            'plans' => json_encode([2,3])
          ],
          [
            'menu_id' => $menu->id,
            'name' => 'Report HSM',
            'slug' => 'hsm',
            'icon' => 'images/submenu/hsm_report.svg',
            'description' => 'Report of your HSM usage',
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
            'plans' => json_encode([2,3])
          ],
          [
            'menu_id' => $menu->id,
            'name' => 'Report Tag',
            'slug' => 'tag',
            'icon' => 'images/submenu/tags.svg',
            'description' => 'Report tag',
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
            'plans' => json_encode([1,2,3])
          ],
          [
            'menu_id' => $menu->id,
            'name' => 'Note',
            'slug' => 'note',
            'icon' => 'images/submenu/tags.svg',
            'description' => 'Report Note',
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
            'plans' => json_encode([1,2,3])
          ],
          [
            'menu_id' => $menu->id,
            'name' => 'Served Agent',
            'slug' => 'time-serve',
            'icon' => 'images/submenu/agent_performance.svg',
            'description' => 'Report Time Until Agent Handle Chat Customer',
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
            'plans' => json_encode([2,3])
          ],
          [
            'menu_id' => $menu->id,
            'name' => 'Handle Admin',
            'slug' => 'handle-room',
            'icon' => 'images/submenu/room_messages.svg',
            'description' => 'Report Room Handle By Admin',
            'created_at' => Carbon::now('Asia/Jakarta'),
            'updated_at' => Carbon::now('Asia/Jakarta'),
            'plans' => json_encode([1,2,3])
          ]
        ];
        Submenu::insert($submenu);
    }
}
