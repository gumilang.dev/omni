<?php
namespace Database\Seeds\ExternalApis;

use Illuminate\Database\Seeder;
use App\Modules\ExternalApi\Models\ExternalApi;
use App\Modules\ExternalApi\Models\ExternalApiEndpoint;
use Carbon\Carbon;

class DamcorpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $extApi = new ExternalApi;
        $extApi->category = 'channel';
        $extApi->provider = 'damcorp';
        $extApi->base_url = 'https://waba.damcorp.id/whatsapp/';
        $extApi->created_at = Carbon::now();
        $extApi->updated_at = Carbon::now();
        $extApi->save();

        $endpointsData = [
            [
                'external_api_id' => $extApi->id,
                'name' => 'send-text',
                'endpoint' => 'whatsapp/sendText',
                'description' => 'send free form message',
                'method' => 'post'
            ],
            [
                'external_api_id' => $extApi->id,
                'name' => 'send-image',
                'endpoint' => 'whatsapp/sendimage',
                'description' => 'send image',
                'method' => 'post'
            ],
            [
                'external_api_id' => $extApi->id,
                'name' => 'send-document',
                'endpoint' => 'whatsapp/sendDocument',
                'description' => 'send document',
                'method' => 'post'
            ],
            [
                'external_api_id' => $extApi->id,
                'name' => 'send-hsm',
                'endpoint' => 'whatsapp/sendHsm',
                'description' => 'send hsm',
                'method' => 'post'
            ],
            [
                'external_api_id' => $extApi->id,
                'name' => 'create-group',
                'endpoint' => 'group',
                'description' => 'create group damcorp',
                'method' => 'post'
            ],
        ];
        ExternalApiEndpoint::insert($endpointsData);
    }
}
