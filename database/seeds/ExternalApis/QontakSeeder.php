<?php
namespace Database\Seeds\ExternalApis;

use Illuminate\Database\Seeder;
use App\Modules\ExternalApi\Models\ExternalApi;
use App\Modules\ExternalApi\Models\ExternalApiEndpoint;
use Carbon\Carbon;

class QontakSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $extApi = new ExternalApi;
        $extApi->category = 'channel';
        $extApi->provider = 'qontak';
        $extApi->base_url = 'https://{url}/';
        $extApi->created_at = Carbon::now();
        $extApi->updated_at = Carbon::now();
        $extApi->save();

        $endpointsData = [
            [
                'external_api_id' => $extApi->id,
                'name' => 'v1-contacts',
                'endpoint' => 'v1/contacts',
                'description' => 'check wa id qontak',
                'method' => 'post'
            ],
            [
                'external_api_id' => $extApi->id,
                'name' => 'v1-messages',
                'endpoint' => 'v1/messages',
                'description' => 'send messages / hsm / image / document',
                'method' => 'post'
            ],
            [
                'external_api_id' => $extApi->id,
                'name' => 'v1-users-login',
                'endpoint' => 'v1/users/login',
                'description' => 'login user to get token',
                'method' => 'post'
            ],
            [
                'external_api_id' => $extApi->id,
                'name' => 'v1-groups',
                'endpoint' => 'v1/groups',
                'description' => 'list group, add group, dll',
                'method' => 'post'
            ],
            [
                'external_api_id' => $extApi->id,
                'name' => 'v1-media',
                'endpoint' => 'v1/media',
                'description' => 'media',
                'method' => 'get'
            ]
        ];
        ExternalApiEndpoint::insert($endpointsData);
    }
}
