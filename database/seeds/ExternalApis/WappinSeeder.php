<?php
namespace Database\Seeds\ExternalApis;

use Illuminate\Database\Seeder;
use App\Modules\ExternalApi\Models\ExternalApi;
use App\Modules\ExternalApi\Models\ExternalApiEndpoint;
use Carbon\Carbon;

class WappinSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $extApi = new ExternalApi;
        $extApi->category = 'channel';
        $extApi->provider = 'wappin';
        $extApi->base_url = 'https://{subdomain}.wappin.id/';
        $extApi->created_at = Carbon::now();
        $extApi->updated_at = Carbon::now();
        $extApi->save();

        $endpointsData = [
            [
                'external_api_id' => $extApi->id,
                'name' => 'v1-token-get',
                'endpoint' => 'v1/token/get',
                'description' => 'generate token for HSM',
                'method' => 'post'
            ],
            [
                'external_api_id' => $extApi->id,
                'name' => 'v1-message-do-send-hsm',
                'endpoint' => 'v1/message/do-send-hsm',
                'description' => 'send HSM & api send notification with sms',
                'method' => 'post'
            ],
            [
                'external_api_id' => $extApi->id,
                'name' => 'v1-messages',
                'endpoint' => 'v1/messages',
                'description' => 'api send message text',
                'method' => 'post'
            ],
            [
                'external_api_id' => $extApi->id,
                'name' => 'v1-media',
                'endpoint' => 'v1/media',
                'description' => 'api retrieve media (document, image, audio)',
                'method' => 'get'
            ],
            // [
            //     'external_api_id' => $extApi->id,
            //     'name' => 'v1-inquiry',
            //     'endpoint' => 'v1/message/inquiry',
            //     'description' => 'Message inquiry API is used to check the status of messages that have been sent',
            //     'method' => 'post'
            // ],
            [
              'external_api_id' => $extApi->id,
              'name' => 'v1-contacts',
              'endpoint' => 'v1/contacts',
              'description' => 'check valid contact of users',
              'method' => 'post'
            ],
            [
              'external_api_id' => $extApi->id,
              'name' => 'v1-users-login',
              'endpoint' => 'v1/users/login',
              'description' => 'login to get token for send message',
              'method' => 'post'
            ],
        ];
        ExternalApiEndpoint::insert($endpointsData);
    }
}
