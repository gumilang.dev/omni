<?php
namespace Database\Seeds\ExternalApis;

use Illuminate\Database\Seeder;
use App\Modules\ExternalApi\Models\ExternalApi;
use App\Modules\ExternalApi\Models\ExternalApiEndpoint;
use Carbon\Carbon;

class SocketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $extApi = new ExternalApi;
        $extApi->category = 'internal';
        $extApi->provider = 'socket';
        $extApi->base_url = env('SOCKET_URL');
        $extApi->created_at = Carbon::now();
        $extApi->updated_at = Carbon::now();
        $extApi->save();

        $endpointsData = [
            [
                'external_api_id' => $extApi->id,
                'name' => 'check-status',
                'endpoint' => 'apps/{socket-appid}/status',
                'method' => 'get'
            ]
        ];
        ExternalApiEndpoint::insert($endpointsData);
    }
}
