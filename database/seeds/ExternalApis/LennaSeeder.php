<?php
namespace Database\Seeds\ExternalApis;

use Illuminate\Database\Seeder;
use App\Modules\ExternalApi\Models\ExternalApi;
use App\Modules\ExternalApi\Models\ExternalApiEndpoint;
use Carbon\Carbon;

class LennaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $extApi = new ExternalApi;
        $extApi->category = 'internal';
        $extApi->provider = 'lenna';
        $extApi->base_url = env('INTERNAL_BASE_URL');
        $extApi->created_at = Carbon::now();
        $extApi->updated_at = Carbon::now();
        $extApi->save();

        $endpointsData = [
            // [
            //     'external_api_id' => $extApi->id,
            //     'name' => 'nlp',
            //     'endpoint' => 'nlp/public/api/inference',
            //     'method' => 'post'
            // ],
            [
                'external_api_id' => $extApi->id,
                'name' => 'transaction-detail',
                'endpoint' => 'backend/api/transaction/detail',
                'method' => 'post'
            ],
            [
                'external_api_id' => $extApi->id,
                'name' => 'transaction',
                'endpoint' => 'backend/api/transactions',
                'method' => 'get'
            ],
            [
                'external_api_id' => $extApi->id,
                'name' => 'login',
                'endpoint' => 'backend/api/{app-id}/auth/login',
                'method' => 'post'
            ],
            [
                'external_api_id' => $extApi->id,
                'name' => 'bot-story-list',
                'endpoint' => 'nlp/public/api/story-list/{bot-id}',
                'method' => 'get'
            ],
            [
                'external_api_id' => $extApi->id,
                'name' => 'bot-input-phrase',
                'endpoint' => 'nlp/public/api/input-phrase/{story-id}',
                'method' => 'get'
            ],
        ];
        ExternalApiEndpoint::insert($endpointsData);
    }
}
