<?php
namespace Database\Seeds\ExternalApis;

use Illuminate\Database\Seeder;
use App\Modules\ExternalApi\Models\ExternalApi;
use App\Modules\ExternalApi\Models\ExternalApiEndpoint;
use Carbon\Carbon;

class WhatsappSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $extApi = new ExternalApi;
        $extApi->category = 'channel';
        $extApi->provider = 'whatsapp apiwha';
        $extApi->base_url = 'http://panel.apiwha.com/';
        $extApi->created_at = Carbon::now();
        $extApi->updated_at = Carbon::now();
        $extApi->save();

        $endpointsData = [
          [
            'external_api_id'=>$extApi->id,
            'name'=>'send-message',
            'endpoint'=>'send_message.php',
            'description'=>'get method use url query, post use form-data',
            'method'=>'post'
          ]
        ];
        ExternalApiEndpoint::insert($endpointsData);
    }
}
