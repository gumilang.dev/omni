<?php
namespace Database\Seeds\ExternalApis;

use Illuminate\Database\Seeder;
use App\Modules\ExternalApi\Models\ExternalApi;
use App\Modules\ExternalApi\Models\ExternalApiEndpoint;
use Carbon\Carbon;

class LineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $extApi = new ExternalApi;
        $extApi->category = 'channel';
        $extApi->provider = 'line';
        $extApi->base_url = 'https://api.line.me/v2/';
        $extApi->created_at = Carbon::now();
        $extApi->updated_at = Carbon::now();
        $extApi->save();

        $endpointsData = [
          [
            'external_api_id'=>$extApi->id,
            'name'=>'get-profile',
            'endpoint'=>'bot/profile/{user-id}',
            'method'=>'get'
          ],
          [
            'external_api_id'=>$extApi->id,
            'name'=>'reply-message',
            'endpoint'=>'bot/message/reply',
            'method'=>'post'
          ],
          [
            'external_api_id'=>$extApi->id,
            'name'=>'push-message',
            'endpoint'=>'bot/message/push',
            'method'=>'post'
          ],
          [
            'external_api_id'=>$extApi->id,
            'name'=>'get-content',
            'endpoint'=>'bot/message/{message-id}/content',
            'method'=>'get'
          ],
          [
            'external_api_id'=>$extApi->id,
            'name'=>'send-multicast',
            'endpoint'=>'bot/message/multicast',
            'method'=>'post'
          ],
          
        ];
        ExternalApiEndpoint::insert($endpointsData);
    }
}
