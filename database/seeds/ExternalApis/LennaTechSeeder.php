<?php
namespace Database\Seeds\ExternalApis;

use Illuminate\Database\Seeder;
use App\Modules\ExternalApi\Models\ExternalApi;
use App\Modules\ExternalApi\Models\ExternalApiEndpoint;
use Carbon\Carbon;

class LennaTechSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $extApi = new ExternalApi;
      $extApi->category = 'internal';
      $extApi->provider = 'lenna-tech';
      $extApi->base_url = 'https://dev.lenna.ai/';
      $extApi->created_at = Carbon::now();
      $extApi->updated_at = Carbon::now();
      $extApi->save();

      $endpointsData = [
          [
              'external_api_id' => $extApi->id,
              'name' => 'nlp-chat',
              'endpoint' => 'ai/nlp/chat',
              'description' => 'send payload message to NLP',
              'method' => 'post'
          ]
      ];
      ExternalApiEndpoint::insert($endpointsData);
    }
}
