<?php
namespace Database\Seeds\ExternalApis;

use Illuminate\Database\Seeder;
use App\Modules\ExternalApi\Models\ExternalApi;
use App\Modules\ExternalApi\Models\ExternalApiEndpoint;
use Carbon\Carbon;

class InfobipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $extApi = new ExternalApi;
        $extApi->category = 'channel';
        $extApi->provider = 'infobip';
        $extApi->base_url = 'https://{url-key}.api.infobip.com/';
        $extApi->created_at = Carbon::now();
        $extApi->updated_at = Carbon::now();
        $extApi->save();

        $endpointsData = [
            [
                'external_api_id' => $extApi->id,
                'name' => 'send-advance-message',
                'endpoint' => 'omni/1/advanced',
                'description' => 'send free form message',
                'method' => 'post'
            ],
        ];
        ExternalApiEndpoint::insert($endpointsData);
    }
}
