<?php
namespace Database\Seeds\ExternalApis;

use Illuminate\Database\Seeder;
use App\Modules\ExternalApi\Models\ExternalApi;
use App\Modules\ExternalApi\Models\ExternalApiEndpoint;
use Carbon\Carbon;

class FacebookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $extApi = new ExternalApi;
        $extApi->category = 'channel';
        $extApi->provider = 'facebook';
        $extApi->base_url = 'https://graph.facebook.com/v3.2/';
        $extApi->created_at = Carbon::now();
        $extApi->updated_at = Carbon::now();
        $extApi->save();

        $endpointsData = [
          [
            'external_api_id'=>$extApi->id,
            'name'=>'subscribe-app',
            'endpoint'=>'{nodes}/subscribed_apps',
            'description'=>'{page-id/nodes}/subscribed_apps?access_token={app-access-token}',
            'method'=>'post'
          ],
          [
            'external_api_id'=>$extApi->id,
            'name'=>'unsubscribe-app',
            'endpoint'=>'{nodes}/subscribed_apps',
            'description'=>'{page-id/nodes}/subscribed_apps?access_token={app-access-token}',
            'method'=>'delete'
          ],
          [
            'external_api_id'=>$extApi->id,
            'name'=>'echo-message',
            'endpoint'=>'me/messages',
            'description'=>'{page-id/nodes}/messages?access_token={page-access-token}',
            'method'=>'post'
          ],
          [
            'external_api_id'=>$extApi->id,
            'name'=>'get-user-pages',
            'endpoint'=>'{nodes}/accounts',
            'description'=>'{user-id/nodes}/accounts?access_token={page-access-token}',
            'method'=>'post'
          ],
          [
            'external_api_id'=>$extApi->id,
            'name'=>'get-user-picture',
            'endpoint'=>'{nodes}/picture',
            'description'=>'{user-id/nodes}/picture?params',
            'method'=>'get'
          ],
          [
            'external_api_id'=>$extApi->id,
            'name'=>'get-user-profile',
            'endpoint'=>'{nodes}',
            'description'=>'{nodes}?params',
            'method'=>'get'
          ],
          [
            'external_api_id'=>$extApi->id,
            'name'=>'get-longlived-token',
            'endpoint'=>'oauth/access_token',
            'description'=>'oauth/access_token?grant_type=fb_exchange_token&client_id={app-id}&client_secret={app-secret}&fb_exchange_token={shortlived-user-token}',
            'method'=>'get'
          ],
        ];
        ExternalApiEndpoint::insert($endpointsData);
    }
}
