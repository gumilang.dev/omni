<?php
namespace Database\Seeds\ExternalApis;

use Illuminate\Database\Seeder;
use App\Modules\ExternalApi\Models\ExternalApi;
use App\Modules\ExternalApi\Models\ExternalApiEndpoint;
use Carbon\Carbon;

class TelegramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $extApi = new ExternalApi;
        $extApi->category = 'channel';
        $extApi->provider = 'telegram';
        $extApi->base_url = 'https://api.telegram.org/{params}';
        $extApi->created_at = Carbon::now();
        $extApi->updated_at = Carbon::now();
        $extApi->save();

        $endpointsData = [
          [
            'external_api_id'=>$extApi->id,
            'name'=>'set-webhook',
            'endpoint'=>'setWebhook',
            'method'=>'post',
            'description'=>''
          ],
          [
            'external_api_id'=>$extApi->id,
            'name'=>'delete-webhook',
            'endpoint'=>'deleteWebhook',
            'method'=>'get',
            'description'=>''
          ],
          [
            'external_api_id'=>$extApi->id,
            'name'=>'send-message',
            'endpoint'=>'send{message-type}',
            'method'=>'post',
            'description'=>'available type: Message, Audio, Document, Photo, Document, Voice, Animation, VideoNote, etc',
          ],
          [
            'external_api_id'=>$extApi->id,
            'name'=>'forward-message',
            'endpoint'=>'forwardMessage',
            'method'=>'post',
            'description'=>''
          ],
          [
            'external_api_id'=>$extApi->id,
            'name'=>'get-user-profile',
            'endpoint'=>'getChat',
            'method'=>'post',
            'description'=>''
          ],
          [
            'external_api_id'=>$extApi->id,
            'name'=>'get-file',
            'endpoint'=>'getFile',
            'method'=>'post',
            'description'=>''
          ],
          [
            'external_api_id'=>$extApi->id,
            'name'=>'download-file',
            'endpoint'=>'{file-path}',
            'method'=>'get',
            'description'=>''
          ]
        ];
        ExternalApiEndpoint::insert($endpointsData);
    }
}
