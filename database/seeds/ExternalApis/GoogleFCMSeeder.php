<?php
namespace Database\Seeds\ExternalApis;

use Illuminate\Database\Seeder;
use App\Modules\ExternalApi\Models\ExternalApi;
use App\Modules\ExternalApi\Models\ExternalApiEndpoint;
use Carbon\Carbon;

class GoogleFCMSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $extApi = new ExternalApi;
        $extApi->category = 'broadcast';
        $extApi->provider = 'google';
        $extApi->base_url = 'https://fcm.googleapis.com/';
        $extApi->created_at = Carbon::now();
        $extApi->updated_at = Carbon::now();
        $extApi->save();

        $endpointsData = [
          [
            'external_api_id'=>$extApi->id,
            'name'=>'send-message',
            'endpoint'=>'fcm/send',
            'method'=>'post'
          ],

        ];
        ExternalApiEndpoint::insert($endpointsData);
    }
}
