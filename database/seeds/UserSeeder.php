<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // untuk table user
      factory("App\Modules\User\Models\User", 20)->create();
   
      for ($i=1; $i <=20 ; $i++) {
        $data= [
          [
          "app_id"=>rand(1, 5),
          "user_id"=>rand(1, 20)
          ]
        ];
        // untuk table pivot app_user
        DB::table('app_user')->insert($data);
        $data2= [
          [
          "app_id"=>rand(1, 5),
          "user_id"=>rand(1, 2)
          ]
        ];
        // untuk table pivot app_user_platform
        DB::table('app_user_platform')->insert($data2);


        // seed table interest_user 
        $data = [
          [
          "interest_id"=>rand(1, 10),
          "user_id"=>rand(1, 20)
          ]
        ];
        DB::table('interest_user')->insert($data);
        
      }

     
    }
}
