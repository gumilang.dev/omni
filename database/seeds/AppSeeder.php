<?php

use Illuminate\Database\Seeder;
use App\Modules\App\Models\App;

class AppSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $app = new App;
      $app->name = 'lenna';
      $app->description = 'lenna app';
      $app->status = true;
      $app->created_by = 1;
      $app->save();
    //   $app->userPlatform()->attach($userPlatformId]);

    }
}
