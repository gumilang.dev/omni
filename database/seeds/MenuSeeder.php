<?php

use Illuminate\Database\Seeder;
use Database\Seeds\Menus\CrmMenuSeeder;
use Database\Seeds\Menus\SalesMenuSeeder;
use Database\Seeds\Menus\DashboardMenuSeeder;
use Database\Seeds\Menus\SettingMenuSeeder;
use Database\Seeds\Menus\ChatMenuSeeder;
use Database\Seeds\Menus\BroadcastMenuSeeder;
use Database\Seeds\Menus\ReportingMenuSeeder;
use Database\Seeds\Menus\WhatsappMenuSeeder;
use Database\Seeds\Menus\TicketingMenuSeeder;
use Database\Seeds\Submenus\ReportingSubmenuSeeder;
use Database\Seeds\Menus\ConversationStudio\IntentMenuSeeder;
use Database\Seeds\Menus\ConversationStudio\NLUMenuSeeder;
use Database\Seeds\Menus\ConversationStudio\SettingMenuSeeder as BotSettingMenuSeeder;
use Illuminate\Support\Facades\DB;
// use Exception;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        DB::table('sub_submenus')->truncate();
        DB::table('submenus')->truncate();
        DB::table('menus')->truncate();
        try {
          // $this->call(DashboardMenuSeeder::class);
          $this->call(ChatMenuSeeder::class);
          $this->call(CrmMenuSeeder::class);
          $this->call(ReportingMenuSeeder::class);
          $this->call(BroadcastMenuSeeder::class);
          $this->call(SettingMenuSeeder::class);
          // $this->call(SalesMenuSeeder::class);
          // $this->call(TicketingMenuSeeder::class);
          $this->call(WhatsappMenuSeeder::class);
          // $this->call(ReportingSubmenuSeeder::class); //submenu report
          $this->call(IntentMenuSeeder::class);
          $this->call(NLUMenuSeeder::class);
          $this->call(BotSettingMenuSeeder::class);
          DB::commit();
        } catch (\Exception $e) {
          DB::rollback();
        }
    }
}
