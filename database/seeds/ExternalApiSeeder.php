<?php

use Illuminate\Database\Seeder;
use Database\Seeds\ExternalApis\LineSeeder;
use Database\Seeds\ExternalApis\FacebookSeeder;
use Database\Seeds\ExternalApis\TelegramSeeder;
use Database\Seeds\ExternalApis\WhatsappSeeder;
use Database\Seeds\ExternalApis\InfobipSeeder;
use Database\Seeds\ExternalApis\LennaSeeder;
use Database\Seeds\ExternalApis\GoogleFCMSeeder;
use Database\Seeds\ExternalApis\DamcorpSeeder;
use Database\Seeds\ExternalApis\QontakSeeder;
use Database\Seeds\ExternalApis\WappinSeeder;
use Database\Seeds\ExternalApis\SocketSeeder;
use Database\Seeds\ExternalApis\AISeeder;
use Illuminate\Support\Facades\DB;

class ExternalApiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        DB::table('external_api_endpoints')->truncate();
        DB::table('external_apis')->truncate();
        $this->call(LineSeeder::class);
        $this->call(FacebookSeeder::class);
        $this->call(TelegramSeeder::class);
        $this->call(WhatsappSeeder::class);
        $this->call(LennaSeeder::class);
        $this->call(GoogleFCMSeeder::class);
        $this->call(InfobipSeeder::class);
        $this->call(DamcorpSeeder::class);
        $this->call(QontakSeeder::class);
        $this->call(WappinSeeder::class);
        $this->call(SocketSeeder::class);
        $this->call(AISeeder::class);
        DB::commit();
    }
}
