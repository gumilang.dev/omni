<?php

use Illuminate\Database\Seeder;
use DB as Database;
use Carbon\Carbon;

class CustomIntegrationListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('omnichannel.custom_integration_list')->truncate();
        $data = [
          "app_id" => 6, // ganti jadi app id bali fiber.
          "is_active" => true,
          "created_at" => Carbon::now()->toDateTimeString(),
          "updated_at" => Carbon::now()->toDateTimeString(),
          "custom_integration_id" => 1,
        ];
        DB::table("omnichannel.custom_integration_list")->insert($data);
    }
}
