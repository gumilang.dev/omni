<?php

use Illuminate\Database\Seeder;
use DB as Database;
use Carbon\Carbon;

class CustomIntegrationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('omnichannel.custom_integration')->truncate();
        $data = [
          "partner_id" => 1,
          "name" => "Qontak CRM",
          "created_at" => Carbon::now()->toDateTimeString(),
          "updated_at" => Carbon::now()->toDateTimeString()
        ];
        DB::table("omnichannel.custom_integration")->insert($data);
    }
}
