<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Modules\User\Models\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'app_id' => rand(1,3),
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'channel_user_id' => rand(111,130),
        'channel_id' => rand(1,4),
        'phone' => $faker->phoneNumber,
        'gender' => $faker->randomElement(['male', 'female']),
        'birth_date' => Carbon::now(),
        'location' => $faker->randomElement(['jakarta', 'bandung','surabaya']),
        'picture' => "https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Gatto_europeo4.jpg/250px-Gatto_europeo4.jpg",
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        // 'remember_token' => str_random(10),
    ];
});
