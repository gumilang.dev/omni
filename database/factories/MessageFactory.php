<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Modules\Message\Models\Message::class, function (Faker $faker) {
  $height = 600;
  $width = 400;
  $dummyImage = [
    'https://pilgrimsvets.org.uk/wp-content/uploads/2017/05/cat-pilgrims-600x400.jpg',
    'https://www.catster.com/wp-content/uploads/2016/06/vet-anemia-cat-TN-600x400.jpg',
    'https://pilgrimsvets.org.uk/wp-content/uploads/2017/05/because-we-need-another-cat-on-there-600x400.jpg'
  ];
  $dummyText = "$faker->sentence ".uniqid();
  $type = [
      [
        'type'=>'text',
        'text'=>$dummyText
      ],
      [
        'type'=>'carousel',
        'imageAspectRatio'=>"rectangle",
        'imageSize'=>"cover",
        'columns'=>
        [
          (object)[
            'thumbnailImageUrl'=>$dummyImage[rand(0,2)],
            'imageBackgroundColor'=>'#FFFFFF',
            'title'=>'Pikachu',
            'text'=>'Informasi tentang pikacu',
            'defaultAction'=>(object)[
              'type'=>'uri',
              'label'=>'View detail',
              'uri'=>'www.google.com',
            ],
            'actions'=> [  
             (object) [  
                'type'=>'postback',
                'label'=>'Choose',
                'data'=>'action=choose&itemid=13',
             ],
             (object)[  
                'type'=>'uri',
                'label'=>'Go to google',
                'uri'=>'www.google.com'
             ]
            ]
          ],
          (object)[
            'thumbnailImageUrl'=>$dummyImage[rand(0,2)],
            'imageBackgroundColor'=>'#FFFFFF',
            'title'=>'Pikachu',
            'text'=>'Informasi tentang pikacu',
            'defaultAction'=>(object)[
              'type'=>'uri',
              'label'=>'View detail',
              'uri'=>'www.google.com',
            ],
            'actions'=> [  
             (object) [  
                'type'=>'postback',
                'label'=>'Choose',
                'data'=>'action=choose&itemid=13',
             ],
             (object)[  
                'type'=>'uri',
                'label'=>'Go to google',
                'uri'=>'www.google.com'
             ]
            ]
          ],
          (object)[
            'thumbnailImageUrl'=>$dummyImage[rand(0,2)],
            'imageBackgroundColor'=>'#FFFFFF',
            'title'=>'Pikachu',
            'text'=>'Informasi tentang pikacu',
            'defaultAction'=>(object)[
              'type'=>'uri',
              'label'=>'View detail',
              'uri'=>'www.google.com',
            ],
            'actions'=> [  
             (object) [  
                'type'=>'postback',
                'label'=>'Choose',
                'data'=>'action=choose&itemid=13',
             ],
             (object)[  
                'type'=>'uri',
                'label'=>'Go to google',
                'uri'=>'www.google.com'
             ]
            ]
          ],
          (object)[
            'thumbnailImageUrl'=>$dummyImage[rand(0,2)],
            'imageBackgroundColor'=>'#FFFFFF',
            'title'=>'Pikachu',
            'text'=>'Informasi tentang pikacu',
            'defaultAction'=>(object)[
              'type'=>'uri',
              'label'=>'View detail',
              'uri'=>'www.google.com',
            ],
            'actions'=> [  
             (object) [  
                'type'=>'postback',
                'label'=>'Choose',
                'data'=>'action=choose&itemid=13',
             ],
             (object)[  
                'type'=>'uri',
                'label'=>'Go to google',
                'uri'=>'www.google.com'
             ]
            ]
          ],
          (object)[
            'thumbnailImageUrl'=>$dummyImage[rand(0,2)],
            'imageBackgroundColor'=>'#FFFFFF',
            'title'=>'Pikachu',
            'text'=>'Informasi tentang pikacu',
            'defaultAction'=>(object)[
              'type'=>'uri',
              'label'=>'View detail',
              'uri'=>'www.google.com',
            ],
            'actions'=> [  
             (object) [  
                'type'=>'postback',
                'label'=>'Choose',
                'data'=>'action=choose&itemid=13',
             ],
             (object)[  
                'type'=>'uri',
                'label'=>'Go to google',
                'uri'=>'www.google.com'
             ]
            ]
          ],
        ],    
      ],
      [
        'type'=>'image',
        'previewImageUrl'=>$dummyImage[rand(0,2)],
        'originalContentUrl'=>$dummyImage[rand(0,2)]
      ],
      [
        'type'=>'confirm',
        'text'=>'apakah kamu yakin?',
        'actions'=>[
          (object) [
            'type'=>'message',
            'label'=>'Yakin',
            'text'=>'yakin',
          ],
          (object) [
            'type'=>'message',
            'label'=>'Ga yakin',
            'text'=>'ga yakin',
          ]
        ]
      ],
      [
        'type'=>'summary',
        'columns'=>[
          'judul'=>'avenger',
          'harga'=>'12.000',
          'waktu'=>'12.00 WIB',

        ]
      ],
      [
        'type'=>'typing',
        'duration'=>rand(1,2)
      ],
    ];
  $rand = rand(0,0);
  $now = Carbon::now();

  $dataInArayy = rand(0,0);
  $finalResponse = [];
  for ($i=0; $i <= $dataInArayy; $i++) { 
    $finalResponse[] = json_encode($type[$rand]);
  }

  $messageAbleId=[
    1,2,3
  ];
  $messageAbleType=[
    'user',
    'user_platform'
  ];
  $roomId = rand(1,1);
  $status = [
    (object) [
      'name'=>"participant name",
      'participable_id'=>1,
      'participable_type'=>'user_platform',
      'read_at'=>null,
      'delivered_at'=>null
    ]
    ];
  return [
      'room_id' => $roomId,
      'messageable_id' => $messageAbleId[rand(0,2)],
      'messageable_type' => $messageAbleType[rand(0,1)],
      'content' => json_encode($finalResponse),
      'parsed' => $dummyText,
      'type'=>$type[$rand]['type'],
      'status'=> $status
  ];
});