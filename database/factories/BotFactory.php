<?php

use Faker\Generator as Faker;

$factory->define(Model::class, function (Faker $faker) {
    return [
      "name"=>$faker->name(),
      "description"=>$faker->sentence(),
      "channel_id"=>rand(1,3),
      "webhook"=>$faker->url,
      "token"=>$faker->md5,
      "secret_key"=>"secret",
      "created_by"=>rand(1,3),
      "updated_by"=>rand(1,3),
      "deleted_by"=>null,
      "created_at"=>$faker->dateTime(),
      "updated_at"=>$faker->dateTime(),
      "deleted_at"=>$faker->dateTime(),
    ];
});
