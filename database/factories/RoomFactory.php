<?php

use Faker\Generator as Faker;

$factory->define(App\Modules\Room\Models\Room::class, function (Faker $faker) {
    return [
        'created_by' => 1,
        'app_id' => 1,
        'channel_id' => 1
    ];
});
