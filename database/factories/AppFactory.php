<?php

use Faker\Generator as Faker;

$factory->define(App\Modules\App\Models\App::class, function (Faker $faker) {
    return [
        "name" => $faker->colorName(),
        "description" => $faker->sentence(),
        "created_by"=>rand(1,3),
        "updated_by"=>rand(1,3),
        "created_by"=>rand(1,3),  
        "created_at"=>$faker->dateTime(),
    ];
});
