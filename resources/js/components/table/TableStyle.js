export default  {
    table: {
      tableClass: "table lenna-table",
      loadingClass: "loading",
      ascendingIcon: "fas fa-caret-up",
      descendingIcon: "fas fa-caret-down",
      ascendingClass: "sorted-asc",
      descendingClass: "sorted-desc",
      sortableIcon: "fas fa-sort sort-color"
    },
    pagination: {
      wrapperClass: "vuetable-pagination float-right",
      activeClass: "active btn-primary btn",
      disabledClass: "disabled btn",
      pageClass: "btn",
      linkClass: "btn",
      dropdownClass: "form-control",
      icons: {
        first: "",
        prev: "",
        next: "",
        last: ""
      }
    }
  }