import formatter from '@/helpers/formatter'

export default [
    // '__sequence',
    {
        name: 'sequence-slot',
        title: 'No',
    },
    {
        name: 'name',
        title: 'Name',
        sortField: 'name',
    },

    {
        name: 'email',
        title: 'Email',
        sortField: 'email',
        formatter(value) {
            return formatter.trimText(value)
        }
    },
    {
        name: 'nickname',
        title: 'Nickname',
        sortField: 'nickname',
    },
    {
        name: 'column1-slot',
        title: 'Channel',
    },
    {
        name: 'actions-slot',
        title: 'Actions',
    },
];
