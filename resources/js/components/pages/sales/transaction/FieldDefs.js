import formatter from '@/helpers/formatter'
export default [
    // '__sequence',
    {
        name: 'sequence-slot',
        title: 'No',
    },
    {
        name: 'userName',
        title: 'User Name',
        sortField: 'userName',
    },
    {
        name: 'userEmail',
        title: 'User Email',
        sortField: 'userEmail',
    },
    {
        name: 'description',
        title: 'Description',
        sortField: 'description',
    },

    {
        name: 'amountString',
        title: 'Amount',
        sortField: 'amountString'
    },
    // {
    //     name: 'cogs_amount',
    //     title: 'Cogs Amount',
    //     sortField: 'cogs_amount',
    //     formatter(value) {
    //         return formatter.currency(value)
    //     }
    // },
    {
        name: 'status',
        title: 'Status',
        sortField: 'status',
    },
    {
        name: 'longDate',
        title: 'Date',
        sortField: 'longDate',
    },
    {
        name: 'actions-slot',
        title: 'actions',
    }
];
