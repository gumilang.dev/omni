import formatter from '@/helpers/formatter'

export default [
    // '__sequence',
    {
        name: 'id',
        title: 'ID',
        sortField: 'id',
    },
    {
        name: 'name',
        title: 'Name',
        sortField: 'name',
    },
    {
        name: 'price',
        title: 'Price',
        sortField: 'price',
        formatter(value) {
            return formatter.currency(value)
        }
    },

    {
        name: 'category.category',
        title: 'category',
        sortField: 'category.category',
    },
    {
        name: 'operator.operator',
        title: 'operator',
        sortField: 'operator.operator',
    },
    {
        name: 'type.type',
        title: 'type',
        sortField: 'type.type',
    },
    {
        name: 'is_active',
        title: 'Status',
        sortField: 'is_active',
        formatter(value) {
            return value == true ? 'Active' : 'Not Active'
        }

    },
    {
        name: 'actions-slot',
        title: 'Actions',
    },
];
