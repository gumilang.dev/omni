import formatter from '@/helpers/formatter'

export default [
    // '__sequence',
    {
        name: 'sequence-slot',
        title: 'No',
    },
    {
        name: 'key',
        title: 'Key',
        sortField: 'key',
    },
    {
        name: 'value',
        title: 'Value',
        sortField: 'value',
        formatter(value) {
            return formatter.trimText(value)
        }
    },
    {
        name: 'description',
        title: 'Description',
        sortField: 'description',
        formatter(value) {
            return formatter.trimText(value)
        }
    },
    {
        name: 'created_at',
        title: 'Created At',
        sortField: 'created_at',
    },
    {
        name: 'is_active',
        title: 'Status',
        sortField: 'is_active',
        formatter(value) {
            return value == true ? 'Active' : 'Not Active'
        }

    },
    {
        name: 'actions-slot',
        title: 'Actions',
    },
];
