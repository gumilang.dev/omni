import formatter from '@/helpers/formatter'

export default [
    // '__sequence',
    {
        name: 'sequence-slot',
        title: 'No',
    },
    {
        name: 'title',
        title: 'Title',
        sortField: 'title',
    },
    {
        name: 'category',
        title: 'Category',
        sortField: 'category',
    },

    {
        name: 'created_at',
        title: 'Created At',
        sortField: 'created_at',
    },
    {
        name: 'is_active',
        title: 'Status',
        sortField: 'is_active',
        formatter(value) {
            return value == true ? 'Active' : 'Not Active'
        }

    },
    {
        name: 'actions-slot',
        title: 'Actions',
    },
];
