
require('./bootstrap');

// core
import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import VueEvents from 'vue-events'
import Router from './router/index'
import Store from './store'

// vendor
import VeeValidate from 'vee-validate';
import BootstrapVue from 'bootstrap-vue'
import Hashids from 'hashids';
import io from 'socket.io-client';
import Viewer from 'v-viewer'
import Notifications from 'vue-notification'
import VueScrollTo from 'vue-scrollto'
import InfiniteLoading from "vue-infinite-loading";

// vendor css
import 'viewerjs/dist/viewer.css'
require('vue2-animate/dist/vue2-animate.min.css')

// custom
import { axiosInitialize, isGuest } from './helpers/general'

// check is user logged in
isGuest()

// global component
import Loading from '@/components/general/Loading';
import BasicModal from '@/components/general/modals/BasicModal';
Vue.component('lenna-loading', Loading);
Vue.component('lenna-modal', BasicModal);


// vendor use
Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(VueEvents)
Vue.use(VeeValidate, {
    fieldsBagName: 'veeFields'
})
Vue.use(Notifications)
Vue.use(VueScrollTo)
Vue.use(BootstrapVue);
Vue.use(Viewer, {
    defaultOptions: {
        movable: false,
        filter(image) {
            return image.getAttribute('class').includes('zoomable');
        }
    }
})

Vue.use(InfiniteLoading, {
    props: {
        spinner: 'default',
        distance: 5000
        /* other props need to configure */
    },
    system: {
        throttleLimit: 500,
        /* other settings need to configure */
    },
});

const router = Router
const store = Store

// axios global vue prototype
let axiosConfig = {
    baseURL: process.env.MIX_APP_URL,
    timeout: 30000,
    headers: {
        userId: JSON.parse(localStorage.getItem('user')).id,
        token: JSON.parse(localStorage.getItem('user')).token.access_token
    }
}

// vue global variable
Vue.prototype.$socket = io(process.env.MIX_SOCKET_URL);
Vue.prototype.$echo = Echo;
Vue.prototype.$hashids = new Hashids('', 6);

Vue.prototype.$axios = axios.create(axiosConfig);
Vue.prototype.$axiosNoLoading = axios.create(axiosConfig);

Vuex.Store.prototype.$axiosNoLoading = axios.create(axiosConfig);
Vuex.Store.prototype.$axios = axios.create(axiosConfig);
axiosInitialize(Vue.prototype.$axios, store)
axiosInitialize(Vuex.Store.prototype.$axios, store)

// main components
import MainApp from './components/main/MainApp'
import CoreMixin from './mixins/CoreMixin'
Vue.mixin(CoreMixin)
const app = new Vue({
    el: '#app',
    components: { 'main-app': MainApp },
    router,
    store,
});



