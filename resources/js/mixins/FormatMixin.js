import formatter from '@/helpers/formatter'

export default {

    methods: {
        formatCurrency(value) {
            return formatter.currency(value)

        }
    },
}