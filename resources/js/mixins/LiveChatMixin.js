export default {
    computed: {
        liveChatHeader() {
            if (this.selectedRoom.livechat.status == "live") {
                return "Live now";
            } else if (this.selectedRoom.livechat.status == "request") {
                return "Requesting live chat";
            }
        },

        liveChatButton() {
            if (_.isNull(this.selectedRoom.livechat)) {
                if (this.appSettings.always_allow_livechat) {
                    return "Handle";
                }
            }
            else if (this.selectedRoom.livechat.status == "request") {
                return "Handle";
            }
            else if (this.selectedRoom.livechat.status == "live") {
                return "Resolve";
            }


        },
        isLive() {
            if (this.selectedRoom == null) {
                return false
            }
            // return true
            return this.selectedRoom.livechat != null && this.selectedRoom.livechat.status == 'live'
        },

        appSettings() {
            return this.$store.getters["app/app"].settings
        },
    },
}