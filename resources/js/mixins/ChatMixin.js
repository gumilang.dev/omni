import formatter from '@/helpers/formatter'

export default {
    props: {
        message: {
            type: Object,
            default: null
        },
        content: {
            type: Object,
            default: null
        },
        isSent: false,
    },
    computed: {
        selectedRoom() {
            return this.$store.getters['chat/selectedRoom'];
        },
    },
    methods: {
        formatDate(date, format) {
            if (_.isNull(date)) {
                return 'sending'
            }
            return formatter.formatDate(date, format)
        }
    }
}