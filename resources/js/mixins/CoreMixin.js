
export default {
    data() {
        return {
            baseUrl: process.env.MIX_APP_URL,
            backendUrl: process.env.MIX_BACKEND_URL,
            authUrl: process.env.MIX_AUTH_URL,
            eventPath: "App\\Events\\",

        }
    },
    computed: {
        isComponentLoading() {
            return this.$store.getters["general/isComponentLoading"];
        },
        isSidebarOpen() {
            return this.$store.state.windowWidth > 500;
        },
        itemId() {
            return this.$route.path.split('/')[3];
        },
        submenu() {
            return this.$route.path.split('/')[2];
        },
    },
    methods: {
        logx(data) {
            console.log(data)
        },
        imgErrorHandler(event) {
            event.target.src = this.asset("images/pictures/no_avatar.jpg");
        },
        asset(path) {
            return `${this.baseUrl}/${path}`
        },
        parseJson(content) {
            return JSON.parse(content)
        },
    },

}