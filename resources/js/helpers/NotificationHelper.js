import Push from "push.js";

class NotificationHelper {
    constructor(data) {
        this.vm = data.vm
        if (!Push.Permission.has("granted")) {
            Push.Permission.request();
        }
    }
    show(data) {
        if (
            _.isNull(this.vm.selectedRoom) ||
            this.vm.selectedRoom.id != data.room.id
        ) {
            this.floatingNotification(data)
        }
    }
    desktopNotification(data) {
        Push.create(data.notifTitle, {
            body: data.notifBody,
            icon: this.vm.asset("images/icons/lenna-logo.png"),
            timeout: 4000,
            onClick: function () {
                window.focus();
                this.vm.close();
            }
        });
    }
    webNotification(data) {
        this.vm.$notify({
            group: "chat-notif",
            title: data.notifTitle,
            text: data.notifBody
        });
    }

    floatingNotification(data) {
        let notificationBody = 'New notification'
        switch (data.notificationType) {
            case "chat-message":
                let lastContent = _.last(data.message.content);
                notificationBody = lastContent.text || lastContent.type || 'New message'
                break;
            case "live-request":
                notificationBody = 'Want to live chat'
                break;
        }
        if (Push.Permission.has("granted")) {
            this.desktopNotification({ notifTitle: data.user.name, notifBody: notificationBody })

        } else {
            this.webNotification({ notifTitle: data.user.name, notifBody: notificationBody })

        }
        this.updateNotificationList({ ...data, notifTitle: data.user.name, notifBody: notificationBody })
    }
    updateNotificationList(data) {
        let notif
        switch (data.notificationType) {
            case "chat-message":

                notif = {
                    id: "chat-message" + data.room.id,
                    type: 'chat-message',
                    unread_count: 1,
                    data: {
                        room: data.room
                    },
                    items: [
                        {
                            title: data.notifTitle,
                            body: data.notifBody,
                            created_at: data.created_at,
                        },
                    ],
                }
                break;

            case "live-request":
                notif = {
                    id: "live-request" + data.room.id,
                    type: 'live-request',
                    unread_count: 1,
                    data: {
                        room: data.room
                    },
                    items: [
                        {
                            title: data.notifTitle,
                            body: data.notifBody,
                            created_at: data.created_at,
                        },
                    ],
                }
                break;

        }
        this.vm.$store.dispatch("notification/updateNotifList", notif);
    }
}
export default NotificationHelper