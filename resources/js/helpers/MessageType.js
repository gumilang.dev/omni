import moment from "moment"
class LennaMessageType {
    constructor(input) {
        this.uploadUrl = process.env.MIX_APP_URL + '/upload/chat'
        // this.uploadUrl = 'https://0577196e.ngrok.io/app/public/upload/chat'

        this.message = {
            temporary_id: moment().format("x"),
            messageable_id: input.messageable_id,
            messageable_type: "user_platform",
            room_id: input.roomId,
            created_at: null,
            // created_at: moment().format("YYYY-MM-DD kk:mm:ss"),

        }
    }
    text(input) {
        let content = [
            {
                type: "text",
                text: input.text || '',
                speech: input.text || '',
            }
        ]
        return {
            ...this.message,
            content: content
        }
    }
    image(input) {
        let content = [
            {
                type: "image",
                originalContentUrl: this.uploadUrl + "/" + input.fileName,
                previewImageUrl: this.uploadUrl + "/" + input.fileName,
            }
        ]
        return {
            ...this.message,
            content: content
        }
    }
    file(input) {
        let content = [
            {
                type: "file",
                fileUrl: this.uploadUrl + "/" + input.fileName,
                fileName: input.fileName,
            }
        ]
        return {
            ...this.message,
            content: content
        }
    }
}
export default LennaMessageType
