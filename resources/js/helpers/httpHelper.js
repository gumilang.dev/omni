export function errorToast(vm,error) {
    vm.$notify({
        group: 'error',
        type: "error",
        title: 'Error',
        text: error.response.data.message,
    });
}
export function successToast(vm,text) {
    vm.$notify({
        group: "notif",
        type: "info",
        title: "Info",
        text: text
      });
}