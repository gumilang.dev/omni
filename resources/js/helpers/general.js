
export const endPoint = {
  baseUrl: process.env.MIX_APP_URL,
}
export function axiosInitialize(axios,store) {
  // Add a request interceptor
  axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    store.dispatch('general/updateIsLoading',true)

    return config;
  }, function (error) {
    // Do something with request error
    store.dispatch('general/updateIsLoading',true)
    
    return Promise.reject(error);
  });

  // Add a response interceptor
  axios.interceptors.response.use(function (response) {
    // Do something with response data
    store.dispatch('general/updateIsLoading',false)
    return response;
  }, function (error) {
    // Do something with response error
    store.dispatch('general/updateIsLoading',false)
    
    if (error.response.data.statusCode == 4001) {
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(localStorage.getItem('user')).token.access_token;
      axios.post(process.env.MIX_AUTH_URL+'/api/logout').then(
          localStorage.removeItem('user'),
          window.location.href = process.env.MIX_AUTH_URL + '/login',
      )
    }
    if(process.env.MIX_APP_ENV != 'production'){
      console.log(error.response)
    }
    return Promise.reject(error);
  });
}

export function isGuest() {
  if (!localStorage.getItem('user')) {
    window.location.href = process.env.MIX_AUTH_URL + '/login'
  }
}

