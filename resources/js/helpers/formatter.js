import moment from "moment"

class Formatter {
    currency(value) {
        let val = (value / 1).toFixed(0).replace('.', ',')
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
    }
    trimText(value, length = 20) {
        if (value.length > length) {
            return value.substr(0, length) + "..";
        }
        return value
    }
    formatDate(date, format) {
        return moment(date).format(format)
    }
}
export default new Formatter()