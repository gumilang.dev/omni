const state = {
    dashboardData:null,
    totalConversation: null,
    totalUser: null,
    totalTransaction: null,
    totalAmount: null,
    consumerBase: null,
    dailyMessage: null,
    dailyUser: null,
}
const getters = {
    dashboardData(state) {
        return state.dashboardData
    },
    totalConversation(state) {
        return state.totalConversation
    },
    totalUser(state) {
        return state.totalUser
    },
    totalTransaction(state) {
        return state.totalTransaction
    },
    totalAmount(state) {
        return state.totalAmount
    },
    consumerBase(state) {
        return state.consumerBase
    },
    dailyMessage(state) {
        return state.dailyMessage
    },
    dailyUser(state) {
        return state.dailyUser
    },
}
const mutations = {
    SET_DASHBOARD_DATA: (state, payload) => {
        state.dashboardData = payload
    },
    SET_TOTAL_CONVERSATION: (state, payload) => {
        state.totalConversation = payload
    },
    SET_TOTAL_USER: (state, payload) => {
        state.totalUser = payload
    },
    SET_TOTAL_TRANSACTION: (state, payload) => {
        state.totalTransaction = payload
    },
    SET_TOTAL_AMOUNT: (state, payload) => {
        state.totalAmount = payload
    },
    SET_CONSUMER_BASE: (state, payload) => {
        state.consumerBase = payload
    },
    SET_DAILY_MESSAGE: (state, payload) => {
        state.dailyMessage = payload
    },
    SET_DAILY_USER: (state, payload) => {
        state.dailyUser = payload
    },
}
const actions = {
    async getDashboardData(context) {
        let response = await this.$axios.get(`api/${context.rootGetters.appId.hashed}/dashboard/get-data`);
        context.commit('SET_DASHBOARD_DATA', response.data.data);
    },
    async getTotalConversation(context) {
        let response = await this.$axios.get(`api/${context.rootGetters.appId.hashed}/dashboard/get-total-conversation`);
        context.commit('SET_TOTAL_CONVERSATION', response.data.data);
    },
    async getTotalUser(context) {
        let response = await this.$axios.get(`api/${context.rootGetters.appId.hashed}/dashboard/get-total-user`);
        context.commit('SET_TOTAL_USER', response.data.data);
    },
    async getTotalTransaction(context) {
        let response = await this.$axios.get(`api/${context.rootGetters.appId.hashed}/dashboard/get-total-transaction`);
        context.commit('SET_TOTAL_TRANSACTION', response.data.data);
    },
    async getTotalAmount(context) {
        let response = await this.$axios.get(`api/${context.rootGetters.appId.hashed}/dashboard/get-total-amount`);
        context.commit('SET_TOTAL_AMOUNT', response.data.data);
    },
    async getConsumerBase(context) {
        let response = await this.$axios.get(`api/${context.rootGetters.appId.hashed}/dashboard/get-consumer-base`);
        context.commit('SET_CONSUMER_BASE', response.data.data);
    },
    async getDailyMessage(context) {
        let response = await this.$axios.get(`api/${context.rootGetters.appId.hashed}/dashboard/get-daily-message`);
        context.commit('SET_DAILY_MESSAGE', response.data.data)
    },
    async getDailyUser(context) {
        let response = await this.$axios.get(`api/${context.rootGetters.appId.hashed}/dashboard/get-daily-user`);
        context.commit('SET_DAILY_USER', response.data.data)
    }
}
export const dashboardStore = {
    namespaced: true,
    actions,
    getters,
    state,
    mutations
}
