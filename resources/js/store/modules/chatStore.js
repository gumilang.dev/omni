
import LennaMessageType from "@/helpers/MessageType"
const state = {
    roomList: [],
    selectedRoom: null,
    unreadMessage: null
}

const getters = {
    roomList(state) {
        return state.roomList
    },
    selectedRoom(state) {
        return state.selectedRoom
    },
    unreadMessage(state) {
        return state.unreadMessage
    }
}
const mutations = {
    HANDLE_SENT_MESSAGE: (state, payload) => {
        let roomIndex = _.findIndex(state.roomList, function (obj) {
            return obj.id == payload.roomId
        })

        state.roomList[roomIndex].messages.push(payload.message)


    },
    UPDATE_MESSAGE: (state, payload) => {
        let roomIndex = _.findIndex(state.roomList, function (obj) {
            return obj.id == payload.message.room_id
        })
        let messageIndex = _.findIndex(state.roomList[roomIndex].messages, function (obj) {
            return obj.temporary_id == payload.temporaryId
        })
        state.roomList[roomIndex].messages[messageIndex].created_at = payload.message.created_at
        state.roomList[roomIndex].messages[messageIndex] = payload.message

        state.roomList[roomIndex].updated_at = payload.message.created_at

    },
    RESET_SELECTED_ROOM: (state) => {
        state.selectedRoom = null
    },
    SET_UNREAD_MESSAGE: (state, payload) => {
        state.unreadMessage = payload
    },
    SET_ROOM_LIST: (state, payload) => {
        state.roomList = payload
    },
    HANDLE_SELECT_ROOM: (state, payload) => {
        state.selectedRoom = _.find(state.roomList, function (obj) {
            return obj.id == payload.id
        })

    },
    HANDLE_NEW_MESSAGE: (state, payload) => {
        let isExist = _.find(state.roomList, function (obj) {
            return obj.id == payload.room.id
        })
        if (isExist) {
            // is room exist
            let roomIndex = _.findIndex(state.roomList, function (obj) {
                return obj.id == payload.room.id
            })
            state.roomList[roomIndex].messages.push(payload.message)
            state.roomList[roomIndex].updated_at = payload.room.updated_at



            return
        }
        else {
            // if doesnt exist
            payload.room.unread_count = 0
            state.roomList.push(payload.room)
        }

    },
    ADD_MORE_MESSAGES: (state, payload) => {

        let roomIndex = _.findIndex(state.roomList, function (obj) {
            return obj.id == state.selectedRoom.id
        })
        state.roomList[roomIndex].messages = payload.concat(state.roomList[roomIndex].messages)

    },
    REQUEST_LIVE_CHAT: (state, payload) => {
        let isExist = _.find(state.roomList, function (obj) {
            return obj.id == payload.room.id
        })
        if (isExist) {
            // is room exist
            let roomIndex = _.findIndex(state.roomList, function (obj) {
                return obj.id == payload.room.id
            })
            state.roomList[roomIndex].livechat = { ...payload.liveChat }


            return
        }
        else {
            // if doesnt exist
            payload.room.unread_count = 0
            state.roomList.push(payload.room)
        }

    },
    UPDATE_LIVE_CHAT: (state, payload) => {
        let isExist = _.find(state.roomList, function (obj) {
            return obj.id == payload.room.id
        })
        if (isExist) {
            // is room exist
            let roomIndex = _.findIndex(state.roomList, function (obj) {
                return obj.id == payload.room.id
            })
            // reset null if resolved, else set with response data
            state.roomList[roomIndex].livechat = payload.liveChat.status == 'resolved' ? null : payload.liveChat


            return
        }
    },
    SET_UNREAD_COUNT: (state, payload) => {
        let roomIndex = _.findIndex(state.roomList, function (obj) {
            return obj.id == payload.room.id
        })
        switch (payload.event) {
            case "select_room":
                state.roomList[roomIndex].unread_count = 0
                break;
            case "new_message_selected":
                state.roomList[roomIndex].unread_count = 0
                break;
            case "new_message":
                state.roomList[roomIndex].unread_count++
                break;
            default:
                break;
        }
    }
}
const actions = {
    liveChatRequest(context, payload) {
        context.commit('REQUEST_LIVE_CHAT', payload)

    },
    liveChatUpdate(context, payload) {
        context.commit('UPDATE_LIVE_CHAT', payload)

    },
    newMessage(context, payload) {
        context.commit('HANDLE_NEW_MESSAGE', payload)

        if (!_.isNull(context.getters.selectedRoom) && context.getters.selectedRoom.id == payload.room.id) {

            context.dispatch('updateUnreadMessage', {
                'room': payload.room,
                'senderId': context.rootGetters.localStorage.decodedUserId,
                'forceUpdate': true
            })
            context.commit('SET_UNREAD_COUNT', { ...payload, event: "new_message_selected" })


        }
        else {
            context.commit('SET_UNREAD_COUNT', { ...payload, event: "new_message" })
        }
    },
    async updateUnreadMessage(context, payload) {
        let condition = !_.isUndefined(payload.room.unread_count) ? payload.room.unread_count > 0 : payload.forceUpdate == true
        if (condition) {
            let response = this.$axiosNoLoading.put(`api/${context.rootGetters.appId.hashed}/message/update-unread-message`, {
                roomId: payload.room.id,
                senderId: payload.senderId,
            });
        }
    },
    async selectRoom(context, payload) {
        context.dispatch('updateUnreadMessage', payload)
        context.commit('HANDLE_SELECT_ROOM', payload.room);
        context.commit('SET_UNREAD_COUNT', { ...payload, event: "select_room" })


    },
    async sendMessage(context, payload) {
        let lennaMessageType = new LennaMessageType({
            messageable_id: payload.senderId,
            roomId: payload.room.id
        })
        let message
        switch (payload.message.type) {
            case "text":
                message = lennaMessageType.text({
                    text: payload.message.text
                })
                break;
            case "image":
                message = lennaMessageType.image({
                    fileName: payload.message.fileName
                })
                break;
            case "file":
                message = lennaMessageType.file({
                    fileName: payload.message.fileName
                })
                break;
            default:
                break;
        }

        context.commit('HANDLE_SENT_MESSAGE', { roomId: payload.room.id, message: message });



        try {
            let response = await this.$axiosNoLoading.post(`/api/${context.rootGetters.appId.hashed}/chat/send-message`,
                {
                    // sender_id will get from decoded room id from API
                    roomId: payload.room.id,
                    senderId: payload.senderId,
                    message: message.content,
                    temporaryId: message.temporary_id
                })
            context.commit('UPDATE_MESSAGE', response.data.data);

        } catch (error) {
            payload.vm.$notify({
                group: 'error',
                type: "error",
                title: 'Error',
                text: error.response.data.message,
            });
        }


    },
    async getRoomList(context) {
        // context.dispatch('general/updateIsComponentLoading', true, { root: true })

        let response = await this.$axiosNoLoading.get(`api/${context.rootGetters.appId.hashed}/chat/get-room-list`);
        context.commit('SET_ROOM_LIST', response.data.data.room_list);
        // context.dispatch('general/updateIsComponentLoading', false, { root: true })

    },
    async handleInfiniteScroll(context, payload) {

        context.commit('ADD_MORE_MESSAGES', payload.messages);

    }

}
export const chatStore = {
    namespaced: true,
    actions,
    getters,
    state,
    mutations
}