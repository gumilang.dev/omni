
const state = {
  isLoading:true,
  isComponentLoading:true,
}

const getters= {
  httpOptions(state,getters,rootState,rootGetters){
    return {
      baseURL: rootState.appUrl,
      timeout: 30000,
      headers: {
        userId :rootState.localStorage.user.id,
        token :rootState.localStorage.user.token.access_token
      }
    }
  },
  isLoading(state){
    return state.isLoading
  },
  isComponentLoading(state){
    return state.isComponentLoading
  },
}
const mutations = {
  updateIsComponentLoading : (state,payload) => {
    state.isComponentLoading= payload
  },
  updateIsLoading : (state,payload) => {
    state.isLoading= payload
  }
}
const actions = {
  updateIsComponentLoading(context,payload){
    context.commit('updateIsComponentLoading',payload);
  },
  updateIsLoading(context,payload){
    context.commit('updateIsLoading',payload);
  },
}
export const generalStore = {
  namespaced:true,
  actions,
  getters,
  state,
  mutations
}