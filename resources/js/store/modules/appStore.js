
const state = {
    app: {}
}

const getters = {
    app(state) {
        return state.app
    },

}
const mutations = {

    SET_APP: (state, payload) => {
        state.app = { settings: { ...payload.settings } }
    },

}
const actions = {
    async getAppData(context) {
        let response = await this.$axios.get(`api/${context.rootGetters.appId.hashed}/app`);
        context.commit('SET_APP', response.data.data.app)

    },
    updateAppData(context, payload) {
        console.log(payload)
        context.commit('SET_APP', payload)

    },

}
export const appStore = {
    namespaced: true,
    actions,
    getters,
    state,
    mutations
}