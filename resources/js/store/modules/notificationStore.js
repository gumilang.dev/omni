
const state = {
    notifList: [],
}

const getters = {
    notifList(state) {
        return state.notifList
    },
    
}
const mutations = {
    SET_NOTIF_LIST: (state, payload) => {
        state.notifList = payload
    },
    SELECT_NOTIF:(state,payload)=>{
        let notifIndex = _.find(state.notifList, function (obj) {
            return obj.id == payload.id
        })
        state.notifList.splice(notifIndex, 1);
        
    },
    UPDATE_NOTIF_LIST: (state, payload) => {
        let isExist = _.find(state.notifList, function (obj) {
            return obj.id == payload.id
        })
        if (isExist) {
            // is notif exist
            let notifIndex = _.findIndex(state.notifList, function (obj) {
                return obj.id == payload.id
            })
            
            let prevUnreadCount = state.notifList[notifIndex].unread_count
            state.notifList[notifIndex].items.push(payload.items[0])
            
            state.notifList[notifIndex].unread_count = prevUnreadCount + 1
            
            return
        }
        else {
            // if doesnt exist
            state.notifList.push(payload)
        }
    },
}
const actions = {
    async getNotifList(context) {
        let response = await this.$axios.get(`api/${context.rootGetters.appId.hashed}/app/get-notif-list`);

        context.commit('SET_NOTIF_LIST', response.data.data.notif_list);
    },

    updateNotifList(context, payload) {
        context.commit('UPDATE_NOTIF_LIST', payload)
    },
    selectNotif(context, payload) {
        context.commit('SELECT_NOTIF', payload)
       
    },
}
export const notificationStore = {
    namespaced: true,
    actions,
    getters,
    state,
    mutations
}