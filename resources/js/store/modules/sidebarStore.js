const state = {
  menus:[],
}

const getters = {
  menus(state){
    return state.menus
  },
}

const mutations = {
  SET_MENUS : (state,payload) => {state.menus = payload },
}
const actions = {
  async getMenus(context,payload){
    let response = await this.$axios.get("api/menus")
    context.commit('SET_MENUS', response.data);

  },

}
export const sidebarStore = {
  namespaced:true,
  state,
  getters,
  mutations,
  actions
}