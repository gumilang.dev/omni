import Vue from 'vue'
import Vuex from 'vuex'
import Router from '@/router/index'
import Hashids from 'hashids';
// modules
import { sidebarStore } from './modules/sidebarStore'
import { chatStore } from './modules/chatStore'
import { generalStore } from './modules/generalStore'
import { dashboardStore } from './modules/dashboardStore'
import { notificationStore } from './modules/notificationStore'
import { appStore } from './modules/appStore'

Vue.use(Vuex)
let hashids = new Hashids('', 6)

export default new Vuex.Store({
  state: {
    appUrl: process.env.MIX_APP_URL,
    localStorage: {
      user: JSON.parse(localStorage.getItem('user')),
      decodedUserId: (localStorage.getItem('user')) ? hashids.decode(JSON.parse(localStorage.getItem('user')).id)[0] : null
    },
    windowWidth:window.innerWidth
  },
  getters: {
    localStorage(state){
      return state.localStorage
    },
    router() {
      return Router.currentRoute
    },
    appId(state, getters) {
      return {
        hashed: getters.router.params.appId,
        decoded: hashids.decode(getters.router.params.appId)[0],
      }
    }
  },
  mutations:{
    UPDATE_WIDTH : (state) => {state.windowWidth = window.innerWidth },

  },
  modules: {
    sidebar: sidebarStore,
    app: appStore,
    chat: chatStore,
    general: generalStore,
    dashboard: dashboardStore,
    notification: notificationStore,
  }
})
