
// Error page

import NotFound from '@/components/main/NotFound'

//chat
import ChatMain from '@/components/pages/chat/ChatMain'

// integration
import IntegrationMain from '@/components/pages/integration/IntegrationMain'
import ChannelLine from '@/components/pages/integration/ChannelLine'
import ChannelFacebook from '@/components/pages/integration/ChannelFacebook'
import ChannelWhatsapp from '@/components/pages/integration/ChannelWhatsapp'
import ChannelTelegram from '@/components/pages/integration/ChannelTelegram'
import ChannelWebchat from '@/components/pages/integration/ChannelWebchat'
import ChannelApp from '@/components/pages/integration/ChannelApp'
import BotIntegration from '@/components/pages/integration/BotIntegration'

// User
import CrmContainer from '@/components/pages/crm/CrmContainer'
import UserMain from '@/components/pages/crm/user/UserMain'

// Dashboard
import DashboardMain from '@/components/pages/dashboard/DashboardMain'

// Transaction
import SalesContainer from '@/components/pages/sales/SalesContainer'
import TransactionMain from '@/components/pages/sales/transaction/TransactionMain'

// settings
import SettingsContainer from '@/components/pages/settings/SettingsContainer'
import AppConfiguration from '@/components/pages/settings/appconfiguration/AppConfiguration'
import AppContentMain from '@/components/pages/settings/appcontent/AppContentMain'
import GeneralValueMain from '@/components/pages/settings/generalvalue/GeneralValueMain'
import PpobProductMain from '@/components/pages/settings/ppobproduct/PpobProductMain'

// broadcast
import BroadcastContainer from '@/components/pages/broadcast/BroadcastContainer'
import BroadcastMessageMain from '@/components/pages/broadcast/broadcastmessage/BroadcastMessageMain'

//  test
import TestPage from '@/components/general/TestPage'
export const routes = [
    //handle 404
    { path: '/404', component: NotFound },
    { path: '*', redirect: '/404' },

    {
        path: '/:appId/dashboard',
        name: 'dashboard',
        component: DashboardMain,
        props: true,
    },
    {
        path: '/:appId/chat',
        name: 'chat',
        props: true,
        component: ChatMain,
    },
    //   user
    {
        path: '/:appId/crm',
        name: 'crm',
        component: CrmContainer,
        redirect: { name: 'crm.user' },
        props: true,
        children: [
            {
                path: 'user',
                name: 'crm.user',
                component: UserMain
            },

        ]
    },
    //   user
    {
        path: '/:appId/sales',
        name: 'sales',
        component: SalesContainer,
        redirect: { name: 'sales.transaction' },
        props: true,
        children: [
            {
                path: 'transaction',
                name: 'sales.transaction',
                component: TransactionMain
            },
        ]
    },
    // integration
    {
        path: '/:appId/integration',
        name: 'integration',
        component: IntegrationMain,
        redirect: { name: 'integration.mobile' },
        props: true,
        children: [
            {
                path: 'mobile',
                name: 'integration.mobile',
                component: ChannelApp
            },
            {
                path: 'line',
                name: 'integration.line',
                component: ChannelLine
            },
            {
                path: 'facebook',
                name: 'integration.facebook',
                component: ChannelFacebook
            },
            {
                path: 'whatsapp',
                name: 'integration.whatsapp',
                component: ChannelWhatsapp
            },
            {
                path: 'telegram',
                name: 'integration.telegram',
                component: ChannelTelegram
            },
            {
                path: 'webchat',
                name: 'integration.webchat',
                component: ChannelWebchat
            },
            {
                path: 'bot-integration',
                name: 'integration.botintegration',
                component: BotIntegration
            }
        ]
    },
    //   settings
    {
        path: '/:appId/settings',
        name: 'settings',
        component: SettingsContainer,
        redirect: { name: 'settings.app-configuration' },
        props: true,
        children: [
            {
                path: 'app-configuration',
                name: 'settings.app-configuration',
                component: AppConfiguration
            },
            {
                path: 'app-content',
                name: 'settings.app-content',
                component: AppContentMain
            },
            {
                path: 'general-value',
                name: 'settings.general-value',
                component: GeneralValueMain
            },
            {
                path: 'ppob-product',
                name: 'settings.ppob-product',
                component: PpobProductMain
            },
        ]
    },

    //   broadcast
    {
        path: '/:appId/broadcast',
        name: 'broadcast',
        component: BroadcastContainer,
        redirect: { name: 'broadcast.broadcast-message' },
        props: true,
        children: [
            {
                path: 'broadcast-message',
                name: 'broadcast.broadcast-message',
                component: BroadcastMessageMain
            },

        ]
    },
    {
        path: '/:appId/test',
        name: 'test',
        component: TestPage,
        props: true,

    },


]
