
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Swipe – The Simplest Chat Platform</title>
		<meta name="description" content="#">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap core CSS -->
		<link href="/chat/css/bootstrap.min.css" type="text/css" rel="stylesheet">
		<!-- Swipe core CSS -->
		<link href="/css/gull.css" type="text/css" rel="stylesheet">
		{{-- <link href="/chat/css/swipe.min.css" type="text/css" rel="stylesheet"> --}}
		<!-- Favicon -->
		<link href="/chat/img/favicon.png" type="image/png" rel="icon">
	</head>
	<body>


    <div class="container" >
      <div class="row">
        <div class="com-md-4">
            <div class="card" style="width: 18rem;">
                <div class="card-header">
                  Featured
                </div>
                <ul class="list-group list-group-flush">
                  <li class="list-group-item ">

                  </div>

                  </li>
                  <li class="list-group-item">Dapibus ac facilisis in</li>
                </ul>
              </div>
        </div>

      </div>
      </div>
    </div>
		<!-- Bootstrap/Swipe core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script>window.jQuery || document.write('<script src="dist/js/vendor/jquery-slim.min.js"><\/script>')</script>
		<script src="/chat/js/popper.min.js"></script>
		<script src="/chat/js/swipe.min.js"></script> 
		<script src="/chat/js/bootstrap.min.js"></script>
		<script>
			function scrollToBottom(el) { el.scrollTop = el.scrollHeight; }
			scrollToBottom(document.getElementById('content'));
		</script>
	</body>
</html>