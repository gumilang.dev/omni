<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{csrf_token()}}">
  <title>Omni Messaging</title>
  <link rel="shortcut icon" href="{{asset('images/icons/lenna-logo.png') }}">
  <link rel="stylesheet" href="{{asset('gull/css/lite-purple.min.css')}}">
  <link rel="stylesheet" href="{{asset('gull/css/gull-custom2.css')}}">
  <link rel="stylesheet" href="{{asset('gull/css/perfect-scrollbar.css')}}">
  <link rel="stylesheet" href="{{asset('css/app.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">

  @include('layout.head')
</head>

<body>
  <div id="app">
    <main-app></main-app>
  </div>
  <script src="{{asset('js/app.js')}}"></script>

  {{-- </body> --}}
  <script src="{{asset('gull/js/perfect-scrollbar.min.js')}}"></script>
  <script src="{{asset('gull/js/script.min.js')}}"></script>

</body>

</html>
