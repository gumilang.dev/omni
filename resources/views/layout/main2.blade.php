<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{csrf_token()}}">
  <title>Omni Messaging</title>
  <link rel="shortcut icon" href="{{asset('images/icons/lenna-logo.png') }}">
  <link rel="icon" type="image/png" href="{{ asset('images/logo_gradient_blue.png') }}">
  <link rel="icon" type="image/png" href="{{ asset('images/icons/favicon-16x16.png') }}" sizes="16x16">
  <link rel="icon" type="image/png" href="{{ asset('images/icons/favicon-32x32.png') }}" sizes="32x32">
  <link rel="icon" type="image/png" href="{{ asset('images/icons/android-192x192.png') }}" sizes="192x192">
  <link rel="apple-touch-icon" href="{{ asset('images/icons/apple-touch-icon-180x180.png') }}" sizes="180x180">
  <meta name="keywords"
    content="lenna platform, auth, omni messaging, bot studio, omni channel, conversation studio, messaging, channels, bot">
  <meta name="author" content="Lenna">
  <meta name="description"
    content="A place to orchestration your customer relation apps, single interface to manage omnichannels and build a smarter chatbot without compromise.">
  <meta property="og:title" content="Lenna Platform">
  <meta property="og:description"
    content="A place to orchestration your customer relation apps, single interface to manage omnichannels and build a smarter chatbot without compromise.">
  <meta property="og:image" content="https://lenna.ai/wp-content/uploads/2019/05/lenna-logo.png">
  <meta name="twitter:card" content="photo">
  <meta name="twitter:url" content="https://lenna.ai/wp-content/uploads/2019/05/lenna-logo.png">
  <meta name="twitter:title" content="Lenna Platform">
  <meta name="twitter:description"
    content="A place to orchestration your customer relation apps, single interface to manage omnichannels and build a smarter chatbot without compromise.">
  <meta name="twitter:image" content="https://lenna.ai/wp-content/uploads/2019/05/lenna-logo.png">
</head>
<body>
  <div id="app">
  </div>
  <script type=text/javascript src="{{asset('dist/app.js')}}"></script>
</body>
</html>
